Imports System.Data.SqlClient

Public Class FrmCapturaHorario

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim idReceta As String
    Dim actualiza As Boolean = False
    Dim cedula As String
    Dim Codigo As Integer
    Dim reduce As Double
    Dim PMU As New PerfilModulo_Class
    Dim id As Integer
#End Region

    Private Sub txtClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                txtClave.Text = ""
                Me.ToolBarNuevo.Enabled = True
                '  Me.ToolBarEditar.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                '  Me.ToolBarRelacion.Enabled = True
                Me.ToolBarImprimir.Enabled = True
                ' Me.ToolBarTeclado.Enabled = True
                Me.txtCedula.Enabled = True
                Me.txtUsuario.Text = usuario
                txtCedula.Focus()
            Else
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Function ValidarUsuario() As Boolean
        Dim sql As String
        Dim clsConexion As New Conexion
        Dim cnnConexion As New System.Data.SqlClient.SqlConnection
        Dim rsReader As SqlClient.SqlDataReader

        cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seguridad", "CONEXION")
        cnnConexion.Open()

        sql = "SELECT Nombre FROM USUARIOS WHERE Clave_interna= '" & txtClave.Text & "' " '"

        cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")

        rsReader = clsConexion.GetRecorset(cnnConexion, sql)
        If rsReader.Read() = False Then
            ValidarUsuario = False
            Exit Function
        End If


        txtUsuario.Text = rsReader(0)
        cnnConexion.Close()

        ValidarUsuario = VSMA(cedula, Me.Name, Seguridad.Secure.Execute)
        



    End Function
    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                txtClave.Text = ""
                Me.ToolBarNuevo.Enabled = True
                '  Me.ToolBarEditar.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                ' Me.ToolBarRelacion.Enabled = True
                Me.ToolBarImprimir.Enabled = True
                ' Me.ToolBarTeclado.Enabled = True
                Me.txtCedula.Enabled = True
                cedula = User_Log.Cedula
                Me.txtUsuario.Text = User_Log.Nombre
                txtCedula.Focus()
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub FrmCapturaHorario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Defaul_Values()
        conectadobd = cConexion.Conectar("Restaurante")
        Me.SqlConnection1.ConnectionString = GetSetting("SeeSoft", "Restaurante", "CONEXION")
        Me.AdCapturaHora.Fill(Me.DsCapturaHora1.CapturaHora)
        If gloNoClave Then
            Loggin_Usuario()
        Else
            txtClave.Focus()
        End If
       
        Dim style As New DataGridViewCellStyle
        With style
            .BackColor = Color.Beige
            .ForeColor = Color.Brown
            .Font = New Font("Verdana", 10)
        End With
        Me.ToolBarRegistrar.Enabled = False
        Me.ToolBarNuevo.Enabled = False
        Me.ToolBarImprimir.Enabled = False
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarBuscar.Enabled = False
        Panel1.Enabled = False

    End Sub

    Private Sub Defaul_Values()
       
        Me.DsCapturaHora1.CapturaHora.CedulaColumn.DefaultValue = ""
        Me.DsCapturaHora1.CapturaHora.Nombre_EmpleadoColumn.DefaultValue = ""
        Me.DsCapturaHora1.CapturaHora.TipoColumn.DefaultValue = ""
        Me.DsCapturaHora1.CapturaHora.Nombre_AdministradorColumn.DefaultValue = ""
        Me.DsCapturaHora1.CapturaHora.AdministradorColumn.DefaultValue = ""
        Me.DsCapturaHora1.CapturaHora.FechaColumn.DefaultValue = Date.Now
        Me.DsCapturaHora1.CapturaHora.HoraEntradaColumn.DefaultValue = Me.DateTimePicker1.Value
        'Me.DsCapturaHora1.CapturaHora.HoraEntradaAColumn.DefaultValue = Me.DateTimePicker1.Value
        Me.DsCapturaHora1.CapturaHora.HoraSalidaColumn.DefaultValue = Me.DateTimePicker1.Value
        'Me.DsCapturaHora1.CapturaHora.HoraSalidaAColumn.DefaultValue = Me.DateTimePicker1.Value

    End Sub

    Private Sub rbEntrada_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbEntrada.CheckedChanged
        If Me.rbEntrada.Checked Then
            Me.rbSalida.Checked = False
            Me.rbSalidaAlmorzar.Checked = False
            Me.rbEntradaAlmorzar.Checked = False
        End If
    End Sub

    Private Sub rbSalidaAlmorzar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSalidaAlmorzar.CheckedChanged
        If Me.rbSalidaAlmorzar.Checked Then
            Me.rbSalida.Checked = False
            Me.rbEntrada.Checked = False
            Me.rbEntradaAlmorzar.Checked = False
        End If
    End Sub

    Private Sub rbEntradaAlmorzar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbEntradaAlmorzar.CheckedChanged
        If Me.rbEntradaAlmorzar.Checked Then
            Me.rbSalida.Checked = False
            Me.rbEntrada.Checked = False
            Me.rbSalidaAlmorzar.Checked = False
        End If
    End Sub

    Private Sub rbSalida_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSalida.CheckedChanged
        If Me.rbSalida.Checked Then
            Me.rbEntradaAlmorzar.Checked = False
            Me.rbEntrada.Checked = False
            Me.rbSalidaAlmorzar.Checked = False
        End If
    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Nuevo()
                actualiza = False
            Case 2
                If PMU.Find Then Buscar() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3
                If PMU.Update Then Me.Guardar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4
                If PMU.Delete Then eliminar() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5
                If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 7 : Close()

        End Select
    End Sub

    Private Sub limpia()
        Me.rbEntradaAlmorzar.Checked = False
        Me.rbEntrada.Checked = False
        Me.rbSalidaAlmorzar.Checked = False
        Me.rbSalida.Checked = False
        Me.TextBox1.Text = ""
        Me.TextBox2.Text = ""
       
    End Sub

    Private Sub Nuevo()
        Try
            limpia()
            Me.DateTimePicker1.Visible = False
            If Me.ToolBarNuevo.Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
                Me.ToolBar1.Buttons(0).Text = "Cancelar"
                Me.ToolBar1.Buttons(0).ImageIndex = 8
                Me.DsCapturaHora1.CapturaHora.Clear()
                Me.ToolBarBuscar.Enabled = False
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarImprimir.Enabled = False
                Panel1.Enabled = True
                Me.TextBox1.Focus()

            Else
                'cambia la imagen a nuevo y habilita los botones del toolbar1
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").CancelCurrentEdit()
                Me.ToolBarNuevo.Text = "Nuevo"
                Me.ToolBarNuevo.ImageIndex = 0
                Me.ToolBar1.Buttons(1).Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarImprimir.Enabled = False
                Panel1.Enabled = False
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub Guardar()
        Dim Fecha As DateTime = Date.Now
        Me.DateTimePicker1.Value = Now
        Dim x As Integer
        Me.AdCapturaHora.Fill(Me.DsCapturaHora1.CapturaHora)

        If Me.TextBox2.Text <> "" And Me.TextBox1.Text <> "" Then


            Dim mensaje As String = ""
            Dim existe As Boolean = False
            cConexion.GetRecorset(conectadobd, "Select * from CapturaHora where Fecha='" & Fecha.Date & "' and Cedula= '" & Me.TextBox1.Text & "'", rs)
            While rs.Read
                existe = True
            End While
            rs.Close()

            If Me.rbEntrada.Checked Then
                If existe = True Then
                    MessageBox.Show("La Hora de Entrada de Hoy ya fue Ingresada!", "Atencion...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

                NumeroId()
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").CancelCurrentEdit()
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").EndCurrentEdit()
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").AddNew()

                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("id") = id
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("Cedula") = Me.TextBox1.Text
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("Nombre_Empleado") = Me.TextBox2.Text
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("Administrador") = cedula
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("Nombre_Administrador") = Me.txtUsuario.Text
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("Fecha") = Fecha.Date
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("HoraEntrada") = DateTimePicker1.Value
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("HoraSalida") = DateTimePicker1.Value
                'Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("HoraEntradaA") = DateTimePicker1.Value
                'Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").Current("HoraSalidaA") = DateTimePicker1.Value
                Me.BindingContext(Me.DsCapturaHora1, "CapturaHora").EndCurrentEdit()
                Registrar()
            End If

            If Me.rbSalida.Checked Then
                If existe = True Then
                    For x = 0 To Me.DsCapturaHora1.CapturaHora.Rows.Count - 1
                        If Me.DsCapturaHora1.CapturaHora.Rows(x).Item("Cedula") = Me.TextBox1.Text Then
                            Me.DsCapturaHora1.CapturaHora.Rows(x).Item("HoraSalida") = DateTimePicker1.Value
                        End If
                    Next
                    Registrar()
                Else
                    MessageBox.Show("No se puede registrar Salida  por que no ha Registrado la Entrada", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                End If
            End If
        Else
            MessageBox.Show("Debe llenar todos los campos", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End If



    End Sub

    Function Registrar() As Boolean

        If Me.SqlConnection1.State <> ConnectionState.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            Me.AdCapturaHora.InsertCommand.Transaction = Trans
            Me.AdCapturaHora.UpdateCommand.Transaction = Trans
            Me.AdCapturaHora.DeleteCommand.Transaction = Trans
            Me.AdCapturaHora.SelectCommand.Transaction = Trans
            AdCapturaHora.Update(Me.DsCapturaHora1.CapturaHora)
            Me.DsCapturaHora1.AcceptChanges()
            Trans.Commit()
            MessageBox.Show("El Registro ha sido ingresado correctamente", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Me.ToolBarNuevo.Text = "Nuevo"
            Me.ToolBarNuevo.ImageIndex = 0
            'nuevo
            Me.ToolBarNuevo.Enabled = True
            'buscar
            Me.ToolBarBuscar.Enabled = True
            'registrar
            Me.ToolBarRegistrar.Enabled = False
            'eliminar
            Me.ToolBarEliminar.Enabled = False
            'Imprimir
            Me.ToolBarImprimir.Enabled = True
            'cerrar
            Me.ToolBarCerrar.Enabled = True
            Me.DsCapturaHora1.CapturaHora.Clear()
            limpia()
            Panel1.Enabled = False
            Return True
        Catch ex As Exception
            Trans.Rollback()
            MessageBox.Show("El Registro no fue ingresado", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Private Sub Buscar()
        Try
            limpia()
            Dim Fbuscador As New buscador
            Fbuscador.TituloModulo.Text = "Restaurante"
            Fbuscador.bd = "Restaurante"
            Fbuscador.busca = "Nombre"
            Fbuscador.lblEncabezado.Text = "             Horas Capturadas"
            Fbuscador.consulta = "SELECT Id, Nombre_Empleado as Descripcion,Fecha FROM  CapturaHora"
            Fbuscador.tabla = "CapturaHora"
            Fbuscador.ToolBarImprimir.Enabled = False
            Fbuscador.cantidad = 3
            Fbuscador.ShowDialog()
            Me.Text = "Editando Horas Capturadas"
            'Fbuscador.GroupBox1.Text = "Editar Secci�n"
            'Fbuscador.GroupBox1.Visible = True
            Me.TextBox2.Text = Fbuscador.descripcion
            Me.DateTimePicker1.Text = Fbuscador.detalle
            Me.DateTimePicker1.Visible = True
            Codigo = Fbuscador.Icodigo
            Me.ToolBarRegistrar.Enabled = False
            If Me.TextBox2.Text <> "" Then
                Me.ToolBarEliminar.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub NumeroId()
        Dim Cx As New Conexion
        id = Cx.SlqExecuteScalar(Cx.Conectar("Restaurante"), "SELECT ISNULL(MAX(Id), 0) + 1  FROM CapturaHora  ")
        Cx.DesConectar(Cx.sQlconexion)
    End Sub

    Private Sub eliminar()
        Dim strdel As String = Me.TextBox2.Text
        Dim Cconexion As New Conexion
        Dim Resultado As String
        If TextBox2.Text <> "" Then
            If MessageBox.Show(" � Desea Eliminar este Registro ? ", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.No Then Exit Sub

            Resultado = Cconexion.SlqExecute(Cconexion.Conectar("Restaurante"), "Delete from CapturaHora where Id='" & Codigo & "'")
            If Resultado = vbNullString Then
                MessageBox.Show("El Registro fue eliminado", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.DsCapturaHora1.CapturaHora.Clear()
                Me.DateTimePicker1.Visible = False
                'nuevo
                Me.ToolBarNuevo.Enabled = True
                'buscar
                Me.ToolBarBuscar.Enabled = True
                'registrar
                Me.ToolBarRegistrar.Enabled = False
                'eliminar
                Me.ToolBarEliminar.Enabled = False
                'imprimir
                Me.ToolBarImprimir.Enabled = True
                'Cerrar
                Me.ToolBarCerrar.Enabled = True
                limpia()
            Else
                MessageBox.Show(Resultado)
                Exit Sub
            End If
        Else
            MessageBox.Show("No hay Registro a eliminar ", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub Imprimir()
        Try
            Dim Reporte As New Visor_Horas
            Reporte.Show()
            Me.DateTimePicker1.Visible = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
       
        Dim nombred, codigod As String
        Dim busqueda As New Buscar
        busqueda.sqlstring = "Select * from EmpleadosPlanilla"
        busqueda.campo = "Nombre"
        busqueda.tipo = False
        busqueda.ShowDialog()
        Try
            codigod = busqueda.codigo
            nombred = busqueda.descrip
            TextBox1.Text = codigod
            TextBox2.Text = nombred



            

        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Function ValidarUsuario1() As Boolean
        Dim sql As String
        Dim clsConexion As New Conexion
        Dim cnnConexion As New System.Data.SqlClient.SqlConnection
        Dim rsReader As SqlClient.SqlDataReader

        sql = "SELECT Nombre FROM USUARIOS WHERE Clave_interna= '" & Me.TextBox1.Text & "' " '"

        Me.TextBox3.Text = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox1.Text & "'")


        cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seguridad", "CONEXION")
        cnnConexion.Open()
        rsReader = clsConexion.GetRecorset(cnnConexion, sql)
        If rsReader.Read() = False Then Exit Function

        Me.TextBox2.Text = rsReader(0)
        cnnConexion.Close()
        ValidarUsuario1 = True

    End Function


    
End Class