<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Acompanamiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Acompanamiento))
        Me.PanelMesas = New System.Windows.Forms.Panel
        Me.lbUsuario = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.TextBox_Clave = New System.Windows.Forms.TextBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'PanelMesas
        '
        Me.PanelMesas.AccessibleDescription = Nothing
        Me.PanelMesas.AccessibleName = Nothing
        resources.ApplyResources(Me.PanelMesas, "PanelMesas")
        Me.PanelMesas.BackgroundImage = Nothing
        Me.PanelMesas.Font = Nothing
        Me.PanelMesas.Name = "PanelMesas"
        '
        'lbUsuario
        '
        Me.lbUsuario.AccessibleDescription = Nothing
        Me.lbUsuario.AccessibleName = Nothing
        resources.ApplyResources(Me.lbUsuario, "lbUsuario")
        Me.lbUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.lbUsuario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbUsuario.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbUsuario.Name = "lbUsuario"
        '
        'Label10
        '
        Me.Label10.AccessibleDescription = Nothing
        Me.Label10.AccessibleName = Nothing
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label10.Font = Nothing
        Me.Label10.Name = "Label10"
        '
        'TextBox_Clave
        '
        Me.TextBox_Clave.AccessibleDescription = Nothing
        Me.TextBox_Clave.AccessibleName = Nothing
        resources.ApplyResources(Me.TextBox_Clave, "TextBox_Clave")
        Me.TextBox_Clave.BackgroundImage = Nothing
        Me.TextBox_Clave.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox_Clave.Name = "TextBox_Clave"
        Me.TextBox_Clave.UseSystemPasswordChar = True
        '
        'Button4
        '
        Me.Button4.AccessibleDescription = Nothing
        Me.Button4.AccessibleName = Nothing
        resources.ApplyResources(Me.Button4, "Button4")
        Me.Button4.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button4.BackgroundImage = Nothing
        Me.Button4.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog1
        Me.Button4.Name = "Button4"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Acompanamiento
        '
        Me.AccessibleDescription = Nothing
        Me.AccessibleName = Nothing
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Nothing
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.TextBox_Clave)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.PanelMesas)
        Me.Controls.Add(Me.Label10)
        Me.Font = Nothing
        Me.Icon = Nothing
        Me.Name = "Acompanamiento"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelMesas As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Clave As System.Windows.Forms.TextBox
End Class
