Public Class AcompanamientoModif

#Region "variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim cedula As String
    Dim PMU As New PerfilModulo_Class
    Public Id_Categoria As Integer = 0
#End Region

#Region "Cerrar"
    Private Sub Acompanamiento_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Close()
    End Sub
#End Region

#Region "Load"
    Private Sub Acompanamiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        CargaAgregados()
        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
     
        If gloNoClave Or (Me.Id_Categoria > 0) Then
            Loggin_Usuario()
        Else
            TextBox_Clave.Focus()
        End If
      
        '---------------------------------------------------------------
    End Sub
#End Region

#Region "Cargar Acompañamientos"
    Private Sub CargaAgregados()
        Try
            Dim aButton As Button
            Dim i, ii, X, Y, n As Integer
            PanelMesas.Controls.Clear()
            For i = 1 To 7
                For ii = 1 To 4
                    aButton = New Button
                    aButton.Top = Y
                    aButton.Left = X
                    aButton.Width = 120
                    aButton.Height = 80
                    aButton.Name = n
                    'aButton.ShowToolTips = False
                    AddHandler aButton.Click, AddressOf Clickbuttons
                    PanelMesas.Controls.Add(aButton)
                    X = X + 122
                    n = n + 1
                Next
                Y = Y + 82
                X = 0
            Next
            UbicarPosiciones()
        Catch ex As Exception
            MessageBox.Show("Error al cargar el formulario: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Close()
        End Try
    End Sub

    Private Sub UbicarPosiciones()
        Dim er As Boolean = False
        Dim ruta As String

        cConexion.GetRecorset(conectadobd, "SELECT ACO_AGREGADO.a As Id, TipoAgregado,ACO_AGREGADO.IdAgregado, Acompañamientos_Menu.Nombre, ACO_AGREGADO.posicion, Acompañamientos_Menu.ImagenRuta " & _
                        " FROM Acompañamientos_Menu INNER JOIN" & _
                      " ACO_AGREGADO ON Acompañamientos_Menu.Id = ACO_AGREGADO.IdAgregado WHERE (ACO_AGREGADO.IdCategoriaMenu = " & Me.Id_Categoria & ")", rs)
        While rs.Read

            PanelMesas.Controls.Item(CInt(rs("posicion"))).Text = rs("Nombre") ' Nombre de la mesa
            PanelMesas.Controls.Item(CInt(rs("posicion"))).Name &= "-" & rs("Id") & "-" & rs("TipoAgregado") 'Codigo de la mesa

            ruta = rs("imagenRuta")
            If ruta.Trim.Length > 0 Then
                'crea la imagen solo si existe
                Try
                    Dim SourceImage As Bitmap
                    SourceImage = New Bitmap(ruta)
                    CType(PanelMesas.Controls.Item(CInt(rs("posicion"))), Button).Image = SourceImage

                Catch ex As Exception
                    er = True
                End Try
            End If
            CType(PanelMesas.Controls.Item(CInt(rs("posicion"))), Button).TextImageRelation = TextImageRelation.ImageAboveText

        End While
        rs.Close()
        cConexion.GetRecorset(conectadobd, "SELECT MOD_AGREGADO.a As Id, TipoAgregado , Modificadores_Forzados.Nombre, MOD_AGREGADO.posicion, Modificadores_Forzados.ImagenRuta, MOD_AGREGADO.IdAgregado, MOD_AGREGADO.TipoAgregado " & _
                            " FROM MOD_AGREGADO INNER JOIN " & _
                      " Modificadores_Forzados ON MOD_AGREGADO.IdAgregado = Modificadores_Forzados.ID WHERE (MOD_AGREGADO.IdCategoriaMenu = " & Me.Id_Categoria & ")", rs)
        While rs.Read

            PanelMesas.Controls.Item(CInt(rs("posicion"))).Text = rs("Nombre") ' Nombre del modificador
            PanelMesas.Controls.Item(CInt(rs("posicion"))).Name &= "-" & rs("id") & "-" & rs("TipoAgregado") 'Codigo del modificador

            ruta = rs("imagenRuta")
            If ruta.Trim.Length > 0 Then
                'crea la imagen solo si existe
                Try
                    Dim SourceImage As Bitmap
                    SourceImage = New Bitmap(ruta)
                    CType(PanelMesas.Controls.Item(CInt(rs("posicion"))), Button).Image = SourceImage
                Catch ex As Exception
                    er = True
                End Try
            End If
            CType(PanelMesas.Controls.Item(CInt(rs("posicion"))), Button).TextImageRelation = TextImageRelation.ImageAboveText
        End While
        rs.Close()

        If er = True Then
            MsgBox("Algunas imagenes fueron movidas de su ubicación original")
        End If
    End Sub
#End Region

#Region "Botones"
    Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("Select Tipo From conf", dt)
            Dim tipo As Integer = 0
            If dt.Rows.Count > 0 Then
                tipo = dt.Rows(0).Item(0)

            End If
            If tipo = 0 Then


                Dim g_prompt As New Prompt
                Dim datos As Array
                g_prompt.Text = "Acompañamientos"
                g_prompt.txtDato.Text = sender.text
                g_prompt.txtMEsas.Visible = True
                g_prompt.btnBuscar.Visible = True
                g_prompt.Label1.Visible = True
                g_prompt.btnBorrar.Visible = True
                g_prompt.btnBuscar.Image = sender.image
                g_prompt.rbArticulo.Visible = True
                g_prompt.rbReceta.Visible = True
                g_prompt.rbReceta.Checked = True
                g_prompt.Label2.Text = "NOMBRE"
                g_prompt.Label1.Text = "IMAGEN"
                g_prompt.Label3.Text = "RECETA"
                g_prompt.Label3.Visible = True
                g_prompt.txtRecetas.Visible = True
                g_prompt.GroupBox1.Visible = False
                g_prompt.cmbxBodegas.Visible = True
                g_prompt.Grupo_Bodega.Visible = True


                datos = CStr(sender.name).Split("-")

                If datos.Length > 1 Then
                    'Busca si es receta o un articulo
                    g_prompt.receta = cConexion.SlqExecuteScalar(conectadobd, "Select Tipo from Acompañamientos_Menu where id =" & datos(1))
                    g_prompt.txtId.Text = datos(2)
                    If g_prompt.receta = 1 Then
                        g_prompt.rbReceta.Checked = True
                        g_prompt.txtRecetas.Text = cConexion.SlqExecuteScalar(conectadobd, "SELECT nombre from recetas where id =" & datos(2))

                    Else
                        cConexion.DesConectar(conectadobd)
                        conectadobd = cConexion.Conectar("Proveeduria")
                        g_prompt.rbArticulo.Checked = True
                        g_prompt.txtRecetas.Text = cConexion.SlqExecuteScalar(conectadobd, "SELECT Descripcion from Inventario where Codigo =" & datos(2))
                        cConexion.DesConectar(conectadobd)
                        conectadobd = cConexion.Conectar("Restaurante")
                    End If
                    g_prompt.ID_BODEGA = cConexion.SlqExecuteScalar(conectadobd, "SELECT isnull(idBodegaDescarga,0) FROM Acompañamientos_Menu WHERE Nombre = '" & sender.text & "'")
                Else
                    g_prompt.ID_BODEGA = 7005
                End If
                g_prompt.ShowDialog()
                PMU = VSM(cedula, Name)
                'valida si tiene los privilegios para ingresar y actualizar datos

                If Trim(g_prompt.txtDato.Text) <> vbNullString And g_prompt.tipo = 1 Then
                    If PMU.Update Then
                        If datos.Length > 1 Then ' update
                            cConexion.UpdateRecords(conectadobd, "Acompañamientos_Menu", "Nombre='" & g_prompt.txtDato.Text & "', Id_receta = " & g_prompt.txtId.Text & ",ImagenRuta='" & g_prompt.ruta & "', Tipo=" & g_prompt.receta & ", Costo_Real = " & g_prompt.COSTO_PROMEDIO & ", idBodegaDescarga = " & g_prompt.ID_BODEGA, " id=" & datos(1))
                        Else 'insert
                            If (g_prompt.txtId.Text = Nothing) Then
                                g_prompt.txtId.Text = 0
                            End If
                            cConexion.AddNewRecord(conectadobd, "Acompañamientos_Menu", "Nombre,posicion,id_receta,ImagenRuta, Tipo, Costo_Real, idBodegaDescarga", "'" & g_prompt.txtDato.Text & "'," & datos(0) & "," & g_prompt.txtId.Text & ",'" & g_prompt.ruta & "'," & g_prompt.receta & "," & g_prompt.COSTO_PROMEDIO & "," & g_prompt.ID_BODEGA)
                        End If
                    Else : MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
                    End If
                ElseIf g_prompt.tipo = 3 Then
                    If PMU.Delete Then
                        If datos.Length > 1 Then
                            If MessageBox.Show("Realmente desea eliminar este acompañamiento", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                cConexion.DeleteRecords(conectadobd, "Acompañamientos_Menu", " id=" & datos(1))
                            End If
                        End If
                    Else : MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
                    End If
                End If
                g_prompt.Dispose()
                CargaAgregados()
            Else
                Dim g_prompt As New Prompt_Categoria
                g_prompt.PanelAgregado.Visible = True
                g_prompt.PanelCategoria.Visible = False
                Dim datos As Array
                g_prompt.Text = "Escoja el Acompañamiento o Modificador"
                g_prompt.txtDato.Text = sender.text
                g_prompt.txtMEsas.Visible = True
                g_prompt.btnBuscar.Visible = True
                'g_prompt.Label1.Visible = True
                g_prompt.btnBorrar.Visible = True
                g_prompt.btnBuscar.Image = sender.image
                'g_prompt.rbArticulo.Visible = True
                'g_prompt.rbReceta.Visible = True
                'g_prompt.rbReceta.Checked = True
                g_prompt.Label2.Text = "NOMBRE"
                g_prompt.txtRecetas.Visible = True
                g_prompt.GroupBox1.Visible = False
                g_prompt.cmbxBodegas.Visible = True
                g_prompt.Grupo_Bodega.Visible = True
                g_prompt.PanelAgregado.Visible = True
                g_prompt.PanelCategoria.Visible = True
                g_prompt.txtDato.ReadOnly = True

                datos = CStr(sender.name).Split("-")

                If datos.Length > 1 Then
                    If datos(2).Equals("ACO") Then
                        g_prompt.RadioButtonModificadores.Checked = False
                        g_prompt.RadioButtonAcompanamiento.Checked = True
                    Else
                        g_prompt.RadioButtonModificadores.Checked = True
                        g_prompt.RadioButtonAcompanamiento.Checked = False
                    End If
                    g_prompt.txtDato.Text = sender.text
                End If
                If Not sender.text.Equals("") Then
                    g_prompt.TextBoxPrecio.Text = cConexion.SlqExecuteScalar(conectadobd, "Select precio from Categoria_Agregado where a =" & datos(1))

                End If


                g_prompt.ShowDialog()
                PMU = VSM(cedula, Name)
                'valida si tiene los privilegios para ingresar y actualizar datos

                If Trim(g_prompt.txtDato.Text) <> vbNullString And g_prompt.tipo = 1 Then
                    If PMU.Update Then
                        If datos.Length > 1 Then ' update
                            If g_prompt.Id_Modfi > 0 Or g_prompt.Id_Acom > 0 Then
                                If g_prompt.Id_Modfi > 0 Then
                                    cConexion.UpdateRecords(conectadobd, "Categoria_Agregado", "IdAgregado=" & g_prompt.Id_Modfi & ", TipoAgregado = 'MOD', precio = " & g_prompt.TextBoxPrecio.Text, " a=" & datos(1))

                                Else
                                    cConexion.UpdateRecords(conectadobd, "Categoria_Agregado", "IdAgregado=" & g_prompt.Id_Acom & ", TipoAgregado = 'ACO', precio = " & g_prompt.TextBoxPrecio.Text, " a=" & datos(1))
                                End If
                            Else
                                cConexion.UpdateRecords(conectadobd, "Categoria_Agregado", " precio = " & g_prompt.TextBoxPrecio.Text, " a=" & datos(1))

                            End If

                        Else 'insert
                            If (g_prompt.txtId.Text = Nothing) Then
                                g_prompt.txtId.Text = 0
                            End If
                            If g_prompt.Id_Modfi > 0 Then
                                cConexion.AddNewRecord(conectadobd, "Categoria_Agregado", "posicion, x, y, IdAgregado, TipoAgregado, IdCategoriaMenu, precio", datos(0) & ",0,0," & g_prompt.Id_Modfi & ",'MOD'," & Me.Id_Categoria & "," & g_prompt.TextBoxPrecio.Text)
                                sender.text = g_prompt.txtDato.Text
                            ElseIf g_prompt.Id_Acom > 0 Then
                                cConexion.AddNewRecord(conectadobd, "Categoria_Agregado", "posicion, x, y, IdAgregado, TipoAgregado, IdCategoriaMenu, precio", datos(0) & ",0,0," & g_prompt.Id_Acom & ",'ACO'," & Me.Id_Categoria & "," & g_prompt.TextBoxPrecio.Text)
                                sender.text = g_prompt.txtDato.Text
                            End If

                        End If
                    Else : MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
                    End If
                ElseIf g_prompt.tipo = 3 Then
                    If PMU.Delete Then
                        If datos.Length > 1 Then
                            If MessageBox.Show("Realmente desea eliminar este agregado a la categoria", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                cConexion.DeleteRecords(conectadobd, "Categoria_Agregado", "a=" & datos(1))
                            End If
                        End If
                    Else : MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
                    End If
                End If
                g_prompt.Dispose()
                CargaAgregados()
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox_Clave.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & TextBox_Clave.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                TextBox_Clave.Text = ""
                PanelMesas.Enabled = True
                lbUsuario.Text = usuario
            Else
                Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                TextBox_Clave.Text = ""
                PanelMesas.Enabled = True
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

End Class