<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormLISTADO
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ListBoxEscoge = New System.Windows.Forms.ListBox
        Me.DataSetReceta1 = New SIALIBEB.DataSetReceta
        Me.ButtonAceptar = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.ButtonAgregar = New System.Windows.Forms.Button
        Me.ButtonEliminar = New System.Windows.Forms.Button
        CType(Me.DataSetReceta1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListBoxEscoge
        '
        Me.ListBoxEscoge.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListBoxEscoge.DataSource = Me.DataSetReceta1
        Me.ListBoxEscoge.DisplayMember = "DataTableLista.Nombre"
        Me.ListBoxEscoge.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBoxEscoge.FormattingEnabled = True
        Me.ListBoxEscoge.ItemHeight = 31
        Me.ListBoxEscoge.Location = New System.Drawing.Point(3, 3)
        Me.ListBoxEscoge.Name = "ListBoxEscoge"
        Me.ListBoxEscoge.Size = New System.Drawing.Size(486, 500)
        Me.ListBoxEscoge.TabIndex = 0
        '
        'DataSetReceta1
        '
        Me.DataSetReceta1.DataSetName = "DataSetReceta"
        Me.DataSetReceta1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonAceptar.Location = New System.Drawing.Point(3, 514)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ButtonAceptar.Size = New System.Drawing.Size(75, 55)
        Me.ButtonAceptar.TabIndex = 1
        Me.ButtonAceptar.Text = "OK"
        Me.ButtonAceptar.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(84, 514)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 55)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ButtonAgregar
        '
        Me.ButtonAgregar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonAgregar.Location = New System.Drawing.Point(414, 514)
        Me.ButtonAgregar.Name = "ButtonAgregar"
        Me.ButtonAgregar.Size = New System.Drawing.Size(75, 55)
        Me.ButtonAgregar.TabIndex = 3
        Me.ButtonAgregar.Text = "Agregar"
        Me.ButtonAgregar.UseVisualStyleBackColor = True
        '
        'ButtonEliminar
        '
        Me.ButtonEliminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonEliminar.Location = New System.Drawing.Point(333, 514)
        Me.ButtonEliminar.Name = "ButtonEliminar"
        Me.ButtonEliminar.Size = New System.Drawing.Size(75, 55)
        Me.ButtonEliminar.TabIndex = 4
        Me.ButtonEliminar.Text = "Quitar"
        Me.ButtonEliminar.UseVisualStyleBackColor = True
        '
        'FormLISTADO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(490, 581)
        Me.Controls.Add(Me.ButtonEliminar)
        Me.Controls.Add(Me.ButtonAgregar)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.ListBoxEscoge)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormLISTADO"
        Me.Text = "FormLISTADO"
        CType(Me.DataSetReceta1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ListBoxEscoge As System.Windows.Forms.ListBox
    Friend WithEvents ButtonAceptar As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ButtonAgregar As System.Windows.Forms.Button
    Friend WithEvents DataSetReceta1 As SIALIBEB.DataSetReceta
    Friend WithEvents ButtonEliminar As System.Windows.Forms.Button
End Class
