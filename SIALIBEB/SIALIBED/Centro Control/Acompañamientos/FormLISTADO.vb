Public Class FormLISTADO

    Public Id_categoria As Integer = 0
    Public tipo As Integer = 0
    Public Id_Select As Integer = 0
    Dim cedula As String
    Dim PMU As New PerfilModulo_Class

    Private Sub FormLISTADO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cargarListado()
    End Sub
    Sub cargarListado()

        If tipo = 1 Then
            cFunciones.Llenar_Tabla_Generico("Select ID, Nombre From Modificadores_Forzados ORDER BY Nombre", Me.DataSetReceta1.DataTableLista)

        Else
            cFunciones.Llenar_Tabla_Generico("Select Id, Nombre From Acompañamientos_Menu ORDER BY Nombre ", Me.DataSetReceta1.DataTableLista)

        End If

    End Sub
    Private Sub ButtonAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        If Me.ListBoxEscoge.Items.Count <> 0 Then
            Me.Id_Select = Me.DataSetReceta1.DataTableLista(Me.ListBoxEscoge.SelectedIndex).Id

        End If
        DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        DialogResult = Windows.Forms.DialogResult.Cancel

    End Sub

    Private Sub ButtonAgregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAgregar.Click
        If tipo = 2 Then

            Dim cConexion As New ConexionR
            Dim conectadobd As SqlClient.SqlConnection
            cConexion.Conectar("Restaurante")
            conectadobd = cConexion.SqlConexion

            Dim g_prompt As New Prompt

            g_prompt.Text = "Acompañamientos"
            g_prompt.txtDato.Text = sender.text
            g_prompt.txtMEsas.Visible = True
            g_prompt.btnBuscar.Visible = True
            g_prompt.Label1.Visible = True
            g_prompt.btnBorrar.Visible = True
            'g_prompt.btnBuscar.Image = sender.image
            g_prompt.rbArticulo.Visible = True
            g_prompt.rbReceta.Visible = True
            g_prompt.rbReceta.Checked = True
            g_prompt.Label2.Text = "NOMBRE"
            g_prompt.Label1.Text = "IMAGEN"
            g_prompt.Label3.Text = "RECETA"
            g_prompt.Label3.Visible = True
            g_prompt.txtRecetas.Visible = True
            g_prompt.GroupBox1.Visible = False
            g_prompt.cmbxBodegas.Visible = True
            g_prompt.Grupo_Bodega.Visible = True

            g_prompt.ShowDialog()
            'valida si tiene los privilegios para ingresar y actualizar datos
            Dim pos As Integer = 0
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT MAX(posicion)+1 AS UltimaPos FROM Acompañamientos_Menu", dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            Try
                If dt.Rows.Count > 0 Then

                    pos = CInt(dt.Rows(0).Item(0))

                End If
            Catch ex As Exception
                pos = 0
            End Try
          
            If Trim(g_prompt.txtDato.Text) <> vbNullString And g_prompt.tipo = 1 Then
                cConexion.AddNewRecord(conectadobd, "Acompañamientos_Menu", "Nombre,posicion,id_receta,ImagenRuta, Tipo, Costo_Real, idBodegaDescarga", "'" & g_prompt.txtDato.Text & "'," & pos & "," & g_prompt.txtId.Text & ",'" & g_prompt.ruta & "'," & g_prompt.receta & "," & g_prompt.COSTO_PROMEDIO & "," & g_prompt.ID_BODEGA)
            End If
            g_prompt.Dispose()
            cargarListado()
        Else

            Dim cConexion As New ConexionR
            Dim conectadobd As SqlClient.SqlConnection
            cConexion.Conectar("Restaurante")
            conectadobd = cConexion.SqlConexion

            Dim ruta As String = ""
            Dim g_prompt As New Prompt
            Dim datos As Array
            g_prompt.Text = "Modificador"
            g_prompt.txtDato.Text = sender.text
            g_prompt.btnBuscar.Visible = True
            g_prompt.btnBorrar.Visible = True
            g_prompt.GroupBox1.Visible = False


            g_prompt.btnBuscar.Image = sender.image
            g_prompt.ruta = ruta
            g_prompt.Label2.Text = "NOMBRE"
            datos = CStr(sender.name).Split("-")

            g_prompt.ShowDialog()


            ruta = g_prompt.ruta
            PMU = VSM(cedula, Me.Name)

            Dim pos As Integer = 0
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT MAX(posicion)+1 AS UltimaPos FROM Modificadores_forzados", dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            Try

                If dt.Rows.Count > 0 Then
                    pos = dt.Rows(0).Item(0)
                End If
            Catch ex As Exception
                pos = 0
            End Try
            If Trim(g_prompt.txtDato.Text) <> vbNullString And g_prompt.tipo = 1 Then
                cConexion.AddNewRecord(conectadobd, "Modificadores_forzados", "Nombre,posicion,ImagenRuta", "'" & g_prompt.txtDato.Text & "'," & pos & ",'" & ruta & "'")
            End If
            g_prompt.Dispose()
            cargarListado()

        End If
    End Sub

    Private Sub ButtonEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEliminar.Click
        If tipo = 2 Then
            If Me.ListBoxEscoge.Items.Count <> 0 Then
                Dim cConexion As New ConexionR
                Dim conectadobd As SqlClient.SqlConnection
                cConexion.Conectar("Restaurante")
                conectadobd = cConexion.SqlConexion


                If MessageBox.Show("Realmente desea eliminar este acompañamiento", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    cConexion.DeleteRecords(conectadobd, "Acompañamientos_Menu", " id=" & Me.DataSetReceta1.DataTableLista(Me.ListBoxEscoge.SelectedIndex).Id)
                End If
            End If
            cargarListado()
        Else
            If Me.ListBoxEscoge.Items.Count <> 0 Then
                Dim cConexion As New ConexionR
                Dim conectadobd As SqlClient.SqlConnection
                cConexion.Conectar("Restaurante")
                conectadobd = cConexion.SqlConexion


                If MessageBox.Show("Realmente desea eliminar este modificador", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    cConexion.DeleteRecords(conectadobd, "Modificadores_forzados", " id=" & Me.DataSetReceta1.DataTableLista(Me.ListBoxEscoge.SelectedIndex).Id)
                End If

            End If

            cargarListado()

        End If
    End Sub
End Class