﻿Imports Utilidades
Public Class frmBuscaCabys
    Dim dtCabys As New DataTable
    Dim MyIdCabys As Integer = 0
    Sub BuscarCodCabys(ByVal Descripcion As String)
        Try

            cFunciones.Llenar_Tabla_Generico("select Id, codigo,Descripcion,porcentajeIVA from Cabys where Descripcion like '%" & Descripcion & "%'",
                                         dtCabys, GetSetting("seeSoft", "Hotel", "Conexion"))
            dgvBusqueda.DataSource = dtCabys
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        ObtenerCodigoCabys()
    End Sub

    Sub ObtenerCodigoCabys()
        Try
            Dim frm As Prompt_Categoria = CType(Owner, Prompt_Categoria)
            frm.cargarCabys(MyIdCabys)
            Me.Close()
        Catch ex As Exception
            MsgBox("Debe elegir un Cabys.", MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub dgvBusqueda_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBusqueda.CellClick
        Try
            If dgvBusqueda.Rows.Count > 0 Then
                lbIdCabys.Text = dgvBusqueda.CurrentRow.Cells.Item(1).Value
                MyIdCabys = dgvBusqueda.CurrentRow.Cells.Item(0).Value
            Else
                lbIdCabys.Text = 0
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub txtBuscarDato_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarDato.TextChanged
        Try
            dtCabys.Clear()
            If txtBuscarDato.Text.Length > 2 Then
                BuscarCodCabys(txtBuscarDato.Text)
            ElseIf txtBuscarDato.Text = "".ToString Then
                dgvBusqueda.DataSource = dtCabys
            ElseIf txtBuscarDato.Text = "0".ToString Then
                dgvBusqueda.DataSource = dtCabys
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub frmBuscaCabys_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtBuscarDato.Select()
    End Sub
End Class