Imports LibPrintTicket


Public Class ClassImpresionTipoFac

#Region "      PARAMETROS"
    Public preF As Boolean = False
    Public nFac As Integer = 0
    Public cuenta As Integer = 0
    Public nombrePuntoVenta As String = ""
    Public mesa As String = ""
    Public cargoHabitacion As Boolean = True

    Public comenzales As String = ""
    Public nSalonero As String = ""
    Public nUsuario As String = ""

    Public Subtotal As Double = 0
    Public descuento As Double = 0
    Public total As Double = 0
    Public impV As Double = 0
    Public exTip As Double = 0
    Public impS As Double = 0
    Public impresora As String = ""

    Public etiquetaTotal2 As String = ""
    Public Simbolo2 As String = "c"
    Public totalMoneda2 As Double = 0

    Public etiquetaTotal3 As String = ""
    Public Simbolo3 As String = "e"
    Public totalMoneda3 As Double = 0
    Public Porcdescuento As String = ""
    Public simbolo As String = "$"
#End Region

    Dim tF As New FacturaRapida
    Private enc As New DataTable 'ENCABEZADO
    Private dt As New DataTable 'DETALLES
    Public Express As Integer = 0

    Sub cargaDatos()
        cargaEncabezado()
        If preF Then
            If Express <> 1 Then


                If cuenta = 0 Then
                    cFunciones.Llenar_Tabla_Generico("SELECT cProducto, cDescripcion, cPrecio, separada, cMesa, Comenzales, Habitacion, Nombre_Mesa, cCantidad, PrecioUnitario, SubTotal, " &
                                    " ImpuestoVenta, ImpuestoServicio, hora, Comanda, descuento, nombre " &
                                    " FROM [Pre-Facturas] " &
                                    " WHERE (Comanda = " & nFac & ") AND (separada = " & cuenta & " OR separada = 1)", dt, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
                Else

                    cFunciones.Llenar_Tabla_Generico("SELECT cProducto, cDescripcion, cPrecio, separada, cMesa, Comenzales, Habitacion, Nombre_Mesa, cCantidad, PrecioUnitario, SubTotal, " &
                                   " ImpuestoVenta, ImpuestoServicio, hora, Comanda, descuento, nombre " &
                                   " FROM [Pre-Facturas] " &
                                   " WHERE (Comanda = " & nFac & ") AND (separada = " & cuenta & ")", dt, GetSetting("SeeSOFT", "Restaurante", "Conexion"))

                End If
            Else
                cFunciones.Llenar_Tabla_Generico("SELECT cProducto, cDescripcion, cPrecio, separada, cMesa, Comenzales, Habitacion, Nombre_Mesa, cCantidad, PrecioUnitario, SubTotal, " &
                                   " ImpuestoVenta, ImpuestoServicio, hora, Comanda, descuento, nombre " &
                                   " FROM [Pre-FacturasExpress] " &
                                   " WHERE (Comanda = " & nFac & ") AND (separada = " & cuenta & ")", dt, GetSetting("SeeSOFT", "Restaurante", "Conexion"))

            End If
            If dt.Rows.Count > 0 Then
                Me.impresionPrefactura()
            End If

        Else

        End If
    End Sub
    Sub cargaEncabezado()
        cFunciones.Llenar_Tabla_Generico("Select * from configuraciones", enc, GetSetting("SeeSOFT", "Hotel", "Conexion"))
        If enc.Rows.Count > 0 Then
            If preF Then
                tF.AnadirLineaCabeza(enc.Rows(0).Item("PersonaJuridica"))
                tF.AnadirLineaCabeza(enc.Rows(0).Item("Cedula"))
                tF.AnadirLineaCabeza(enc.Rows(0).Item("Direccion"))
                tF.AnadirLineaCabeza("TEL: " & enc.Rows(0).Item("Tel_01"))
            End If
        End If
    End Sub

    'MWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWM
    'MWMWMWMWMMWM   IMPRESION PREFACTURA..   MMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWM
    'MWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWMMWMWMWMWM

    Sub impresionPrefactura()

        'Declaramos una nueva instancia de la clase que imprime ticketes..
        Dim t As New LibPrintTicket.Ticket

        'cargamos las configuraciones de la empresa actual..
        Dim inf As New cls_Configuraciones()
        inf.obtenerConfiguraciones()

        'Preguntamos cual es la moneda del Restaurante..
        'y configuramos para que imprima bien el currency..
        If inf.MonedaRestaurante.Equals("1") Then
            Me.simbolo = "c"
            Me.Simbolo2 = "$"
        Else
            Me.simbolo = "$"
            Me.Simbolo2 = "c"
        End If

        'Agregamos el encabezado  ------
        t.AddSuperHeaderLine("          " & mesa)
        t.AddSuperHeaderLine(inf.PersonaJuridica)
        t.AddHeaderLine(inf.Cedula)
        t.AddHeaderLine("Comanda: " & Me.nFac & " | " & "Cuenta: " & Me.cuenta)
        t.AddHeaderLine(Format(Now, "dd/MM/yy  ") & Format(dt.Rows(0).Item("hora"), "hh:mm:ss tt"))
        't.AddHeaderLine(" Persona(s): " & dt.Rows(0).Item("Comenzales"))
        't.AddHeaderLine("Salonero: " & nSalonero)

        'Con esto agregamos el nombre del salonero en caso que el mismo exista como un Item del menu.
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i).Item("cDescripcion").ToString().StartsWith("@") Then
                t.AddHeaderLine("Salonero: " & dt.Rows(i).Item("cDescripcion").ToString())
            End If
        Next



        For i As Integer = 0 To dt.Rows.Count - 1
            If Not dt.Rows(i).Item("cDescripcion").ToString().StartsWith("@") Then

                ' Porcdescuento = dt.Rows(i).Item("descuento").ToString()
                'Dim cantidad As Integer = dt.Rows(i).Item("cCantidad")
                'MsgBox(cantidad.ToString)
                If Math.Abs(Convert.ToInt32(dt.Rows(i).Item("cCantidad"))) = 1 Then
                    t.AddItem("1", dt.Rows(i).Item("cDescripcion"), Format(dt.Rows(i).Item("SubTotal"), "#,##0.00"))
                ElseIf dt.Rows(i).Item("cCantidad") > 0 Then
                    t.AddItem("-", dt.Rows(i).Item("cDescripcion"), "")
                    t.AddItem(dt.Rows(i).Item("cCantidad"), " x " & Format(dt.Rows(i).Item("PrecioUnitario"), "#,##0.00"), Format(dt.Rows(i).Item("SubTotal"), "#,##0.00"))
                End If
            End If
        Next

        t.AddTotal("SubTotal: ", Format(Me.Subtotal, simbolo & "#,##0.00"))

        If Me.descuento > 0 Then
            t.AddTotal("Desc(" & Porcdescuento & "%): ", Format(Me.descuento, simbolo & "#,##0.00"))
        End If
        t.AddTotal("Imp.Vent: ", Format(Me.impV, simbolo & "#,##0.00"))
        t.AddTotal("Imp.Serv: ", Format(Me.impS, simbolo & "#,##0.00"))
        't.AddTotal("Descuento: ", Format(Me.impS, simbolo & "#,##0.00"))
        t.AddSuperTotal("TOTAL:", Format(Me.total, "#,##0.00"))

        If exTip > 0 Then
            t.AddTotal("Extra TIP: ", "_______________")
        End If

        If Me.totalMoneda2 > 0 Then
            t.AddSuperTotalNotes(etiquetaTotal2, Format(totalMoneda2, "#,##0.00"))
        End If
        If Me.totalMoneda3 > 0 Then
            t.AddSuperTotalNotes(etiquetaTotal3, Format(totalMoneda3, "#,##0.00"))
        End If

        If cargoHabitacion Then
            t.AddFooterLine(" ")
            t.AddFooterLine("___________________________")
            t.AddFooterLine("    Recibido Conforme")
        End If
        If tF.ImpresoraExistente(Me.impresora) Then
            t.PrintTicket(impresora)
        Else
            MsgBox("Impresosa incorrecta")
        End If

        'tF.AnadirLineaSubcabeza(nombrePuntoVenta)
        'tF.AnadirLineaSubcabeza("Comanda No: " & Me.nFac)

        'If Me.cuenta > 0 Then
        'tF.AnadirLineaSubcabeza("Cuenta: " & Me.cuenta)
        'End If

        'tF.AnadirLineaSubcabeza(Format(Now, "dd/MM/yy"))
        'tF.AnadirLineaSubcabeza("Persona(s): " & dt.Rows(0).Item("Comenzales"))
        'tF.AnadirLineaSubcabeza("Mesa: " & mesa)
        'tF.AnadirLineaSubcabeza("Hora: " & Format(dt.Rows(0).Item("hora"), "hh:mm:ss tt"))
        'tF.AnadirLineaSubcabeza("Salonero: " & nSalonero)

        'If Me.cargoHabitacion Then

        'If dt.Rows(0).Item("Nombre") = "" Then
        'tF.AnadirLineaSubcabeza(" ")
        'tF.AnadirLineaSubcabeza("Customer:  " & "_____________________")
        'Else
        'tF.AnadirLineaSubcabeza(" ")
        'tF.AnadirLineaSubcabeza("Customer: " & dt.Rows(0).Item("Nombre"))

        'End If
        'tF.AnadirLineaSubcabeza(" ")
        'tF.AnadirLineaSubcabeza("Room:      _____________________")

        'End If

        'tF.EncabezadoElementos = "C DESCRIPCION P/UNIT      SUBT."
        'For i As Integer = 0 To dt.Rows.Count - 1
        'If dt.Rows(i).Item("cCantidad") = 1 Then
        'tF.AnadirElemento("1", dt.Rows(i).Item("cDescripcion"), Format(dt.Rows(i).Item("SubTotal"), "#,##0.00"))
        'ElseIf dt.Rows(i).Item("cCantidad") > 0 Then
        'tF.AnadirElemento("-", dt.Rows(i).Item("cDescripcion"), "")
        'tF.AnadirElemento(dt.Rows(i).Item("cCantidad"), " x " & Format(dt.Rows(i).Item("PrecioUnitario"), "#,##0.00"), Format(dt.Rows(i).Item("SubTotal"), "#,##0.00"))

        'End If
        'Next



        'tF.AnadirTotal("SubTotal: ", Format(Me.Subtotal, simbolo & "#,##0.00"))

        'If Me.descuento > 0 Then
        'tF.AnadirTotal("Descuento: ", Format(Me.descuento, simbolo & "#,##0.00"))
        'End If
        'tF.AnadirTotal("Impuesto Ventas: ", Format(Me.impV, simbolo & "#,##0.00"))
        'tF.AnadirTotal("Impuesto Servicios: ", Format(Me.impS, simbolo & "#,##0.00"))
        'tF.AnadirTotal("TOTAL: ", Format(Me.total, simbolo & "#,##0.00"))
        'If exTip > 0 Then
        ' tF.AnadirTotal("Extra TIP: ", "_______________")
        'End If
        'If Me.totalMoneda2 > 0 Then
        'tF.AnadirTotal(etiquetaTotal2, Format(totalMoneda2, Simbolo2 & "#,##0.00"))
        'End If
        'If Me.totalMoneda3 > 0 Then
        'tF.AnadirTotal(etiquetaTotal3, Format(totalMoneda3, Simbolo3 & "#,##0.00"))
        'End If


        'If cargoHabitacion Then
        'tF.AnadeLineaAlPie(" ")
        'tF.AnadeLineaAlPie("___________________________")
        'tF.AnadeLineaAlPie("    Recibido Conforme")
        'End If
        'If tF.ImpresoraExistente(Me.impresora) Then
        'tF.ImprimeTicket(impresora)
        'Else
        'MsgBox("Impresosa incorrecta")

        'End If
    End Sub

    Sub impresionFactura()

    End Sub
End Class
