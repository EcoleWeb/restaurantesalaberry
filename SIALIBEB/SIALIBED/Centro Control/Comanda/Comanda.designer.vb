<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Comanda
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Comanda))
        Me.PanelMesas = New System.Windows.Forms.Panel()
        Me.lbestado = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolButtonBuscarFactura = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolButtonCambiarMesa = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripUnirMesa = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSepararMesa = New System.Windows.Forms.ToolStripButton()
        Me.botonVerComandas = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btParallevar = New System.Windows.Forms.ToolStripButton()
        Me.btExpress = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolButtonTerminar = New System.Windows.Forms.ToolStripButton()
        Me.PanelSecciones = New System.Windows.Forms.Panel()
        Me.PanelGrupos = New System.Windows.Forms.Panel()
        Me.LabelEstado = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtLibre = New System.Windows.Forms.TextBox()
        Me.txtOcupada = New System.Windows.Forms.TextBox()
        Me.txtSeccion = New System.Windows.Forms.TextBox()
        Me.txtFactura = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.LabelTiempo = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PanelMesas.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelMesas
        '
        Me.PanelMesas.BackColor = System.Drawing.Color.White
        Me.PanelMesas.Controls.Add(Me.lbestado)
        Me.PanelMesas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelMesas.Enabled = False
        Me.PanelMesas.Location = New System.Drawing.Point(0, 0)
        Me.PanelMesas.Name = "PanelMesas"
        Me.PanelMesas.Size = New System.Drawing.Size(559, 594)
        Me.PanelMesas.TabIndex = 1
        '
        'lbestado
        '
        Me.lbestado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbestado.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lbestado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbestado.ForeColor = System.Drawing.Color.Red
        Me.lbestado.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbestado.Location = New System.Drawing.Point(0, 575)
        Me.lbestado.Name = "lbestado"
        Me.lbestado.Size = New System.Drawing.Size(555, 15)
        Me.lbestado.TabIndex = 34
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.Transparent
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(50, 50)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolButtonBuscarFactura, Me.ToolStripButton2, Me.ToolButtonCambiarMesa, Me.ToolStripButton1, Me.ToolStripUnirMesa, Me.ToolStripSepararMesa, Me.botonVerComandas, Me.ToolStripSeparator1, Me.btParallevar, Me.btExpress, Me.ToolStripSeparator2, Me.ToolButtonTerminar})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 629)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(836, 77)
        Me.ToolStrip1.TabIndex = 43
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolButtonBuscarFactura
        '
        Me.ToolButtonBuscarFactura.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.ToolButtonBuscarFactura.ForeColor = System.Drawing.Color.White
        Me.ToolButtonBuscarFactura.Image = Global.SIALIBEB.My.Resources.Resources.FINDFILE16
        Me.ToolButtonBuscarFactura.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolButtonBuscarFactura.Name = "ToolButtonBuscarFactura"
        Me.ToolButtonBuscarFactura.Size = New System.Drawing.Size(112, 74)
        Me.ToolButtonBuscarFactura.Text = "Buscar Factura"
        Me.ToolButtonBuscarFactura.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.ToolButtonBuscarFactura.ToolTipText = "Buscar Facturas"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.ToolStripButton2.ForeColor = System.Drawing.Color.White
        Me.ToolStripButton2.Image = Global.SIALIBEB.My.Resources.Resources.FINDFILE16
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(111, 74)
        Me.ToolStripButton2.Text = "Buscar Artículo"
        Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.ToolStripButton2.ToolTipText = "Buscar Facturas"
        '
        'ToolButtonCambiarMesa
        '
        Me.ToolButtonCambiarMesa.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.ToolButtonCambiarMesa.ForeColor = System.Drawing.Color.White
        Me.ToolButtonCambiarMesa.Image = Global.SIALIBEB.My.Resources.Resources.Cambio_de_Mesas_Logo_V3
        Me.ToolButtonCambiarMesa.ImageTransparentColor = System.Drawing.Color.White
        Me.ToolButtonCambiarMesa.Name = "ToolButtonCambiarMesa"
        Me.ToolButtonCambiarMesa.Size = New System.Drawing.Size(128, 74)
        Me.ToolButtonCambiarMesa.Text = "Cambiar de Mesa"
        Me.ToolButtonCambiarMesa.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.ToolButtonCambiarMesa.ToolTipText = "Cambiar de Mesa al Cliente"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.ToolStripButton1.ForeColor = System.Drawing.Color.White
        Me.ToolStripButton1.Image = Global.SIALIBEB.My.Resources.Resources.Separar_cuentas__Logo_V1
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(64, 74)
        Me.ToolStripButton1.Text = "Separar"
        Me.ToolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.ToolStripButton1.Visible = False
        '
        'ToolStripUnirMesa
        '
        Me.ToolStripUnirMesa.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.ToolStripUnirMesa.ForeColor = System.Drawing.Color.White
        Me.ToolStripUnirMesa.Image = Global.SIALIBEB.My.Resources.Resources.Unir_Mesa
        Me.ToolStripUnirMesa.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolStripUnirMesa.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripUnirMesa.Name = "ToolStripUnirMesa"
        Me.ToolStripUnirMesa.Size = New System.Drawing.Size(79, 74)
        Me.ToolStripUnirMesa.Text = "Unir Mesa"
        Me.ToolStripUnirMesa.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ToolStripUnirMesa.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'ToolStripSepararMesa
        '
        Me.ToolStripSepararMesa.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.ToolStripSepararMesa.ForeColor = System.Drawing.Color.White
        Me.ToolStripSepararMesa.Image = Global.SIALIBEB.My.Resources.Resources.Separar_Mesa
        Me.ToolStripSepararMesa.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolStripSepararMesa.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSepararMesa.Name = "ToolStripSepararMesa"
        Me.ToolStripSepararMesa.Size = New System.Drawing.Size(102, 74)
        Me.ToolStripSepararMesa.Text = "Separar Mesa"
        Me.ToolStripSepararMesa.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ToolStripSepararMesa.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'botonVerComandas
        '
        Me.botonVerComandas.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.botonVerComandas.ForeColor = System.Drawing.Color.White
        Me.botonVerComandas.Image = CType(resources.GetObject("botonVerComandas.Image"), System.Drawing.Image)
        Me.botonVerComandas.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.botonVerComandas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.botonVerComandas.Name = "botonVerComandas"
        Me.botonVerComandas.Size = New System.Drawing.Size(108, 74)
        Me.botonVerComandas.Text = "Ver Comandas"
        Me.botonVerComandas.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.botonVerComandas.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 77)
        '
        'btParallevar
        '
        Me.btParallevar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btParallevar.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.btParallevar.ForeColor = System.Drawing.Color.White
        Me.btParallevar.Image = Global.SIALIBEB.My.Resources.Resources.Para_llevar
        Me.btParallevar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btParallevar.Name = "btParallevar"
        Me.btParallevar.Size = New System.Drawing.Size(82, 74)
        Me.btParallevar.Text = "Para llevar"
        Me.btParallevar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.btParallevar.ToolTipText = "Para llevar"
        '
        'btExpress
        '
        Me.btExpress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btExpress.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.btExpress.ForeColor = System.Drawing.Color.White
        Me.btExpress.Image = Global.SIALIBEB.My.Resources.Resources.Express_Inicio_Logo
        Me.btExpress.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btExpress.Name = "btExpress"
        Me.btExpress.Size = New System.Drawing.Size(63, 74)
        Me.btExpress.Text = "Express"
        Me.btExpress.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.btExpress.ToolTipText = "Cerrar el módulo"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 77)
        '
        'ToolButtonTerminar
        '
        Me.ToolButtonTerminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolButtonTerminar.Font = New System.Drawing.Font("Trebuchet MS", 11.0!)
        Me.ToolButtonTerminar.ForeColor = System.Drawing.Color.White
        Me.ToolButtonTerminar.Image = Global.SIALIBEB.My.Resources.Resources.Salir_Inicio_Logo1
        Me.ToolButtonTerminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolButtonTerminar.Name = "ToolButtonTerminar"
        Me.ToolButtonTerminar.Size = New System.Drawing.Size(54, 74)
        Me.ToolButtonTerminar.Text = "Salir"
        Me.ToolButtonTerminar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.ToolButtonTerminar.ToolTipText = "Cerrar el módulo"
        '
        'PanelSecciones
        '
        Me.PanelSecciones.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelSecciones.AutoScroll = True
        Me.PanelSecciones.BackColor = System.Drawing.Color.White
        Me.PanelSecciones.Font = New System.Drawing.Font("Trebuchet MS", 8.25!)
        Me.PanelSecciones.Location = New System.Drawing.Point(1, 362)
        Me.PanelSecciones.Name = "PanelSecciones"
        Me.PanelSecciones.Size = New System.Drawing.Size(214, 145)
        Me.PanelSecciones.TabIndex = 2
        '
        'PanelGrupos
        '
        Me.PanelGrupos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelGrupos.AutoScroll = True
        Me.PanelGrupos.BackColor = System.Drawing.Color.White
        Me.PanelGrupos.Font = New System.Drawing.Font("Trebuchet MS", 8.25!)
        Me.PanelGrupos.Location = New System.Drawing.Point(2, 29)
        Me.PanelGrupos.Name = "PanelGrupos"
        Me.PanelGrupos.Size = New System.Drawing.Size(213, 307)
        Me.PanelGrupos.TabIndex = 3
        '
        'LabelEstado
        '
        Me.LabelEstado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelEstado.BackColor = System.Drawing.Color.LightSlateGray
        Me.LabelEstado.Font = New System.Drawing.Font("Trebuchet MS", 14.25!)
        Me.LabelEstado.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.LabelEstado.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.LabelEstado.Location = New System.Drawing.Point(-226, -1)
        Me.LabelEstado.Name = "LabelEstado"
        Me.LabelEstado.Size = New System.Drawing.Size(26, 26)
        Me.LabelEstado.TabIndex = 44
        Me.LabelEstado.Text = "0"
        Me.LabelEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LabelEstado.Visible = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.DimGray
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(1, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(189, 26)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Grupo de Mesas"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.DimGray
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(2, 339)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(194, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Secciones de Restaurante"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.DimGray
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(4, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(452, 25)
        Me.Label3.TabIndex = 6
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtLibre
        '
        Me.txtLibre.BackColor = System.Drawing.Color.Beige
        Me.txtLibre.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtLibre.CausesValidation = False
        Me.txtLibre.Location = New System.Drawing.Point(14, 19)
        Me.txtLibre.Name = "txtLibre"
        Me.txtLibre.ReadOnly = True
        Me.txtLibre.Size = New System.Drawing.Size(14, 13)
        Me.txtLibre.TabIndex = 35
        Me.txtLibre.TabStop = False
        '
        'txtOcupada
        '
        Me.txtOcupada.BackColor = System.Drawing.Color.PaleGreen
        Me.txtOcupada.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtOcupada.CausesValidation = False
        Me.txtOcupada.Location = New System.Drawing.Point(14, 31)
        Me.txtOcupada.Name = "txtOcupada"
        Me.txtOcupada.ReadOnly = True
        Me.txtOcupada.Size = New System.Drawing.Size(14, 13)
        Me.txtOcupada.TabIndex = 36
        Me.txtOcupada.TabStop = False
        '
        'txtSeccion
        '
        Me.txtSeccion.BackColor = System.Drawing.Color.LightGray
        Me.txtSeccion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSeccion.CausesValidation = False
        Me.txtSeccion.Location = New System.Drawing.Point(14, 46)
        Me.txtSeccion.Name = "txtSeccion"
        Me.txtSeccion.ReadOnly = True
        Me.txtSeccion.Size = New System.Drawing.Size(14, 13)
        Me.txtSeccion.TabIndex = 37
        Me.txtSeccion.TabStop = False
        '
        'txtFactura
        '
        Me.txtFactura.BackColor = System.Drawing.Color.Red
        Me.txtFactura.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFactura.CausesValidation = False
        Me.txtFactura.Location = New System.Drawing.Point(106, 38)
        Me.txtFactura.Name = "txtFactura"
        Me.txtFactura.ReadOnly = True
        Me.txtFactura.Size = New System.Drawing.Size(14, 13)
        Me.txtFactura.TabIndex = 38
        Me.txtFactura.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(34, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Libre"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(34, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 13)
        Me.Label5.TabIndex = 40
        Me.Label5.Text = "Ocupada"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(34, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 41
        Me.Label6.Text = "Sección"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(125, 35)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 13)
        Me.Label7.TabIndex = 42
        Me.Label7.Text = "Facturando"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(225, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtLibre)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtOcupada)
        Me.GroupBox1.Controls.Add(Me.txtSeccion)
        Me.GroupBox1.Controls.Add(Me.txtFactura)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GroupBox1.ForeColor = System.Drawing.Color.CadetBlue
        Me.GroupBox1.Location = New System.Drawing.Point(-6, 509)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(221, 81)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = " Simbología de Mesas"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(125, 58)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(98, 13)
        Me.Label10.TabIndex = 49
        Me.Label10.Text = "Facturando Cuenta"
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.CausesValidation = False
        Me.TextBox3.Location = New System.Drawing.Point(106, 61)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(14, 13)
        Me.TextBox3.TabIndex = 48
        Me.TextBox3.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(126, 19)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(42, 13)
        Me.Label11.TabIndex = 47
        Me.Label11.Text = "En Uso"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.Orange
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.CausesValidation = False
        Me.TextBox2.Location = New System.Drawing.Point(106, 19)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(14, 13)
        Me.TextBox2.TabIndex = 46
        Me.TextBox2.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(225, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(34, 59)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 13)
        Me.Label8.TabIndex = 44
        Me.Label8.Text = "Prefacturado"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.RoyalBlue
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.CausesValidation = False
        Me.TextBox1.Location = New System.Drawing.Point(14, 58)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(14, 13)
        Me.TextBox1.TabIndex = 43
        Me.TextBox1.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'LabelTiempo
        '
        Me.LabelTiempo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelTiempo.BackColor = System.Drawing.Color.DimGray
        Me.LabelTiempo.Font = New System.Drawing.Font("Trebuchet MS", 14.25!)
        Me.LabelTiempo.ForeColor = System.Drawing.Color.Yellow
        Me.LabelTiempo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.LabelTiempo.Location = New System.Drawing.Point(793, 5)
        Me.LabelTiempo.Name = "LabelTiempo"
        Me.LabelTiempo.Size = New System.Drawing.Size(40, 26)
        Me.LabelTiempo.TabIndex = 36
        Me.LabelTiempo.Text = "0"
        Me.LabelTiempo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.BackColor = System.Drawing.Color.DimGray
        Me.Label9.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(674, 5)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(124, 26)
        Me.Label9.TabIndex = 37
        Me.Label9.Text = "Actualizar en:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer1.Location = New System.Drawing.Point(2, 35)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.PanelMesas)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.PanelGrupos)
        Me.SplitContainer1.Panel2.Controls.Add(Me.PanelSecciones)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Size = New System.Drawing.Size(830, 594)
        Me.SplitContainer1.SplitterDistance = 559
        Me.SplitContainer1.SplitterWidth = 10
        Me.SplitContainer1.TabIndex = 46
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.LabelTiempo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(836, 36)
        Me.Panel1.TabIndex = 47
        '
        'Comanda
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(836, 706)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.LabelEstado)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(844, 726)
        Me.Name = "Comanda"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Centro de Comandas"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.PanelMesas.ResumeLayout(False)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelMesas As System.Windows.Forms.Panel
    Friend WithEvents PanelSecciones As System.Windows.Forms.Panel
    Friend WithEvents PanelGrupos As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lbestado As System.Windows.Forms.Label
    Friend WithEvents txtLibre As System.Windows.Forms.TextBox
    Friend WithEvents txtOcupada As System.Windows.Forms.TextBox
    Friend WithEvents txtSeccion As System.Windows.Forms.TextBox
    Friend WithEvents txtFactura As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents LabelTiempo As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolButtonBuscarFactura As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolButtonCambiarMesa As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolButtonTerminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents LabelEstado As System.Windows.Forms.Label
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents botonVerComandas As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btParallevar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btExpress As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripUnirMesa As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSepararMesa As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
End Class
