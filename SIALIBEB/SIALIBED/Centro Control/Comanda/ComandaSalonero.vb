Imports System.Data.SqlClient
Public Class ComandaSalonero

#Region "Variables"
    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlConnection
    Public cedulaS, nombreS As String
#End Region

    Private Sub ComandaSalonero_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        cargar()
    End Sub
    Private Sub ComandaSalonero_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub cargar()
        Dim datas3 As New DataSet
        cConexion.GetDataSet(conectadobd, "Select CedulaS, NombreS AS [Nombre Salonero(a)] From RelacionTrabajo where CedulaU='" & txtcedulaU.Text & "'", datas3, "RelacionTrabajo")
        Me.GridRelacion.DataSource = datas3.Tables("RelacionTrabajo")
        Me.GridRelacion.Columns(0).Visible = False
        Me.GridRelacion.Columns(1).Width = 400
        Me.GridRelacion.Columns(1).ReadOnly = True
        Me.GridRelacion.Visible = True
    End Sub

    Private Sub guardar()
        cedulaS = Me.GridRelacion(0, Me.GridRelacion.CurrentRow.Index).Value
        nombreS = Me.GridRelacion(1, Me.GridRelacion.CurrentRow.Index).Value
        Close()
    End Sub

    Private Sub GridRelacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridRelacion.DoubleClick
        guardar()
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 4 : guardar()    'HACER
            Case 8 : Close()
        End Select
    End Sub
End Class