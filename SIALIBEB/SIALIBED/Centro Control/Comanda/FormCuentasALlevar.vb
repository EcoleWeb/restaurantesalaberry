Public Class FormCuentasALlevar

    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim cedula1 As String
    Dim usaGrupo As Boolean = False
    Dim impVenta As Double = 0
    Dim currentcomanda As String
    Dim currentIdCliente As Integer
    Dim currentCliente As New DataTable
    Dim modificar As Integer = 0
    Dim cargando As Boolean = True
    Dim usuario As String = ""
    Dim nombreUsuario As String = ""
    Dim factura As String = ""
    Dim cedula As String
    Dim idcliente As String
	Dim transporte As Double = 0
	'Clase de pedidos express
	Dim PedidoExpres As New cls_PedidosExpress()
	'CLase con clientes express
	Dim ClienteExpress As New cls_ClientesExpress()
	Dim PMU As New PerfilModulo_Class
	'Bandera de busqueda..
	Dim buscando As Boolean = False
    Public Acumular As Integer = 0
    Dim SoyAcumulado As Integer = 0
    Dim direcci As Integer

    'Cargamos el formulario..!
    Private Sub FormCuentasALlevar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        usaGrupo = Me.PedidoExpres.configs.utilizaGrupo
        impVenta = Me.PedidoExpres.configs.Imp_Venta

        cedula1 = User_Log.Cedula

        listadoPedidos()

        Me.cargarDatos()

        cargando = False

    End Sub
    'Cargamos las configuraciones iniciales de la pantalla..
    Public Sub cargarDatos()
        If dgCliente.Rows.Count > 0 Then
            Me.dgCliente.Columns("Telefono").Width = 100
            Me.dgCliente.Columns("Nombre").Width = 250
        End If
    End Sub

    'Limpiamos los campos 
    Public Sub limpiarCampos()

        Me.TextBoxTelefono.Text = ""
        Me.TextBoxNombreCliente.Text = ""
        Me.TextBoxDireccion.Text = ""
		Me.TextBoxComentario.Text = ""
		Me.txtCedula.Text = ""
		Me.txtCorreo.Text = ""
		Me.txtTransporte.Text = ""


		Me.DataGridView1.DataSource = Nothing
        Me.dgCliente.DataSource = Nothing

        Me.ClienteExpress.obtenerTodosLosClientes()

        Me.Panel1.Visible = False

        Me.LinkLabel3.Visible = False
        Me.LinkLabel4.Visible = False
        Me.LinkLabel5.Visible = False
        Me.LinkLabel6.Visible = False
        Me.LinkLabel8.Visible = False
        Me.lbMarcarEntregado.Visible = False

    End Sub

    'Dependiendo del valor en el texboxtelefono, cargamos los datos del cliente..
    Public Sub cargarDatosCliente()
        'Buscamos la informacion del cliente con el numero de telefono..
        Me.ClienteExpress.obtenerCliente(Me.TextBoxTelefono.Text)

        Me.TextBoxNombreCliente.Text = Me.ClienteExpress.Nombre
        Me.TextBoxDireccion.Text = Me.ClienteExpress.Direccion
		Me.TextBoxComentario.Text = Me.ClienteExpress.Comentarios
		Me.txtCedula.Text = Me.ClienteExpress.Cedula
		Me.txtCorreo.Text = Me.ClienteExpress.Correo
		Me.txtTransporte.Text = Me.ClienteExpress.Transporte

		Me.LinkLabel3.Visible = True
        Me.LinkLabel4.Visible = False
        Me.LinkLabel5.Visible = False


    End Sub

    'BUscamos el cliente
    Sub buscarCliente(ByVal telefono As String, ByVal pedido As Boolean)

        cFunciones.Llenar_Tabla_Generico("SELECT ClienteExpress.* " & _
                            "FROM ClienteExpress Where Telefono = '" & telefono & "' ", currentCliente)

        If currentCliente.Rows.Count > 0 Then
            Me.currentIdCliente = currentCliente.Rows(0).Item("Id")
            Me.TextBoxNombreCliente.Text = currentCliente.Rows(0).Item("Nombre")
            Me.TextBoxDireccion.Text = currentCliente.Rows(0).Item("Direccion")
			Me.TextBoxComentario.Text = currentCliente.Rows(0).Item("Comentarios")
			Me.txtCedula.Text = currentCliente.Rows(0).Item("Cedula")
			Me.txtCorreo.Text = currentCliente.Rows(0).Item("Correo")
			Me.txtTransporte.Text = currentCliente.Rows(0).Item("Transporte")
			transporte = Double.Parse(currentCliente.Rows(0).Item("Transporte"))
			If pedido Then
                'guardarCliente()
            End If
        Else
            If pedido Then
                MsgBox("No existe ese cliente, se procedera a guardarlo", MsgBoxStyle.OkOnly)
                'guardarCliente()
            Else
                MsgBox("No existe ese cliente", MsgBoxStyle.OkOnly)
                Me.TextBoxNombreCliente.Focus()
            End If
        End If
    End Sub

    'Supuestamente carga el pedido..
    Sub cargarPedido(ByVal IdComanda As Integer)

        Dim SubTotal As Double = 0
        Dim SubTotalG As Double = 0
        Dim Totaldescuento As Double = 0
        Dim impServicio As Double = 0

        Dim temp As DataTable
        Dim ValidarPedidoFacturado As DataTable
        Dim FilasPedidos As Integer = 0

        temp = Me.PedidoExpres.ObtenerPedido(IdComanda.ToString())
        If direcci = 1 Then
            ValidarPedidoFacturado = Me.PedidoExpres.obtenerDetallePedidoFacturado(IdComanda.ToString(), 1)
        Else
            ValidarPedidoFacturado = Me.PedidoExpres.obtenerDetallePedidoFacturado(IdComanda.ToString())
        End If

        If IsNothing(ValidarPedidoFacturado) Then
            FilasPedidos = 0
        Else
            FilasPedidos = ValidarPedidoFacturado.Rows.Count
        End If
        If temp.Rows.Count > 0 Or FilasPedidos > 0 Then

            'Cargamos el grid con la informacion del pedido..
            Me.DataGridView1.DataSource = temp
            'FOrmateamos el grid..
            Me.DataGridView1.Columns("Id").Visible = False
            Me.DataGridView1.Columns("cProducto").Visible = False
            Me.DataGridView1.Columns("cPrecio").Visible = False
            Me.DataGridView1.Columns("cCodigo").Visible = False
            Me.DataGridView1.Columns("cMesa").Visible = False
            Me.DataGridView1.Columns("Impreso").Visible = False
            Me.DataGridView1.Columns("Impresora").Visible = False
            Me.DataGridView1.Columns("Comenzales").Visible = False
            Me.DataGridView1.Columns("Tipo_Plato").Visible = False
            Me.DataGridView1.Columns("CantImp").Visible = False
            Me.DataGridView1.Columns("Descuento").Visible = False
            Me.DataGridView1.Columns("Cedula").Visible = False
            Me.DataGridView1.Columns("Habitacion").Visible = False
            Me.DataGridView1.Columns("Primero").Visible = False
            Me.DataGridView1.Columns("Separada").Visible = False
            Me.DataGridView1.Columns("Hora").Visible = False
            Me.DataGridView1.Columns("Express").Visible = False
            Me.DataGridView1.Columns("Cantidad1").Visible = False
            Me.DataGridView1.Columns("costo_real").Visible = False
            Me.DataGridView1.Columns("comandado").Visible = False
            Me.DataGridView1.Columns("iVenta").Visible = False
            Me.DataGridView1.Columns("iServicio").Visible = False

            Me.DataGridView1.Columns("Cantidad").Width = 100
            Me.DataGridView1.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            Me.DataGridView1.Columns("Descripcion").Width = 210

            Me.DataGridView1.Columns("SubTotal").Width = 100
            Me.DataGridView1.Columns("SubTotal").DefaultCellStyle.Format = "c"

            Dim r As DataRow

            For Each r In temp.Rows
                If r("iVenta") Then
                    SubTotalG += r("SubTotal")
                End If
                Totaldescuento += r("SubTotal") * (r("Descuento") / 100)
                SubTotal += r("SubTotal")
            Next
            'Si se quiere aplicar impuesto de servicio a pedidos express..
            If GetSetting("SeeSOFT", "Restaurante", "IServicioExpress") = 1 Then
                impServicio = SubTotal * 10 / 100
                Me.textImpuestoServicio.Text = Format(impServicio, "##,#0.00")
            Else
                Me.textImpuestoServicio.Text = Format(0, "##,#0.00")
            End If
            'If txtTransporte.Text Then
            Me.TextBoxSubTotal.Text = Format(SubTotal, "##,#0.00")
            Dim iVenta As Double = (SubTotal - Totaldescuento) * (impVenta / 100)
            Me.TextBoxImpVenta.Text = Format(iVenta, "##,#0.00")
            Me.TextBoxTotal.Text = Format(SubTotal + impServicio + iVenta + transporte - Totaldescuento, "#,#0.00")
            Me.textdescuento.Text = Format(Totaldescuento, "##,#0.00")

            Me.txtTotalTransporte.Text = Format(transporte, "##,#0.00")
            Me.Panel1.Visible = True
        Else
            Dim cx As New Conexion
            cx.Conectar()

            cx.SlqExecute(cx.sQlconexion, "Delete PedidoExpress  WHERE cComanda = " & Me.currentcomanda)
            cx.DesConectar(cx.sQlconexion)
        End If

    End Sub



    'Este procedimiento carga un pedido que ya fue facturado
    'por lo tanto, este ya no se encuentra en la comanda, 
    ' si no mas bien, en las tablas ventas y ventas_detalle
    Sub cargarPedidoFacturado(ByVal IdComanda As Integer)

        Dim SubTotal As Double = 0
        Dim SubTotalG As Double = 0
        Dim Totaldescuento As Double = 0

		Dim temp As DataTable
		Dim tempMaestro As DataTable
        'Cargamos el detalle de la venta
        If direcci = 1 Then
            temp = Me.PedidoExpres.obtenerDetallePedidoFacturado(IdComanda.ToString(), 1)
            'Cargamos el maestro de la venta..
            tempMaestro = Me.PedidoExpres.obtenerEncabezadoPedidoFacturado(IdComanda.ToString(), 1)
        Else
            temp = Me.PedidoExpres.obtenerDetallePedidoFacturado(IdComanda.ToString())
            'Cargamos el maestro de la venta..
            tempMaestro = Me.PedidoExpres.obtenerEncabezadoPedidoFacturado(IdComanda.ToString())
        End If


        Me.Panel1.Visible = False
		If IsNothing(tempMaestro) Then
			Exit Sub
		End If
		If tempMaestro.Rows.Count = 0 Then
			Exit Sub
		End If
		SubTotal = tempMaestro.Rows(0)("SubTotal")
        Totaldescuento = tempMaestro.Rows(0)("Descuento")

        'Cargamos el grid con la informacion del pedido..
        Me.DataGridView1.DataSource = temp
        'FOrmateamos el grid..

        Me.DataGridView1.Columns("Cantidad").Width = 100
        Me.DataGridView1.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        Me.DataGridView1.Columns("Descripcion").Width = 210

        Me.DataGridView1.Columns("SubTotal").Width = 100
        Me.DataGridView1.Columns("SubTotal").DefaultCellStyle.Format = "c"

        'Si se quiere aplicar impuesto de servicio a pedidos express..
        If GetSetting("SeeSOFT", "Restaurante", "IServicioExpress") = 1 Then
            Me.textImpuestoServicio.Text = Format(tempMaestro.Rows(0)("Monto_Saloero"), "##,#0.00")
        End If

        Me.TextBoxSubTotal.Text = Format(SubTotal, "##,#0.00")
        Dim iVenta As Double = (SubTotal - Totaldescuento) * (impVenta / 100)
        Me.TextBoxImpVenta.Text = Format(tempMaestro.Rows(0)("Imp_Venta"), "##,#0.00")
        Me.TextBoxTotal.Text = Format(tempMaestro.Rows(0)("Total"), "#,#0.00")
        Me.textdescuento.Text = Format(Totaldescuento, "##,#0.00")
		Me.txtTotalTransporte.Text = Format(transporte, "##,#0.00")

		Me.Panel1.Visible = True

    End Sub


    'PRocedimiento que guarda el pedido
    Sub guardarPedidoExpress()

        Dim cx As New Conexion
        cx.Conectar()
        Dim direccion As Integer
        If Acumular = 1 Then
            'TextBoxSubTotal.Text = "0"
            'Me.TextBoxImpVenta.Text = "0"
            'TextBoxTotal.Text = "0"
            Acumular = 0
            direccion = 1
        End If
        If Me.modificar = 0 Then 'NUEVO PEDIDO
            If Not Me.TextBoxTotal.Text.Equals("") Then
                If Me.TextBoxTotal.Text.Equals("0.00") Then
                    cx.SlqExecute(cx.sQlconexion, "INSERT INTO PedidoExpress" &
                                  "(IdCliente, cComanda, SubTotal, ImpVenta, Total, Preparado, LoTraen, Entregado,Direccion)" &
                                    " VALUES(" & Me.currentIdCliente & ", " & Me.currentcomanda & ", " & CDbl(Me.TextBoxSubTotal.Text) & ", " & CDbl(Me.TextBoxImpVenta.Text) & ", " & CDbl(Me.TextBoxTotal.Text) & ", 0, 0, 1," & direccion & ")")

                Else
                    cx.SlqExecute(cx.sQlconexion, "INSERT INTO PedidoExpress" &
                                  "(IdCliente, cComanda, SubTotal, ImpVenta, Total, Preparado, LoTraen, Entregado,Direccion)" &
                                    " VALUES(" & Me.currentIdCliente & ", " & Me.currentcomanda & ", " & CDbl(Me.TextBoxSubTotal.Text) & ", " & CDbl(Me.TextBoxImpVenta.Text) & ", " & CDbl(Me.TextBoxTotal.Text) & ", 0, 0, 0," & direccion & ")")
                End If
            End If
        Else 'ACTUALIZAR EL PEDIDO
                cx.SlqExecute(cx.sQlconexion, "UPDATE PedidoExpress SET SubTotal = " & CDbl(Me.TextBoxSubTotal.Text) & ", ImpVenta = " & CDbl(Me.TextBoxImpVenta.Text) & ", Total = " & CDbl(Me.TextBoxTotal.Text) & ",Direccion=" & direccion & " WHERE cComanda = " & Me.currentcomanda)
			If Not Me.factura.Equals("") Then
                cx.SlqExecute(cx.sQlconexion, "UPDATE PedidoExpress SET Preparado = 1, HoraLlevado= getDate() WHERE cComanda = " & Me.currentcomanda)
            End If
        End If
        cx.DesConectar(cx.sQlconexion)

    End Sub

    'Guardamos un nuevo cliente
    Sub guardarCliente()

        Me.ClienteExpress.Nombre = Me.TextBoxNombreCliente.Text
        Me.ClienteExpress.Telefono = Me.TextBoxTelefono.Text
        Me.ClienteExpress.Direccion = Me.TextBoxDireccion.Text
		Me.ClienteExpress.Comentarios = Me.TextBoxComentario.Text
		Me.ClienteExpress.Cedula = Me.txtCedula.Text
		Me.ClienteExpress.Correo = Me.txtCorreo.Text
        Me.ClienteExpress.Transporte = Double.TryParse(Me.txtTransporte.Text, 0)
        Me.ClienteExpress.nuevoCliente()
        Me.ClienteExpress.obtenerTodosLosClientes()

        'Cargamos los datos del cliente..

        Me.Panel2.Visible = False
        Me.TextBoxTelefono.Focus()
        Me.LinkLabel1.Visible = False
        Me.LinkLabel2.Visible = True
        Me.LinkLabel8.Visible = True
        'Mostramos los datos del cliente..
        Me.cargarDatosCliente()

    End Sub

    'Actualizamos la info de un cliente existente..
    Public Sub actualizarCliente()
        Me.ClienteExpress.Nombre = Me.TextBoxNombreCliente.Text
        Me.ClienteExpress.Telefono = Me.TextBoxTelefono.Text
        Me.ClienteExpress.Direccion = Me.TextBoxDireccion.Text
		Me.ClienteExpress.Comentarios = Me.TextBoxComentario.Text
		Me.ClienteExpress.Cedula = Me.txtCedula.Text
		Me.ClienteExpress.Correo = Me.txtCorreo.Text
		Me.ClienteExpress.Transporte = Double.Parse(Me.txtTransporte.Text)
		Me.ClienteExpress.actualizarCliente()
        Me.ClienteExpress.obtenerTodosLosClientes()
    End Sub

#Region "Verificar si Existe"
    Function verificarsiexiste() As Boolean

        Dim cx1 As New Conexion
        Try
            idcliente = cConexion.SlqExecuteScalar(cx1.Conectar(), "Select Id from ClienteExpress WITH (NOLOCK) where Telefono='" & Me.TextBoxTelefono.Text & "'")
            cConexion.DesConectar(cx1.sQlconexion)
        Catch ex As Exception
            MsgBox("No se pudo Verificar la existencia de este cliente", MsgBoxStyle.OkOnly)
        End Try
        If idcliente <> "" Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

    'Creamos un nuevo pedido
    Sub crearPedido()
        Dim idMesa As String = "0"
        Dim comenzales As String = "1"
        Dim numeroComanda As String = "0"
        Dim idGrupoConta As String = "0"

ReintentarClave:

        Dim rEmpleado0 As New registro
        rEmpleado0.Text = "DIGITE CONTRASE�A"
        rEmpleado0.txtCodigo.PasswordChar = "*"

        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
        If gloNoClave Then
            clave = User_Log.Clave_Interna
            rEmpleado0.iOpcion = 1
        Else
            rEmpleado0.ShowDialog()
            clave = rEmpleado0.txtCodigo.Text
        End If

        '---------------------------------------------------------------

        rEmpleado0.Dispose()
        If rEmpleado0.iOpcion = 0 Then Exit Sub

        If clave <> "" Then
            cedula1 = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Cedula from Usuarios WITH (NOLOCK) where Clave_Interna='" & clave & "'")
            If Trim(cedula1) = "" Or Trim(cedula1) = vbNullString Or rEmpleado0.iOpcion = 0 Then
                MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atenci�n...")
                GoTo ReintentarClave
                Exit Sub
            End If
        Else
            GoTo ReintentarClave
        End If
        Dim iCategoria As Integer = 1
        Dim primera As Boolean = True

        Dim platos As New PlatosExpress(txtTransporte.Text, False)
        Dim conectadobd As New SqlClient.SqlConnection
        conectadobd = cConexion.Conectar("Restaurante")
        cConexion.GetRecorset(conectadobd, "Select aperturacaja.NApertura, aperturacaja.Nombre, aperturacaja.Cedula from aperturacaja inner join Usuarios on aperturacaja.cedula = Usuarios.Cedula where aperturacaja.Anulado = 0 AND Usuarios.Clave_Interna = '" & clave & "' and aperturacaja.Estado = 'A'", rs)
        While rs.Read
            platos.Num_apertura = rs("NApertura")
        End While
        rs.Close()
        conectadobd.Close()

        While iCategoria = 1

            Hide()

            If usaGrupo = True Then
                Dim cGrupo As New GrupoPrincipal
                cGrupo.ShowDialog()
                idGrupoConta = cGrupo.idGrupo
                If cGrupo.idGrupo = -1 Then
                    Exit Sub
                End If
                cGrupo.Dispose()
            Else
                idGrupoConta = 0
            End If
            If modificar = 0 Then
                If primera Then
                    numeroComanda = 0
                    primera = False
                End If

            Else
                numeroComanda = currentcomanda
            End If

			platos.idMesa = idMesa
			platos.cod_Clienteexpress = ClienteExpress.Id
			platos.nombreClienteexpress = TextBoxNombreCliente.Text
            platos.telefonocliente = TextBoxTelefono.Text
			platos.cedulaexpress = txtCedula.Text
			platos.Direccion = TextBoxDireccion.Text


			platos.comenzales = comenzales
            platos.numeroComanda = numeroComanda
            platos.nombreMesa = "Pedido Express"
            platos.idGrupo = idGrupoConta
            platos.usuario = cedula1
            platos.cCuentas = 0

            If Me.modificar = 1 Then
                platos.TipoPedido = 2
            End If
            platos.ShowDialog()
            iCategoria = platos.iCategoria
            currentcomanda = platos.numeroComanda
            numeroComanda = platos.numeroComanda
            direcci = platos.Acumular
            factura = platos.numFactura
            platos.Dispose()
            Show()

		End While
		If currentcomanda > 0 Then ' se valida que se haya comandado algo
			cargarPedido(currentcomanda)
			buscarCliente(Me.TextBoxTelefono.Text, True)
			guardarPedidoExpress()
		End If

		listadoPedidos()
        Me.limpiarCampos()
    End Sub

    Private Sub CheckBoxLlevar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.TextBoxDireccion.Enabled = Not Me.CheckBoxLlevar.Checked
    End Sub

    Private Sub ButtonBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.buscarCliente(Me.TextBoxTelefono.Text, False)

    End Sub

    Private Sub ButtonCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()

    End Sub

    Private Sub ButtonAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        modificar = 0
        crearPedido()
    End Sub
    ' LIstamos los pedidos pendientes. y damos formato al grid..
    Sub listadoPedidos()


        Me.DataGridView2.DataSource = Me.PedidoExpres.obtenerPedidosPendientes()
        Me.DataGridView2.Columns("Cliente").Width = 175


        Me.DataGridView2.Columns("Id").Visible = False
        Me.DataGridView2.Columns("idCliente").Visible = False
        Me.DataGridView2.Columns("Fecha").Visible = False
        Me.DataGridView2.Columns("cComanda").Visible = False
        Me.DataGridView2.Columns("SubTotal").Visible = False
        Me.DataGridView2.Columns("ImpVenta").Visible = False
        Me.DataGridView2.Columns("Total").Visible = False
        Me.DataGridView2.Columns("Preparado").Visible = False
        Me.DataGridView2.Columns("LoTraen").Visible = False
        Me.DataGridView2.Columns("Entregado").Visible = False
        Me.DataGridView2.Columns("HoraPreparado").Visible = False
        Me.DataGridView2.Columns("HoraLlevado").Visible = False
        Me.DataGridView2.Columns("HoraEntregado").Visible = False
        Me.DataGridView2.Columns("Direccion").Visible = False




    End Sub

    Private Sub ButtonGuardarCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub DataGridView2_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellClick

        Me.cargando = True


        Me.currentIdCliente = Me.DataGridView2.CurrentRow.Cells("idCliente").Value.ToString()
        Me.currentcomanda = Me.DataGridView2.CurrentRow.Cells("cComanda").Value.ToString()

        Me.CheckBoxLlevar.Checked = Me.DataGridView2.CurrentRow.Cells("LoTraen").Value
        Me.CheckBoxEntregado.Checked = Me.DataGridView2.CurrentRow.Cells("Entregado").Value
        Me.CheckBoxPreparando.Checked = Me.DataGridView2.CurrentRow.Cells("Preparado").Value
        Me.LabelTiempoPrep.Text = Me.DataGridView2.CurrentRow.Cells("Fecha").Value
        direcci = Me.DataGridView2.CurrentRow.Cells("Direccion").Value

        If Not Me.DataGridView2.CurrentRow.Cells("Entregado").Value Then
            Me.LabelTiempo.Text = "Tiempo Entregando"
        Else
            Me.LabelTiempo.Text = Me.DataGridView2.CurrentRow.Cells("HoraLlevado").Value
        End If

        cargarCurrentPedido()

        Me.cargando = False


    End Sub

    'Cargamos el pedido seleccionadod..
    Public Sub cargarCurrentPedido()

        Me.ClienteExpress.obtenerClienteById(currentIdCliente)

        If Me.ClienteExpress.Id <> "" Then
            Me.buscando = False
            Me.Panel2.Visible = False
            Me.currentIdCliente = Me.ClienteExpress.Id
            Me.TextBoxNombreCliente.Text = Me.ClienteExpress.Nombre
            Me.TextBoxDireccion.Text = Me.ClienteExpress.Direccion
            Me.TextBoxComentario.Text = Me.ClienteExpress.Comentarios
			Me.TextBoxTelefono.Text = Me.ClienteExpress.Telefono
			Me.txtCedula.Text = Me.ClienteExpress.Cedula
			Me.txtCorreo.Text = Me.ClienteExpress.Correo
			Me.txtTransporte.Text = Me.ClienteExpress.Transporte
			transporte = Me.ClienteExpress.Transporte
		End If

        'Si el pedido ya fue facturado, entonces cargamos los datos de la factura..
        If Me.PedidoExpres.pedidoFacturado(currentcomanda) Then
            Me.cargarPedidoFacturado(currentcomanda)

            Me.LinkLabel2.Visible = False
            Me.LinkLabel8.Visible = False

            Me.LinkLabel3.Visible = False
            Me.LinkLabel4.Visible = False
            Me.LinkLabel5.Visible = False
            Me.LinkLabel6.Visible = True
            Me.lbMarcarEntregado.Visible = True

        Else
            'Si no, cargamos los datos de la comanda..
            Me.cargarPedido(currentcomanda)

            Me.LinkLabel2.Visible = False
            Me.LinkLabel8.Visible = False
            Me.LinkLabel3.Visible = False
            Me.LinkLabel4.Visible = True
            Me.LinkLabel5.Visible = True
            Me.LinkLabel6.Visible = False
            Me.lbMarcarEntregado.Visible = False

        End If

    End Sub

    'Limpiamos los campos
    Private Sub ButtonLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.currentIdCliente = 0
        Me.currentCliente.Rows.Clear()
        Me.TextBoxTelefono.Text = 0
        Me.TextBoxSubTotal.Text = 0
        Me.TextBoxDireccion.Text = "-"
        Me.TextBoxNombreCliente.Text = ""
        Me.TextBoxTotal.Text = 0
		Me.TextBoxComentario.Text = "-"
		Me.txtCedula.Text = ""
		Me.txtCorreo.Text = ""
		Me.txtTransporte.Text = ""
		TextBoxTelefono.SelectAll()
        Me.TextBoxTelefono.Focus()


    End Sub

    Private Sub ButtonComandar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        
    End Sub

    Private Sub ButtonQuitarPedido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

      
    End Sub

    Private Sub CheckBoxEntregado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxEntregado.CheckedChanged
       

    End Sub

    Sub cerrarPedido()
        If Not cargando Then
            If MsgBox("�Desea dejar como entregado el pedido?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
ReintentarClave:

                Dim rEmpleado0 As New registro
                rEmpleado0.Text = "DIGITE CONTRASE�A"
                rEmpleado0.txtCodigo.PasswordChar = "*"
                '---------------------------------------------------------------
                'VERIFICA SI PIDE O NO EL USUARIO
                If gloNoClave Then
                    clave = User_Log.Clave_Interna
                    rEmpleado0.iOpcion = 1
                Else
                    rEmpleado0.ShowDialog()
                    clave = rEmpleado0.txtCodigo.Text
                End If
                rEmpleado0.Dispose()
                If rEmpleado0.iOpcion = 0 Then Exit Sub

                If clave <> "" Then
                    Dim dt_usua As New DataTable
                    cFunciones.Llenar_Tabla_Generico("Select Cedula,nombre from Usuarios WITH (NOLOCK) where Clave_Interna='" & clave & "'", dt_usua)
                    If dt_usua.Rows.Count = 0 Then
                        MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atenci�n...")
                        GoTo ReintentarClave
                        Exit Sub
                    Else
                        usuario = dt_usua.Rows(0).Item("Cedula")
                        nombreUsuario = dt_usua.Rows(0).Item("Nombre")
                    End If

                Else
                    GoTo ReintentarClave
                End If

                '---------------------------------------------------------
                Dim cx As New Conexion

                cx.Conectar()

                Dim factura As String = ""
                Dim dt As New DataTable

                cFunciones.Llenar_Tabla_Generico("Select Numerofactura From Comanda Where  NumeroComanda = " & currentcomanda, dt)
				If dt.Rows.Count > 0 Then
					factura = dt.Rows(0).Item("Numerofactura")

				Else 'cuando se

                    cx.SlqExecute(cx.sQlconexion, "UPDATE PedidoExpress Set Entregado = 1, HoraEntregado = getDate() WHERE cComanda = " & Me.currentcomanda)
                    cx.DesConectar(cx.sQlconexion)
					listadoPedidos()
					Me.limpiarCampos()
					Exit Sub
				End If

                '....
                Dim fact As New cls_Facturas()
                If direcci = 0 Then
                    fact.obtenerDatosDeFactura(factura)
                    If fact.vueltos.Rows.Count = 0 Then
                        OpcionesdePago(factura)
                    End If

                End If



                cx.SlqExecute(cx.sQlconexion, "UPDATE PedidoExpress Set Entregado = 1, HoraEntregado = getDate() WHERE cComanda = " & Me.currentcomanda)
				cx.DesConectar(cx.sQlconexion)
				Me.modificar = 1
				guardarPedidoExpress()
				Me.modificar = 0
				LabelTiempo.Text = Now
                Panel1.Visible = False
                listadoPedidos()

                Me.limpiarCampos()

            End If

        End If

    End Sub

    Private Sub OpcionesdePago(ByVal fac As String)
        Try
            Dim nFactura As Int64
            Dim cFacturaVenta As New frmMovimientoCajaPagoAbono(User_Log.PuntoVenta)
            Dim strb As String = "SELECT * from BuscaFactura WITH (NOLOCK) where Facturado=0 AND anulado=0 and Num_Factura = " & fac & " AND Proveniencia_Venta = " & User_Log.PuntoVenta & " Order by NumeroComanda ASC"
            cConexion.GetRecorset(cConexion.Conectar(), strb, rs)

            While rs.Read
                cFacturaVenta.Factura = CDbl(rs("Num_Factura"))
                cFacturaVenta.fecha = CDate(rs("Fecha"))
				cFacturaVenta.Total = Math.Round(CDbl(rs("Total")), 2)
				cFacturaVenta.codmod = rs("Cod_Moneda")
                cFacturaVenta.Tipo = "FV"
                nFactura = cFacturaVenta.Factura
            End While
            rs.Close()

            cFacturaVenta.Tipo = "FV"
            cFacturaVenta.codmod = 1
            cFacturaVenta.cedu = usuario
            cFacturaVenta.nombre = nombreUsuario
            cFacturaVenta.ShowDialog()
            cConexion.UpdateRecords(cConexion.Conectar(), "Comanda", "Facturado=1", "Facturado=0 and Numerofactura=" & nFactura)
            cFacturaVenta.Dispose()

        Catch ex As Exception
            MsgBox("Seleccione la cuenta a cancelar", MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
    
    Private Sub TextBoxTelefono_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            If Me.TextBoxTelefono.Text.Equals("0") Or Me.TextBoxTelefono.Text.Equals("") Then
                MsgBox("Digite un numero de telefono valido", MsgBoxStyle.Information, "Atenci�n...")
                Exit Sub
            End If
            Me.buscarCliente(Me.TextBoxTelefono.Text, False)
        End If
        If e.KeyCode = Keys.F1 Then
            clientesexpress()
        End If



    End Sub

    Sub clientesexpress()
        Dim frmclieteexpress As New Clienteexpress
        frmclieteexpress.ToolBarEliminar.Enabled = True
        frmclieteexpress.ShowDialog()
        If frmclieteexpress.nombre <> "" Then
            Me.TextBoxNombreCliente.Text = frmclieteexpress.nombre
            Me.TextBoxTelefono.Text = frmclieteexpress.telefono
			Me.TextBoxDireccion.Text = frmclieteexpress.Direccion
			'Me.txtCedula.Text = frmclieteexpress.cedula

		End If
        frmclieteexpress.Close()
    End Sub
    
    Private Sub TextBoxNombreCliente_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.F1 Then
            clientesexpress()
        End If
        If e.KeyCode = Keys.Enter Then
            Me.TextBoxDireccion.Focus()
        End If
    End Sub

    Private Sub Buttonteclado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        teclado()
    End Sub

    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
            TextBoxTelefono.Focus()
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicaci�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
  
    Private Sub TextBoxTelefono_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        If TextBoxTelefono.Text = "0" Then
            TextBoxTelefono.SelectAll()
        End If
    End Sub



    'Cuando escribimos en el textbox de telefono, buscamos un cliente..
    Private Sub TextBoxTelefono_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxTelefono.TextChanged

        ' Si lo que estamos haciendo es No Buscando entonces salimos y no hacemos la busqueda..
        If Me.buscando = False Then Exit Sub

        Me.dgCliente.DataSource = Me.ClienteExpress.BuscarCliente(Me.TextBoxTelefono.Text)
        Me.dgCliente.Columns("Telefono").Width = 100
        Me.dgCliente.Columns("Nombre").Width = 200


        'Si el campo esta vacio, entonces desaparece el panel..
        If Me.TextBoxTelefono.Text.Length = 0 Then
            Me.Panel2.Visible = False
            Me.LinkLabel1.Visible = False
            Me.LinkLabel2.Visible = False
            Me.LinkLabel8.Visible = False

            Me.limpiarCampos()
            Exit Sub
        End If

        'Si existen resultados, mostramos el panel de busqueda..
        If Me.dgCliente.Rows.Count > 0 Then
            Me.Panel2.Visible = True
            Me.LinkLabel1.Visible = False
            Me.LinkLabel2.Visible = True
            Me.LinkLabel8.Visible = True

            Me.buscando = True
        Else
            Me.Panel2.Visible = False
            Me.LinkLabel1.Visible = True
            Me.LinkLabel2.Visible = False
            Me.LinkLabel8.Visible = False

        End If

    End Sub

    'CUando presionamos una tecla.. 
    Private Sub TextBoxTelefono_KeyDown_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxTelefono.KeyDown
        'Si es la tecla de abajo, entonces quiere buscar en el grid de busqueda..
        If e.KeyCode = Keys.Down Then
            dgCliente.Focus()
            If dgCliente.Rows.Count > 1 Then
                dgCliente.CurrentCell = dgCliente.Rows(0).Cells(0)
            End If
        End If

        'SI es un enter.. entonces el usuario sabe el numero de telefono y dio Enter sobre el texbox
        'con lo que encuentra el cliente..
        If e.KeyCode = Keys.Enter And Me.Panel2.Visible = True Then

            Me.TextBoxTelefono.Text = Me.dgCliente.CurrentRow.Cells("Telefono").Value.ToString()
            Me.Panel2.Visible = False
            Me.TextBoxTelefono.Focus()
            Me.LinkLabel1.Visible = False
            Me.LinkLabel2.Visible = True
            Me.LinkLabel8.Visible = True

            'Mostramos los datos del cliente..
            Me.cargarDatosCliente()

        End If

    End Sub

    'Si dentro del grid de buscar cliente se presiona una tecla..
    Private Sub dgCliente_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgCliente.KeyDown
        'Si es el retroceso..
        If e.KeyCode = Keys.Back Then
            Me.TextBoxTelefono.Focus()
            Me.TextBoxTelefono.Text.Remove(Me.TextBoxTelefono.Text.Length - 1, 1)
        End If

        'CUando presiona entrar es porque encontro el numero deseado
        If e.KeyCode = Keys.Enter Then

            Me.TextBoxTelefono.Text = Me.dgCliente.CurrentRow.Cells("Telefono").Value.ToString()
            Me.Panel2.Visible = False
            Me.TextBoxTelefono.Focus()

            Me.LinkLabel1.Visible = False
            Me.LinkLabel2.Visible = True
            Me.LinkLabel8.Visible = True


            'Mostramos los datos del cliente..

            Me.cargarDatosCliente()

        End If
    End Sub

    'CUando cambiamos de fila..
    Private Sub dgCliente_RowEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgCliente.RowEnter

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Me.guardarCliente()
    End Sub

    'Al dar doble click sobre el grid, encontramos el cliente..
    Private Sub dgCliente_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgCliente.CellDoubleClick

        Me.TextBoxTelefono.Text = Me.dgCliente.CurrentRow.Cells("Telefono").Value.ToString()
        Me.Panel2.Visible = False
        Me.TextBoxTelefono.Focus()
        'Mostramos los datos del cliente..
        Me.cargarDatosCliente()

    End Sub

    Private Sub dgCliente_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgCliente.CellClick

    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Me.actualizarCliente()
    End Sub

    Private Sub DataGridView2_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick

    End Sub

    Private Sub TextBoxTelefono_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxTelefono.Enter
        Me.buscando = True
    End Sub

    Private Sub TextBoxTotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxTotal.TextChanged

    End Sub

    Private Sub LinkLabel3_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        modificar = 0
        crearPedido()
    End Sub

    Private Sub LinkLabel5_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel5.LinkClicked
		PMU = VSM(User_Log.Cedula, "PlatoExpress")
		If Not PMU.Delete Then
			MsgBox("No tiene permiso para eliminar el pedido...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
		End If
		Me.PedidoExpres.eliminarPedidoExpress(Me.currentcomanda)
		Me.listadoPedidos()
        Me.Panel1.Visible = False
        Me.limpiarCampos()
        'Me.GroupBoxDatos.Visible = False
    End Sub

    Private Sub LinkLabel4_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel4.LinkClicked
        Me.modificar = 1
        Me.crearPedido()
        Me.limpiarCampos()
    End Sub

    Private Sub LinkLabel6_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel6.LinkClicked
        CargarRepartidores()
    End Sub

    Private Sub CargarRepartidores()
        Try
            Dim frm As New frmRepartidores
            Dim dt As New DataTable

            frm.QuienMeLlama = "Asignar"
            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Numerofactura From Comanda Where  NumeroComanda = " & currentcomanda & " order by IdComanda desc", dt)
            If dt.Rows.Count > 0 Then
                frm.IdFactura = Convert.ToInt32(dt.Rows(0).Item("Numerofactura"))
            End If
            frm.ShowDialog()
            Me.cerrarPedido()
        Catch ex As Exception
            MsgBox("Error al cargar repartidores. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub LinkLabel7_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel7.LinkClicked
        Me.Close()
    End Sub

    Private Sub GroupBoxListado_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBoxListado.Enter

    End Sub

    Private Sub LinkLabel8_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel8.LinkClicked

        'Borramos un cliente ..
        Me.ClienteExpress.obtenerCliente(Me.TextBoxTelefono.Text)
        Me.ClienteExpress.borraCliente()
        Me.limpiarCampos()

    End Sub

    Private Sub lbMarcarEntregado_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lbMarcarEntregado.LinkClicked
        Me.cerrarPedido()
    End Sub
End Class