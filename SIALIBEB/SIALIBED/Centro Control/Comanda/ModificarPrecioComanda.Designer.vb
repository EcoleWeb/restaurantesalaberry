<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ModificarPrecioComanda
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.TxtPrecio = New System.Windows.Forms.TextBox
        Me.TxtItem = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.TxtCantidad = New System.Windows.Forms.Label
        Me.TxtDescripcion = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtComanda = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.TxtProducto = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.TxtImpuestoVentas = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.TxtImpuestoServicio = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.TxtPrecioFinal = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(224, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Item"
        '
        'TxtPrecio
        '
        Me.TxtPrecio.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtPrecio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtPrecio.Location = New System.Drawing.Point(217, 101)
        Me.TxtPrecio.Name = "TxtPrecio"
        Me.TxtPrecio.Size = New System.Drawing.Size(143, 16)
        Me.TxtPrecio.TabIndex = 1
        '
        'TxtItem
        '
        Me.TxtItem.BackColor = System.Drawing.Color.Transparent
        Me.TxtItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtItem.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtItem.Location = New System.Drawing.Point(274, 4)
        Me.TxtItem.Name = "TxtItem"
        Me.TxtItem.Size = New System.Drawing.Size(93, 17)
        Me.TxtItem.TabIndex = 2
        Me.TxtItem.Text = "0.00"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(7, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 17)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Cantidad"
        '
        'TxtCantidad
        '
        Me.TxtCantidad.BackColor = System.Drawing.Color.Transparent
        Me.TxtCantidad.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TxtCantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCantidad.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtCantidad.Location = New System.Drawing.Point(107, 75)
        Me.TxtCantidad.Name = "TxtCantidad"
        Me.TxtCantidad.Size = New System.Drawing.Size(104, 17)
        Me.TxtCantidad.TabIndex = 4
        Me.TxtCantidad.Text = "0.00"
        Me.TxtCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtDescripcion
        '
        Me.TxtDescripcion.BackColor = System.Drawing.Color.Transparent
        Me.TxtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TxtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDescripcion.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtDescripcion.Location = New System.Drawing.Point(107, 52)
        Me.TxtDescripcion.Name = "TxtDescripcion"
        Me.TxtDescripcion.Size = New System.Drawing.Size(260, 18)
        Me.TxtDescripcion.TabIndex = 6
        Me.TxtDescripcion.Text = "Descrición del Item"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(8, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 17)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Descripción"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.Location = New System.Drawing.Point(78, 101)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(133, 17)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Precio / Unit."
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtComanda
        '
        Me.txtComanda.BackColor = System.Drawing.Color.Transparent
        Me.txtComanda.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.txtComanda.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComanda.ForeColor = System.Drawing.Color.RoyalBlue
        Me.txtComanda.Location = New System.Drawing.Point(107, 5)
        Me.txtComanda.Name = "txtComanda"
        Me.txtComanda.Size = New System.Drawing.Size(93, 17)
        Me.txtComanda.TabIndex = 9
        Me.txtComanda.Text = "0.00"
        Me.txtComanda.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label9.Location = New System.Drawing.Point(5, 5)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(96, 17)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Comanda"
        '
        'TxtProducto
        '
        Me.TxtProducto.BackColor = System.Drawing.Color.Transparent
        Me.TxtProducto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TxtProducto.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtProducto.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtProducto.Location = New System.Drawing.Point(107, 26)
        Me.TxtProducto.Name = "TxtProducto"
        Me.TxtProducto.Size = New System.Drawing.Size(93, 17)
        Me.TxtProducto.TabIndex = 10
        Me.TxtProducto.Text = "0.00"
        Me.TxtProducto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(157, 196)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 39)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Aceptar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(271, 196)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(96, 39)
        Me.Button2.TabIndex = 12
        Me.Button2.Text = "Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(5, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 17)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Producto"
        '
        'TxtImpuestoVentas
        '
        Me.TxtImpuestoVentas.BackColor = System.Drawing.Color.Transparent
        Me.TxtImpuestoVentas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TxtImpuestoVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtImpuestoVentas.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtImpuestoVentas.Location = New System.Drawing.Point(217, 124)
        Me.TxtImpuestoVentas.Name = "TxtImpuestoVentas"
        Me.TxtImpuestoVentas.Size = New System.Drawing.Size(143, 17)
        Me.TxtImpuestoVentas.TabIndex = 15
        Me.TxtImpuestoVentas.Text = "0.00"
        Me.TxtImpuestoVentas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Location = New System.Drawing.Point(78, 124)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(133, 17)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Imp. Ventas"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TxtImpuestoServicio
        '
        Me.TxtImpuestoServicio.BackColor = System.Drawing.Color.Transparent
        Me.TxtImpuestoServicio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TxtImpuestoServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtImpuestoServicio.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtImpuestoServicio.Location = New System.Drawing.Point(217, 144)
        Me.TxtImpuestoServicio.Name = "TxtImpuestoServicio"
        Me.TxtImpuestoServicio.Size = New System.Drawing.Size(143, 17)
        Me.TxtImpuestoServicio.TabIndex = 17
        Me.TxtImpuestoServicio.Text = "0.00"
        Me.TxtImpuestoServicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label10.Location = New System.Drawing.Point(81, 145)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(130, 17)
        Me.Label10.TabIndex = 16
        Me.Label10.Text = "Imp. Serv"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TxtPrecioFinal
        '
        Me.TxtPrecioFinal.BackColor = System.Drawing.Color.Transparent
        Me.TxtPrecioFinal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TxtPrecioFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtPrecioFinal.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtPrecioFinal.Location = New System.Drawing.Point(217, 164)
        Me.TxtPrecioFinal.Name = "TxtPrecioFinal"
        Me.TxtPrecioFinal.Size = New System.Drawing.Size(143, 17)
        Me.TxtPrecioFinal.TabIndex = 19
        Me.TxtPrecioFinal.Text = "0.00"
        Me.TxtPrecioFinal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label12.Location = New System.Drawing.Point(81, 165)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(130, 17)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Precio Final/Unit"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ModificarPrecioComanda
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(372, 236)
        Me.ControlBox = False
        Me.Controls.Add(Me.TxtPrecioFinal)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.TxtImpuestoServicio)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.TxtImpuestoVentas)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TxtProducto)
        Me.Controls.Add(Me.txtComanda)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TxtDescripcion)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TxtCantidad)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TxtItem)
        Me.Controls.Add(Me.TxtPrecio)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "ModificarPrecioComanda"
        Me.Text = "Cambio de Precio"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtPrecio As System.Windows.Forms.TextBox
    Friend WithEvents TxtItem As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TxtCantidad As System.Windows.Forms.Label
    Friend WithEvents TxtDescripcion As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtComanda As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TxtProducto As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtImpuestoVentas As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxtImpuestoServicio As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TxtPrecioFinal As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
End Class
