Public Class frmInfoPlatillo

    Public Sub New(ByRef _dt As DataTable)

        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        Me.DataGridView1.DataSource = _dt
        Me.DataGridView1.Columns("Descripcion").Width = 391

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        Me.Close()

    End Sub

    Private Sub TextBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Leave, Label5.Click, Label4.Click, Label3.Click, Label1.Click
        Me.Close()
    End Sub

    Private Sub frmInfoPlatillo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Click, DataGridView1.Click
        Me.Close()
    End Sub

    Private Sub frmInfoPlatillo_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Me.Close()
    End Sub
End Class