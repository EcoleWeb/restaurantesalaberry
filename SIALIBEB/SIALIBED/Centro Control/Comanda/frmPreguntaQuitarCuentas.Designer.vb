﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreguntaQuitarCuentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPreguntaQuitarCuentas))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnSeleccionadas = New System.Windows.Forms.Button()
        Me.btnTodas = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(84, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(274, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Que cuetas desea quitar"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.SIALIBEB.My.Resources.Resources.Pregunta
        Me.PictureBox1.Location = New System.Drawing.Point(30, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'btnSeleccionadas
        '
        Me.btnSeleccionadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSeleccionadas.Location = New System.Drawing.Point(23, 78)
        Me.btnSeleccionadas.Name = "btnSeleccionadas"
        Me.btnSeleccionadas.Size = New System.Drawing.Size(171, 50)
        Me.btnSeleccionadas.TabIndex = 2
        Me.btnSeleccionadas.Text = "Cuentas Selecciondas"
        Me.btnSeleccionadas.UseVisualStyleBackColor = True
        '
        'btnTodas
        '
        Me.btnTodas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTodas.Location = New System.Drawing.Point(200, 78)
        Me.btnTodas.Name = "btnTodas"
        Me.btnTodas.Size = New System.Drawing.Size(171, 50)
        Me.btnTodas.TabIndex = 3
        Me.btnTodas.Text = "Todas las Cuentas"
        Me.btnTodas.UseVisualStyleBackColor = True
        '
        'frmPreguntaQuitarCuentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(401, 141)
        Me.Controls.Add(Me.btnTodas)
        Me.Controls.Add(Me.btnSeleccionadas)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPreguntaQuitarCuentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cuentas a Quitar"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnSeleccionadas As System.Windows.Forms.Button
    Friend WithEvents btnTodas As System.Windows.Forms.Button
End Class
