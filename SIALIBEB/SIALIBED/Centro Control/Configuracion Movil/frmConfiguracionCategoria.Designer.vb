﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfiguracionCategoria
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.grCategorias = New System.Windows.Forms.DataGridView
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Color = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Posicion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bsCategoriaMenu = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtsCategoriaMenu1 = New SIALIBEB.dtsCategoriaMenu
        Me.btGuardar = New System.Windows.Forms.Button
        Me.Categorias_MenuTableAdapter = New SIALIBEB.dtsCategoriaMenuTableAdapters.Categorias_MenuTableAdapter
        CType(Me.grCategorias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCategoriaMenu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsCategoriaMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grCategorias
        '
        Me.grCategorias.AllowUserToAddRows = False
        Me.grCategorias.AllowUserToDeleteRows = False
        Me.grCategorias.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grCategorias.AutoGenerateColumns = False
        Me.grCategorias.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grCategorias.BackgroundColor = System.Drawing.SystemColors.ControlLightLight
        Me.grCategorias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grCategorias.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NombreDataGridViewTextBoxColumn, Me.Color, Me.Posicion})
        Me.grCategorias.DataSource = Me.bsCategoriaMenu
        Me.grCategorias.Location = New System.Drawing.Point(12, 12)
        Me.grCategorias.Name = "grCategorias"
        Me.grCategorias.RowHeadersVisible = False
        Me.grCategorias.Size = New System.Drawing.Size(465, 298)
        Me.grCategorias.TabIndex = 0
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Color
        '
        Me.Color.DataPropertyName = "Color"
        Me.Color.HeaderText = "Color"
        Me.Color.Name = "Color"
        Me.Color.ReadOnly = True
        Me.Color.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Posicion
        '
        Me.Posicion.DataPropertyName = "PosicionApp"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N0"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.Posicion.DefaultCellStyle = DataGridViewCellStyle1
        Me.Posicion.HeaderText = "Posicion"
        Me.Posicion.Name = "Posicion"
        '
        'bsCategoriaMenu
        '
        Me.bsCategoriaMenu.DataMember = "Categorias_Menu"
        Me.bsCategoriaMenu.DataSource = Me.DtsCategoriaMenu1
        '
        'DtsCategoriaMenu1
        '
        Me.DtsCategoriaMenu1.DataSetName = "dtsCategoriaMenu"
        Me.DtsCategoriaMenu1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btGuardar
        '
        Me.btGuardar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btGuardar.Location = New System.Drawing.Point(204, 343)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btGuardar.TabIndex = 1
        Me.btGuardar.Text = "Guardar"
        Me.btGuardar.UseVisualStyleBackColor = True
        '
        'Categorias_MenuTableAdapter
        '
        Me.Categorias_MenuTableAdapter.ClearBeforeFill = True
        '
        'frmConfiguracionCategoria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(498, 378)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.grCategorias)
        Me.MinimumSize = New System.Drawing.Size(514, 417)
        Me.Name = "frmConfiguracionCategoria"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configuración Categoría"
        CType(Me.grCategorias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCategoriaMenu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsCategoriaMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grCategorias As System.Windows.Forms.DataGridView
    Friend WithEvents DtsCategoriaMenu1 As SIALIBEB.dtsCategoriaMenu
    Friend WithEvents bsCategoriaMenu As System.Windows.Forms.BindingSource
    Friend WithEvents Categorias_MenuTableAdapter As SIALIBEB.dtsCategoriaMenuTableAdapters.Categorias_MenuTableAdapter
    Friend WithEvents btGuardar As System.Windows.Forms.Button
    Friend WithEvents NombreDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Color As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posicion As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
