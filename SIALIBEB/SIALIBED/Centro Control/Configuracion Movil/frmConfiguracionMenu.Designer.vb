﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfiguracionMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.grMenu = New System.Windows.Forms.DataGridView
        Me.NombreMenuDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Color = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Posicion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bsMenu = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtsCategoriaMenu1 = New SIALIBEB.dtsCategoriaMenu
        Me.Menu_RestauranteTableAdapter = New SIALIBEB.dtsCategoriaMenuTableAdapters.Menu_RestauranteTableAdapter
        Me.btGuardar = New System.Windows.Forms.Button
        CType(Me.grMenu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsMenu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsCategoriaMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grMenu
        '
        Me.grMenu.AllowUserToAddRows = False
        Me.grMenu.AllowUserToDeleteRows = False
        Me.grMenu.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grMenu.AutoGenerateColumns = False
        Me.grMenu.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grMenu.BackgroundColor = System.Drawing.SystemColors.ControlLightLight
        Me.grMenu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grMenu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NombreMenuDataGridViewTextBoxColumn, Me.Color, Me.Posicion})
        Me.grMenu.DataSource = Me.bsMenu
        Me.grMenu.Location = New System.Drawing.Point(21, 12)
        Me.grMenu.Name = "grMenu"
        Me.grMenu.RowHeadersVisible = False
        Me.grMenu.Size = New System.Drawing.Size(477, 305)
        Me.grMenu.TabIndex = 1
        '
        'NombreMenuDataGridViewTextBoxColumn
        '
        Me.NombreMenuDataGridViewTextBoxColumn.DataPropertyName = "Nombre_Menu"
        Me.NombreMenuDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NombreMenuDataGridViewTextBoxColumn.Name = "NombreMenuDataGridViewTextBoxColumn"
        Me.NombreMenuDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Color
        '
        Me.Color.DataPropertyName = "Color"
        Me.Color.HeaderText = "Color"
        Me.Color.Name = "Color"
        Me.Color.ReadOnly = True
        Me.Color.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Posicion
        '
        Me.Posicion.DataPropertyName = "PosicionApp"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Posicion.DefaultCellStyle = DataGridViewCellStyle2
        Me.Posicion.HeaderText = "Posicion"
        Me.Posicion.Name = "Posicion"
        '
        'bsMenu
        '
        Me.bsMenu.DataMember = "Menu_Restaurante"
        Me.bsMenu.DataSource = Me.DtsCategoriaMenu1
        '
        'DtsCategoriaMenu1
        '
        Me.DtsCategoriaMenu1.DataSetName = "dtsCategoriaMenu"
        Me.DtsCategoriaMenu1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Menu_RestauranteTableAdapter
        '
        Me.Menu_RestauranteTableAdapter.ClearBeforeFill = True
        '
        'btGuardar
        '
        Me.btGuardar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btGuardar.Location = New System.Drawing.Point(215, 334)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btGuardar.TabIndex = 2
        Me.btGuardar.Text = "Guardar"
        Me.btGuardar.UseVisualStyleBackColor = True
        '
        'frmConfiguracionMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(519, 378)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.grMenu)
        Me.MinimumSize = New System.Drawing.Size(514, 417)
        Me.Name = "frmConfiguracionMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configuración Menú"
        CType(Me.grMenu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsMenu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsCategoriaMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grMenu As System.Windows.Forms.DataGridView
    Friend WithEvents DtsCategoriaMenu1 As SIALIBEB.dtsCategoriaMenu
    Friend WithEvents bsMenu As System.Windows.Forms.BindingSource
    Friend WithEvents Menu_RestauranteTableAdapter As SIALIBEB.dtsCategoriaMenuTableAdapters.Menu_RestauranteTableAdapter
    Friend WithEvents btGuardar As System.Windows.Forms.Button
    Friend WithEvents NombreMenuDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Color As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posicion As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
