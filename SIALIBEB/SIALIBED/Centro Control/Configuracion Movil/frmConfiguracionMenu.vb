﻿Public Class frmConfiguracionMenu
    Public pubIdCategoriaMenu As Integer = 0
    Dim colorseleccionado As String
    Dim pricambio As Boolean = False
    Sub spIniciarForm()
        spCargarCategorias()
    End Sub
    Sub spCargarCategorias()
        Dim cmd As New SqlClient.SqlCommand
        cmd.CommandText = "SELECT dbo.Menu_Restaurante.* FROM dbo.Categorias_Menu INNER JOIN dbo.Menu_Restaurante ON dbo.Categorias_Menu.Id = dbo.Menu_Restaurante.Id_Categoria WHERE(dbo.Menu_Restaurante.Id_Categoria = @IdCategoria)AND (dbo.Menu_Restaurante.deshabilitado = '0')"
        cmd.Parameters.AddWithValue("@IdCategoria", pubIdCategoriaMenu)
        cFunciones.spCargarDatos(cmd, DtsCategoriaMenu1.Menu_Restaurante, GetSetting("Seesoft", "Restaurante", "Conexion"))
    End Sub
    Private Function fnPermiteSalir() As Boolean
        If pricambio Then
            Dim frm As New frmMensaje("           ¿Desea guardar los cambios?", 1)
            Dim resultado As Integer = frm.ShowDialog
            If resultado = Windows.Forms.DialogResult.Yes Then

                spGuardar()
                Return pricambio
            ElseIf resultado = Windows.Forms.DialogResult.No Then
                Return True
            Else
                Return False
            End If
        End If

        Return True
    End Function
    Private Sub frmConfiguracionMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        spIniciarForm()
    End Sub
    Sub spColor()
        Dim MyDialog As New ColorDialog()
        MyDialog.AllowFullOpen = True
        MyDialog.ShowHelp = True
        If (MyDialog.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            Dim intColorDec As Integer = Convert.ToInt32(MyDialog.Color.ToArgb)
            colorseleccionado = Microsoft.VisualBasic.Conversion.Hex(intColorDec)
        End If
    End Sub

    Private Sub grMenu_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grMenu.CellDoubleClick
        If e.ColumnIndex = 1 Then
            spColor()
            If colorseleccionado <> Nothing Then
                bsMenu.Current("Color") = colorseleccionado
                bsMenu.EndEdit()
                pricambio = True
            End If
          
        End If
    End Sub

    Private Sub grMenu_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles grMenu.CellFormatting
        If e.ColumnIndex = 1 Then
            e.CellStyle.ForeColor = System.Drawing.ColorTranslator.FromHtml("#" & e.Value)
            e.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#" & e.Value)
        End If
    End Sub

    Private Sub grMenu_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles grMenu.DataError
        Try
            If grMenu.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name.Equals("PosicionApp") Then
                Dim valor As Double = grMenu.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue
                grMenu.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = valor
                pricambio = True
            Else
                MsgBox("Datos invalidos.", 2)
                e.Cancel = False
            End If

        Catch ex As Exception
            MsgBox("Datos invalidos.", 2)
            e.Cancel = False
        End Try
    End Sub
    Sub spGuardar()
        Menu_RestauranteTableAdapter.Connection.ConnectionString = GetSetting("Seesoft", "Restaurante", "Conexion")
        If bsMenu.Count > 0 Then
            For i As Integer = 0 To DtsCategoriaMenu1.Menu_Restaurante.Count - 1
                For j As Integer = 0 To bsMenu.Count - 1
                    bsMenu.Position = j
                    If DtsCategoriaMenu1.Menu_Restaurante(i).Id_Menu <> bsMenu.Current("Id_Menu") Then
                        If DtsCategoriaMenu1.Menu_Restaurante(i).PosicionApp = bsMenu.Current("PosicionApp") Then
                            spMensaje("Hay posiciones iguales.Debe cambiarlas.", 2)
                            pricambio = False
                            Exit Sub
                        End If
                    End If
                Next
            Next
            Try
                Menu_RestauranteTableAdapter.Update(DtsCategoriaMenu1.Menu_Restaurante)
                spMensaje("La información se guardó exitosamente.", 2)
                pricambio = False
                Me.Close()
            Catch ex As Exception
                MsgBox(ex.ToString, 1)
            End Try
        End If
    End Sub

    Private Sub btGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGuardar.Click

        spGuardar()

    End Sub

    Private Sub frmConfiguracionMenu_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Not fnPermiteSalir() Then
            e.Cancel = True
        End If
    End Sub

    Private Sub grMenu_CellValidating(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles grMenu.CellValidating
        If e.ColumnIndex = 2 Then
            pricambio = True
        End If
    End Sub
End Class