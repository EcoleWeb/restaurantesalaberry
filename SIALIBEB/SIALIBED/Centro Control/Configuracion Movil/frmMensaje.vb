﻿Public Class frmMensaje

    Public Sub New(ByVal _mensaje As String, ByVal _estilo As Integer)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        lbMensaje.Text = _mensaje

        Panel1.Visible = _estilo = 1 'Si, No, Cancelar
        Panel2.Visible = _estilo = 2 'OK 


    End Sub
    Private Sub frmMensaje_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btSi.Focus()
        btSi.TabIndex = 0
    End Sub

    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        DialogResult = Windows.Forms.DialogResult.OK
        Close()
    End Sub

    Private Sub btSi_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSi.Click
        DialogResult = Windows.Forms.DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub btNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btNo.Click
        DialogResult = Windows.Forms.DialogResult.No
        Me.Close()
    End Sub

    Private Sub btNo_KeyDown_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles btNo.KeyDown
        If e.KeyCode = Keys.N Then
            DialogResult = Windows.Forms.DialogResult.No
            Me.Close()
        End If

        If e.KeyCode = Keys.S Then
            DialogResult = Windows.Forms.DialogResult.Yes
            Me.Close()
        End If

        If e.KeyCode = Keys.Escape Then
            DialogResult = Windows.Forms.DialogResult.Cancel
            Me.Close()
        End If
    End Sub
End Class