
Imports System.Drawing.Printing

Public Class anulacion

#Region "variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Public cedula As String
    Private usuario As String
    Public CuentaContable As String
    Public Notas As String
#End Region

    Private Sub cbkCortesia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbkCortesia.CheckedChanged
        If Me.cbkCortesia.Checked = False Then
            txtCuenta.Enabled = True
        Else
            txtCuenta.Enabled = False
            txtCuenta.Text = ""
        End If
    End Sub

    Private Sub txtCuenta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCuenta.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim Fbuscador As New buscador
            Fbuscador.cantidad = 3
            Fbuscador.bd = "Contabilidad"
            Fbuscador.tabla = "CuentaContable"
            Fbuscador.lblEncabezado.Text = "          Buscador de Cuentas"
            Fbuscador.TituloModulo.Text = "Cuentas Contables"
            Fbuscador.consulta = "Select CuentaContable, Descripcion, Tipo, CuentaMadre from CuentaContable where Movimiento=1"
            Fbuscador.ShowDialog()
            txtCuenta.Text = Fbuscador.cuenta
            CuentaContable = Fbuscador.cuenta
            Fbuscador.Dispose()
        End If
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 1 : validar()
            Case 2 : cerrar()
        End Select
    End Sub

    Private Sub Validar()
        If Trim(Label2.Text) = vbNullString Then
            MessageBox.Show("Debe ingresar un usuario para realizar la anulación", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
        ElseIf Trim(TextBox1.Text) = vbNullString Then
            MessageBox.Show("Debe ingresar un motivo para realizar la anulación", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
        ElseIf cbkCortesia.Checked = False And Trim(txtCuenta.Text) = vbNullString Then
            MessageBox.Show("Debe ingresar una cuenta contable para realizar la anulación", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            Notas = TextBox1.Text
            'Me.cbkCortesia.Checked = Not Me.cbkCortesia.Checked
            Close()
        End If
    End Sub

    Private Sub TextBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox2.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox2.Text & "'")
            usuario = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")
            If usuario <> "" Then
                Me.Label2.Text = usuario
            Else
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub cerrar()
        usuario = ""
        cedula = ""
        CuentaContable = ""
        If (MsgBox("Esta seguro que desea concelar esta operación", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
            Me.Close()
        End If
    End Sub

    Private Sub anulacion_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub anulacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar
        cedula = ""
        CuentaContable = ""
        Notas = ""
        TextBox2.Focus()
    End Sub
End Class
