Public Class frmComisionistaPuntoVenta
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents ButtonImprimir As System.Windows.Forms.Button
    Friend WithEvents ButtonDeshabilitar As System.Windows.Forms.Button
    Friend WithEvents ButtonEditar As System.Windows.Forms.Button
    Friend WithEvents ButtonBuscar As System.Windows.Forms.Button
    Friend WithEvents ButtonRegistrar As System.Windows.Forms.Button
    Friend WithEvents ButtonNuevo As System.Windows.Forms.Button
    Friend WithEvents ImageList3 As System.Windows.Forms.ImageList
    Friend WithEvents pComisionista As System.Windows.Forms.Panel
    Friend WithEvents txCedula As System.Windows.Forms.TextBox
    Friend WithEvents lbCedula As System.Windows.Forms.Label
    Friend WithEvents txNombre As System.Windows.Forms.TextBox
    Friend WithEvents lbNombre As System.Windows.Forms.Label
    Friend WithEvents txTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txCelular As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbTipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chAgencia As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbAgencia As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblDeshabilitado As System.Windows.Forms.Label
    Friend WithEvents txCorreo As System.Windows.Forms.TextBox
    Friend WithEvents DtsComisionista1 As SIALIBEB.dtsComisionista
    Friend WithEvents txObservacion As System.Windows.Forms.TextBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmComisionistaPuntoVenta))
        Me.ButtonImprimir = New System.Windows.Forms.Button()
        Me.ImageList3 = New System.Windows.Forms.ImageList(Me.components)
        Me.ButtonDeshabilitar = New System.Windows.Forms.Button()
        Me.ButtonEditar = New System.Windows.Forms.Button()
        Me.ButtonBuscar = New System.Windows.Forms.Button()
        Me.ButtonRegistrar = New System.Windows.Forms.Button()
        Me.ButtonNuevo = New System.Windows.Forms.Button()
        Me.pComisionista = New System.Windows.Forms.Panel()
        Me.txObservacion = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txCorreo = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbAgencia = New System.Windows.Forms.ComboBox()
        Me.chAgencia = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbTipo = New System.Windows.Forms.ComboBox()
        Me.txCelular = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txTelefono = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txCedula = New System.Windows.Forms.TextBox()
        Me.lbCedula = New System.Windows.Forms.Label()
        Me.txNombre = New System.Windows.Forms.TextBox()
        Me.lbNombre = New System.Windows.Forms.Label()
        Me.lblDeshabilitado = New System.Windows.Forms.Label()
        Me.DtsComisionista1 = New SIALIBEB.dtsComisionista()
        Me.pComisionista.SuspendLayout()
        CType(Me.DtsComisionista1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButtonImprimir
        '
        Me.ButtonImprimir.Enabled = False
        Me.ButtonImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ButtonImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ButtonImprimir.ImageIndex = 27
        Me.ButtonImprimir.ImageList = Me.ImageList3
        Me.ButtonImprimir.Location = New System.Drawing.Point(408, 392)
        Me.ButtonImprimir.Name = "ButtonImprimir"
        Me.ButtonImprimir.Size = New System.Drawing.Size(72, 56)
        Me.ButtonImprimir.TabIndex = 11
        Me.ButtonImprimir.Text = "Imprimir"
        Me.ButtonImprimir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ButtonImprimir.Visible = False
        '
        'ImageList3
        '
        Me.ImageList3.ImageStream = CType(resources.GetObject("ImageList3.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList3.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList3.Images.SetKeyName(0, "")
        Me.ImageList3.Images.SetKeyName(1, "")
        Me.ImageList3.Images.SetKeyName(2, "")
        Me.ImageList3.Images.SetKeyName(3, "")
        Me.ImageList3.Images.SetKeyName(4, "")
        Me.ImageList3.Images.SetKeyName(5, "")
        Me.ImageList3.Images.SetKeyName(6, "")
        Me.ImageList3.Images.SetKeyName(7, "")
        Me.ImageList3.Images.SetKeyName(8, "")
        Me.ImageList3.Images.SetKeyName(9, "")
        Me.ImageList3.Images.SetKeyName(10, "")
        Me.ImageList3.Images.SetKeyName(11, "")
        Me.ImageList3.Images.SetKeyName(12, "")
        Me.ImageList3.Images.SetKeyName(13, "")
        Me.ImageList3.Images.SetKeyName(14, "")
        Me.ImageList3.Images.SetKeyName(15, "")
        Me.ImageList3.Images.SetKeyName(16, "")
        Me.ImageList3.Images.SetKeyName(17, "")
        Me.ImageList3.Images.SetKeyName(18, "")
        Me.ImageList3.Images.SetKeyName(19, "")
        Me.ImageList3.Images.SetKeyName(20, "")
        Me.ImageList3.Images.SetKeyName(21, "")
        Me.ImageList3.Images.SetKeyName(22, "")
        Me.ImageList3.Images.SetKeyName(23, "")
        Me.ImageList3.Images.SetKeyName(24, "")
        Me.ImageList3.Images.SetKeyName(25, "")
        Me.ImageList3.Images.SetKeyName(26, "")
        Me.ImageList3.Images.SetKeyName(27, "")
        '
        'ButtonDeshabilitar
        '
        Me.ButtonDeshabilitar.Enabled = False
        Me.ButtonDeshabilitar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ButtonDeshabilitar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ButtonDeshabilitar.ImageIndex = 26
        Me.ButtonDeshabilitar.ImageList = Me.ImageList3
        Me.ButtonDeshabilitar.Location = New System.Drawing.Point(328, 392)
        Me.ButtonDeshabilitar.Name = "ButtonDeshabilitar"
        Me.ButtonDeshabilitar.Size = New System.Drawing.Size(72, 56)
        Me.ButtonDeshabilitar.TabIndex = 10
        Me.ButtonDeshabilitar.Text = "Deshabilitar"
        Me.ButtonDeshabilitar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ButtonEditar
        '
        Me.ButtonEditar.Enabled = False
        Me.ButtonEditar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ButtonEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ButtonEditar.ImageIndex = 24
        Me.ButtonEditar.ImageList = Me.ImageList3
        Me.ButtonEditar.Location = New System.Drawing.Point(248, 392)
        Me.ButtonEditar.Name = "ButtonEditar"
        Me.ButtonEditar.Size = New System.Drawing.Size(72, 56)
        Me.ButtonEditar.TabIndex = 9
        Me.ButtonEditar.Text = "Editar"
        Me.ButtonEditar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ButtonBuscar
        '
        Me.ButtonBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ButtonBuscar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ButtonBuscar.ImageIndex = 21
        Me.ButtonBuscar.ImageList = Me.ImageList3
        Me.ButtonBuscar.Location = New System.Drawing.Point(168, 392)
        Me.ButtonBuscar.Name = "ButtonBuscar"
        Me.ButtonBuscar.Size = New System.Drawing.Size(72, 56)
        Me.ButtonBuscar.TabIndex = 8
        Me.ButtonBuscar.Text = "Buscar"
        Me.ButtonBuscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ButtonRegistrar
        '
        Me.ButtonRegistrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ButtonRegistrar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ButtonRegistrar.ImageIndex = 23
        Me.ButtonRegistrar.ImageList = Me.ImageList3
        Me.ButtonRegistrar.Location = New System.Drawing.Point(88, 392)
        Me.ButtonRegistrar.Name = "ButtonRegistrar"
        Me.ButtonRegistrar.Size = New System.Drawing.Size(72, 56)
        Me.ButtonRegistrar.TabIndex = 7
        Me.ButtonRegistrar.Text = "Registrar"
        Me.ButtonRegistrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ButtonNuevo
        '
        Me.ButtonNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ButtonNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ButtonNuevo.ImageIndex = 25
        Me.ButtonNuevo.ImageList = Me.ImageList3
        Me.ButtonNuevo.Location = New System.Drawing.Point(8, 392)
        Me.ButtonNuevo.Name = "ButtonNuevo"
        Me.ButtonNuevo.Size = New System.Drawing.Size(72, 56)
        Me.ButtonNuevo.TabIndex = 6
        Me.ButtonNuevo.Text = "Nuevo"
        Me.ButtonNuevo.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'pComisionista
        '
        Me.pComisionista.Controls.Add(Me.txObservacion)
        Me.pComisionista.Controls.Add(Me.Label6)
        Me.pComisionista.Controls.Add(Me.txCorreo)
        Me.pComisionista.Controls.Add(Me.Label5)
        Me.pComisionista.Controls.Add(Me.Label4)
        Me.pComisionista.Controls.Add(Me.cbAgencia)
        Me.pComisionista.Controls.Add(Me.chAgencia)
        Me.pComisionista.Controls.Add(Me.Label3)
        Me.pComisionista.Controls.Add(Me.cbTipo)
        Me.pComisionista.Controls.Add(Me.txCelular)
        Me.pComisionista.Controls.Add(Me.Label2)
        Me.pComisionista.Controls.Add(Me.txTelefono)
        Me.pComisionista.Controls.Add(Me.Label1)
        Me.pComisionista.Controls.Add(Me.txCedula)
        Me.pComisionista.Controls.Add(Me.lbCedula)
        Me.pComisionista.Controls.Add(Me.txNombre)
        Me.pComisionista.Controls.Add(Me.lbNombre)
        Me.pComisionista.Location = New System.Drawing.Point(8, 8)
        Me.pComisionista.Name = "pComisionista"
        Me.pComisionista.Size = New System.Drawing.Size(472, 368)
        Me.pComisionista.TabIndex = 12
        '
        'txObservacion
        '
        Me.txObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txObservacion.Location = New System.Drawing.Point(96, 328)
        Me.txObservacion.Multiline = True
        Me.txObservacion.Name = "txObservacion"
        Me.txObservacion.Size = New System.Drawing.Size(360, 32)
        Me.txObservacion.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(8, 328)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 16)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Observaciones :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txCorreo
        '
        Me.txCorreo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCorreo.Location = New System.Drawing.Point(96, 288)
        Me.txCorreo.Name = "txCorreo"
        Me.txCorreo.Size = New System.Drawing.Size(360, 20)
        Me.txCorreo.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(24, 288)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 16)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Correo :"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(24, 240)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 16)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Agencia :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cbAgencia
        '
        Me.cbAgencia.DataSource = Me.DtsComisionista1
        Me.cbAgencia.DisplayMember = "Cliente.Nombre"
        Me.cbAgencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbAgencia.Enabled = False
        Me.cbAgencia.Location = New System.Drawing.Point(96, 240)
        Me.cbAgencia.Name = "cbAgencia"
        Me.cbAgencia.Size = New System.Drawing.Size(360, 21)
        Me.cbAgencia.TabIndex = 5
        Me.cbAgencia.ValueMember = "Cliente.Id"
        '
        'chAgencia
        '
        Me.chAgencia.Location = New System.Drawing.Point(96, 208)
        Me.chAgencia.Name = "chAgencia"
        Me.chAgencia.Size = New System.Drawing.Size(104, 24)
        Me.chAgencia.TabIndex = 21
        Me.chAgencia.Text = "Agencia"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(24, 168)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 16)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Tipo :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cbTipo
        '
        Me.cbTipo.DataSource = Me.DtsComisionista1
        Me.cbTipo.DisplayMember = "tb_H_TipoComisionista.Nombre"
        Me.cbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTipo.Location = New System.Drawing.Point(96, 168)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Size = New System.Drawing.Size(360, 21)
        Me.cbTipo.TabIndex = 4
        Me.cbTipo.ValueMember = "tb_H_TipoComisionista.Id"
        '
        'txCelular
        '
        Me.txCelular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCelular.Location = New System.Drawing.Point(308, 120)
        Me.txCelular.Name = "txCelular"
        Me.txCelular.Size = New System.Drawing.Size(148, 20)
        Me.txCelular.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(248, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Celular :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txTelefono
        '
        Me.txTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txTelefono.Location = New System.Drawing.Point(96, 120)
        Me.txTelefono.Name = "txTelefono"
        Me.txTelefono.Size = New System.Drawing.Size(148, 20)
        Me.txTelefono.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(24, 120)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 16)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Telefono :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txCedula
        '
        Me.txCedula.Location = New System.Drawing.Point(96, 24)
        Me.txCedula.Name = "txCedula"
        Me.txCedula.Size = New System.Drawing.Size(120, 20)
        Me.txCedula.TabIndex = 0
        '
        'lbCedula
        '
        Me.lbCedula.Location = New System.Drawing.Point(8, 24)
        Me.lbCedula.Name = "lbCedula"
        Me.lbCedula.Size = New System.Drawing.Size(72, 23)
        Me.lbCedula.TabIndex = 14
        Me.lbCedula.Text = "Cedula : "
        Me.lbCedula.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txNombre
        '
        Me.txNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txNombre.Location = New System.Drawing.Point(96, 72)
        Me.txNombre.Name = "txNombre"
        Me.txNombre.Size = New System.Drawing.Size(360, 20)
        Me.txNombre.TabIndex = 1
        '
        'lbNombre
        '
        Me.lbNombre.Location = New System.Drawing.Point(24, 72)
        Me.lbNombre.Name = "lbNombre"
        Me.lbNombre.Size = New System.Drawing.Size(56, 16)
        Me.lbNombre.TabIndex = 12
        Me.lbNombre.Text = "Nombre :"
        Me.lbNombre.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblDeshabilitado
        '
        Me.lblDeshabilitado.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeshabilitado.ForeColor = System.Drawing.Color.Red
        Me.lblDeshabilitado.Location = New System.Drawing.Point(240, 24)
        Me.lblDeshabilitado.Name = "lblDeshabilitado"
        Me.lblDeshabilitado.Size = New System.Drawing.Size(224, 32)
        Me.lblDeshabilitado.TabIndex = 28
        Me.lblDeshabilitado.Text = "DESHABILITADO"
        Me.lblDeshabilitado.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.lblDeshabilitado.Visible = False
        '
        'DtsComisionista1
        '
        Me.DtsComisionista1.DataSetName = "dtsComisionista"
        Me.DtsComisionista1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmComisionistaPuntoVenta
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(488, 450)
        Me.Controls.Add(Me.lblDeshabilitado)
        Me.Controls.Add(Me.pComisionista)
        Me.Controls.Add(Me.ButtonImprimir)
        Me.Controls.Add(Me.ButtonDeshabilitar)
        Me.Controls.Add(Me.ButtonEditar)
        Me.Controls.Add(Me.ButtonBuscar)
        Me.Controls.Add(Me.ButtonRegistrar)
        Me.Controls.Add(Me.ButtonNuevo)
        Me.Name = "frmComisionistaPuntoVenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisionista Punto de venta"
        Me.pComisionista.ResumeLayout(False)
        Me.pComisionista.PerformLayout()
        CType(Me.DtsComisionista1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim nuevo As Boolean = True
    Dim Id As Integer
    Dim tabla As New DataTable

    Private Sub frmComisionistaPuntoVenta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        spCargarTipo()
        spCargarAgencia()
    End Sub
    Private Sub spCargarAgencia()
        Dim sql As String
        Dim Cx As New Conexion
        sql = "Select * from Cliente"
        cFunciones.Llenar_Tabla_Generico(sql, Me.DtsComisionista1.Cliente, GetSetting("SeeSoft", "Hotel", "Conexion"))
    End Sub
    Private Sub spCargarTipo()
        Dim sql As String
        Dim Cx As New Conexion
        sql = "Select * from tb_FD_TipoComisionista where Estado = 0"
        cFunciones.Llenar_Tabla_Generico(sql, Me.DtsComisionista1.tb_H_TipoComisionista, GetSetting("SeeSoft", "Hotel", "Conexion"))
    End Sub
    Sub nuevoComisionista()
        nuevo = True
        Me.txCedula.Text = ""
        Me.txNombre.Text = ""
        Me.txTelefono.Text = ""
        Me.txCelular.Text = ""
        Me.cbTipo.SelectedIndex = 0
        Me.chAgencia.Checked = False
        Me.txCorreo.Text = ""
        Me.txObservacion.Text = ""
        Me.lblDeshabilitado.Visible = False
    End Sub

    Private Sub ButtonNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNuevo.Click
        nuevoComisionista()
        Id = 0
        Me.ButtonDeshabilitar.Enabled = False
        Me.ButtonImprimir.Enabled = False
        Me.ButtonRegistrar.Enabled = True
        Me.ButtonEditar.Enabled = False
        Me.pComisionista.Enabled = True
        Me.txCedula.Focus()

    End Sub
    Sub guardarComisionista()
        Try
            Dim Cliente As Integer
            If nuevo Then
                Dim cx As New Conexion
                If Me.chAgencia.Checked Then
                    Cliente = Me.cbAgencia.SelectedValue
                Else
                    Cliente = 0
                End If
                Dim mensaje As String = cx.SlqExecute(cx.Conectar("Hotel"), "INSERT INTO tb_FD_Comisionista(Cedula,Nombre,Telefono,Celular,Tipo,Correo,IdCliente,Observaciones) VALUES " & _
                                    "('" & Me.txCedula.Text & "', '" & Me.txNombre.Text & "', '" & Me.txTelefono.Text & "', '" & Me.txCelular.Text & "', '" & Me.cbTipo.SelectedValue & "', '" & Me.txCorreo.Text & "', '" & Cliente & "', '" & Me.txObservacion.Text & "')")
                cx.DesConectar(cx.sQlconexion)

            Else
              
                If Me.chAgencia.Checked Then
                    Cliente = Me.cbAgencia.SelectedValue
                Else
                    Cliente = 0
                End If
                Dim sql As String = "UPDATE tb_FD_Comisionista SET Cedula = '" & Me.txCedula.Text & "', Nombre = '" & Me.txNombre.Text & "' ," & _
                                                                   " Telefono = '" & Me.txTelefono.Text & "', Celular = '" & Me.txCelular.Text & "' ," & _
                                                                   " Tipo = '" & Me.cbTipo.SelectedValue & "', Correo = '" & Me.txCorreo.Text & "' ," & _
                                                                   " IdCliente = '" & Cliente & "', Observaciones = '" & Me.txObservacion.Text & "' " & _
                                                                   " where Id = '" & Id & "'"
                Dim cx As New Conexion
                Dim mensaje As String = cx.SlqExecute(cx.Conectar("Hotel"), sql)
                cx.DesConectar(cx.sQlconexion)

            End If
        Catch ex As Exception
            MessageBox.Show("Error" & ex.ToString, "Error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Function fnValidarCampos() As Boolean
        Dim cx As New Conexion
        Dim Resultado As String

        Resultado = cx.SlqExecuteScalar(cx.Conectar("Hotel"), "select count(*) from tb_FD_Comisionista where Cedula = '" & Me.txCedula.Text & "' and Id <> '" & Id & "'")
        If CDbl(Resultado) > 0 Then
            Me.txCedula.BackColor = Color.Bisque
            MsgBox("Los datos ya existen.", MsgBoxStyle.Information, "Atenci�n...")
            Me.txCedula.Focus()
            Return False
        Else
            Me.txCedula.BackColor = Color.White
        End If
        If Not Me.txNombre.Text = "" Then
            If Not Replace(Me.txNombre.Text, " ", "").Length > 0 Then
                Me.txNombre.BackColor = Color.Bisque
                MsgBox("Por Favor, completar los campos obligatorios.", MsgBoxStyle.Information, "Atenci�n...")
                Me.txNombre.Focus()
                Return False
            Else
                Me.txNombre.BackColor = Color.White
            End If
        Else
            Me.txNombre.BackColor = Color.Bisque
            MsgBox("Por Favor, completar los campos obligatorios.", MsgBoxStyle.Information, "Atenci�n...")
            Me.txNombre.Focus()
            Return False
        End If
      

        cx.DesConectar(cx.sQlconexion)
        Return True
    End Function
    Private Sub ButtonRegistrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRegistrar.Click
        Try
            If Me.ButtonRegistrar.Text = "Registrar" Then
                If fnValidarCampos() Then
                    guardarComisionista()
                Else
                    Exit Sub
                End If

                MessageBox.Show("La informaci�n se guard� exitosamente.", "Exito...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                nuevo = True
                Me.ButtonNuevo.Enabled = True
                Me.ButtonNuevo_Click(sender, e)
                Me.ButtonRegistrar.Text = "Registrar"
                Me.ButtonImprimir.Enabled = False
                Me.ButtonBuscar.Enabled = True
            Else
                nuevo = False
                If fnValidarCampos() Then
                    guardarComisionista()
                Else
                    Exit Sub
                End If

                MessageBox.Show("La informaci�n se actualizo exitosamente.", "Exito...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.ButtonNuevo.Enabled = True
                Me.ButtonNuevo_Click(sender, e)
                Me.ButtonRegistrar.Text = "Registrar"
                Me.ButtonEditar.Text = "Editar"
                Me.ButtonImprimir.Enabled = False
                Me.ButtonBuscar.Enabled = True
            End If
        Catch ex As Exception
            MessageBox.Show("Error al actualizar datos" & ex.ToString, "Error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ButtonBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBuscar.Click
        buscarComisionista()
    End Sub
    Function buscarComisionista() As Boolean
        Try
            Me.nuevo = False
            Me.pComisionista.Enabled = False
            Dim cx As New cFunciones
            Dim Id1 As String = cx.BuscarDatos("SELECT cedula, Nombre FROM tb_FD_Comisionista", "Nombre", "Buscar Comisionista", GetSetting("SeeSoft", "Hotel", "Conexion"))
            If Id1 = Nothing Then
                Return False
            Else
                Dim dtComisionista As New DataTable
                cFunciones.Llenar_Tabla_Generico("SELECT * FROM tb_FD_Comisionista WHERE cedula = '" & Id1 & "'", dtComisionista, GetSetting("SeeSoft", "Hotel", "Conexion"))
                If dtComisionista.Rows.Count > 0 Then
                    Id = dtComisionista.Rows(0).Item("Id")
                    Me.txCedula.Text = dtComisionista.Rows(0).Item("cedula")
                    Me.txNombre.Text = dtComisionista.Rows(0).Item("Nombre")
                    Me.txTelefono.Text = dtComisionista.Rows(0).Item("Telefono")
                    Me.txCelular.Text = dtComisionista.Rows(0).Item("Celular")
                    Me.cbTipo.SelectedValue = dtComisionista.Rows(0).Item("Tipo")
                    Me.txCorreo.Text = dtComisionista.Rows(0).Item("Correo")
                    If dtComisionista.Rows(0).Item("IdCliente") <> "0" Then
                        Me.chAgencia.Checked = True
                        Me.cbAgencia.SelectedValue = dtComisionista.Rows(0).Item("IdCliente")
                    Else
                        Me.chAgencia.Checked = False
                    End If
                    Me.txObservacion.Text = dtComisionista.Rows(0).Item("Observaciones")

                    If dtComisionista.Rows(0).Item("Estado") Then
                        Me.lblDeshabilitado.Visible = True
                        Me.pComisionista.Enabled = False
                    Else
                        Me.pComisionista.Enabled = False
                    End If

                    If dtComisionista.Rows(0).Item("Estado") Then
                        Me.ButtonEditar.Enabled = False
                        Me.ButtonDeshabilitar.Enabled = True
                        Me.ButtonDeshabilitar.Text = "HABILITAR"
                        Me.ButtonImprimir.Enabled = False
                        Me.ButtonRegistrar.Enabled = False
                        Me.lblDeshabilitado.Visible = True
                    Else
                        Me.ButtonEditar.Enabled = True
                        Me.ButtonImprimir.Enabled = True
                        Me.ButtonRegistrar.Enabled = False
                        Me.ButtonDeshabilitar.Text = "Deshabilitar"
                        Me.ButtonDeshabilitar.Enabled = True
                        Me.lblDeshabilitado.Visible = False
                    End If
                    Return True
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error al actualizar datos" & ex.ToString, "Error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Sub ButtonEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEditar.Click
        If Me.ButtonEditar.Text = "Editar" Then
            Me.ButtonNuevo.Enabled = False
            Me.ButtonImprimir.Enabled = False
            Me.ButtonDeshabilitar.Enabled = False
            Me.ButtonEditar.Text = "Cancelar"
            Me.ButtonRegistrar.Text = "Actualizar"
            Me.ButtonRegistrar.Enabled = True
            Me.pComisionista.Enabled = True
            Me.ButtonBuscar.Enabled = False
        Else
            Me.ButtonNuevo.Enabled = True
            Me.ButtonEditar.Text = "Editar"
            Me.ButtonDeshabilitar.Enabled = True
            Me.ButtonRegistrar.Text = "Registrar"
            Me.ButtonRegistrar.Enabled = False
            Me.ButtonImprimir.Enabled = True
            Me.pComisionista.Enabled = False
            Me.ButtonBuscar.Enabled = True
        End If
    End Sub

    Private Sub ButtonDeshabilitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDeshabilitar.Click
        Try
            Dim sql As New SqlClient.SqlCommand
            If Me.ButtonDeshabilitar.Text = "Deshabilitar" Then
                sql.CommandText = "update tb_FD_Comisionista SET Estado = 1 where id= @Id"
                sql.Parameters.AddWithValue("@Id", Id)
                cFunciones.spEjecutar(sql, GetSetting("SeeSoft", "Hotel", "Conexion"))
                MessageBox.Show("La informaci�n se actualizo exitosamente.", "Accion Completada", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.lblDeshabilitado.Visible = True
                Me.ButtonDeshabilitar.Text = "HABILITAR"
                Me.ButtonEditar.Enabled = False
                Me.ButtonRegistrar.Enabled = False
                Me.ButtonImprimir.Enabled = False
                Me.pComisionista.Enabled = False
            Else
                sql.CommandText = "update tb_FD_Comisionista SET Estado = 0 where id= @Id"
                sql.Parameters.AddWithValue("@Id", Id)
                cFunciones.spEjecutar(sql, GetSetting("SeeSoft", "Hotel", "Conexion"))
                MessageBox.Show("La informaci�n se actualizo exitosamente.", "Accion Completada", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.lblDeshabilitado.Visible = False
                Me.ButtonDeshabilitar.Text = "Deshabilitar"
                Me.ButtonEditar.Enabled = True
                Me.ButtonRegistrar.Enabled = False
                Me.ButtonImprimir.Enabled = True
                Me.pComisionista.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show("Error" & ex.ToString, "Error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub chAgencia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chAgencia.CheckedChanged
        If Me.chAgencia.Checked Then
            Me.cbAgencia.Enabled = True
        Else
            Me.cbAgencia.Enabled = False
        End If

    End Sub

   
    Private Sub txCedula_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txCedula.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.txNombre.Focus()
        End If
    End Sub

    Private Sub txNombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txNombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.txTelefono.Focus()
        End If
    End Sub

    Private Sub txTelefono_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txTelefono.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.txCelular.Focus()
        End If
    End Sub

    Private Sub txCelular_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txCelular.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.cbTipo.Focus()
        End If
    End Sub

    Private Sub cbTipo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbTipo.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.chAgencia.Focus()
        End If
    End Sub

    Private Sub chAgencia_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles chAgencia.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.cbAgencia.Focus()
        End If
    End Sub

    Private Sub cbAgencia_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbAgencia.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.txCorreo.Focus()
        End If
    End Sub

    Private Sub txCorreo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txCorreo.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.txObservacion.Focus()
        End If
    End Sub
End Class
