﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComision
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmComision))
        Me.txtComisionista = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txIdentificacion = New System.Windows.Forms.TextBox()
        Me.txNombreGuia = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ToolBar2 = New System.Windows.Forms.ToolBar()
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton()
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.DatasetCliente1 = New SIALIBEB.DatasetCliente()
        Me.lbMensaje = New System.Windows.Forms.Label()
        CType(Me.DatasetCliente1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtComisionista
        '
        Me.txtComisionista.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComisionista.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtComisionista.Location = New System.Drawing.Point(170, 67)
        Me.txtComisionista.Name = "txtComisionista"
        Me.txtComisionista.Size = New System.Drawing.Size(320, 26)
        Me.txtComisionista.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(166, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(324, 20)
        Me.Label1.TabIndex = 215
        Me.Label1.Text = "Nombre Cliente"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Blue
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(1, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(489, 25)
        Me.Label12.TabIndex = 216
        Me.Label12.Text = "Registros de Comisionista"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(23, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(126, 20)
        Me.Label2.TabIndex = 218
        Me.Label2.Text = "Identificación"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txIdentificacion
        '
        Me.txIdentificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txIdentificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txIdentificacion.Location = New System.Drawing.Point(12, 67)
        Me.txIdentificacion.Name = "txIdentificacion"
        Me.txIdentificacion.Size = New System.Drawing.Size(142, 26)
        Me.txIdentificacion.TabIndex = 0
        '
        'txNombreGuia
        '
        Me.txNombreGuia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txNombreGuia.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txNombreGuia.Location = New System.Drawing.Point(12, 120)
        Me.txNombreGuia.Name = "txNombreGuia"
        Me.txNombreGuia.Size = New System.Drawing.Size(478, 26)
        Me.txNombreGuia.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(12, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(478, 20)
        Me.Label3.TabIndex = 220
        Me.Label3.Text = "Nombre Guia"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ToolBar2
        '
        Me.ToolBar2.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar2.AutoSize = False
        Me.ToolBar2.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarEditar, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarButton1, Me.ToolBarCerrar})
        Me.ToolBar2.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar2.DropDownArrows = True
        Me.ToolBar2.ImageList = Me.ImageList
        Me.ToolBar2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar2.Location = New System.Drawing.Point(0, 184)
        Me.ToolBar2.Name = "ToolBar2"
        Me.ToolBar2.ShowToolTips = True
        Me.ToolBar2.Size = New System.Drawing.Size(510, 57)
        Me.ToolBar2.TabIndex = 3
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        Me.ToolBarNuevo.Text = "Nuevo"
        '
        'ToolBarEditar
        '
        Me.ToolBarEditar.Enabled = False
        Me.ToolBarEditar.ImageIndex = 1
        Me.ToolBarEditar.Name = "ToolBarEditar"
        Me.ToolBarEditar.Text = "Editar"
        Me.ToolBarEditar.Visible = False
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        Me.ToolBarBuscar.ImageIndex = 2
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        Me.ToolBarBuscar.Text = "Buscar"
        Me.ToolBarBuscar.Visible = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.ImageIndex = 3
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        Me.ToolBarRegistrar.Text = "Registrar"
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.ImageIndex = 4
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        Me.ToolBarEliminar.Text = "Anular"
        Me.ToolBarEliminar.Visible = False
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        Me.ToolBarImprimir.ImageIndex = 8
        Me.ToolBarImprimir.Name = "ToolBarImprimir"
        Me.ToolBarImprimir.Text = "Imprimir"
        Me.ToolBarImprimir.Visible = False
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.Enabled = False
        Me.ToolBarButton1.ImageIndex = 9
        Me.ToolBarButton1.Name = "ToolBarButton1"
        Me.ToolBarButton1.Text = "Teclado"
        Me.ToolBarButton1.Visible = False
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 10
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "")
        Me.ImageList.Images.SetKeyName(9, "teclado1.gif")
        Me.ImageList.Images.SetKeyName(10, "")
        '
        'DatasetCliente1
        '
        Me.DatasetCliente1.DataSetName = "DatasetCliente"
        Me.DatasetCliente1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lbMensaje
        '
        Me.lbMensaje.AutoSize = True
        Me.lbMensaje.ForeColor = System.Drawing.Color.Red
        Me.lbMensaje.Location = New System.Drawing.Point(145, 157)
        Me.lbMensaje.Name = "lbMensaje"
        Me.lbMensaje.Size = New System.Drawing.Size(217, 13)
        Me.lbMensaje.TabIndex = 221
        Me.lbMensaje.Text = "Por Favor, complete los campos obligatorios."
        Me.lbMensaje.Visible = False
        '
        'frmComision
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(510, 241)
        Me.Controls.Add(Me.lbMensaje)
        Me.Controls.Add(Me.ToolBar2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txNombreGuia)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txIdentificacion)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtComisionista)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(518, 268)
        Me.MinimumSize = New System.Drawing.Size(518, 268)
        Me.Name = "frmComision"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisión"
        CType(Me.DatasetCliente1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtComisionista As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txIdentificacion As System.Windows.Forms.TextBox
    Friend WithEvents txNombreGuia As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents ToolBar2 As System.Windows.Forms.ToolBar
    Protected Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Public WithEvents ImageList As System.Windows.Forms.ImageList
    Friend WithEvents DatasetCliente1 As SIALIBEB.DatasetCliente
    Friend WithEvents lbMensaje As System.Windows.Forms.Label
End Class
