Public Class FormCrearCuentas
    Public cantCuentas As Integer = 0
    Public Mesa As String
    Public NombreMesa As String = ""
    Private cargando As Boolean = True
    Public currentProducto As Integer = 0
    Public currentComanda As String = "0"
    Public currentCantidad As Integer = 0
    Public currentSeparada As String = "0"
    Dim rapido As Boolean = False
    Public Facturada As Boolean = False

    Dim currentPos As Integer = 0
    Dim dt As New DataTable
    Dim Iv As Double = 0
    Dim Iserv As Double = 0

    Dim PosCuenta As Integer = 1

    Private Sub FormCrearCuentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            '  Dim dt As New DataTable
            ' cFunciones.Llenar_Tabla_Generico("select imp_venta, imp_servicio from configuraciones", dt)
            '  If dt.Rows.Count > 0 Then
            'Iv = dt.Rows(0).Item("imp_venta")
            '  Iserv = dt.Rows(0).Item("imp_servicio")
            '  End If

            Iv = glodtsGlobal.Configuraciones(0).Imp_Venta
            Iserv = glodtsGlobal.Configuraciones(0).Imp_Servicio

            Me.Text = "Cuentas de la Mesa: " & NombreMesa
            Me.ComboBoxCantPasar.Enabled = True
            Me.ComboBoxCantPasar.Visible = False
            Me.LabelCantidad.Visible = False
            Dim xContador As Integer

            If cantCuentas > 0 Then
                spCargarCuentas()
            Else
                Me.ComboBoxCuentas.Items.Add(1)
                Dim ListView1 As New ListViewItem(1)
                ListView1.SubItems.Add("-")
                ListViewCuentas.Items.Add(ListView1)
                cantCuentas = 1

                Dim cx As New Conexion
                cx.SlqExecute(cx.Conectar, "UPDATE ComandaTemporal Set separada = 1 WHERE cMesa = " & Mesa)
                cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas SET separada = " & Me.cantCuentas & " Where Id = " & Me.Mesa)
                cx.SlqExecute(cx.sQlconexion, "INSERT INTO Tb_R_Cuentas ([IdMesa],[Cuenta]) VALUES ('" & Me.Mesa & "','" & 1 & "')")
                cx.DesConectar(cx.sQlconexion)
            End If
            cargando = False
            If ListViewCuentas.Items.Count > 0 Then
                ListViewCuentas.Items(ListViewCuentas.Items.Count - 1).Selected = True
                ListViewCuentas.Items(ListViewCuentas.Items.Count - 1).Focused = True
            End If
            ' buscarArticuloCuenta(cantCuentas)
            '  PosCuenta = cantCuentas


        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Load", ex.Message)
        End Try
    End Sub

    Private Sub ListViewCuentas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListViewCuentas.SelectedIndexChanged
        Try
            If Not cargando Then
                Me.ComboBoxCantPasar.Visible = False
                Me.LabelCantidad.Visible = False
                If ListViewCuentas.SelectedItems.Count > 0 Then
                    PosCuenta = Int(ListViewCuentas.Items(ListViewCuentas.SelectedItems(0).Index).SubItems(0).Text)
                    Me.buscarArticuloCuenta(PosCuenta)
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ListViewCuentas_SelectedIndexChanged", ex.Message)
        End Try
    End Sub
    Private Sub buscarArticuloCuenta(ByVal NumCuenta As Integer)
        Try

            cFunciones.Llenar_Tabla_Generico("SELECT  *,isnull(MR.ImpServ,1) as ImpServicio, Abs(cCantidad) AS CantCobrar, 0 As Check1 " &
                         " FROM ComandasActivas LEFT OUTER JOIN  Menu_Restaurante as MR ON ComandasActivas.cProducto = MR.Id_Menu" &
                         " WHERE  (cPrecio > 0) AND (cMesa = " & Mesa & ") AND separada = " & NumCuenta, Me.DataSetComandas.ComandaTemporal)
            Me.Label1.Text = "De clic sobre el nombre del producto"
            Me.Label1.Enabled = False
            Me.ComboBoxCuentas.Enabled = False

            Dim cant As Double = 0
            Dim precio As Double = 0
            Dim subTotal As Double = 0
            Dim imp_venta As Double = 0
            Dim imp_serv As Double = 0
            Dim total As Double = 0

            Dim TotalSubTotal As Double = 0
            Dim TotalImp_venta As Double = 0
            Dim TotalImp_serv As Double = 0
            Dim Total_General As Double = 0

            For i As Integer = 0 To Me.DataSetComandas.ComandaTemporal.Count - 1
                cant = Math.Abs(Me.DataSetComandas.ComandaTemporal(i).cCantidad)
                precio = Me.DataSetComandas.ComandaTemporal(i).cPrecio
                subTotal = cant * precio
                imp_venta = subTotal * (Iv / 100)

                Try

                    If Me.DataSetComandas.ComandaTemporal(i).Item("ImpServicio") Then
                        imp_serv = subTotal * (Iserv / 100)
                    Else
                        imp_serv = 0
                    End If
                Catch ex As Exception
                    imp_serv = subTotal * (Iserv / 100)
                End Try


                total = subTotal + imp_venta + imp_serv

                TotalSubTotal += subTotal
                TotalImp_venta += imp_venta
                TotalImp_serv += imp_serv
                Total_General += total

            Next


            Me.TextBoxImpVenta.Text = Format(TotalImp_venta, "##,##0.00")
            Me.TextBoxImpServ.Text = Format(TotalImp_serv, "##,##0.00")
            Me.TextBoxSubTotal.Text = Format(TotalSubTotal, "##,##0.00")
            Me.TextBoxTotal.Text = Format(Total_General, "##,##0.00")

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "buscarArticuloCuenta", ex.Message)
        End Try
    End Sub

    Private Sub DataGridViewComandas_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridViewComandas.CellClick
        Try
            cargando = True
            If e.RowIndex >= 0 Then
                If Me.DataSetComandas.ComandaTemporal.Columns(e.ColumnIndex).Caption.Equals("Cobrar") Then
                    'MsgBox("PASE al clic")
                    Me.DataSetComandas.ComandaTemporal(e.RowIndex).Check1 = Not Me.DataSetComandas.ComandaTemporal(e.RowIndex).Check1
                End If
                Dim obj As EnumerableRowCollection(Of DataSetComandas.ComandaTemporalRow) = From x In DataSetComandas.ComandaTemporal Where x.Check1 = True

                If Me.DataSetComandas.ComandaTemporal(e.RowIndex).cCantidad > 1 And obj.Count = 1 Then
                    Me.ComboBoxCantPasar.Visible = True
                    Me.LabelCantidad.Visible = True
                    Dim cantAgregar As Integer = Me.DataSetComandas.ComandaTemporal(e.RowIndex).cCantidad


                    Dim xContador As Integer

                    ComboBoxCantPasar.Items.Clear()

                    ComboBoxCantPasar.Items.Add(1)
                    If cantAgregar > 1 Then

                        For xContador = 2 To cantAgregar
                            ComboBoxCantPasar.Items.Add(xContador)

                        Next
                    End If
                    Me.ComboBoxCantPasar.Text = cantAgregar

                Else
                    Me.ComboBoxCantPasar.Visible = False
                    Me.LabelCantidad.Visible = False
                End If
                Me.currentPos = e.RowIndex
                Me.Label1.Text = Me.DataSetComandas.ComandaTemporal(e.RowIndex).cCantidad & " - " & Me.DataSetComandas.ComandaTemporal(e.RowIndex).cDescripcion
                Me.ComboBoxCuentas.Text = Me.DataSetComandas.ComandaTemporal(e.RowIndex).separada
                currentProducto = Me.DataSetComandas.ComandaTemporal(e.RowIndex).id
                currentComanda = Me.DataSetComandas.ComandaTemporal(e.RowIndex).cCodigo
                currentCantidad = Me.DataSetComandas.ComandaTemporal(e.RowIndex).cCantidad
                Me.Label1.Enabled = True
                Me.ComboBoxCuentas.Enabled = True
            End If
            cargando = False
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "DataGridViewComandas_CellClick", ex.Message)
        End Try
    End Sub



    Private Sub ComboBoxCuentas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCuentas.SelectedIndexChanged
        Try
            If Not cargando Then
                Dim cant As String = Me.ComboBoxCantPasar.Text
                If cant.Length = 0 Then
                    cant = "1"
                End If
                If (Me.DataSetComandas.ComandaTemporal(Me.currentPos).separada <= 1 And Me.ComboBoxCuentas.Text = "1") Or (Me.DataSetComandas.ComandaTemporal(Me.currentPos).separada = Me.ComboBoxCuentas.Text) Then
                    Exit Sub
                End If
                If Me.ComboBoxCantPasar.Visible = False Or (currentCantidad = CInt(cant)) Then

                    pasarArticulosMarcados(ComboBoxCuentas.Text)

                Else
                    'Para cuando se divide la linea de la comanda
                    Dim cx As New Conexion
                    Dim cantS As String = Me.ComboBoxCantPasar.Text
                    If cantS.Length = 0 Then
                        cantS = "1"
                    End If
                    Dim cantidadQuienSeVa As Integer = CInt(cantS)
                    Dim cantidadQuienSeQueda As Integer = currentCantidad - cantidadQuienSeVa
                    cx.SlqExecute(cx.Conectar, "UPDATE ComandaTemporal Set cCantidad = " & cantidadQuienSeQueda & " WHERE id = " & currentProducto)
                    cargando = True
                    Dim dondEstaba As Integer = ListViewCuentas.Items(ListViewCuentas.SelectedItems(0).Index).SubItems(0).Text 'Me.ListBoxCuentas.SelectedIndex

                    cargando = False

                    cx.SlqExecute(cx.sQlconexion, "INSERT INTO ComandaTemporal" & _
                          " (cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, " & _
                          "cMesa, Impreso, Impresora, Comenzales, Tipo_Plato, CantImp, Cedula, Habitacion, primero, " & _
                          " separada, cantidad, Express, costo_real, comandado, solicitud) " & _
                        " VALUES (" & Me.DataSetComandas.ComandaTemporal(currentPos).cProducto & "," & cantidadQuienSeVa & ",'" & DataSetComandas.ComandaTemporal(currentPos).cDescripcion & "'," & _
                        "" & DataSetComandas.ComandaTemporal(currentPos).cPrecio & "," & _
                        "" & DataSetComandas.ComandaTemporal(currentPos).cCodigo & "," & _
                        "" & Mesa & "," & DataSetComandas.ComandaTemporal(currentPos).Impreso & "," & _
                        "'" & Me.DataSetComandas.ComandaTemporal(currentPos).Impresora & "'," & _
                        "" & DataSetComandas.ComandaTemporal(currentPos).Comenzales & "," & _
                        "'" & DataSetComandas.ComandaTemporal(currentPos).Tipo_Plato & "'," & _
                        "" & DataSetComandas.ComandaTemporal(currentPos).CantImp & "," & _
                        "'" & DataSetComandas.ComandaTemporal(currentPos).Cedula & "'," & _
                        "'" & DataSetComandas.ComandaTemporal(currentPos).Habitacion & "'," & DataSetComandas.ComandaTemporal(currentPos).primero & "," & ComboBoxCuentas.Text & "," & _
                        "0, " & CInt(DataSetComandas.ComandaTemporal(currentPos).Express) * -1 & "," & DataSetComandas.ComandaTemporal(currentPos).costo_real & "," & CInt(DataSetComandas.ComandaTemporal(currentPos).comandado) * -1 & ", " & Me.DataSetComandas.ComandaTemporal(currentPos).solicitud & ")")


                    Dim dt As New DataTable
                    cFunciones.Llenar_Tabla_Generico("Select * From ComandasActivas WHERE  cCodigo = " & Me.currentComanda & " AND id > " & Me.currentProducto, dt)
                    'CAMBIAR ACOMPAÑAMIENTOS
                    cx.Conectar()
                    For i As Integer = 0 To dt.Rows.Count - 1
                        If dt.Rows(i).Item("cCantidad") <= 0 Then
                            cx.SlqExecute(cx.sQlconexion, "INSERT INTO ComandaTemporal" & _
                                                " (cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, " & _
                                                "cMesa, Impreso, Impresora, Comenzales, Tipo_Plato, CantImp, Cedula, Habitacion, primero, " & _
                                                " separada,cantidad, Express,costo_real, comandado, solicitud) " & _
         " VALUES (" & dt.Rows(i).Item("cProducto") & ",0," & _
         "'" & dt.Rows(i).Item("cDescripcion") & "'," & _
         "" & dt.Rows(i).Item("cPrecio") & "," & _
         "" & dt.Rows(i).Item("cCodigo") & "," & _
         "" & Mesa & ", " & dt.Rows(i).Item("Impreso") & "," & _
         "'" & dt.Rows(i).Item("Impresora") & "'," & _
         "" & dt.Rows(i).Item("Comenzales") & "," & _
         "'" & dt.Rows(i).Item("Tipo_Plato") & "'," & _
         "" & dt.Rows(i).Item("CantImp") & "," & _
         "'" & dt.Rows(i).Item("Cedula") & "'," & _
         "'" & dt.Rows(i).Item("Habitacion") & "'," & _
         "" & dt.Rows(i).Item("primero") & "," & ComboBoxCuentas.Text & "," & _
         "0," & CInt(dt.Rows(i).Item("Express")) * -1 & "," & dt.Rows(i).Item("costo_real") & "," & CInt(dt.Rows(i).Item("comandado")) * -1 & ", " & Me.DataSetComandas.ComandaTemporal(currentPos).solicitud & ")")


                        Else
                            Exit For
                        End If

                    Next


                    cx.DesConectar(cx.sQlconexion)
                End If

            End If
            ListViewCuentas_SelectedIndexChanged(sender, e)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ComboBoxCuentas_SelectedIndexChanged", ex.Message)
        End Try
    End Sub

    Private Sub ButtonAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgregar.Click
        Try
            Dim canPedido As Double
            Dim rEmpleado0 As New registro
            rEmpleado0.Text = "Cuentas a agregar"
            rEmpleado0.ShowDialog()
            If rEmpleado0.iOpcion = 1 Then
                Try
                    canPedido = CInt(rEmpleado0.txtCodigo.Text)
                Catch ex As Exception
                    canPedido = 1
                End Try
            Else
                Exit Sub
            End If

            'If canPedido > 12 Then
            'canPedido = 1
            'End If

            rEmpleado0.Close()
            rEmpleado0.Dispose()

            Dim cx As New Conexion
            Dim anterior As Integer = cantCuentas + 1
            cx.Conectar("Restaurante")
            Me.cantCuentas += canPedido
            cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas SET separada = " & Me.cantCuentas & " Where Id = " & Me.Mesa)
            For i As Integer = anterior To cantCuentas
                cx.SlqExecute(cx.sQlconexion, "INSERT INTO Tb_R_Cuentas ([IdMesa],[Cuenta]) VALUES ('" & Me.Mesa & "','" & i & "')")

            Next

            cx.DesConectar(cx.sQlconexion)
            cargando = True
            Dim xContador As Integer

            If cantCuentas > 0 Then
                spCargarCuentas()
            Else
                ComboBoxCuentas.Items.Clear()
                ListViewCuentas.Items.Clear()
                Me.ComboBoxCuentas.Items.Add(1)
                Dim ListView1 As New ListViewItem(1)
                ListView1.SubItems.Add("-")
                ListViewCuentas.Items.Add(ListView1)
                cantCuentas = 1
            End If

            Me.ListViewCuentas.Focus()
            Me.ListViewCuentas.Items(0).Focused = True
            Me.ListViewCuentas.Items(0).Selected = True
            buscarArticuloCuenta(1)
            cargando = False
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ButtonAgregar_Click", ex.Message)
        End Try

    End Sub

    Private Sub CargarCuentas()
        Try
            cargando = True
            Dim xContador As Integer
            If cantCuentas > 0 Then
                spCargarCuentas()
            Else
                ComboBoxCuentas.Items.Clear()
                ListViewCuentas.Items.Clear()
                Me.ComboBoxCuentas.Items.Add(1)
                Dim ListView1 As New ListViewItem(1)
                ListView1.SubItems.Add("-")
                ListViewCuentas.Items.Add(ListView1)
                cantCuentas = 1
            End If
            buscarArticuloCuenta(cantCuentas)
            Me.ListViewCuentas.Focus()
            Me.ListViewCuentas.Items(0).Focused = True
            Me.ListViewCuentas.Items(0).Selected = True
            buscarArticuloCuenta(1)
            cargando = False
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "CargarCuentas", ex.Message)
        End Try
    End Sub

    Private Sub QuitarSeleccionadas()
        Try
            For i As Integer = 0 To Me.ListViewCuentas.SelectedItems.Count - 1
                If cantCuentas <> 1 Then
                    Dim CuentaSeleccionada As String = ListViewCuentas.Items(ListViewCuentas.SelectedItems(i).Index).SubItems(0).Text
                    If Not CuentaSeleccionada.Equals("1") Then
                        Dim cx As New Conexion
                        cx.Conectar("Restaurante")
                        cx.SlqExecute(cx.sQlconexion, "UPDATE ComandaTemporal SET separada = 1 WHERE separada = " & CuentaSeleccionada & " AND cMesa = " & Mesa & " AND cCodigo = '" & currentComanda & "'")
                        Me.cantCuentas -= 1
                        cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas SET separada = " & Me.cantCuentas & " Where Id = " & Me.Mesa)
                        cx.SlqExecute(cx.sQlconexion, "Delete from Tb_R_Cuentas where Cuenta = " & CuentaSeleccionada & " and IdMesa = " & Me.Mesa)
                        cx.DesConectar(cx.sQlconexion)
                    End If
                End If
            Next
            Me.CargarCuentas()
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "QuitarSeleccionadas", ex.Message)
        End Try
    End Sub

    Private Sub QuitarTodas()
        Try
            For i As Integer = 0 To Me.ListViewCuentas.Items.Count - 1
                If cantCuentas <> 1 Then
                    Dim CuentaSeleccionada As String = ListViewCuentas.Items(i).SubItems(0).Text
                    If Not CuentaSeleccionada.Equals("1") Then
                        Dim cx As New Conexion
                        cx.Conectar("Restaurante")
                        cx.SlqExecute(cx.sQlconexion, "UPDATE ComandaTemporal SET separada = 1 WHERE separada = " & Me.cantCuentas & " AND cMesa = " & Mesa)
                        Me.cantCuentas -= 1
                        cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas SET separada = " & Me.cantCuentas & " Where Id = " & Me.Mesa)
                        cx.SlqExecute(cx.sQlconexion, "Delete from Tb_R_Cuentas where Cuenta = " & CuentaSeleccionada & " and IdMesa = " & Me.Mesa)
                        cx.DesConectar(cx.sQlconexion)
                    End If
                End If
            Next
            Me.CargarCuentas()
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "QuitarTodas", ex.Message)
        End Try
    End Sub

    Private Sub QuitarCuentas()
        Try
            Dim reg As String = ""

            Try
                reg = GetSetting("SeeSOFT", "Restaurante", "PreguntaQuitarCueta")
                If reg = "" Then
                    reg = "1"
                    SaveSetting("SeeSOFT", "Restaurante", "PreguntaQuitarCueta", "1")
                End If
            Catch ex As Exception
                SaveSetting("SeeSOFT", "Restaurante", "PreguntaQuitarCueta", "1")
            End Try

            Select Case reg
                Case "0" ' pregunta si quita las cuentas seleccionadas o todas
                    If cantCuentas > 1 Then
                        Dim frm As New frmPreguntaQuitarCuentas
                        If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                            If frm.QuitarTodas = False Then
                                Me.QuitarSeleccionadas()
                            Else
                                Me.QuitarTodas()
                            End If
                        End If
                    End If
                Case "1" : Me.QuitarSeleccionadas() 'no pregunta, quita las cuentas selecciondas
                Case "2" : Me.QuitarTodas() 'no pregunta, quita todas las cuentas
            End Select
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "QuitarCuentas", ex.Message)
        End Try
    End Sub

    Private Sub ButtonEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEliminar.Click
        Me.QuitarCuentas()
    End Sub

    Public frm As New Form
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        frm.Show()
        Me.Close()
    End Sub
    Sub crearOtraCuenta()

        Try
            Dim cx As New Conexion
            cx.Conectar("Restaurante")
            cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas SET separada = " & ListViewCuentas.Items.Count + 1 & " Where Id = " & Me.Mesa)
            cx.DesConectar(cx.sQlconexion)

            cargando = True

            '  Me.pasarArticulosMarcados(CDbl(ListViewCuentas.Items(ListViewCuentas.SelectedItems(0).Index).SubItems(0).Text) + 1)
            Me.pasarArticulosMarcados(ListViewCuentas.Items.Count + 1)
            rapido = True
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "crearOtraCuenta", ex.Message)
        End Try

    End Sub
    Sub pasarArticulosMarcados(ByVal cuenta As Integer)

        Try
            For i As Integer = 0 To Me.DataSetComandas.ComandaTemporal.Count - 1
                If DataSetComandas.ComandaTemporal(i).Check1 Then

                    If DataSetComandas.ComandaTemporal(i).CantCobrar = DataSetComandas.ComandaTemporal(i).cCantidad Then

                        Dim cx As New Conexion
                        cx.SlqExecute(cx.Conectar, "UPDATE ComandaTemporal Set separada = " & cuenta & " WHERE id = " & DataSetComandas.ComandaTemporal(i).id)
                        'Dim dondEstaba As Integer = Me.ListBoxCuentas.SelectedIndex
                        'Me.ListBoxCuentas.SelectedIndex = ComboBoxCuentas.SelectedIndex
                        Dim dt As New DataTable
                        cFunciones.Llenar_Tabla_Generico("Select id, cCantidad From ComandasActivas WHERE  cCodigo = " & Me.currentComanda & " AND id > " & DataSetComandas.ComandaTemporal(i).id, dt)
                        'CAMBIAR ACOMPAÑAMIENTOS
                        For j As Integer = 0 To dt.Rows.Count - 1
                            If dt.Rows(j).Item("cCantidad") <= 0 Then
                                cx.SlqExecute(cx.sQlconexion, "UPDATE ComandaTemporal SET separada = " & cuenta & " WHERE id = " & dt.Rows(j).Item("id"))
                            Else
                                Exit For
                            End If

                        Next

                        cx.DesConectar(cx.sQlconexion)
                    Else
                        'Para cuando se divide la linea de la comanda
                        Dim cx As New Conexion
                        Dim cantS As String = Me.ComboBoxCantPasar.Text
                        If cantS.Length = 0 Then
                            cantS = "1"
                        End If
                        Dim cantidadQuienSeVa As Integer = Me.DataSetComandas.ComandaTemporal(i).CantCobrar
                        Dim cantidadQuienSeQueda As Integer = Me.DataSetComandas.ComandaTemporal(i).cCantidad - Me.DataSetComandas.ComandaTemporal(i).CantCobrar
                        cx.SlqExecute(cx.Conectar, "UPDATE ComandaTemporal Set cCantidad = " & cantidadQuienSeQueda & " WHERE id = " & Me.DataSetComandas.ComandaTemporal(i).id)
                        cargando = True
                        cargando = False

                        cx.SlqExecute(cx.sQlconexion, "INSERT INTO ComandaTemporal" & _
                              " (cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, " & _
                              "cMesa, Impreso, Impresora, Comenzales, Tipo_Plato, CantImp, Cedula, Habitacion, primero, " & _
                              " separada, cantidad, Express, costo_real, comandado, solicitud) " & _
                            " VALUES (" & Me.DataSetComandas.ComandaTemporal(i).cProducto & "," & cantidadQuienSeVa & ",'" & DataSetComandas.ComandaTemporal(i).cDescripcion & "'," & _
                            "" & DataSetComandas.ComandaTemporal(i).cPrecio & "," & _
                            "" & DataSetComandas.ComandaTemporal(i).cCodigo & "," & _
                            "" & Mesa & "," & DataSetComandas.ComandaTemporal(i).Impreso & "," & _
                            "'" & Me.DataSetComandas.ComandaTemporal(i).Impresora & "'," & _
                            "" & DataSetComandas.ComandaTemporal(i).Comenzales & "," & _
                            "'" & DataSetComandas.ComandaTemporal(i).Tipo_Plato & "'," & _
                            "" & DataSetComandas.ComandaTemporal(i).CantImp & "," & _
                            "'" & DataSetComandas.ComandaTemporal(i).Cedula & "'," & _
                            "'" & DataSetComandas.ComandaTemporal(i).Habitacion & "'," & DataSetComandas.ComandaTemporal(i).primero & "," & cuenta & "," & _
                            "0, " & CInt(DataSetComandas.ComandaTemporal(i).Express) * -1 & "," & DataSetComandas.ComandaTemporal(i).costo_real & "," & CInt(DataSetComandas.ComandaTemporal(i).comandado) * -1 & ", " & Me.DataSetComandas.ComandaTemporal(i).solicitud & ") ")

                        Dim dt As New DataTable
                        cFunciones.Llenar_Tabla_Generico("Select * From ComandasActivas WHERE  cCodigo = " & Me.currentComanda & " AND id > " & DataSetComandas.ComandaTemporal(i).id, dt)
                        'CAMBIAR ACOMPAÑAMIENTOS
                        cx.Conectar()
                        For j As Integer = 0 To dt.Rows.Count - 1
                            If dt.Rows(j).Item("cCantidad") <= 0 Then
                                cx.SlqExecute(cx.sQlconexion, "INSERT INTO ComandaTemporal" & _
                                                    " (cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, " & _
                                                    "cMesa, Impreso, Impresora, Comenzales, Tipo_Plato, CantImp, Cedula, Habitacion, primero, " & _
                                                    " separada,cantidad, Express,costo_real, comandado, solicitud) " & _
             " VALUES (" & dt.Rows(j).Item("cProducto") & ",0," & _
             "'" & dt.Rows(j).Item("cDescripcion") & "'," & _
             "" & dt.Rows(j).Item("cPrecio") & "," & _
             "" & dt.Rows(j).Item("cCodigo") & "," & _
             "" & Mesa & ", " & dt.Rows(j).Item("Impreso") & "," & _
             "'" & dt.Rows(j).Item("Impresora") & "'," & _
             "" & dt.Rows(j).Item("Comenzales") & "," & _
             "'" & dt.Rows(j).Item("Tipo_Plato") & "'," & _
             "" & dt.Rows(j).Item("CantImp") & "," & _
             "'" & dt.Rows(j).Item("Cedula") & "'," & _
             "'" & dt.Rows(j).Item("Habitacion") & "'," & _
             "" & dt.Rows(j).Item("primero") & "," & cuenta & "," & _
             "0," & CInt(dt.Rows(j).Item("Express")) * -1 & "," & dt.Rows(j).Item("costo_real") & "," & CInt(dt.Rows(j).Item("comandado")) * -1 & ", " & Me.DataSetComandas.ComandaTemporal(i).solicitud & ")")

                            Else
                                Exit For
                            End If

                        Next
                        cx.DesConectar(cx.sQlconexion)
                    End If

                End If
            Next
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "pasarArticulosMarcados", ex.Message)
        End Try
    End Sub
    Function valoraSiFacturaRapido() As Boolean
        Try
            For i As Integer = 0 To Me.DataSetComandas.ComandaTemporal.Count - 1
                If Me.DataSetComandas.ComandaTemporal(i).Check1 Then
                    Return True
                End If
            Next
            Return False
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "valoraSiFacturaRapido", ex.Message)
        End Try

    End Function
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        facturame()
        If Facturada And Not OpcionFacturacion Then
            Close()
        End If
    End Sub
    Sub facturame(Optional ByVal fast As Boolean = False)
        Dim cFacturaVenta As New Factura
        Try
            Dim rapido As Boolean = valoraSiFacturaRapido()
            If rapido Then crearOtraCuenta()
            Dim cx As New Conexion
            cx.Conectar("Restaurante")
            cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = 2 WHERE Id = " & ComandaMenu.idMesa)

            cx.DesConectar(cx.sQlconexion)
            Me.Button2.Enabled = False
            If ComandaMenu.todo_comandado = False Then
                ComandaMenu.spProcesarComanda()
            Else
                ' ComandaMenu.spCorrigeEstadoMesa()
            End If
            ComandaMenu.CSeparada = False
            Dim datareader As SqlClient.SqlDataReader
            If rapido Then
				' ComandaMenu.cConexion.GetRecorset(ComandaMenu.cConexion.Conectar, "Select * from ComandasActivas where  cMesa=" & Mesa & " and separada = '" & CDbl(ListViewCuentas.Items(ListViewCuentas.SelectedItems(0).Index).SubItems(0).Text) + 1 & "'", datareader)
				ComandaMenu.cConexion.GetRecorset(ComandaMenu.cConexion.Conectar, "Select * from ComandasActivas where  cMesa=" & Mesa & " and separada = '" & CDbl(ListViewCuentas.Items.Count + 1) & "'", datareader)
			Else
                ComandaMenu.cConexion.GetRecorset(ComandaMenu.cConexion.Conectar, "Select * from ComandasActivas where  cMesa=" & Mesa & " and separada = '" & PosCuenta & "'", datareader)
            End If
            If datareader.Read = False Then
                MessageBox.Show("No tiene artículos pendientes", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                datareader.Close()
                Me.Button2.Enabled = True
                Exit Sub
            Else
            End If
            datareader.Close()
            cFacturaVenta.rapido = fast
            cFacturaVenta.idmesa = ComandaMenu.idMesa
            cFacturaVenta.nombremesa = NombreMesa
            cFacturaVenta.numeroComanda = ComandaMenu.numeroComanda
            cFacturaVenta.Comenzales = ComandaMenu.comenzales
            If rapido Then
                cFacturaVenta.separada = True
                ' cFacturaVenta.numeroSeparado = CDbl(ListViewCuentas.Items(ListViewCuentas.SelectedItems(0).Index).SubItems(0).Text) + 1
                cFacturaVenta.numeroSeparado = ListViewCuentas.Items.Count + 1

                Dim asdf As String = ListViewCuentas.Items.Count
            Else
                cFacturaVenta.separada = True
                cFacturaVenta.numeroSeparado = ListViewCuentas.Items(ListViewCuentas.SelectedItems(0).Index).SubItems(0).Text
            End If
            cFacturaVenta.CedulaS = ComandaMenu.cConexion.SlqExecuteScalar(ComandaMenu.conectadobd, "Select CEDULA  from ComandasActivas where cMesa=" & Mesa)
            ' Me.Hide()
            ComandaMenu.vuelto = 0
            cFacturaVenta.ShowDialog()
            Dim NFactura As Int64 = cFacturaVenta.NumeroFactura
            If cFacturaVenta.hecho = True Then
                Facturada = True
                If OpcionFacturacion Then
                    ComandaMenu.OpcionesdePago()
                Else
                    Dim c As New Conexion
                    c.Conectar("Restaurante")

                    c.SlqExecute(c.sQlconexion, "UPDATE Mesas Set Activa = 2 WHERE Id = " & ComandaMenu.idMesa)
                    c.DesConectar(c.sQlconexion)
                End If
                ' TipoImpresion = ComandaMenu.cConexion.SlqExecuteScalar(ComandaMenu.conectadobd, "Select ImpFactura from conf")
                If cFacturaVenta.CheckBoxSR.Checked Then
                    ComandaMenu.imprimir_SERVICIO_RESTAURANTE(NFactura, "espanol", cFacturaVenta.id_ventas)
                Else
                    If TipoImpres = 1 Then
                        ComandaMenu.IMPRIMIR_MEJORADO(cFacturaVenta.CheckBoxSR.Checked, NFactura, cFacturaVenta.id_ventas)
                    ElseIf TipoImpres = 2 Then
                        ComandaMenu.IMPRIMIR_MEJORADO(cFacturaVenta.CheckBoxSR.Checked, NFactura, cFacturaVenta.id_ventas)
                    ElseIf TipoImpres = 3 Then
                        If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                            ComandaMenu.IMPRIMIR_MEJORADO(cFacturaVenta.CheckBoxSR.Checked, NFactura, cFacturaVenta.id_ventas)
                        Else
                            ComandaMenu.IMPRIMIR_MEJORADO(cFacturaVenta.CheckBoxSR.Checked, NFactura, cFacturaVenta.id_ventas)
                        End If
                    ElseIf TipoImpres = 4 Then
                        ComandaMenu.IMPRIMIR_MEJORADO(cFacturaVenta.CheckBoxSR.Checked, NFactura, cFacturaVenta.id_ventas)

                    End If

                End If
            Else
                ' cx.SlqExecute(cx.Conectar, "UPDATE comandatemporal Set separada = " & PosCuenta & " WHERE Estado='Activo' and cMesa = " & Mesa & " and separada = " & PosCuenta + 1)
                ' cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas SET separada = " & Me.cantCuentas & " Where Id = " & Me.Mesa)
                ' cx.DesConectar(cx.sQlconexion)
            End If
            cFacturaVenta.Dispose()
            cFacturaVenta.Close()
            If (ComandaMenu.vuelto > 0) And GetSetting("SeeSoft", "Restaurante", "MostrarVuelto").Equals("1") Then
                Dim fvuelto As New Vuelto
                fvuelto.txtvuelto.Text = ComandaMenu.vuelto
                fvuelto.ShowDialog()
            End If
            If Not cargando Then
                Me.ComboBoxCantPasar.Visible = False
                Me.LabelCantidad.Visible = False
                Me.buscarArticuloCuenta(Me.PosCuenta)
            End If
            If rapido And cFacturaVenta.hecho Then
                Me.buscarArticuloCuenta(Me.PosCuenta)
                Dim cxn As New Conexion
                cxn.Conectar("Restaurante")
                cxn.SlqExecute(cxn.sQlconexion, "UPDATE Mesas SET separada = " & Me.cantCuentas & " Where Id = " & Me.Mesa)
                cxn.DesConectar(cxn.sQlconexion)
                Me.cargando = False
            Else
                If rapido Then

                    Me.cargando = True

                    Dim cxn As New Conexion
                    cxn.Conectar("Restaurante")
                    cxn.SlqExecute(cxn.sQlconexion, "UPDATE Mesas SET separada = " & Me.cantCuentas & " Where Id = " & Me.Mesa)
                    cxn.SlqExecute(cxn.sQlconexion, "UPDATE comandatemporal SET separada = " & PosCuenta & " Where Estado = 'Activo' AND separada = " & CDbl(ListViewCuentas.Items.Count) + 1 & " AND cmesa = " & Me.Mesa)
                    cxn.Conectar("Restaurante")

                    If cantCuentas > 0 Then
                        spCargarCuentas()
                    Else
                        ComboBoxCuentas.Items.Clear()
                        ListViewCuentas.Items.Clear()
                        Me.ComboBoxCuentas.Items.Add(1)
                        Dim ListView1 As New ListViewItem(1)
                        ListView1.SubItems.Add("-")
                        ListViewCuentas.Items.Add(ListView1)
                        cantCuentas = 1
                    End If
                    Me.buscarArticuloCuenta(Me.PosCuenta)
                    Me.cargando = False
                Else
                    Me.buscarArticuloCuenta(Me.PosCuenta)
                    Me.cargando = False
                End If
            End If
            cx.SlqExecute(cx.Conectar(), "UPDATE Mesas Set Activa = 4 WHERE Id = " & ComandaMenu.idMesa)

        Catch ex As Exception
            MessageBox.Show("Error al realizar la factura, " & ex.ToString, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "facturame", ex.Message)
        End Try


        If ListViewCuentas.Items.Count > 0 Then
            ListViewCuentas.Items(PosCuenta - 1).Selected = True
            ListViewCuentas.Items(PosCuenta - 1).Focused = True
        End If
        'If ListViewCuentas.Items.Count > 0 Then
        '    ListViewCuentas.Items(ListViewCuentas.Items.Count - 1).Selected = True
        '    ListViewCuentas.Items(ListViewCuentas.Items.Count - 1).Focused = True
        'End If
        Me.Button2.Enabled = True
       
    End Sub

    Private Sub ComboBoxCantPasar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCantPasar.SelectedIndexChanged
        Try
            If Not Me.cargando Then
                Me.DataSetComandas.ComandaTemporal(Me.currentPos).CantCobrar = Me.ComboBoxCantPasar.Text

            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ComboBoxCantPasar_SelectedIndexChanged", ex.Message)
        End Try
    End Sub

    Private Sub RAPIDOToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RAPIDOToolStripMenuItem1.Click
        Me.facturame(True)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Close()
    End Sub

    Private Sub ListViewCuentas_DoubleClick(sender As Object, e As EventArgs) Handles ListViewCuentas.DoubleClick
        Try
            Dim frm As New frmNombreCuenta
            frm.NombreCuenta = ListViewCuentas.Items(ListViewCuentas.SelectedItems(0).Index).SubItems(1).Text
            frm.ShowDialog()
            If frm.Aceptado Then
                Dim CuentaSeleccionada As String = ListViewCuentas.Items(ListViewCuentas.SelectedItems(0).Index).SubItems(0).Text
                Dim cx As New Conexion
                cx.Conectar("Restaurante")
                cx.SlqExecute(cx.sQlconexion, "UPDATE Tb_R_Cuentas set  NombreCuenta = '" & frm.NombreCuenta & "' where IdMesa = '" & Mesa & "' and Cuenta = '" & CuentaSeleccionada & "'")
                cx.DesConectar(cx.sQlconexion)
                spCargarCuentas()
                buscarArticuloCuenta(CuentaSeleccionada)
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ListViewCuentas_DoubleClick", ex.Message)
        End Try
    End Sub

    Private Sub spCargarCuentas()
        Try
            ComboBoxCuentas.Items.Clear()
            ListViewCuentas.Items.Clear()
            Dim cx As New ConexionR
            Dim rs As SqlClient.SqlDataReader
            cx.GetRecorset(cx.Conectar, "select Cuenta, NombreCuenta from Tb_R_Cuentas where IdMesa = '" & Mesa & "' order by Cuenta", rs)

            While rs.Read
                ComboBoxCuentas.Items.Add(rs("Cuenta"))
                Dim ListView2 As New ListViewItem(rs("Cuenta").ToString)
                ListView2.SubItems.Add(IIf(rs("NombreCuenta").ToString.Equals(""), "-", rs("NombreCuenta").ToString))
                ListViewCuentas.Items.Add(ListView2)
            End While
            rs.Close()
            cx.DesConectar(cx.SqlConexion)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "spCargarCuentas", ex.Message)
        End Try
    End Sub

    Private Sub ListViewCuentas_KeyDown(sender As Object, e As KeyEventArgs) Handles ListViewCuentas.KeyDown
        If e.KeyCode = Keys.Delete Then
            Dim msg As New frmMensaje("Desea quitar las cuentas seleccionadas", 1)
            If msg.ShowDialog = Windows.Forms.DialogResult.Yes Then
                Me.QuitarSeleccionadas()
            End If
        End If
    End Sub

End Class