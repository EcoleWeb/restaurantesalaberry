

Public Class FrmDatos_Express
    Public Comanda As Long
    Public Cancelado As Boolean









#Region "Controles GUI ToolBar"
    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Select Case e.Button.Name
            Case ToolBarRegistrar.Name : Registrando()
            Case ToolBarEditar.Name : Editando()
            Case ToolBarEliminar.Name : EliminarDatos()
            Case ToolBarCerrar.Name : Cerrando()
        End Select
    End Sub
    Private Sub Editando()
        If ToolBarRegistrar.Text = "Registrar" Then
            ToolBarRegistrar.Text = "Actualizar"
            ToolBarEditar.Text = "Cancelar"
            TxtNombre.Enabled = True
            TxtTel.Enabled = True
            TxtDireccion.Enabled = True
        Else
            ToolBarRegistrar.Text = "Registrar"
            ToolBarEditar.Text = "Editar"

            TxtNombre.Enabled = False
            TxtTel.Enabled = False
            TxtDireccion.Enabled = False
        End If
    End Sub
    Private Sub Registrando()
        If ToolBarRegistrar.Text = "Registrar" Then RegistrarDatos() Else ActualizarDatos()
        Cancelado = False
        Close()
    End Sub
    Private Sub Cerrando()
        Cancelado = True
        If MessageBox.Show("Desea cerrar la ventana actual... ", "Atención...", MessageBoxButtons.YesNo) = MsgBoxResult.Yes Then Close()
    End Sub


#End Region

#Region "Intefaz con BD"
    Private Sub RegistrarDatos()
        Dim Conexion As New Conexion
        Dim Campos As String = "id_Comanda,Nombre, Telefono, Direccion"
        Dim Datos As String = Comanda & ",'" & TxtNombre.Text & "','" & TxtTel.Text & "','" & TxtDireccion.Text & "'"
        Dim Mensaje As String = ""

        Mensaje = Conexion.AddNewRecord("DatosExpress", Campos, Datos)
        If Mensaje <> "" Then
            MessageBox.Show(Mensaje, "Atención", MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning)
        End If
        Conexion.DesConectar(Conexion.sQlconexion)
    End Sub
    Private Sub ActualizarDatos()
        Try
            Dim Conexion As New Conexion
            If Conexion.UpdateRecords("Datos_Express", "Id_Comanda =" & lbComanda.Text & ", Nombre = '" & TxtNombre.Text & "', Telefono = '" & TxtTel.Text & ", Direccion= '" & TxtDireccion.Text & "'", "id_Comanda = " & lbComanda.Text) = "" Then
            End If
            Conexion.DesConectar(Conexion.sQlconexion)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Atención...", MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub BuscarDatos(ByVal Comanda As Long)
        Dim Conexion As New Conexion
        Dim Rs As SqlClient.SqlDataReader
        Conexion.Conectar()
        Rs = Conexion.GetRecorset(Conexion.sQlconexion, "select * from DatosExpress where id_comanda = " & Comanda)
        If Rs.Read() Then
            TxtNombre.Text = Rs("nombre")
            TxtTel.Text = Rs("Telefono")
            TxtDireccion.Text = Rs("Direccion")
            ToolBarEditar.Enabled = True
            ToolBarEliminar.Enabled = True
            TxtDireccion.Enabled = False
            TxtTel.Enabled = False
            TxtNombre.Enabled = False
        End If
        Rs.Close()
        Conexion.DesConectar(Conexion.sQlconexion)
    End Sub
    Private Sub EliminarDatos()
        Try
            Dim Conexion As New Conexion
            Conexion.DeleteRecords("DatosExpress", "ID_Comanda =" & Comanda, "Restaurante")
            Conexion.DesConectar(Conexion.sQlconexion)
            MessageBox.Show("Datos del Pedido Express han sido eliminado", "Antención...")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Atención..", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region

#Region "GUI"
    Private Sub TxtTel_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtTel.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Or e.KeyChar = "-" Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub FrmDatos_Express_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ToolBarRegistrar.Enabled = True
        lbComanda.Text = Comanda
        BuscarDatos(Comanda)
    End Sub

    Private Sub Nuevo()
        TxtNombre.Text = ""
        TxtTel.Text = ""
        TxtDireccion.Text = ""
    End Sub

#End Region

End Class