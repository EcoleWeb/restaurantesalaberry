﻿Imports System.Data
Imports System.Data.SqlClient
Public Class ImpuestoProducto
    Public Shared Function Obtener(IdCliente As String, Optional IdInventario As String = "0") As Double

        Dim porcentaje As Double = 0
        Dim cd As New SqlCommand
        Dim dtInventario As New DataTable

        cd.CommandText = "SELECT cedula, nombre, Credito,  Cliente.idTipoCliente, TipoCliente.porcentajeIVA, Limite_Credito,
                        Plazo_credito, descuento, tipoprecio, Cliente.id, CodMoneda
                        FROM Cliente join TipoCliente on Cliente.idTipoCliente=TipoCliente.id
                        where Cliente.id = @IdCliente"
        cd.Parameters.AddWithValue("@IdCliente", IdCliente)
        Dim dtCliente As New DataTable
        cFunciones.CargarTablaSqlCommand(cd, dtCliente, GetSetting("SeeSoft", "Hotel", "Conexion"))
        If Not IdInventario.Equals("0") Then
            cd.CommandText = "SELECT Id_Categoria, CA.porcentajeIVA 
                          FROM Menu_Restaurante INNER JOIN  Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id
                          join Hotel.dbo.Cabys CA on Categorias_Menu.idCabys=CA.id where Id_Menu=@Codigo"
            cd.Parameters.AddWithValue("@Codigo", IdInventario)

            cFunciones.CargarTablaSqlCommand(cd, dtInventario, GetSetting("SeeSoft", "Restaurante", "Conexion"))

        End If


        If dtInventario.Rows.Count > 0 And dtCliente.Rows.Count > 0 Then

            If dtCliente.Rows(0).Item("porcentajeIVA") < dtInventario.Rows(0).Item("porcentajeIVA") Then
                Return dtCliente.Rows(0).Item("porcentajeIVA")
            End If
            Return dtInventario.Rows(0).Item("porcentajeIVA")
        Else
            If dtCliente.Rows.Count > 0 Then
                Return dtCliente.Rows(0).Item("porcentajeIVA")
            End If
            If dtInventario.Rows.Count > 0 Then
                Return dtInventario.Rows(0).Item("porcentajeIVA")
            End If

            Return 13
        End If
    End Function

End Class
