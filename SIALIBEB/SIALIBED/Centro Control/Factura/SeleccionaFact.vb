Public Class SeleccionaFact

#Region "variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim DataS As New DataSet
    Dim DataV As DataView
    Public separada As Boolean = False
    Public hecho As Boolean = False
    Public idmesa, numerocomanda As Integer
    Public NFactura As Int64
    Public clave As String
#End Region

    Private Sub SeleccionaFact_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DeleteRecords(conectadobd, "Cuentas", "idMesa=" & idmesa)
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub SeleccionaFact_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        cargarDatos()
    End Sub

    Private Sub CargarDatos()
        Try
            Dim cantidad, precio, descripcion As String
            cConexion.GetDataSet(conectadobd, "Select id, cProducto,cCantidad,cDescripcion,cPrecio,cCodigo from comandaTemporal where cCodigo=" & numerocomanda, DataS, "ComandaTemporal")
            Dim fila As DataRow
            ListBox1.Items.Add("     Cantidad  Precio/U  Descripci�n")
            For Each fila In DataS.Tables("ComandaTemporal").Rows
                precio = fila("cCantidad") * fila("cPrecio")
                cantidad = fila("cCantidad")
                descripcion = Trim(fila("cDescripcion")).ToUpper
                'If cantidad = 0 Then
                ' ListBox1.Items.Add(fila("Id") & "^" & cantidad & vbTab & precio & vbTab & descripcion)
                ' Else
                ListBox1.Items.Add(fila("id") & "^" & vbTab & cantidad & vbTab & precio & vbTab & Trim(descripcion))
                'End If
            Next
        Catch ex As Exception
            MessageBox.Show("Error al cargar los datos", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    
    Private Sub cboxFactura_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim arrayLista As Array
            Dim fila As DataRow
            Dim ii As Integer
            Dim inst As String
            For ii = 0 To ListBox1.Items.Count - 1
                arrayLista = ListBox1.Text.ToString.Split("^") '
                If ListBox1.GetSelected(ii) = True Then
                    'Busca el articulo en el dataset,para rebajarlo
                    For Each fila In DataS.Tables("ComandaTemporal").Rows
                        If arrayLista(0) = fila("id") Then
                            If fila("cCantidad") = 1 Then
                                Dim copia As String = ListBox1.Text
                                ListBox2.Items.Add(ListBox1.Text)
                                Dim s As Integer = arrayLista(0)
                                ingresar(s, fila("cCantidad"))
                                ingresa_detalles(fila("id"))
                                ListBox1.Items.Remove(copia)
                                NumCantidad.Value = 1
                            ElseIf fila("cCantidad") <> 0 Then
                                inst = (arrayLista(0) & "^" & vbTab & NumCantidad.Value & vbTab & fila("cPrecio") & vbTab & Trim(fila("cDescripcion")))
                                If NumCantidad.Value <> 0 And NumCantidad.Value <= fila("cCantidad") Then
                                    ListBox2.Items.Add(inst)
                                    ListBox1.Items.Remove(ListBox1.Text)
                                    If fila("cCantidad") >= NumCantidad.Value Then
                                        Dim s As Integer = arrayLista(0)
                                        ingresar(s, NumCantidad.Value)
                                        If (fila("cCantidad") - NumCantidad.Value) > 0 Then
                                            inst = (arrayLista(0) & "^" & vbTab & (fila("cCantidad") - NumCantidad.Value) & vbTab & fila("cPrecio") & vbTab & Trim(fila("cDescripcion")))
                                            ListBox1.Items.Add(inst)
                                        End If
                                        NumCantidad.Value = 0
                                    End If
                                Else
                                    MessageBox.Show("Verifique la cantidad seleccionada", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ingresar(ByVal s As Integer, ByVal cant As Decimal)
        cConexion.GetRecorset(conectadobd, "Select * from Cuentas where idcTemporal=" & s & " and idMesa=" & idmesa, rs)
        If rs.Read = True Then
            Dim can As Integer = rs("Cantidad")
            rs.Close()
            cant = can + cant
            cConexion.UpdateRecords(conectadobd, "Cuentas", "Cantidad= " & cant, "idcTemporal=" & s & " and idMesa=" & idmesa)
        Else
            rs.Close()
            cConexion.AddNewRecord(conectadobd, "Cuentas", "idMesa, idcTemporal, Cantidad", idmesa & "," & s & "," & cant)
        End If

    End Sub

    Private Sub ingresa_detalles(ByVal id As Integer)
        Dim fila As DataRowView
        Dim fila2 As DataRow
        Dim datav As DataView
        Dim k As Integer = id
        Dim str As String = ""
        For Each fila2 In DataS.Tables("ComandaTemporal").Rows
            datav = New DataView(DataS.Tables("ComandaTemporal"))
            id += 1
            datav.RowFilter = "Id = " & id

            For Each fila In datav
                If fila("cCantidad") <> 0 And fila("Id") > k Then
                    Exit Sub
                End If
                str = fila("id") & "^" & vbTab & fila("cCantidad") & vbTab & CInt(fila("cPrecio")) & vbTab & Trim(fila("cDescripcion").ToString.ToUpper)
                ListBox2.Items.Add(str)
                ListBox1.Items.Remove(str)
                ingresar(fila("id"), fila("cCantidad"))
            Next
        Next
    End Sub
    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 1 : CrearFactura()
            Case 2 : Eliminar()
            Case 3 : teclado()
            Case 4 : Close()

        End Select
    End Sub


    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicaci�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub CrearFactura()
        If ListBox2.Items.Count > 0 Then
            Dim nombremesa As String
            nombremesa = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre_Mesa from Mesas where Id=" & idmesa)
            Dim cFacturaVenta As New Factura 'Venta
            cFacturaVenta.idmesa = idmesa
            cFacturaVenta.nombremesa = nombremesa
            cFacturaVenta.numeroComanda = numerocomanda
            cFacturaVenta.Comenzales = 1
            cFacturaVenta.separada = True
            Me.Hide()

            cFacturaVenta.ShowDialog()
            separada = cFacturaVenta.separada
            NFactura = cFacturaVenta.numerofactura
            hecho = cFacturaVenta.hecho
            Me.Dispose()
        Else
            MessageBox.Show("No seleccion� ning�n art�culo para crear la cuenta separada", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Close()
        End If
    End Sub

    Private Sub Eliminar()
        Try
            Dim existe As Boolean = False
            Dim cant As Decimal
            Dim arrayLista, arrayLista2 As Array
            Dim ii, i As Integer
            For ii = 0 To ListBox2.Items.Count - 1
                arrayLista = ListBox2.Text.ToString.Split("" & vbTab & "") '
                If ListBox2.GetSelected(ii) = True Then
                    cant = arrayLista(1)
                    If cant = 0 Then
                        Exit Sub
                    End If
                    For i = 0 To ListBox1.Items.Count - 1
                        arrayLista2 = ListBox1.Items(i).ToString.Split("" & vbTab & "")  '.Text.ToString.Split()
                        If arrayLista(0) = arrayLista2(0) Then
                            Dim idct As Array = ListBox2.Text.ToString.Split("^")
                            Dim cant2 As Decimal = arrayLista2(1)
                            ListBox2.Items.Remove(ListBox2.Text)
                            ListBox1.Items.Remove(ListBox1.Items(i))
                            cConexion.DeleteRecords(conectadobd, "Cuentas", "idcTemporal=" & idct(0))
                            cant = cant + cant2
                            ListBox1.Items.Add(arrayLista(0) & vbTab & cant & vbTab & arrayLista(2) & vbTab & Trim(arrayLista(3)))
                            existe = True
                        End If
                    Next
                    If existe = False Then
                        ListBox1.Items.Add(ListBox2.Text)
                        arrayLista = ListBox2.Text.ToString.Split("^")  '.Text.ToString.Split()
                        cConexion.DeleteRecords(conectadobd, "Cuentas", "idcTemporal=" & arrayLista(0))
                        elimina_detalles(arrayLista(0))
                        ListBox2.Items.Remove(ListBox2.Text)
                    End If
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub elimina_detalles(ByVal id As Integer)
        Dim fila As DataRowView
        Dim fila2 As DataRow
        Dim datav As DataView
        Dim k As Integer = id
        Dim str As String = ""
        For Each fila2 In DataS.Tables("ComandaTemporal").Rows
            datav = New DataView(DataS.Tables("ComandaTemporal"))
            id += 1
            datav.RowFilter = "Id = " & id

            For Each fila In datav
                If fila("cCantidad") <> 0 And fila("Id") > k Then
                    Exit Sub
                End If
                str = fila("id") & "^" & vbTab & fila("cCantidad") & vbTab & fila("cPrecio") & vbTab & Trim(fila("cDescripcion"))
                ListBox1.Items.Add(str)
                ListBox2.Items.Remove(str)
                cConexion.DeleteRecords(conectadobd, "Cuentas", "idcTemporal=" & fila("id"))
            Next
        Next
    End Sub
End Class