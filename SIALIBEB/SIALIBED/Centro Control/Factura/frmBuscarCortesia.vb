Public Class frmBuscarCortesia

    Dim miFactura As New cls_Facturas()
    Public idCentro As String = ""
    Public nombreCentro As String = ""
    Public datos As DataTable

    'Cargamos los datos de los centros de cortesias..
    Public Sub cargarDatos()
        Me.datos = Me.miFactura.obtenerCentroCortesia()
        Me.DataGridView1.DataSource = Me.datos

        Me.DataGridView1.Columns("Nombre").Width = Me.DataGridView1.Width - 20
        Me.DataGridView1.Columns("Id_Centro").Visible = False
        Me.DataGridView1.Columns("Observaciones").Visible = False
    End Sub

    'Seleccionamos una cortesia..
    Public Sub seleccionarCortesia()
        Me.idCentro = Me.DataGridView1.SelectedRows(0).Cells("Id_Centro").Value
        Me.nombreCentro = Me.DataGridView1.SelectedRows(0).Cells("Nombre").Value
    End Sub


    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click, Label1.Click
        Me.seleccionarCortesia()
        Me.Close()
    End Sub
    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click, Label2.Click
        Me.idCentro = ""
        Me.nombreCentro = ""
        Me.Close()
    End Sub
   
End Class