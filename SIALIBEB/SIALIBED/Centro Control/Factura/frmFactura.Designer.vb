<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFactura
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtCliente = New System.Windows.Forms.TextBox
        Me.txtComisionista = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtSubtotal = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtDescuento = New System.Windows.Forms.TextBox
        Me.txtImpServ = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtImpVentas = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtExtraProp = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtTotal = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.dgDetalle = New System.Windows.Forms.DataGridView
        Me.PictureBox14 = New System.Windows.Forms.PictureBox
        Me.pbExtraProp = New System.Windows.Forms.PictureBox
        Me.pbImpVentas = New System.Windows.Forms.PictureBox
        Me.pbImpServ = New System.Windows.Forms.PictureBox
        Me.pbDescuento = New System.Windows.Forms.PictureBox
        Me.pbDolares = New System.Windows.Forms.PictureBox
        Me.pbColones = New System.Windows.Forms.PictureBox
        Me.pbComisionista = New System.Windows.Forms.PictureBox
        Me.pbCliente = New System.Windows.Forms.PictureBox
        Me.pbServH = New System.Windows.Forms.PictureBox
        Me.pbCortesia = New System.Windows.Forms.PictureBox
        Me.pbCargoHab = New System.Windows.Forms.PictureBox
        Me.pbCredito = New System.Windows.Forms.PictureBox
        Me.pbContado = New System.Windows.Forms.PictureBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label19 = New System.Windows.Forms.Label
        Me.pbSR = New System.Windows.Forms.PictureBox
        Me.Button2 = New System.Windows.Forms.Button
        CType(Me.dgDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbExtraProp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbImpVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbImpServ, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbDolares, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbColones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbComisionista, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbServH, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbCortesia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbCargoHab, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbCredito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbContado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbSR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(36, 21)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 16)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "CONTADO"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(154, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 16)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "CREDITO"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(257, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 16)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "CARGO HAB"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(378, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 16)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "CORTESIA"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(492, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 16)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "SERV HAB"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.White
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(42, 84)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 16)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "CLIENTE"
        '
        'txtCliente
        '
        Me.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCliente.Location = New System.Drawing.Point(138, 72)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(543, 40)
        Me.txtCliente.TabIndex = 27
        Me.txtCliente.Text = "CLIENTE DE CONTADO"
        '
        'txtComisionista
        '
        Me.txtComisionista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtComisionista.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComisionista.ForeColor = System.Drawing.Color.DimGray
        Me.txtComisionista.Location = New System.Drawing.Point(138, 124)
        Me.txtComisionista.Name = "txtComisionista"
        Me.txtComisionista.Size = New System.Drawing.Size(543, 29)
        Me.txtComisionista.TabIndex = 30
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.White
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DimGray
        Me.Label8.Location = New System.Drawing.Point(29, 132)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 16)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "COMISIONISTA"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.White
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(269, 189)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 16)
        Me.Label11.TabIndex = 35
        Me.Label11.Text = "DOLARES"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.White
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(151, 189)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(79, 16)
        Me.Label12.TabIndex = 33
        Me.Label12.Text = "COLONES"
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.White
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.DarkGray
        Me.Label10.Location = New System.Drawing.Point(20, 212)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(341, 24)
        Me.Label10.TabIndex = 36
        Me.Label10.Text = "_________________________________________________________________________________" & _
            "________________________"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.White
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(38, 249)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 16)
        Me.Label13.TabIndex = 37
        Me.Label13.Text = "SUBTOTAL"
        '
        'txtSubtotal
        '
        Me.txtSubtotal.BackColor = System.Drawing.Color.White
        Me.txtSubtotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubtotal.ForeColor = System.Drawing.Color.DimGray
        Me.txtSubtotal.Location = New System.Drawing.Point(138, 241)
        Me.txtSubtotal.Name = "txtSubtotal"
        Me.txtSubtotal.ReadOnly = True
        Me.txtSubtotal.Size = New System.Drawing.Size(222, 29)
        Me.txtSubtotal.TabIndex = 38
        Me.txtSubtotal.Text = "0.0"
        Me.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.White
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(42, 286)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(82, 16)
        Me.Label14.TabIndex = 40
        Me.Label14.Text = "Descuento"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtDescuento
        '
        Me.txtDescuento.BackColor = System.Drawing.Color.White
        Me.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescuento.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescuento.ForeColor = System.Drawing.Color.DimGray
        Me.txtDescuento.Location = New System.Drawing.Point(138, 278)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.ReadOnly = True
        Me.txtDescuento.Size = New System.Drawing.Size(222, 29)
        Me.txtDescuento.TabIndex = 41
        Me.txtDescuento.Text = "0.0"
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtImpServ
        '
        Me.txtImpServ.BackColor = System.Drawing.Color.White
        Me.txtImpServ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImpServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImpServ.ForeColor = System.Drawing.Color.DimGray
        Me.txtImpServ.Location = New System.Drawing.Point(138, 319)
        Me.txtImpServ.Name = "txtImpServ"
        Me.txtImpServ.ReadOnly = True
        Me.txtImpServ.Size = New System.Drawing.Size(222, 29)
        Me.txtImpServ.TabIndex = 44
        Me.txtImpServ.Text = "0.0"
        Me.txtImpServ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.White
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(42, 327)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(73, 16)
        Me.Label15.TabIndex = 43
        Me.Label15.Text = "Imp. Serv"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtImpVentas
        '
        Me.txtImpVentas.BackColor = System.Drawing.Color.White
        Me.txtImpVentas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImpVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImpVentas.ForeColor = System.Drawing.Color.DimGray
        Me.txtImpVentas.Location = New System.Drawing.Point(138, 360)
        Me.txtImpVentas.Name = "txtImpVentas"
        Me.txtImpVentas.ReadOnly = True
        Me.txtImpVentas.Size = New System.Drawing.Size(222, 29)
        Me.txtImpVentas.TabIndex = 47
        Me.txtImpVentas.Text = "0.0"
        Me.txtImpVentas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.White
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(36, 368)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(89, 16)
        Me.Label16.TabIndex = 46
        Me.Label16.Text = "Imp. Ventas"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtExtraProp
        '
        Me.txtExtraProp.BackColor = System.Drawing.Color.White
        Me.txtExtraProp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtExtraProp.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExtraProp.ForeColor = System.Drawing.Color.DimGray
        Me.txtExtraProp.Location = New System.Drawing.Point(138, 401)
        Me.txtExtraProp.Name = "txtExtraProp"
        Me.txtExtraProp.ReadOnly = True
        Me.txtExtraProp.Size = New System.Drawing.Size(222, 29)
        Me.txtExtraProp.TabIndex = 50
        Me.txtExtraProp.Text = "0.0"
        Me.txtExtraProp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.White
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(31, 409)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(95, 15)
        Me.Label17.TabIndex = 49
        Me.Label17.Text = "Extra Prop"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.Color.White
        Me.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(140, 442)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(220, 40)
        Me.txtTotal.TabIndex = 53
        Me.txtTotal.Text = "1234567890"
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.LightSkyBlue
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(32, 449)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(92, 29)
        Me.Label18.TabIndex = 52
        Me.Label18.Text = "TOTAL"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(20, 47)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(661, 24)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "_________________________________________________________________________________" & _
            "________________________"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.White
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DarkGray
        Me.Label9.Location = New System.Drawing.Point(20, 152)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(661, 24)
        Me.Label9.TabIndex = 31
        Me.Label9.Text = "_________________________________________________________________________________" & _
            "________________________"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'dgDetalle
        '
        Me.dgDetalle.AllowUserToAddRows = False
        Me.dgDetalle.AllowUserToDeleteRows = False
        Me.dgDetalle.AllowUserToResizeColumns = False
        Me.dgDetalle.AllowUserToResizeRows = False
        Me.dgDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgDetalle.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgDetalle.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgDetalle.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgDetalle.GridColor = System.Drawing.Color.White
        Me.dgDetalle.Location = New System.Drawing.Point(381, 179)
        Me.dgDetalle.Name = "dgDetalle"
        Me.dgDetalle.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgDetalle.RowHeadersVisible = False
        Me.dgDetalle.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgDetalle.Size = New System.Drawing.Size(300, 184)
        Me.dgDetalle.TabIndex = 54
        '
        'PictureBox14
        '
        Me.PictureBox14.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro2
        Me.PictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox14.Location = New System.Drawing.Point(25, 442)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(108, 40)
        Me.PictureBox14.TabIndex = 51
        Me.PictureBox14.TabStop = False
        '
        'pbExtraProp
        '
        Me.pbExtraProp.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbExtraProp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbExtraProp.Location = New System.Drawing.Point(25, 398)
        Me.pbExtraProp.Name = "pbExtraProp"
        Me.pbExtraProp.Size = New System.Drawing.Size(108, 35)
        Me.pbExtraProp.TabIndex = 48
        Me.pbExtraProp.TabStop = False
        '
        'pbImpVentas
        '
        Me.pbImpVentas.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbImpVentas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbImpVentas.Location = New System.Drawing.Point(25, 357)
        Me.pbImpVentas.Name = "pbImpVentas"
        Me.pbImpVentas.Size = New System.Drawing.Size(108, 35)
        Me.pbImpVentas.TabIndex = 45
        Me.pbImpVentas.TabStop = False
        '
        'pbImpServ
        '
        Me.pbImpServ.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbImpServ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbImpServ.Location = New System.Drawing.Point(25, 316)
        Me.pbImpServ.Name = "pbImpServ"
        Me.pbImpServ.Size = New System.Drawing.Size(108, 35)
        Me.pbImpServ.TabIndex = 42
        Me.pbImpServ.TabStop = False
        '
        'pbDescuento
        '
        Me.pbDescuento.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbDescuento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbDescuento.Location = New System.Drawing.Point(25, 275)
        Me.pbDescuento.Name = "pbDescuento"
        Me.pbDescuento.Size = New System.Drawing.Size(108, 35)
        Me.pbDescuento.TabIndex = 39
        Me.pbDescuento.TabStop = False
        '
        'pbDolares
        '
        Me.pbDolares.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbDolares.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbDolares.Location = New System.Drawing.Point(253, 179)
        Me.pbDolares.Name = "pbDolares"
        Me.pbDolares.Size = New System.Drawing.Size(108, 35)
        Me.pbDolares.TabIndex = 34
        Me.pbDolares.TabStop = False
        '
        'pbColones
        '
        Me.pbColones.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbColones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbColones.Location = New System.Drawing.Point(138, 179)
        Me.pbColones.Name = "pbColones"
        Me.pbColones.Size = New System.Drawing.Size(108, 35)
        Me.pbColones.TabIndex = 32
        Me.pbColones.TabStop = False
        '
        'pbComisionista
        '
        Me.pbComisionista.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbComisionista.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbComisionista.Location = New System.Drawing.Point(23, 123)
        Me.pbComisionista.Name = "pbComisionista"
        Me.pbComisionista.Size = New System.Drawing.Size(108, 31)
        Me.pbComisionista.TabIndex = 28
        Me.pbComisionista.TabStop = False
        '
        'pbCliente
        '
        Me.pbCliente.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbCliente.Location = New System.Drawing.Point(23, 72)
        Me.pbCliente.Name = "pbCliente"
        Me.pbCliente.Size = New System.Drawing.Size(108, 40)
        Me.pbCliente.TabIndex = 25
        Me.pbCliente.TabStop = False
        '
        'pbServH
        '
        Me.pbServH.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbServH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbServH.Location = New System.Drawing.Point(479, 12)
        Me.pbServH.Name = "pbServH"
        Me.pbServH.Size = New System.Drawing.Size(108, 35)
        Me.pbServH.TabIndex = 22
        Me.pbServH.TabStop = False
        '
        'pbCortesia
        '
        Me.pbCortesia.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbCortesia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbCortesia.Location = New System.Drawing.Point(365, 11)
        Me.pbCortesia.Name = "pbCortesia"
        Me.pbCortesia.Size = New System.Drawing.Size(108, 35)
        Me.pbCortesia.TabIndex = 20
        Me.pbCortesia.TabStop = False
        '
        'pbCargoHab
        '
        Me.pbCargoHab.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbCargoHab.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbCargoHab.Location = New System.Drawing.Point(252, 11)
        Me.pbCargoHab.Name = "pbCargoHab"
        Me.pbCargoHab.Size = New System.Drawing.Size(108, 35)
        Me.pbCargoHab.TabIndex = 18
        Me.pbCargoHab.TabStop = False
        '
        'pbCredito
        '
        Me.pbCredito.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbCredito.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbCredito.Location = New System.Drawing.Point(138, 11)
        Me.pbCredito.Name = "pbCredito"
        Me.pbCredito.Size = New System.Drawing.Size(108, 35)
        Me.pbCredito.TabIndex = 16
        Me.pbCredito.TabStop = False
        '
        'pbContado
        '
        Me.pbContado.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbContado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbContado.Location = New System.Drawing.Point(23, 11)
        Me.pbContado.Name = "pbContado"
        Me.pbContado.Size = New System.Drawing.Size(108, 35)
        Me.pbContado.TabIndex = 14
        Me.pbContado.TabStop = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Image = Global.SIALIBEB.My.Resources.Resources.Select2
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(381, 415)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(160, 67)
        Me.Button1.TabIndex = 55
        Me.Button1.Text = "Registrar"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.White
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(492, 376)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(175, 16)
        Me.Label19.TabIndex = 57
        Me.Label19.Text = "Servicio de Restaurante"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pbSR
        '
        Me.pbSR.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.pbSR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbSR.Location = New System.Drawing.Point(479, 373)
        Me.pbSR.Name = "pbSR"
        Me.pbSR.Size = New System.Drawing.Size(204, 23)
        Me.pbSR.TabIndex = 56
        Me.pbSR.TabStop = False
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Image = Global.SIALIBEB.My.Resources.Resources.action_delete
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(582, 415)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(99, 67)
        Me.Button2.TabIndex = 58
        Me.Button2.Text = "Salir"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = True
        '
        'frmFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(719, 511)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.pbSR)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.dgDetalle)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.PictureBox14)
        Me.Controls.Add(Me.txtExtraProp)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.pbExtraProp)
        Me.Controls.Add(Me.txtImpVentas)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.pbImpVentas)
        Me.Controls.Add(Me.txtImpServ)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.pbImpServ)
        Me.Controls.Add(Me.txtDescuento)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.pbDescuento)
        Me.Controls.Add(Me.txtSubtotal)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.pbDolares)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.pbColones)
        Me.Controls.Add(Me.txtComisionista)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.pbComisionista)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.pbCliente)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.pbServH)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.pbCortesia)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.pbCargoHab)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pbCredito)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.pbContado)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFactura"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Factura"
        CType(Me.dgDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbExtraProp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbImpVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbImpServ, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbDolares, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbColones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbComisionista, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbServH, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbCortesia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbCargoHab, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbCredito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbContado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbSR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents pbContado As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pbCredito As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pbCargoHab As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pbCortesia As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents pbServH As System.Windows.Forms.PictureBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents pbCliente As System.Windows.Forms.PictureBox
    Friend WithEvents txtCliente As System.Windows.Forms.TextBox
    Friend WithEvents txtComisionista As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents pbComisionista As System.Windows.Forms.PictureBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents pbDolares As System.Windows.Forms.PictureBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents pbColones As System.Windows.Forms.PictureBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtSubtotal As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents pbDescuento As System.Windows.Forms.PictureBox
    Friend WithEvents txtDescuento As System.Windows.Forms.TextBox
    Friend WithEvents txtImpServ As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents pbImpServ As System.Windows.Forms.PictureBox
    Friend WithEvents txtImpVentas As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents pbImpVentas As System.Windows.Forms.PictureBox
    Friend WithEvents txtExtraProp As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents pbExtraProp As System.Windows.Forms.PictureBox
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents pbSR As System.Windows.Forms.PictureBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
