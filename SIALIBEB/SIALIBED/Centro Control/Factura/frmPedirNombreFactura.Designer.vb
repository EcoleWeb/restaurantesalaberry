﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPedirNombreFactura
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtNombreFactura = New System.Windows.Forms.TextBox()
        Me.lbNombreFactura = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtNombreFactura
        '
        Me.txtNombreFactura.Location = New System.Drawing.Point(33, 26)
        Me.txtNombreFactura.Name = "txtNombreFactura"
        Me.txtNombreFactura.Size = New System.Drawing.Size(281, 20)
        Me.txtNombreFactura.TabIndex = 0
        '
        'lbNombreFactura
        '
        Me.lbNombreFactura.AutoSize = True
        Me.lbNombreFactura.Location = New System.Drawing.Point(30, 9)
        Me.lbNombreFactura.Name = "lbNombreFactura"
        Me.lbNombreFactura.Size = New System.Drawing.Size(118, 13)
        Me.lbNombreFactura.TabIndex = 1
        Me.lbNombreFactura.Text = "Nombre para la factura:"
        '
        'frmPedirNombreFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(341, 65)
        Me.Controls.Add(Me.lbNombreFactura)
        Me.Controls.Add(Me.txtNombreFactura)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPedirNombreFactura"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nombre Factura..."
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtNombreFactura As TextBox
    Friend WithEvents lbNombreFactura As Label
End Class
