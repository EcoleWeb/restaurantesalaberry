Imports System.Data.SqlClient

Public Class GrupoMenu

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim ruta As String
    Dim ced As String
    Dim PMU As New PerfilModulo_Class
    Dim cedula, usuario As String
    Public posicion As Integer
#End Region

#Region "Load"
    Private Sub GrupoMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ToolBar1.Buttons(0).Enabled = False
        ToolBar1.Buttons(1).Enabled = False
        ToolBar1.Buttons(2).Enabled = True

        conectadobd = cConexion.Conectar("Restaurante")
        If Trim(txtIDGrupo.Text) <> vbNullString Then
            Cargar()
        End If

        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
      
        If gloNoClave Then
            Loggin_Usuario()
        Else
            TextBox1.Focus()
        End If
      
        '---------------------------------------------------------------
    End Sub

    Private Sub Cargar()
        Dim SourceImage As Bitmap
        Try
            cConexion.GetRecorset(conectadobd, "Select Nombre_Grupo, ISNULL(ImagenRuta, '') AS ImagenRuta, ISNULL(CuentaIngreso,'') AS CuentaIngreso, ISNULL(CuentaCosto,'') AS CuentaCosto, ISNULL(DescripcionCuentaIngreso,'') AS DescripcionCuentaIngreso, ISNULL(DescripcionCuentaCosto,'') AS DescripcionCuentaCosto from grupos_menu where id=" & txtIDGrupo.Text, rs)
            While rs.Read
                txtnombre.Text = rs("Nombre_Grupo")
                ruta = rs("ImagenRuta")
                txtCuentaIngreso.Text = rs("CuentaIngreso")
                txtCosto.Text = rs("CuentaCosto")
                txtDescripcionCuentaIngreso.Text = rs("DescripcionCuentaIngreso")
                txtDescripcionCuentaCosto.Text = rs("DescripcionCuentaCosto")
            End While
            rs.Close()

            Try
                SourceImage = New Bitmap(ruta)
                btCargar.Image = SourceImage
            Catch ex As Exception
            End Try

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
#End Region

#Region "ToolBar"
    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Registrar()
            Case 2 : Eliminar()
            Case 3 : Close()
        End Select
    End Sub
#End Region

#Region "Registrar"
    Private Sub Registrar()
        Try
            If Trim(txtnombre.Text) = vbNullString Or Trim(txtCuentaIngreso.Text) = vbNullString Or Trim(txtCosto.Text) = vbNullString Then
                MessageBox.Show("Rellene todos los datos")
                Exit Sub
            End If

            If Trim(txtIDGrupo.Text) = vbNullString Then
                cConexion.AddNewRecord(conectadobd, "Grupos_Menu", "nombre_grupo,cedUsuario,nombre_usuario,imagenRuta,posicion,CuentaIngreso,DescripcionCuentaIngreso,CuentaCosto,DescripcionCuentaCosto", "'" & txtnombre.Text & "','" & cedula & "','" & usuario & "','" & ruta & "'," & posicion & ",'" & txtCuentaIngreso.Text & "','" & txtDescripcionCuentaIngreso.Text & "','" & txtCosto.Text & "','" & txtDescripcionCuentaCosto.Text & "'")
            Else
                cConexion.UpdateRecords(conectadobd, "Grupos_menu", "nombre_grupo='" & txtnombre.Text & "',cedUsuario='" & cedula & "',nombre_usuario='" & usuario & "',imagenRuta='" & ruta & "',posicion=" & posicion & ",CuentaIngreso= '" & txtCuentaIngreso.Text & "',DescripcionCuentaIngreso = '" & txtDescripcionCuentaIngreso.Text & "',CuentaCosto= '" & txtCosto.Text & "',DescripcionCuentaCosto = '" & txtDescripcionCuentaCosto.Text & "'", " id=" & txtIDGrupo.Text)
            End If

            MessageBox.Show("El registro se realizo correctamente")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        Finally
            Close()
        End Try
    End Sub
#End Region

#Region "Eliminar"
    Private Sub Eliminar()
        Try
            If Trim(txtIDGrupo.Text) = vbNullString Then
                Exit Sub
            Else
                cConexion.SlqExecute(conectadobd, "Delete from Grupos_Menu where id=" & txtIDGrupo.Text)
                Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
#End Region

#Region "Cerrar"
    Private Sub GrupoMenu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub
#End Region

#Region "Cuenta Contable"
    Private Sub buscacuenta(ByVal a1 As Object, ByVal a2 As Object)
        'Buscar Cuenta Contable
        Dim Fbuscador As New buscador
        Dim Conexion As New ConexionR
        Fbuscador.cantidad = 4
        Fbuscador.bd = "Contabilidad"
        Fbuscador.tabla = "CuentaContable"
        Fbuscador.lblEncabezado.Text = "          Buscador de Cuentas"
        Fbuscador.TituloModulo.Text = "Cuentas Contables"
        Fbuscador.consulta = "Select CuentaContable, Descripcion, Tipo, CuentaMadre from CuentaContable where Movimiento=1"
        Fbuscador.ShowDialog()
        a1.Text = Fbuscador.cuenta
        a2.Text = Conexion.SlqExecuteScalar(Conexion.Conectar("Contabilidad"), "Select Descripcion from contabilidad.dbo.cuentacontable where cuentaContable='" & Fbuscador.cuenta & "'")
        Fbuscador.Dispose()
    End Sub
#End Region

#Region "Imagen"
    Private Sub btCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCargar.Click
        Dim fImagen As New Imagen
        Try
            fImagen.PictureBox1.Image = btCargar.Image
            fImagen.ShowDialog()
            btCargar.Image = fImagen.PictureBox1.Image
            ruta = fImagen.ruta
            fImagen.Dispose()
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Controles"
    Private Sub txtnombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtnombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtCuentaIngreso.Focus()
        End If
    End Sub

    Private Sub txtCosto_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCosto.KeyDown
        If e.KeyCode = Keys.F1 Then
            buscacuenta(txtCosto, txtDescripcionCuentaCosto)
        End If
    End Sub

    Private Sub txtCuentaIngreso_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCuentaIngreso.KeyDown
        If e.KeyCode = Keys.F1 Then
            buscacuenta(txtCuentaIngreso, txtDescripcionCuentaIngreso)
        End If
    End Sub

    Private Sub txtCuentaIngreso_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCuentaIngreso.KeyPress
        If e.KeyChar = Chr(13) Then
            Dim datos As String
            Dim Conexion As New ConexionR

            Try
                datos = Conexion.SlqExecuteScalar(Conexion.Conectar("Contabilidad"), "Select Descripcion from contabilidad.dbo.cuentacontable where cuentaContable='" & txtCuentaIngreso.Text & "'")
                If datos <> vbNullString Then
                    txtDescripcionCuentaIngreso.Text = datos
                    txtCosto.Focus()
                Else
                    MessageBox.Show("Cuenta Contable incorrecta")
                    txtCuentaIngreso.Focus()
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            End Try
        End If
    End Sub

    Private Sub txtCosto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCosto.KeyPress
        If e.KeyChar = Chr(13) Then
            Dim datos As String
            Dim Conexion As New ConexionR

            Try
                datos = Conexion.SlqExecuteScalar(Conexion.Conectar("Contabilidad"), "Select Descripcion from contabilidad.dbo.cuentacontable where cuentaContable='" & txtCosto.Text & "'")
                If datos <> vbNullString Then
                    txtDescripcionCuentaCosto.Text = datos
                Else
                    MessageBox.Show("Cuenta Contable incorrecta")
                    txtCosto.Focus()
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            End Try
        End If
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                rs.Close()
            Catch ex As Exception
            End Try

            Try
                cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox1.Text & "'")
                usuario = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

                If usuario <> "" Then
                    TextBox1.Text = ""
                    Me.lbUsuario.Text = usuario
                    ToolBar1.Buttons(0).Enabled = True
                    ToolBar1.Buttons(1).Enabled = True
                    txtnombre.Focus()
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            End Try
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                TextBox1.Text = ""
                ToolBar1.Buttons(0).Enabled = True
                ToolBar1.Buttons(1).Enabled = True
                cedula = User_Log.Cedula
                usuario = User_Log.Nombre
                Me.lbUsuario.Text = User_Log.Nombre
                txtnombre.Focus()
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

End Class