
Public Class GrupoPrincipal

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Public idGrupo As String = "0"
#End Region

    Private Sub GrupoPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        crearGrupos()
    End Sub

    Private Sub crearGrupos()

        Dim aButton As Button
        Dim i, ii, X, Y, n As Integer

        Panel1.Controls.Clear()

        For i = 1 To 7
            For ii = 1 To 6
                aButton = New Button
                aButton.Top = Y
                aButton.Left = X
                aButton.Width = 120
                aButton.Height = 80
                aButton.Name = n
                aButton.Visible = False
                AddHandler aButton.Click, AddressOf Clickbuttons
                Me.Panel1.Controls.Add(aButton)
                X = X + 122
                n = n + 1
            Next
            Y = Y + 82
            X = 0
        Next

        UbicarPosiciones()

    End Sub

    Private Sub UbicarPosiciones()
        Dim ruta As String
        cConexion.GetRecorset(cConexion.Conectar(), "select ID,Nombre_grupo,posicion,ImagenRuta from grupos_menu  ORDER BY Posicion", rs)
        While rs.Read
            Panel1.Controls.Item(CInt(rs("posicion"))).Text = rs("nombre_grupo")
            Panel1.Controls.Item(CInt(rs("posicion"))).Name &= "-" & rs("ID")
            Panel1.Controls.Item(CInt(rs("posicion"))).Visible = True

            ruta = rs("imagenRuta")
            If ruta.Trim.Length > 0 Then
                Try
                    Dim SourceImage As Bitmap
                    SourceImage = New Bitmap(ruta)
                    CType(Panel1.Controls.Item(CInt(rs("posicion"))), Button).Image = SourceImage
                    'CType(Panel1.Controls.Item(CInt(rs("posicion"))), Button).TextImageRelation = TextImageRelation.ImageAboveText
                    'CType(Panel1.Controls.Item(CInt(rs("posicion"))), Button).ImageAlign = ContentAlignment.MiddleCenter
                Catch ex As Exception

                End Try
            End If
        End While
        rs.Close()
    End Sub


    Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs) 'todos los eventos de botones
        Dim arregloNombre() As String
        arregloNombre = sender.name.ToString.Split("-")
        idGrupo = arregloNombre(1)
        Close()        
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        idGrupo = -1
        Close()
    End Sub
End Class