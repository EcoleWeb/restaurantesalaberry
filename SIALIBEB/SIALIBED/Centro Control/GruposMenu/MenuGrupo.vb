Public Class MenuGrupo

#Region "variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim cedula As String
    Dim PMU As New PerfilModulo_Class
#End Region

#Region "Cerrar"
    Private Sub MenuGrupo_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            cConexion.DesConectar(conectadobd)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Close()
    End Sub
#End Region

#Region "Load"
    Private Sub MenuGrupo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            conectadobd = cConexion.Conectar("Restaurante")
            Carga()
            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO
          
            If gloNoClave Then
                Loggin_Usuario()
            Else
                TextBox1.Focus()
            End If
      
            '---------------------------------------------------------------
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Carga()
        Try
            cargaDatos()
            Ubicar()
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Grupo"
    Private Sub cargaDatos()
        Dim aButton As Button
        Dim i, ii, X, Y, n As Integer

        Panel1.Controls.Clear()

        For i = 1 To 7
            For ii = 1 To 6
                aButton = New Button
                aButton.Top = Y
                aButton.Left = X
                aButton.Width = 120
                aButton.Height = 80
                aButton.Name = n
                AddHandler aButton.Click, AddressOf Clickbuttons
                Me.Panel1.Controls.Add(aButton)
                X = X + 122
                n = n + 1
            Next
            Y = Y + 82
            X = 0
        Next
    End Sub

    Private Sub Ubicar()
        Dim er As String = False
        Dim ruta As String
        Dim botones As Button

        cConexion.GetRecorset(conectadobd, "select ID,Nombre_grupo,posicion,ISNULL(ImagenRuta,'') AS ImagenRuta from grupos_menu", rs)
        While rs.Read
            botones = New Button
            botones = Panel1.Controls.Item(CInt(rs("posicion")))
            botones.Text = rs("Nombre_grupo") '  
            botones.Name = botones.Name & "-" & rs("ID") ' 
            ruta = rs("ImagenRuta")
            If ruta.Trim.Length > 0 Then
                Try

                    Dim SourceImage As Bitmap
                    SourceImage = New Bitmap(ruta)
                    botones.Image = SourceImage
                Catch ex As Exception
                    er = True
                End Try
            End If
            botones.TextImageRelation = TextImageRelation.ImageAboveText
            botones.ImageAlign = ContentAlignment.MiddleCenter
        End While
        rs.Close()

        If er = True Then
            MsgBox("Algunas imagenes fueron movidas de su ubicación original")
        End If
    End Sub

    Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim grupomenu As New GrupoMenu
            Dim datos() As String

            datos = CStr(sender.name).Split("-")
            grupomenu.posicion = datos(0)
            If datos.Length = 2 Then
                grupomenu.txtIDGrupo.Text = datos(1)
            End If
            grupomenu.ShowDialog()
            grupomenu.Dispose()
            cargaDatos()
            Ubicar()
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                rs.Close()
            Catch ex As Exception
            End Try

            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox1.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                TextBox1.Text = ""
                Me.Panel1.Enabled = True
                Me.lbUsuario.Text = usuario
            Else
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                TextBox1.Text = ""
                Me.Panel1.Enabled = True
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

End Class