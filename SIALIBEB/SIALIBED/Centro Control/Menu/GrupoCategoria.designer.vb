<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GruposMenu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GruposMenu))
        Me.PanelMesas = New System.Windows.Forms.Panel
        Me.Button4 = New System.Windows.Forms.Button
        Me.lbUsuario = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.NumericFilas = New System.Windows.Forms.NumericUpDown
        Me.NumericColumnas = New System.Windows.Forms.NumericUpDown
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.LabelTotal = New System.Windows.Forms.Label
        Me.cmbGrupos = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.NumericFilas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericColumnas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelMesas
        '
        resources.ApplyResources(Me.PanelMesas, "PanelMesas")
        Me.PanelMesas.Name = "PanelMesas"
        '
        'Button4
        '
        resources.ApplyResources(Me.Button4, "Button4")
        Me.Button4.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button4.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog1
        Me.Button4.Name = "Button4"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'lbUsuario
        '
        resources.ApplyResources(Me.lbUsuario, "lbUsuario")
        Me.lbUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.lbUsuario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbUsuario.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbUsuario.Name = "lbUsuario"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label10.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label10.Name = "Label10"
        '
        'TextBox1
        '
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Name = "TextBox1"
        '
        'NumericFilas
        '
        resources.ApplyResources(Me.NumericFilas, "NumericFilas")
        Me.NumericFilas.Name = "NumericFilas"
        Me.NumericFilas.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'NumericColumnas
        '
        resources.ApplyResources(Me.NumericColumnas, "NumericColumnas")
        Me.NumericColumnas.Name = "NumericColumnas"
        Me.NumericColumnas.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.Name = "Label2"
        '
        'LabelTotal
        '
        resources.ApplyResources(Me.LabelTotal, "LabelTotal")
        Me.LabelTotal.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelTotal.Name = "LabelTotal"
        '
        'cmbGrupos
        '
        resources.ApplyResources(Me.cmbGrupos, "cmbGrupos")
        Me.cmbGrupos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbGrupos.FormattingEnabled = True
        Me.cmbGrupos.Name = "cmbGrupos"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.Name = "Label3"
        '
        'GruposMenu
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbGrupos)
        Me.Controls.Add(Me.LabelTotal)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.NumericColumnas)
        Me.Controls.Add(Me.NumericFilas)
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.PanelMesas)
        Me.Controls.Add(Me.Label10)
        Me.Name = "GruposMenu"
        CType(Me.NumericFilas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericColumnas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelMesas As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents NumericFilas As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericColumnas As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LabelTotal As System.Windows.Forms.Label
    Friend WithEvents cmbGrupos As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
