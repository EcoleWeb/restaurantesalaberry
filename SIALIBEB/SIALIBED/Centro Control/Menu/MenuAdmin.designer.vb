<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MenuAdmin
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MenuAdmin))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtCodigoBarras = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtReceta = New System.Windows.Forms.TextBox()
        Me.txtCostoP = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.cboImpresoras = New System.Windows.Forms.ComboBox()
        Me.txtPUtilidadA = New System.Windows.Forms.TextBox()
        Me.txtPUtilidadB = New System.Windows.Forms.TextBox()
        Me.txtPUtilidadC = New System.Windows.Forms.TextBox()
        Me.txtPUtilidadD = New System.Windows.Forms.TextBox()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtPVentaA = New System.Windows.Forms.TextBox()
        Me.txtPVentaB = New System.Windows.Forms.TextBox()
        Me.txtPVentaC = New System.Windows.Forms.TextBox()
        Me.txtPVentaD = New System.Windows.Forms.TextBox()
        Me.txtFinalesD = New System.Windows.Forms.TextBox()
        Me.txtFinalesC = New System.Windows.Forms.TextBox()
        Me.txtFinalesB = New System.Windows.Forms.TextBox()
        Me.txtFinalesA = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.chkImpVentas = New System.Windows.Forms.CheckBox()
        Me.chkImServicio = New System.Windows.Forms.CheckBox()
        Me.chkModificadores = New System.Windows.Forms.CheckBox()
        Me.chkPlatos = New System.Windows.Forms.CheckBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.BtImagen = New System.Windows.Forms.Button()
        Me.rbarticulo = New System.Windows.Forms.RadioButton()
        Me.rbreceta = New System.Windows.Forms.RadioButton()
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton()
        Me.ToolBar2 = New System.Windows.Forms.ToolBar()
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton()
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.txtAcompanamientos = New System.Windows.Forms.NumericUpDown()
        Me.lbUsuario = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ComboBoxMonedaVenta = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbDescarga = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtNombreIdioma = New System.Windows.Forms.TextBox()
        Me.CBsinimpresora = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.opNoSolicitar = New System.Windows.Forms.RadioButton()
        Me.opSinImpuesto = New System.Windows.Forms.RadioButton()
        Me.opConImpuestos = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.nuComision = New System.Windows.Forms.NumericUpDown()
        Me.lbComision = New System.Windows.Forms.Label()
        Me.btCentroProduccion = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtPFinalExpressA = New System.Windows.Forms.TextBox()
        Me.txtPVentaExpressA = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtUtilidadExpressA = New System.Windows.Forms.TextBox()
        Me.txtPVentaExpressM = New System.Windows.Forms.TextBox()
        Me.txtUtilidadExpressM = New System.Windows.Forms.TextBox()
        Me.txtPFinalExpressM = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        CType(Me.txtAcompanamientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.nuComision, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Name = "Label7"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Name = "Label11"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Name = "Label12"
        '
        'txtCodigoBarras
        '
        Me.txtCodigoBarras.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtCodigoBarras, "txtCodigoBarras")
        Me.txtCodigoBarras.Name = "txtCodigoBarras"
        '
        'txtNombre
        '
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtNombre, "txtNombre")
        Me.txtNombre.Name = "txtNombre"
        '
        'txtReceta
        '
        Me.txtReceta.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtReceta.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtReceta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtReceta, "txtReceta")
        Me.txtReceta.Name = "txtReceta"
        Me.txtReceta.ReadOnly = True
        Me.txtReceta.TabStop = False
        '
        'txtCostoP
        '
        Me.txtCostoP.BackColor = System.Drawing.Color.White
        Me.txtCostoP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCostoP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtCostoP, "txtCostoP")
        Me.txtCostoP.Name = "txtCostoP"
        Me.txtCostoP.ReadOnly = True
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'cboImpresoras
        '
        Me.cboImpresoras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.cboImpresoras, "cboImpresoras")
        Me.cboImpresoras.FormattingEnabled = True
        Me.cboImpresoras.Name = "cboImpresoras"
        '
        'txtPUtilidadA
        '
        Me.txtPUtilidadA.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPUtilidadA, "txtPUtilidadA")
        Me.txtPUtilidadA.Name = "txtPUtilidadA"
        '
        'txtPUtilidadB
        '
        Me.txtPUtilidadB.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPUtilidadB, "txtPUtilidadB")
        Me.txtPUtilidadB.Name = "txtPUtilidadB"
        '
        'txtPUtilidadC
        '
        Me.txtPUtilidadC.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPUtilidadC, "txtPUtilidadC")
        Me.txtPUtilidadC.Name = "txtPUtilidadC"
        '
        'txtPUtilidadD
        '
        Me.txtPUtilidadD.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPUtilidadD, "txtPUtilidadD")
        Me.txtPUtilidadD.Name = "txtPUtilidadD"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtObservaciones, "txtObservaciones")
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.Name = "Label16"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'txtPVentaA
        '
        Me.txtPVentaA.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPVentaA.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPVentaA, "txtPVentaA")
        Me.txtPVentaA.Name = "txtPVentaA"
        '
        'txtPVentaB
        '
        Me.txtPVentaB.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPVentaB.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPVentaB, "txtPVentaB")
        Me.txtPVentaB.Name = "txtPVentaB"
        '
        'txtPVentaC
        '
        Me.txtPVentaC.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPVentaC.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPVentaC, "txtPVentaC")
        Me.txtPVentaC.Name = "txtPVentaC"
        '
        'txtPVentaD
        '
        Me.txtPVentaD.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPVentaD.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPVentaD, "txtPVentaD")
        Me.txtPVentaD.Name = "txtPVentaD"
        '
        'txtFinalesD
        '
        Me.txtFinalesD.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtFinalesD, "txtFinalesD")
        Me.txtFinalesD.Name = "txtFinalesD"
        '
        'txtFinalesC
        '
        Me.txtFinalesC.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtFinalesC, "txtFinalesC")
        Me.txtFinalesC.Name = "txtFinalesC"
        '
        'txtFinalesB
        '
        Me.txtFinalesB.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtFinalesB, "txtFinalesB")
        Me.txtFinalesB.Name = "txtFinalesB"
        '
        'txtFinalesA
        '
        Me.txtFinalesA.AcceptsReturn = True
        Me.txtFinalesA.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtFinalesA, "txtFinalesA")
        Me.txtFinalesA.Name = "txtFinalesA"
        '
        'Label20
        '
        resources.ApplyResources(Me.Label20, "Label20")
        Me.Label20.Name = "Label20"
        '
        'chkImpVentas
        '
        resources.ApplyResources(Me.chkImpVentas, "chkImpVentas")
        Me.chkImpVentas.Name = "chkImpVentas"
        Me.chkImpVentas.UseVisualStyleBackColor = True
        '
        'chkImServicio
        '
        resources.ApplyResources(Me.chkImServicio, "chkImServicio")
        Me.chkImServicio.Name = "chkImServicio"
        Me.chkImServicio.UseVisualStyleBackColor = True
        '
        'chkModificadores
        '
        resources.ApplyResources(Me.chkModificadores, "chkModificadores")
        Me.chkModificadores.Name = "chkModificadores"
        Me.chkModificadores.UseVisualStyleBackColor = True
        '
        'chkPlatos
        '
        resources.ApplyResources(Me.chkPlatos, "chkPlatos")
        Me.chkPlatos.Name = "chkPlatos"
        Me.chkPlatos.UseVisualStyleBackColor = True
        '
        'Label21
        '
        resources.ApplyResources(Me.Label21, "Label21")
        Me.Label21.Name = "Label21"
        '
        'BtImagen
        '
        Me.BtImagen.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.BtImagen, "BtImagen")
        Me.BtImagen.Name = "BtImagen"
        Me.BtImagen.UseVisualStyleBackColor = False
        '
        'rbarticulo
        '
        resources.ApplyResources(Me.rbarticulo, "rbarticulo")
        Me.rbarticulo.Checked = True
        Me.rbarticulo.FlatAppearance.BorderSize = 0
        Me.rbarticulo.Name = "rbarticulo"
        Me.rbarticulo.TabStop = True
        Me.rbarticulo.UseVisualStyleBackColor = True
        '
        'rbreceta
        '
        resources.ApplyResources(Me.rbreceta, "rbreceta")
        Me.rbreceta.FlatAppearance.BorderSize = 0
        Me.rbreceta.Name = "rbreceta"
        Me.rbreceta.UseVisualStyleBackColor = True
        '
        'ToolBarCerrar
        '
        resources.ApplyResources(Me.ToolBarCerrar, "ToolBarCerrar")
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        '
        'ToolBarImprimir
        '
        resources.ApplyResources(Me.ToolBarImprimir, "ToolBarImprimir")
        Me.ToolBarImprimir.Name = "ToolBarImprimir"
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        '
        'ToolBarBuscar
        '
        resources.ApplyResources(Me.ToolBarBuscar, "ToolBarBuscar")
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        '
        'ToolBarEditar
        '
        resources.ApplyResources(Me.ToolBarEditar, "ToolBarEditar")
        Me.ToolBarEditar.Name = "ToolBarEditar"
        '
        'ToolBarNuevo
        '
        resources.ApplyResources(Me.ToolBarNuevo, "ToolBarNuevo")
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        '
        'ToolBar2
        '
        resources.ApplyResources(Me.ToolBar2, "ToolBar2")
        Me.ToolBar2.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarEditar, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarButton1, Me.ToolBarCerrar, Me.ToolBarButton2})
        Me.ToolBar2.ImageList = Me.ImageList
        Me.ToolBar2.Name = "ToolBar2"
        '
        'ToolBarButton1
        '
        resources.ApplyResources(Me.ToolBarButton1, "ToolBarButton1")
        Me.ToolBarButton1.Name = "ToolBarButton1"
        '
        'ToolBarButton2
        '
        resources.ApplyResources(Me.ToolBarButton2, "ToolBarButton2")
        Me.ToolBarButton2.Name = "ToolBarButton2"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "")
        Me.ImageList.Images.SetKeyName(9, "teclado1.gif")
        Me.ImageList.Images.SetKeyName(10, "")
        '
        'txtAcompanamientos
        '
        resources.ApplyResources(Me.txtAcompanamientos, "txtAcompanamientos")
        Me.txtAcompanamientos.Name = "txtAcompanamientos"
        '
        'lbUsuario
        '
        resources.ApplyResources(Me.lbUsuario, "lbUsuario")
        Me.lbUsuario.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.lbUsuario.ForeColor = System.Drawing.Color.White
        Me.lbUsuario.Name = "lbUsuario"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'TextBox1
        '
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Name = "TextBox1"
        '
        'ComboBoxMonedaVenta
        '
        Me.ComboBoxMonedaVenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.ComboBoxMonedaVenta, "ComboBoxMonedaVenta")
        Me.ComboBoxMonedaVenta.FormattingEnabled = True
        Me.ComboBoxMonedaVenta.Name = "ComboBoxMonedaVenta"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Name = "Label4"
        '
        'cmbDescarga
        '
        Me.cmbDescarga.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDescarga.FormattingEnabled = True
        resources.ApplyResources(Me.cmbDescarga, "cmbDescarga")
        Me.cmbDescarga.Name = "cmbDescarga"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.ComboBox1, "ComboBox1")
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Name = "ComboBox1"
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.Name = "Label19"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DecimalPlaces = 2
        resources.ApplyResources(Me.NumericUpDown1, "NumericUpDown1")
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {1, 0, 0, 196608})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label22
        '
        resources.ApplyResources(Me.Label22, "Label22")
        Me.Label22.Name = "Label22"
        '
        'Label23
        '
        resources.ApplyResources(Me.Label23, "Label23")
        Me.Label23.Name = "Label23"
        '
        'txtNombreIdioma
        '
        Me.txtNombreIdioma.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreIdioma.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtNombreIdioma, "txtNombreIdioma")
        Me.txtNombreIdioma.Name = "txtNombreIdioma"
        '
        'CBsinimpresora
        '
        resources.ApplyResources(Me.CBsinimpresora, "CBsinimpresora")
        Me.CBsinimpresora.Name = "CBsinimpresora"
        Me.CBsinimpresora.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.opNoSolicitar)
        Me.GroupBox1.Controls.Add(Me.opSinImpuesto)
        Me.GroupBox1.Controls.Add(Me.opConImpuestos)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'opNoSolicitar
        '
        resources.ApplyResources(Me.opNoSolicitar, "opNoSolicitar")
        Me.opNoSolicitar.Checked = True
        Me.opNoSolicitar.Name = "opNoSolicitar"
        Me.opNoSolicitar.TabStop = True
        Me.opNoSolicitar.UseVisualStyleBackColor = True
        '
        'opSinImpuesto
        '
        resources.ApplyResources(Me.opSinImpuesto, "opSinImpuesto")
        Me.opSinImpuesto.Name = "opSinImpuesto"
        Me.opSinImpuesto.UseVisualStyleBackColor = True
        '
        'opConImpuestos
        '
        resources.ApplyResources(Me.opConImpuestos, "opConImpuestos")
        Me.opConImpuestos.Name = "opConImpuestos"
        Me.opConImpuestos.UseVisualStyleBackColor = True
        '
        'Button1
        '
        resources.ApplyResources(Me.Button1, "Button1")
        Me.Button1.Name = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.rbreceta)
        Me.Panel1.Controls.Add(Me.rbarticulo)
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'nuComision
        '
        Me.nuComision.DecimalPlaces = 2
        resources.ApplyResources(Me.nuComision, "nuComision")
        Me.nuComision.Name = "nuComision"
        '
        'lbComision
        '
        resources.ApplyResources(Me.lbComision, "lbComision")
        Me.lbComision.Name = "lbComision"
        '
        'btCentroProduccion
        '
        Me.btCentroProduccion.BackColor = System.Drawing.Color.Tomato
        resources.ApplyResources(Me.btCentroProduccion, "btCentroProduccion")
        Me.btCentroProduccion.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btCentroProduccion.Name = "btCentroProduccion"
        Me.btCentroProduccion.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.txtFinalesA)
        Me.GroupBox2.Controls.Add(Me.txtFinalesD)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.txtPVentaD)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtPFinalExpressA)
        Me.GroupBox3.Controls.Add(Me.txtPVentaExpressA)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.Label27)
        Me.GroupBox3.Controls.Add(Me.txtUtilidadExpressA)
        Me.GroupBox3.Controls.Add(Me.txtPVentaExpressM)
        Me.GroupBox3.Controls.Add(Me.txtUtilidadExpressM)
        Me.GroupBox3.Controls.Add(Me.txtPFinalExpressM)
        Me.GroupBox3.Controls.Add(Me.Label25)
        Me.GroupBox3.Controls.Add(Me.Label28)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'txtPFinalExpressA
        '
        Me.txtPFinalExpressA.AcceptsReturn = True
        Me.txtPFinalExpressA.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPFinalExpressA, "txtPFinalExpressA")
        Me.txtPFinalExpressA.Name = "txtPFinalExpressA"
        '
        'txtPVentaExpressA
        '
        Me.txtPVentaExpressA.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPVentaExpressA.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPVentaExpressA, "txtPVentaExpressA")
        Me.txtPVentaExpressA.Name = "txtPVentaExpressA"
        '
        'Label26
        '
        resources.ApplyResources(Me.Label26, "Label26")
        Me.Label26.Name = "Label26"
        '
        'Label24
        '
        resources.ApplyResources(Me.Label24, "Label24")
        Me.Label24.Name = "Label24"
        '
        'Label27
        '
        resources.ApplyResources(Me.Label27, "Label27")
        Me.Label27.Name = "Label27"
        '
        'txtUtilidadExpressA
        '
        Me.txtUtilidadExpressA.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtUtilidadExpressA, "txtUtilidadExpressA")
        Me.txtUtilidadExpressA.Name = "txtUtilidadExpressA"
        '
        'txtPVentaExpressM
        '
        Me.txtPVentaExpressM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPVentaExpressM.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPVentaExpressM, "txtPVentaExpressM")
        Me.txtPVentaExpressM.Name = "txtPVentaExpressM"
        '
        'txtUtilidadExpressM
        '
        Me.txtUtilidadExpressM.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtUtilidadExpressM, "txtUtilidadExpressM")
        Me.txtUtilidadExpressM.Name = "txtUtilidadExpressM"
        '
        'txtPFinalExpressM
        '
        Me.txtPFinalExpressM.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPFinalExpressM, "txtPFinalExpressM")
        Me.txtPFinalExpressM.Name = "txtPFinalExpressM"
        '
        'Label25
        '
        resources.ApplyResources(Me.Label25, "Label25")
        Me.Label25.Name = "Label25"
        '
        'Label28
        '
        resources.ApplyResources(Me.Label28, "Label28")
        Me.Label28.Name = "Label28"
        '
        'MenuAdmin
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.txtPVentaA)
        Me.Controls.Add(Me.txtPUtilidadD)
        Me.Controls.Add(Me.txtPUtilidadA)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btCentroProduccion)
        Me.Controls.Add(Me.nuComision)
        Me.Controls.Add(Me.lbComision)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CBsinimpresora)
        Me.Controls.Add(Me.txtNombreIdioma)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.NumericUpDown1)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cmbDescarga)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ComboBoxMonedaVenta)
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.txtAcompanamientos)
        Me.Controls.Add(Me.ToolBar2)
        Me.Controls.Add(Me.BtImagen)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.chkPlatos)
        Me.Controls.Add(Me.chkModificadores)
        Me.Controls.Add(Me.chkImServicio)
        Me.Controls.Add(Me.chkImpVentas)
        Me.Controls.Add(Me.txtFinalesC)
        Me.Controls.Add(Me.txtFinalesB)
        Me.Controls.Add(Me.txtPVentaC)
        Me.Controls.Add(Me.txtPVentaB)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.txtPUtilidadC)
        Me.Controls.Add(Me.txtPUtilidadB)
        Me.Controls.Add(Me.cboImpresoras)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtCostoP)
        Me.Controls.Add(Me.txtReceta)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.txtCodigoBarras)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "MenuAdmin"
        CType(Me.txtAcompanamientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.nuComision, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoBarras As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtReceta As System.Windows.Forms.TextBox
    Friend WithEvents txtCostoP As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cboImpresoras As System.Windows.Forms.ComboBox
    Friend WithEvents txtPUtilidadA As System.Windows.Forms.TextBox
    Friend WithEvents txtPUtilidadB As System.Windows.Forms.TextBox
    Friend WithEvents txtPUtilidadC As System.Windows.Forms.TextBox
    Friend WithEvents txtPUtilidadD As System.Windows.Forms.TextBox
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtPVentaA As System.Windows.Forms.TextBox
    Friend WithEvents txtPVentaB As System.Windows.Forms.TextBox
    Friend WithEvents txtPVentaC As System.Windows.Forms.TextBox
    Friend WithEvents txtPVentaD As System.Windows.Forms.TextBox
    Friend WithEvents txtFinalesD As System.Windows.Forms.TextBox
    Friend WithEvents txtFinalesC As System.Windows.Forms.TextBox
    Friend WithEvents txtFinalesB As System.Windows.Forms.TextBox
    Friend WithEvents txtFinalesA As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents chkImpVentas As System.Windows.Forms.CheckBox
    Friend WithEvents chkImServicio As System.Windows.Forms.CheckBox
    Friend WithEvents chkModificadores As System.Windows.Forms.CheckBox
    Friend WithEvents chkPlatos As System.Windows.Forms.CheckBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents BtImagen As System.Windows.Forms.Button
    Friend WithEvents rbarticulo As System.Windows.Forms.RadioButton
    Friend WithEvents rbreceta As System.Windows.Forms.RadioButton
    Protected Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Public WithEvents ToolBar2 As System.Windows.Forms.ToolBar
    Friend WithEvents txtAcompanamientos As System.Windows.Forms.NumericUpDown
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Public WithEvents ImageList As System.Windows.Forms.ImageList
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxMonedaVenta As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbDescarga As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtNombreIdioma As System.Windows.Forms.TextBox
    Friend WithEvents CBsinimpresora As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents opSinImpuesto As System.Windows.Forms.RadioButton
    Friend WithEvents opConImpuestos As System.Windows.Forms.RadioButton
    Friend WithEvents opNoSolicitar As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents nuComision As System.Windows.Forms.NumericUpDown
    Friend WithEvents lbComision As System.Windows.Forms.Label
    Friend WithEvents btCentroProduccion As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtPFinalExpressA As TextBox
    Friend WithEvents txtPVentaExpressA As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents txtUtilidadExpressA As TextBox
    Friend WithEvents txtPVentaExpressM As TextBox
    Friend WithEvents txtUtilidadExpressM As TextBox
    Friend WithEvents txtPFinalExpressM As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents Label28 As Label
End Class
