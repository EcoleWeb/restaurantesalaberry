Imports System.Drawing.Printing

Public Class MenuAdmin

#Region "variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim impV, impS As Double
    Dim impVB, impSB As Integer
    Public posicion, categoria As Integer
    Dim ruta As String
    Dim origen As Integer
    Dim cedula As String
    Dim PMU As New PerfilModulo_Class
    Dim Dataset1 As New DataSet
    Dim inicio As Integer = 0
    Dim NuevoValor As Double = 0
    Dim puntero As Boolean = False
    Dim puntos As Integer = 0
    Dim ID_BODEGA As Integer = 7005
    Dim Receta_Completa As Boolean = True

    '*--------------------------------------------------------------------------------------------------------------------
    'VARIABLES NUEVAS Q VAN A SUSTITUIR LOS TXTBOX FANTASMA!!!!
    Public ID_ARTICULO As String
    Public ID_RECETA As String
    Public PORCION As String
    Public PRESENTACION As String
    Public COSTO_UNITARIO As String
    '*--------------------------------------------------------------------------------------------------------------------


#End Region

    Private Sub MenuAdmin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")

        If GetSetting("SeeSoft", "Restaurante", "OpcionesEspeciales") = 1 Then
            chkPlatos.Text = "Opciones Especiales"
        End If

        Try
            Impresoras()
        Catch ex As Exception
            Me.Close()
        End Try

        impV = User_Log.IVI
        impS = User_Log.ISS
        CargarMonedas()
        CargaBodegas()
        If ID_ARTICULO <> Nothing Then
            If ID_ARTICULO.Trim.Length > 0 Then
                Try
                    CargarArticulo()
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                rbarticulo.Checked = True
            End If
        End If
        ToolBar2.Buttons(0).Enabled = False
        ToolBar2.Buttons(1).Enabled = False
        ToolBar2.Buttons(2).Enabled = False
        ToolBar2.Buttons(5).Enabled = False
        EstadoControles(False)
        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
        cConexion.DesConectar(conectadobd)
        If gloNoClave Then
            Loggin_Usuario()
        Else
            TextBox1.Focus()
        End If
        conectadobd = cConexion.Conectar("Restaurante")
        '---------------------------------------------------------------

        If GetSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta").Equals("1") Then
            lbComision.Visible = True
            nuComision.Visible = True
        Else
            lbComision.Visible = False
            nuComision.Visible = False
            SaveSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta", 0)
        End If
    End Sub



    Private Sub CargarArticulo()
        Dim multiplo As Double = 1
        Dim costo As Double = 0
        Dim cIVentas As Boolean
        Dim cIServicio As Boolean
        Dim CostoAnterior As Double
        Dim CostoCalculado As Double
        Dim nombrePre() As String
        Dim e As New System.Windows.Forms.KeyEventArgs(Keys.Enter)
        Try
            cConexion.GetRecorset(conectadobd, "SELECT impVenta,impServ,Codigo_Barras, Nombre_Menu, NombreMenu_SegIdioma, Plato_Principal, Modificador_Forzado, Numero_Acompa�amientos, Id_Receta, UtilidadA, UtilidadB, UtilidadC, UtilidadD, Precio_VentaA, Precio_VentaB, Precio_VentaC, Precio_VentaD, Observaciones, Impresora, ImagenRuta, Tipo, Precio_Costo, moneda, bodega, cantidad, presentacion, CostoTotal,unitario,SinPrecio, SinPrecioImp, Sinimpresora, PorcentajeComision, UtilidadExpressA
      ,Precio_VentaExpressA, UtilidadExpressM, Precio_VentaExpressM  FROM Menu_Restaurante where id_menu=" & ID_ARTICULO, rs)
            While rs.Read
                'aqui vamoas a guardar el costo anterior para saber si cambio por la composicion de la receta
                CostoAnterior = rs("Precio_Costo")
                txtCodigoBarras.Text = rs("Codigo_Barras")
                txtNombre.Text = rs("Nombre_Menu")
                txtNombreIdioma.Text = rs("NombreMenu_SegIdioma")
                Me.CBsinimpresora.Checked = CBool(rs("Sinimpresora"))
                If rs("cantidad") <= 0 Then
                    NumericUpDown1.Value= 1
                Else
                    Me.NumericUpDown1.Value= rs("cantidad")
                End If
                Try
                    COSTO_UNITARIO = rs("Precio_Costo")
                    nombrePre = CStr(rs("presentacion")).Split("-")
                    PRESENTACION = nombrePre(0)
                Catch ex As Exception
                    PRESENTACION = ""
                End Try
                If rs("tipo") = 1 Then

                    CostoCalculado = CalculaCostoReceta(rs("id_receta"))
                Else
                    Dim cnx As New SqlClient.SqlConnection
                    cnx.ConnectionString = GetSetting("SeeSoft", "Proveeduria", "Conexion")
                    If cnx.State = ConnectionState.Closed Then cnx.Open()
                    CostoCalculado = cConexion.SlqExecuteScalar(cnx, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega WHERE Proveeduria.dbo.ArticulosXBodega.codigo = " & rs("id_receta") & " and Proveeduria.dbo.ArticulosXBodega.idBodega = " & rs("bodega"))
                    If cnx.State = ConnectionState.Open Then cnx.Close()
                End If
                'If rs("CostoTotal") = 0 Then
                'PORCION = rs("CostoTotal") * rs("Cantidad")
                'Else
                PORCION = rs("Precio_Costo")
                'End If

                chkPlatos.Checked = CBool(rs("Plato_Principal"))
                chkModificadores.Checked = CBool(rs("Modificador_Forzado"))

                cIServicio = CBool(rs("impServ"))
                cIVentas = CBool(rs("impVenta"))
                txtAcompanamientos.Text = rs("Numero_Acompa�amientos")
                ID_RECETA = rs("Id_Receta")
                chkImpVentas.Checked = cIVentas
                chkImServicio.Checked = cIServicio
                txtPUtilidadA.Text = rs("UtilidadA")
                txtPUtilidadB.Text = rs("UtilidadB")
                txtPUtilidadC.Text = rs("UtilidadC")
                txtPUtilidadD.Text = rs("UtilidadD")
                txtUtilidadExpressA.Text = rs("UtilidadExpressA")
                txtUtilidadExpressM.Text = rs("UtilidadExpressM")
                opSinImpuesto.Checked = rs("SinPrecio")
                opConImpuestos.Checked = rs("SinPrecioImp")

                Dim PreciosVentas(3) As Double
                PreciosVentas(0) = Format(rs("Precio_VentaA"), "###,##0.0000")
                PreciosVentas(1) = Format(rs("Precio_VentaB"), "###,##0.0000")
                PreciosVentas(2) = Format(rs("Precio_VentaC"), "###,##0.0000")
                PreciosVentas(3) = Format(rs("Precio_VentaD"), "###,##0.0000")

                costo = Format(rs("Precio_Costo"), "###,##0.0000")
                txtPVentaA.Text = Format(rs("Precio_VentaA"), "###,##0.0000")
                txtPVentaB.Text = Format(rs("Precio_VentaB"), "###,##0.0000")
                txtPVentaC.Text = Format(rs("Precio_VentaC"), "###,##0.0000")
                txtPVentaD.Text = Format(rs("Precio_VentaD"), "###,##0.0000")
                txtPVentaExpressA.Text = Format(rs("Precio_VentaExpressA"), "###,##0.0000")
                txtPVentaExpressM.Text = Format(rs("Precio_VentaExpressM"), "###,##0.0000")
                ComboBoxMonedaVenta.SelectedValue = rs("Moneda")
                puntero = True
                Moneda()
                cmbDescarga.SelectedValue = rs("bodega")
                txtObservaciones.Text = rs("Observaciones")
                cboImpresoras.Text = rs("Impresora")
                origen = rs("tipo")
                ruta = rs("ImagenRuta")

                txtPUtilidadA_KeyDown(txtPVentaA, e)
                txtPUtilidadA_KeyDown(txtPVentaB, e)
                txtPUtilidadA_KeyDown(txtPVentaC, e)
                txtPUtilidadA_KeyDown(txtPVentaD, e)
                txtPUtilidadA_KeyDown(txtPVentaExpressA, e)
                txtPUtilidadA_KeyDown(txtPVentaExpressM, e)



                If cboImpresoras.Text <> rs("Impresora") Then
                    'MsgBox("No se encuentra instalada la impresora ( " & rs("Impresora") & " ) en este equipo o se encuentra local" & vbCrLf & "Se agregar� al final de la Lista.", MsgBoxStyle.Information, "Atenci�n...")
                    cboImpresoras.Items.Add(rs("Impresora"))
                    cboImpresoras.Text = rs("Impresora")
                End If
                nuComision.Value = rs("PorcentajeComision")
            End While
            rs.Close()
            If Trim(PRESENTACION) <> vbNullString Then
                CargarConversiones(PRESENTACION)
                ComboBox1.SelectedIndex = ComboBox1.FindString((nombrePre(1)))
                txtCostoP.Text = costo

            End If
            If origen = 1 Then 'determina si utiliza un articulo o una receta
                rbreceta.Checked = True
                cConexion.GetRecorset(conectadobd, "Select nombre,costoPorcion from recetas where id=" & ID_RECETA, rs)
                While rs.Read
                    txtReceta.Text = rs("nombre")
                End While
                rs.Close()
            Else
                'CUANDO ES UN ARTICULO
                rbarticulo.Checked = True
                Try
                    Dim conectado As New SqlClient.SqlConnection
                    conectado.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
                    conectado.Open()
                    cConexion.GetRecorset(conectado, "Select Descripcion, Costo from Inventario where Codigo=" & ID_RECETA, rs)
                    While rs.Read
                        txtReceta.Text = rs("Descripcion")
                        CostoCalculado = Math.Round(CDbl(rs("Costo")), 2)
                        Dim cant() As String = (PRESENTACION).Split(" ")

                        Dim multiplo_1 As Double = cConexion.SlqExecuteScalar(conectadobd, "Select Multiplo from TablaConversiones where NombreUnidad='" & cant(1) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
                        Dim estado As String = cConexion.SlqExecuteScalar(conectadobd, "Select Estado from TablaConversiones where NombreUnidad='" & cant(1) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
                        Dim cc As Double = cant(0) * multiplo_1
                        Label22.Text = estado
                        'QUE NO MODIFIQUE EL COSTO POR UNIDAD
                        CostoCalculado = (CDbl(CostoCalculado) / cc) * NumericUpDown1.Value

                    End While
                    rs.Close()
                    conectado.Close()
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If

            Dim i As New System.Windows.Forms.KeyEventArgs(Keys.Enter)
            If Math.Round(CostoAnterior, 2) <> Math.Round(CostoCalculado, 2) Then
                If MsgBox("Los costos han cambiado desea actualizarlos", MsgBoxStyle.YesNo, "GESTION DE MENU") = MsgBoxResult.Yes Then
                    If MsgBox("El costo de " & txtNombre.Text & " ha variado de [" & Math.Round(CostoAnterior, 2) & "] a un costo de [" & Math.Round(CostoCalculado, 2) & "]" & vbCrLf & "Desea actualizar los precios con base al nuevo costo manteniendo la utilidad...?" & vbCrLf & "Si no se proceder� a actualizar la utilidad con base al nuevo costo actual.", MsgBoxStyle.YesNo, "ATENCI�N...") = MsgBoxResult.Yes Then
                        txtCostoP.Text = CostoCalculado
                        txtPUtilidadA_KeyDown(txtPUtilidadA, i)
                        txtPUtilidadA_KeyDown(txtPUtilidadB, i)
                        txtPUtilidadA_KeyDown(txtPUtilidadC, i)
                        txtPUtilidadA_KeyDown(txtPUtilidadD, i)
                        txtPUtilidadA_KeyDown(txtUtilidadExpressA, i)
                        txtPUtilidadA_KeyDown(txtUtilidadExpressM, i)
                    Else
                        txtCostoP.Text = CostoCalculado
                        txtPUtilidadA_KeyDown(txtPVentaA, e)
                        txtPUtilidadA_KeyDown(txtPVentaB, e)
                        txtPUtilidadA_KeyDown(txtPVentaC, e)
                        txtPUtilidadA_KeyDown(txtPVentaD, e)
                        txtPUtilidadA_KeyDown(txtPVentaExpressA, e)
                        txtPUtilidadA_KeyDown(txtPVentaExpressM, e)

                        txtCostoP.Text = CostoCalculado
                        CalculaUtilidades()
                        txtPUtilidadA_KeyDown(txtFinalesA, i)
                        txtPUtilidadA_KeyDown(txtFinalesB, i)
                        txtPUtilidadA_KeyDown(txtFinalesC, i)
                        txtPUtilidadA_KeyDown(txtFinalesD, i)
                        txtPUtilidadA_KeyDown(txtPFinalExpressA, i)
                        txtPUtilidadA_KeyDown(txtPFinalExpressM, i)
                    End If
                Else
                    txtCostoP.Text = CostoCalculado
                    txtPUtilidadA_KeyDown(txtPVentaA, e)
                    txtPUtilidadA_KeyDown(txtPVentaB, e)
                    txtPUtilidadA_KeyDown(txtPVentaC, e)
                    txtPUtilidadA_KeyDown(txtPVentaD, e)
                    txtPUtilidadA_KeyDown(txtPVentaExpressA, e)
                    txtPUtilidadA_KeyDown(txtPVentaExpressM, e)
                End If
            Else
                'bandera
                txtCostoP.Text = CostoCalculado
                txtPUtilidadA_KeyDown(txtPVentaA, e)
                txtPUtilidadA_KeyDown(txtPVentaB, e)
                txtPUtilidadA_KeyDown(txtPVentaC, e)
                txtPUtilidadA_KeyDown(txtPVentaD, e)
                txtPUtilidadA_KeyDown(txtPVentaExpressA, e)
                txtPUtilidadA_KeyDown(txtPVentaExpressM, e)
            End If

        Catch ex As SystemException
            MessageBox.Show("Error al cargar los productos, algunos art�culos no se encuentran", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub txtCodigoBarras_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoBarras.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtNombre.Focus()
        End If
    End Sub

    Private Sub txtCodigoBarras_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigoBarras.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
        If e.KeyChar = Chr(Keys.Back) Then
            e.Handled = False
        End If
    End Sub

    Private Sub txtReceta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReceta.KeyDown

        Dim i As New System.Windows.Forms.KeyEventArgs(Keys.Enter)
        If e.KeyCode = Keys.F1 Then
            Dim Fbuscador As New buscador
            Dim nombrePre() As String

            Fbuscador.TituloModulo.Text = "Restaurante"
            'BuscarUnaReceta
            puntero = True
            If rbreceta.Checked = True Then
                Fbuscador.bd = "Restaurante"
                Fbuscador.tabla = "Recetas"
                Fbuscador.busca = "Nombre"
                Fbuscador.cantidad = 3
                Fbuscador.lblEncabezado.Text = "RECETAS"
                Fbuscador.consulta = "SELECT ID, Nombre AS Descripcion, Porciones FROM Recetas"
                Fbuscador.ShowDialog()
                If (Fbuscador.descripcion <> "") Then
                    txtReceta.Text = Fbuscador.descripcion
                    ID_RECETA = Fbuscador.Icodigo
                    Dim dt As New DataTable

                    cFunciones.Llenar_Tabla_Generico("Select * From Recetas Where ID = " & Fbuscador.Icodigo, dt)
                    Dim porciones As Integer = dt.Rows(0).Item("Porciones")
                    txtCostoP.Text = CalculaCostoReceta(Fbuscador.Icodigo) '/ porciones
                End If
                If Fbuscador.Icodigo <> 0 Then
                    txtPUtilidadA_KeyDown(txtPVentaA, i)
                    txtPUtilidadA_KeyDown(txtPVentaB, i)
                    txtPUtilidadA_KeyDown(txtPVentaC, i)
                    txtPUtilidadA_KeyDown(txtPVentaD, i)
                    txtPUtilidadA_KeyDown(txtPVentaExpressA, i)
                    txtPUtilidadA_KeyDown(txtPVentaExpressM, i)
                Else
                    ID_ARTICULO = ""
                End If
                Me.NumericUpDown1.Enabled = True
            Else
                Dim unidad, prese As String
                Dim d As Integer = 0
                Fbuscador.bd = "Proveeduria"
                Fbuscador.tabla = "inventario"
                Fbuscador.busca = "descripcion"
                Fbuscador.cantidad = 3
                Fbuscador.lblEncabezado.Text = "Inventario"
                Fbuscador.consulta = "Select i.codigo AS CODIGO,i.descripcion + isnull((select case inhabilitado when 1 then ' - (INHABILITADO)' end),'')  as DESCRIPCION,i.costo AS COSTO,(cast(i.PresentaCant as varchar)+ ' ' + p.Presentaciones) as PRESENTACIONES from inventario as i, presentaciones as p where i.CodPresentacion = p.codpres"

                Fbuscador.ShowDialog()
                If (Fbuscador.descripcion <> "") Then
                    txtReceta.Text = Fbuscador.descripcion
                    ID_RECETA = Fbuscador.Icodigo
                    PRESENTACION = Fbuscador.presentacion
                    d = Trim(PRESENTACION).IndexOf(" ")
                    prese = PRESENTACION.Remove(d)
                    Me.NumericUpDown1.Value = prese
                    txtCostoP.Text = CDbl(Fbuscador.detalle) * NumericUpDown1.Value
                    PORCION = Fbuscador.detalle
                    unidad = Fbuscador.presentacion
                    CargarConversiones(unidad)

                    nombrePre = CStr(Fbuscador.presentacion).Split(" ")
                    ComboBox1.SelectedIndex = ComboBox1.FindString((nombrePre(1)))
                    If Fbuscador.Icodigo <> 0 Then
                        txtPUtilidadA_KeyDown(txtPVentaA, i)
                        txtPUtilidadA_KeyDown(txtPVentaB, i)
                        txtPUtilidadA_KeyDown(txtPVentaC, i)
                        txtPUtilidadA_KeyDown(txtPVentaD, i)
                        txtPUtilidadA_KeyDown(txtPVentaExpressA, i)
                        txtPUtilidadA_KeyDown(txtPVentaExpressM, i)
                    Else
                        ID_ARTICULO = ""
                        ID_RECETA = ""

                    End If
                End If
            End If
        ElseIf e.KeyCode = Keys.Enter Then
            chkImpVentas.Focus()
        End If
    End Sub

    Private Sub CargarConversiones(ByVal unidad As String)

        Dim sel As String
        Dim arunidad() As String
        arunidad = unidad.Split(" ")
        Dataset1.Tables.Clear()
        sel = "SELECT Conversiones.dbo.Unidades.NombreUnidad, Conversiones.dbo.Conversiones.ConvertirA," & _
        "Conversiones.dbo.Conversiones.Multiplo,Conversiones.dbo.Conversiones.Estado" & _
        " FROM Conversiones.dbo.Unidades INNER JOIN" & _
        " Conversiones.dbo.Conversiones ON Conversiones.dbo.Unidades.Id = Conversiones.dbo.Conversiones.Id_Unidad" & _
        " where Conversiones.dbo.Unidades.NombreUnidad = '" & arunidad(1) & "'"


        cConexion.GetDataSet(conectadobd, sel, Dataset1, "Presentaciones")
        ComboBox1.DataSource = Dataset1.Tables("Presentaciones")

        Try
            ComboBox1.DisplayMember = "ConvertirA"
        Catch ex As Exception

        End Try


    End Sub

    Private Sub MenuAdmin_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub Impresoras()
        Dim PrinterInstalled As String
        Try
            For Each PrinterInstalled In PrinterSettings.InstalledPrinters
                cboImpresoras.Items.Add(PrinterInstalled)
            Next
            cboImpresoras.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Instale una Impresora", MsgBoxStyle.Information)
        End Try

    End Sub

    Private Function verifica() As Boolean
        If txtReceta.Text = "" Or txtNombre.Text = "" Or cmbDescarga.Text = "" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub txtPUtilidadA_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPUtilidadA.KeyDown,
    txtPUtilidadB.KeyDown, txtPUtilidadC.KeyDown, txtPUtilidadD.KeyDown, txtFinalesA.KeyDown, txtFinalesB.KeyDown, txtFinalesC.KeyDown, txtFinalesD.KeyDown,
    txtPVentaA.KeyDown, txtPVentaB.KeyDown, txtPVentaC.KeyDown, txtPVentaD.KeyDown, txtUtilidadExpressA.KeyDown, txtPVentaExpressA.KeyDown, txtPFinalExpressA.KeyDown, txtUtilidadExpressM.KeyDown, txtPVentaExpressM.KeyDown, txtPFinalExpressM.KeyDown

        If e.KeyCode = Keys.Enter And txtCostoP.Text <> "" Then
            Dim Iv, Isv As Double
            Dim costo As Double
            If Me.rbreceta.Checked Then
                costo = CDbl(txtCostoP.Text) * Me.NumericUpDown1.Value
            Else
                Try
                    costo = CDbl(txtCostoP.Text)
                Catch ex As Exception
                    costo = 0
                    Me.txtCostoP.Text = 0
                End Try


            End If

            If txtPUtilidadA.Text = "" Or txtPUtilidadA.Text = "NeUm" Then
                txtPUtilidadA.Text = "0"

            ElseIf txtPUtilidadB.Text = "" Then
                txtPUtilidadB.Text = "0"

            ElseIf txtPUtilidadC.Text = "" Then
                txtPUtilidadC.Text = "0"

            ElseIf txtPUtilidadD.Text = "" Then
                txtPUtilidadD.Text = "0"

            ElseIf txtUtilidadExpressA.Text = "" Then
                txtUtilidadExpressA.Text = "0"

            ElseIf txtUtilidadExpressM.Text = "" Then
                txtUtilidadExpressM.Text = "0"

            End If

            If Me.chkImpVentas.Checked = True Then
                Iv = impV / 100
            End If

            If Me.chkImServicio.Checked = True Then
                Isv = impS / 100
            End If
            If (NuevoValor = 0) Then NuevoValor = 1
            Select Case sender.Name
                Case "txtUtilidadExpressA"
                    If Me.txtUtilidadExpressA.Text = 0 Then
                        txtPVentaExpressA.Text = 0 : txtPFinalExpressA.Text = 0
                    Else
                        txtPVentaExpressA.Text = Format((costo * (txtUtilidadExpressA.Text / 100) + costo) / NuevoValor, "###,##0.0000")
                        txtPFinalExpressA.Text = Format(Math.Round(((txtPVentaExpressA.Text * Iv) + txtPVentaExpressA.Text), 2), "###,##0.00")
                    End If

                Case "txtUtilidadExpressM"
                    If Me.txtUtilidadExpressM.Text = 0 Then
                        txtPVentaExpressM.Text = 0 : txtPFinalExpressM.Text = 0
                    Else
                        txtPVentaExpressM.Text = Format((costo * (txtUtilidadExpressM.Text / 100) + costo) / NuevoValor, "###,##0.0000")
                        txtPFinalExpressM.Text = Format(Math.Round((((txtPVentaExpressM.Text * Iv)) + txtPVentaExpressM.Text), 2), "###,##0.00")
                    End If
                Case "txtPUtilidadA"
                    If Me.txtPUtilidadA.Text = 0 Then
                        txtPVentaA.Text = 0 : txtFinalesA.Text = 0
                    Else
                        txtPVentaA.Text = Format((costo * (txtPUtilidadA.Text / 100) + costo) / NuevoValor, "###,##0.0000")
                        txtFinalesA.Text = Format(Math.Round((((txtPVentaA.Text * Isv) + (txtPVentaA.Text * Iv)) + txtPVentaA.Text), 2), "###,##0.00")
                    End If

                Case "txtPVentaA"
                    txtPUtilidadA.Text = Utilidad(costo, txtPVentaA.Text)
                    If (CDbl(txtPUtilidadA.Text) < 0) Then txtPUtilidadA.Text = 0
                    txtFinalesA.Text = Format(Math.Round((((txtPVentaA.Text * Isv) + (txtPVentaA.Text * Iv)) + txtPVentaA.Text), 2), "###,##0.00")

                Case "txtPVentaExpressA"
                    txtUtilidadExpressA.Text = Utilidad(costo, txtPVentaExpressA.Text)
                    If (CDbl(txtUtilidadExpressA.Text) < 0) Then txtUtilidadExpressA.Text = 0
                    txtPFinalExpressA.Text = Format(Math.Round((((txtPVentaExpressA.Text * Iv)) + txtPVentaExpressA.Text), 2), "###,##0.00")

                Case "txtPVentaExpressM"
                    txtUtilidadExpressM.Text = Utilidad(costo, txtPVentaExpressM.Text)
                    If (CDbl(txtUtilidadExpressM.Text) < 0) Then txtUtilidadExpressM.Text = 0
                    txtPFinalExpressM.Text = Format(Math.Round((((txtPVentaExpressM.Text * Iv)) + txtPVentaExpressM.Text), 2), "###,##0.00")

                Case "txtFinalesA"
                    txtPVentaA.Text = Format((txtFinalesA.Text / (1 + Iv + Isv)), "###,##0.0000")
                    txtPUtilidadA.Text = Utilidad(costo, txtPVentaA.Text)
                    If (CDbl(txtPUtilidadA.Text) < 0) Then txtPUtilidadA.Text = 0

                Case "txtPFinalExpressA"
                    txtPVentaExpressA.Text = Format((txtPFinalExpressA.Text / (1 + Iv)), "###,##0.0000")
                    txtUtilidadExpressA.Text = Utilidad(costo, txtPVentaExpressA.Text)
                    If (CDbl(txtUtilidadExpressA.Text) < 0) Then txtUtilidadExpressA.Text = 0

                Case "txtPFinalExpressM"
                    txtPVentaExpressM.Text = Format((txtPFinalExpressM.Text / (1 + Iv)), "###,##0.0000")
                    txtUtilidadExpressM.Text = Utilidad(costo, txtPVentaExpressM.Text)
                    If (CDbl(txtUtilidadExpressM.Text) < 0) Then txtUtilidadExpressM.Text = 0

                Case "txtPUtilidadB"
                    If txtPUtilidadB.Text = 0 Then
                        txtPVentaB.Text = 0 : txtFinalesB.Text = 0
                    Else
                        txtPVentaB.Text = Format((costo * (txtPUtilidadB.Text / 100) + costo) / NuevoValor, "###,##0.0000")
                        txtFinalesB.Text = Format(((txtPVentaB.Text * Isv + txtPVentaB.Text * Iv) + txtPVentaB.Text), "###,##0.00")
                    End If

                Case "txtPVentaB"
                    txtPUtilidadB.Text = Utilidad(costo, txtPVentaB.Text)
                    If (CDbl(txtPUtilidadB.Text) < 0) Then txtPUtilidadB.Text = 0
                    txtFinalesB.Text = Format(((txtPVentaB.Text * Isv + txtPVentaB.Text * Iv) + txtPVentaB.Text), "###,##0.00")
                Case "txtFinalesB"
                    txtPVentaB.Text = Format(txtFinalesB.Text / (1 + Iv + Isv), "###,##0.0000")
                    txtPUtilidadB.Text = Utilidad(costo, txtPVentaB.Text)
                    If (CDbl(txtPUtilidadB.Text) < 0) Then txtPUtilidadB.Text = 0
                Case "txtPUtilidadC"
                    If txtPUtilidadC.Text = 0 Then
                        txtPVentaC.Text = 0 : txtFinalesC.Text = 0
                    Else
                        txtPVentaC.Text = Format((costo * (txtPUtilidadC.Text / 100) + costo) / NuevoValor, "###,##0.0000")
                        txtFinalesC.Text = Format((txtPVentaC.Text * Isv + txtPVentaC.Text * Iv) + txtPVentaC.Text, "###,##0.00")
                    End If

                Case "txtPVentaC"
                    txtPUtilidadC.Text = Utilidad(costo, txtPVentaC.Text)
                    If (CDbl(txtPUtilidadC.Text) < 0) Then txtPUtilidadC.Text = 0
                    txtFinalesC.Text = Format(((txtPVentaC.Text * Isv + txtPVentaC.Text * Iv) + txtPVentaC.Text), "###,##0.00")
                Case "txtFinalesC"
                    txtPVentaC.Text = Format(txtFinalesC.Text / (1 + Iv + Isv), "###,##0.0000")
                    txtPUtilidadC.Text = Utilidad(costo, txtPVentaC.Text)
                    If (CDbl(txtPUtilidadC.Text) < 0) Then txtPUtilidadC.Text = 0
                Case "txtPUtilidadD"
                    If txtPVentaD.Text = 0 Then
                        txtPVentaD.Text = 0 : txtFinalesD.Text = 0
                    Else
                        txtPVentaD.Text = Format((costo * (txtPUtilidadD.Text / 100) + costo) / NuevoValor, "###,##0.0000")
                        txtFinalesD.Text = Format(((txtPVentaD.Text * Isv + txtPVentaD.Text * Iv) + txtPVentaD.Text), "###,##0.00")
                    End If

                Case "txtPVentaD"
                    txtPUtilidadD.Text = Utilidad(costo, txtPVentaD.Text)
                    If (CDbl(txtPUtilidadD.Text) < 0) Then txtPUtilidadD.Text = 0
                    txtFinalesD.Text = Format(((txtPVentaD.Text * Isv + txtPVentaD.Text * Iv) + txtPVentaD.Text), "###,##0.00")
                Case "txtFinalesD"
                    txtPVentaD.Text = Format((txtFinalesD.Text / (1 + Iv + Isv)), "###,##0.0000")
                    txtPUtilidadD.Text = Utilidad(costo, txtPVentaD.Text)
                    If (CDbl(txtPUtilidadD.Text) < 0) Then txtPUtilidadD.Text = 0
            End Select
            If txtPUtilidadA.Focused = True Then
                txtPUtilidadD.Focus()
            ElseIf txtPUtilidadD.Focused = True Then
                txtUtilidadExpressA.Focus()
            ElseIf txtUtilidadExpressA.Focused = True Then
                txtUtilidadExpressM.Focus()
            ElseIf Me.txtPVentaA.Focused = True Then
                Me.txtPVentaD.Focus()
            ElseIf Me.txtPVentaD.Focused = True Then
                Me.txtPVentaExpressA.Focus()
            ElseIf Me.txtPVentaExpressA.Focused = True Then
                Me.txtPVentaExpressM.Focus()
            ElseIf Me.txtFinalesA.Focused = True Then
                Me.txtFinalesD.Focus()
            ElseIf Me.txtFinalesD.Focused = True Then
                Me.txtPFinalExpressA.Focus()
            ElseIf Me.txtPFinalExpressA.Focused = True Then
                Me.txtPFinalExpressM.Focus()
            ElseIf Me.txtPFinalExpressM.Focused = True Then
                txtObservaciones.Focus()
            End If
        End If
    End Sub

    Private Function Utilidad(ByVal Costo As Double, ByVal Precio As Double) As Double
        Utilidad = IIf(Costo = 0, 0, Math.Round(((Precio / (Costo / NuevoValor)) - 1) * 100, 2))
        Return Utilidad
    End Function

    Private Sub chkImpVentas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkImpVentas.CheckedChanged
        If (chkImpVentas.Checked = True) Then
            impVB = 1
        Else
            impVB = 0
        End If
        ActualizaDatos()
    End Sub

    Private Sub ActualizaDatos()
        Dim e As New System.Windows.Forms.KeyEventArgs(Keys.Enter)
        txtPUtilidadA_KeyDown(txtPUtilidadA, e)
        txtPUtilidadA_KeyDown(txtPVentaA, e)
        txtPUtilidadA_KeyDown(txtFinalesA, e)

        txtPUtilidadA_KeyDown(txtUtilidadExpressA, e)
        txtPUtilidadA_KeyDown(txtPVentaExpressA, e)
        txtPUtilidadA_KeyDown(txtPFinalExpressA, e)

        txtPUtilidadA_KeyDown(txtUtilidadExpressM, e)
        txtPUtilidadA_KeyDown(txtPVentaExpressM, e)
        txtPUtilidadA_KeyDown(txtPFinalExpressM, e)

        txtPUtilidadA_KeyDown(txtPUtilidadB, e)
        txtPUtilidadA_KeyDown(txtPVentaB, e)
        txtPUtilidadA_KeyDown(txtFinalesB, e)

        txtPUtilidadA_KeyDown(txtPUtilidadC, e)
        txtPUtilidadA_KeyDown(txtPVentaC, e)
        txtPUtilidadA_KeyDown(txtFinalesC, e)

        txtPUtilidadA_KeyDown(txtPUtilidadD, e)
        txtPUtilidadA_KeyDown(txtPVentaD, e)
        txtPUtilidadA_KeyDown(txtFinalesD, e)
    End Sub

    Private Sub chkImServicio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkImServicio.CheckedChanged
        If (chkImServicio.Checked = True) Then
            impSB = 1
        Else
            impSB = 0
        End If
        ActualizaDatos()
    End Sub

    Private Sub BtImagen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtImagen.Click
        Dim fImagen As New Imagen
        fImagen.PictureBox1.Image = BtImagen.Image
        fImagen.ShowDialog()
        BtImagen.Image = fImagen.PictureBox1.Image
        ruta = fImagen.ruta
        fImagen.Dispose()

    End Sub

    Private Sub txtNombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtNombreIdioma.Focus()
        End If
    End Sub

    Private Sub txtAcompanamientos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            txtPUtilidadA.Focus()
        End If
    End Sub

    Private Sub txtCostoP_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCostoP.KeyDown
        If e.KeyCode = Keys.Enter Then
            CalculaUtilidades()
            chkImpVentas.Focus()
        End If
    End Sub

    Private Sub chkImServicio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles chkImServicio.KeyDown
        If e.KeyCode = Keys.Enter Then
            chkModificadores.Focus()
        End If
    End Sub

    Private Sub chkModificadores_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles chkModificadores.KeyDown
        If e.KeyCode = Keys.Enter Then
            chkPlatos.Focus()
        End If
    End Sub

    Private Sub chkPlatos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles chkPlatos.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtAcompanamientos.Focus()
        End If
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 4
                If PMU.Update Then
                    agregar()
                Else
                    MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                End If
            Case 5 : If PMU.Delete Then borrar() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 6 : If PMU.Print Then imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : teclado()
            Case 8 : Close()
        End Select
    End Sub

    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicaci�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub agregar()
        Dim disminuye As Double
        Dim impresora As String
        'bandera
        If Not verifica() Then
            MsgBox("Verifica que Nombre de Articulo(Men�), Nombre Articulo o Receta y Bodega no se encuentren en blanco", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If Not TieneVinculo() Then
            MsgBox("Verifica que " & IIf(rbarticulo.Checked = True, "el Articulo Exista en proveeduria y no este Deshabilitado", "la Receta Exista") & ".", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        impresora = cboImpresoras.Text

        If CBsinimpresora.Checked = True Then
            impresora = ""
        End If

        If Trim(txtCodigoBarras.Text) = "" Then
            txtCodigoBarras.Text = "0"
        End If

        If PRESENTACION <> "" Then
            disminuye = disminuir()
        End If

        If COSTO_UNITARIO = "" Then
            COSTO_UNITARIO = 1
        End If
        If ID_ARTICULO <> Nothing Then
            If origen = 1 Then
                cConexion.UpdateRecords(conectadobd, "Menu_Restaurante", "codigo_barras='" & txtCodigoBarras.Text & "',nombre_menu='" & txtNombre.Text & "', NombreMenu_SegIdioma= '" & txtNombreIdioma.Text & "',id_categoria=" & categoria & ",plato_principal=" & CInt(chkPlatos.Checked) & ",modificador_forzado=" & CInt(chkModificadores.Checked) & ",Numero_Acompa�amientos=" & txtAcompanamientos.Text & ",id_receta=" & ID_RECETA & ",Precio_VentaA=" & CDbl(txtPVentaA.Text) & ",Precio_VentaB=" & CDbl(txtPVentaB.Text) & ",Precio_VentaC=" & CDbl(txtPVentaC.Text) & ",Precio_VentaD=" & CDbl(txtPVentaD.Text) & ",ImpVenta=" & impVB & ",ImpServ=" & impSB & ",Observaciones='" & txtObservaciones.Text & "',Impresora='" & impresora & "',Posicion=" & posicion & ",ImagenRuta='" & ruta & "', Tipo=" & origen & ", Precio_Costo=" & CDbl(txtCostoP.Text) & ", UtilidadA =" & CDbl(txtPUtilidadA.Text) & ", UtilidadB =" & CDbl(txtPUtilidadB.Text) & ", UtilidadC =" & CDbl(txtPUtilidadC.Text) & ", UtilidadD =" & CDbl(txtPUtilidadD.Text) & ",bodega = " & cmbDescarga.SelectedValue & ", moneda =" & ComboBoxMonedaVenta.SelectedValue & ", cantidad =" & NumericUpDown1.Value & ", CostoTotal =" & PORCION & ", unitario =" & CDbl(txtCostoP.Text) & ", disminuye =" & disminuye & ",Sinimpresora=" & CInt(CBsinimpresora.Checked) & ",SinPrecioImp = " & CInt(opConImpuestos.Checked) * -1 & ", SinPrecio = " & CInt(Me.opSinImpuesto.Checked) * -1 & ", PorcentajeComision = " & nuComision.Value & ", Precio_VentaExpressA=" & CDbl(txtPVentaExpressA.Text) & ", Precio_VentaExpressM=" & CDbl(txtPVentaExpressM.Text) & ", UtilidadExpressA=" & CDbl(txtUtilidadExpressA.Text) & ", UtilidadExpressM=" & CDbl(txtUtilidadExpressM.Text), "id_menu=" & ID_ARTICULO)
            Else
                cConexion.UpdateRecords(conectadobd, "Menu_Restaurante", "codigo_barras='" & txtCodigoBarras.Text & "',nombre_menu='" & txtNombre.Text & "', NombreMenu_SegIdioma='" & txtNombreIdioma.Text & "',id_categoria=" & categoria & ",plato_principal=" & CInt(chkPlatos.Checked) & ",modificador_forzado=" & CInt(chkModificadores.Checked) & ",Numero_Acompa�amientos=" & txtAcompanamientos.Text & ",id_receta=" & ID_RECETA & ",Precio_VentaA=" & CDbl(txtPVentaA.Text) & ",Precio_VentaB=" & CDbl(txtPVentaB.Text) & ",Precio_VentaC=" & CDbl(txtPVentaC.Text) & ",Precio_VentaD=" & CDbl(txtPVentaD.Text) & ",ImpVenta=" & impVB & ",ImpServ=" & impSB & ",Observaciones='" & txtObservaciones.Text & "',Impresora='" & impresora & "',Posicion=" & posicion & ",ImagenRuta='" & ruta & "', Tipo=" & origen & ", Precio_Costo=" & CDbl(txtCostoP.Text) & ", UtilidadA =" & CDbl(txtPUtilidadA.Text) & ", UtilidadB =" & CDbl(txtPUtilidadB.Text) & ", UtilidadC =" & CDbl(txtPUtilidadC.Text) & ", UtilidadD =" & CDbl(txtPUtilidadD.Text) & ", moneda =" & ComboBoxMonedaVenta.SelectedValue & ", bodega =" & cmbDescarga.SelectedValue & ", disminuye =" & disminuye & ", cantidad =" & NumericUpDown1.Value & ", Presentacion ='" & PRESENTACION & "-" & ComboBox1.Text & "', Sinimpresora=" & CInt(CBsinimpresora.Checked) & ",CostoTotal =" & PORCION & ", unitario =" & CDbl(txtCostoP.Text) & ",SinPrecioImp =" & CInt(opConImpuestos.Checked) * -1 & ", SinPrecio = " & CInt(Me.opSinImpuesto.Checked) * -1 & ", PorcentajeComision = " & nuComision.Value & ", Precio_VentaExpressA=" & CDbl(txtPVentaExpressA.Text) & ", Precio_VentaExpressM=" & CDbl(txtPVentaExpressM.Text) & ", UtilidadExpressA=" & CDbl(txtUtilidadExpressA.Text) & ", UtilidadExpressM=" & CDbl(txtUtilidadExpressM.Text), "id_menu=" & ID_ARTICULO)
            End If
        Else
            Dim posApp As Integer = 0
            posApp = fnUltimaPosicionApp(categoria)
            posApp = posApp + 1
            If origen = 1 Then
                cConexion.AddNewRecord(conectadobd, "Menu_Restaurante", "deshabilitado, codigo_barras,nombre_menu, NombreMenu_SegIdioma, id_categoria,plato_principal,modificador_forzado,Numero_Acompa�amientos,id_receta,UtilidadA, UtilidadB, UtilidadC, UtilidadD, Precio_VentaA,Precio_VentaB,Precio_VentaC,Precio_VentaD,ImpVenta,ImpServ,Observaciones,Impresora,Posicion,ImagenRuta, Tipo, Precio_Costo,moneda,bodega,Sinimpresora,SinPrecio, SinPrecioImp,PosicionApp,PorcentajeComision, UtilidadExpressA,Precio_VentaExpressA, UtilidadExpressM, Precio_VentaExpressM", "0,'" & txtCodigoBarras.Text & "','" & txtNombre.Text & "','" & txtNombreIdioma.Text & "'," & categoria & "," & CInt(chkPlatos.Checked) & "," & CInt(chkModificadores.Checked) & "," & txtAcompanamientos.Text & "," & ID_RECETA & "," & CDbl(txtPUtilidadA.Text) & "," & CDbl(txtPUtilidadB.Text) & "," & CDbl(txtPUtilidadC.Text) & "," & CDbl(txtPUtilidadD.Text) & "," & CDbl(txtPVentaA.Text) & "," & CDbl(txtPVentaB.Text) & "," & CDbl(txtPVentaC.Text) & "," & CDbl(txtPVentaD.Text) & "," & impVB & "," & impSB & ",'" & txtObservaciones.Text & "','" & impresora & "'," & posicion & ",'" & ruta & "'," & origen & "," & CDbl(txtCostoP.Text) & "," & ComboBoxMonedaVenta.SelectedValue & "," & cmbDescarga.SelectedValue & "," & CInt(CBsinimpresora.Checked) * -1 & "," & CInt(Me.opSinImpuesto.Checked) * -1 & "," & CInt(opConImpuestos.Checked) * -1 & "," & posApp & "," & nuComision.Value & "," & CDbl(txtUtilidadExpressA.Text) & "," & CDbl(txtPVentaExpressA.Text) & "," & CDbl(txtUtilidadExpressM.Text) & "," & CDbl(txtPVentaExpressM.Text))
                '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"codigo_barras,nombre_menu,id_categoria,plato_principal,modificador_forzado,Numero_Acompa�amientos,id_receta,UtilidadA, UtilidadB, UtilidadC, UtilidadD, Precio_VentaA,Precio_VentaB,Precio_VentaC,Precio_VentaD,ImpVenta,ImpServ,Observaciones,Impresora,Posicion,ImagenRuta, Tipo, Precio_Costo,moneda"
            Else
                cConexion.AddNewRecord(conectadobd, "Menu_Restaurante", "deshabilitado, codigo_barras,nombre_menu, NombreMenu_SegIdioma, id_categoria,plato_principal,modificador_forzado,Numero_Acompa�amientos,id_receta,UtilidadA, UtilidadB, UtilidadC, UtilidadD, Precio_VentaA,Precio_VentaB,Precio_VentaC,Precio_VentaD,ImpVenta,ImpServ,Observaciones,Impresora,Posicion,ImagenRuta, Tipo, Precio_Costo,moneda, bodega, disminuye,cantidad,Presentacion,CostoTotal,unitario,Sinimpresora,SinPrecio, SinPrecioImp,PosicionApp,PorcentajeComision, UtilidadExpressA,Precio_VentaExpressA, UtilidadExpressM, Precio_VentaExpressM", "0,'" & txtCodigoBarras.Text & "','" & txtNombre.Text & "','" & txtNombreIdioma.Text & "'," & categoria & "," & CInt(chkPlatos.Checked) & "," & CInt(chkModificadores.Checked) & "," & txtAcompanamientos.Text & "," & ID_RECETA & "," & CDbl(txtPUtilidadA.Text) & "," & CDbl(txtPUtilidadB.Text) & "," & CDbl(txtPUtilidadC.Text) & "," & CDbl(txtPUtilidadD.Text) & "," & CDbl(txtPVentaA.Text) & "," & CDbl(txtPVentaB.Text) & "," & CDbl(txtPVentaC.Text) & "," & CDbl(txtPVentaD.Text) & "," & impVB & "," & impSB & ",'" & txtObservaciones.Text & "','" & impresora & "'," & posicion & ",'" & ruta & "'," & origen & "," & CDbl(txtCostoP.Text) & "," & ComboBoxMonedaVenta.SelectedValue & "," & cmbDescarga.SelectedValue & "," & disminuye & "," & NumericUpDown1.Value & ",'" & PRESENTACION & "-" & ComboBox1.Text & "'," & PORCION & "," & COSTO_UNITARIO & "," & CInt(CBsinimpresora.Checked) * -1 & "," & CInt(Me.opSinImpuesto.Checked) * -1 & "," & CInt(opConImpuestos.Checked) * -1 & "," & posApp & "," & nuComision.Value & "," & CDbl(txtUtilidadExpressA.Text) & "," & CDbl(txtPVentaExpressA.Text) & "," & CDbl(txtUtilidadExpressM.Text) & "," & CDbl(txtPVentaExpressM.Text))
            End If
        End If
        MessageBox.Show("Debe reiniciar el sistema en todas las maquinas para poder cargar los cambios efectuados en el menu.", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Close()
    End Sub

    Function fnUltimaPosicionApp(ByVal _IdCategoria As Integer)
        Dim dt As New DataTable
        Dim posicion As Integer = 0
        Dim cmd As New SqlClient.SqlCommand
        cmd.CommandText = "Select Isnull(MAX(PosicionApp),0) as PosicionApp  from Menu_Restaurante where Id_Categoria = @IdCategoria AND deshabilitado=0"
        cmd.Parameters.AddWithValue("@IdCategoria", _IdCategoria)
        cFunciones.spCargarDatos(cmd, dt, GetSetting("Seesoft", "Restaurante", "Conexion"))
        If dt.Rows.Count > 0 Then
            posicion = dt.Rows(0).Item("PosicionApp")
            Return posicion
        End If
    End Function
    Private Function disminuir() As Double
        'psv
        'Dim cConexion1 As ConexionR


        Dim cant As Array = (PRESENTACION).Split(" ")
        Dim cantidad As String
        ' Dim cc As Double
        cantidad = cant(0)
        Dim multiplo As Double
        Dim reduce As Double

        If CStr(cant(1)) = Me.ComboBox1.Text Then
            If cant(0) = NumericUpDown1.Value Then
                reduce = 1
            Else
                'If cant(0) > NumericUpDown1.Value Then
                '   reduce = cant(0) - NumericUpDown1.Value
                'Else
                'If cant(0) < NumericUpDown1.Value Then
                reduce = NumericUpDown1.Value / cant(0)
                'End If
                ' End If
            End If
        Else
            multiplo = cConexion.SlqExecuteScalar(conectadobd, "Select Multiplo from TablaConversiones where NombreUnidad='" & CStr(cant(1)) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
            If Me.Label22.Text = ">" Then
                'cc = CDbl(cantidad) * multiplo
                'reduce = NumericUpDown1.Value / cc
                reduce = NumericUpDown1.Value * multiplo
            ElseIf Me.Label22.Text = "<" Then
                'cc = NumericUpDown1.Value / multiplo
                'reduce = cc / cantidad
                reduce = NumericUpDown1.Value / multiplo
            End If
        End If

        Return reduce
    End Function

    Private Sub borrar()
        If ID_ARTICULO <> "" Then
            If MessageBox.Show("Realmente art�culo del men�", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim con As New Conexion
                Dim sql As New SqlClient.SqlConnection
                sql = con.Conectar()
                con.SlqExecute(sql, "update Menu_Restaurante set Deshabilitado = 1, posicion = -1 where id_menu = " & ID_ARTICULO)
                con.DesConectar(sql)

                'cConexion.DeleteRecords(conectadobd, "Menu_Restaurante", " Id_Menu=" & ID_ARTICULO) 'datos(1))
            End If
            Close()
        End If

    End Sub

    Private Sub imprimir()
        Try
            If ID_ARTICULO = "" Then
                MessageBox.Show("No se seleccion� ning�n men�", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If
            Dim art As Integer = ID_ARTICULO
            Dim IMenu As New rptMenu
            Dim visor As New frmVisorReportes
            IMenu.SetParameterValue(0, art)
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, IMenu, False, Me.conectadobd.ConnectionString)
            Me.Hide()
            visor.ShowDialog()
            visor.Dispose()
            Me.Show()
        Catch ex As Exception
            MessageBox.Show("Error al cargar el reporte", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub txtAcompanamientos_KeyDown1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAcompanamientos.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtPUtilidadA.Focus()
            If txtAcompanamientos.Text = "" Then
                txtAcompanamientos.Text = "0"
            End If
        End If
    End Sub

    Private Sub txtAcompanamientos_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAcompanamientos.KeyPress
        If Not IsNumeric(e.KeyChar) Or e.KeyChar = Chr(Keys.Back) Then
            e.Handled = True
        End If

        If e.KeyChar = Chr(Keys.Back) Then
            e.Handled = False
        End If
    End Sub

    Private Sub txtPUtilidadA_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPUtilidadA.KeyPress, txtUtilidadExpressA.KeyPress, txtPVentaExpressA.KeyPress, txtPFinalExpressA.KeyPress, txtUtilidadExpressM.KeyPress, txtPVentaExpressM.KeyPress, txtPFinalExpressM.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
        If e.KeyChar = Chr(Keys.Back) Then
            e.Handled = False
        End If
    End Sub

    Private Sub txtObservaciones_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtObservaciones.KeyDown
        If e.KeyCode = Keys.Enter Then
            cboImpresoras.Focus()
        End If
    End Sub

    Private Sub chkImpVentas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles chkImpVentas.KeyDown
        If e.KeyCode = Keys.Enter Then
            chkImServicio.Focus()
        End If
    End Sub

    Private Sub rbarticulo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbarticulo.CheckedChanged
        origen = 2
        ComboBox1.Enabled = True
        NumericUpDown1.Enabled = True
        Label6.Visible = True : cmbDescarga.Visible = True
        'Button1.Focus()
    End Sub

    Private Sub rbreceta_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbreceta.CheckedChanged
        origen = 1
        ComboBox1.Enabled = False
        NumericUpDown1.Enabled = False
        Label6.Visible = False : cmbDescarga.Visible = False
        'Button1.Focus()
    End Sub

    Private Sub txtPUtilidadB_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPUtilidadB.KeyPress
        If Not IsNumeric(e.KeyChar) Or e.KeyChar = Chr(Keys.Back) Then
            e.Handled = True
        End If

        If e.KeyChar = Chr(Keys.Back) Then
            e.Handled = False
        End If

    End Sub

    Private Sub txtPUtilidadC_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPUtilidadC.KeyPress
        If Not IsNumeric(e.KeyChar) Or e.KeyChar = Chr(Keys.Back) Then
            e.Handled = True
        End If

        If e.KeyChar = Chr(Keys.Back) Then
            e.Handled = False
        End If

    End Sub

    Private Sub txtPUtilidadD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPUtilidadD.KeyPress
        If Not IsNumeric(e.KeyChar) Or e.KeyChar = Chr(Keys.Back) Then
            e.Handled = True
        End If

        If e.KeyChar = Chr(Keys.Back) Then
            e.Handled = False
        End If

    End Sub

    Private Sub CargarMonedas()
        Dim Dataset As New DataSet

        cConexion.GetDataSet(conectadobd, "SELECT CodMoneda, MonedaNombre, ValorCompra, Simbolo FROM Moneda", Dataset, "MonedaVenta")
        ComboBoxMonedaVenta.DataSource = Dataset.Tables("MonedaVenta")
        ComboBoxMonedaVenta.DisplayMember = "MonedaNombre"
        ComboBoxMonedaVenta.ValueMember = "CodMoneda"
    End Sub

    Private Sub CargaBodegas()

        Dim Datasets As New DataSet
        Dim Conexiones As New ConexionR

        Conexiones.GetDataSet(conectadobd, "Select idBodega, Nombre  from Proveeduria.dbo.Bodega", Datasets, "Bodega")
        cmbDescarga.DataSource = Datasets.Tables("Bodega")
        cmbDescarga.DisplayMember = "Nombre"
        cmbDescarga.ValueMember = "idBodega"

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Try
            Dim cc As Double
            Dim estado As String
            Dim multiplo As Double

            If inicio > 0 Then
                Dim cant() As String = (PRESENTACION).Split(" ")
                multiplo = cConexion.SlqExecuteScalar(conectadobd, "Select Multiplo from TablaConversiones where NombreUnidad='" & cant(1) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
                estado = cConexion.SlqExecuteScalar(conectadobd, "Select Estado from TablaConversiones where NombreUnidad='" & cant(1) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
                cc = cant(0) * multiplo
                txtCostoP.Text = (CDbl(PORCION) / cc) * NumericUpDown1.Value
                Label22.Text = estado
                'QUE NO MODIFIQUE EL COSTO POR UNIDAD
                COSTO_UNITARIO = (CDbl(PORCION) / cc)
            Else
                inicio = 1
            End If

            'ActualizaDatos()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Costo()
        Dim cc As Double
        Dim estado As String
        Dim multiplo As Double

        If inicio > 0 Then
            Dim cant() As String = (PRESENTACION).Split(" ")
            multiplo = cConexion.SlqExecuteScalar(conectadobd, "Select Multiplo from TablaConversiones where NombreUnidad='" & cant(1) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
            estado = cConexion.SlqExecuteScalar(conectadobd, "Select Estado from TablaConversiones where NombreUnidad='" & cant(1) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
            cc = cant(0) * multiplo
            txtCostoP.Text = (CDbl(PORCION) / cc) * NumericUpDown1.Value
            Label22.Text = estado
            'QUE NO MODIFIQUE EL COSTO POR UNIDAD
            COSTO_UNITARIO = (CDbl(PORCION) / cc)
        Else
            inicio = 1
        End If

        'ActualizaDatos()
    End Sub

    Private Sub NumericUpDown1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles NumericUpDown1.KeyDown
        If ((e.KeyCode = Keys.Enter) And (COSTO_UNITARIO <> "")) Then
            Costo()
            txtCostoP.Text = Format((COSTO_UNITARIO * NumericUpDown1.Value), "###,##0.00")
            'ActualizaDatos()
        End If
    End Sub


    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown1.ValueChanged
        Try
            Dim i As New System.Windows.Forms.KeyEventArgs(Keys.Enter)
            If COSTO_UNITARIO = "" And (Not (Me.rbreceta.Checked)) Then Exit Sub
            If Me.rbarticulo.Checked Then
                txtCostoP.Text = Format((COSTO_UNITARIO * NumericUpDown1.Value), "###,##0.00")
            End If

            txtPUtilidadA_KeyDown(txtPVentaA, i)
            txtPUtilidadA_KeyDown(txtPVentaB, i)
            txtPUtilidadA_KeyDown(txtPVentaC, i)
            txtPUtilidadA_KeyDown(txtPVentaD, i)
        Catch ex As Exception
        End Try
    End Sub
    Public Sub Recalcula()
        Dim dime As Boolean
        If (dime = False) Then
            txtCostoP.Text = Format((COSTO_UNITARIO * NumericUpDown1.Value), "###,##0.00")
            ActualizaDatos()
            dime = True
        End If
    End Sub
    Public Sub Moneda()
        Dim conectado As New SqlClient.SqlConnection
        conectado.ConnectionString = GetSetting("SeeSOFT", "Seguridad", "Conexion")
        conectado.Open()
        NuevoValor = cConexion.SlqExecuteScalar(conectado, " Select ValorCompra FROM Moneda WHERE CodMoneda=" & Me.ComboBoxMonedaVenta.SelectedValue & "")
        conectado.Close()
    End Sub
    Private Sub ComboBoxMonedaVenta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxMonedaVenta.SelectedIndexChanged
        If (puntero = True) Then
            Moneda()

            txtPVentaA.Text = Format((txtCostoP.Text * (txtPUtilidadA.Text / 100) + txtCostoP.Text) / NuevoValor, "###,##0.00")
            txtFinalesA.Text = Math.Round(((txtPVentaA.Text * (impS / 100) + txtPVentaA.Text * (impV / 100)) + txtPVentaA.Text), 2)

            txtPVentaB.Text = Format((txtCostoP.Text * (txtPUtilidadB.Text / 100) + txtCostoP.Text) / NuevoValor, "###,##0.00")
            txtFinalesB.Text = Format(((txtPVentaB.Text * (impS / 100) + txtPVentaB.Text * (impV / 100)) + txtPVentaB.Text), "###,##0.00")

            txtPVentaC.Text = Format((txtCostoP.Text * (txtPUtilidadC.Text / 100) + txtCostoP.Text) / NuevoValor, "###,##0.00")
            txtFinalesC.Text = Format(((txtPVentaC.Text * (impS / 100) + txtPVentaC.Text * (impV / 100)) + txtPVentaC.Text), "###,##0.00")

            txtPVentaD.Text = Format((txtCostoP.Text * (txtPUtilidadD.Text / 100) + txtCostoP.Text) / NuevoValor, "###,##0.00")
            txtFinalesD.Text = Format(((txtPVentaD.Text * (impS / 100) + txtPVentaD.Text * (impV / 100)) + txtPVentaD.Text), "###,##0.00")
        End If
    End Sub

    Private Sub txtFinalesA_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFinalesA.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtFinalesA.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtPVentaA_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPVentaA.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtPVentaA.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtPVentaB_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPVentaB.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtPVentaB.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtPVentaC_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPVentaC.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtPVentaC.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtPVentaD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPVentaD.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtPVentaD.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtFinalesB_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFinalesB.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtFinalesB.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtFinalesC_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFinalesC.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtFinalesC.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtFinalesD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFinalesD.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtFinalesD.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub



    Private Sub cmbDescarga_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbDescarga.KeyDown
        Try
            ID_BODEGA = cmbDescarga.SelectedValue
            BuscaCostoPromedio()
            txtCostoP.Focus()
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Sub

    'ESTA FUNCION BUSCA EL PRECIO COSTO DE ARTICULOSXBODEGA, BUSCA EL COSTO PROMEDIO BASADO EN EL ARTICULO QUE SE SELECCIONARA
    'Y EN LA BODEGA QUE SE SELECCIONA DEL CMBDESCARGA
    Private Sub BuscaCostoPromedio()
        Try
            If txtReceta.Text = "" Then Exit Sub
            Dim exe As New ConexionR
            Dim costopromedio As Double
            Dim codigo, contador As Integer
            Dim dsDetalle_Receta As New DataSet
            Dim cnx As New SqlClient.SqlConnection
            Dim fila_receta As DataRow
            codigo = CInt(ID_RECETA)

            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()


            If rbreceta.Checked = True Then

                If cnx.State = ConnectionState.Closed Then cnx.Open()
                Dim fila As DataRow
                exe.GetDataSet(cnx, "SELECT idDetalle, Descripcion, idReceta, Codigo, Cantidad, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo, dsDetalle_Receta, "Recetas_Detalles")

                For Each fila_receta In dsDetalle_Receta.Tables("Recetas_Detalles").Rows
                    If fila_receta("Articulo") = 0 Then
                        'AQUI MANDA A BUSCAR SI ES UNA RECETA ANIDADA
                        costopromedio += CalculaCostoReceta(fila_receta("Codigo"))
                    Else
                        'SI ES UN ARTICULO DE MENU ENTONCES LO SUMA NADA MAS
                        costopromedio += (CalculaCostoArticulo(fila_receta("Codigo"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), fila_receta("IdReceta")) * fila_receta("Cantidad"))
                    End If
                    If Receta_Completa = False Then
                        If MsgBox("La receta no esta completa desea continuar", MsgBoxStyle.OkCancel, "Servicios Estructurales SeeSoft") <> MsgBoxResult.Yes Then
                            txtCostoP.Text = 0
                            PORCION = 0
                            Exit Sub
                        End If
                    End If
                Next
            End If

            'EN CASO DE QUE SEA SOLO UN ARTICULO ASI YA ESTA LISTO
            If rbarticulo.Checked = True Then
                costopromedio = exe.SlqExecuteScalar(cnx, "Select Costo_Promedio From Proveeduria.dbo.ArticulosXBodega Where idBodega = " & ID_BODEGA & " AND Proveeduria.dbo.ArticulosXBodega.Codigo = " & codigo)
                If costopromedio <> 0 Then
                    txtCostoP.Text = costopromedio
                    PORCION = costopromedio
                    Costo()
                Else
                    MsgBox("Este articulo no esta registrado en esta bodega, proceda a registrarlo o escoja otra bodega")
                End If

            End If
            PORCION = costopromedio
            Costo()
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Sub

    'ESTA FUNCION CALCULA DE FORMA RECURSIVA EL COSTO DE UNA RECETA EN CASO DE QUE TUVIERA ANIDADAS OTRAS RECETAS
    Private Function CalculaCostoReceta(ByVal codigo_receta As Integer) As Double

        Try
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("Select * From Recetas WHere ID = " & codigo_receta, dt)
            Dim porc As Double = dt.Rows(0).Item("Porciones")

            Dim cnx As New SqlClient.SqlConnection

            Dim exe As New ConexionR

            Dim calculando_costo As Double

            Dim dsDetalle_RecetaAnidada As New DataSet

            Dim fila_receta As DataRow

            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")

            If cnx.State = ConnectionState.Closed Then cnx.Open()

            exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Cantidad, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")

            For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows

                If fila_receta("Articulo") = 0 Then
                    calculando_costo += CalculaCostoReceta(fila_receta("Codigo")) * fila_receta("Cantidad")

                Else
                    calculando_costo += (CalculaCostoArticulo(fila_receta("Codigo"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), fila_receta("idreceta")) * fila_receta("Cantidad"))

                End If
            Next

            Dim especies As Double = CDbl(calculando_costo) * (CDbl(dt.Rows(0).Item("Especies")) / 100)

            Dim desaprovechamiento As Double = CDbl(calculando_costo) * (CDbl(dt.Rows(0).Item("aprovechamiento")) / 100)

            calculando_costo = (calculando_costo + especies + desaprovechamiento) '/ dt.Rows(0).Item("Porciones")

            Return calculando_costo / porc

        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")

        End Try

    End Function


    'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA 
    Private Function CalculaCostoArticulo(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal Receta As Integer) As Double
        Try
            Dim costoXarticulo As Double
            Dim exe As New ConexionR
            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()

            costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoUnit FROM VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & " and IdReceta = " & Receta)
            If costoXarticulo = 0 Then
                MsgBox("El Articulo " + nombre + " no se ha registrado en la bodega que se indico para descarga, se utilizar� el precio base para el calculo de la receta", MsgBoxStyle.OkOnly, "Servicios Estructurales SeeSoft")
                costoXarticulo = 0
            End If
            Return costoXarticulo
            cnx.Close()
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function


    'ESTA FUNCION CALCULA HACIA ATRAS LAS UTILIDADES BASANDOSE EN EL COSTO PROMEDIO QUE TIENE EL ARTICULOXBODEGA
    Private Sub CalculaUtilidades()
        Try
            Dim Iv, Isv As Double

            If Me.chkImpVentas.Checked = True Then
                Iv = impV / 100
            End If

            If Me.chkImServicio.Checked = True Then
                Isv = impS / 100
            End If

            txtPVentaA.Text = Format((txtFinalesA.Text / (1 + Iv + Isv)), "###,##0.00")
            txtPUtilidadA.Text = Utilidad(txtCostoP.Text, txtPVentaA.Text)
            If (CDbl(txtPUtilidadA.Text) < 0) Then txtPUtilidadA.Text = 0

            txtPVentaB.Text = Format(txtFinalesB.Text / (1 + Iv + Isv), "###,##0.00")
            txtPUtilidadB.Text = Utilidad(txtCostoP.Text, txtPVentaB.Text)
            If (CDbl(txtPUtilidadB.Text) < 0) Then txtPUtilidadB.Text = 0

            txtPVentaC.Text = Format(txtFinalesC.Text / (1 + Iv + Isv), "###,##0.00")
            txtPUtilidadC.Text = Utilidad(txtCostoP.Text, txtPVentaC.Text)
            If (CDbl(txtPUtilidadC.Text) < 0) Then txtPUtilidadC.Text = 0

            txtPVentaD.Text = Format((txtFinalesD.Text / (1 + Iv + Isv)), "###,##0.00")
            txtPUtilidadD.Text = Utilidad(txtCostoP.Text, txtPVentaD.Text)
            If (CDbl(txtPUtilidadD.Text) < 0) Then txtPUtilidadD.Text = 0

        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try

    End Sub

    'ESTA SUB HABILITA O INHABILITA LOS CONTROLES DEL FORMULARIO
    Private Sub EstadoControles(ByVal estado As Boolean)
        Try
            txtCodigoBarras.Enabled = estado : txtNombre.Enabled = estado : txtNombreIdioma.Enabled = estado : txtReceta.Enabled = estado
            ComboBox1.Enabled = estado : NumericUpDown1.Enabled = estado : txtCostoP.Enabled = estado
            chkImpVentas.Enabled = estado : chkImServicio.Enabled = estado : chkModificadores.Enabled = estado
            txtAcompanamientos.Enabled = estado : cmbDescarga.Enabled = estado : ComboBoxMonedaVenta.Enabled = estado
            txtPUtilidadA.Enabled = estado : txtPUtilidadB.Enabled = estado : txtPUtilidadC.Enabled = estado
            txtUtilidadExpressA.Enabled = estado : txtPVentaExpressA.Enabled = estado : txtPFinalExpressA.Enabled = estado : txtUtilidadExpressM.Enabled = estado : txtPVentaExpressM.Enabled = estado : txtPFinalExpressM.Enabled = estado
            txtPUtilidadD.Enabled = estado : txtPVentaA.Enabled = estado : txtPVentaB.Enabled = estado
            txtPVentaC.Enabled = estado : txtPVentaD.Enabled = estado : txtFinalesA.Enabled = estado
            txtFinalesB.Enabled = estado : txtFinalesC.Enabled = estado : txtFinalesD.Enabled = estado
            txtObservaciones.Enabled = estado : BtImagen.Enabled = estado
            cboImpresoras.Enabled = estado
            nuComision.Enabled = estado
            btCentroProduccion.Enabled = estado
            If CBsinimpresora.Checked = True Then
                cboImpresoras.Enabled = False
            End If

        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Sub

#Region "Validacion Usuario "
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox1.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")
            If usuario <> "" Then
                TextBox1.Text = ""
                ToolBarRegistrar.Enabled = True
                ToolBarButton1.Enabled = True
                ToolBarEliminar.Enabled = True
                ToolBarImprimir.Enabled = True
                lbUsuario.Text = usuario
                EstadoControles(True)
                txtCodigoBarras.Focus()
            Else
                Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                TextBox1.Text = ""
                ToolBarRegistrar.Enabled = True
                ToolBarButton1.Enabled = True
                ToolBarEliminar.Enabled = True
                ToolBarImprimir.Enabled = True
                EstadoControles(True)
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
                txtCodigoBarras.Focus()
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub txtNombreIdioma_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombreIdioma.KeyDown
        If e.KeyCode = Keys.Enter Then
            rbarticulo.Focus()
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBsinimpresora.CheckedChanged
        If CBsinimpresora.Checked = True Then
            cboImpresoras.Enabled = False

        Else
            cboImpresoras.Enabled = True
        End If
    End Sub

    Private Sub opConImpuestos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opConImpuestos.CheckedChanged
        If opConImpuestos.Checked = True Then
            txtPVentaA.Text = 0 : txtPVentaB.Text = 0 : txtPVentaD.Text = 0 : txtPVentaC.Text = 0
            txtFinalesA.Text = 0 : txtFinalesB.Text = 0 : txtFinalesC.Text = 0 : txtFinalesD.Text = 0
            txtPUtilidadA.Text = 0 : txtPUtilidadB.Text = 0 : txtPUtilidadC.Text = 0 : txtPUtilidadD.Text = 0
            txtPUtilidadA.Enabled = False : txtPUtilidadB.Enabled = False : txtPUtilidadC.Enabled = False : txtPUtilidadD.Enabled = False
            txtPVentaA.Enabled = False : txtPVentaB.Enabled = False : txtPVentaC.Enabled = False : txtPVentaD.Enabled = False
            txtFinalesA.Enabled = False : txtFinalesB.Enabled = False : txtFinalesC.Enabled = False : txtFinalesD.Enabled = False
        Else
            txtPUtilidadA.Enabled = True : txtPUtilidadB.Enabled = True : txtPUtilidadC.Enabled = True : txtPUtilidadD.Enabled = True
            txtPVentaA.Enabled = True : txtPVentaB.Enabled = True : txtPVentaC.Enabled = True : txtPVentaD.Enabled = True
            txtFinalesA.Enabled = True : txtFinalesB.Enabled = True : txtFinalesC.Enabled = True : txtFinalesD.Enabled = True
        End If
    End Sub

    Private Sub opSinImpuesto_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opSinImpuesto.CheckedChanged
        If opSinImpuesto.Checked = True Then
            txtPVentaA.Text = 0 : txtPVentaB.Text = 0 : txtPVentaD.Text = 0 : txtPVentaC.Text = 0
            txtFinalesA.Text = 0 : txtFinalesB.Text = 0 : txtFinalesC.Text = 0 : txtFinalesD.Text = 0
            txtPUtilidadA.Text = 0 : txtPUtilidadB.Text = 0 : txtPUtilidadC.Text = 0 : txtPUtilidadD.Text = 0
            txtPUtilidadA.Enabled = False : txtPUtilidadB.Enabled = False : txtPUtilidadC.Enabled = False : txtPUtilidadD.Enabled = False
            txtPVentaA.Enabled = False : txtPVentaB.Enabled = False : txtPVentaC.Enabled = False : txtPVentaD.Enabled = False
            txtFinalesA.Enabled = False : txtFinalesB.Enabled = False : txtFinalesC.Enabled = False : txtFinalesD.Enabled = False
        Else
            txtPUtilidadA.Enabled = True : txtPUtilidadB.Enabled = True : txtPUtilidadC.Enabled = True : txtPUtilidadD.Enabled = True
            txtPVentaA.Enabled = True : txtPVentaB.Enabled = True : txtPVentaC.Enabled = True : txtPVentaD.Enabled = True
            txtFinalesA.Enabled = True : txtFinalesB.Enabled = True : txtFinalesC.Enabled = True : txtFinalesD.Enabled = True
        End If
    End Sub

    Private Sub btCentroProduccion_Click(sender As Object, e As EventArgs) Handles btCentroProduccion.Click
        Try
            Dim frm As New frmListaCentros
            frm.IdMenu = ID_ARTICULO
            frm.ShowDialog()
        Catch ex As Exception

        End Try
    End Sub





    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        txtReceta_KeyDown(Me.txtReceta, New System.Windows.Forms.KeyEventArgs(Keys.F1))
        Me.txtReceta.Focus()
    End Sub

    Private Sub rbreceta_KeyDown_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles rbreceta.KeyDown, rbarticulo.KeyDown
        If e.KeyCode = Keys.Enter Then Me.txtReceta.Focus()
    End Sub

    Private Function TieneVinculo() As Boolean
        If rbarticulo.Checked = True Then
            'buscamos el articulo
            If General.Ejecuta("select * from proveeduria.dbo.inventario where inhabilitado = 0 and codigo = " & ID_RECETA).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            'buscamos la receta
            If General.Ejecuta("select * from recetas where id = " & ID_RECETA).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End If
    End Function

End Class