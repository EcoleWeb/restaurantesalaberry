
Imports System.IO

Public Class MenuI

#Region "variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim cambio As Boolean = False
    Dim posicion As String = "Origen"
    Dim id As String = ""
    Dim cedula As String
    Dim PMU As New PerfilModulo_Class
    Dim Inicializando As Boolean = False
#End Region

    Private Sub Menu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub Menu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        CargarGrupos()
        Inicializando = True

        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
        cConexion.DesConectar(conectadobd)
        If gloNoClave Then
            Loggin_Usuario()
        Else
            TextBox1.Focus()
        End If
        conectadobd = cConexion.Conectar("Restaurante")
        '---------------------------------------------------------------

    End Sub

    Private Sub CargarGrupos()
        Try
            cboGrupos.Items.Clear()
            ListItems.Items.Clear()
            cConexion.GetRecorset(conectadobd, "Select id,nombre from categorias_menu order by nombre", rs)
            While rs.Read
                cboGrupos.Items.Add(rs("nombre"))
                ListItems.Items.Add(rs("id"))
            End While
            rs.Close()
            cboGrupos.SelectedIndex = 0
            ListItems.SelectedIndex = 0
        Catch ex As Exception
            MessageBox.Show("No existe ning�n men� creado", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try

    End Sub

    Private Sub CargaMenu()
        Try
            Dim aButton As Button
            Dim i, ii, X, Y, n As Integer
            PanelMesas.Controls.Clear()

            ObtenerDimension()

            For i = 1 To NumericFilas.Value
                For ii = 1 To Me.NumericColumnas.Value
                    'bandera
                    aButton = New Button
                    aButton.Top = Y
                    aButton.Left = X
                    aButton.Width = 120
                    aButton.Height = 80
                    aButton.Name = n
                    'aButton.ShowToolTips = False
                    AddHandler aButton.Click, AddressOf Clickbuttons
                    Me.PanelMesas.Controls.Add(aButton)
                    X = X + 122
                    n = n + 1
                Next
                Y = Y + 82
                X = 0
            Next
            UbicarPosiciones()

        Catch ex As Exception

        End Try
    End Sub

    Dim titulo As New ToolTip

    Private Sub UbicarPosiciones()
        Try
            Dim ruta As String

            Dim botones As Button
            cConexion.GetRecorset(conectadobd, "SELECT Id_Menu, Nombre_Menu, imagenRuta, posicion FROM Menu_Restaurante where deshabilitado = 0 and  Id_Categoria =" & ListItems.Text, rs)
            While rs.Read
                botones = New Button
                botones = PanelMesas.Controls.Item(CInt(rs("posicion")))
                botones.Text = rs("Nombre_Menu") ' Nombre de la mesa
                botones.Name = botones.Name & "-" & rs("Id_Menu") 'Codigo de la mesa
                              
                ruta = rs("imagenRuta")

                If File.Exists(ruta) Then
                    Try
                        If General.TieneVinculo(rs("Id_Menu")) = False Then
                            botones.BackColor = Color.Purple
                            titulo.SetToolTip(botones, "Verificar Receta...")
                        Else
                            Dim SourceImage As Bitmap
                            SourceImage = New Bitmap(ruta)
                            botones.Image = SourceImage
                        End If

                    Catch ex As Exception
                        'MsgBox("La ruta de la imagen del menu " + botones.Text + " no se encuentra, Favor Revisar", MsgBoxStyle.Critical, "Sistemas Estructurales")
                        'rs.Close()
                        GoTo seguir
                    End Try
                Else
                    If General.TieneVinculo(rs("Id_Menu")) = False Then
                        botones.BackColor = Color.Purple
                        titulo.SetToolTip(botones, "Verificar Receta...")
                    End If
                End If
seguir:
                botones.TextImageRelation = TextImageRelation.ImageAboveText

            End While
            rs.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
            rs.Close()
        End Try
    End Sub

    Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim datos As Array
            datos = CStr(sender.name).Split("-")
            If datos.Length > 1 Then
                id = datos(1)
            End If
            PMU = VSM(cedula, Me.Name)
            If cambio = False Then 'si no va cambiar el menu
                Try
                    If PMU.Find Then
                        Dim g_MenuAdmin As New MenuAdmin
                        datos = CStr(sender.name).Split("-")
                        If datos.Length > 1 Then
                            g_MenuAdmin.ID_ARTICULO = datos(1)
                        End If
                        g_MenuAdmin.posicion = datos(0)
                        g_MenuAdmin.categoria = ListItems.Text
                        g_MenuAdmin.BtImagen.Image = sender.image
                        g_MenuAdmin.ShowDialog()
                        g_MenuAdmin.Dispose()
                        datos = Nothing
                        CargaMenu()
                    Else : MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                    End If
                Catch ex As Exception

                End Try

            Else
                If PMU.Others Then
                    If id <> "" Then
                        If posicion = "Origen" Then 'si elige la categoria de origen
                            posicion = "Final"
                            CargaMenu()
                            txtEstado.Text = "Elija la categor�a donde ubicar� el men�"
                        Else ' si elige la categoria final
                            If datos.Length <> 2 Then
                                cConexion.UpdateRecords(conectadobd, "Menu_Restaurante", "Id_Categoria=" & ListItems.Text & ", Posicion=" & datos(0), "Id_Menu=" & id)
                                CargaMenu()
                                posicion = "Origen"
                                cambio = False
                                txtEstado.Text = "Men�"
                            Else
                                MessageBox.Show("Ya existe un men� en esta posici�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                                cambio = False
                                txtEstado.Text = "Men�"
                            End If
                        End If
                    Else
                        MessageBox.Show("No existe ning�n men� en esa posici�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        cambio = False
                        txtEstado.Text = "Men�"
                    End If
                Else : MsgBox("No tiene permiso para mover datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                End If
            End If
        Catch ex As Exception

        End Try
      
    End Sub

    Private Sub cboGrupos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrupos.SelectedIndexChanged
        ListItems.SelectedIndex = cboGrupos.SelectedIndex
        CargaMenu()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim r_GrupoMenu As New GruposMenu
        r_GrupoMenu.ShowDialog()
        r_GrupoMenu.Dispose()
        r_GrupoMenu = Nothing
        CargarGrupos()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Close()
    End Sub

    Private Sub btnMover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMover.Click
        If MessageBox.Show("Desea cambiar el men� de Categor�a", "Cambio de Categor�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
            txtEstado.Text = "Elija el men� a cambiar"
            cambio = True
            posicion = "Origen"
        Else
            cambio = False
            txtEstado.Text = "Men�"
        End If
    End Sub

    Private Sub ObtenerDimension() ' SAJ 0809807. REDIMESIONAMIENTO DE LSA CATEGORIAS
        Dim Conexion As New Conexion
        'Dim F, C As Integer
        Try
            Me.NumericColumnas.Value = Conexion.SlqExecuteScalar(conectadobd, "SELECT Columnas FROM Categorias_Menu WHERE (Id = " & ListItems.Items(cboGrupos.SelectedIndex) & ")")
            Me.NumericFilas.Value = Conexion.SlqExecuteScalar(conectadobd, "SELECT Filas FROM Categorias_Menu WHERE (Id = " & ListItems.Items(cboGrupos.SelectedIndex) & ")")
            LabelDimension.Text = "F:" & NumericFilas.Value & " x C:" & Me.NumericColumnas.Value & " = " & Me.NumericColumnas.Value * Me.NumericFilas.Value
        Catch ex As Exception
            MsgBox("Error al cargar la dimensi�n de las categorias.." & ex.ToString, MsgBoxStyle.Information, "Alerta...")
        End Try

    End Sub

    Private Sub VerificarLongitud() ' SAJ 080807 Verifica la dimesion de los datos registrados antes de realizar las operaciones de actualizaci�n para que ningun elemento registrado quede por fuera de la matriz
        Dim Conexion As New Conexion
        Dim F, C, MaxPosicion As Integer
        Try
            MaxPosicion = Conexion.SlqExecuteScalar(conectadobd, "SELECT MAX(Posicion) FROM Menu_Restaurante")
            F = Me.NumericFilas.Value
            C = Me.NumericColumnas.Value

            If (F * C) < (MaxPosicion + 1) Then
                MsgBox("No se puede redimensionar el tama�o de las categoria.. quedar�a un elemento fuera de este..", MsgBoxStyle.Information, "Atenci�n..")
                ObtenerDimension()
                CargaMenu()
            Else
                Conexion.UpdateRecords("Categoria_Dimesion", "Filas =" & F & ",Columnas = " & C, "ID =" & ListItems.Items(cboGrupos.SelectedIndex), "Restaurante") ' actualiza la nueva dimesion en la bd.
                CargaMenu()
            End If


        Catch ex As Exception
            MsgBox("Error al cargar la dimesion de las categorias..", MsgBoxStyle.Information, "Alerta...")
        End Try

    End Sub

    Private Sub NumericFilas_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericFilas.ValueChanged
        If Inicializando Then VerificarLongitud()
    End Sub

    Private Sub NumericColumnas_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericColumnas.ValueChanged
        If Inicializando Then VerificarLongitud()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Me.GroupBox2.Enabled = CheckBox1.Checked
        Me.NumericFilas.Enabled = CheckBox1.Checked
        Me.NumericColumnas.Enabled = CheckBox1.Checked

    End Sub

#Region "Validacion Usuario"
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox1.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                TextBox1.Text = ""
                Me.PanelMesas.Enabled = True
                Me.Panel1.Enabled = True
                Me.lbUsuario.Text = usuario
            Else
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                TextBox1.Text = ""
                Me.PanelMesas.Enabled = True
                Me.Panel1.Enabled = True
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre

            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region

End Class
