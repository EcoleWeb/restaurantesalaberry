<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MesaF
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MesaF))
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.nAsiento = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCombo = New System.Windows.Forms.ComboBox()
        Me.lstItems = New System.Windows.Forms.ListBox()
        Me.BtImagen = New System.Windows.Forms.Button()
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar2 = New System.Windows.Forms.ToolBar()
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton()
        Me.TituloModulo = New System.Windows.Forms.Label()
        Me.ChMesaExpress = New System.Windows.Forms.CheckBox()
        CType(Me.nAsiento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtNombre
        '
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtNombre, "txtNombre")
        Me.txtNombre.Name = "txtNombre"
        '
        'nAsiento
        '
        resources.ApplyResources(Me.nAsiento, "nAsiento")
        Me.nAsiento.Name = "nAsiento"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCombo)
        Me.Panel1.Controls.Add(Me.lstItems)
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'cmdCombo
        '
        Me.cmdCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.cmdCombo, "cmdCombo")
        Me.cmdCombo.FormattingEnabled = True
        Me.cmdCombo.Name = "cmdCombo"
        '
        'lstItems
        '
        Me.lstItems.FormattingEnabled = True
        resources.ApplyResources(Me.lstItems, "lstItems")
        Me.lstItems.Name = "lstItems"
        '
        'BtImagen
        '
        Me.BtImagen.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.BtImagen, "BtImagen")
        Me.BtImagen.Name = "BtImagen"
        Me.BtImagen.UseVisualStyleBackColor = False
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "teclado1.gif")
        Me.ImageList.Images.SetKeyName(9, "")
        '
        'ToolBar2
        '
        resources.ApplyResources(Me.ToolBar2, "ToolBar2")
        Me.ToolBar2.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarEditar, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarCerrar})
        Me.ToolBar2.ImageList = Me.ImageList
        Me.ToolBar2.Name = "ToolBar2"
        '
        'ToolBarNuevo
        '
        resources.ApplyResources(Me.ToolBarNuevo, "ToolBarNuevo")
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        '
        'ToolBarEditar
        '
        resources.ApplyResources(Me.ToolBarEditar, "ToolBarEditar")
        Me.ToolBarEditar.Name = "ToolBarEditar"
        '
        'ToolBarBuscar
        '
        resources.ApplyResources(Me.ToolBarBuscar, "ToolBarBuscar")
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        '
        'ToolBarImprimir
        '
        resources.ApplyResources(Me.ToolBarImprimir, "ToolBarImprimir")
        Me.ToolBarImprimir.Name = "ToolBarImprimir"
        '
        'ToolBarCerrar
        '
        resources.ApplyResources(Me.ToolBarCerrar, "ToolBarCerrar")
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        '
        'TituloModulo
        '
        Me.TituloModulo.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.TituloModulo, "TituloModulo")
        Me.TituloModulo.ForeColor = System.Drawing.Color.White
        Me.TituloModulo.Name = "TituloModulo"
        '
        'ChMesaExpress
        '
        resources.ApplyResources(Me.ChMesaExpress, "ChMesaExpress")
        Me.ChMesaExpress.Name = "ChMesaExpress"
        Me.ChMesaExpress.UseVisualStyleBackColor = True
        '
        'MesaF
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ChMesaExpress)
        Me.Controls.Add(Me.TituloModulo)
        Me.Controls.Add(Me.ToolBar2)
        Me.Controls.Add(Me.BtImagen)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.nAsiento)
        Me.Controls.Add(Me.txtNombre)
        Me.Name = "MesaF"
        CType(Me.nAsiento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents nAsiento As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCombo As System.Windows.Forms.ComboBox
    Friend WithEvents lstItems As System.Windows.Forms.ListBox
    Friend WithEvents BtImagen As System.Windows.Forms.Button
    Public WithEvents ImageList As System.Windows.Forms.ImageList
    Public WithEvents ToolBar2 As System.Windows.Forms.ToolBar
    Protected Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarTeclado As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents TituloModulo As System.Windows.Forms.Label
    Public WithEvents ChMesaExpress As CheckBox
End Class
