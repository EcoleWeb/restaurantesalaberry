Imports System.Data.SqlClient

Public Class Receta

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim cConexion1 As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim idReceta As String
    Dim actualiza As Boolean = False
    Dim cedula As String
    Dim unidad As String
    Dim reduce As Double
    Dim PMU As New PerfilModulo_Class
    Dim puntos As Integer
    Dim IDBODEGA_DESCARGA As Integer = 7005
    Dim NombreBodega As String
    Dim especies, desaprovechamiento As Double
    Dim desperdicio, especies1 As Double
    Dim iv As Double
    Dim Total As Single
    Dim subprecio As Double
    Dim PresentaCant As Double
#End Region

#Region "Controles"
    Private Sub Controles(ByVal Estado As Boolean)
        txtnombre.Enabled = Estado
        nmPorciones.Enabled = Estado
        txtInsumo.Enabled = Estado
        ComboBox1.Enabled = Estado
        txtCantidad.Enabled = Estado
        cbxBodega.Enabled = Estado
        txtEspecies.Enabled = Estado
        TextBox3.Enabled = Estado
        ckReceta.Enabled = Estado
        dtgInsumos.Enabled = Estado
    End Sub
#End Region

    Private Sub txtPorciones_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) And Asc(e.KeyChar) <> Keys.Back Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtInsumo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtInsumo.KeyDown

        If dtgInsumos.Enabled = False Then
            MsgBox("En este momento se encuentra realizando una modificaci�n a > " & Me.txtInsumo.Text & vbCrLf & "SE LE RECOMIENDA : Cancelar la operaci�n � terminar con la misma..", MsgBoxStyle.Information, "Atenci�n...")
            dtgInsumos.Enabled = True
            Exit Sub
        End If

        If PMU.Find Then

            If e.KeyCode = Keys.Enter Then
                txtPUnitario.Focus()
            End If

            If e.KeyCode = Keys.F1 Then
                Dim Fbuscador As New buscador
                If Me.ckReceta.Checked = True Then
                    Fbuscador.check = 1
                End If
                Fbuscador.cantidad = 3
                Fbuscador.tabla = "Recetas"

                If ckReceta.Checked = True Then
                    Fbuscador.TituloModulo.Text = "Buscando Recetas en el Inventario"
                    Fbuscador.lblEncabezado.Text = " > Recetas <"
                    Fbuscador.bd = "Restaurante"
                    Fbuscador.consulta = "select ID,Nombre,Porciones,Especies,Aprovechamiento,'UNIDAD' AS Presentacion from Recetas"
                Else
                    Fbuscador.TituloModulo.Text = "Buscando un articulo en el Inventario"
                    Fbuscador.lblEncabezado.Text = " > Inventario <"
                    Fbuscador.bd = "Proveeduria"
                    Fbuscador.consulta = "Select i.codigo AS Codigo, i.descripcion as Descripcion, i.PrecioBase as Costo, (cast(i.PresentaCant as varchar)+ ' ' + p.Presentaciones) as Presentaci�n,i.IVenta as Impuesto,i.existencia from inventario as i inner join Presentaciones as p on i.CodPresentacion = p.codpres WHERE (i.Inhabilitado = 0)"
                End If

                Fbuscador.busca = "Descripci�n"
                Fbuscador.ShowDialog()
                txtInsumo.Text = Fbuscador.descripcion
                unidad = Fbuscador.presentacion
                iv = Fbuscador.iv

                If unidad <> "" Then
                    Dim ii As Array = unidad.Split(" ")
                    txtIdInsumo.Text = Fbuscador.Icodigo
                    txtpresentacion.Text = Fbuscador.presentacion
                    Dim detalle As Decimal
                    If ckReceta.Checked Then
                        detalle = Me.CalculoTotalReceta(Fbuscador.Icodigo) 'CalculaCostoReceta(Fbuscador.Icodigo, 1)
                    Else
                        detalle = Fbuscador.detalle
                    End If

                    txtPUnitario.Text = Format(detalle, "###,##0.00")
                    buscador.Dispose()
                    CargarConversiones(ii(1))
                    If TextBox2.Text = "Infinito" Then
                        TextBox2.Text = "0.00"
                    End If
                    'Calcula el precio del art�culo seg�n su presentaci�n
                    CalculoReceta()
                    '-----------------------------------------------------
                    cbxBodega.Focus()
                Else
                    txtIdInsumo.Focus()
                End If

            End If
        Else : MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
        End If
    End Sub

    Private Sub CargarConversiones(ByVal unidad As String)
        Dim cnn As SqlConnection = Nothing
        Dim mensaje As String
        Dim cConexion As New ConexionR
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Me.DataSetReceta1.TablaConversiones.Clear()
            Dim sConn As String = GetSetting("Seesoft", "Restaurante", "Conexion")
            cnn = New SqlConnection(sConn)
            cConexion.Conectar("Restaurante")
            cnn.Open()
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String
            If Me.ckReceta.Checked Then
                sel = "SELECT Conversiones.dbo.Unidades.NombreUnidad, 'PORCIONES' AS ConvertirA," & _
                                            " 1 AS Multiplo, '=' AS Estado" & _
                                            " FROM Conversiones.dbo.Unidades" & _
                                            " where Conversiones.dbo.Unidades.NombreUnidad = 'UNIDAD'"
            Else

                sel = "SELECT Conversiones.dbo.Unidades.NombreUnidad, Conversiones.dbo.Conversiones.ConvertirA," & _
                            "Conversiones.dbo.Conversiones.Multiplo,Conversiones.dbo.Conversiones.Estado" & _
                            " FROM Conversiones.dbo.Unidades INNER JOIN" & _
                            " Conversiones.dbo.Conversiones ON Conversiones.dbo.Unidades.Id = Conversiones.dbo.Conversiones.Id_Unidad" & _
                            " where Conversiones.dbo.Unidades.NombreUnidad = '" & unidad & "'"
            End If
            
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetReceta1.TablaConversiones)
            If Me.DataSetReceta1.TablaConversiones.Rows.Count = 0 Then
                MsgBox("No hay conversiones registradas para esa unidad de medida...", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            'MsgBox(ex.ToString)
            mensaje = ex.Message
        Finally
            If Not cnn Is Nothing Then
                cConexion.DesConectar(cnn)
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub agregaLinea()
        Dim cant As Array = (Me.txtpresentacion.Text).Split(" ")
        Dim cantidad As String
        Dim cc As Double
        cantidad = cant(0)
        Dim multiplo As Double

        If CStr(cant(1)) = Me.ComboBox1.Text Then
            If cant(0) = CDbl(Me.txtCantidad.Text) Then
                reduce = 1 ' / nmPorciones.Value
            Else
                reduce = CDbl(txtCantidad.Text) / cant(0) ' / nmPorciones.Value
            End If
        Else
            multiplo = cConexion.SlqExecuteScalar(conectadobd, "Select Multiplo from TablaConversiones where NombreUnidad='" & CStr(cant(1)) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
            If Me.Label15.Text = ">" Then
                cc = CDbl(cantidad) * multiplo
                reduce = CDbl(Me.txtCantidad.Text) / cc '/ nmPorciones.Value
            ElseIf Me.Label15.Text = "<" Then
                cc = CDbl(Me.txtCantidad.Text) / multiplo
                reduce = cc / cantidad '/ nmPorciones.Value
            End If
        End If
        Dim piv, pivi As Double : Dim articulo As Integer
        If iv <> 0 Then
            piv = 0 'CDbl(Me.TextBox2.Text) * iv / 100
            pivi = CDbl(Me.TextBox2.Text) + piv '
        Else
            pivi = CDbl(Me.TextBox2.Text)
        End If

        If ckReceta.CheckState = CheckState.Checked Then
            articulo = 0
            IDBODEGA_DESCARGA = 7005
        Else
            articulo = 1
            IDBODEGA_DESCARGA = cbxBodega.SelectedValue
            NombreBodega = cbxBodega.Text
        End If

        Dim row0 As String() = {txtIdInsumo.Text, txtCantidad.Text, Me.TextBox1.Text, txtInsumo.Text, (Format(Math.Round(CDbl(Me.TextBox2.Text), 3), "#####0.00")), (Format((Math.Round(CDbl(txtCantidad.Text), 3) * Math.Round(CDbl(pivi), 3)), "#####0.00")), Math.Round(CDbl(pivi), 3), reduce, IDBODEGA_DESCARGA, articulo, NombreBodega}

        With Me.dtgInsumos.Rows
            .Add(row0)
            Totales()
        End With

        txtIdInsumo.Clear()
        txtpresentacion.Clear()
        txtInsumo.Clear()
        txtInsumo.Select()
        TextBox2.Text = "0.00"
        txtPUnitario.Text = "0.00"
        txtCantidad.Text = "0"
        CalcularEspeciesDesaprovechamiento()
        cbxBodega.SelectedIndex = 0
        cbxBodega.Enabled = True
    End Sub

    Private Sub Receta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub Receta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cFunciones.Llenar_Tabla_Generico("Select * From Bodega", Me.ProveeduriaDataSet2.Bodega, GetSetting("SeeSoft", "Proveeduria", "Conexion"))
        conectadobd = cConexion.Conectar("Restaurante")
        limpia()
        Controles(False)
        dtgInsumos.EditMode = DataGridViewEditMode.EditProgrammatically
        Dim style As New DataGridViewCellStyle
        With style
            .BackColor = Color.Beige
            .ForeColor = Color.Brown
            .Font = New Font("Verdana", 10)
        End With
        ToolBar2.Buttons(1).Enabled = False
        dtgInsumos.DefaultCellStyle = style
        CagaComboBodegas()
        Dim cnx As New SqlClient.SqlConnection
        Dim consultaReceta As String = "SELECT ID, Nombre, Porciones, CostoTotal, CostoPorcion AS CostoPorciones, Especies, aprovechamiento AS Desaprovechamiento " & _
                                        " FROM Recetas"

        cFunciones.Llenar_Tabla_Generico(consultaReceta, ds.Receta)
        Dim consultaDetalles As String = "Select * FROM Recetas_Detalles"
        cFunciones.Llenar_Tabla_Generico(consultaDetalles, ds.Recetas_Detalles)

        spCargarCostosReceta()
        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
       
        If gloNoClave Then
            Loggin_Usuario()
        Else
            txtClave.Focus()
        End If
       
        '---------------------------------------------------------------
    End Sub

    Private Sub CagaComboBodegas()
        Try
        Catch ex As Exception
            MsgBox("Ocurrio el siguiente error mientras se cargaba la informacion" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        End Try
    End Sub

    Private Sub limpia()
        conectadobd = cConexion.Conectar("Restaurante")
        If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
        txtnombre.Focus()
        subprecio = 0
        txtIdReceta.Text = ""
        txtnombre.Text = ""
        nmPorciones.Text = "0"
        txtInsumo.Text = ""
        txtIdInsumo.Text = ""
        txtPUnitario.Text = "0.0"
        txtCantidad.Text = "0"
        Me.txtSubTotal.Text = "0.0"
        txtCPorcion.Text = ""
        txtCTotal.Text = ""
        dtgInsumos.Rows.Clear()
    End Sub

    Private Sub Totales()
        Total = 0
        For Each row As DataGridViewRow In Me.dtgInsumos.Rows
            Total += Val(row.Cells(5).Value)
        Next
        Me.txtSubTotal.Text = Total
    End Sub

    Private Sub TotalesEli()
        Total = 0
        For Each row As DataGridViewRow In Me.dtgInsumos.Rows
            Total += Val(row.Cells(5).Value)
        Next
        Total -= Me.dtgInsumos.Item(5, Me.dtgInsumos.CurrentRow.Index).Value
        If (Total < 0) Then Total = 0
        Me.txtSubTotal.Text = Total
        Me.txtCTotal.Text = Math.Round((CDbl(txtCTotal.Text) - Me.dtgInsumos.Item(5, Me.dtgInsumos.CurrentRow.Index).Value), 2)
        Me.txtCPorcion.Text = Math.Round((CDbl(txtCTotal.Text) / CInt(Me.nmPorciones.Value)), 2)
    End Sub

    Private Sub dtgInsumos_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgInsumos.CellDoubleClick
        MessageBox.Show("No se pueden editar productos, elimine la l�nea y vuelva a ingresar el producto" & Chr(13) & "Seleccionando el registro y oprimiendo la tecla Supr o Detele para eliminarlo")
        dtgInsumos.Enabled = True
        'dtgInsumos.Enabled = False
        'If Trim(txtIdReceta.Text) = vbNullString Then
        '    MessageBox.Show("No se pueden editar productos si no est�n registrados en la base de datos, elimine la l�nea y vuelva a ingresar el producto" & Chr(13) & "Seleccionando el registro y oprimiendo la tecla Supr o Detele para eliminarlo")
        '    Me.dtgInsumos.Enabled = True
        '    Exit Sub
        'End If
        'txtIdInsumo.Text = Me.dtgInsumos.Item(0, Me.dtgInsumos.CurrentRow.Index).Value
        'txtCantidatxtCPorciond.Text = Me.dtgInsumos.Item(1, Me.dtgInsumos.CurrentRow.Index).Value
        'TextBox1.Text = Me.dtgInsumos.Item(2, Me.dtgInsumos.CurrentRow.Index).Value
        'txtInsumo.Text = Me.dtgInsumos.Item(3, Me.dtgInsumos.CurrentRow.Index).Value
        'TxtSubTotalTemp.Text = Me.dtgInsumos.Item(5, Me.dtgInsumos.CurrentRow.Index).Value
        'TextBox2.Text = Me.dtgInsumos.Item(6, Me.dtgInsumos.CurrentRow.Index).Value
        'cbxBodega.SelectedValue = Me.dtgInsumos.Item(8, Me.dtgInsumos.CurrentRow.Index).Value
        'InfoEditarLinea(txtIdReceta.Text, Me.dtgInsumos.Item(0, Me.dtgInsumos.CurrentRow.Index).Value) ' cargar la info del articulo
        'txtPUnitario.Text = Me.dtgInsumos.Item(4, Me.dtgInsumos.CurrentRow.Index).Value * PresentaCant
        'Dim ii As Array = txtpresentacion.Text.Split(" ")
        'On Error Resume Next
        'CargarConversiones(ii(1))
        'If (ComboBox1.Items.Count - 1) >= 0 Then Me.ComboBox1.SelectedIndex = 0
        'Me.ComboBox1.Text = Me.dtgInsumos.Item(2, Me.dtgInsumos.CurrentRow.Index).Value
        'Me.TextBox1.Text = Me.dtgInsumos.Item(2, Me.dtgInsumos.CurrentRow.Index).Value
    End Sub

    Private Sub InfoEditarLinea(ByVal Receta As Long, ByVal Codigo As Long)
        Dim Conexion As New ConexionR
        Dim Data As SqlDataReader = Nothing
        Conexion.Conectar()
        '    If Not Data.IsClosed Then Data.Close()
        Conexion.GetRecorset(Conexion.SqlConexion, "SELECT Recetas_Detalles.IdDetalle, Recetas_Detalles.IdReceta, Recetas_Detalles.Codigo, Proveeduria.dbo.Inventario.PresentaCant,  Proveeduria.dbo.Presentaciones.Presentaciones, Recetas_Detalles.Articulo FROM Recetas_Detalles INNER JOIN  Proveeduria.dbo.Inventario ON Recetas_Detalles.Codigo = Proveeduria.dbo.Inventario.Codigo INNER JOIN  Proveeduria.dbo.Presentaciones ON Proveeduria.dbo.Inventario.CodPresentacion = Proveeduria.dbo.Presentaciones.CodPres WHERE (Recetas_Detalles.Codigo = " & Codigo & ") AND (Recetas_Detalles.IdReceta = " & Receta & ")", Data)

        While Data.Read
            Me.txtpresentacion.Text = Data("PresentaCant") & " " & Data("Presentaciones")
            PresentaCant = Data("PresentaCant")
        End While

        Conexion.DesConectar(Conexion.SqlConexion)
        Data = Nothing

    End Sub

    Private Sub dtgInsumos_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dtgInsumos.UserDeletingRow
        TotalesEli()
    End Sub

    Private Sub txtnombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtnombre.KeyDown
        PMU = VSM(cedula, Me.Name)
        If e.KeyCode = Keys.F1 Then
            If PMU.Find Then Me.modificar_recetas() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
        End If
    End Sub

    Private Sub modificar_recetas()
        Dim fbuscador As New buscador
        Dim costo_enbodega As Double
        Dim costo_total As Double
        Dim cnx As New SqlClient.SqlConnection
        Dim exe As New ConexionR
        dtgInsumos.Rows.Clear() : fbuscador.check = 1
        fbuscador.ToolBarImprimir.Enabled = False : fbuscador.TituloModulo.Text = "Buscando Receta"
        fbuscador.bd = "Restaurante" : fbuscador.tabla = "Recetas"
        fbuscador.busca = "Nombre" : fbuscador.cantidad = 3 : fbuscador.lblEncabezado.Text = "RECETAS"
        fbuscador.consulta = "SELECT ID, Nombre, Porciones,Especies,Aprovechamiento FROM Recetas"
        fbuscador.ShowDialog()

        If fbuscador.Icodigo = 0 Then
            Exit Sub
        End If

        txtnombre.Text = fbuscador.descripcion
        txtIdReceta.Text = fbuscador.Icodigo
        nmPorciones.Value = fbuscador.detalle
        ToolBarEliminar.Enabled = True
        ToolBarImprimir.Enabled = True
        Controles(False)
        cConexion.GetRecorset(conectadobd, "Select Aprovechamiento, Especies From Recetas Where id =" & txtIdReceta.Text, rs)
        If rs.Read() Then
            Me.txtEspecies.Text = rs("Especies")
            Me.TextBox3.Text = rs("Aprovechamiento")
        End If
        rs.Close()
        'AQUI SE OBTIENEN LOS DATOS DE LA RECETA O DEL ARTICULO DE MENU QUE SE COMPONE LA RECETA
        cConexion.GetRecorset(conectadobd, "Select idreceta,codigo,descripcion,cantidad,costoUnit,costoTotal,UConversion,PUConversion,Articulo,Disminuye,idBodegaDescarga from Recetas_Detalles Where idreceta=" & txtIdReceta.Text, rs)
        While rs.Read
            If rs("Articulo") = True Then
                costo_enbodega = CalculaCostoArticulo(rs("codigo"), rs("idBodegaDescarga"), rs("descripcion"), rs("idreceta"))
                costo_total = costo_enbodega * rs("Cantidad")
            Else
                Dim Cantidad As Double = rs("Cantidad")
                costo_enbodega = Me.CalculoTotalReceta(rs("codigo")) '* nmPorciones.Value
                costo_total = costo_enbodega * Cantidad
            End If
            If costo_enbodega = 0 Then
                costo_enbodega = rs("costoUnit") * nmPorciones.Value
                costo_total = rs("costoTotal")
            End If

            '------------------------------------------------------------------------
            'AQUI SE OBTIENEN EL NOMBRE DE LA BODEGA
            cnx.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()
            NombreBodega = cConexion.SlqExecuteScalar(cnx, "Select Bodega.Nombre from Bodega where Bodega.IdBodega= " & rs("idBodegaDescarga"))
            If cnx.State <> ConnectionState.Closed Then cnx.Close()
            '------------------------------------------------------------------------

            Dim row0 As String() = {rs("codigo"), rs("cantidad"), rs("UConversion"), rs("descripcion"), Math.Round(costo_enbodega, 2), Math.Round(costo_total, 2), rs("PUConversion"), rs("Disminuye"), rs("idBodegaDescarga"), rs("Articulo"), NombreBodega}
            With dtgInsumos.Rows
                .Add(row0)
                Totales()
            End With
        End While
        rs.Close()
        CalcularEspeciesDesaprovechamiento()
        If txtIdReceta.Text <> "" Then
            actualiza = True
            Controles(True)
        End If
    End Sub
    Dim ds As New DataSetRecetaImp
    Function CalculoTotalReceta(ByVal Id_Receta As Integer) As Double
        Dim costo_enbodega As Double = 0
        Dim costo_total As Double = 0
        Dim dtTable As DataSetRecetaImp
        Dim Totales As Double = 0
        Dim especiesl As Double = 0
        Dim aprovechamientol As Double = 0
        Dim porciones As Double = 0
        'Dim cnx As New SqlClient.SqlConnection
        'cFunciones.Llenar_Tabla_Generico("Select Aprovechamiento, Especies, Porciones From Recetas Where id =" & Id_Receta, dtTable)

        Dim RowFiltro = (From i As DataSetRecetaImp.RecetaRow In ds.Receta Where i.Id = Id_Receta)
        For Each r As DataSetRecetaImp.RecetaRow In RowFiltro
            especiesl = r.Especies
            aprovechamientol = r.Desaprovechamiento
            porciones = r.Porciones
        Next

      

        'AQUI SE OBTIENEN LOS DATOS DE LA RECETA O DEL ARTICULO DE MENU QUE SE COMPONE LA RECETA
        Dim RowFiltroDetalle = (From i As DataSetRecetaImp.Recetas_DetallesRow In ds.Recetas_Detalles Where i.IdReceta = Id_Receta)
        For Each r As DataSetRecetaImp.Recetas_DetallesRow In RowFiltroDetalle

            If r.Articulo = True Then
                costo_enbodega = CalculaCostoArticulo(r.Codigo, r.idBodegaDescarga, r.Descripcion, r.IdReceta)
                costo_total = costo_enbodega * r.Cantidad
            Else
                costo_enbodega = Me.CalculoTotalReceta(r.Codigo) 'Me.CalculaCostoReceta(dtTable.Rows(i).Item("codigo"), dtTable.Rows(i).Item("Cantidad"))
                costo_total = costo_enbodega * r.Cantidad
            End If
            If costo_enbodega = 0 Then
                costo_enbodega = r.CostoUnit
                costo_total = r.CostoTotal
            End If
            Totales += costo_total
        Next

        Dim especiesD As Double = CDbl(Totales) * (CDbl(especiesl) / 100)
        Dim desaprovechamientoD As Double = Totales * (CDbl(aprovechamientol) / 100)
        Totales = (Totales + especiesD + desaprovechamientoD) / porciones

        Return Totales

    End Function

    Private Function CalculaCostoReceta(ByVal codigo_receta As Integer, ByVal cantidadReceta As Double) As Double
        Try
            Dim cnx As New SqlClient.SqlConnection
            Dim exe As New ConexionR
            Dim calculando_costo As Double
            Dim dsDetalle_RecetaAnidada As New DataSet
            Dim fila_receta As DataRow
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()

            exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Cantidad, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")
            For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows
                If fila_receta("Articulo") = 0 Then
                    calculando_costo += Me.CalculoTotalReceta(fila_receta("Codigo")) 'CalculaCostoReceta(fila_receta("Codigo"), fila_receta("Cantidad"))
                Else
                    calculando_costo += CalculaCostoArticulo(fila_receta("Codigo"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), fila_receta("idreceta")) * fila_receta("Cantidad") '* cantidadReceta
                End If
            Next
            Return calculando_costo
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function

    Sub spCargarCostosReceta()
        cFunciones.Llenar_Tabla_Generico("Select * From VCostoDetalleReceta", ds.VCostoDetalleReceta)

    End Sub
    'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA ESPECIFICAMENTE, BODEGA CODIGO
    Private Function CalculaCostoArticulo(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal Receta As Integer) As Double
        Try
            Dim costoXarticulo As Double
            Dim RowFiltroDetalle = (From i As DataSetRecetaImp.VCostoDetalleRecetaRow In ds.VCostoDetalleReceta Where i.idBodegaDescarga = idbodega_descarga And i.Codigo = codigo_articulo And i.IdReceta = Receta)

            For Each r As DataSetRecetaImp.VCostoDetalleRecetaRow In RowFiltroDetalle
                costoXarticulo = r.CostoUnit
            Next

            '     costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoUnit From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & "and IdReceta = " & Receta)
            If costoXarticulo = 0 Then
                ' MsgBox("El Articulo " + nombre + " no se ha registrado en la bodega que se indico para descarga, se utilizar� el precio base para el calculo de la receta", MsgBoxStyle.OkOnly, "Servicios Estructurales SeeSoft")
                costoXarticulo = 0

            End If
            Return costoXarticulo
            ' cnx.Close()
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function

    Private Sub txtnombre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtnombre.KeyPress, nmPorciones.KeyPress
        If Asc(e.KeyChar) = Keys.Enter Then
            SendKeys.Send("{tab}")
        End If
    End Sub

    Private Sub txtClave_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = Keys.Enter Then
            SendKeys.Send("{tab}")
        End If
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick

        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 1
                If (ToolBarNuevo.Text = "Nuevo") Then
                    ToolBarNuevo.ImageIndex = 7
                    ToolBarNuevo.Text = "Cancelar"
                    limpia()
                    Controles(True)
                    actualiza = False
                    ToolBarEliminar.Enabled = False
                    ToolBarBuscar.Enabled = False
                    ToolBarImprimir.Enabled = False
                    dtgInsumos.Enabled = True
                    txtnombre.Focus()
                Else
                    ToolBarNuevo.ImageIndex = 0
                    ToolBarNuevo.Text = "Nuevo"
                    limpia()
                    Controles(False)
                    actualiza = False
                    ToolBarEliminar.Enabled = False
                    ToolBarBuscar.Enabled = True
                    ToolBarImprimir.Enabled = False
                    dtgInsumos.Enabled = True
                End If

            Case 3 : If PMU.Find Then modificar_recetas() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Update Then agregar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Delete Then borrar() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 6 : If PMU.Print Then Me.imprimirPorDataSet() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : teclado()
            Case 8 : Close()
        End Select

    End Sub

    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicaci�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try

    End Sub

    Private Sub agregar()
        Try
            ToolBarEliminar.Enabled = False
            especies1 = CDbl(Me.txtEspecies.Text)
            desperdicio = CDbl(Me.TextBox3.Text)

            If txtCedula.Text = "" Then
                MessageBox.Show("Debe validar el usuario que ingreso la receta", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If

            If dtgInsumos.RowCount <= 1 Then
                MessageBox.Show("Debe ingresar almenos un art�culo a la receta", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If

            If txtnombre.Text = "" Or nmPorciones.Value = 0 Then
                MessageBox.Show("Verifique el nombre y el n�mero de porciones de la Receta", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If
            Dim idInsumo, nInsumo, UConv As String
            Dim bodega_descarga As Integer
            Dim articulo As Boolean
            Dim pPorcion, pCantidad, pCosto, pU, resta As Double
            If actualiza = False Then
                idReceta = cConexion.SlqExecuteScalar(conectadobd, "Insert into Recetas(Nombre,CedUsuario,Nombre_usuario,Porciones,CostoTotal,CostoPorcion,Especies,Aprovechamiento) values('" & txtnombre.Text & "','" & txtCedula.Text & "','" & txtUsuario.Text & "'," & nmPorciones.Value & "," & txtCTotal.Text & "," & txtCPorcion.Text & "," & Me.txtEspecies.Text & "," & Me.TextBox3.Text & ") select max(id)as receta from Recetas")
            Else
                cConexion.UpdateRecords(conectadobd, "Recetas", "Nombre='" & txtnombre.Text & "', CedUsuario='" & txtCedula.Text & "', Nombre_usuario='" & txtUsuario.Text & "', Porciones=" & nmPorciones.Value & ", CostoTotal=" & txtCTotal.Text & ", CostoPorcion=" & txtCPorcion.Text & ",especies = " & Me.txtEspecies.Text & ", aprovechamiento=" & Me.TextBox3.Text, "id=" & txtIdReceta.Text)
                cConexion.DeleteRecords(conectadobd, "Recetas_Detalles", "idreceta=" & txtIdReceta.Text)
                idReceta = txtIdReceta.Text
                actualiza = False
            End If

            For Each row As DataGridViewRow In dtgInsumos.Rows

                idInsumo = row.Cells(0).Value 'id Insumo
                pCantidad = row.Cells(1).Value 'Cantidad del insumo en la porcion
                UConv = row.Cells(2).Value  'unidad de conversion
                nInsumo = row.Cells(3).Value 'Descripcion del insumo
                pPorcion = row.Cells(4).Value 'Precio Unitario
                pCosto = row.Cells(5).Value 'Costo del Insumo en la porcion
                pU = row.Cells(6).Value
                resta = row.Cells(7).Value
                bodega_descarga = row.Cells(8).Value 'Bodega de descarga
                articulo = row.Cells(9).Value

                If nInsumo = "" Then
                    Exit For
                End If
                If articulo = False Then
                    cConexion.AddNewRecord(conectadobd, "Recetas_Detalles", "idreceta,codigo,descripcion,cantidad,costoUnit,costoTotal,UConversion,PUConversion,Articulo,Disminuye,idBodegaDescarga", idReceta & "," & idInsumo & ",'" & nInsumo & "'," & pCantidad & "," & pPorcion & "," & pCosto & ",'" & UConv & "'," & pU & ",0,0,7005")
                Else
                    cConexion.AddNewRecord(conectadobd, "Recetas_Detalles", "idreceta,codigo,descripcion,cantidad,costoUnit,costoTotal,UConversion,PUConversion,Articulo,Disminuye,idBodegaDescarga", idReceta & "," & idInsumo & ",'" & nInsumo & "'," & pCantidad & "," & pPorcion & "," & pCosto & ",'" & UConv & "'," & pU & ",1," & Math.Round(resta, 4) & "," & bodega_descarga)
                End If
            Next

            If MessageBox.Show("La receta ha sido almacenada correctamente", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.OK Then
                ' Close()
                limpia()
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarNuevo.Text = "Nuevo"
                Me.ToolBarNuevo.ImageIndex = 0
                Me.ToolBarBuscar.Enabled = True
                actualiza = False
                Controles(False)
            End If
        Catch ex As Exception
            MessageBox.Show("Error al guardar la receta: " & ex.Message, "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try

    End Sub

    Private Sub borrar()
        If dtgInsumos.RowCount = 1 Then
            MsgBox("Cargue una receta antes de eliminar", MsgBoxStyle.Information, "Sistemas Estructurales")
            Exit Sub
        End If
        If MessageBox.Show("�Esta seguro(a) que desea eliminar esta receta?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
            Dim valida As Boolean = False ' valida si se puede borrar la receta
            If actualiza = True Then
                idReceta = txtIdReceta.Text
            End If
            cConexion.GetRecorset(conectadobd, "Select * from Menu_Restaurante where Id_Receta=" & idReceta, rs)
            While rs.Read
                valida = True
            End While
            rs.Close()
            If valida = True Then
                MessageBox.Show("No se puede eliminar la receta, esta siendo utilizada por un men�", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Me.limpia()
                Exit Sub
            End If
            cConexion.DeleteRecords(conectadobd, "Recetas", "Id=" & idReceta)
            cConexion.DeleteRecords(conectadobd, "Recetas_Detalles", "Id_Receta=" & idReceta)
            MessageBox.Show("La receta '" & txtnombre.Text & "' ha sido eliminada correctamente", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Me.limpia()
            Controles(False)
        End If
    End Sub

    Sub imprimirPorDataSet()
        Dim c As New clsReporteRecetas
        c.imprimirPorDataSet(txtIdReceta.Text)
    End Sub

    Private Sub imprimir()
        Me.imprimirPorDataSet()
        'If idReceta = "" Then
        '    If actualiza = True Then
        '        idReceta = txtIdReceta.Text
        '    Else
        '        agregar()
        '    End If
        'End If

        'Try
        '    If idReceta = "" Then
        '        MessageBox.Show("Seleccione una receta para imprimir", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        Exit Sub
        '    End If
        '    Dim rptreceta As New rptRecetas
        '    Dim visor As New frmVisorReportes
        '    rptreceta.SetParameterValue(0, idReceta)
        '    CrystalReportsConexion.LoadReportViewer(visor.rptViewer, rptreceta, False, Me.conectadobd.ConnectionString)
        '    Me.Hide()
        '    visor.ShowDialog()
        '    visor.Dispose()
        '    Me.Show()
        'Catch ex As Exception
        '    MessageBox.Show("Error al cargar el reporte", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        'End Try
        ''
    End Sub

    Private Sub ModificarLinea()
        ' ESTA VARA LA COPIE PERO NI PUTA IDEA PARA QUE ES 
        ' PARA MI NO TIENE SENTIDO.
        Dim cant As Array = (Me.txtpresentacion.Text).Split(" ")
        Dim cantidad As String
        Dim cc As Double
        cantidad = cant(0)
        Dim multiplo As Double
        If CStr(cant(1)) = Me.ComboBox1.Text Then
            If cant(0) = CDbl(Me.txtCantidad.Text) Then
                reduce = 1
            Else
                reduce = (CDbl(Me.txtCantidad.Text) / cant(0))
            End If
        Else
            multiplo = cConexion.SlqExecuteScalar(conectadobd, "Select Multiplo from TablaConversiones where NombreUnidad='" & Me.ComboBox1.Text & "' and ConvertirA= '" & CStr(cant(1)) & "'")
            If Label15.Text = ">" Then
                cc = CDbl(cantidad) * multiplo
                reduce = CDbl(Me.txtCantidad.Text) / cc
            ElseIf Label15.Text = "<" Then
                cc = CDbl(Me.txtCantidad.Text) / multiplo
                reduce = cc / cantidad
            End If
        End If
        Dim piv, pivi As Double
        If iv <> 0 Then
            piv = CDbl(Me.TextBox2.Text) * iv / 100
            pivi = CDbl(Me.TextBox2.Text) + piv
        Else
            pivi = CDbl(Me.TextBox2.Text)
        End If
        dtgInsumos.Item(0, dtgInsumos.CurrentRow.Index).Value = txtIdInsumo.Text 'Me.txtIdReceta.Text
        dtgInsumos.Item(1, dtgInsumos.CurrentRow.Index).Value = txtCantidad.Text
        dtgInsumos.Item(2, dtgInsumos.CurrentRow.Index).Value = TextBox1.Text
        dtgInsumos.Item(3, dtgInsumos.CurrentRow.Index).Value = txtInsumo.Text
        dtgInsumos.Item(4, dtgInsumos.CurrentRow.Index).Value = TextBox2.Text
        dtgInsumos.Item(5, dtgInsumos.CurrentRow.Index).Value = CDbl(txtCantidad.Text) * CDbl(TextBox2.Text)
        dtgInsumos.Item(6, dtgInsumos.CurrentRow.Index).Value = TextBox2.Text
        dtgInsumos.Item(7, dtgInsumos.CurrentRow.Index).Value = reduce
        dtgInsumos.Item(2, dtgInsumos.CurrentRow.Index).Value = ComboBox1.Text
        dtgInsumos.Item(2, dtgInsumos.CurrentRow.Index).Value = TextBox1.Text
        dtgInsumos.Item(8, dtgInsumos.CurrentRow.Index).Value = cbxBodega.SelectedValue
        dtgInsumos.Item(10, dtgInsumos.CurrentRow.Index).Value = cbxBodega.Text
        Totales()
        txtIdInsumo.Clear()
        txtpresentacion.Clear()
        txtInsumo.Clear()
        txtEspecies.Focus()
        txtPUnitario.Text = "0.00"
        txtCantidad.Text = "0"
        CalcularEspeciesDesaprovechamiento()
        dtgInsumos.Enabled = True
    End Sub

    Private Sub txtCantidad_KeyDown1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.KeyDown
        Try
            If e.KeyCode = Keys.Tab Then
                txtClave.Focus()
            ElseIf e.KeyCode = Keys.Enter Then
                If nmPorciones.Value <> 0 Then
                    If txtInsumo.Text <> "" Then
                        If txtCantidad.Text <> "0" Then
                            If Me.TextBox2.Text = "0.00" Then
                                MsgBox("Revise el campo P/Unidad, puede estar incorrecto", MsgBoxStyle.Information)
                            Else

                                If ckReceta.Checked = True And ComboBox1.Text = "PORCIONES" Then
                                    txtCantidad.Text = (1 / (txtPUnitario.Text / TextBox2.Text)) * txtCantidad.Text
                                    TextBox2.Text = txtPUnitario.Text
                                End If

                                If Me.dtgInsumos.Enabled = True Then
                                    agregaLinea()
                                Else
                                    ModificarLinea()
                                End If

                            End If
                        Else
                            MessageBox.Show("Debe ingresar otro valor que no sea Cero", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        End If
                    Else
                        MessageBox.Show("Debe seleccionar un Insumo", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    End If
                Else
                    MessageBox.Show("El n�mero de porciones debe ser mayor que cero", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Verifique la cantidad", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub txtPUnitario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPUnitario.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtCantidad.Focus()
        End If
    End Sub

    Private Sub txtPUnitario_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPUnitario.KeyPress
        REM If IsNumeric(e.KeyChar) Then e.Handled = True
        If (Asc(e.KeyChar)) >= 48 And (Asc(e.KeyChar)) <= 57 Or (Asc(e.KeyChar)) = System.Windows.Forms.Keys.Back Or (Asc(e.KeyChar)) = 46 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtCantidad_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCantidad.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtCantidad.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If Keys.Enter Then
            txtCantidad.Focus()
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        CalculoReceta()
    End Sub

    Private Sub CalculoReceta()
        Dim multiplo As Double
        Dim cant As Array = (Me.txtpresentacion.Text).Split(" ")
        Dim cantidad As String
        Dim cc As Double
        Dim estado As String
        Try
            If (txtpresentacion.Text <> Nothing) Then
                cantidad = cant(0)
                multiplo = cConexion.SlqExecuteScalar(conectadobd, "Select Multiplo from TablaConversiones where NombreUnidad='" & CStr(cant(1)) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
                estado = cConexion.SlqExecuteScalar(conectadobd, "Select Estado from TablaConversiones where NombreUnidad='" & CStr(cant(1)) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
                If ckReceta.Checked = True And ComboBox1.Text = "PORCIONES" Then
                    multiplo = 1 ' = cConexion.SlqExecuteScalar(conectadobd, "Select Porciones from Recetas where id=" & txtIdInsumo.Text)
                End If
                cc = CDbl(cantidad) * multiplo
                If (cc = 0) Then cc = 1
                TextBox2.Text = CDbl(txtPUnitario.Text) / cc
                TextBox1.Text = ComboBox1.Text
                Label15.Text = estado
                txtCantidad.Focus()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub txtEspecies_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtEspecies.KeyDown

        Try
            If e.KeyCode = Keys.Enter Then
                CalcularEspeciesDesaprovechamiento()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub TextBox3_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox3.KeyDown

        Try
            CalcularEspeciesDesaprovechamiento()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub CalcularEspeciesDesaprovechamiento()

        If txtEspecies.Text = "" Then
            txtEspecies.Text = 0
        End If
        If TextBox3.Text = "" Then
            TextBox3.Text = 0
        End If
        especies = CDbl(txtSubTotal.Text) * (CDbl(txtEspecies.Text) / 100)
        desaprovechamiento = CDbl(txtSubTotal.Text) * (CDbl(TextBox3.Text) / 100)
        txtCPorcion.Text = Math.Round((CDbl(txtSubTotal.Text) + especies + desaprovechamiento) / Me.nmPorciones.Value, 2)
        txtCTotal.Text = Math.Round((CDbl(txtSubTotal.Text) + especies + desaprovechamiento), 2) '/ Me.nmPorciones.Value

    End Sub

    Private Sub txtEspecies_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEspecies.KeyPress

        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtEspecies.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If

    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress

        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = TextBox3.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If

    End Sub

    Private Sub cbxBodega_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxBodega.KeyDown

        If e.KeyCode = Keys.Enter Then
            IDBODEGA_DESCARGA = cbxBodega.SelectedValue
            ComboBox1.Focus()
        End If

    End Sub


    Private Sub ckReceta_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ckReceta.CheckStateChanged
        If ckReceta.CheckState = CheckState.Checked Then
            cbxBodega.SelectedIndex = 0
            cbxBodega.Enabled = False
        Else
            cbxBodega.Enabled = True
        End If
    End Sub

#Region "Validacion Usuario"
    Private Sub txtClave_KeyDown1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")
            If usuario <> "" Then
                txtCedula.Text = cedula
                txtClave.Text = ""
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBarButton1.Enabled = True
                Me.txtUsuario.Text = usuario
            Else
                Controles(False)
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBarButton1.Enabled = True
                Me.txtUsuario.Text = User_Log.Nombre
                cedula = User_Log.Cedula
                txtCedula.Text = cedula
                txtClave.Text = ""
                txtnombre.Focus()
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub FillByToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FillByToolStripButton.Click
        Try
            Me.BodegaTableAdapter.FillBy(Me.ProveeduriaDataSet2.Bodega)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class