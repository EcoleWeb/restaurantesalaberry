﻿Friend Class clsReporteRecetas
    Sub spCargarCostosReceta()
        cFunciones.Llenar_Tabla_Generico("Select * From VCostoDetalleReceta", ds.VCostoDetalleReceta)

    End Sub
    Dim ds As New DataSetRecetaImp
    Dim dtBodegas As New DataTable
    Private Function fnNombreBodega(IdBodega As Integer)
        For i As Integer = 0 To dtBodegas.Rows.Count - 1
            If dtBodegas.Rows(i).Item("IdBodega") = IdBodega Then
                Return dtBodegas.Rows(i).Item("Nombre")
            End If
        Next
        Return ""
    End Function

    Friend Sub imprimirPorDataSet(Optional IdRecetaEspecifica As Integer = 0)

        Dim cnx As New SqlClient.SqlConnection
        Dim consultaReceta As String = "SELECT ID, Nombre, Porciones, CostoTotal, CostoPorcion AS CostoPorciones, Especies, aprovechamiento AS Desaprovechamiento " & _
                                        " FROM Recetas"

        cFunciones.Llenar_Tabla_Generico(consultaReceta, ds.Receta)
        Dim consultaDetalles As String = "Select * FROM Recetas_Detalles"
        cFunciones.Llenar_Tabla_Generico(consultaDetalles, ds.Recetas_Detalles)
        Dim consultaConfiguraciones As String = "Select Cedula, Empresa, Tel_01, Tel_02, Fax_01, Fax_02, Direccion, Logo FROM Configuraciones"
        cFunciones.Llenar_Tabla_Generico(consultaConfiguraciones, ds.Configuraciones)
        cFunciones.Llenar_Tabla_Generico("Select Nombre, IdBodega from Bodega ", dtBodegas)
        spCargarCostosReceta()

        For j As Integer = 0 To ds.Receta.Count - 1
            ds.Receta(j).CostoPorciones = CalculoTotalReceta(ds.Receta(j).Id) '* nmPorciones.Value
            ds.Receta(j).CostoTotal = ds.Receta(j).CostoPorciones * ds.Receta(j).Porciones
        Next

        For i As Integer = 0 To ds.Recetas_Detalles.Count - 1
            If ds.Recetas_Detalles(i).Articulo = False Then
                ds.Recetas_Detalles(i).CostoUnit = Me.CalculoTotalReceta(ds.Recetas_Detalles(i).Codigo) ', ds.Recetas_Detalles(i).Cantidad) '* nmPorciones.Value
                ds.Recetas_Detalles(i).CostoTotal = ds.Recetas_Detalles(i).CostoUnit * ds.Recetas_Detalles(i).Cantidad
            Else
                ds.Recetas_Detalles(i).CostoUnit = Me.CalculaCostoArticulo(ds.Recetas_Detalles(i).Codigo, ds.Recetas_Detalles(i).idBodegaDescarga, ds.Recetas_Detalles(i).Descripcion, ds.Recetas_Detalles(i).IdReceta) '* nmPorciones.Value
                ds.Recetas_Detalles(i).CostoTotal = ds.Recetas_Detalles(i).CostoUnit * ds.Recetas_Detalles(i).Cantidad
            End If
            ds.Recetas_Detalles(i).Bodega = fnNombreBodega(ds.Recetas_Detalles(i).idBodegaDescarga)

        Next

        Dim rpt As New CrystalReportRecetas
        rpt.SetDataSource(ds)
        If IdRecetaEspecifica = 0 Then
            rpt.SetParameterValue(0, False)
        Else
            rpt.SetParameterValue(0, True)
        End If

        rpt.SetParameterValue(1, IdRecetaEspecifica)

        Dim visor As New frmVisorReportes
        visor.rptViewer.ReportSource = rpt
        visor.ShowDialog()
        visor.Dispose()
        ds = Nothing
    End Sub

    Function CalculoTotalReceta(ByVal Id_Receta As Integer) As Double
        Dim costo_enbodega As Double = 0
        Dim costo_total As Double = 0
        Dim Totales As Double = 0
        Dim especiesl As Double = 0
        Dim aprovechamientol As Double = 0
        Dim porciones As Double = 0
        'Dim cnx As New SqlClient.SqlConnection
        'cFunciones.Llenar_Tabla_Generico("Select Aprovechamiento, Especies, Porciones From Recetas Where id =" & Id_Receta, dtTable)

        Dim RowFiltro = (From i As DataSetRecetaImp.RecetaRow In ds.Receta Where i.Id = Id_Receta)
        For Each r As DataSetRecetaImp.RecetaRow In RowFiltro
            especiesl = r.Especies
            aprovechamientol = r.Desaprovechamiento
            porciones = r.Porciones
        Next



        'AQUI SE OBTIENEN LOS DATOS DE LA RECETA O DEL ARTICULO DE MENU QUE SE COMPONE LA RECETA
        Dim RowFiltroDetalle = (From i As DataSetRecetaImp.Recetas_DetallesRow In ds.Recetas_Detalles Where i.IdReceta = Id_Receta)
        For Each r As DataSetRecetaImp.Recetas_DetallesRow In RowFiltroDetalle

            If r.Articulo = True Then
                costo_enbodega = CalculaCostoArticulo(r.Codigo, r.idBodegaDescarga, r.Descripcion, r.IdReceta)
                costo_total = costo_enbodega * r.Cantidad
            Else
                costo_enbodega = Me.CalculoTotalReceta(r.Codigo) 'Me.CalculaCostoReceta(dtTable.Rows(i).Item("codigo"), dtTable.Rows(i).Item("Cantidad"))
                costo_total = costo_enbodega * r.Cantidad
            End If
            If costo_enbodega = 0 Then
                costo_enbodega = r.CostoUnit
                costo_total = r.CostoTotal
            End If
            Totales += costo_total
        Next

        Dim especiesD As Double = CDbl(Totales) * (CDbl(especiesl) / 100)
        Dim desaprovechamientoD As Double = Totales * (CDbl(aprovechamientol) / 100)
        Totales = (Totales + especiesD + desaprovechamientoD) / porciones

        Return Totales

    End Function
    Private Function CalculaCostoArticulo(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal Receta As Integer) As Double
        Try
            Dim costoXarticulo As Double
            Dim RowFiltroDetalle = (From i As DataSetRecetaImp.VCostoDetalleRecetaRow In ds.VCostoDetalleReceta Where i.idBodegaDescarga = idbodega_descarga And i.Codigo = codigo_articulo And i.IdReceta = Receta)

            For Each r As DataSetRecetaImp.VCostoDetalleRecetaRow In RowFiltroDetalle
                costoXarticulo = r.CostoUnit
            Next

            '     costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoUnit From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & "and IdReceta = " & Receta)
            If costoXarticulo = 0 Then
                ' MsgBox("El Articulo " + nombre + " no se ha registrado en la bodega que se indico para descarga, se utilizará el precio base para el calculo de la receta", MsgBoxStyle.OkOnly, "Servicios Estructurales SeeSoft")
                costoXarticulo = 0

            End If
            Return costoXarticulo
            ' cnx.Close()
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function

End Class
