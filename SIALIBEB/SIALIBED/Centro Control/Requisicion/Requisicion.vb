Imports System.Data.SqlClient
Public Class Requisicion
    Dim bodegas As New List(Of String)

    Private Function EXISTE(ByVal _nombre As String) As Boolean
        For i As Integer = 0 To bodegas.Count - 1
            If bodegas.Item(i) = _nombre Then
                Return True
            End If
        Next
        Return False
    End Function

    Sub Trans_Asientos()
        If CadenaContabilidad.State <> ConnectionState.Open Then CadenaContabilidad.Open()
        Dim Trans As SqlTransaction = CadenaContabilidad.BeginTransaction
        Try
            adAsientos.UpdateCommand.Transaction = Trans
            adAsientos.InsertCommand.Transaction = Trans
            adAsientos.DeleteCommand.Transaction = Trans
            adAsientesDetalle.UpdateCommand.Transaction = Trans
            adAsientesDetalle.InsertCommand.Transaction = Trans
            adAsientesDetalle.DeleteCommand.Transaction = Trans

            Me.adAsientos.Update(Me.DsRequisicion1, "AsientosContables")
            Me.adAsientesDetalle.Update(Me.DsRequisicion1, "DetallesAsientosContable")
            Trans.Commit()
        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub Guardar_Asiento()
        DsRequisicion1.AsientosContables.Clear()
        DsRequisicion1.DetallesAsientosContable.Clear()
        Dim Fx As New cFunciones
        BindingContext(DsRequisicion1, "AsientosContables").EndCurrentEdit()
        BindingContext(DsRequisicion1, "AsientosContables").AddNew()
        BindingContext(DsRequisicion1, "AsientosContables").Current("NumAsiento") = Fx.BuscaNumeroAsiento("BUF-" & Format(Me.dtFecha.Value.Month, "00") & Format(Me.dtFecha.Value, "yy") & "-")
        BindingContext(DsRequisicion1, "AsientosContables").Current("Fecha") = dtFecha.Value
        BindingContext(DsRequisicion1, "AsientosContables").Current("IdNumDoc") = 0
        BindingContext(DsRequisicion1, "AsientosContables").Current("NumDoc") = 0
        BindingContext(DsRequisicion1, "AsientosContables").Current("Beneficiario") = "Buffet : " & Me.txtnombre.Text
        BindingContext(DsRequisicion1, "AsientosContables").Current("TipoDoc") = 15
        BindingContext(DsRequisicion1, "AsientosContables").Current("Accion") = "AUT"
        BindingContext(DsRequisicion1, "AsientosContables").Current("Anulado") = 0
        BindingContext(DsRequisicion1, "AsientosContables").Current("Mayorizado") = 0
        BindingContext(DsRequisicion1, "AsientosContables").Current("FechaEntrada") = Now.Date
        BindingContext(DsRequisicion1, "AsientosContables").Current("Periodo") = Fx.BuscaPeriodo(Me.dtFecha.Value.ToShortDateString)
        BindingContext(DsRequisicion1, "AsientosContables").Current("NumMayorizado") = 0
        BindingContext(DsRequisicion1, "AsientosContables").Current("Modulo") = "Buffet"
        BindingContext(DsRequisicion1, "AsientosContables").Current("Observaciones") = "Buffet : " & Me.txtnombre.Text
        BindingContext(DsRequisicion1, "AsientosContables").Current("NombreUsuario") = txtUsuario.Text
        BindingContext(DsRequisicion1, "AsientosContables").Current("CodMoneda") = 1
        BindingContext(DsRequisicion1, "AsientosContables").Current("TipoCambio") = 1
        BindingContext(DsRequisicion1, "AsientosContables").EndCurrentEdit()
        'CREA LOS DETALLES DE ASIENTOS CONTABLES

        Dim costo_total As Decimal
        bodegas.Clear()
        'tomo los distintos nombres de las bodegas
        For Each fila As DataGridViewRow In dtgInsumos.Rows
            If EXISTE(fila.Cells("Bodega").Value) = False Then
                bodegas.Add(fila.Cells("Bodega").Value)
            End If
        Next
        'tomo el monto total del costo
        For Each fila As DataGridViewRow In dtgInsumos.Rows
            costo_total += Val(fila.Cells(5).Value)
        Next

        If GetSetting("SeeSOFT", "Restaurante", "CuentaBufet").Equals("") Then SaveSetting("SeeSOFT", "Restaurante", "CuentaBufet", "")
        Dim funcion As New cFunciones
        Dim Id As String = ""
        Id = GetSetting("seesoft", "Restaurante", "CuentaBufet")
        Dim dt As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT CuentaContable, Descripcion FROM   CuentasContablesConMovimiento Where CuentaContable= '" & Id & "'", dt, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
        If Not dt.Rows.Count > 0 Then
            Id = ""
        End If
        While Id = ""
            Id = funcion.BuscarDatos("Select * from CuentasContablesConMovimiento", "descripcion", "Distribuir Monto " & Format(costo_total, "###.00"), GetSetting("SeeSoft", "Contabilidad", "Conexion"))
        End While
        cFunciones.Llenar_Tabla_Generico("SELECT CuentaContable, Descripcion FROM   CuentasContablesConMovimiento Where CuentaContable= '" & Id & "'", dt, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
        If dt.Rows.Count > 0 Then
            GuardaAsientoDetalle(costo_total, True, False, dt.Rows(0).Item("CuentaContable"), dt.Rows(0).Item("Descripcion"))
        End If

        Dim costo As Decimal
        For i As Integer = 0 To bodegas.Count - 1

            For Each fila As DataGridViewRow In dtgInsumos.Rows
                If fila.Cells("Bodega").Value = bodegas.Item(i) Then
                    costo += Val(fila.Cells(5).Value)
                End If
            Next

            Dim dts As New DataTable
            If bodegas(i) <> "" Then
                dts = GetCuentaBodega(bodegas(i))
                GuardaAsientoDetalle(Math.Round(costo, 2), False, True, dts.Rows(0).Item("CuentaContable"), dts.Rows(0).Item("Descripcion"))
                costo = 0
            End If

        Next

        BindingContext(DsRequisicion1, "AsientosContables").Current("TotalDebe") = costo_total
        BindingContext(DsRequisicion1, "AsientosContables").Current("TotalHaber") = costo_total
        BindingContext(DsRequisicion1, "AsientosContables").EndCurrentEdit()
    End Sub

    Public Function GetCuentaBodega(ByVal _nombre As String) As DataTable
        Dim dts As New DataTable
        cFunciones.Llenar_Tabla_Generico("select CuentaContable as CuentaContable, DescripcionCuentaContable as Descripcion from bodega where nombre = '" & _nombre & "'", dts, GetSetting("SeeSoft", "PROVEEDURIA", "Conexion"))
        Return dts
    End Function

    Public Sub GuardaAsientoDetalle(ByVal Monto As Double, ByVal Debe As Boolean, ByVal Haber As Boolean, ByVal Cuenta As String, ByVal NombreCuenta As String)
        Dim fx As New cFunciones
        Dim TipoCambio As Double = 1
        Try
            If Monto <> 0 Then

                'If engrosarlacuenta(Monto, Debe, Haber, Cuenta, NombreCuenta) Then

                '    Exit Sub
                'End If
                'CREA LOS DETALLES DE ASIENTOS CONTABLES
                BindingContext(DsRequisicion1, "DetallesAsientosContable").EndCurrentEdit()
                BindingContext(DsRequisicion1, "DetallesAsientosContable").AddNew()
                BindingContext(DsRequisicion1, "DetallesAsientosContable").Current("NumAsiento") = BindingContext(DsRequisicion1, "AsientosContables").Current("NumAsiento")
                BindingContext(DsRequisicion1, "DetallesAsientosContable").Current("DescripcionAsiento") = BindingContext(DsRequisicion1, "AsientosContables").Current("Observaciones")
                BindingContext(DsRequisicion1, "DetallesAsientosContable").Current("Cuenta") = Cuenta
                BindingContext(DsRequisicion1, "DetallesAsientosContable").Current("NombreCuenta") = NombreCuenta
                BindingContext(DsRequisicion1, "DetallesAsientosContable").Current("Monto") = Monto
                BindingContext(DsRequisicion1, "DetallesAsientosContable").Current("Debe") = Debe
                BindingContext(DsRequisicion1, "DetallesAsientosContable").Current("Haber") = Haber
                BindingContext(DsRequisicion1, "DetallesAsientosContable").Current("TipoCambio") = TipoCambio
                BindingContext(DsRequisicion1, "DetallesAsientosContable").EndCurrentEdit()

            End If
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim cConexion1 As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim idReceta As String
    Dim actualiza As Boolean = False
    Dim cedula As String
    Dim unidad As String
    Dim reduce As Double
    Dim PMU As New PerfilModulo_Class
    Dim puntos As Integer
    Dim especies, desaprovechamiento As Double
    Dim desperdicio, especies1 As Double
    Dim iv As Double
    Dim Total As Single
    Dim subprecio As Double
    Dim PresentaCant As Double
    Dim IDBODEGA_DESCARGA As Integer = 7005
    Dim NombreBodega As String
#End Region

#Region "Load"

    Private Sub INICIALIZAASIENTOS()
        'VALORES POR DEFECTO PARA LA TABLA ASIENTOS
        DsRequisicion1.AsientosContables.FechaColumn.DefaultValue = Now.Date
        DsRequisicion1.AsientosContables.NumDocColumn.DefaultValue = "0"
        DsRequisicion1.AsientosContables.IdNumDocColumn.DefaultValue = 0
        DsRequisicion1.AsientosContables.BeneficiarioColumn.DefaultValue = ""
        DsRequisicion1.AsientosContables.TipoDocColumn.DefaultValue = 5
        DsRequisicion1.AsientosContables.AccionColumn.DefaultValue = "AUT"
        DsRequisicion1.AsientosContables.AnuladoColumn.DefaultValue = 0
        DsRequisicion1.AsientosContables.FechaEntradaColumn.DefaultValue = Now.Date
        DsRequisicion1.AsientosContables.MayorizadoColumn.DefaultValue = 0
        DsRequisicion1.AsientosContables.PeriodoColumn.DefaultValue = Now.Month & "/" & Now.Year
        DsRequisicion1.AsientosContables.NumMayorizadoColumn.DefaultValue = 0
        DsRequisicion1.AsientosContables.ModuloColumn.DefaultValue = "Buffet"
        DsRequisicion1.AsientosContables.ObservacionesColumn.DefaultValue = ""
        DsRequisicion1.AsientosContables.NombreUsuarioColumn.DefaultValue = ""
        DsRequisicion1.AsientosContables.TotalDebeColumn.DefaultValue = 0
        DsRequisicion1.AsientosContables.TotalHaberColumn.DefaultValue = 0
        DsRequisicion1.AsientosContables.CodMonedaColumn.DefaultValue = 1
        DsRequisicion1.AsientosContables.TipoCambioColumn.DefaultValue = 1

        'VALORES POR DEFECTO PARA LA TABLA DETALLES ASIENTOS
        DsRequisicion1.DetallesAsientosContable.NumAsientoColumn.DefaultValue = ""
        DsRequisicion1.DetallesAsientosContable.DescripcionAsientoColumn.DefaultValue = ""
        DsRequisicion1.DetallesAsientosContable.CuentaColumn.DefaultValue = ""
        DsRequisicion1.DetallesAsientosContable.NombreCuentaColumn.DefaultValue = ""
        DsRequisicion1.DetallesAsientosContable.MontoColumn.DefaultValue = 0
        DsRequisicion1.DetallesAsientosContable.DebeColumn.DefaultValue = 0
        DsRequisicion1.DetallesAsientosContable.HaberColumn.DefaultValue = 0
        DsRequisicion1.DetallesAsientosContable.TipocambioColumn.DefaultValue = 1
    End Sub


    Private Sub Requisicion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
        SqlConnection2.ConnectionString = GetSetting("SeeSoft", "Proveeduria", "Conexion")
        CadenaContabilidad.ConnectionString = GetSetting("SeeSoft", "Contabilidad", "Conexion")

        conectadobd = cConexion.Conectar("Restaurante")
        adBodegas.Fill(DsRequisicion1.Bodega)
        adConversiones.Fill(DsRequisicion1.TablaConversiones)
        limpia()
        INICIALIZAASIENTOS()
        Controles(False)
        dtgInsumos.EditMode = DataGridViewEditMode.EditProgrammatically
        Dim style As New DataGridViewCellStyle
        With style
            .BackColor = Color.Beige
            .ForeColor = Color.Brown
            .Font = New Font("Verdana", 10)
        End With
        ToolBar2.Buttons(1).Enabled = False
        dtgInsumos.DefaultCellStyle = style

        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
      
        If gloNoClave Then
            Loggin_Usuario()
        Else
            txtClave.TabIndex = 0
            txtClave.Focus()
        End If

      
        '---------------------------------------------------------------
        dtFecha.Value = Now
    End Sub

    Private Sub limpia()
        conectadobd = cConexion.Conectar("Restaurante")
        If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
        txtnombre.Focus()
        subprecio = 0
        txtIdReceta.Text = ""
        txtnombre.Text = ""
        nmPax.Text = "0"
        txtInsumo.Text = ""
        txtIdInsumo.Text = ""
        txtPUnitario.Text = "0.0"
        txtCantidad.Text = "0"
        txtSubTotal.Text = "0.00"
        txtPorEspecies.Text = "0.00"
        txtEspecie.Text = "0.00"
        txtPorDesapro.Text = "0.00"
        txtDesapro.Text = "0.00"
        txtCTotal.Text = "0.00"
        dtgInsumos.Rows.Clear()
        Edicion = False
    End Sub

    Private Sub Controles(ByVal Estado As Boolean)
        txtnombre.Enabled = Estado
        nmPax.Enabled = Estado
        txtInsumo.Enabled = Estado
        ComboBox1.Enabled = Estado
        txtCantidad.Enabled = Estado
        txtSubTotal.Enabled = Estado
        txtPorEspecies.Enabled = Estado
        txtEspecie.Enabled = Estado
        txtPorDesapro.Enabled = Estado
        txtDesapro.Enabled = Estado
        txtCTotal.Enabled = Estado
        dtgInsumos.Enabled = Estado
        dtFecha.Enabled = Estado
        cbxBodega.Enabled = Estado
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBarButton1.Enabled = True
                Me.txtUsuario.Text = User_Log.Nombre
                cedula = User_Log.Cedula
                txtCedula.Text = cedula
                txtClave.Text = ""
                txtnombre.Focus()
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region

#Region "Operaci�n"

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 1
                If (ToolBarNuevo.Text = "Nuevo") Then
                    ToolBarNuevo.ImageIndex = 7
                    ToolBarNuevo.Text = "Cancelar"
                    limpia()
                    Controles(True)
                    Label19.Visible = False
                    actualiza = False
                    ToolBarEliminar.Enabled = False
                    ToolBarBuscar.Enabled = False
                    ToolBarImprimir.Enabled = False
                    dtgInsumos.Enabled = True
                    txtNumero.Text = ""
                    ToolBar2.Buttons(3).Enabled = True
                    Edicion = False
                    txtnombre.Focus()
                Else
                    ToolBarNuevo.ImageIndex = 0
                    ToolBarNuevo.Text = "Nuevo"
                    limpia()
                    Controles(False)
                    actualiza = False
                    ToolBarEliminar.Enabled = False
                    ToolBarBuscar.Enabled = True
                    ToolBarImprimir.Enabled = False
                    dtgInsumos.Enabled = True
                    ToolBar2.Buttons(3).Enabled = False
                    Edicion = False
                End If

            Case 3 : If PMU.Find Then modificar_requisicion() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Update Then agregar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Delete Then Anular() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 6 : If PMU.Print Then imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : Teclado()
            Case 8 : Close()
        End Select
    End Sub

    Private Sub imprimir()
        If idReceta = "" Then
            idReceta = txtIdReceta.Text
        End If

        Try
            If idReceta = "" Then
                MessageBox.Show("Seleccione un buffet para imprimir", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
            Dim rptreceta As New rptRequisicion
            Dim visor As New frmVisorReportes
            rptreceta.SetParameterValue(0, idReceta)
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, rptreceta, False, Me.conectadobd.ConnectionString)
            Me.Hide()
            visor.ShowDialog()
            visor.Dispose()
            Me.Show()
        Catch ex As Exception
            MessageBox.Show("Error al cargar el reporte", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
        '
    End Sub

    Private Sub Anular()
        Dim idInsumo, nInsumo, UConv As String
        Dim bodega_descarga As Integer
        Dim pPorcion, pCantidad, pCosto, pU, resta As Double
        Try

            Dim con As New data_management(GetSetting("seesoft", "restaurante", "conexion"))
            'busco el numero de asiento
            Dim asiento As String = con.Ejecuta("select isnull(asiento,0) as asiento from RequisicionRest where id = " & txtIdReceta.Text).Rows(0).Item(0)
            'anulo el asiento
            con.StringConection = GetSetting("seesoft", "contabilidad", "conexion")

                If asiento <> "0" Then
                    Dim edita As String
                    rs.Close()
                    Dim cx As New Conexion

                    edita = cx.SlqExecuteScalar(cx.Conectar("Contabilidad"), "select Mayorizado from asientoscontables where numasiento = '" & asiento & "'")
                    cx.DesConectar(cx.Conectar("Contabilidad"))
                    If edita = "False" Then
                        con.Ejecuta("update asientoscontables set anulado = 1 where numasiento = '" & asiento & "'")
                    Else
                    MessageBox.Show("No se puede Anular el registro de Buffet.", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        Exit Sub
                    End If
                Else
                MessageBox.Show("No se puede Anular el registro de Buffet.", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

                cConexion.UpdateRecords(conectadobd, "RequisicionRest", "Anulado= 1", "id=" & txtNumero.Text)
                For Each row As DataGridViewRow In dtgInsumos.Rows
                    idInsumo = row.Cells(0).Value 'id Insumo
                    pCantidad = row.Cells(1).Value 'Cantidad del insumo en la porcion
                    UConv = row.Cells(2).Value  'unidad de conversion
                    nInsumo = row.Cells(3).Value 'Descripcion del insumo
                    pPorcion = row.Cells(4).Value 'Precio Unitario
                    pCosto = row.Cells(5).Value 'Costo del Insumo en la porcion
                    pU = row.Cells(6).Value
                    resta = row.Cells(7).Value
                    bodega_descarga = row.Cells(8).Value 'Bodega de descarga
                    If nInsumo = "" Then
                        Exit For
                    End If
                    DescargaInventario(idInsumo, resta, bodega_descarga, True)
                Next
                MessageBox.Show("Buffet fue Anulado Correctamente ....", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ToolBar2.Buttons(3).Enabled = False
            ToolBar2.Buttons(4).Enabled = False : Label19.Visible = True
                Edicion = False
        Catch ex As Exception
            MessageBox.Show("Error al Anular el buffet: " & ex.Message, "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub agregar()
        Try
            ToolBarEliminar.Enabled = False
            especies1 = CDbl(txtEspecie.Text)
            desperdicio = CDbl(txtPorDesapro.Text)

            If txtCedula.Text = "" Then
                MessageBox.Show("Debe validar el usuario que ingreso el Buffet", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If

            If dtgInsumos.RowCount <= 1 Then
                MessageBox.Show("Debe ingresar al menos un art�culo al Buffet", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If

            If txtnombre.Text = "" Or nmPax.Value = 0 Then
                MessageBox.Show("Verifique el nombre y el n�mero de porciones de la Receta", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If
            Dim idInsumo, nInsumo, UConv As String
            Dim bodega_descarga As Integer
            Dim pPorcion, pCantidad, pCosto, pU, resta As Double
            If actualiza = False Then
                Guardar_Asiento()
                idReceta = cConexion.SlqExecuteScalar(conectadobd, "Insert into RequisicionRest(Concepto,CedUsuario,Nombre_usuario,Pax,Especie,Desaprovechamiento, Subtotal, MontoEspecie, MontoDesaprovechamiento, Total,Asiento) values('" & txtnombre.Text & "','" & txtCedula.Text & "','" & txtUsuario.Text & "'," & nmPax.Value & "," & txtPorEspecies.Text & "," & txtPorDesapro.Text & "," & Format(CDbl(txtSubTotal.Text), "#####0.00") & "," & Format(CDbl(txtEspecie.Text), "#####0.00") & "," & Format(CDbl(txtDesapro.Text), "#####0.00") & "," & Format(CDbl(txtCTotal.Text), "#####0.00") & ",'" & BindingContext(DsRequisicion1, "AsientosContables").Current("NumAsiento") & "') select max(id)as Requisicion from RequisicionRest")
                Trans_Asientos()
            Else
                Dim con As New data_management(GetSetting("seesoft", "restaurante", "conexion"))
                'busco el numero de asiento
                Dim asiento As String = con.Ejecuta("select isnull(asiento,0) as asiento from RequisicionRest where id = " & txtIdReceta.Text).Rows(0).Item(0)
                'anulo el asiento
                con.StringConection = GetSetting("seesoft", "contabilidad", "conexion")
                If asiento <> "0" Then
                    Dim edita As String
                    rs.Close()
                    Dim cx As New Conexion

                    edita = cx.SlqExecuteScalar(cx.Conectar("Contabilidad"), "select Mayorizado from asientoscontables where numasiento = '" & asiento & "'")
                    cx.DesConectar(cx.Conectar("Contabilidad"))
                    If edita = "False" Then
                        con.Ejecuta("update asientoscontables set anulado = 1 where numasiento = '" & asiento & "'")
                    Else
                        MessageBox.Show("No se puede editar el registro de Buffet.", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        Exit Sub
                    End If
                Else
                    MessageBox.Show("No se puede editar el registro de Buffet.", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If
                
                Guardar_Asiento()
                cConexion.UpdateRecords(conectadobd, "RequisicionRest", "Concepto='" & txtnombre.Text & "', CedUsuario='" & txtCedula.Text & "', Nombre_usuario='" & txtUsuario.Text & "', Pax=" & nmPax.Value & ", Especie=" & txtPorEspecies.Text & ", Desaprovechamiento=" & txtPorDesapro.Text & ",Subtotal = " & Format(CDbl(txtSubTotal.Text), "#####0.00") & ",MontoDesaprovechamiento= " & Format(CDbl(txtDesapro.Text), "#####0.00") & ",Total=" & Format(CDbl(txtCTotal.Text), "#####0.00") & ", MontoEspecie=" & Format(CDbl(txtEspecie.Text), "#####0.00") & ", Asiento='" & BindingContext(DsRequisicion1, "AsientosContables").Current("NumAsiento") & "'", "id=" & txtIdReceta.Text)
                Trans_Asientos()

                cConexion.GetRecorset(conectadobd, "Select Codigo,Disminuye,idBodegaDescarga from RequisicionRest_Detalle Where IdRequisicion =" & txtIdReceta.Text, rs)
                With rs.Read
                    DescargaInventario(rs("Codigo"), rs("Disminuye"), rs("idBodegaDescarga"), True)
                End With
                rs.Close()
                cConexion.DeleteRecords(conectadobd, "RequisicionRest_Detalle", "IdRequisicion=" & txtIdReceta.Text)
                idReceta = txtIdReceta.Text
                actualiza = False
            End If

            For Each row As DataGridViewRow In dtgInsumos.Rows
                idInsumo = row.Cells(0).Value 'id Insumo
                pCantidad = row.Cells(1).Value 'Cantidad del insumo en la porcion
                UConv = row.Cells(2).Value  'unidad de conversion
                nInsumo = row.Cells(3).Value 'Descripcion del insumo
                pPorcion = row.Cells(4).Value 'Precio Unitario
                pCosto = row.Cells(5).Value 'Costo del Insumo en la porcion
                pU = row.Cells(6).Value
                resta = row.Cells(7).Value
                bodega_descarga = row.Cells(8).Value 'Bodega de descarga
                If nInsumo = "" Then
                    Exit For
                End If
                cConexion.AddNewRecord(conectadobd, "RequisicionRest_Detalle", "IdRequisicion,codigo,descripcion,cantidad,costoUnit,costoTotal,UConversion,PUConversion,Disminuye,idBodegaDescarga", idReceta & "," & idInsumo & ",'" & nInsumo & "'," & pCantidad & "," & pPorcion & "," & pCosto & ",'" & UConv & "'," & pU & "," & Math.Round(resta, 4) & "," & bodega_descarga)
                DescargaInventario(idInsumo, resta, bodega_descarga, False)
            Next

            If MessageBox.Show("El buffet se registro correctamente", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.OK Then
                limpia()
                actualiza = False
                Controles(False)
            End If
            ToolBar2.Buttons(3).Enabled = False
            ToolBar2.Buttons(5).Enabled = True

            DsRequisicion1.AsientosContables.Clear()
            DsRequisicion1.DetallesAsientosContable.Clear()

            imprimir()

        Catch ex As Exception
            MessageBox.Show("Error al guardar el buffet: " & ex.Message, "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try

    End Sub


    Private Sub modificar_requisicion()
        Dim fbuscador As New buscador
        Dim costo_enbodega As Double
        Dim costo_total As Double
        Dim cnx As New SqlClient.SqlConnection
        Dim exe As New ConexionR
        idReceta = ""
        dtgInsumos.Rows.Clear() : fbuscador.check = 1
        fbuscador.ToolBarImprimir.Enabled = False : fbuscador.TituloModulo.Text = "Buscando Buffet"
        fbuscador.bd = "Restaurante" : fbuscador.tabla = "RequisicionRest"
        fbuscador.busca = "Nombre" : fbuscador.cantidad = 3 : fbuscador.lblEncabezado.Text = "BUFFETS"
        fbuscador.consulta = "SELECT Id, Concepto AS Nombre, Pax,Especie,Desaprovechamiento FROM RequisicionRest"
        fbuscador.ShowDialog()

        If fbuscador.Icodigo = 0 Then
            Exit Sub
        End If

        txtnombre.Text = fbuscador.descripcion
        txtIdReceta.Text = fbuscador.Icodigo
        nmPax.Value = fbuscador.detalle
        ToolBarEliminar.Enabled = True
        ToolBarImprimir.Enabled = True
        Controles(True)
        cConexion.GetRecorset(conectadobd, "Select Id, Desaprovechamiento, Especie, Anulado From RequisicionRest Where id =" & txtIdReceta.Text, rs)
        If rs.Read() Then
            txtNumero.Text = rs("Id")
            txtPorEspecies.Text = rs("Especie")
            txtPorDesapro.Text = rs("Desaprovechamiento")
            If rs("Anulado") = True Then
                Label19.Visible = True
                ToolBar2.Buttons(3).Enabled = False
                ToolBar2.Buttons(4).Enabled = False
            Else
                Label19.Visible = False
                ToolBar2.Buttons(3).Enabled = True
                ToolBar2.Buttons(4).Enabled = True
            End If
        End If
        rs.Close()
        'AQUI SE OBTIENEN LOS DATOS DE LA RECETA O DEL ARTICULO DE MENU QUE SE COMPONE LA RECETA
        cConexion.GetRecorset(conectadobd, "Select IdRequisicion,codigo,descripcion,cantidad,costoUnit,costoTotal,UConversion,PUConversion,Disminuye,idBodegaDescarga from RequisicionRest_Detalle Where IdRequisicion=" & txtIdReceta.Text, rs)
        While rs.Read
            costo_enbodega = rs("costoUnit")
            costo_total = rs("costoTotal")

            '------------------------------------------------------------------------
            'AQUI SE OBTIENEN EL NOMBRE DE LA BODEGA
            cnx.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()
            NombreBodega = cConexion.SlqExecuteScalar(cnx, "Select Bodega.Nombre from Bodega where Bodega.IdBodega= " & rs("idBodegaDescarga"))
            If cnx.State <> ConnectionState.Closed Then cnx.Close()
            '------------------------------------------------------------------------

            Dim row0 As String() = {rs("codigo"), rs("cantidad"), rs("UConversion"), rs("descripcion"), Math.Round(costo_enbodega, 2), Math.Round(costo_total, 2), rs("PUConversion"), rs("Disminuye"), rs("idBodegaDescarga"), NombreBodega}
            With dtgInsumos.Rows
                .Add(row0)
                Totales()
            End With
        End While
        rs.Close()
        CalcularEspeciesDesaprovechamiento()
        If txtIdReceta.Text <> "" Then
            actualiza = True
            'Controles(True)
        End If
    End Sub

    'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA ESPECIFICAMENTE, BODEGA CODIGO
    Private Function CalculaCostoArticulo(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal Receta As Integer) As Double
        Try
            Dim costoXarticulo As Double
            Dim exe As New ConexionR
            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()

            costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoUnit From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & "and IdReceta = " & Receta)
            If costoXarticulo = 0 Then
                MsgBox("El Articulo " + nombre + " no se ha registrado en la bodega que se indico para descarga, se utilizar� el precio base para el calculo de la receta", MsgBoxStyle.OkOnly, "Servicios Estructurales SeeSoft")
                costoXarticulo = 0
            End If
            Return costoXarticulo
            cnx.Close()
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function


    Private Sub Teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicaci�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try

    End Sub

    Private Sub txtClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")
            If usuario <> "" Then
                txtCedula.Text = cedula
                txtClave.Text = ""
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarRegistrar.Enabled = False
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBarButton1.Enabled = True
                Me.txtUsuario.Text = usuario
            Else
                Controles(False)
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub txtInsumo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtInsumo.KeyDown
        Dim detalle As Decimal
        If dtgInsumos.Enabled = False Then
            MsgBox("En este momento se encuentra realizando una modificaci�n a > " & Me.txtInsumo.Text & vbCrLf & "SE LE RECOMIENDA : Cancelar la operaci�n � terminar con la misma..", MsgBoxStyle.Information, "Atenci�n...")
            dtgInsumos.Enabled = True
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            txtPUnitario.Focus()
        End If

        If e.KeyCode = Keys.F1 Then
            Dim Fbuscador As New buscador
            Fbuscador.cantidad = 3
            Fbuscador.tabla = "Requisici�n"
            Fbuscador.TituloModulo.Text = "Buscando un articulo en el Inventario"
            Fbuscador.lblEncabezado.Text = " > Inventario <"
            Fbuscador.bd = "Proveeduria"
            Fbuscador.consulta = "Select i.codigo AS Codigo, i.descripcion as Descripcion, i.PrecioBase as Costo, (cast(i.PresentaCant as varchar)+ ' ' + p.Presentaciones) as Presentaci�n,i.IVenta as Impuesto,i.existencia from inventario as i WITH (NOLOCK) inner join Presentaciones as p WITH (NOLOCK) on i.CodPresentacion = p.codpres"
            Fbuscador.busca = "Descripci�n"
            Fbuscador.ShowDialog()
            txtInsumo.Text = Fbuscador.descripcion
            unidad = Fbuscador.presentacion
            iv = Fbuscador.iv
            Edicion = False

            If unidad <> "" Then
                Dim ii As Array = unidad.Split(" ")
                txtIdInsumo.Text = Fbuscador.Icodigo
                txtpresentacion.Text = Fbuscador.presentacion
                detalle = Fbuscador.detalle
                txtPUnitario.Text = Format(detalle, "###,##0.00")
                txtCantidad.Text = 0
                cbxBodega.SelectedIndex = 0
                buscador.Dispose()
                CargarConversiones(ii(1))
                If TextBox2.Text = "Infinito" Then
                    TextBox2.Text = "0.00"
                End If
                'Calcula el precio del art�culo seg�n su presentaci�n
                CalculoReceta()
                '-----------------------------------------------------
                ComboBox1.Focus()
            Else
                txtIdInsumo.Focus()
            End If
        Else : MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
        End If
    End Sub


    Private Sub CargarConversiones(ByVal unidad As String)
        Dim cnn As SqlConnection = Nothing
        Dim mensaje As String
        Dim cConexion As New ConexionR
        ' Dentro de un Try/Catch por si se produce un error
        Try
            DsRequisicion1.TablaConversiones.Clear()
            Dim sConn As String = GetSetting("Seesoft", "Restaurante", "Conexion")
            cnn = New SqlConnection(sConn)
            cConexion.Conectar("Restaurante")
            cnn.Open()
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String
            sel = "SELECT Conversiones.dbo.Unidades.NombreUnidad, Conversiones.dbo.Conversiones.ConvertirA," & _
            "Conversiones.dbo.Conversiones.Multiplo,Conversiones.dbo.Conversiones.Estado" & _
            " FROM Conversiones.dbo.Unidades INNER JOIN" & _
            " Conversiones.dbo.Conversiones ON Conversiones.dbo.Unidades.Id = Conversiones.dbo.Conversiones.Id_Unidad" & _
            " where Conversiones.dbo.Unidades.NombreUnidad = '" & unidad & "'"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(DsRequisicion1.TablaConversiones)
            If DsRequisicion1.TablaConversiones.Rows.Count = 0 Then
                MsgBox("No hay conversiones registradas para esa unidad de medida...", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            'MsgBox(ex.ToString)
            mensaje = ex.Message
        Finally
            If Not cnn Is Nothing Then
                cConexion.DesConectar(cnn)
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub txtnombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtnombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            nmPax.Focus()
        End If
    End Sub

    Private Sub nmPax_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles nmPax.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.dtFecha.Focus()
        End If
    End Sub

    Private Sub dtFecha_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dtFecha.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtInsumo.Focus()
        End If
    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            cbxBodega.Focus()
        End If
    End Sub

    Private Sub CalculoReceta()
        Dim multiplo As Double
        Dim cant As Array = (Me.txtpresentacion.Text).Split(" ")
        Dim cantidad As String
        Dim cc As Double
        Dim estado As String
        Try
            If (txtpresentacion.Text <> Nothing) Then
                cantidad = cant(0)
                multiplo = cConexion.SlqExecuteScalar(conectadobd, "Select Multiplo from TablaConversiones where NombreUnidad='" & CStr(cant(1)) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
                estado = cConexion.SlqExecuteScalar(conectadobd, "Select Estado from TablaConversiones where NombreUnidad='" & CStr(cant(1)) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
                cc = CDbl(cantidad) * multiplo
                If (cc = 0) Then cc = 1
                TextBox2.Text = CDbl(txtPUnitario.Text) / cc
                TextBox1.Text = ComboBox1.Text
                Label15.Text = estado
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        CalculoReceta()
    End Sub
    Private Sub txtCantidad_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.KeyDown
        Try
            If e.KeyCode = Keys.Tab Then
                txtClave.Focus()
            ElseIf e.KeyCode = Keys.Enter Then
                If nmPax.Value <> 0 Then
                    If txtInsumo.Text <> "" Then
                        If txtCantidad.Text <> "0" Then
                            If Me.TextBox2.Text = "0.00" Then
                                MsgBox("Revise el campo P/Unidad, puede estar incorrecto", MsgBoxStyle.Information)
                            Else
                                If dtgInsumos.Enabled = True Then
                                    agregaLinea()
                                Else
                                    'ModificarLinea()
                                End If
                            End If
                        Else
                            MessageBox.Show("Debe ingresar otro valor que no sea Cero", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        End If
                    Else
                        MessageBox.Show("Debe seleccionar un Insumo", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    End If
                Else
                    MessageBox.Show("El n�mero de porciones debe ser mayor que cero", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Verifique la cantidad", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub agregaLinea()
        Dim cant As Array = (Me.txtpresentacion.Text).Split(" ")
        Dim cantidad As String
        Dim cc As Double
        cantidad = cant(0)
        Dim multiplo As Double

        If CStr(cant(1)) = Me.ComboBox1.Text Then
            If cant(0) = CDbl(Me.txtCantidad.Text) Then
                reduce = 1
            Else
                reduce = CDbl(txtCantidad.Text) / cant(0)
            End If
        Else
            multiplo = cConexion.SlqExecuteScalar(conectadobd, "Select Multiplo from TablaConversiones where NombreUnidad='" & CStr(cant(1)) & "' and ConvertirA= '" & Me.ComboBox1.Text & "'")
            If Me.Label15.Text = ">" Then
                cc = CDbl(cantidad) * multiplo
                reduce = CDbl(Me.txtCantidad.Text) / cc
            ElseIf Me.Label15.Text = "<" Then
                cc = CDbl(Me.txtCantidad.Text) / multiplo
                reduce = cc / cantidad
            End If
        End If
        Dim piv, pivi As Double : Dim articulo As Integer
        If iv <> 0 Then
            piv = 0 'CDbl(Me.TextBox2.Text) * iv / 100
            pivi = CDbl(Me.TextBox2.Text) + piv
        Else
            pivi = CDbl(Me.TextBox2.Text)
        End If

        articulo = 1
        IDBODEGA_DESCARGA = cbxBodega.SelectedValue
        NombreBodega = cbxBodega.Text

        Dim row0 As String() = {txtIdInsumo.Text, txtCantidad.Text, Me.TextBox1.Text, txtInsumo.Text, (Format(Math.Round(CDbl(Me.TextBox2.Text), 3), "#####0.00")), (Format((Math.Round(CDbl(txtCantidad.Text), 3) * Math.Round(CDbl(pivi), 3)), "#####0.00")), Math.Round(CDbl(pivi), 3), reduce, IDBODEGA_DESCARGA, NombreBodega, articulo}
        If Edicion Then
            dtgInsumos.Rows(Indice).SetValues(row0)
            Totales()
        Else
            With Me.dtgInsumos.Rows
                .Add(row0)
                Totales()
            End With
        End If

        txtIdInsumo.Clear()
        txtpresentacion.Clear()
        txtInsumo.Clear()
        txtInsumo.Select()
        TextBox2.Text = "0.00"
        txtPUnitario.Text = "0.00"
        txtCantidad.Text = "0"
        CalcularEspeciesDesaprovechamiento()
        cbxBodega.SelectedIndex = 0
        cbxBodega.Enabled = True
    End Sub
    Private Sub CalcularEspeciesDesaprovechamiento()
        If txtPorEspecies.Text = "" Then
            txtPorEspecies.Text = 0
        End If
        If txtPorDesapro.Text = "" Then
            txtPorDesapro.Text = 0
        End If
        txtSubTotal.Text = Format(CDbl(txtSubTotal.Text), "#,#0.00")
        especies = CDbl(txtSubTotal.Text) * (CDbl(txtPorEspecies.Text) / 100)
        txtEspecie.Text = Format(especies, "#,#0.00")
        desaprovechamiento = CDbl(txtSubTotal.Text) * (CDbl(txtPorDesapro.Text) / 100)
        txtDesapro.Text = Format(desaprovechamiento, "#,#0.00")
        txtCTotal.Text = Format(CDbl(txtSubTotal.Text) + especies + desaprovechamiento, "#,#0.00")
    End Sub

    Private Sub Totales()
        Total = 0
        For Each row As DataGridViewRow In Me.dtgInsumos.Rows
            Total += Val(row.Cells(5).Value)
        Next
        Me.txtSubTotal.Text = Total
    End Sub

    Private Sub txtCantidad_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCantidad.KeyPress
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
            puntos = txtCantidad.Text.IndexOf(".") 'Busca para ver si el numero ya tiene un punto
            If ((e.KeyChar = ".") And (puntos <= 0)) Then
                puntos += 1
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtPorEspecies_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPorEspecies.KeyDown
        If e.KeyCode = Keys.Enter Then
            CalcularEspeciesDesaprovechamiento()
            txtPorDesapro.Focus()
        End If
    End Sub

    Private Sub txtPorDesapro_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPorDesapro.KeyDown
        If e.KeyCode = Keys.Enter Then
            CalcularEspeciesDesaprovechamiento()
        End If
    End Sub

    Private Sub cbxBodega_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxBodega.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtCantidad.Focus()
        End If
    End Sub

#End Region

#Region "Descarga Inventario"
    Private Sub DescargaInventario(ByVal Codigo As Integer, ByVal Disminuye As Double, ByVal Bodega As Integer, ByVal Anula As Boolean)
        Try
            'Obtiene dataset con los articulos que no requieren receta para luego ser rebajados del inventario general de proveeduria
            Dim conexion As New SqlConnection
            Dim Conexion1 As New SqlConnection
            '
            Dim existencia As Double = 0
            Dim existencia_inv As Double = 0
            Dim existencia_bod As Double = 0
            Dim existencia_inv_act As Double = 0
            Dim existencia_bod_act As Double = 0
            Dim arrayBodegas(200) As Integer

            'RELACIONADOS CON EL COSTO PROMEDIO Y EL SALDO FINAL
            Dim Costo_Movimiento As Double
            Dim saldo_final As Double
            Dim costo_promedio As Double
            Dim id_bodega As Integer
            Dim costo_real As Double
            conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
            Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If conexion.State = ConnectionState.Closed Then conexion.Open()
            'EXISTENCIA EN BODEGA
            existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & Codigo)
            existencia_inv = existencia
            If Anula = True Then
                existencia = existencia + Disminuye
            Else
                existencia = existencia - Disminuye
            End If
            existencia_inv_act = existencia
            'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
            cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & Codigo)
            'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
            id_bodega = Bodega
            existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & Codigo & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & id_bodega)
            saldo_final = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & Codigo & "and idBodega = " & id_bodega)
            costo_promedio = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & Codigo & "and idBodega = " & id_bodega)
            'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
            existencia_bod = existencia
            If Anula = False Then
                existencia = existencia - Disminuye
                existencia = Math.Round(existencia, 2)
                Costo_Movimiento = Disminuye * costo_promedio
                Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
                saldo_final = saldo_final - Costo_Movimiento
            Else
                existencia = existencia + Disminuye
                existencia = Math.Round(existencia, 2)
                Costo_Movimiento = Disminuye * costo_promedio
                Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
                saldo_final = saldo_final + Costo_Movimiento
            End If
            existencia_bod_act = existencia
            'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
            cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & Codigo & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & id_bodega)
            'INGRESA AL KARDEX
            If Anula = True Then
                actualiza_kardex(conexion, Codigo, Disminuye, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, id_bodega, costo_promedio, saldo_final, True)
            Else
                actualiza_kardex(conexion, Codigo, Disminuye, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, id_bodega, costo_promedio, saldo_final, False)
            End If

        Catch ex As Exception
            MessageBox.Show("Error al actualizar el inventario: " & ex.Message, "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub actualiza_kardex(ByRef conexion As SqlClient.SqlConnection, ByVal art As Integer, ByVal cantidad As Decimal, ByVal existencia_inv As Decimal, ByVal existencia_bod As Decimal, ByVal existencia_inv_act As Decimal, ByVal existencia_bod_act As Decimal, ByVal Codigo_Bodega As Integer, ByVal CostoPromedio As Double, ByVal SaldoFinal As Double, ByVal Anular As Boolean)
        Try
            Dim codigo_prov, cod_moneda As Integer
            Dim costo_unit As Double
            Dim data As New DataSet
            'BUSCA CODIGO DEL PROVEEDOR, COSTO DEL ARTICULO Y CODIGO DE MONEDA
            cConexion.GetDataSet(conexion, "Select * from [Articulos x Proveedor] where CodigoArticulo=" & art, data, "Articulos")
            Dim fil As DataRow
            For Each fil In data.Tables("Articulos").Rows
                codigo_prov = fil("CodigoProveedor")
                cod_moneda = fil("Moneda")
                costo_unit = fil("UltimoCosto")
            Next
            Dim tipo As String
            If Anular = True Then
                tipo = "ABU"
            Else
                tipo = "BU" & Mid(User_Log.NombrePunto, 1, 1)
            End If

            'ACTUALIZAR MOVIMIENTOS EN EL KARDEX
            Dim campos As String = "Codigo, Documento, Tipo, Fecha, Exist_Ant, Cantidad, Exist_Act, Costo_Unit, Costo_Mov, Cod_Moneda, IdBodegaOrigen, IdBodegaDestino, Exist_AntBod, Exist_ActBod, Cod_Proveedor, Observaciones, Costo_Promedio, Saldo_Final"
            If idReceta = "" Then
                idReceta = txtIdReceta.Text
            End If
            Dim datos As String = art & "," & idReceta & ",'" & tipo & "', getdate()," & existencia_inv & "," & cantidad & "," & (existencia_inv_act) & "," & CostoPromedio & "," & (cantidad * CostoPromedio) & "," & cod_moneda & "," & Codigo_Bodega & "," & Codigo_Bodega & "," & existencia_bod & "," & (existencia_bod_act) & "," & codigo_prov & ",'Buffet " & User_Log.NombrePunto & " # " & idReceta & "' ," & CostoPromedio & "," & SaldoFinal
            cConexion.AddNewRecord(conexion, "Kardex", campos, datos)
        Catch ex As Exception
            MessageBox.Show("Error al actualizar el kardex: " & ex.Message, "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Guardar_Asiento()
    End Sub

   
    Dim Indice As Integer = -1
    Dim Edicion As Boolean
    Private Sub dtgInsumos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgInsumos.CellClick
        Dim Obj As DataRow
        Obj = dtgInsumos.CurrentRow.DataBoundItem
        Dim Cx As New Conexion
        Dim row() As DataRow
        Dim asd As String

        If Not IsNothing(dtgInsumos.Rows(e.RowIndex).Cells(0).Value) Then
            Indice = e.RowIndex
            txtIdInsumo.Text = dtgInsumos.Rows(e.RowIndex).Cells(0).Value
            txtCantidad.Text = dtgInsumos.Rows(e.RowIndex).Cells(1).Value
            txtInsumo.Text = dtgInsumos.Rows(e.RowIndex).Cells(3).Value
            txtPUnitario.Text = dtgInsumos.Rows(e.RowIndex).Cells(4).Value
            cbxBodega.SelectedValue = dtgInsumos.Rows(e.RowIndex).Cells(8).Value
            CargarConversiones(dtgInsumos.Rows(e.RowIndex).Cells(2).Value)
            row = DsRequisicion1.TablaConversiones.Select("ConvertirA = '" & dtgInsumos.Rows(e.RowIndex).Cells(2).Value & "'")
            ComboBox1.SelectedValue = row(0)(3)
            txtpresentacion.Text = Cx.SlqExecuteScalar(Cx.Conectar("Proveeduria"), "select (cast(i.PresentaCant as varchar)+ ' ' + p.Presentaciones) as Presentacion from inventario as i inner join Presentaciones as p on i.CodPresentacion = p.codpres where i.Codigo = '" & dtgInsumos.Rows(e.RowIndex).Cells(0).Value & "'")

            CalculoReceta()
            Edicion = True
        End If

    End Sub

   

    Private Sub dtgInsumos_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles dtgInsumos.RowsRemoved
        Totales()
        CalcularEspeciesDesaprovechamiento()
        txtIdInsumo.Clear()
        txtpresentacion.Clear()
        txtInsumo.Clear()
        TextBox2.Text = "0.00"
        txtPUnitario.Text = "0.00"
        txtCantidad.Text = "0"
        CalcularEspeciesDesaprovechamiento()
        If Not IsNothing(cbxBodega.SelectedValue) Then cbxBodega.SelectedIndex = 0
        cbxBodega.Enabled = True
    End Sub
End Class

