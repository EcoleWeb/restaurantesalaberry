Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Public Class Relacion_Saloneros

#Region "Variables"
    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlConnection
    Dim rs As SqlDataReader
    Dim DataS, DataS2, DataS3 As New DataSet
    Dim DataV, DataV2, DataV3 As DataView
    Dim cedulaU, nombreU, cedulaS, nombreS As String
    Dim PMU As New PerfilModulo_Class
    Dim cedula As String
#End Region

    Private Sub Relacion_Saloneros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        'Carga los usuarios
        cargar()
        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
      
        If gloNoClave Then
            Loggin_Usuario()
        Else
            txtClave.Focus()
        End If
      
        '---------------------------------------------------------------
    End Sub

    Private Sub cargar()
        '        cConexion.GetDataSet(conectadobd, "SELECT Usuarios.Cedula, Usuarios.Nombre from Usuarios INNER JOIN Saloneros ON Usuarios.Cedula = Saloneros.Cedula", DataS, "Usuarios")
        cConexion.GetDataSet(conectadobd, "SELECT Usuarios.Cedula, Usuarios.Nombre from Usuarios", DataS, "Usuarios")
        DataV = New DataView(DataS.Tables("Usuarios"))
        Me.GridUsuarios.DataSource = DataV
        Me.GridUsuarios.Columns(0).Visible = False
        Me.GridUsuarios.Columns(1).Width = 250
        Me.GridUsuarios.Visible = True
        'Carga los Saloneros
        cConexion.GetDataSet(conectadobd, "SELECT Cedula, Nombre from Saloneros", DataS2, "Saloneros")
        DataV2 = New DataView(DataS2.Tables("Saloneros"))
        Me.GridSaloneros.DataSource = DataV2
        Me.GridSaloneros.Columns(0).Visible = False
        Me.GridSaloneros.Columns(1).Width = 250
        Me.GridSaloneros.Visible = True
        actualiza()
    End Sub

    Private Sub actualiza()
        DataS3.Clear()
        cConexion.GetDataSet(conectadobd, "Select CedulaU, NombreU AS Nombre_Usuario, CedulaS, NombreS AS Nombre_Salonero From RelacionTrabajo", DataS3, "RelacionTrabajo")
        DataV3 = New DataView(DataS3.Tables("RelacionTrabajo"))
        Me.GridRelacion.DataSource = DataV3
        Me.GridRelacion.Columns(0).Visible = False
        Me.GridRelacion.Columns(2).Visible = False
        Me.GridRelacion.Columns(1).Width = 150
        Me.GridRelacion.Columns(3).Width = 150
        Me.GridRelacion.Visible = True
    End Sub
    Private Sub Relacion_Saloneros_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Dim existe = False
        Try
            cedulaU = GridUsuarios(0, GridUsuarios.CurrentRow.Index).Value
            nombreU = GridUsuarios(1, GridUsuarios.CurrentRow.Index).Value
            cedulaS = GridSaloneros(0, GridSaloneros.CurrentRow.Index).Value
            nombreS = GridSaloneros(1, GridSaloneros.CurrentRow.Index).Value
            cConexion.GetRecorset(conectadobd, "Select * from Usuario_Salonero where Cedula_Usuario='" & cedulaU & "' AND Cedula_Salonero='" & cedulaS & "'", rs)
            While rs.Read
                existe = True
            End While
            rs.Close()
            If existe = False Then
                cConexion.AddNewRecord(conectadobd, "Usuario_Salonero", "Cedula_Usuario, Cedula_Salonero", "'" & cedulaU & "','" & cedulaS & "'")
                actualiza()
            Else
                MessageBox.Show("Esta relaci�n de trabajo ya existe", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            End If
        Catch ex As Exception
            MessageBox.Show("Debe seleccionar un Usuario y un Salonero para crear la relaci�n de Trabajo", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Exit Sub
        End Try
    End Sub

    Private Sub btnQuitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitar.Click
        Try
            cedulaU = GridRelacion(0, GridRelacion.CurrentRow.Index).Value
            nombreU = GridRelacion(1, GridRelacion.CurrentRow.Index).Value
            cedulaS = GridRelacion(2, GridRelacion.CurrentRow.Index).Value
            nombreS = GridRelacion(3, GridRelacion.CurrentRow.Index).Value
            Dim mens As String = cConexion.DeleteRecords(conectadobd, "Usuario_Salonero", "Cedula_Usuario='" & cedulaU & "' AND Cedula_Salonero='" & cedulaS & "'")
            If mens = "" Then
                With Me.GridRelacion.Rows
                    .RemoveAt(Me.GridRelacion.CurrentRow.Index)
                End With
            Else
                MessageBox.Show("Error al eliminar el registro", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
            End If
        Catch ex As Exception
            MessageBox.Show("Debe seleccionar una Relaci�n de Trabajo", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Exit Sub
        End Try
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 4 : MessageBox.Show("Las relaciones de Trabajo fueron creadas correctamente", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Close()
            Case 6 : If PMU.Print Then imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : teclado()
            Case 8 : Close()
        End Select
    End Sub

    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicaci�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Public Sub imprimir()
        Try
            Dim IRelacion As New RelacionTrabajo
            Dim visor As New frmVisorReportes
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, IRelacion, False, Me.conectadobd.ConnectionString)
            Me.Hide()
            visor.ShowDialog()
            visor.Dispose()
            Me.Show()
        Catch ex As Exception
            MessageBox.Show("Error al cargar el reporte", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub txtUsuario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyUp
        If Trim(txtUsuario.Text).Length >= 0 Then
            DataV.RowFilter = "Nombre Like '%" & txtUsuario.Text & "%'"
            DataV3.RowFilter = "Nombre_Usuario Like '%" & txtUsuario.Text & "%'"
        ElseIf Trim(txtUsuario.Text).Length = 0 Then
            DataV = New DataView(DataS.Tables("Usuario"))
            DataV3 = New DataView(DataS3.Tables("RelacionTrabajo"))
        End If
        GridUsuarios.DataSource = DataV
        GridRelacion.DataSource = DataV3
    End Sub

    Private Sub txtSalonero_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSalonero.KeyUp
        If Trim(txtSalonero.Text).Length >= 0 Then
            DataV2.RowFilter = "Nombre Like '%" & txtSalonero.Text & "%'"
            DataV3.RowFilter = "Nombre_Salonero Like '%" & txtSalonero.Text & "%'"
        ElseIf Trim(txtSalonero.Text).Length = 0 Then
            DataV2 = New DataView(DataS2.Tables("Salonero"))
            DataV3 = New DataView(DataS3.Tables("RelacionTrabajo"))
        End If
        GridSaloneros.DataSource = DataV2
        GridRelacion.DataSource = DataV3
    End Sub

#Region "Validacion Usuario"
    Private Sub txtClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")
            If usuario <> "" Then
                txtClave.Text = ""
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBarTeclado.Enabled = True
                Me.lbUsuario.Text = usuario
                PMU = VSM(cedula, Me.Name)
                If PMU.Update Then Me.btnAgregar.Enabled = True Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                If PMU.Delete Then Me.btnQuitar.Enabled = True Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                txtUsuario.Focus()
            Else
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                txtClave.Text = ""
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBarTeclado.Enabled = True
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
                PMU = VSM(cedula, Me.Name)
                If PMU.Update Then Me.btnAgregar.Enabled = True Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                If PMU.Delete Then Me.btnQuitar.Enabled = True Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                txtUsuario.Focus()
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region
End Class