<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Saloneros
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Saloneros))
        Me.txtCedula = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtNombre = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtTelefono = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtDireccion = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.rbMasculino = New System.Windows.Forms.RadioButton
        Me.rbFemenino = New System.Windows.Forms.RadioButton
        Me.Label6 = New System.Windows.Forms.Label
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar2 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRelacion = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarTeclado = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.txtTurno = New System.Windows.Forms.TextBox
        Me.lbUsuario = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtClave = New System.Windows.Forms.TextBox
        Me.TituloModulo = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtCedula
        '
        Me.txtCedula.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtCedula, "txtCedula")
        Me.txtCedula.Name = "txtCedula"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'txtNombre
        '
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtNombre, "txtNombre")
        Me.txtNombre.Name = "txtNombre"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'txtTelefono
        '
        Me.txtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtTelefono, "txtTelefono")
        Me.txtTelefono.Name = "txtTelefono"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtDireccion, "txtDireccion")
        Me.txtDireccion.Name = "txtDireccion"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'rbMasculino
        '
        resources.ApplyResources(Me.rbMasculino, "rbMasculino")
        Me.rbMasculino.Name = "rbMasculino"
        Me.rbMasculino.TabStop = True
        Me.rbMasculino.UseVisualStyleBackColor = True
        '
        'rbFemenino
        '
        resources.ApplyResources(Me.rbFemenino, "rbFemenino")
        Me.rbFemenino.Name = "rbFemenino"
        Me.rbFemenino.TabStop = True
        Me.rbFemenino.UseVisualStyleBackColor = True
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "TL_proj.gif")
        Me.ImageList.Images.SetKeyName(8, "")
        Me.ImageList.Images.SetKeyName(9, "")
        Me.ImageList.Images.SetKeyName(10, "teclado1.gif")
        Me.ImageList.Images.SetKeyName(11, "")
        '
        'ToolBar2
        '
        resources.ApplyResources(Me.ToolBar2, "ToolBar2")
        Me.ToolBar2.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarEditar, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarRelacion, Me.ToolBarImprimir, Me.ToolBarTeclado, Me.ToolBarCerrar})
        Me.ToolBar2.ImageList = Me.ImageList
        Me.ToolBar2.Name = "ToolBar2"
        '
        'ToolBarNuevo
        '
        resources.ApplyResources(Me.ToolBarNuevo, "ToolBarNuevo")
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        '
        'ToolBarEditar
        '
        resources.ApplyResources(Me.ToolBarEditar, "ToolBarEditar")
        Me.ToolBarEditar.Name = "ToolBarEditar"
        '
        'ToolBarBuscar
        '
        resources.ApplyResources(Me.ToolBarBuscar, "ToolBarBuscar")
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        '
        'ToolBarRelacion
        '
        resources.ApplyResources(Me.ToolBarRelacion, "ToolBarRelacion")
        Me.ToolBarRelacion.Name = "ToolBarRelacion"
        '
        'ToolBarImprimir
        '
        resources.ApplyResources(Me.ToolBarImprimir, "ToolBarImprimir")
        Me.ToolBarImprimir.Name = "ToolBarImprimir"
        '
        'ToolBarTeclado
        '
        resources.ApplyResources(Me.ToolBarTeclado, "ToolBarTeclado")
        Me.ToolBarTeclado.Name = "ToolBarTeclado"
        '
        'ToolBarCerrar
        '
        resources.ApplyResources(Me.ToolBarCerrar, "ToolBarCerrar")
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        '
        'txtTurno
        '
        Me.txtTurno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtTurno, "txtTurno")
        Me.txtTurno.Name = "txtTurno"
        '
        'lbUsuario
        '
        Me.lbUsuario.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.lbUsuario, "lbUsuario")
        Me.lbUsuario.ForeColor = System.Drawing.Color.White
        Me.lbUsuario.Name = "lbUsuario"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'txtClave
        '
        Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtClave, "txtClave")
        Me.txtClave.Name = "txtClave"
        '
        'TituloModulo
        '
        Me.TituloModulo.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.TituloModulo, "TituloModulo")
        Me.TituloModulo.ForeColor = System.Drawing.Color.White
        Me.TituloModulo.Name = "TituloModulo"
        '
        'Saloneros
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtClave)
        Me.Controls.Add(Me.txtTurno)
        Me.Controls.Add(Me.ToolBar2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.rbFemenino)
        Me.Controls.Add(Me.rbMasculino)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtTelefono)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtCedula)
        Me.Controls.Add(Me.TituloModulo)
        Me.Name = "Saloneros"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Protected Friend WithEvents TituloModulo As System.Windows.Forms.Label
    Friend WithEvents txtCedula As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents rbMasculino As System.Windows.Forms.RadioButton
    Friend WithEvents rbFemenino As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Public WithEvents ImageList As System.Windows.Forms.ImageList
    Public WithEvents ToolBar2 As System.Windows.Forms.ToolBar
    Protected Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarTeclado As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents txtTurno As System.Windows.Forms.TextBox
    Friend WithEvents ToolBarRelacion As System.Windows.Forms.ToolBarButton
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
End Class
