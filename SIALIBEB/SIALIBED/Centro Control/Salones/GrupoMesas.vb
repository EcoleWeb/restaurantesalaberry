Imports System.IO
Imports System.Drawing.Printing

Public Class GrupoMesas

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim PMU As New PerfilModulo_Class
    Dim cedula As String

#End Region

#Region "Cerrar"
    Private Sub GrupoMesas_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub
#End Region

#Region "Load"
    Private Sub GrupoMesas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        lbimagen.Text = ""
        CargarGrupos()
        ToolBar2.Buttons(2).Enabled = False
        ToolBar2.Buttons(3).Enabled = False
        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
       
        If gloNoClave Then
            Loggin_Usuario()
        End If
       
        '---------------------------------------------------------------
    End Sub

    Private Sub CargarGrupos()
        lstVGrupos.View = View.Details
        lstVGrupos.FullRowSelect = True
        lstVGrupos.LabelEdit = False
        lstVGrupos.Items.Clear()

        cConexion.GetRecorset(conectadobd, "Select id,nombre_grupo,numeros_mesas, ImagenRuta, ParaLlevar from grupos_mesas", rs)
        While rs.Read
            With lstVGrupos.Items.Add(rs("id"))
                .SubItems.Add(rs("nombre_grupo"))
                .SubItems.Add(rs("numeros_mesas"))
                .subitems.add(rs("ImagenRuta"))
                .subitems.add(rs("ParaLlevar"))
            End With
        End While
        rs.Close()
    End Sub
#End Region

#Region "ToolBar"
    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 1 : If PMU.Update Then Me.agregar() Else MsgBox("No tiene permiso para agregar información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 2 : If PMU.Update Then Me.editar() Else MsgBox("No tiene permiso para actualizar información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 5 : If PMU.Delete Then Me.borrar() Else MsgBox("No tiene permiso para buscar información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 6 : If PMU.Print Then imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 7 : teclado()
            Case 8 : Close()
        End Select
    End Sub
#End Region

#Region "Teclado"
    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicación", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
#End Region

#Region "Agregar"
    'crear un salon
    Private Sub agregar()
        Dim prom As New Prompt
        'Dim w As Integer
w:
        prom.Text = "Nuevo Grupo Mesas"
        prom.Label2.Text = "Salon"
        prom.txtDato.Focus()
        prom.GroupBox1.Visible = False
        prom.Label1.Visible = True
        prom.txtMEsas.Visible = True
        prom.btnBuscar.Visible = True
        prom.ckParaLlevar.Visible = True
        prom.ShowDialog()
        If prom.tipo = 0 Then 'psv
        Else
            If Trim(prom.txtDato.Text) <> vbNullString And IsNumeric(prom.txtMEsas.Text) And prom.tipo = 1 Then
                Dim ParaLlevar As Integer = 0
                Dim dt As New DataTable
                If prom.ckParaLlevar.Checked Then
                    ParaLlevar = 1
                    cFunciones.Llenar_Tabla_Generico("Select Id from Grupos_Mesas where ParaLlevar=1", dt)
                    If dt.Rows.Count > 0 Then
                        MsgBox("Ya existe una mesa configurada para llevar. No se puede configurar mas de una mesa.", MsgBoxStyle.Information)
                        Exit Sub
                    Else
                        cConexion.AddNewRecord(conectadobd, "Grupos_Mesas", " nombre_grupo,numeros_Mesas, ImagenRuta,ParaLlevar", "'" & prom.txtDato.Text & "'," & prom.txtMEsas.Text & ",'" & prom.ruta & "'," & ParaLlevar & "")

                    End If
                Else
                    cConexion.AddNewRecord(conectadobd, "Grupos_Mesas", " nombre_grupo,numeros_Mesas, ImagenRuta,ParaLlevar", "'" & prom.txtDato.Text & "'," & prom.txtMEsas.Text & ",'" & prom.ruta & "'," & ParaLlevar & "")

                End If
                CargarGrupos()
            Else
                MessageBox.Show("Debe llenar todos los campos", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                prom.ShowDialog()
                GoTo w
            End If
        End If
    End Sub
#End Region

#Region "Editar"
    'editar un salon
    Private Sub editar()
        If lstVGrupos.SelectedItems.Count <= 0 Then
            Exit Sub
        End If
        'Dim w As Integer
        Dim prom As New Prompt
        Dim theItem As New ListViewItem
        theItem = lstVGrupos.SelectedItems(0)
w:
        prom.txtDato.Text = theItem.SubItems(1).Text
        prom.Label1.Visible = True
        prom.Label2.Text = "Salón:"
        prom.txtMEsas.Visible = True
        prom.GroupBox1.Visible = False
        prom.btnBuscar.Visible = True
        prom.ckParaLlevar.Visible = True
        prom.txtMEsas.Text = theItem.SubItems(2).Text
        prom.ckParaLlevar.Checked = theItem.SubItems(4).Text
        prom.ruta = theItem.SubItems(3).Text
        If theItem.SubItems(3).Text <> "" Then
            Try
                Dim SourceImage As Bitmap
                SourceImage = New Bitmap(theItem.SubItems(3).Text)
                prom.btnBuscar.Image = SourceImage
            Catch ex As Exception
            End Try
        End If

        prom.ShowDialog()
        If prom.ruta = "" Then
            prom.ruta = theItem.SubItems(3).Text
        End If
        If prom.tipo = 0 Then
        Else
            If Trim(prom.txtDato.Text) <> vbNullString And IsNumeric(prom.txtMEsas.Text) And prom.tipo = 1 Then
                If prom.ckParaLlevar.Checked Then
                    Dim dt As New DataTable
                    cFunciones.Llenar_Tabla_Generico("Select Id from Grupos_Mesas where ParaLlevar=1", dt)
                    If dt.Rows.Count > 0 Then
                        MsgBox("Ya existe una mesa configurada para llevar. No se puede configurar mas de una mesa.", MsgBoxStyle.Information)
                        cConexion.UpdateRecords(conectadobd, "Grupos_Mesas", "ParaLlevar='False', nombre_grupo='" & prom.txtDato.Text & "',numeros_Mesas=" & prom.txtMEsas.Text & ",ImagenRuta='" & prom.ruta & "'", " id=" & theItem.SubItems(0).Text)
                        CargarGrupos()
                        Exit Sub
                    Else
                        cConexion.UpdateRecords(conectadobd, "Grupos_Mesas", "ParaLlevar='True', nombre_grupo='" & prom.txtDato.Text & "',numeros_Mesas=" & prom.txtMEsas.Text & ",ImagenRuta='" & prom.ruta & "'", " id=" & theItem.SubItems(0).Text)
                        CargarGrupos()
                    End If
                Else
                    cConexion.UpdateRecords(conectadobd, "Grupos_Mesas", "ParaLlevar='False', nombre_grupo='" & prom.txtDato.Text & "',numeros_Mesas=" & prom.txtMEsas.Text & ",ImagenRuta='" & prom.ruta & "'", " id=" & theItem.SubItems(0).Text)
                    CargarGrupos()
                End If
            Else
                MessageBox.Show("Debe llenar todos los campos", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                prom.ShowDialog()
                GoTo w
            End If
        End If
        theItem = Nothing
        prom = Nothing
    End Sub
#End Region

#Region "Borrar"
    'Borrar un salon
    Private Sub borrar()
        If lstVGrupos.SelectedItems.Count <= 0 Then
            Exit Sub
        End If
        Dim theItem As New ListViewItem
        theItem = lstVGrupos.SelectedItems(0)
        If MessageBox.Show("Desea Eliminar este grupo de mesas", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            cConexion.DeleteRecords(conectadobd, "Grupos_Mesas", " id=" & theItem.SubItems(0).Text)
            CargarGrupos()
        End If
        theItem = Nothing
    End Sub
#End Region

#Region "Imprmir"
    Private Sub imprimir()
        Try
            Dim Imprime As New rptSalones
            Dim visor As New frmVisorReportes
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, Imprime, False, Me.conectadobd.ConnectionString)
            Me.Hide()
            visor.ShowDialog()
            visor.Dispose()
            Me.Show()
        Catch ex As Exception
            MessageBox.Show("Error al cargar el reporte", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
#End Region

#Region "Validar Usuario"
    Private Sub txtClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")
                Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

                If usuario <> "" Then
                    txtClave.Text = ""
                    Me.ToolBarNuevo.Enabled = True
                    Me.ToolBarEditar.Enabled = True
                    Me.ToolBarImprimir.Enabled = True
                    Me.ToolBar2.Buttons(2).Enabled = True
                    Me.ToolBarEliminar.Enabled = True
                    Me.ToolBarCerrar.Enabled = True
                    lbUsuario.Text = usuario
                Else
                    Me.Enabled = True
                End If
            Catch ex As Exception
                MessageBox.Show("Error al validar los derechos del usuario: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            End Try
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarEditar.Enabled = True
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBar2.Buttons(2).Enabled = True
                Me.ToolBarEliminar.Enabled = True
                Me.ToolBarCerrar.Enabled = True
                cedula = User_Log.Cedula
                lbUsuario.Text = User_Log.Nombre
                txtClave.Text = ""
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

End Class