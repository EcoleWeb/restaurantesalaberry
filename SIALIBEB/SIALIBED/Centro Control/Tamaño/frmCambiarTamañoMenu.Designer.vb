﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCambiarTamañoMenu
	Inherits System.Windows.Forms.Form

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.txtAncho = New System.Windows.Forms.TextBox()
		Me.txtLargo = New System.Windows.Forms.TextBox()
		Me.btnGuardar = New System.Windows.Forms.Button()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.txtLargoMesas = New System.Windows.Forms.TextBox()
		Me.txtAnchoMesas = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.btnGuardarMesas = New System.Windows.Forms.Button()
		Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
		Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.PonerValoresPorDefectoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.MesasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.TodosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GroupBox1.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.MenuStrip1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(15, 84)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(49, 16)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Ancho:"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.Location = New System.Drawing.Point(16, 23)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(46, 16)
		Me.Label2.TabIndex = 1
		Me.Label2.Text = "Largo:"
		'
		'txtAncho
		'
		Me.txtAncho.Location = New System.Drawing.Point(70, 84)
		Me.txtAncho.Name = "txtAncho"
		Me.txtAncho.Size = New System.Drawing.Size(100, 20)
		Me.txtAncho.TabIndex = 2
		'
		'txtLargo
		'
		Me.txtLargo.Location = New System.Drawing.Point(71, 19)
		Me.txtLargo.Name = "txtLargo"
		Me.txtLargo.Size = New System.Drawing.Size(100, 20)
		Me.txtLargo.TabIndex = 3
		'
		'btnGuardar
		'
		Me.btnGuardar.Location = New System.Drawing.Point(158, 126)
		Me.btnGuardar.Name = "btnGuardar"
		Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
		Me.btnGuardar.TabIndex = 4
		Me.btnGuardar.Text = "Guardar"
		Me.btnGuardar.UseVisualStyleBackColor = True
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.btnGuardarMesas)
		Me.GroupBox1.Controls.Add(Me.txtLargoMesas)
		Me.GroupBox1.Controls.Add(Me.txtAnchoMesas)
		Me.GroupBox1.Controls.Add(Me.Label3)
		Me.GroupBox1.Controls.Add(Me.Label4)
		Me.GroupBox1.Location = New System.Drawing.Point(19, 239)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(265, 159)
		Me.GroupBox1.TabIndex = 5
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Mesas"
		'
		'txtLargoMesas
		'
		Me.txtLargoMesas.Location = New System.Drawing.Point(110, 19)
		Me.txtLargoMesas.Name = "txtLargoMesas"
		Me.txtLargoMesas.Size = New System.Drawing.Size(100, 20)
		Me.txtLargoMesas.TabIndex = 7
		'
		'txtAnchoMesas
		'
		Me.txtAnchoMesas.Location = New System.Drawing.Point(109, 84)
		Me.txtAnchoMesas.Name = "txtAnchoMesas"
		Me.txtAnchoMesas.Size = New System.Drawing.Size(100, 20)
		Me.txtAnchoMesas.TabIndex = 6
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(55, 23)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(46, 16)
		Me.Label3.TabIndex = 5
		Me.Label3.Text = "Largo:"
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.Location = New System.Drawing.Point(54, 84)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(49, 16)
		Me.Label4.TabIndex = 4
		Me.Label4.Text = "Ancho:"
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.txtLargo)
		Me.GroupBox2.Controls.Add(Me.Label1)
		Me.GroupBox2.Controls.Add(Me.btnGuardar)
		Me.GroupBox2.Controls.Add(Me.Label2)
		Me.GroupBox2.Controls.Add(Me.txtAncho)
		Me.GroupBox2.Location = New System.Drawing.Point(27, 41)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(256, 174)
		Me.GroupBox2.TabIndex = 6
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "Menú"
		'
		'btnGuardarMesas
		'
		Me.btnGuardarMesas.Location = New System.Drawing.Point(167, 120)
		Me.btnGuardarMesas.Name = "btnGuardarMesas"
		Me.btnGuardarMesas.Size = New System.Drawing.Size(75, 23)
		Me.btnGuardarMesas.TabIndex = 8
		Me.btnGuardarMesas.Text = "Guardar"
		Me.btnGuardarMesas.UseVisualStyleBackColor = True
		'
		'MenuStrip1
		'
		Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem})
		Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
		Me.MenuStrip1.Name = "MenuStrip1"
		Me.MenuStrip1.Size = New System.Drawing.Size(317, 24)
		Me.MenuStrip1.TabIndex = 7
		Me.MenuStrip1.Text = "MenuStrip1"
		'
		'MenuToolStripMenuItem
		'
		Me.MenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PonerValoresPorDefectoToolStripMenuItem, Me.MesasToolStripMenuItem, Me.TodosToolStripMenuItem})
		Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
		Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(154, 20)
		Me.MenuToolStripMenuItem.Text = "Poner valores por defecto"
		'
		'PonerValoresPorDefectoToolStripMenuItem
		'
		Me.PonerValoresPorDefectoToolStripMenuItem.Name = "PonerValoresPorDefectoToolStripMenuItem"
		Me.PonerValoresPorDefectoToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
		Me.PonerValoresPorDefectoToolStripMenuItem.Text = "Menú"
		'
		'MesasToolStripMenuItem
		'
		Me.MesasToolStripMenuItem.Name = "MesasToolStripMenuItem"
		Me.MesasToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
		Me.MesasToolStripMenuItem.Text = "Mesas"
		'
		'TodosToolStripMenuItem
		'
		Me.TodosToolStripMenuItem.Name = "TodosToolStripMenuItem"
		Me.TodosToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
		Me.TodosToolStripMenuItem.Text = "Todos"
		'
		'frmCambiarTamañoMenu
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(317, 433)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.MenuStrip1)
		Me.MainMenuStrip = Me.MenuStrip1
		Me.Name = "frmCambiarTamañoMenu"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Cambiar Tamaño Menu"
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.GroupBox2.ResumeLayout(False)
		Me.GroupBox2.PerformLayout()
		Me.MenuStrip1.ResumeLayout(False)
		Me.MenuStrip1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents Label1 As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents txtAncho As TextBox
	Friend WithEvents txtLargo As TextBox
	Friend WithEvents btnGuardar As Button
	Friend WithEvents GroupBox1 As GroupBox
	Friend WithEvents btnGuardarMesas As Button
	Friend WithEvents txtLargoMesas As TextBox
	Friend WithEvents txtAnchoMesas As TextBox
	Friend WithEvents Label3 As Label
	Friend WithEvents Label4 As Label
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents MenuStrip1 As MenuStrip
	Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents PonerValoresPorDefectoToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents MesasToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents TodosToolStripMenuItem As ToolStripMenuItem
End Class
