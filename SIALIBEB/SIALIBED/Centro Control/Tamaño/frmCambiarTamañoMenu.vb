﻿Public Class frmCambiarTamañoMenu
	Dim ancho As New Int16
	Dim largo As New Int16
	Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

		If validar() Then
			SaveSetting("SeeSoft", "Restaurante", "anchoComanda", txtAncho.Text)
			SaveSetting("SeeSoft", "Restaurante", "largoComanda", txtLargo.Text)
			SaveSetting("SeeSoft", "Restaurante", "ModificaTamano", "1")
			MsgBox("Datos correctamente guardados")

		Else
			MsgBox("Por favor revisar los datos")
		End If
	End Sub

	Function validar() As Boolean
		If txtAncho.Text = "" Or txtLargo.Text = "" Then
			Return False

		End If
		If Not IsNumeric(txtAncho.Text) Or Not IsNumeric(txtLargo.Text) Then
			Return False

		End If
		Return True
	End Function

	Function validarCamposMesas() As Boolean
		If txtAnchoMesas.Text = "" Or txtLargoMesas.Text = "" Then
			Return False

		End If
		If Not IsNumeric(txtAnchoMesas.Text) Or Not IsNumeric(txtLargoMesas.Text) Then
			Return False

		End If
		Return True
	End Function

	Sub limpiarCampos()
		txtAncho.Text = ""
		txtLargo.Text = ""
		txtAnchoMesas.Text = ""
		txtLargoMesas.Text = ""
	End Sub

	Private Sub frmCambiarTamañoMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Try
			txtLargo.Text = GetSetting("SeeSoft", "Restaurante", "largoComanda")
			txtAncho.Text = GetSetting("SeeSoft", "Restaurante", "anchoComanda")
			txtLargoMesas.Text = GetSetting("SeeSoft", "Restaurante", "largoMesas")
			txtAnchoMesas.Text = GetSetting("SeeSoft", "Restaurante", "anchoMesas")
		Catch ex As Exception
			MsgBox("Ha ocurrido un problema")
		End Try

	End Sub

	Private Sub btnGuardarMesas_Click(sender As Object, e As EventArgs) Handles btnGuardarMesas.Click
		If validarCamposMesas() Then
			SaveSetting("SeeSoft", "Restaurante", "anchoMesas", txtAnchoMesas.Text)
			SaveSetting("SeeSoft", "Restaurante", "largoMesas", txtLargoMesas.Text)
			SaveSetting("SeeSoft", "Restaurante", "ModificaTamano", "1")
			MsgBox("Datos correctamente guardados")

		Else
			MsgBox("Por favor revisar los datos")

		End If

	End Sub

	Private Sub PonerValoresPorDefectoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PonerValoresPorDefectoToolStripMenuItem.Click
		Try
			SaveSetting("SeeSoft", "Restaurante", "anchoComanda", "80")
			SaveSetting("SeeSoft", "Restaurante", "largoComanda", "120")
			SaveSetting("SeeSoft", "Restaurante", "ModificaTamano", "1")

			txtAncho.Text = "80"
			txtLargo.Text = "120"

			MsgBox("Valores correctamente guardados")
		Catch ex As Exception
			MsgBox("Ha ocurrido un problema")
		End Try

	End Sub

	Private Sub MesasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MesasToolStripMenuItem.Click
		Try
			SaveSetting("SeeSoft", "Restaurante", "anchoMesas", "77")
			SaveSetting("SeeSoft", "Restaurante", "largoMesas", "77")
			SaveSetting("SeeSoft", "Restaurante", "ModificaTamano", "1")

			txtAnchoMesas.Text = "77"
			txtLargoMesas.Text = "77"

			MsgBox("Valores correctamente guardados")
		Catch ex As Exception
			MsgBox("Ha ocurrido un problema")
		End Try
	End Sub

	Private Sub TodosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TodosToolStripMenuItem.Click
		Try
			SaveSetting("SeeSoft", "Restaurante", "anchoComanda", "80")
			SaveSetting("SeeSoft", "Restaurante", "largoComanda", "120")
			SaveSetting("SeeSoft", "Restaurante", "anchoMesas", "77")
			SaveSetting("SeeSoft", "Restaurante", "largoMesas", "77")
			SaveSetting("SeeSoft", "Restaurante", "ModificaTamano", "1")

			txtAnchoMesas.Text = "77"
			txtLargoMesas.Text = "77"
			txtAncho.Text = "80"
			txtLargo.Text = "120"

			MsgBox("Valores correctamente guardados")
		Catch ex As Exception
			MsgBox("Ha ocurrido un problema")
		End Try
	End Sub
End Class