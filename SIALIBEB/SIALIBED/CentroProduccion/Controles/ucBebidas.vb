﻿Public Class ucBebidas

    Public NumMesa As String = ""
    Public Tomado As String = ""
    Public NameMesa As String = ""
    Dim NumCentros As Double = 0
    Dim Filas As Integer = 0
    Public IdMesa As Integer = 0
    Public IdCentro As Integer = 0
    Public Index As Integer = 0
    Public IdOrden As Integer = 0
    Public frm As New frmCentroProduccionBebidas
    Public Max As Integer = 0
    Dim Milisegundos As String = "0"
    Dim Segundos As String = "0"
    Dim Minutos As String = "0"
    Dim Hora As String = "0"
    Dim Tiempo() As String

    Private Sub ucBebidas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtnumMesa.Text = NumMesa
        txtnumMesa.Name = NameMesa
        CargarDetalles(IdCentro, IdMesa, IdOrden)
        txtnumMesa.TabIndex = 0
        Tiempo = Tomado.Split(":")
        If Tiempo.Count > 0 Then
            Hora = Tiempo(0)
            Minutos = Tiempo(1)
            Segundos = Tiempo(2)
        End If
        tmTiempoTranscurrido.Start()

        If Index = 1 Then
            txtnumMesa.Select()
        End If
    End Sub

    Private Function ValidarOrden(ByVal Centro As Integer, ByVal Mesa As Integer, ByVal _IdOrden As Integer) As Boolean
        Try

            Dim ii As Integer
            Dim dt As New DataTable
            Dim TodoListo As Integer = 0

            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Despachado from tb_DetalleOrden with (Nolock) where IdCentroProduccion=" & Centro & " and codMesa=" & Mesa & " and IdOrden=" & _IdOrden & "", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then
                For ii = 0 To dt.Rows.Count - 1

                    If dt.Rows(ii).Item("Despachado") = "False" Then
                        TodoListo = 1
                    End If
                Next

                If TodoListo = 1 Then
                    Return False
                Else
                    Return True
                End If

            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Error al validar orden. " & ex.Message, MsgBoxStyle.Critical)
            Return False
        End Try
    End Function

    Private Sub CargarDetalles(ByVal Centro As Integer, ByVal Mesa As Integer, ByVal _IdOrden As Integer)
        Try

            Dim ii As Integer
            Dim y As Integer = 26
            Dim x As Integer = 11
            Dim dt As New DataTable
            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select codProducto,SUM( Cantidad) as Cantidad,Descripcion,Despachado,Prioridad from tb_DetalleOrden with (Nolock) where Estado=1 and IdCentroProduccion=" & Centro & " and codMesa=" & Mesa & " and IdOrden=" & _IdOrden & "group by codProducto,Cantidad,Descripcion,Despachado,Prioridad order by Prioridad desc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            pnDetalle.Controls.Clear()
            txtnumMesa.Top = 2
            pnDetalle.Controls.Add(txtnumMesa)

            If dt.Rows.Count > 0 Then
                For ii = 0 To dt.Rows.Count - 1

                    Dim aButton As New TextBox
                    aButton.Top = y
                    aButton.Left = x
                    aButton.Width = 300

                    If dt.Rows(ii).Item("Despachado") = "True" Then
                        aButton.BackColor = Color.PaleGreen
                        aButton.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                    Else
                        aButton.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                    End If

                    If dt.Rows(ii).Item("Prioridad") = "True" Then
                        aButton.BackColor = Color.Red
                        aButton.ForeColor = Color.White
                    End If

                    aButton.Name = "lb_" & dt.Rows(ii).Item("Descripcion") & "_" & _IdOrden
                    aButton.Text = "(" & dt.Rows(ii).Item("Cantidad") & ") - " & dt.Rows(ii).Item("Descripcion")
                    aButton.TabIndex = 1 + ii
                    AddHandler aButton.KeyDown, AddressOf Me.Navegacion
                    AddHandler aButton.KeyPress, AddressOf Me.EvitarEscritura
                    AddHandler aButton.DoubleClick, AddressOf Me.Tachar_DestacharOrden

                    pnDetalle.Controls.Add(aButton)
                    If ii = dt.Rows.Count - 1 Then
                        txtIngreso.Top = Max - 25
                        pnDetalle.Controls.Add(txtIngreso)
                    End If
                    y = y + 25
                Next
            End If

            txtIngreso.TabIndex = dt.Rows.Count + 1

        Catch ex As Exception
            MsgBox("Error al cargar detalles. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Tachar_DestacharOrden(sender As Object, e As EventArgs)
        Try
            Dim datos As String
            datos = CStr(sender.name)

            For Each control As Control In pnDetalle.Controls
                If control.Name = datos Then
                    If RevisarDespacho(control.Name) Then
                        control.BackColor = Color.White
                        control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                        AumentarCantidad(control.Name)
                        Exit Sub
                    Else
                        control.BackColor = Color.PaleGreen
                        control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                        DisminuirCantidad(control.Name)
                        Exit Sub
                    End If

                End If
            Next
            frm.CargarResumen()
        Catch ex As Exception
            MsgBox("Error al regresar orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Function RevisarDespacho(NombreControl As String) As Boolean
        Try
            Dim _info() As String
            Dim Descripcion As String = ""

            _info = NombreControl.Split("_")
            If _info.Count > 0 Then
                Descripcion = _info(1)
            End If

            Dim dt As New DataTable
            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Despachado from tb_DetalleOrden with (Nolock) where Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and CodMesa=" & Me.IdMesa & "", dt)

            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Despachado") = "True" Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub EvitarEscritura(sender As Object, e As KeyPressEventArgs)
        e.Handled = True
    End Sub

    Public Sub Navegacion(sender As Object, e As KeyEventArgs)
        Try
            Dim datos As String
            datos = CStr(sender.name)

            If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then
                Select Case e.KeyCode
                    Case Keys.Down
                        If Not Me.GetNextControl(sender, True) Is Nothing Then
                            Me.GetNextControl(sender, True).Focus()
                        End If
                    Case Keys.Up
                        If Not Me.GetNextControl(sender, False) Is Nothing Then
                            Me.GetNextControl(sender, False).Focus()
                        End If
                    Case Keys.Right
                        For Each control As Control In pnDetalle.Controls
                            If control.Name = datos Then
                                control.BackColor = Color.PaleGreen
                                control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                                DisminuirCantidad(control.Name)
                                frm.CargarResumen()
                                Exit Sub
                            End If
                        Next
                    Case Keys.Left
                        For Each control As Control In pnDetalle.Controls
                            If control.Name = datos Then
                                control.BackColor = Color.White
                                control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                                AumentarCantidad(control.Name)
                                frm.CargarResumen()
                                Exit Sub
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            MsgBox("Error al leer Id del Centro de Producción. " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub



    Public Sub DisminuirCantidad(ByVal info As String)
        Try
            Dim _info() As String
            Dim Descripcion As String = ""

            _info = info.Split("_")
            If _info.Count > 0 Then
                Descripcion = _info(1)
            End If

            Dim cx As New Conexion
            Dim Sql As String = ""
            Sql = "Update tb_DetalleOrden set CantidadDespachada=Cantidad,Despachado=1,Entregado=1 where Estado=1 and Descripcion='" & Descripcion & "' and CodMesa=" & Me.IdMesa & " and IdOrden=" & Me.IdOrden & ""
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)

        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub AumentarCantidad(ByVal info As String)
        Try
            Dim _info() As String
            Dim Descripcion As String = ""

            _info = info.Split("_")
            If _info.Count > 0 Then
                Descripcion = _info(1)
            End If

            Dim cx As New Conexion
            Dim Sql As String = ""
            Sql = "Update tb_DetalleOrden set CantidadDespachada=0, Despachado=0,Entregado=0 where Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and CodMesa=" & Me.IdMesa & ""
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)

        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub txtnumMesa_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtnumMesa.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtIngreso_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIngreso.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtnumMesa_KeyDown(sender As Object, e As KeyEventArgs) Handles txtnumMesa.KeyDown
        If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then
            Select Case e.KeyCode
                Case Keys.Down
                    If Not Me.GetNextControl(sender, True) Is Nothing Then
                        Me.GetNextControl(sender, True).Select()
                    End If
                Case Keys.Up

                    If DevolverEstado(Me.Name) = "Listo" Then
                        frm.DevolverListos(IdMesa, IdOrden)
                    End If

                Case Keys.Left
                Case Keys.Right

                    If DevolverEstado(Me.Name) = "Listo" Then
                        If ValidarOrden(Me.IdCentro, Me.IdMesa, Me.IdOrden) Then
                            frm.EliminarListos(IdMesa, IdOrden)
                        End If
                    End If

            End Select
        End If
    End Sub


    Private Sub txtIngreso_KeyDown(sender As Object, e As KeyEventArgs) Handles txtIngreso.KeyDown
        If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode Then
            Select Case e.KeyCode
                Case Keys.Down
                    If ValidarOrden(Me.IdCentro, Me.IdMesa, Me.IdOrden) Then
                        If DevolverEstado(Me.Name) = "Listo" Then
                            frm.EliminarListos(IdMesa, IdOrden)
                        Else
                            frm.CargarListos(IdMesa, IdOrden)
                            Me.Hide()
                        End If

                    End If

                Case Keys.Up
                    If Not Me.GetNextControl(sender, False) Is Nothing Then
                        Me.GetNextControl(sender, False).Focus()
                    End If
                Case Keys.Left
                Case Keys.Right
            End Select
        End If
    End Sub

    Public Function DevolverEstado(ByVal NombreControl As String) As String
        Try
            Dim datos As String = NombreControl
            Dim _info() As String
            Dim Estado As String = ""

            _info = datos.Split("_")

            If _info.Count > 0 Then
                Estado = _info(3)
            End If

            Return Estado

        Catch ex As Exception

        End Try
    End Function


    Private Sub txtIngreso_Click(sender As Object, e As EventArgs) Handles txtIngreso.Click
        If ValidarOrden(Me.IdCentro, Me.IdMesa, Me.IdOrden) Then


            If DevolverEstado(Me.Name) = "Listo" Then
                frm.EliminarListos(IdMesa, IdOrden)
                Me.Hide()
            Else
                frm.CargarListos(IdMesa, IdOrden)
            End If


        End If
    End Sub

    Private Sub txtnumMesa_DoubleClick(sender As Object, e As EventArgs) Handles txtnumMesa.DoubleClick
        frm.DevolverListos(IdMesa, IdOrden)
    End Sub

    Private Sub tmTiempoTranscurrido_Tick(sender As Object, e As EventArgs) Handles tmTiempoTranscurrido.Tick
        tmTiempoTranscurrido.Interval = 10

        Milisegundos += 1

        If Milisegundos = "60" Then
            Segundos += 1
            Milisegundos = 0
        End If

        If Segundos = 60 Then
            Minutos += 1
            Segundos = 0
        End If

        If Minutos = 60 Then

            Hora += 1
            Milisegundos = 0

        End If
        txtIngreso.Text = Hora + ":" + Minutos + ":" + Segundos
    End Sub

    Public Sub GuardarTiempoTranscurrido()
        Dim cx As New Conexion
        Dim Sql As String = ""
        If Not txtIngreso.Text.Trim().Equals("") Then
            Sql = "Update tb_Orden set TiempoTranscurrido='" & txtIngreso.Text & "' where IdOrden=" & IdOrden & ""
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)
        End If
        tmTiempoTranscurrido.Stop()
    End Sub

    Private Sub txtnumMesa_Click(sender As Object, e As EventArgs) Handles txtnumMesa.Click
        MarcarDesmarcarTodo()
    End Sub

    Private Sub MarcarDesmarcarTodo()
        Try

            For Each control As Control In pnDetalle.Controls
                If Not control.Name.Contains("Mesa") And Not control.Name.Contains("Ingreso") Then
                    If RevisarDespacho(control.Name) Then
                        control.BackColor = Color.White
                        control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                        DesmarcarTodo(control.Name)
                    Else
                        control.BackColor = Color.PaleGreen
                        control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                        MarcarTodo(control.Name)
                    End If
                End If
            Next
            frm.CargarResumen()
        Catch ex As Exception
            MsgBox("Error al regresar orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub MarcarTodo(ByVal Info As String)
        Try
            Dim _info() As String
            Dim Descripcion As String = ""

            _info = Info.Split("_")
            If _info.Count > 0 Then
                Descripcion = _info(1)
            End If

            Dim cx As New Conexion
            Dim Sql As String = ""
            Sql = "Update tb_DetalleOrden set CantidadDespachada=Cantidad,Despachado=1,Entregado=1 where Estado=1 and Descripcion='" & Descripcion & "' and CodMesa=" & Me.IdMesa & " and IdOrden=" & Me.IdOrden & ""
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)
        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub DesmarcarTodo(ByVal Info As String)
        Try
            Dim _info() As String
            Dim Descripcion As String = ""

            _info = Info.Split("_")
            If _info.Count > 0 Then
                Descripcion = _info(1)
            End If

            Dim cx As New Conexion
            Dim Sql As String = ""
            Sql = "Update tb_DetalleOrden set CantidadDespachada=0, Despachado=0,Entregado=0 where Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and CodMesa=" & Me.IdMesa & ""
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)

        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub
End Class
