﻿Public Class ucCocina

    Public NumOrden As String = ""
    Public Tomado As String = ""
    Public NameOrden As String = ""
    Dim NumCentros As Double = 0
    Dim Filas As Integer = 0
    Public IdCentro As Integer = 0
    Public Index As Integer = 0
    Public IdOrden As Integer = 0
    Public frm As New frmCentroProduccionCocina
    Dim Descripcion As String = ""
    Public Max As Integer = 0
    Public Urgente As Integer = 0
    Dim Milisegundos As String = "0"
    Dim Segundos As String = "0"
    Dim Minutos As String = "0"
    Dim Hora As String = "0"
    Dim Tiempo() As String


    Private Sub ucCocina_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtnumOrden.Text = NumOrden
        txtnumOrden.Name = NameOrden
        CargarDetalles(IdCentro, IdOrden)
        txtnumOrden.TabIndex = 0
        Tiempo = Tomado.Split(":")
        If Tiempo.Count > 0 Then
            Hora = Tiempo(0)
            Minutos = Tiempo(1)
            Segundos = Tiempo(2)
        End If
        tmTiempoTranscurrido.Start()
    End Sub

    Private Sub CargarDetalles(ByVal Centro As Integer, ByVal _IdOrden As Integer)
        Try

            Dim ii As Integer
            Dim y As Integer = 26
            Dim x As Integer = 11
            Dim dt As New DataTable


            pnDetalle.Controls.Clear()
            txtnumOrden.Top = 2
            pnDetalle.Controls.Add(txtnumOrden)

            dt.Clear()

            cFunciones.Llenar_Tabla_Generico("Select sum(Cantidad) as TotalOrden,Descripcion,Despachado, Prioridad, 0 as IdComandaTemporal,Agrupar from vs_OrdenOrdenada with (Nolock) where IdCentroProduccion=" & Centro & " and IdOrden=" & _IdOrden & " and Agrupar='Si'
group by CodProducto,Descripcion,Despachado,Prioridad,Agrupar union Select sum(Cantidad) as TotalOrden,Descripcion,Despachado, Prioridad,IdComandaTemporal,Agrupar from vs_OrdenOrdenada with (Nolock) where IdCentroProduccion=" & Centro & " and IdOrden=" & _IdOrden & " and Agrupar='No'
group by CodProducto,Descripcion,Despachado,Prioridad,IdComandaTemporal,Agrupar order by Despachado asc, Prioridad desc,IdComandaTemporal asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                For ii = 0 To dt.Rows.Count - 1

                    Dim aButton As New TextBox
                    aButton.Top = y
                    aButton.Left = x
                    aButton.Width = 300

                    If dt.Rows(ii).Item("Despachado") = "True" Then
                        aButton.BackColor = Color.PaleGreen
                        aButton.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                    Else
                        aButton.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                    End If

                    If dt.Rows(ii).Item("Prioridad") = "True" Then
                        aButton.BackColor = Color.Red
                        aButton.ForeColor = Color.White
                    End If

                    If dt.Rows(ii).Item("IdComandaTemporal") = 0 Then
                        aButton.Name = "lb_" & dt.Rows(ii).Item("Descripcion") & "_" & dt.Rows(ii).Item("Agrupar") '+ "_" + Convert.ToString(dt.Rows(ii).Item("NumComanda"))
                    Else
                        aButton.Name = "lb_" & dt.Rows(ii).Item("Descripcion") & "_" & dt.Rows(ii).Item("Agrupar") + "_" + Convert.ToString(dt.Rows(ii).Item("IdComandaTemporal"))
                    End If

                    If Convert.ToInt32(dt.Rows(ii).Item("TotalOrden")) < 0 Then
                        aButton.Text = "*     " & dt.Rows(ii).Item("Descripcion")
                    Else

                        aButton.Text = "(" & dt.Rows(ii).Item("TotalOrden") & ")  " & dt.Rows(ii).Item("Descripcion")
                    End If

                    Me.ToolTip1.SetToolTip(aButton, dt.Rows(ii).Item("Descripcion"))

                    aButton.TabIndex = 1 + ii
                    AddHandler aButton.KeyDown, AddressOf Me.Navegacion
                    AddHandler aButton.KeyPress, AddressOf Me.EvitarEscritura
                    AddHandler aButton.DoubleClick, AddressOf Me.Tachar_DestacharOrden

                    If Urgente = 1 Then
                        Dim cx As New Conexion
                        Dim Sql As String = ""
                        Sql = "Update tb_DetalleOrden set Prioridad=1 where Estado=1 and Descripcion='" & dt.Rows(ii).Item("Descripcion") & "' and IdOrden=" & Me.IdOrden & " and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & dt.Rows(ii).Item("Agrupar") & "'"
                        cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                        cx.DesConectar(cx.sQlconexion)
                        txtIngreso.BackColor = Color.Red
                        txtIngreso.ForeColor = Color.White
                    End If

                    pnDetalle.Controls.Add(aButton)
                    If ii = dt.Rows.Count - 1 Then
                        txtIngreso.Top = Max - 25
                        pnDetalle.Controls.Add(txtIngreso)
                    End If
                    y = y + 25
                Next
            End If

            txtIngreso.TabIndex = dt.Rows.Count + 1

        Catch ex As Exception
            MsgBox("Error al cargar detalles. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Tachar_DestacharOrden(sender As Object, e As EventArgs)
        Try
            Dim datos As String
            datos = CStr(sender.name)

            For Each control As Control In pnDetalle.Controls
                If control.Name = datos Then
                    If RevisarDespacho(control.Name) Then
                        control.BackColor = Color.White
                        control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                        AumentarCantidad(control.Name)
                        Exit For
                    Else
                        control.BackColor = Color.PaleGreen
                        control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                        DisminuirCantidad(control.Name)
                        Exit For
                    End If
                End If
            Next
            frm.CargarComandas()
        Catch ex As Exception
            MsgBox("Error al regresar orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Function RevisarDespacho(NombreControl As String) As Boolean
        Try
            Dim _info() As String
            Dim Descripcion As String = ""
            Dim Agrupar As String = ""
            Dim IdComandaTemporal As String = "0"

            _info = NombreControl.Split("_")

            If _info.Count > 0 Then
                If _info.Count > 3 Then
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                    IdComandaTemporal = _info(3)
                Else
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                End If
            End If

            Dim dt As New DataTable
            dt.Clear()

            If IdComandaTemporal <> "0" Then
                cFunciones.Llenar_Tabla_Generico("Select Despachado from tb_DetalleOrden with (Nolock) where Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & "  and IdCentroProduccion=" & Me.IdCentro & " and IdComandaTemporal=" & IdComandaTemporal & " and Agrupar='" & Agrupar & "' ", dt)
            Else
                cFunciones.Llenar_Tabla_Generico("Select Despachado from tb_DetalleOrden with (Nolock) where Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & "  and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & Agrupar & "' ", dt)
            End If

            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Despachado") = "True" Then
                    Return True
                Else
                    Return False
                End If
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub EvitarEscritura(sender As Object, e As KeyPressEventArgs)
        e.Handled = True
    End Sub

    Public Sub Navegacion(sender As Object, e As KeyEventArgs)
        Try
            Dim datos As String
            datos = CStr(sender.name)

            If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then
                Select Case e.KeyCode
                    Case Keys.Down
                        If Not Me.GetNextControl(sender, True) Is Nothing Then
                            Me.GetNextControl(sender, True).Focus()
                        End If
                    Case Keys.Up
                        If Not Me.GetNextControl(sender, False) Is Nothing Then
                            Me.GetNextControl(sender, False).Focus()
                        End If
                    Case Keys.Right
                        For Each control As Control In pnDetalle.Controls
                            If control.Name = datos Then
                                control.BackColor = Color.PaleGreen
                                control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                                DisminuirCantidad(control.Name)
                                frm.CargarComandas()
                                Exit Sub
                            End If
                        Next
                    Case Keys.Left
                        For Each control As Control In pnDetalle.Controls
                            If control.Name = datos Then
                                control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Regular, GraphicsUnit.Pixel)
                                control.BackColor = Color.White
                                AumentarCantidad(control.Name)
                                frm.CargarComandas()
                                Exit Sub
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            MsgBox("Error al leer Id del Centro de Producción. " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Public Sub DisminuirCantidad(ByVal info As String)
        Try
            Dim _info() As String
            Dim Descripcion As String = ""
            Dim Agrupar As String = ""
            Dim IdAfectado As Integer = 0
            Dim dtModificadores As New DataTable
            Dim Sumar As Integer = 1
            Dim IdComandaTemporal As String = "0"

            _info = info.Split("_")
            If _info.Count > 0 Then
                If _info.Count > 3 Then
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                    IdComandaTemporal = _info(3)
                Else
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                End If
            End If

            Dim cx As New Conexion
            Dim Sql As String = ""

            If IdComandaTemporal <> "0" Then
                Sql = "Update tb_DetalleOrden set CantidadDespachada=Cantidad,Despachado=1,ParaEntregar=1,Entregado=1 OUTPUT INSERTED.Id where IdComandaTemporal=" & IdComandaTemporal & " and Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & Agrupar & "' "
            Else
                Sql = "Update tb_DetalleOrden set CantidadDespachada=Cantidad,Despachado=1,ParaEntregar=1,Entregado=1 OUTPUT INSERTED.Id where Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & Agrupar & "' "
            End If

            Try
                IdAfectado = cx.SlqExecuteScalar(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)
            Catch ex As Exception
                IdAfectado = 0
            End Try

            If IdAfectado > 0 Then
                cFunciones.Llenar_Tabla_Generico("Select Cantidad from tb_DetalleOrden with (Nolock) where Id=" & IdAfectado & "", dtModificadores)
                If dtModificadores.Rows(0).Item("Cantidad") <> -1 Then


Repetir:
                    dtModificadores.Clear()
                    cFunciones.Llenar_Tabla_Generico("Select Cantidad from tb_detalleOrden with (Nolock) where Id=" & IdAfectado + Sumar & "", dtModificadores)
                    If dtModificadores.Rows.Count > 0 Then
                        If dtModificadores.Rows(0).Item("Cantidad") < 0 Then
                            Sql = "Update tb_DetalleOrden set CantidadDespachada=Cantidad,Despachado=1,ParaEntregar=1, Entregado=1 where Id=" & IdAfectado + Sumar & ""
                            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                            Sumar = Sumar + 1
                            cx.DesConectar(cx.sQlconexion)
                            GoTo Repetir
                        End If
                    End If
                End If
            End If

            frm.NombreControl = Descripcion
        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub AumentarCantidad(ByVal info As String)
        Try
            Dim _info() As String
            Dim Descripcion As String = ""
            Dim Agrupar As String = ""
            Dim IdAfectado As Integer = 0
            Dim IdComandaTemporal As String = "0"

            _info = info.Split("_")
            If _info.Count > 0 Then
                If _info.Count > 3 Then
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                    IdComandaTemporal = _info(3)
                Else
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                End If
            End If

            Dim cx As New Conexion
            Dim Sql As String = ""

            If IdComandaTemporal <> "0" Then
                Sql = "Update tb_DetalleOrden set CantidadDespachada=0, Despachado=0, ParaEntregar=0,Entregado=0 OUTPUT INSERTED.Id where IdComandaTemporal=" & IdComandaTemporal & " and Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & Agrupar & "' "
            Else
                Sql = "Update tb_DetalleOrden set CantidadDespachada=0, Despachado=0, ParaEntregar=0,Entregado=0 OUTPUT INSERTED.Id where Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & Agrupar & "' "
            End If

            Try
                IdAfectado = cx.SlqExecuteScalar(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)
            Catch ex As Exception
                IdAfectado = 0
            End Try

            If IdAfectado > 0 Then
                Dim dtModificadores As New DataTable
                Dim Sumar As Integer = 1
                cFunciones.Llenar_Tabla_Generico("Select Cantidad from tb_DetalleOrden with (Nolock) where Id=" & IdAfectado & "", dtModificadores)
                If dtModificadores.Rows(0).Item("Cantidad") <> -1 Then
Repetir:
                    dtModificadores.Clear()
                    cFunciones.Llenar_Tabla_Generico("Select Cantidad from tb_detalleOrden with (Nolock) where Id=" & IdAfectado + Sumar & "", dtModificadores)
                    If dtModificadores.Rows.Count > 0 Then
                        If dtModificadores.Rows(0).Item("Cantidad") < 0 Then
                            Sql = "Update tb_DetalleOrden set CantidadDespachada=0, Despachado=0, ParaEntregar=0,Entregado=0 where Id=" & IdAfectado + Sumar & ""
                            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                            Sumar = Sumar + 1
                            cx.DesConectar(cx.sQlconexion)
                            GoTo Repetir
                        End If
                    End If
                End If
            End If


        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub txtnumOrden_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtnumOrden.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtIngreso_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIngreso.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtnumOrden_KeyDown(sender As Object, e As KeyEventArgs) Handles txtnumOrden.KeyDown
        If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then
            Select Case e.KeyCode
                Case Keys.Down
                    If Not Me.GetNextControl(sender, True) Is Nothing Then
                        Me.GetNextControl(sender, True).Select()
                    End If
            End Select
        End If
    End Sub

    Private Sub txtIngreso_KeyDown(sender As Object, e As KeyEventArgs) Handles txtIngreso.KeyDown
        If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode Then

            Select Case e.KeyCode
                Case Keys.Down
                    If ValidarOrden(Me.IdCentro, Me.IdOrden) Then
                        frm.EliminarListos(IdCentro, IdOrden)
                        Me.Hide()
                    End If

                Case Keys.Up
                    If Not Me.GetNextControl(sender, False) Is Nothing Then
                        Me.GetNextControl(sender, False).Focus()
                    End If
                Case Keys.Left
                Case Keys.Right
            End Select
        End If
    End Sub

    Private Function ValidarOrden(ByVal Centro As Integer, ByVal _IdOrden As Integer) As Boolean
        Try

            Dim ii As Integer
            Dim y As Integer = 26
            Dim x As Integer = 5
            Dim dt As New DataTable
            Dim TodoListo As Integer = 0

            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Despachado from tb_DetalleOrden where EstadoComanda=1 and IdCentroProduccion=" & Centro & " and IdOrden=" & _IdOrden & " group by CodProducto,Descripcion,Despachado", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then
                For ii = 0 To dt.Rows.Count - 1
                    'bandera

                    Dim aButton As New TextBox
                    aButton.Top = y
                    aButton.Left = x
                    aButton.Width = 121
                    If dt.Rows(ii).Item("Despachado") = "False" Then
                        TodoListo = 1
                    End If
                Next

                If TodoListo = 1 Then
                    Return False
                Else
                    Return True
                End If

            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Error al validar orden. " & ex.Message, MsgBoxStyle.Critical)
            Return False
        End Try
    End Function

    Private Sub txtIngreso_Click(sender As Object, e As EventArgs) Handles txtIngreso.Click

        If ValidarOrden(Me.IdCentro, Me.IdOrden) Then
            frm.EliminarListos(IdCentro, IdOrden)
        End If


    End Sub

    Private Sub tmTiempoTranscurrido_Tick(sender As Object, e As EventArgs) Handles tmTiempoTranscurrido.Tick
        tmTiempoTranscurrido.Interval = 10

        Milisegundos += 1

        If Milisegundos = "60" Then
            Segundos += 1
            Milisegundos = 0
        End If

        If Segundos = 60 Then
            Minutos += 1
            Segundos = 0
        End If

        If Minutos = 60 Then

            Hora += 1
            Milisegundos = 0

        End If
        txtIngreso.Text = Hora + ":" + Minutos + ":" + Segundos
    End Sub

    Public Sub GuardarTiempoTranscurrido()
        Dim cx As New Conexion
        Dim Sql As String = ""
        If Not txtIngreso.Text.Trim().Equals("") Then
            Sql = "Update tb_Orden set TiempoTranscurrido='" & txtIngreso.Text & "' where IdOrden=" & IdOrden & ""
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)
        End If
        tmTiempoTranscurrido.Stop()
    End Sub

    Private Sub txtnumOrden_Click(sender As Object, e As EventArgs) Handles txtnumOrden.Click
        MarcarDesmarcarTodo()
    End Sub

    Private Sub MarcarDesmarcarTodo()
        Try

            For Each control As Control In pnDetalle.Controls
                If Not control.Name.Contains("Orden") And Not control.Name.Contains("Ingreso") Then
                    If RevisarDespacho(control.Name) Then
                        control.BackColor = Color.White
                        control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                        DesmarcarTodo(control.Name)
                    Else
                        control.BackColor = Color.PaleGreen
                        control.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold, GraphicsUnit.Pixel)
                        MarcarTodo(control.Name)
                    End If
                End If
            Next
            ' frm.CargarComandas()
        Catch ex As Exception
            MsgBox("Error al regresar orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub MarcarTodo(ByVal Info As String)
        Try
            Dim _info() As String
            Dim Descripcion As String = ""
            Dim Agrupar As String = ""
            Dim IdAfectado As Integer = 0
            Dim dtModificadores As New DataTable
            Dim Sumar As Integer = 1
            Dim IdComandaTemporal As String = "0"
            _info = Info.Split("_")

            If _info.Count > 0 Then
                If _info.Count > 3 Then
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                    IdComandaTemporal = _info(3)
                Else
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                End If
            End If

            Dim cx As New Conexion
            Dim Sql As String = ""

            If IdComandaTemporal <> "0" Then
                Sql = "Update tb_DetalleOrden set CantidadDespachada=Cantidad,Despachado=1,ParaEntregar=1,Entregado=1 OUTPUT INSERTED.Id where IdComandaTemporal=" & IdComandaTemporal & " and Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & Agrupar & "' "
            Else
                Sql = "Update tb_DetalleOrden set CantidadDespachada=Cantidad,Despachado=1,ParaEntregar=1,Entregado=1 OUTPUT INSERTED.Id where Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & Agrupar & "' "
            End If

            Try
                IdAfectado = cx.SlqExecuteScalar(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)
            Catch ex As Exception
                IdAfectado = 0
            End Try

            frm.NombreControl = Descripcion

        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub DesmarcarTodo(ByVal Info As String)
        Try
            Dim _info() As String
            Dim Descripcion As String = ""
            Dim Agrupar As String = ""
            Dim IdAfectado As Integer = 0
            Dim IdComandaTemporal As String = "0"
            _info = Info.Split("_")

            If _info.Count > 0 Then
                If _info.Count > 3 Then
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                    IdComandaTemporal = _info(3)
                Else
                    Descripcion = _info(1)
                    Agrupar = _info(2)
                End If
            End If

            Dim cx As New Conexion
            Dim Sql As String = ""

            If IdComandaTemporal <> "0" Then
                Sql = "Update tb_DetalleOrden set CantidadDespachada=0, Despachado=0, ParaEntregar=0,Entregado=0 OUTPUT INSERTED.Id where IdComandaTemporal=" & IdComandaTemporal & " and Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & Agrupar & "' "
            Else
                Sql = "Update tb_DetalleOrden set CantidadDespachada=0, Despachado=0, ParaEntregar=0,Entregado=0 OUTPUT INSERTED.Id where Estado=1 and Descripcion='" & Descripcion & "' and IdOrden=" & Me.IdOrden & " and IdCentroProduccion=" & Me.IdCentro & " and Agrupar='" & Agrupar & "' "
            End If

            Try
                IdAfectado = cx.SlqExecuteScalar(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)
            Catch ex As Exception
                IdAfectado = 0
            End Try


        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub
End Class
