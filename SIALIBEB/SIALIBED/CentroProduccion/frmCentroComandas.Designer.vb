﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCentroComandas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.panelListosEntregar = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.panelCentroComandas = New System.Windows.Forms.Panel()
        Me.lbTimer = New System.Windows.Forms.Label()
        Me.panelLlevarExpress = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.panelListoExpress = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbTodos = New System.Windows.Forms.RadioButton()
        Me.rbPlatos = New System.Windows.Forms.RadioButton()
        Me.rbBebidas = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtActualizar = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbHora = New System.Windows.Forms.Label()
        Me.tmHora = New System.Windows.Forms.Timer(Me.components)
        Me.btLimpiarComandas = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelListosEntregar
        '
        Me.panelListosEntregar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.panelListosEntregar.AutoScroll = True
        Me.panelListosEntregar.BackColor = System.Drawing.Color.White
        Me.panelListosEntregar.Location = New System.Drawing.Point(4, 49)
        Me.panelListosEntregar.Name = "panelListosEntregar"
        Me.panelListosEntregar.Size = New System.Drawing.Size(385, 126)
        Me.panelListosEntregar.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DimGray
        Me.Label1.Location = New System.Drawing.Point(89, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(228, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "LISTOS PARA ENTREGAR"
        '
        'panelCentroComandas
        '
        Me.panelCentroComandas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelCentroComandas.AutoScroll = True
        Me.panelCentroComandas.BackColor = System.Drawing.Color.White
        Me.panelCentroComandas.Location = New System.Drawing.Point(395, 48)
        Me.panelCentroComandas.Name = "panelCentroComandas"
        Me.panelCentroComandas.Size = New System.Drawing.Size(511, 127)
        Me.panelCentroComandas.TabIndex = 2
        '
        'lbTimer
        '
        Me.lbTimer.BackColor = System.Drawing.Color.DimGray
        Me.lbTimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTimer.ForeColor = System.Drawing.Color.Red
        Me.lbTimer.Location = New System.Drawing.Point(321, 14)
        Me.lbTimer.Name = "lbTimer"
        Me.lbTimer.Size = New System.Drawing.Size(49, 30)
        Me.lbTimer.TabIndex = 1
        Me.lbTimer.Text = "timer"
        Me.lbTimer.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'panelLlevarExpress
        '
        Me.panelLlevarExpress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelLlevarExpress.AutoScroll = True
        Me.panelLlevarExpress.BackColor = System.Drawing.Color.White
        Me.panelLlevarExpress.Location = New System.Drawing.Point(395, 181)
        Me.panelLlevarExpress.Name = "panelLlevarExpress"
        Me.panelLlevarExpress.Size = New System.Drawing.Size(511, 266)
        Me.panelLlevarExpress.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DimGray
        Me.Label5.Location = New System.Drawing.Point(558, 183)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(237, 20)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "PARA LLEVAR Y EXPRESS"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'panelListoExpress
        '
        Me.panelListoExpress.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.panelListoExpress.AutoScroll = True
        Me.panelListoExpress.BackColor = System.Drawing.Color.White
        Me.panelListoExpress.Location = New System.Drawing.Point(4, 181)
        Me.panelListoExpress.Name = "panelListoExpress"
        Me.panelListoExpress.Size = New System.Drawing.Size(385, 266)
        Me.panelListoExpress.TabIndex = 2
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.BackColor = System.Drawing.Color.White
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.DimGray
        Me.Label7.Location = New System.Drawing.Point(72, 185)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(262, 20)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "LISTOS LLEVAR Y EXPRESS"
        '
        'Timer1
        '
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.rbTodos)
        Me.GroupBox1.Controls.Add(Me.rbPlatos)
        Me.GroupBox1.Controls.Add(Me.rbBebidas)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.GroupBox1.Location = New System.Drawing.Point(379, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(203, 44)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filtrar"
        '
        'rbTodos
        '
        Me.rbTodos.AutoSize = True
        Me.rbTodos.Location = New System.Drawing.Point(140, 14)
        Me.rbTodos.Name = "rbTodos"
        Me.rbTodos.Size = New System.Drawing.Size(58, 17)
        Me.rbTodos.TabIndex = 2
        Me.rbTodos.TabStop = True
        Me.rbTodos.Text = "Todos."
        Me.rbTodos.UseVisualStyleBackColor = True
        '
        'rbPlatos
        '
        Me.rbPlatos.AutoSize = True
        Me.rbPlatos.Location = New System.Drawing.Point(77, 14)
        Me.rbPlatos.Name = "rbPlatos"
        Me.rbPlatos.Size = New System.Drawing.Size(57, 17)
        Me.rbPlatos.TabIndex = 1
        Me.rbPlatos.TabStop = True
        Me.rbPlatos.Text = "Platos."
        Me.rbPlatos.UseVisualStyleBackColor = True
        '
        'rbBebidas
        '
        Me.rbBebidas.AutoSize = True
        Me.rbBebidas.Location = New System.Drawing.Point(5, 13)
        Me.rbBebidas.Name = "rbBebidas"
        Me.rbBebidas.Size = New System.Drawing.Size(66, 17)
        Me.rbBebidas.TabIndex = 0
        Me.rbBebidas.TabStop = True
        Me.rbBebidas.Text = "Bebidas."
        Me.rbBebidas.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Label2.Location = New System.Drawing.Point(663, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Actualizar en:"
        '
        'txtActualizar
        '
        Me.txtActualizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtActualizar.Location = New System.Drawing.Point(766, 16)
        Me.txtActualizar.Name = "txtActualizar"
        Me.txtActualizar.Size = New System.Drawing.Size(46, 20)
        Me.txtActualizar.TabIndex = 6
        Me.txtActualizar.Text = "1"
        Me.txtActualizar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Label4.Location = New System.Drawing.Point(818, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 20)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "segundos."
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DimGray
        Me.Label6.Location = New System.Drawing.Point(537, 49)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(284, 20)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "CENTRO DE COMANDAS"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbHora
        '
        Me.lbHora.BackColor = System.Drawing.Color.DimGray
        Me.lbHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbHora.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.lbHora.Location = New System.Drawing.Point(12, 14)
        Me.lbHora.Name = "lbHora"
        Me.lbHora.Size = New System.Drawing.Size(176, 30)
        Me.lbHora.TabIndex = 8
        Me.lbHora.Text = "timer"
        Me.lbHora.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'tmHora
        '
        '
        'btLimpiarComandas
        '
        Me.btLimpiarComandas.BackColor = System.Drawing.Color.LightSkyBlue
        Me.btLimpiarComandas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btLimpiarComandas.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btLimpiarComandas.ForeColor = System.Drawing.Color.Purple
        Me.btLimpiarComandas.Location = New System.Drawing.Point(153, 5)
        Me.btLimpiarComandas.Name = "btLimpiarComandas"
        Me.btLimpiarComandas.Size = New System.Drawing.Size(151, 39)
        Me.btLimpiarComandas.TabIndex = 9
        Me.btLimpiarComandas.Text = "Limpiar Comandas"
        Me.btLimpiarComandas.UseVisualStyleBackColor = False
        '
        'frmCentroComandas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(910, 450)
        Me.Controls.Add(Me.btLimpiarComandas)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbHora)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtActualizar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lbTimer)
        Me.Controls.Add(Me.panelListoExpress)
        Me.Controls.Add(Me.panelLlevarExpress)
        Me.Controls.Add(Me.panelCentroComandas)
        Me.Controls.Add(Me.panelListosEntregar)
        Me.Name = "frmCentroComandas"
        Me.Text = "Centro de Comandas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents panelListosEntregar As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents panelCentroComandas As Panel
    Friend WithEvents panelLlevarExpress As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents panelListoExpress As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents Timer1 As Timer
    Protected WithEvents lbTimer As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rbTodos As RadioButton
    Friend WithEvents rbPlatos As RadioButton
    Friend WithEvents rbBebidas As RadioButton
    Friend WithEvents Label2 As Label
    Friend WithEvents txtActualizar As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label6 As Label
    Protected WithEvents lbHora As Label
    Friend WithEvents tmHora As Timer
    Friend WithEvents btLimpiarComandas As Button
    Friend WithEvents ToolTip1 As ToolTip
End Class
