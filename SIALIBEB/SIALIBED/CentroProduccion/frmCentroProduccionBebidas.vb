﻿Imports System.Data.SqlClient
Imports System.ComponentModel

Public Class frmCentroProduccionBebidas


    Dim PMU As New PerfilModulo_Class
    Dim NumCentros As Integer = 0
    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlClient.SqlConnection
    Dim rs As SqlClient.SqlDataReader
    Dim dt As New DataTable
    Dim cambio As Boolean = False
    Public Shared IdCentro As Integer = 0
    Public TiempoEspera As Double
    Dim TiempoInicial As Date
    Public NombreCentro As String = ""
    Dim aButton As ucBebidas
    Dim ListaColores As New ArrayList
    Dim CantDetalles As Integer = 0
    Dim Consulta As String = ""

    Private Sub frmCentroProduccionBebidas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        CargarColores()
        lbCentroProduccion.Text = NombreCentro
        CargarListos()
        TiempoInicial = Now
        Me.Timer1.Enabled = True
        Me.tmHora.Start()
    End Sub

    Public Sub CargarColores()

        ListaColores.Add("Firebrick")
        ListaColores.Add("LightCoral")
        ListaColores.Add("Tomato")
        ListaColores.Add("OrangeRed")
        ListaColores.Add("Chocolate")
        ListaColores.Add("SandyBrown")
        ListaColores.Add("Goldenrod")
        ListaColores.Add("Orange")
        ListaColores.Add("Gold")
        ListaColores.Add("Yellow")
        ListaColores.Add("YellowGreen")
        ListaColores.Add("GreenYellow")
        ListaColores.Add("RosyBrown")
        ListaColores.Add("LimeGreen")
        ListaColores.Add("SpringGreen")
        ListaColores.Add("Turquoise")
        ListaColores.Add("Aqua")
        ListaColores.Add("HotPink")
        ListaColores.Add("DarkKhaki")
        ListaColores.Add("Tan")
        ListaColores.Add("NavajoWhite")
        ListaColores.Add("Lime")
        ListaColores.Add("DeepPink")
        ListaColores.Add("Crimson")
        ListaColores.Add("Magenta")
        ListaColores.Add("DarkSeaGreen")
        ListaColores.Add("PaleTurquoise")
        ListaColores.Add("CadetBlue")
        ListaColores.Add("DodgerBlue")
        ListaColores.Add("MediumVioletRed")
        ListaColores.Add("Violet")
        ListaColores.Add("Olive")
        ListaColores.Add("Coral")
        ListaColores.Add("Peru")
        ListaColores.Add("DarkOrange")
    End Sub

    Public Function CalcularColumas() As Integer
        Try
            Dim AnchoPanel As Double = 0
            Dim AnchoControl As Double = 0
            Dim Columnas As Integer = 0
            aButton = New ucBebidas
            AnchoPanel = PanelMesas.Size.Width
            AnchoControl = aButton.Size.Width

            Columnas = Math.Floor((AnchoPanel / AnchoControl))
            Return Columnas

        Catch ex As Exception
            Return 6
        End Try
    End Function

    Private Sub CargarCentros() 'Panel principal 
        Try

            Dim i, ii, n As Integer
            Dim y As Integer = 3
            Dim x As Integer = 3
            Dim Filas As Integer = 0
            Dim Columnas As Integer = CalcularColumas() 'Modificar si se desan mas columnas por fila
            Dim Orden As Integer = 0


            Dim num_controles As Int32 = Me.PanelMesas.Controls.Count - 1
            For z As Integer = num_controles To 0 Step -1
                Dim ctrl As Windows.Forms.Control = Me.PanelMesas.Controls(z)
                Me.PanelMesas.Controls.Remove(ctrl)
                ctrl.Dispose()
            Next

            PanelMesas.Controls.Clear()

            Dim dt As New DataTable
            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList
            Dim LosNuevos As Integer = 0

            cFunciones.Llenar_Tabla_Generico("select CONVERT(varchar, tb_DetalleOrden.IdOrden)+'-'+CONVERT(varchar, CodMesa) as CodMesa,IdComandaTemporal,NumOrden from dbo.tb_DetalleOrden with (Nolock) right outer join tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden 
            where tb_detalleOrden.IdCentroProduccion=" & IdCentro & " and tb_DetalleOrden.Estado=1 and ParaEntregar=0 order by Prioridad desc,NumOrden asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not unicos.Contains(dt.Rows(k).Item("codMesa")) Then
                        unicos.Add(dt.Rows(k).Item("codMesa"))
                        UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                    End If
                Next

                Dim _Filas As Integer = UnicosId.Count

                For b As Integer = 0 To _Filas 'Calcula cuantas filas son necesarias para mostrar toda la info
                    If _Filas >= 1 Then
                        _Filas = _Filas - Columnas 'modificar para aumentar o disminuir la cantidad a mostrar por filas, 1 = mostrar uno por fila
                        Filas += 1
                    Else
                        If b = 0 Then
                            Filas = 1
                        End If
                    End If
                Next


                Dim quitar As Integer = 0
                Dim recorrer As Integer = Columnas - 1 'Cuantas columnas deseas 0 = 1 columna, 1 = 2 columnas, etc
                Dim LargoUc As Integer = CalcularLargoUc("Central")

                If Filas = 1 Then
                    recorrer = UnicosId.Count - 1
                End If

                For i = 0 To Filas - 1
                    For ii = 0 To recorrer

                        quitar = ii

                        dt.Clear()
                        cFunciones.Llenar_Tabla_Generico("select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,TiempoTranscurrido,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal=" & Convert.ToInt32(UnicosId(ii)) & " ", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        aButton = New ucBebidas
                        aButton.Top = y
                        aButton.Left = x
                        aButton.Width = 326
                        aButton.Height = LargoUc + 6
                        aButton.pnDetalle.Height = LargoUc
                        aButton.Max = LargoUc
                        aButton.Name = "Mesa_" & dt.Rows(0).Item("CodMesa") & "_" & dt.Rows(0).Item("IdOrden") & "_Proceso_" & Orden
                        aButton.NumMesa = "Mesa #" & dt.Rows(0).Item("NombreMesa")
                        aButton.Tomado = dt.Rows(0).Item("TiempoTranscurrido")
                        aButton.IdMesa = dt.Rows(0).Item("codMesa")
                        aButton.IdCentro = dt.Rows(0).Item("IdCentroProduccion")
                        aButton.IdOrden = dt.Rows(0).Item("IdOrden")
                        aButton.frm = Me
                        aButton.NameMesa = "Mesa_" & dt.Rows(0).Item("NombreMesa") & "_" & dt.Rows(0).Item("IdOrden")
                        Orden = Orden + 1
                        aButton.txtnumMesa.BackColor = Color.FromName(Convert.ToString(dt.Rows(0).Item("Color")).Replace("Color [", "").Replace("]", ""))

                        If i = 0 And ii = 0 Then
                            aButton.Index = 1
                        End If

                        Me.PanelMesas.Controls.Add(aButton)
                        x = x + 328
                        n = n + 1

                    Next

                    If UnicosId.Count > 0 Then
                        For g As Integer = 0 To quitar
                            Dim a As Integer = 0
                            UnicosId.RemoveAt(a)
                        Next
                    End If

                    If UnicosId.Count <= Columnas Then
                        recorrer = UnicosId.Count - 1
                    End If

                    y = y + LargoUc + 7
                    x = 3
                Next
            Else

                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Error al crear mesas. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub CargarResumen() ' Panel derecho
        Try

            PanelResumen.Visible = False
            Dim dt As New DataTable
            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("select Descripcion, sum( Cantidad-cantidadDespachada)  as Total from tb_DetalleOrden with (Nolock) where Estado=1 and IdCentroProduccion=" & IdCentro & " group by CodProducto,Descripcion", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            Dim num_controles As Int32 = Me.PanelResumen.Controls.Count - 1
            For z As Integer = num_controles To 0 Step -1
                Dim ctrl As Windows.Forms.Control = Me.PanelResumen.Controls(z)
                Me.PanelResumen.Controls.Remove(ctrl)
                ctrl.Dispose()
            Next

            PanelResumen.Controls.Clear()

            Dim lbtitulo As New Label
            lbtitulo.Location = New System.Drawing.Point(78, 13)
            lbtitulo.Text = "Preparación"
            lbtitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            lbtitulo.Size = New System.Drawing.Size(129, 26)
            PanelResumen.Controls.Add(lbtitulo)

            Dim y As Integer = 45
            Dim x As Integer = 8
            Dim CodProducto As Integer = 0
            Dim CodigosProducto As New ArrayList

            If dt.Rows.Count > 0 Then
                For a As Integer = 0 To dt.Rows.Count - 1

                    Dim aButton As New TextBox
                    aButton.Top = y
                    aButton.Left = x
                    aButton.Width = 200

                    aButton.Name = "lbResumen_" & dt.Rows(a).Item("Descripcion")

                    If Convert.ToDouble(dt.Rows(a).Item("Total")) <> 0 Then
                        aButton.Text = dt.Rows(a).Item("Total") & " - " & dt.Rows(a).Item("Descripcion")
                        aButton.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                    Else
                        aButton.Text = dt.Rows(a).Item("Total") & " - " & dt.Rows(a).Item("Descripcion")
                        aButton.BackColor = Color.PaleGreen
                        aButton.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                    End If

                    AddHandler aButton.KeyDown, AddressOf Me.GestionarResumen
                    AddHandler aButton.DoubleClick, AddressOf Me.Tachar_DestacharOrden

                    Me.PanelResumen.Controls.Add(aButton)
                    y = y + 25
                Next
            End If

            Me.PanelResumen.Visible = True

        Catch ex As Exception
            MsgBox("Error al cargar el resumen de las ordenes. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Tachar_DestacharOrden(sender As Object, e As EventArgs)
        Try
            Dim datos As String
            datos = CStr(sender.name)

            If RevisarDespacho(datos) Then
                For Each control As Control In PanelResumen.Controls
                    If control.Name = datos Then
                        control.BackColor = Color.PaleGreen
                        control.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                        CancelarDespacharOrden(control.Name)
                    End If
                Next
            Else

                For Each control As Control In PanelResumen.Controls
                    If control.Name = datos Then
                        control.BackColor = Color.PaleGreen
                        control.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                        DespacharOrden(control.Name)
                    End If
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Function RevisarDespacho(NombreControl As String) As Boolean
        Try
            Dim _info() As String
            Dim Descripcion As String = ""
            Dim dt As New DataTable

            _info = NombreControl.Split("_")

            If _info.Count > 0 Then
                Descripcion = Convert.ToString(_info(1)).Trim()
            End If

            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Despachado from tb_DetalleOrden with (Nolock) where Estado=1 and Descripcion='" & Descripcion & "'", dt)

            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Despachado") = "True" Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub GestionarResumen(sender As Object, e As KeyEventArgs)
        Try
            Dim datos As String
            datos = CStr(sender.name)

            If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then
                Select Case e.KeyCode
                    Case Keys.Down
                        If Not Me.GetNextControl(sender, True) Is Nothing Then
                            Me.GetNextControl(sender, True).Focus()
                        End If
                    Case Keys.Up
                        If Not Me.GetNextControl(sender, False) Is Nothing Then
                            Me.GetNextControl(sender, False).Focus()
                        End If
                    Case Keys.Right
                        For Each control As Control In PanelResumen.Controls
                            If control.Name = datos Then
                                control.BackColor = Color.PaleGreen
                                control.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                                DespacharOrden(control.Name)
                            End If
                        Next
                    Case Keys.Left
                        For Each control As Control In PanelResumen.Controls
                            If control.Name = datos Then
                                control.BackColor = Color.PaleGreen
                                control.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                                CancelarDespacharOrden(control.Name)
                            End If
                        Next
                End Select
            End If
        Catch ex As Exception
            MsgBox("Error al gestionar resumen. " & ex.Message)
        End Try
    End Sub

    Sub DespacharOrden(ByVal Info As String)
        Try

            Dim _info() As String
            Dim Descripcion As String = ""
            Dim dt As New DataTable

            _info = Info.Split("_")

            If _info.Count > 0 Then
                Descripcion = Convert.ToString(_info(1)).Trim()
            End If

            cFunciones.Llenar_Tabla_Generico("Select IdComandaTemporal from tb_DetalleOrden with (Nolock) where Estado=1 and Descripcion='" & Descripcion & "' and Despachado=0", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            Dim cx As New Conexion
            Dim Sql As String = ""


            If dt.Rows.Count > 0 Then
                For p As Integer = 0 To dt.Rows.Count - 1
                    Sql = "Update tb_DetalleOrden set CantidadDespachada=Cantidad,Despachado=1,Entregado=1 where Estado=1 and IdComandaTemporal=" & dt.Rows(p).Item("IdComandaTemporal") & ""
                    cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                    cx.DesConectar(cx.sQlconexion)
                Next
            End If

            CargarCentros()
            CargarResumen()

        Catch ex As Exception
            MsgBox("Error al despachar orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub CancelarDespacharOrden(ByVal Info As String)
        Try

            Dim _info() As String
            Dim Descripcion As String = ""
            Dim dt As New DataTable
            _info = Info.Split("_")
            If _info.Count > 0 Then
                Descripcion = Convert.ToString(_info(1)).Trim()
            End If

            cFunciones.Llenar_Tabla_Generico("Select IdComandaTemporal from tb_DetalleOrden with (Nolock) where Descripcion='" & Descripcion & "' and Despachado=1 and Estado=1", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            Dim cx As New Conexion
            Dim Sql As String = ""


            If dt.Rows.Count > 0 Then
                For p As Integer = 0 To dt.Rows.Count - 1
                    Sql = "Update tb_DetalleOrden set CantidadDespachada=0,Despachado=0,Entregado=0 where IdComandaTemporal=" & dt.Rows(p).Item("IdComandaTemporal") & " and Estado=1"
                    cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                    cx.DesConectar(cx.sQlconexion)
                Next
            End If

            CargarCentros()
            CargarResumen()

        Catch ex As Exception
            MsgBox("Error al despachar orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub CargarListos(Optional IdMesa As Integer = 0, Optional IdOrden As Integer = 0) 'Panel inferior
        Try

            Dim ii As Integer
            Dim y As Integer = 3
            Dim x As Integer = 3

            Dim num_controles As Int32 = Me.PanelSeleccionados.Controls.Count - 1
            For z As Integer = num_controles To 0 Step -1
                Dim ctrl As Windows.Forms.Control = Me.PanelSeleccionados.Controls(z)
                Me.PanelSeleccionados.Controls.Remove(ctrl)
                ctrl.Dispose()
            Next


            PanelSeleccionados.Controls.Clear()
            PanelSeleccionados.Visible = False

            Dim cx As New Conexion
            Dim Sql As String = ""

            If IdMesa <> 0 Then
                Sql = "Update tb_DetalleOrden set ParaEntregar=1,Entregado=1 where CodMesa=" & IdMesa & " and IdOrden=" & IdOrden & " and Estado=1"
                cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)

            End If

            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("select IdComandaTemporal,CONVERT(varchar, tb_DetalleOrden.IdOrden)+'-'+CONVERT(varchar,tb_DetalleOrden.CodMesa) as CodMesa from dbo.tb_DetalleOrden with (Nolock)
                        where tb_detalleOrden.ParaEntregar=1 and Estado=1 and IdCentroProduccion=" & IdCentro & " order by codMesa asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList
            Dim LargoUc As Integer = CalcularLargoUc("Listos")
            If dt.Rows.Count > 0 Then

                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not unicos.Contains(dt.Rows(k).Item("codMesa")) Then
                        unicos.Add(dt.Rows(k).Item("codMesa"))
                        UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                    End If
                Next

                For ii = 0 To UnicosId.Count - 1

                    dt.Clear()
                    cFunciones.Llenar_Tabla_Generico("select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,TiempoTranscurrido,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,NumComanda, Color from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal=" & UnicosId(ii) & " and tb_DetalleOrden.Estado=1 order by codMesa asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                    Dim aButton As New ucBebidas
                    aButton.Top = y
                    aButton.Left = x
                    aButton.Width = 326
                    aButton.Height = LargoUc + 6
                    aButton.pnDetalle.Height = LargoUc
                    aButton.Max = LargoUc
                    aButton.Name = "Mesa_" & dt.Rows(0).Item("CodMesa") & "_" & dt.Rows(0).Item("IdOrden") & "_Listo" & "_" & dt.Rows(0).Item("NumComanda")
                    aButton.NumMesa = "Mesa #" & dt.Rows(0).Item("NombreMesa")
                    aButton.Tomado = dt.Rows(0).Item("TiempoTranscurrido")
                    aButton.IdMesa = dt.Rows(0).Item("codMesa")
                    aButton.IdCentro = dt.Rows(0).Item("IdCentroProduccion")
                    aButton.IdOrden = dt.Rows(0).Item("IdOrden")
                    aButton.txtnumMesa.BackColor = Color.FromName(Convert.ToString(dt.Rows(0).Item("Color")).Replace("Color [", "").Replace("]", ""))

                    aButton.frm = Me

                    PanelSeleccionados.Controls.Add(aButton)
                    x = x + 328
                Next

            End If

            PanelSeleccionados.Visible = True
            If IdMesa <> 0 Then
                CargarCentros()
                CargarResumen()
            End If
        Catch ex As Exception
            MsgBox("Error al crear mesas. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub DevolverListos(Optional IdMesa As Integer = 0, Optional IdOrden As Integer = 0)
        Try
            PanelSeleccionados.Visible = False

            Dim cx As New Conexion
            Dim Sql As String = ""

            If IdMesa <> 0 Then
                Sql = "Update tb_DetalleOrden set ParaEntregar=0,Entregado=0 where CodMesa=" & IdMesa & " and IdOrden=" & IdOrden & " and Estado=1"
                cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)
            End If

            PanelSeleccionados.Visible = True
            If IdMesa <> 0 Then
                CargarCentros()
                CargarResumen()
                CargarListos()
            End If
        Catch ex As Exception
            MsgBox("Error al regresar mesa al panel superior. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub EliminarListos(Optional IdMesa As Integer = 0, Optional IdOrden As Integer = 0)
        Try

            PanelSeleccionados.Visible = False

            Dim cx As New Conexion
            Dim Sql As String = ""

            If IdMesa <> 0 Then
                Sql = "Update tb_DetalleOrden set Estado=0 where CodMesa=" & IdMesa & " and IdOrden=" & IdOrden & ""
                cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)
            End If

            PanelSeleccionados.Visible = True
            If IdMesa <> 0 Then
                CargarResumen()
                CargarListos()
            End If
        Catch ex As Exception
            MsgBox("Error al regresar mesa al panel superior. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Function RandomColor() As Color
        Dim random As New Random
        Dim ColorDevuelto As Color
        Dim MiColor As String = ""
        Dim dt As New DataTable


        Try

OtraVez:
            ColorDevuelto = Color.FromName(ListaColores(random.Next(0, 35)))
            MiColor = ColorDevuelto.ToString()
            MiColor = MiColor.Replace("Color [", "").Replace("]", "")
            'ColorDevuelto = Color.FromName(MiColor.Replace("Color [", "").Replace("]", ""))

            If IsNumeric(MiColor) Then
                GoTo OtraVez
            End If

            Dim sql As New SqlCommand("Select IdOrden from tb_Orden with (Nolock) where Color='" & ColorDevuelto.ToString() & "' and  Estado=1 ")

            cFunciones.spCargarDatos(sql, dt)

            If dt.Rows.Count > 0 Then
                GoTo OtraVez
            End If

            Return ColorDevuelto

        Catch ex As Exception
            GoTo OtraVez
        End Try

    End Function

    Public Sub ActualizarOrdenes()
        Try

            PanelMesas.Visible = False

            Dim dtNuevos As New DataTable()
            Dim dtOrden As New DataTable()
            Dim IdOrden As Integer
            Dim Entrar As Integer = 0

            dtNuevos.Clear()
            dtOrden.Clear()
            'cFunciones.Llenar_Tabla_Generico("Select * from vs_CentroProduccionConId where IdCentro=" & IdCentro & "", dtNuevos, GetSetting("Seesoft", "Restaurante", "Conexion"))

            cFunciones.Llenar_Tabla_Generico("EXEC[dbo].[DevolverComandas] @IdCentro = " & IdCentro & "", dtNuevos, GetSetting("Seesoft", "Restaurante", "Conexion"))

            For n As Integer = 0 To dtNuevos.Rows.Count - 1

                If dtNuevos.Rows(n).Item("IdCentro") = IdCentro Then
                    Entrar = 1
                    Exit For
                End If

            Next

            If Entrar = 1 Then

                Dim cx As New Conexion
                Dim Sql As String = ""
                Dim dtNumOrden As New DataTable
                Dim NumOrden As Integer = 0
                dtNumOrden.Clear()

                cFunciones.Llenar_Tabla_Generico("Select isnull( Max(NumOrden),0) as NumOrden from tb_Orden with (Nolock) where Estado=1", dtNumOrden)

                If CDbl(dtNumOrden.Rows(0).Item("NumOrden")) = 0 Then
                    NumOrden = 1
                Else
                    NumOrden = CDbl(dtNumOrden.Rows(0).Item("NumOrden")) + 1
                End If

                Try
                    My.Computer.Audio.Play(My.Resources.Campana, AudioPlayMode.Background)
                Catch ex As Exception

                End Try

                Sql = "Insert Into tb_Orden ([FechaIngreso],[IdCentroProduccion],[Color],[Estado],[NumOrden],[TiempoTranscurrido]) Values (GETDATE()," & IdCentro & ",'" & RandomColor().ToString() & "',1," & NumOrden & ",'0:0:0')"
                cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)

                cFunciones.Llenar_Tabla_Generico("Select Max(IdOrden) as IdOrden from tb_Orden with (Nolock)", dtOrden, GetSetting("Seesoft", "Restaurante", "Conexion"))

                IdOrden = dtOrden.Rows(0).Item("IdOrden")

                For h As Integer = 0 To dtNuevos.Rows.Count - 1
                    If dtNuevos.Rows(h).Item("IdCentro") = IdCentro Then
                        Dim Express As Integer = 0

                        If dtNuevos.Rows(h).Item("Express") = "False" Then
                            Express = 0
                        Else
                            Express = 1
                        End If

                        Sql = "Insert Into tb_DetalleOrden ([IdOrden],[IdComandaTemporal],[Cantidad],[Descripcion],[CodProducto],[Estado],[IdCentroProduccion],[CodMesa],[NombreMesa],[CantidadDespachada],[ParaEntregar],[Prioridad],[Express],[EstadoComanda],[Entregado],[Agrupar],[NumComanda],[FueImpresa]) 
                            Values (" & IdOrden & "," & dtNuevos.Rows(h).Item("Id") & "," & dtNuevos.Rows(h).Item("cCantidad") & ",'" & dtNuevos.Rows(h).Item("cDescripcion") & "'," & dtNuevos.Rows(h).Item("cProducto") & ",1," & dtNuevos.Rows(h).Item("IdCentro") & "," & dtNuevos.Rows(h).Item("cMesa") & ",'" & dtNuevos.Rows(h).Item("Nombre_Mesa") & "',0,0," & dtNuevos.Rows(h).Item("Primero") & "," & Express & ",1,0,'" & dtNuevos.Rows(h).Item("Agrupar") & "'," & dtNuevos.Rows(h).Item("cCodigo") & ",0)"
                        cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                        cx.DesConectar(cx.sQlconexion)
                    End If
                Next
            End If

            GuardarTiempoTranscurrido()
            CargarCentros()
            CargarResumen()

            PanelMesas.Visible = True
        Catch ex As Exception
            MsgBox("Error al actualizar ordenes. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub GuardarTiempoTranscurrido()
        For Each control As ucBebidas In PanelMesas.Controls
            control.GuardarTiempoTranscurrido()
        Next
    End Sub

    Private Sub spRefrescarMesas()
        Try
            If DateDiff(DateInterval.Second, TiempoInicial, Now) <= TiempoEspera Then
                lbTimer.Text = TiempoEspera - DateDiff(DateInterval.Second, TiempoInicial, Now) + 1
            Else
                If lbTimer.Text <> "Actualizando..." Then
                    TiempoInicial = Now
                    lbTimer.Text = "Actualizando..."
                    ActualizarOrdenes()
                ElseIf DateDiff(DateInterval.Second, TiempoInicial, Now) >= TiempoEspera Then
                    TiempoInicial = Now
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Timer1_Tick", ex.Message)
        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        spRefrescarMesas()
    End Sub

    Private Sub frmCentroProduccionBebidas_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Me.Timer1.Enabled = False
        Me.tmHora.Stop()
    End Sub

    Private Sub btActualizar_Click(sender As Object, e As EventArgs) Handles btActualizar.Click
        ActualizarOrdenes()
    End Sub

    Private Sub btLimpiar_Click(sender As Object, e As EventArgs) Handles btLimpiar.Click
        LimpiarPanelListos()
    End Sub

    Private Sub LimpiarPanelListos()
        Try
            PanelSeleccionados.Visible = False

            Dim cx As New Conexion
            Dim Sql As String = ""
            Dim IdMesa As Integer = 0
            Dim IdOrden As Integer = 0
            Dim Info As String = ""
            Dim _info() As String
            Dim Descripcion As String = ""
            Dim Estado As String = ""
            Dim NumComanda As Integer = 0

            For Each control As ucBebidas In PanelSeleccionados.Controls
                Info = control.Name
                _info = Info.Split("_")

                If _info.Count > 0 Then
                    IdMesa = Convert.ToString(_info(1)).Trim()
                    IdOrden = Convert.ToString(_info(2)).Trim()
                    Estado = Convert.ToString(_info(3)).Trim()
                    NumComanda = Convert.ToInt32(_info(4).Trim())
                    Sql = "Update tb_DetalleOrden set Estado=0 where CodMesa=" & IdMesa & " and IdOrden=" & IdOrden & " and NumComanda=" & NumComanda & ""
                    cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                    cx.DesConectar(cx.sQlconexion)
                End If
            Next
            PanelSeleccionados.Visible = True
            PanelSeleccionados.Controls.Clear()
            ActualizarOrdenes()
        Catch ex As Exception
            ' MsgBox("Error al limpiar mesas del panel listos. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub



    Public Function CalcularLargoUc(ByVal QuienLlama As String) As Integer
        Try
            Dim dt As New DataTable
            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList
            Dim Max As Integer = 0
            Dim sqlDetalle As String = ""
            Dim SqlGrupo As String = ""

            Select Case QuienLlama
                Case "Central"
                    SqlGrupo = "select CONVERT(varchar, tb_DetalleOrden.IdOrden)+'-'+CONVERT(varchar, CodMesa) as CodMesa,IdComandaTemporal,NumOrden from dbo.tb_DetalleOrden with (Nolock) right outer join tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden 
            where tb_detalleOrden.IdCentroProduccion=" & IdCentro & " and tb_DetalleOrden.Estado=1 and ParaEntregar=0 order by NumOrden asc"
                    sqlDetalle = "select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,FechaIngreso,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal="
                Case "Listos"
                    SqlGrupo = "select IdComandaTemporal,CONVERT(varchar, tb_DetalleOrden.IdOrden)+'-'+CONVERT(varchar,tb_DetalleOrden.CodMesa) as CodMesa from dbo.tb_DetalleOrden with (Nolock)
                        where tb_detalleOrden.ParaEntregar = 1 And Estado = 1 And IdCentroProduccion = " & IdCentro & " order by codMesa asc"
                    sqlDetalle = "select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,FechaIngreso,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar, Color from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal="
            End Select
            dt.Clear()

            cFunciones.Llenar_Tabla_Generico(SqlGrupo, dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not unicos.Contains(dt.Rows(k).Item("codMesa")) Then
                        unicos.Add(dt.Rows(k).Item("codMesa"))
                        UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                    End If
                Next

                Dim _Filas As Integer = UnicosId.Count

                dt.Clear()
                For o As Integer = 0 To UnicosId.Count - 1
                    cFunciones.Llenar_Tabla_Generico(sqlDetalle & UnicosId(o) & "", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                    If VerificarDetalles(Convert.ToInt32(dt.Rows(0).Item("IdOrden")), Convert.ToInt32(dt.Rows(0).Item("codMesa"))) Then
                        If Max < CantDetalles Then
                            Max = CantDetalles
                        End If
                    End If
                Next


            End If

            Max = Max + 2
            Return Max * 25 '25 es el ancho del textbox que contiene las descripciones de las comandas en los user control

        Catch ex As Exception
            Return 125
        End Try
    End Function

    Public Function VerificarDetalles(ByVal _IdOrden As Integer, ByVal Mesa As Integer) As Boolean
        Try
            Dim dt As New DataTable

            dt.Clear()

            cFunciones.Llenar_Tabla_Generico("Select IdComandaTemporal,codProducto,Cantidad,Descripcion,Despachado,Prioridad from tb_DetalleOrden with (Nolock) where IdCentroProduccion=" & IdCentro & " and codMesa=" & Mesa & " and IdOrden=" & _IdOrden & " order by Prioridad desc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then
                CantDetalles = dt.Rows.Count
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub tmHora_Tick(sender As Object, e As EventArgs) Handles tmHora.Tick
        lbHora.Text = DateTime.Now.ToString("hh:mm:ssss")
    End Sub
End Class