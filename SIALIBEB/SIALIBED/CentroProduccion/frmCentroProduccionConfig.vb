﻿Public Class frmCentroProduccionConfig

    Dim FiltroEstado As Integer = 1
    Dim PrimeraVez As Integer = 0

    Private Sub frmCentroProduccion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarGrid()
        PrimeraVez = 1
    End Sub

    Private Sub MuestraForm(ByRef FormDialog As Form)
        Hide()
        FormDialog.ShowDialog(Me)
        CargarGrid()
        Show()
    End Sub

    Sub BuscarCentro()
        Try

            Dim consulta As String = ""

            If txtBuscar.Text.Trim() = "" Then
                consulta = "Select Id as 'Código', NombreCentro as 'Nombre', TiempoMin as 'Tiempo por Orden',TiempoPrioridad as 'T. Prioridad', Tipo, Estado from tb_CentroProduccion where  Estado=" & FiltroEstado & ""
            Else
                consulta = "Select Id as 'Código', NombreCentro as 'Nombre', TiempoMin as 'Tiempo por Orden',TiempoPrioridad as 'T.Prioridad', Tipo, Estado from tb_CentroProduccion where NombreCentro like '%" & txtBuscar.Text & "%' and Estado=" & FiltroEstado & ""
            End If

            Dim dtBuscar As New DataTable()

            cFunciones.Llenar_Tabla_Generico(consulta, dtBuscar, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dtBuscar.Rows.Count > 0 Then
                dgvCentroProduccion.DataSource = dtBuscar
                dgvCentroProduccion.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
            Else
                MsgBox("No hay centros de producción para mostrar", MsgBoxStyle.Information)
            End If


        Catch ex As Exception
            MsgBox("Error al buscar centro de producción. " & +ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub CargarGrid()
        Try
            If PrimeraVez <> 0 Then
                Dim dt As New DataTable()

                cFunciones.Llenar_Tabla_Generico("Select Id as 'Código', NombreCentro as 'Nombre', TiempoMin as 'Tiempo por Orden',TiempoPrioridad as 'T. Prioridad', Tipo, Estado from tb_CentroProduccion where Estado=1", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                If dt.Rows.Count > 0 Then
                    dgvCentroProduccion.DataSource = dt
                    dgvCentroProduccion.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                Else
                    MsgBox("No hay centros de producción para mostrar", MsgBoxStyle.Information)
                End If
            End If
        Catch ex As Exception
            MsgBox("Error al cargar grid. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btNuevo_Click(sender As Object, e As EventArgs) Handles btNuevo.Click
        MuestraForm(New frmCentroProduccionNuevo)
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        BuscarCentro()
    End Sub

    Private Sub btEditar_Click(sender As Object, e As EventArgs) Handles btEditar.Click
        Try

            If dgvCentroProduccion.SelectedRows.Count > 0 Then
                Dim Id As Integer = dgvCentroProduccion.CurrentRow.Cells(0).Value
                Dim frmNuevo As New frmCentroProduccionNuevo
                frmNuevo.Id = Id
                MuestraForm(frmNuevo)
            Else
                MsgBox("Selecciona una fila para editar la información.", MsgBoxStyle.Information)
            End If

        Catch ex As Exception
            MsgBox("Error al seleccionar centro de producción. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub rbActivos_CheckedChanged(sender As Object, e As EventArgs) Handles rbActivos.CheckedChanged
        If rbActivos.Checked Then
            FiltroEstado = 1
            BuscarCentro()
        End If
    End Sub

    Private Sub rbInactivos_CheckedChanged(sender As Object, e As EventArgs) Handles rbInactivos.CheckedChanged
        If rbInactivos.Checked Then
            FiltroEstado = 0
            BuscarCentro()
        End If
    End Sub

End Class