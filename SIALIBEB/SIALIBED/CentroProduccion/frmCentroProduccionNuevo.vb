﻿Public Class frmCentroProduccionNuevo

    Public Id As Integer = 0

    Private Sub frmCentroProduccionNuevo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarCbTipo()
        CargarCbEstado()

        If Id <> 0 Then
            Dim dtEditar As New DataTable()
            cFunciones.Llenar_Tabla_Generico("Select * from tb_CentroProduccion where Id=" & Id & "", dtEditar, GetSetting("Seesoft", "Restaurante", "Conexion"))

            Dim Tipo As String = ""

            If dtEditar.Rows.Count > 0 Then

                txtNombreCentro.Text = dtEditar.Rows(0).Item("NombreCentro")
                numDuracion.Value = dtEditar.Rows(0).Item("TiempoMin")
                Tipo = dtEditar.Rows(0).Item("Tipo")
                numPrioridad.Value = dtEditar.Rows(0).Item("TiempoPrioridad")

                Select Case Tipo
                    Case "Bebidas"
                        cbTipo.SelectedIndex = 0
                    Case "Cocina"
                        cbTipo.SelectedIndex = 1
                End Select

                If dtEditar.Rows(0).Item("Estado") = "True" Then
                    cbEstado.SelectedIndex = 1
                Else
                    cbEstado.SelectedIndex = 0
                End If

            End If
        Else
            cbTipo.SelectedIndex = 0
            cbEstado.SelectedIndex = 1
        End If

    End Sub

    Private Sub btCancelar_Click(sender As Object, e As EventArgs) Handles btCancelar.Click
        Me.Close()
    End Sub

    Sub CargarCbTipo() 'Cargar el combobox de tipos
        Try
            cbTipo.Items.Insert(0, "Bebidas")
            cbTipo.Items.Insert(1, "Cocina")
        Catch ex As Exception
            MsgBox("Error al cargar cb de Tipos. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub CargarCbEstado() 'Carga el combobox de estados
        Try
            cbEstado.Items.Insert(0, "Inactivo")
            cbEstado.Items.Insert(1, "Activo")
        Catch ex As Exception
            MsgBox("Error al cargar cb de Tipos. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub Guardar() 'funcion para guardar en la base de datos
        Try

            Dim cx As New Conexion
            Dim Sql As String = ""

            If Id <> 0 Then
                Sql = "Update tb_CentroProduccion set [NombreCentro]='" & txtNombreCentro.Text & "'
           ,[TiempoMin]=" & numDuracion.Value & "
           ,[Tipo]='" & cbTipo.Text & "'
           ,[Estado]=" & cbEstado.SelectedIndex & "
           ,[TiempoPrioridad]=" & numPrioridad.Value & " where Id=" & Id & ""

            Else
                Sql = "INSERT INTO tb_CentroProduccion([NombreCentro]
           ,[TiempoMin]
           ,[Tipo]
           ,[Estado],[TiempoPrioridad])" &
           " VALUES( '" & txtNombreCentro.Text & "'," & numDuracion.Value & ",'" & cbTipo.Text & "'," & cbEstado.SelectedIndex & "," & numPrioridad.Value & ")"

            End If

            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)

            cx.DesConectar(cx.sQlconexion)

        Catch ex As Exception
            MsgBox("Error en procedimiento de guardado. " & ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btNuevo_Click(sender As Object, e As EventArgs) Handles btNuevo.Click

        Try
            Guardar()
            MsgBox("La infomación se guardo correctamente", MsgBoxStyle.Information)
            Me.Close()
        Catch ex As Exception
            MsgBox("La infomación no se guardo correctamente. " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub cbTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipo.SelectedIndexChanged
        If cbTipo.SelectedIndex = 1 Then
            lbPrioridad.Visible = True
            lbPrioridad2.Visible = True
            numPrioridad.Visible = True
        Else
            lbPrioridad.Visible = False
            lbPrioridad2.Visible = False
            numPrioridad.Visible = False
            numPrioridad.Value = 0
        End If
    End Sub
End Class