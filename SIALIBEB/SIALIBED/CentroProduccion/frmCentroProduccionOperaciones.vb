﻿Imports System.IO

Public Class frmCentroProduccionOperaciones
    Dim PMU As New PerfilModulo_Class
    Dim NumCentros As Integer = 0
    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlClient.SqlConnection
    Dim rs As SqlClient.SqlDataReader
    Dim dt As New DataTable
    Dim id As String = ""
    Dim cambio As Boolean = False
    Dim posicion As String = "Origen"

    Private Sub frmCentroProduccionOperaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        CargarCentros()
    End Sub

    Private Sub CargarCentros()
        Try

            Dim i, ii, X, Y, n As Integer
            Dim Filas As Integer = 0
            Dim UnicosId As New ArrayList
            Dim dt As New DataTable
            Dim Columnas As Integer = 10

            PanelCentros.Controls.Clear()

            cFunciones.Llenar_Tabla_Generico("SELECT Id, NombreCentro FROM tb_CentroProduccion where Estado=1 order by Id asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then


                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not UnicosId.Contains(dt.Rows(k).Item("Id")) Then
                        UnicosId.Add(dt.Rows(k).Item("Id"))
                    End If
                Next

                Dim _Filas As Integer = UnicosId.Count

                For b As Integer = 0 To _Filas 'Calcula cuantas filas son necesarias para mostrar toda la info
                    If _Filas >= 1 Then
                        _Filas = _Filas - Columnas 'modificar para aumentar o disminuir la cantidad a mostrar por filas, 1 = mostrar uno por fila
                        Filas += 1
                    Else
                        If b = 0 Then
                            Filas = 1
                        End If
                    End If
                Next

                Dim quitar As Integer = 0
                Dim recorrer As Integer = Columnas - 1 'Cuantas columnas deseas 0 = 1 columna, 1 = 2 columnas, etc

                If Filas = 1 Then
                    recorrer = UnicosId.Count - 1
                End If

                For i = 0 To Filas - 1
                    For ii = 0 To recorrer

                        dt.Clear()
                        cFunciones.Llenar_Tabla_Generico("SELECT Id,NombreCentro FROM tb_CentroProduccion where Id=" & UnicosId(ii) & "", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        quitar = ii

                        Dim aButton As New Button
                        aButton.Top = Y
                        aButton.Left = X
                        aButton.Width = 120
                        aButton.Height = 80
                        aButton.Name = n & "-" & dt.Rows(0).Item("Id")
                        aButton.Text = dt.Rows(0).Item("NombreCentro")
                        aButton.BackColor = Color.DimGray
                        aButton.ForeColor = Color.LightSkyBlue
                        AddHandler aButton.Click, AddressOf Me.Clickbuttons
                        Me.PanelCentros.Controls.Add(aButton)
                        X = X + 122
                        n = n + 1
                    Next

                    If UnicosId.Count > 0 Then
                        For g As Integer = 0 To quitar
                            Dim a As Integer = 0
                            UnicosId.RemoveAt(a)
                        Next
                    End If


                    If UnicosId.Count <= Columnas Then
                        recorrer = UnicosId.Count - 1
                    End If

                    Y = Y + 82
                    X = 0
                Next
            Else
                MsgBox("No hay centros de producción para mostrar.", MsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MuestraForm(ByRef FormDialog As Form)
        Hide()
        FormDialog.ShowDialog(Me)
        Show()
    End Sub

    Private Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim datos As Array
            datos = CStr(sender.name).Split("-")
            If datos.Length > 1 Then
                id = datos(1)

                If id <> 0 Then
                    Dim dt As New DataTable()
                    Dim frmBebidas As New frmCentroProduccionBebidas
                    Dim frmCocina As New frmCentroProduccionCocina

                    cFunciones.Llenar_Tabla_Generico("SELECT NombreCentro,Tipo,TiempoMin,TiempoExtra,TiempoPrioridad FROM tb_CentroProduccion where Id=" & id & "", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                    If dt.Rows.Count > 0 Then
                        Select Case dt.Rows(0).Item("Tipo").ToString()
                            Case "Bebidas"
                                frmBebidas.IdCentro = id
                                frmBebidas.TiempoEspera = CDbl(dt.Rows(0).Item("TiempoMin"))
                                frmBebidas.NombreCentro = dt.Rows(0).Item("NombreCentro")
                                MuestraForm(frmBebidas)
                            Case "Cocina"
                                frmCocina.IdCentro = id
                                frmCocina.TiempoEspera = CDbl(dt.Rows(0).Item("TiempoMin"))
                                frmCocina.NombreCentro = dt.Rows(0).Item("NombreCentro")
                                frmCocina.TiempoExtra = CDbl(dt.Rows(0).Item("TiempoExtra"))
                                frmCocina.TiempoPrioridad = CDbl(dt.Rows(0).Item("TiempoPrioridad"))
                                MuestraForm(frmCocina)
                        End Select
                    End If
                End If
            End If
            'PMU = VSM(cedula, Me.Name)
            'If PMU.Find Then
        Catch ex As Exception
            MsgBox("Error al leer Id del Centro de Producción. " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
End Class