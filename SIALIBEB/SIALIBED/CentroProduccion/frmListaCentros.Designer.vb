﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListaCentros
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.clCentrosProduccion = New System.Windows.Forms.CheckedListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btGuardar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'clCentrosProduccion
        '
        Me.clCentrosProduccion.FormattingEnabled = True
        Me.clCentrosProduccion.Location = New System.Drawing.Point(12, 37)
        Me.clCentrosProduccion.Name = "clCentrosProduccion"
        Me.clCentrosProduccion.Size = New System.Drawing.Size(240, 124)
        Me.clCentrosProduccion.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(350, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Selecciona los centros de producción que deseas  vincular a esté menú."
        '
        'btGuardar
        '
        Me.btGuardar.Location = New System.Drawing.Point(269, 80)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btGuardar.TabIndex = 3
        Me.btGuardar.Text = "Guardar"
        Me.btGuardar.UseVisualStyleBackColor = True
        '
        'frmListaCentros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(366, 171)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.clCentrosProduccion)
        Me.MaximizeBox = False
        Me.Name = "frmListaCentros"
        Me.Text = "Centros de producción"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents clCentrosProduccion As CheckedListBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btGuardar As Button
End Class
