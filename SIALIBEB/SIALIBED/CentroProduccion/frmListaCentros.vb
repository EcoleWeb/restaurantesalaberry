﻿Public Class frmListaCentros

    Public IdMenu As Integer = 0

    Private Sub frmListaCentros_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarLista()
    End Sub

    Sub CargarLista()
        Try
            Dim dt As New DataTable()
            Dim dtUnion As New DataTable()
            Dim dtNoEsta As New DataTable()

            cFunciones.Llenar_Tabla_Generico("Select Id,NombreCentro from tb_CentroProduccion where Estado=1 order by Id asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))
            cFunciones.Llenar_Tabla_Generico("Select IdCentroProduccion from tb_UnionMenuRCentroProduccion where IdMenu=" & IdMenu & " order by IdCentroProduccion asc", dtUnion, GetSetting("Seesoft", "Restaurante", "Conexion"))
            cFunciones.Llenar_Tabla_Generico("Select NombreCentro from tb_CentroProduccion where Estado=1 and Id not in (Select IdCentroProduccion from tb_UnionMenuRCentroProduccion where IdMenu=" & IdMenu & ")", dtNoEsta, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dtUnion.Rows.Count > 0 Then

                For b As Integer = 0 To dtUnion.Rows.Count - 1
                    For j As Integer = 0 To dt.Rows.Count - 1
                        If dtUnion.Rows(b).Item("IdCentroProduccion") = dt.Rows(j).Item("Id") Then
                            clCentrosProduccion.Items.Add(dt.Rows(j).Item("NombreCentro"), True)
                        End If
                    Next
                Next

                EliminarRepetidos(clCentrosProduccion)

                For c As Integer = 0 To dtNoEsta.Rows.Count - 1
                    clCentrosProduccion.Items.Add(dtNoEsta.Rows(c).Item("NombreCentro"), False)
                Next

            Else
                If dt.Rows.Count > 0 Then
                    For b As Integer = 0 To dt.Rows.Count - 1
                        clCentrosProduccion.Items.Add(dt.Rows(b).Item("NombreCentro"), False)
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox("Error al cargar lista de centros. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub EliminarRepetidos(ByVal LB As CheckedListBox)
        Dim i As Int32
        Dim j As Int32

        ' Recorre los items ( compara empezando _  
        'desde el primero , de abajo hacia arriba)  
        For i = 0 To LB.Items.Count - 2
            For j = LB.Items.Count - 1 To i + 1 Step -1
                ' ... si es el mismo  
                If LB.Items(i).ToString = LB.Items(j).ToString Then
                    LB.Items.RemoveAt(j)
                End If
            Next
        Next
    End Sub

    Sub GuardarCambio(ByVal guardar As String, ByVal check As Boolean)
        Try
            Dim dtId As New DataTable()
            Dim cx As New Conexion
            Dim Sql As String = ""

            cFunciones.Llenar_Tabla_Generico("Select Id from tb_CentroProduccion where NombreCentro='" & guardar & "'", dtId, GetSetting("Seesoft", "Restaurante", "Conexion"))
            If dtId.Rows.Count > 0 Then
                Dim dtExiste As New DataTable()
                Dim IdUnion As Integer = 0
                Dim IdCentro As Integer = dtId.Rows(0).Item("Id")

                cFunciones.Llenar_Tabla_Generico("Select Id from tb_UnionMenuRCentroProduccion where IdMenu=" & IdMenu & " and IdCentroProduccion=" & IdCentro & "", dtExiste, GetSetting("Seesoft", "Restaurante", "Conexion"))

                If dtExiste.Rows.Count > 0 Then
                    IdUnion = dtExiste.Rows(0).Item("Id")
                    If Not check Then
                        Sql = "Delete from tb_UnionMenuRCentroProduccion where Id=" & IdUnion & ""
                        cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                        cx.DesConectar(cx.sQlconexion)
                    End If
                Else
                    If check Then
                        Sql = "Insert Into tb_UnionMenuRCentroProduccion ([IdMenu],[IdCentroProduccion]) Values (" & IdMenu & "," & IdCentro & ")"
                        cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                        cx.DesConectar(cx.sQlconexion)
                    End If

                End If

            End If

        Catch ex As Exception
            MsgBox("Error al guardar los cambios. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub



    Private Sub btGuardar_Click(sender As Object, e As EventArgs) Handles btGuardar.Click
        Try
            Dim i As Integer
            Dim Valor As String
            Dim check As Boolean

            For i = 0 To clCentrosProduccion.Items.Count - 1

                check = clCentrosProduccion.GetItemChecked(i)
                Valor = clCentrosProduccion.Items(i).ToString()
                GuardarCambio(Valor, check)

            Next
            MsgBox("Cambios guardados con éxito.", MsgBoxStyle.Information)
            Me.Close()
        Catch ex As Exception
            MsgBox("Error al guardar cambios. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

End Class