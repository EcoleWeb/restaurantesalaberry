Imports System.Data.SqlClient

Public Class cls_Broker


    Dim StringConexion As String
    Dim stringcone As String

    'Constructor de la clase..
    Public Sub New()
        'Inicializamos el string de conexion..
        Me.StringConexion = GetSetting("SeeSOFT", "Restaurante", "Conexion")
        Me.stringcone = GetSetting("SeeSoft", "Hotel", "Conexion")

    End Sub



    'Esta funcion ejecuta una sentencia SQL con retorno..
    Public Function fireSQL(ByRef _sql As String) As DataTable
        Dim ConexionX As SqlConnection = New SqlConnection(Me.StringConexion)
        Dim Tabla As New DataTable
        Dim Comando As SqlCommand = New SqlCommand
        Dim DataAdapter As New Data.SqlClient.SqlDataAdapter()
        Try
            ConexionX.Open()
            Comando.CommandText = _sql
            Comando.Connection = ConexionX
            Comando.CommandType = CommandType.Text
            Comando.CommandTimeout = 90
            DataAdapter.SelectCommand = Comando
            DataAdapter.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.ToString) ' Si hay error, devolvemos un valor nulo.
            Return Nothing
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
        Return Tabla ' Devolvemos el objeto DataTable con los datos de la consulta
    End Function

    ' Esta funcion ejecuta una sentencia SQL que no espera retorno..
    Public Function fireSQLNoReturn(ByRef _sql As String) As String
        Dim Command As SqlCommand
        Dim Mensaje As String = ""
        Dim ConexionX As SqlConnection = New SqlConnection(Me.StringConexion)
        ConexionX.Open()
        Command = New SqlCommand(_sql, ConexionX)
        Try
            Command.ExecuteNonQuery()
        Catch ex As Exception
            Mensaje = ex.Message
        Finally
            Command.Dispose()
            Command = Nothing
        End Try
        Return Mensaje
    End Function


    ''**********************************************************************************************

    ' esta funcion ejecuta una sentencia SQL y recive un string de conexion que no espera retorno
    Public Function fireSQL_CnConexionNoReturn(ByRef _sql As String, ByVal _StringConexion As String) As String
        Dim Command As SqlCommand
        Dim Mensaje As String = ""
        Dim ConexionX As SqlConnection = New SqlConnection(_StringConexion)
        ConexionX.Open()
        Command = New SqlCommand(_sql, ConexionX)
        Try
            Command.ExecuteNonQuery()
        Catch ex As Exception
            Mensaje = ex.Message
        Finally
            Command.Dispose()
            Command = Nothing
        End Try
        Return Mensaje
    End Function

    'Esta funcion ejecuta una sentencia SQL con retorno..
    Public Function fireSQL_Hotel(ByRef _sql As String) As DataTable
        Dim ConexionX As SqlConnection = New SqlConnection(Me.stringcone)
        Dim Tabla As New DataTable
        Dim Comando As SqlCommand = New SqlCommand
        Dim DataAdapter As New Data.SqlClient.SqlDataAdapter()
        Try
            ConexionX.Open()
            Comando.CommandText = _sql
            Comando.Connection = ConexionX
            Comando.CommandType = CommandType.Text
            Comando.CommandTimeout = 90
            DataAdapter.SelectCommand = Comando
            DataAdapter.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.ToString) ' Si hay error, devolvemos un valor nulo.
            Return Nothing
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
        Return Tabla ' Devolvemos el objeto DataTable con los datos de la consulta
    End Function



End Class


