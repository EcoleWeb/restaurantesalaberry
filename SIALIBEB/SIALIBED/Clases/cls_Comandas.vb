'Esta clase 

Imports System.Data.SqlClient
Public Class cls_Comandas

    Dim f As New cls_Broker()

    Public idComanda As String
    Public numeroComanda As String
    Public fecha As String
    Public Subtotal As String
    Public impuesto_venta As String
    Public total As String
    Public cedusuario As String
    Public cedsalonero As String
    Public idMesa As String
    Public comenzales As String
    Public facturado As String
    Public numerofactura As String
    Public cuentacontable As String
    Public contabilizado As String
    Public cortesia As String
    Public anulado As String
    Public numeroCortesia As String
    Public cod_moneda As String

    Public Sub New() 'Constructor de la clase...
        Me.idComanda = ""
        Me.numeroComanda = ""
        Me.fecha = ""
        Me.Subtotal = ""
        Me.impuesto_venta = ""
        Me.total = ""
        Me.cedusuario = ""
        Me.cedsalonero = ""
        Me.idMesa = ""
        Me.comenzales = ""
        Me.facturado = ""
        Me.numerofactura = ""
        Me.cuentacontable = ""
        Me.contabilizado = ""
        Me.cortesia = ""
        Me.anulado = ""
        Me.numeroCortesia = ""
        Me.cod_moneda = ""
    End Sub


    'Agregamos una nueva comanda..
    Public Sub nuevoComanda()
        Dim str As String

        str = "insert into Comanda (NumeroComanda, Fecha, Subtotal, Impuesto_venta, Total, Cedusuario, CedSalonero, IdMesa, Comenzales, Facturado, Numerofactura,CuentaContable,cortesia ,NumeroCortesia, Cod_Moneda) values " & _
        "('" & Me.numeroComanda & "',getDate(),'" & Me.Subtotal & "','" & Me.impuesto_venta & "','" & Me.total & "','" & Me.cedusuario & "','" & Me.cedsalonero & "','" & Me.idMesa & "','" & Me.comenzales & "','" & Me.facturado & "','" & Me.numerofactura & "','" & Me.cuentacontable & "','" & Me.cortesia & "','" & Me.numeroCortesia & "','" & Me.cod_moneda & "')"
        Me.f.fireSQLNoReturn(str)

    End Sub

    'borramos la comanda temporal...
    Public Sub borrarComandaTemporal(ByRef _numeroSeparado As String, ByRef _numeroComanda As String)
        Dim str As String
        str = "delete ComandaTemporal  WHERE cCodigo= " & _numeroComanda & " and separada= " & _numeroSeparado
        Me.f.fireSQLNoReturn(str)
    End Sub

    'Obtenemos el ultimo id de comanda generado..
    Public Function obtenerUltimoIdComandaGenerado() As String
        Dim str As String
        str = "select max(idComanda) from comanda "
        Return Me.f.fireSQL(str).Rows(0)(0)
    End Function


    ' Ingresamos un nuevo registro a detalleMenuComanda....
    Public Sub nuevoDetalleMenuComanda(ByVal _idComanda As String, ByVal _idMenu As String, ByVal _Cantidad As String, ByVal _PrecioUnitario As String, ByVal _Subtotal As String, ByVal _ImpVenta As String, ByVal _impServicio As String, ByVal _ced_Salonero As String, ByVal _costo_real As String)

        Dim str As String

        str = "insert into DetalleMenuComanda (idComanda, idMenu, Cantidad, PrecioUnitario, Subtotal, ImpVenta, impServicio, ced_Salonero, hora, costo_real) " & _
        " values ('" & _idComanda & "','" & _idMenu & "','" & _Cantidad & "','" & _PrecioUnitario & "','" & _Subtotal & "','" & _ImpVenta & "','" & _impServicio & "','" & _ced_Salonero & "',getDate(),'" & _costo_real & "')"

        Me.f.fireSQLNoReturn(str)

    End Sub



    'Esta funcion nos devuelve todas las impresoras configuradas para imprimir comandas..
    Public Function obtenerTodasLasImpresoras() As DataTable
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select  distinct impresora from menu_Restaurante ")
        Return dt
    End Function

    'Esta funcion devuelve el punto de venta..
    Public Function obtenerPuntoVenta() As String
        Dim lastpoint As String = GetSetting("SeeSOFT", "Restaurante", "LastPoint")
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select idpuntoventa from puntoventa where nombre ='" + lastpoint + "'")
        Dim a As String = dt.Rows(0).Item(0).ToString
        Return a
    End Function

End Class
