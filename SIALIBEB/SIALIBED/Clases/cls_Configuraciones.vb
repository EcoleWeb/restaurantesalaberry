Public Class cls_Configuraciones


    Public Cedula As String
    Public Empresa As String
    Public Tel_01 As String
    Public Tel_02 As String
    Public Fax_01 As String
    Public Fax_02 As String
    Public Direccion As String
    Public Imp_Venta As String
    Public Frase As String
    Public Imp_Servicio As String
    Public Logo As String
    Public Intereses As String
    Public Imp_ICT As String
    Public Total_Dias_Cancelar As String
    Public Email As String
    Public Dirrecion_Web As String
    Public Comision_Habitaciones As String
    Public Comision_Servicio As String
    Public Comision_Restaurant As String
    Public Cuenta_Bancaria1 As String
    Public Cuenta_Bancaria2 As String
    Public Impresora_Facturacion As String
    Public Maximo_Descuento As String
    Public PersonaJuridica As String
    Public CantidadHabitaciones As String
    Public ConsecutivoUnico As String
    Public Desayunos As String
    Public Imp_Servicio_Habitacion As String
    Public UnicoEncargadoXMesa As String
    Public MontoApertura As String
    Public MonedaRestaurante As String
    Public OpcionFacturacion As String
    Public Prefactura As String
    Public monedaPrefactura As String
    Public MonedaSistema As String
    Public IncluirServicios As String
    Public NinosGratisHab As String
    Public PedirTarjeta As String
    Public General As String
    Public TIPOFACT As String
    Public FormatoCheck As String
    Public TipoPlanning As String
    Public AsignarHab As String
    Public PoliticasCancelacion As String
    Public AsignarHabAlloment As String
    Public PersonaporHab As String
    Public Hora_Inicio As String
    Public Logo2 As String
    Public Auditoria As String
    Public Moneda_Vuelto As String
    Public Contabilidad As String
    Public PolCancelESP As String
    Public PolCancelING As String
    Public PolSoloPREP As String
    Public MiniBar As String
    Public NoClave As String
    Public CostoCortesia As String
    Public utilizaGrupo As String


    Dim f As New cls_Broker()



    'Obtenemos el tipo de cambio..
    Public Function obtenerTipoCambio() As String

        Dim dt As DataTable
        dt = Me.f.fireSQL("Select valorVenta from seguridad.dbo.moneda where monedaNombre = 'DOLAR'")

        Return dt.Rows(0)(0).ToString()

    End Function

    Public Function obtenerTipoCambioValorCompra() As String

        Dim dt As DataTable
        dt = Me.f.fireSQL("Select valorCompra from seguridad.dbo.moneda where monedaNombre = 'DOLAR'")

        Return dt.Rows(0)(0).ToString()

    End Function

    Public Sub New()


        Me.Cedula = ""
        Me.Empresa = ""
        Me.Tel_01 = ""
        Me.Tel_02 = ""
        Me.Fax_01 = ""
        Me.Fax_02 = ""
        Me.Direccion = ""
        Me.Imp_Venta = ""
        Me.Frase = ""
        Me.Imp_Servicio = ""
        Me.Logo = ""
        Me.Intereses = ""
        Me.Imp_ICT = ""
        Me.Total_Dias_Cancelar = ""
        Me.Email = ""
        Me.Dirrecion_Web = ""
        Me.Comision_Habitaciones = ""
        Me.Comision_Servicio = ""
        Me.Comision_Restaurant = ""
        Me.Cuenta_Bancaria1 = ""
        Me.Cuenta_Bancaria2 = ""
        Me.Impresora_Facturacion = ""
        Me.Maximo_Descuento = ""
        Me.PersonaJuridica = ""
        Me.CantidadHabitaciones = ""
        Me.ConsecutivoUnico = ""
        Me.Desayunos = ""
        Me.Imp_Servicio_Habitacion = ""
        Me.UnicoEncargadoXMesa = ""
        Me.MontoApertura = ""
        Me.MonedaRestaurante = ""
        Me.OpcionFacturacion = ""
        Me.Prefactura = ""
        Me.monedaPrefactura = ""
        Me.MonedaSistema = ""
        Me.IncluirServicios = ""
        Me.NinosGratisHab = ""
        Me.PedirTarjeta = ""
        Me.General = ""
        Me.TIPOFACT = ""
        Me.FormatoCheck = ""
        Me.TipoPlanning = ""
        Me.AsignarHab = ""
        Me.PoliticasCancelacion = ""
        Me.AsignarHabAlloment = ""
        Me.PersonaporHab = ""
        Me.Hora_Inicio = ""
        Me.Logo2 = ""
        Me.Auditoria = ""
        Me.Moneda_Vuelto = ""
        Me.Contabilidad = ""
        Me.PolCancelESP = ""
        Me.PolCancelING = ""
        Me.PolSoloPREP = ""
        Me.MiniBar = ""
        Me.NoClave = ""
        Me.CostoCortesia = ""
        Me.utilizaGrupo = ""

    End Sub


    'Obtenemos el punto de venta actual..
    Public Function obtenerIdPuntoVenta() As String

        Dim s As String = GetSetting("Seesoft", "Restaurante", "LastPoint")
        Dim str As String = "select idpuntoventa from puntoventa where nombre = '" & s & "'"
        Dim dt As DataTable
        dt = Me.f.fireSQL(str)

        Return dt.Rows(0)(0).ToString()

    End Function


    Public Function obtenerNumeroCedula() As String
        Dim dt As New DataTable
        dt = Me.f.fireSQL("Select * from Hotel.dbo.configuraciones")
        Dim a As String = dt.Rows(0).Item(0).ToString
        Return a
    End Function

    Public Sub obtenerConfiguraciones()

        Dim dt As New DataTable

        dt = Me.f.fireSQL("Select * from Hotel.dbo.configuraciones")

        If (Not IsDBNull(dt.Rows(0)("Cedula"))) Then Me.Cedula = dt.Rows(0)("Cedula")
        If (Not IsDBNull(dt.Rows(0)("Empresa"))) Then Me.Empresa = dt.Rows(0)("Empresa")
        If (Not IsDBNull(dt.Rows(0)("Tel_01"))) Then Me.Tel_01 = dt.Rows(0)("Tel_01")
        If (Not IsDBNull(dt.Rows(0)("Tel_02"))) Then Me.Tel_02 = dt.Rows(0)("Tel_02")
        If (Not IsDBNull(dt.Rows(0)("Fax_01"))) Then Me.Fax_01 = dt.Rows(0)("Fax_01")
        If (Not IsDBNull(dt.Rows(0)("Fax_02"))) Then Me.Fax_02 = dt.Rows(0)("Fax_02")
        If (Not IsDBNull(dt.Rows(0)("Direccion"))) Then Me.Direccion = dt.Rows(0)("Direccion")
        If (Not IsDBNull(dt.Rows(0)("Imp_Venta"))) Then Me.Imp_Venta = dt.Rows(0)("Imp_Venta")
        If (Not IsDBNull(dt.Rows(0)("Frase"))) Then Me.Frase = dt.Rows(0)("Frase")
        If (Not IsDBNull(dt.Rows(0)("Imp_Servicio"))) Then Me.Imp_Servicio = dt.Rows(0)("Imp_Servicio")
        If (Not IsDBNull(dt.Rows(0)("Intereses"))) Then Me.Intereses = dt.Rows(0)("Intereses")
        If (Not IsDBNull(dt.Rows(0)("Imp_ICT"))) Then Me.Imp_ICT = dt.Rows(0)("Imp_ICT")
        If (Not IsDBNull(dt.Rows(0)("Total_Dias_Cancelar"))) Then Me.Total_Dias_Cancelar = dt.Rows(0)("Total_Dias_Cancelar")
        If (Not IsDBNull(dt.Rows(0)("Email"))) Then Me.Email = dt.Rows(0)("Email")
        If (Not IsDBNull(dt.Rows(0)("Dirrecion_Web"))) Then Me.Dirrecion_Web = dt.Rows(0)("Dirrecion_Web")
        If (Not IsDBNull(dt.Rows(0)("Comision_Habitaciones"))) Then Me.Comision_Habitaciones = dt.Rows(0)("Comision_Habitaciones")
        If (Not IsDBNull(dt.Rows(0)("Comision_Servicio"))) Then Me.Comision_Servicio = dt.Rows(0)("Comision_Servicio")
        If (Not IsDBNull(dt.Rows(0)("Comision_Restaurant"))) Then Me.Comision_Restaurant = dt.Rows(0)("Comision_Restaurant")
        If (Not IsDBNull(dt.Rows(0)("Cuenta_Bancaria1"))) Then Me.Cuenta_Bancaria1 = dt.Rows(0)("Cuenta_Bancaria1")
        If (Not IsDBNull(dt.Rows(0)("Cuenta_Bancaria2"))) Then Me.Cuenta_Bancaria2 = dt.Rows(0)("Cuenta_Bancaria2")
        If (Not IsDBNull(dt.Rows(0)("Impresora_Facturacion"))) Then Me.Impresora_Facturacion = dt.Rows(0)("Impresora_Facturacion")
        If (Not IsDBNull(dt.Rows(0)("Maximo_Descuento"))) Then Me.Maximo_Descuento = dt.Rows(0)("Maximo_Descuento")
        If (Not IsDBNull(dt.Rows(0)("PersonaJuridica"))) Then Me.PersonaJuridica = dt.Rows(0)("PersonaJuridica")
        If (Not IsDBNull(dt.Rows(0)("CantidadHabitaciones"))) Then Me.CantidadHabitaciones = dt.Rows(0)("CantidadHabitaciones")
        If (Not IsDBNull(dt.Rows(0)("ConsecutivoUnico"))) Then Me.ConsecutivoUnico = dt.Rows(0)("ConsecutivoUnico")
        If (Not IsDBNull(dt.Rows(0)("Desayunos"))) Then Me.Desayunos = dt.Rows(0)("Desayunos")
        If (Not IsDBNull(dt.Rows(0)("Imp_Servicio_Habitacion"))) Then Me.Imp_Servicio_Habitacion = dt.Rows(0)("Imp_Servicio_Habitacion")
        If (Not IsDBNull(dt.Rows(0)("UnicoEncargadoXMesa"))) Then Me.UnicoEncargadoXMesa = dt.Rows(0)("UnicoEncargadoXMesa")
        If (Not IsDBNull(dt.Rows(0)("MontoApertura"))) Then Me.MontoApertura = dt.Rows(0)("MontoApertura")
        If (Not IsDBNull(dt.Rows(0)("MonedaRestaurante"))) Then Me.MonedaRestaurante = dt.Rows(0)("MonedaRestaurante")
        If (Not IsDBNull(dt.Rows(0)("OpcionFacturacion"))) Then Me.OpcionFacturacion = dt.Rows(0)("OpcionFacturacion")
        If (Not IsDBNull(dt.Rows(0)("Prefactura"))) Then Me.Prefactura = dt.Rows(0)("Prefactura")
        If (Not IsDBNull(dt.Rows(0)("monedaPrefactura"))) Then Me.monedaPrefactura = dt.Rows(0)("monedaPrefactura")
        If (Not IsDBNull(dt.Rows(0)("MonedaSistema"))) Then Me.MonedaSistema = dt.Rows(0)("MonedaSistema")
        If (Not IsDBNull(dt.Rows(0)("IncluirServicios"))) Then Me.IncluirServicios = dt.Rows(0)("IncluirServicios")
        If (Not IsDBNull(dt.Rows(0)("NinosGratisHab"))) Then Me.NinosGratisHab = dt.Rows(0)("NinosGratisHab")
        If (Not IsDBNull(dt.Rows(0)("PedirTarjeta"))) Then Me.PedirTarjeta = dt.Rows(0)("PedirTarjeta")
        If (Not IsDBNull(dt.Rows(0)("General"))) Then Me.General = dt.Rows(0)("General")
        If (Not IsDBNull(dt.Rows(0)("TIPOFACT"))) Then Me.TIPOFACT = dt.Rows(0)("TIPOFACT")
        If (Not IsDBNull(dt.Rows(0)("FormatoCheck"))) Then Me.FormatoCheck = dt.Rows(0)("FormatoCheck")
        If (Not IsDBNull(dt.Rows(0)("TipoPlanning"))) Then Me.TipoPlanning = dt.Rows(0)("TipoPlanning")
        If (Not IsDBNull(dt.Rows(0)("AsignarHab"))) Then Me.AsignarHab = dt.Rows(0)("AsignarHab")
        If (Not IsDBNull(dt.Rows(0)("PoliticasCancelacion"))) Then Me.PoliticasCancelacion = dt.Rows(0)("PoliticasCancelacion")
        If (Not IsDBNull(dt.Rows(0)("AsignarHabAlloment"))) Then Me.AsignarHabAlloment = dt.Rows(0)("AsignarHabAlloment")
        If (Not IsDBNull(dt.Rows(0)("PersonaporHab"))) Then Me.PersonaporHab = dt.Rows(0)("PersonaporHab")
        If (Not IsDBNull(dt.Rows(0)("Hora_Inicio"))) Then Me.Hora_Inicio = dt.Rows(0)("Hora_Inicio")
        If (Not IsDBNull(dt.Rows(0)("Logo2"))) Then Me.Logo2 = dt.Rows(0)("Logo2")
        If (Not IsDBNull(dt.Rows(0)("Auditoria"))) Then Me.Auditoria = dt.Rows(0)("Auditoria")
        If (Not IsDBNull(dt.Rows(0)("Moneda_Vuelto"))) Then Me.Moneda_Vuelto = dt.Rows(0)("Moneda_Vuelto")
        If (Not IsDBNull(dt.Rows(0)("Contabilidad"))) Then Me.Contabilidad = dt.Rows(0)("Contabilidad")
        If (Not IsDBNull(dt.Rows(0)("PolCancelESP"))) Then Me.PolCancelESP = dt.Rows(0)("PolCancelESP")
        If (Not IsDBNull(dt.Rows(0)("PolCancelING"))) Then Me.PolCancelING = dt.Rows(0)("PolCancelING")
        If (Not IsDBNull(dt.Rows(0)("PolSoloPREP"))) Then Me.PolSoloPREP = dt.Rows(0)("PolSoloPREP")
        If (Not IsDBNull(dt.Rows(0)("MiniBar"))) Then Me.MiniBar = dt.Rows(0)("MiniBar")
        If (Not IsDBNull(dt.Rows(0)("NoClave"))) Then Me.NoClave = dt.Rows(0)("NoClave")
        If (Not IsDBNull(dt.Rows(0)("CostoCortesia"))) Then Me.CostoCortesia = dt.Rows(0)("CostoCortesia")
        If (Not IsDBNull(dt.Rows(0)("utilizaGrupo"))) Then Me.utilizaGrupo = dt.Rows(0)("utilizaGrupo")

    End Sub


End Class
