
'Esta clase imprime una factura usando la libreria LibPrintTicket
'
'
Imports System.Data



Public Class cls_Facturas


    Public NumeroFactura As String
    Public id_Factura As String
    Public numeroComanda As String

    Public t As New LibPrintTicket.Ticket()

    Public Ventas As DataTable
    Public ventasDetalle As DataTable
    Dim Comanda As DataTable
    Dim cliente As DataTable
    Public vueltos As DataTable

    Dim configs As New cls_Configuraciones()

    Dim f As New cls_Broker()

    'campos del encabezado de la factura 
    Public Id As String
    Public num_factura As String
    Public tipo As String
    Public cod_cliente As String
    Public nombre_cliente As String
    Public orden As String
    Public cedula_usuario As String
    Public pago_comision As String
    Public subtotal As String
    Public descuento As String
    Public imp_venta As String
    Public total As String
    Public fecha As String
    Public vence As String
    Public cod_encargado_compra As String
    Public contabilizado As String
    Public asientoVenta As String
    Public contabilizadocventa As String
    Public asientocosto As String
    Public anulado As String
    Public pagoimpuesto As String
    Public facturacancelado As String
    Public num_apertura As String
    Public entregado As String
    Public cod_moneda As String
    Public moneda_nombre As String
    Public direccion As String
    Public telefono As String
    Public subtotalgravada As String
    Public subtotalExcento As String
    Public transporte As String
    Public tipo_cambio As String
    Public monto_ict As String
    Public id_reservacion As String
    Public monto_saloero As String
    Public proveniencia_venta As String
    Public huesped As String
    Public descripcion As String
    Public tipocambiodolar As String
    Public idcliente As String
    Public extrapropina As String
    Public general As String
    Public monedaPrefactura As String
    Public mesa As String
    Public montoletras As String
    Public cedula As String
    Public observacion As String
    Public fechaApertura As String

    Public nombreUsuarioApertura As String



    Public Sub New()

        'Obtenemos las configuraciones


        Me.numeroComanda = ""
        Me.NumeroFactura = ""
        Me.id_Factura = ""

    End Sub
    'Obtenemos el siguiente numerode factura..
    Public Function obtenerSiguienteNumeroFacturaReal()
        Dim dt As New DataTable
        'dt = Me.f.fireSQL("select max(num_factura)+1 from hotel.dbo.ventas where tipo = 'CON'")
        dt = Me.f.fireSQL("select max(num_factura)+1 from hotel.dbo.ventas")

        Return dt.Rows(0)(0).ToString()
    End Function
    'Pasamos una factura acumlada a una factura normal..
    Public Sub guardarNuevaFactura()

        Dim s As String

        'Asignamos el numero de factura..
        Me.num_factura = Me.obtenerSiguienteNumeroFacturaReal()
        'Obtenemos la apertura abierta mas reciente..
        Me.obtenerNumeroApertura()

        s = "insert into hotel.dbo.ventas ( num_factura, tipo ,cod_cliente ,nombre_cliente,orden ,cedula_usuario ,pago_comision, " & _
        "subtotal, descuento ,imp_venta ,total ,fecha ,vence ,cod_encargado_compra, " & _
        "contabilizado, asientoVenta ,contabilizadocventa ,asientocosto ,anulado ,pagoimpuesto ,facturacancelado," & _
        "num_apertura, entregado , cod_moneda , moneda_nombre , direccion , telefono , subtotalgravada, " & _
        "subtotalExento , transporte , tipo_cambio ,monto_ict ,id_reservacion , monto_saloero ,proveniencia_venta, " & _
        "huesped ,descripcion , tipocambiodolar , idcliente , extrapropina ,general , monedaPrefactura , mesa, " & _
        "montoletras, cedula , observacion ) values " & _
        "('" & Me.num_factura & "'," & _
        "'" & Me.tipo & "'," & _
        "'" & Me.cod_cliente & "'," & _
        "'" & Me.nombre_cliente & "'," & _
        "'" & Me.orden & "'," & _
        "'" & Me.cedula_usuario & "'," & _
        "'" & Me.pago_comision & "'," & _
        "'" & Me.subtotal & "'," & _
        "'" & Me.descuento & "'," & _
        "'" & Me.imp_venta & "'," & _
        "'" & Me.total & "'," & _
        "getDate()," & _
        "getDate()," & _
        "'" & Me.cod_encargado_compra & "'," & _
        "'" & Me.contabilizado & "'," & _
        "'" & Me.asientoVenta & "'," & _
        "'" & Me.contabilizadocventa & "'," & _
        "'" & Me.asientocosto & "'," & _
        "'" & Me.anulado & "'," & _
        "'" & Me.pagoimpuesto & "'," & _
        "'" & Me.facturacancelado & "'," & _
        "'" & Me.num_apertura & "'," & _
        "'" & Me.entregado & "'," & _
        "'" & Me.cod_moneda & "'," & _
        "'" & Me.moneda_nombre & "'," & _
        "'" & Me.direccion & "'," & _
        "'" & Me.telefono & "'," & _
        "'" & Me.subtotalgravada & "'," & _
        "'" & Me.subtotalExcento & "'," & _
        "'" & Me.transporte & "'," & _
        "'" & Me.tipo_cambio & "'," & _
        "'" & Me.monto_ict & "'," & _
        "'" & Me.id_reservacion & "'," & _
        "'" & Me.monto_saloero & "'," & _
        "'" & Me.proveniencia_venta & "'," & _
        "'" & Me.huesped & "'," & _
        "'" & Me.descripcion & "'," & _
        "'" & Me.tipocambiodolar & "'," & _
        "'" & Me.idcliente & "'," & _
        "'" & Me.extrapropina & "'," & _
        "'" & Me.general & "'," & _
        "'" & Me.monedaPrefactura & "'," & _
        "'" & Me.mesa & "'," & _
        "'" & Me.montoletras & "'," & _
        "'" & Me.cedula & "'," & _
        "'" & Me.observacion & "' )"

        'Insertamos el nuevo registro..
        Me.f.fireSQLNoReturn(s)
        Me.Id = Me.obtenerIdFactura()

    End Sub

    'Obtenemos el siguiente numerode factura..
    Public Function obtenerIdFactura() As String
        Dim dt As New DataTable
        dt = Me.f.fireSQL("select max(id) from hotel.dbo.ventas")

        Return dt.Rows(0)(0).ToString()
    End Function
    Public Function obtenerNombreSalonero(ByRef _idUsuario As String) As String
        Dim dt As DataTable
        Dim nombre As String

        dt = Me.f.fireSQL("Select A.Iniciales  from seguridad.dbo.usuarios A, Ventas B where A.id_usuario = B.Cedula_Usuario and B.Cedula_Usuario = '" & _idUsuario & "'")
        If dt.Rows.Count > 0 Then
            If Not IsDBNull(dt.Rows(0)(0)) Then
                nombre = dt.Rows(0)(0).ToString()
            End If
        End If

        Return nombre

    End Function
    'nos damos cuenta si la factura tiene ya aplicado un descuento
    Public Function saberDescuentoFactura(ByRef _numeroSeparado As String, ByRef _numeroComanda As String) As Boolean
        Dim str As String
        Dim dt As DataTable

        str = "select distinct  descuento from ComandaTemporal WHERE cCodigo= " & _numeroComanda & " and separada= " & _numeroSeparado
        dt = Me.f.fireSQL(str)

        'si devuelve datos..
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)(0) = 0 Then
                Return False
            Else
                Return True
            End If
        End If

        Return False
    End Function

    'Aplicamos un descuento a las comandas..
    Public Sub aplicarDescuentoComandas(ByRef _descuento As String, ByRef _numeroComanda As String, ByRef _numeroSeparado As String)

        Dim str As String = "Update ComandaTemporal " & _
                                  " SET descuento = " & _descuento & "" & _
                                  " WHERE cCodigo=" & _numeroComanda & " and separada= " & _numeroSeparado

        Me.f.fireSQLNoReturn(str)

    End Sub


    'obtenemos la cedula del salonero..
    Public Function obtenerCedulaSalonero(ByVal _numFactura As String) As DataTable
        Dim dt As DataTable = Me.f.fireSQL("Select CedSalonero from Comanda where numerofactura= '" & _numFactura & "'")
        Return dt
    End Function
    'Obtenemos el monto del descuento de la factura..
    Public Function obtenerDescuentoFactura(ByRef _numeroSeparado As String, ByRef _numeroComanda As String) As Double
        Dim str As String
        Dim dt As DataTable

        str = "select distinct  descuento from ComandaTemporal WHERE cCodigo= " & _numeroComanda & " and separada= " & _numeroSeparado
        dt = Me.f.fireSQL(str)
        Return dt.Rows(0)(0)
    End Function

    'Obtenemos los datos para las cortesias..
    Public Function obtenerDatosCortesias(ByRef _numeroComanda As String, ByRef _numeroSeparado As String) As DataTable
        Dim str As String = "SELECT ComandaTemporal.*, Mesas.Nombre_Mesa FROM ComandaTemporal inner join Mesas on ComandaTemporal.cMesa = Mesas.Id WHERE cCodigo = " & _numeroComanda & " and Impreso=1 and ComandaTemporal.separada=" & _numeroSeparado
        Return Me.f.fireSQL(str)
    End Function

    'obtenemos el maximo numero de cortesia..
    Public Function obtenerNumeroCortesia() As Integer
        Dim dt As New DataTable
        dt = Me.f.fireSQL("Select isnull(MAX(NumeroCortesia)+1,0) as NumeroCortesia from Comanda")
        Return dt.Rows(0)(0)
    End Function
    'OBtenemos los datos de la factura..
    Public Sub obtenerDatosDeFactura(ByRef _idFactura As String)
        Try
            Me.configs.obtenerConfiguraciones()
            Me.Ventas = Me.f.fireSQL("Select * from Ventas where num_factura = " & _idFactura)
            Me.ventasDetalle = Me.f.fireSQL("select count(A.codigo) as 'Cantidad', A.Descripcion, sum(A.precio_unit) as 'SubTotal' from ventas_Detalle A, Ventas B  where A.id_Factura = B.Id and B.num_factura = " & _idFactura & " and A.cantidad > 0 group by (A.codigo), A.Descripcion, A.precio_unit")
            Me.vueltos = Me.f.fireSQL("select denominacion,vuelto,simbolo from  VistaOpagar where numeroFactura = " & _idFactura)

        Catch ex As Exception

        End Try
    End Sub
    'Obtenemos los datos de la factura acumulada
    Public Sub obtenerDatosDeFacturaAcumulada(ByRef _idFactura As String)
        Try
            Me.configs.obtenerConfiguraciones()
            Me.Ventas = Me.f.fireSQL("Select * from FacturasAcumuladas where Id = " & _idFactura)
            Me.ventasDetalle = Me.f.fireSQL("select count(A.codigo) as 'Cantidad', A.Descripcion, sum(A.precio_unit) as 'SubTotal' from facturasacumuladasdetalle A, facturasacumuladas B  where A.id_Factura = B.Id and B.Id = " & _idFactura & " and A.cantidad > 0 group by (A.codigo), A.Descripcion, A.precio_unit")


        Catch ex As Exception

        End Try
    End Sub

    'obtenemos
    Public Function obtenerPrecioCostoArticulo(ByRef _codigo As Integer) As Double
        Dim str As String = "Select CostoTotal from Menu_Restaurante where Id_Menu =" & _codigo
        Return Me.f.fireSQL(str).Rows(0)(0)
    End Function
    '-------------------------------------------------------------------------------------
    ' Este procedimiento retorna el detalle de la factura de una mesa  de una cuenta en especifico..
    Public Function obtenerDetalleFactura(ByRef _numeroSeparado As String, ByRef _numeroComanda As String) As DataTable
        Dim dt As DataTable
        Dim str As String

        str = "SELECT ComandaTemporal.Tipo_Plato, ComandaTemporal.cCantidad as 'Cant', ComandaTemporal.cProducto, ComandaTemporal.cDescripcion as 'Desc', ComandaTemporal.cPrecio as 'Precio', ComandaTemporal.cCodigo, ComandaTemporal.primero, ComandaTemporal.Cedula, ComandaTemporal.hora, ComandaTemporal.Express, (CASE WHEN ComandaTemporal.cProducto = 0 THEN (  ( ComandaTemporal.cCantidad * ComandaTemporal.cPrecio) - ((ComandaTemporal.cCantidad * ComandaTemporal.cPrecio) *  (ComandaTemporal.descuento / 100)   )   ) *   (Configuraciones.Imp_Venta / 100) ELSE   ((ComandaTemporal.cCantidad * ComandaTemporal.cPrecio) -       (ComandaTemporal.cCantidad * ComandaTemporal.cPrecio) *    (ComandaTemporal.descuento / 100)   ) *    (ISNULL(Menu_Restaurante.ImpVenta, 0) * Configuraciones.Imp_Venta / 100) END ) AS IVentas, (CASE WHEN ComandaTemporal.cProducto = 0 THEN ComandaTemporal.cCantidad * ComandaTemporal.cPrecio * (Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) ELSE ComandaTemporal.cCantidad * ComandaTemporal.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0))END) AS IServicios,  ComandaTemporal.descuento  FROM ComandaTemporal WITH (NOLOCK) LEFT OUTER JOIN  Menu_Restaurante ON ComandaTemporal.cProducto = Menu_Restaurante.Id_Menu CROSS JOIN  Configuraciones WHERE cCodigo= " & _numeroComanda & " and separada= " & _numeroSeparado & " ORDER BY primero DESC,express ASC"

        dt = Me.f.fireSQL(str)

        Return dt
    End Function
    'Obtenemos el centro de cortesia..
    Public Function obtenerCentroCortesia() As DataTable
        Dim str As String = "SELECT Id_Centro, Nombre, Observaciones FROM Cortesia_Centros where inhabilitado =0"

        Return Me.f.fireSQL(str)

    End Function

    Public Function obtenerCuentaContableCortesia(ByRef _codigo As String) As DataTable
        Dim str As String = "Select Nombre, ISNULL(CuentaContable,'') AS CuentaContable, ISNULL(DescripcionCuenta,'') AS DescripcionCuenta, Observaciones FROM Cortesia_Centros WHERE id_Centro =" & _codigo
        Return Me.f.fireSQL(str)
    End Function
    'Obtenemos el tipo de cambio para mostrar el total en dolares o colones segun sea el caso de la moneda del restaurante..
    Public Sub obtenerTipoCambio()
        Dim dt As DataTable
        dt = Me.f.fireSQL("select * from seguridad.dbo.moneda where codMoneda = 2")
        Me.tipoCambioDolar = dt.Rows(0)("ValorCompra")

    End Sub


    'OBtenemos los datos de la factura..
    Public Sub obtenerDatosDeFactura(ByRef _idFactura As String, ByRef _numeroComanda As String)
        Me.configs.obtenerConfiguraciones()
        Me.Ventas = Me.f.fireSQL("Select  * from Ventas where num_factura = '" & _idFactura & "' ")
        Me.ventasDetalle = Me.f.fireSQL("select count(A.codigo) as 'Cantidad', A.Descripcion, sum(A.precio_unit) as 'SubTotal' from ventas_Detalle A, Ventas B  where A.id_Factura = B.Id and B.num_factura = " & _idFactura & " and A.cantidad > 0 group by (A.codigo), A.Descripcion, A.precio_unit")
        Me.vueltos = Me.f.fireSQL("select denominacion,vuelto,simbolo,FormaPago from  VistaOpagar where numeroFactura = " & _idFactura)
    End Sub

    Public Function obtenerDescuentoDeFactura(ByRef _idFactura As String) As String

        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select distinct A.descuento from ventas_detalle A, ventas B where  B.id = A.id_factura and B.num_factura = " & _idFactura)
        Return dt.Rows(0)("descuento").ToString()

    End Function

    'Funcion que obtiene el numero de apertura mas proximo.. 
    Public Function obtenerNumeroApertura() As String

        Dim dt As DataTable
        dt = Me.f.fireSQL("select top 1 NApertura, Nombre, Cedula, fecha from aperturacaja where estado = 'A' order by nApertura desc")

        If dt.Rows.Count > 0 Then
            If Not IsDBNull(dt.Rows(0)(0)) Then
                Me.fechaApertura = dt.Rows(0)("fecha").ToString()
                Me.num_apertura = dt.Rows(0)("NApertura").ToString()
                Me.cedula_usuario = dt.Rows(0)("Cedula").ToString()
                Me.nombreUsuarioApertura = dt.Rows(0)("Nombre").ToString()
            End If
        End If
        Return Me.num_apertura
    End Function


    'Imprimimos la factura en espa�ol..
    Public Sub ImprimirFacturaEsp(ByRef _idFactura As String, ByRef _numeroComanda As String, ByRef _impresora As String, ByRef _servicioRestaurante As Boolean)

        Dim nombreSalonero As String



        Me.configs.obtenerConfiguraciones()

        Me.obtenerDatosDeFactura(_idFactura, _numeroComanda)

        Dim tipoCliente As String = "A"
        Dim nombreCliente As String = Me.Ventas.Rows(0)("Nombre_Cliente")
        Dim codCliente As Integer = Me.Ventas.Rows(0)("Cod_Cliente")

        If Me.Ventas.Rows(0)("Cod_cliente") <> "0" Then
            Me.cliente = Me.f.fireSQL("Select nombre, tipoPrecio from Hotel.dbo.cliente where id = " & Me.Ventas.Rows(0)("Cod_cliente"))
            'Obtenemos el tipo de cliente...
            nombreCliente = cliente.Rows(0)("nombre")
            If cliente.Rows(0)("tipoPrecio") = "Tipo A" Then tipoCliente = "A"
            If cliente.Rows(0)("tipoPrecio") = "Tipo B" Then tipoCliente = "B"
            If cliente.Rows(0)("tipoPrecio") = "Tipo C" Then tipoCliente = "C"
            If cliente.Rows(0)("tipoPrecio") = "Tipo D" Then tipoCliente = "D"
        End If

        nombreSalonero = Me.obtenerNombreSalonero(Me.Ventas.Rows(0)("Cedula_Usuario"))

        'Creamos el encabezado de la factura..
        Me.t.AddSuperHeaderLine("            " & Me.Ventas.Rows(0)("Mesa"))
        Me.t.AddSuperHeaderLine("            ")
        Me.t.AddSuperHeaderLine(Me.configs.PersonaJuridica)
        Me.t.AddHeaderLine("Ced.Jur " & Me.configs.Cedula)
        Me.t.AddHeaderLine("Tel. " & Me.configs.Tel_01 & " " & Me.configs.Tel_02)
        Me.t.AddHeaderLine(Me.configs.Empresa)

        Me.t.AddHeaderNotes("Factura#:     " & tipoCliente & _idFactura & Me.Ventas.Rows(0)("Tipo"))

        Me.t.AddSubHeaderLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
        'Me.t.AddSubHeaderLine("Moneda: " & Me.Ventas.Rows(0)("Moneda_Nombre") & " (" & Me.Ventas.Rows(0)("Tipo_Cambio") & " )")
        'Me.t.AddSubHeaderLine("Mesa: " & Me.Ventas.Rows(0)("Mesa")) '& " Atendido por " & nombreSalonero)
        Me.t.AddSubHeaderLine("Cliente: " & nombreCliente)
        Me.t.AddSubHeaderLine("(" & codCliente & ")")

        If Me.Ventas.Rows(0)("Direccion") <> "" Then
            Me.t.AddSubHeaderLine("Direccion: " & Me.Ventas.Rows(0)("Direccion"))
        End If
        If Me.Ventas.Rows(0)("Telefono") <> "" Then
            Me.t.AddSubHeaderLine("Telefono: " & Me.Ventas.Rows(0)("Telefono"))
        End If

        'Con esto agregamos el nombre del salonero en caso que el mismo exista como un Item del menu.
        Dim d As DataRow
        For Each d In Me.ventasDetalle.Rows
            If d("Descripcion").ToString().StartsWith("@") Then
                Me.t.AddSubHeaderLine("Salonero: " & d("Descripcion").ToString())
            End If
        Next

        If Not _servicioRestaurante Then
            'Creamos el detalle de la factura..
            For Each d In Me.ventasDetalle.Rows
                If Not d("Descripcion").ToString().StartsWith("@") Then
                    If (d("Cantidad") = "1") Then
                        Me.t.AddItem("1", d("Descripcion"), Format(Math.Round(Double.Parse(d("subtotal")), 2), "#,##0.00"))
                    Else
                        Me.t.AddItem("-", d("Descripcion"), "")
                        Me.t.AddItem(d("Cantidad"), " X      " & Format((Double.Parse(d("subtotal")) / Double.Parse(d("Cantidad"))), "#,##0.00"), Format(Math.Round(Double.Parse(d("subtotal")), 2), "#,##0.00"))
                    End If
                End If
            Next
        Else
            Me.t.AddItem(" ", "Servicio de Restaurante", Format(Me.Ventas.Rows(0)("SubTotal"), "#,##0.00"))
        End If

        Dim descuento As String = Me.obtenerDescuentoDeFactura(_idFactura)
        'Agregamos el detalle de los totales..  Format(Me.impV, simbolo & "#,##0.00")

        Me.t.AddTotal("Sub.Total ", Format(Me.Ventas.Rows(0)("SubTotal"), "#,##0.00"))
        Me.t.AddTotal("Desc(" & descuento & "%)", Format(Me.Ventas.Rows(0)("Descuento"), "#,##0.00"))
        Me.t.AddTotal("Imp.Serv 10% ", Format(Me.Ventas.Rows(0)("Monto_saloero"), "#,##0.00"))
        Me.t.AddTotal("Imp.Venta ", Format(Me.Ventas.Rows(0)("Imp_venta"), "#,##0.00"))
        Me.t.AddSuperTotal("TOTAL", Format(Me.Ventas.Rows(0)("Total"), "#,##0.00"))

        'Si la moneda es colones.. mostramos el total en dolares..
        Me.obtenerTipoCambio()
        If Me.Ventas.Rows(0)("Cod_moneda") = 1 Then
            Me.t.AddSuperTotalNotes("Dolar", Format(Me.Ventas.Rows(0)("Total") / Me.tipocambiodolar, "#,##0.00"))
        End If
        If Me.Ventas.Rows(0)("Cod_moneda") = 2 Then
            Me.t.AddSuperTotalNotes("Colon", Format(Me.Ventas.Rows(0)("Total") * Me.tipocambiodolar, "#,##0.00"))
        End If

        'Si la factura no es de pedidos express (revisar esto, la factura con pedido express muestra un vuelto equivocado)
        'Si la factura muestra el vuelto, y ademas no es de pedidos express.. (And Me.Ventas.Rows(0)("Direccion") = "" )
        If Me.Ventas.Rows(0)("Tipo") = "CON" Then
            If GetSetting("SeeSOFT", "Restaurante", "MostrarVuelto") = 1 Then
                If vueltos.Rows.Count > 0 Then
                    If Not IsDBNull(Me.vueltos.Rows(0)(0)) Then
                        Me.t.AddFooterLine("Paga con : ")
                        For I As Integer = 0 To vueltos.Rows.Count - 1
                            Dim forma As String = Me.vueltos.Rows(I)("FormaPago")
                            Me.t.AddFooterLine("           " & forma(0) & forma(1) & forma(2) & " " & Me.vueltos.Rows(I)("simbolo") & Me.vueltos.Rows(I)("denominacion"))
                        Next
                        Me.t.AddFooterLine("")
                        Me.t.AddFooterLine("Vuelto   : " & Me.vueltos.Rows(0)("simbolo") & Math.Round(Double.Parse(Me.vueltos.Rows(0)("vuelto").ToString()), 2).ToString())
                    End If
                End If
            End If
        End If
        Me.t.AddFooterLine("")
        Me.t.AddFooterLine("")
        If Me.Ventas.Rows(0)("Tipo") = "CRE" Then
            Me.t.AddFooterLine(" ------------------------- ")
            Me.t.AddFooterLine(" FIRMA CLIENTE")
        End If
        Me.t.AddFooterLine("")
        Me.t.AddFooterLine("")
        Me.t.AddFooterLine("Autorizada mediante resoluci�n 11-97 de la fecha 05-09-97, Gaceta no. 171.")
        Me.t.PrintTicket(_impresora)
    End Sub

End Class

