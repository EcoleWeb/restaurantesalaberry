Public Class cls_FacturasDetalle

    'instancia del broker..
    Dim f As New cls_Broker()

    Public id_venta_detalle As String
    Public Id_Factura As String
    Public Codigo As String
    Public Descripcion As String
    Public Cantidad As String
    Public Precio_Costo As String
    Public TipoCambioCosto As String
    Public Precio_Base As String
    Public Precio_Flete As String
    Public Precio_Otros As String
    Public Precio_Unit As String
    Public Descuento As String
    Public Monto_Descuento As String
    Public Impuesto As String
    Public Monto_Impuesto As String
    Public SubtotalGravado As String
    Public SubTotalExcento As String
    Public SubTotal As String
    Public Devoluciones As String
    Public Numero_Entrega As String
    Public Max_Descuento As String
    Public Tipo_Cambio_ValorCompra As String
    Public Cod_MonedaVenta As String
    Public Impuesto_Ict As String
    Public Monto_Impuesto_Ict As String
    Public Propina As String
    Public Monto_Propina As String
    Public IdOperador As String


    Public Sub New()
        Me.id_venta_detalle = ""
        Me.Id_Factura = ""
        Me.Codigo = "" 'x
        Me.Descripcion = "" 'x
        Me.Cantidad = "" 'x
        Me.Precio_Costo = "" 'x
        Me.TipoCambioCosto = "0" 'x
        Me.Precio_Base = "0" 'x
        Me.Precio_Flete = "0" 'x
        Me.Precio_Otros = "0" 'x
        Me.Precio_Unit = "" 'x
        Me.Descuento = "0" 'x
        Me.Monto_Descuento = "0"
        Me.Impuesto = "0"
        Me.Monto_Impuesto = "0"
        Me.SubtotalGravado = "0"
        Me.SubTotalExcento = "0"
        Me.SubTotal = "0"
        Me.Devoluciones = "0"
        Me.Numero_Entrega = "0"
        Me.Max_Descuento = "0"
        Me.Tipo_Cambio_ValorCompra = "1"
        Me.Cod_MonedaVenta = "1"
        Me.Impuesto_Ict = "0"
        Me.Monto_Impuesto_Ict = "0"
        Me.Propina = "10"
        Me.Monto_Propina = ""
        Me.IdOperador = "0"
    End Sub



    'borramos los articulos de la mesa..
    Public Sub borrarComandaTemporal(ByRef _codigoMesa As String)
        Me.f.fireSQLNoReturn("delete comandatemporal where cMesa = '" & _codigoMesa & "' ")
    End Sub


    'Creamos un detalle de la factura acumulada con lo que esta en la tabla comandatemporal..
    Public Sub nuevoDetalle(ByRef _codigo As String, ByRef _iv As Double, ByRef _is As Double)

        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select * from comandatemporal where cantidad > 0 and cCodigo = '" & _codigo & "' ")
        Dim d As DataRow
        Dim msubtotal, miv, mis, md, mprecio, mcantidad As Double

        For Each d In dt.Rows
            mcantidad = d("cCantidad")
            mprecio = d("cPrecio") * mcantidad
            md = mprecio * d("Descuento") / 100

            miv = (mprecio - md) * _iv
            mis = (mprecio - md) * _is

            msubtotal = mprecio - md + miv + mis

            miv = mprecio * _iv

            Me.Codigo = d("cProducto").ToString()
            Me.Descripcion = d("cDescripcion").ToString()
            Me.Cantidad = d("cCantidad").ToString()
            Me.Precio_Costo = d("costo_real").ToString()
            Me.Precio_Unit = d("cPrecio").ToString()
            Me.Impuesto = _iv.ToString()
            Me.Monto_Impuesto = miv
            Me.Descuento = d("Descuento")
            Me.Monto_Descuento = md
            Me.SubtotalGravado = mprecio - md + miv
            Me.SubTotal = msubtotal
            Me.Propina = _is
            Me.Monto_Propina = mis

            Dim s As String
            s = "insert into hotel.dbo.ventasdetalle (Id_Factura, Codigo, Descripcion, Cantidad, Precio_Costo,TipoCambioCosto, " & _
            "Precio_Base, Precio_Flete, Precio_Otros, Precio_Unit, Descuento, Monto_Descuento, Impuesto, Monto_Impuesto, SubtotalGravado," & _
            "SubTotalExcento, SubTotal, Devoluciones, Numero_Entrega, Max_Descuento, Tipo_Cambio_ValorCompra, Cod_MonedaVenta," & _
            "Impuesto_Ict, Monto_Impuesto_Ict, Propina, Monto_Propina, IdOperador) values " & _
            "('" & Me.Id_Factura & "'," & _
            "'" & Me.Codigo & "'," & _
            "'" & Me.Descripcion & "'," & _
            "'" & Me.Cantidad & "'," & _
            "'" & Me.Precio_Costo & "'," & _
            "'" & Me.TipoCambioCosto & "'," & _
            "'" & Me.Precio_Base & "'," & _
            "'" & Me.Precio_Flete & "'," & _
            "'" & Me.Precio_Otros & "'," & _
            "'" & Me.Precio_Unit & "'," & _
            "'" & Me.Descuento & "'," & _
            "'" & Me.Monto_Descuento & "'," & _
            "'" & Me.Impuesto & "'," & _
            "'" & Me.Monto_Impuesto & "'," & _
            "'" & Me.SubtotalGravado & "'," & _
            "'" & Me.SubTotalExcento & "'," & _
            "'" & Me.SubTotal & "'," & _
            "'" & Me.Devoluciones & "'," & _
            "'" & Me.Numero_Entrega & "'," & _
            "'" & Me.Max_Descuento & "'," & _
            "'" & Me.Tipo_Cambio_ValorCompra & "'," & _
            "'" & Me.Cod_MonedaVenta & "'," & _
            "'" & Me.Impuesto_Ict & "'," & _
            "'" & Me.Monto_Impuesto_Ict & "'," & _
            "'" & Me.Propina & "'," & _
            "'" & Me.Monto_Propina & "'," & _
            "'" & Me.IdOperador & "')"

            Me.f.fireSQLNoReturn(s)
        Next


    End Sub



    '-----------------------------------------------------------

    'Creamos un detalle de la factura acumulada con lo que esta en la tabla comandatemporal..
    Public Sub nuevoDetalle()

        Dim s As String
        s = "insert into hotel.dbo.ventas_detalle (Id_Factura, Codigo, Descripcion, Cantidad, Precio_Costo,TipoCambioCosto, " & _
        "Precio_Base, Precio_Flete, Precio_Otros, Precio_Unit, Descuento, Monto_Descuento, Impuesto, Monto_Impuesto, SubtotalGravado," & _
        "SubTotalExcento, SubTotal, Devoluciones, Numero_Entrega, Max_Descuento, Tipo_Cambio_ValorCompra, Cod_MonedaVenta," & _
        "Impuesto_Ict, Monto_Impuesto_Ict, Propina, Monto_Propina, IdOperador) values " & _
        "('" & Me.Id_Factura & "'," & _
        "'" & Me.Codigo & "'," & _
        "'" & Me.Descripcion & "'," & _
        "'" & Me.Cantidad & "'," & _
        "'" & Me.Precio_Costo & "'," & _
        "'" & Me.TipoCambioCosto & "'," & _
        "'" & Me.Precio_Base & "'," & _
        "'" & Me.Precio_Flete & "'," & _
        "'" & Me.Precio_Otros & "'," & _
        "'" & Me.Precio_Unit & "'," & _
        "'" & Me.Descuento & "'," & _
        "'" & Me.Monto_Descuento & "'," & _
        "'" & Me.Impuesto & "'," & _
        "'" & Me.Monto_Impuesto & "'," & _
        "'" & Me.SubtotalGravado & "'," & _
        "'" & Me.SubTotalExcento & "'," & _
        "'" & Me.SubTotal & "'," & _
        "'" & Me.Devoluciones & "'," & _
        "'" & Me.Numero_Entrega & "'," & _
        "'" & Me.Max_Descuento & "'," & _
        "'" & Me.Tipo_Cambio_ValorCompra & "'," & _
        "'" & Me.Cod_MonedaVenta & "'," & _
        "'" & Me.Impuesto_Ict & "'," & _
        "'" & Me.Monto_Impuesto_Ict & "'," & _
        "'" & Me.Propina & "'," & _
        "'" & Me.Monto_Propina & "'," & _
        "'" & Me.IdOperador & "')"

        Me.f.fireSQLNoReturn(s)

    End Sub










End Class
