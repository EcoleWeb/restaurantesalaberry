'Clase que maneja operaciones sobre mesas..

Imports System.Data


Public Class cls_Mesas

    Dim f As New cls_Broker()

    Public id As String
    Public nombre_mesa As String
    Public imagen As String
    Public id_grupoMesa As String
    Public id_seccion As String
    Public numero_asientos As String
    Public posicion As String
    Public activa As String
    Public imagenruta As String
    Public separada As String
    Public comenzales As String
    Public tem As String


    'Constructor de la clase
    Public Sub New()

        Me.id = ""
        Me.nombre_mesa = ""
        Me.imagen = ""
        Me.id_grupoMesa = ""
        Me.id_seccion = ""
        Me.numero_asientos = ""
        Me.posicion = ""
        Me.activa = ""
        Me.imagenruta = ""
        Me.separada = ""
        Me.comenzales = ""
        Me.tem = ""

    End Sub


    'Obtenemos la informacion de una mesa en especifico referenciada por el id..
    Public Function obtenerMesa(ByRef _id As String) As cls_Mesas
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select * from mesas where id = " & _id)
        If dt.Rows.Count > 0 Then
            If (Not IsDBNull(dt.Rows(0)("Id"))) Then Me.id = dt.Rows(0)("Id")
            If (Not IsDBNull(dt.Rows(0)("nombre_mesa"))) Then Me.nombre_mesa = dt.Rows(0)("nombre_mesa")
            If (Not IsDBNull(dt.Rows(0)("imagen"))) Then Me.imagen = dt.Rows(0)("imagen")
            If (Not IsDBNull(dt.Rows(0)("id_grupoMesa"))) Then Me.id_grupoMesa = dt.Rows(0)("id_grupoMesa")
            If (Not IsDBNull(dt.Rows(0)("id_seccion"))) Then Me.id_seccion = dt.Rows(0)("id_seccion")
            If (Not IsDBNull(dt.Rows(0)("numero_asientos"))) Then Me.numero_asientos = dt.Rows(0)("numero_asientos")
            If (Not IsDBNull(dt.Rows(0)("posicion"))) Then Me.posicion = dt.Rows(0)("posicion")
            If (Not IsDBNull(dt.Rows(0)("activa"))) Then Me.activa = dt.Rows(0)("activa")
            If (Not IsDBNull(dt.Rows(0)("imagenruta"))) Then Me.imagenruta = dt.Rows(0)("imagenruta")
            If (Not IsDBNull(dt.Rows(0)("separada"))) Then Me.separada = dt.Rows(0)("separada")
            If (Not IsDBNull(dt.Rows(0)("comenzales"))) Then Me.comenzales = dt.Rows(0)("comenzales")
            If (Not IsDBNull(dt.Rows(0)("tem"))) Then Me.tem = dt.Rows(0)("tem")
        End If
        Return Me
    End Function

    Public Function obtenerIdMesaPorNombre(ByRef _nombreMesa As String) As String
        Dim str As String = "select id from mesas where nombre_mesa = '" & _nombreMesa & "'"
        Return Me.f.fireSQL(str).Rows(0)(0).ToString()
    End Function

    'Este procedimiento actualiza la informacion de los campos de la tabla 
    ' con los campos de los atributos de la clase..
    Public Sub actualizar()

        Me.f.fireSQLNoReturn("update mesas set " & _
                                "nombre_mesa = '" & Me.nombre_mesa & "'" & _
                                ",id_grupoMesa = '" & Me.id_grupoMesa & "'" & _
                                ",id_seccion = '" & Me.id_seccion & "'" & _
                                ",numero_asientos ='" & Me.numero_asientos & "'" & _
                                ",posicion ='" & Me.posicion & "'" & _
                                ",activa = '" & Me.activa & "'" & _
                                ",imagenruta = '" & Me.imagenruta & "'" & _
                                ",separada = '" & Me.separada & "'" & _
                                ",comenzales = '" & Me.comenzales & "'" & _
                                ",tem = '" & Me.tem & "'" & _
                                " where id = " & Me.id)
    End Sub

    'PRocedimiento que cambia el nombre a la mesa

    Public Sub cambiarNombreMesa(ByVal _nuevoNombre As String)
        If _nuevoNombre <> "" Then
            If Me.tem = "" Then
                Me.tem = Me.nombre_mesa
                Me.nombre_mesa = _nuevoNombre
                Me.actualizar()
            Else
                Me.nombre_mesa = _nuevoNombre
                Me.actualizar()
            End If
        Else
            Me.restablecerNombreMesa()
        End If
    End Sub


    'PRocedimiento que restablece el nombre de la mesa
    Public Sub restablecerNombreMesa()
        If Me.tem <> "" Then
            Me.nombre_mesa = Me.tem
            Me.tem = ""
            Me.actualizar()
        End If
    End Sub

    'procedimiento que me devuelve el nombre actual de la mesa, tem o nombre_mesa
    Public Function obtenerNombreMesa() As String
        If Me.tem.Length <> 0 Then
            Return Me.nombre_mesa
        End If
        Return ""
    End Function


End Class
