Public Class cls_PedidosExpress

    'Broker..
    Dim f As New cls_Broker()

    'COnfiguraciones..
    Public configs As New cls_Configuraciones()

    'Campos de pedido Express
    Dim Id As String
    Dim IdCliente As String
    Dim Fecha As String
    Dim cComanda As String
    Dim SubTotal As String
    Dim ImpVenta As String
    Dim Total As String
    Dim Preparado As String
    Dim LoTraen As String
    Dim Entregado As String
    Dim HoraPreparado As String
    Dim HoraLlevado As String
    Dim HoraEntregado As String
    Dim Direccion As String

    'COnstructor
    Public Sub New()

        Me.Id = ""
        Me.IdCliente = ""
        Me.Fecha = ""
        Me.cComanda = ""
        Me.SubTotal = ""
        Me.ImpVenta = ""
        Me.Total = ""
        Me.Preparado = ""
        Me.LoTraen = ""
        Me.Entregado = ""
        Me.HoraPreparado = ""
        Me.HoraLlevado = ""
        Me.HoraEntregado = ""
        Me.Direccion = ""

        Me.configs.obtenerConfiguraciones()

    End Sub

    'Agregamos un nuevo pedido..
    Public Sub agregarNuevoPedido()
        Me.f.fireSQLNoReturn("Insert into PedidoExpress ( IdCliente,Fecha,cComanda,SubTotal,ImpVenta,Total,Preparado,LoTraen,Entregado,HoraPreparado,HoraLlevado,HoraEntregado,Direccion) " & _
                        " values ( '" & Me.IdCliente & "', '" & Me.Fecha & "', '" & Me.cComanda & "', '" & Me.SubTotal & "','" & Me.ImpVenta & "', '" & Me.Total & "', '" & Me.Preparado & "', '" & Me.HoraPreparado & "', '" & Me.HoraLlevado & "', '" & Me.HoraEntregado & "', '" & Me.Direccion & "' )")
    End Sub

    'Actualizamos los datos de un pedidod..
    Public Sub actualizarPedido(ByRef _currentComanda As String)
        Me.f.fireSQLNoReturn("Update PedidoExpress set " & _
        "idCliente = " & Me.IdCliente & "'," & _
        " Fecha = " & Me.Fecha & "'," & _
        " cComanda = " & Me.cComanda & "'," & _
        " SubTotal = " & Me.SubTotal & "'," & _
        " ImpVenta = " & Me.ImpVenta & "'," & _
        " Total = " & Me.Total & "'," & _
        " Preparado = " & Me.Preparado & "'," & _
        " LoTraen = " & Me.LoTraen & "'," & _
        " Entregado = " & Me.Entregado & "'," & _
        " HoraPreparado = " & Me.HoraPreparado & "'," & _
        " HoraLlevado = " & Me.HoraLlevado & "'," & _
        " HoraEntregado = " & Me.HoraEntregado & "'," & _
        " Direccion = " & Me.Direccion & "' " & _
        " where cCodigo = " & _currentComanda)
    End Sub

    Public Function obtenerNumeroComandaExpress() As String
        Dim temp As DataTable
        Dim numeroComanda As Integer
        temp = Me.f.fireSQL("SELECT ISNULL(MAX(NumeroComanda), 0) AS NumComanda FROM ConsecutivoComandas")
        numeroComanda = temp.Rows(0)(0)
        numeroComanda += 1
        Me.f.fireSQLNoReturn("INSERT INTO ConsecutivoComandas (Fecha,NumeroComanda)  VALUES (GETDATE(), " & numeroComanda & ")")
        Return numeroComanda.ToString()
    End Function


    'Obtenemos todos los pedidos pendientes..
    Public Function obtenerPedidosPendientes() As DataTable

        Dim temp As DataTable

        temp = Me.f.fireSQL("SELECT PedidoExpress.Id,PedidoExpress.IdCliente,ClienteExpress.Nombre AS Cliente, PedidoExpress.Fecha, PedidoExpress.cComanda, PedidoExpress.SubTotal, " & _
                      "PedidoExpress.ImpVenta, PedidoExpress.Total, PedidoExpress.Preparado, PedidoExpress.LoTraen, PedidoExpress.Entregado, " & _
                      "PedidoExpress.HoraPreparado, PedidoExpress.HoraLlevado, PedidoExpress.HoraEntregado,PedidoExpress.Direccion" & _
                      " FROM ClienteExpress INNER JOIN" & _
                      " PedidoExpress ON ClienteExpress.Id = PedidoExpress.IdCliente WHERE Entregado = 0 order by PedidoExpress.Id")

        Return temp
    End Function

    'Obtenemos un pedido en especifico...
    Public Function ObtenerPedido(ByRef _idComanda As String) As DataTable

        Dim consulta As String = "Select * from vs_ObtenerPedidoExpress Where cCodigo = " & _idComanda
        Dim temp As DataTable
        temp = Me.f.fireSQL(consulta)

        Return temp

    End Function


    'Eliminamos un pedido express con su comanda..
    Public Sub eliminarPedidoExpress(ByRef _comanda As String)

        Me.f.fireSQLNoReturn("DELETE PedidoExpress WHERE cComanda = " & _comanda)
        Me.f.fireSQLNoReturn("DELETE ComandaTemporal_Expres WHERE cComanda = " & _comanda)

    End Sub


    'Nos damos cuenta si un pediddo ya fue facturado...
    Public Function pedidoFacturado(ByRef _comanda As String) As Boolean
        Dim temp As DataTable
        temp = Me.f.fireSQL("select count(*) from ComandaTemporal_Expres where cCodigo = " & _comanda)

        'Si el valor no es nulo..
        If Not IsDBNull(temp.Rows(0)(0)) Then
            'Si la consulta devuelve cero, quiere decir que la comanda ya fue facturada..
            If temp.Rows(0)(0) = 0 Then
                Return True
            End If
        End If
        'De otra manera, la comanda aun existe..
        Return False
    End Function

    'OBtenemos el detalle de la factura de un pedido ya facturado...
    Public Function obtenerDetallePedidoFacturado(ByRef _comanda As String, Optional EsAcumulada As Integer = 0) As DataTable

        Dim numerofactura As String = ""
        Dim temp As DataTable
        Dim miFactura As New cls_Facturas()

        temp = Me.f.fireSQL("Select Numerofactura From Comanda Where  NumeroComanda = " & _comanda)

        'Si el campo no es nulo, entonces obtenemos el numero de factura..
        If temp.Rows.Count > 0 Then
            If Not IsDBNull(temp.Rows(0)("Numerofactura")) Then
                numerofactura = temp.Rows(0)("Numerofactura")
                If EsAcumulada = 0 Then
                    miFactura.obtenerDatosDeFactura(numerofactura)
                Else
                    miFactura.obtenerDatosDeFacturaAcumulada(numerofactura)
                End If

            End If
        End If



        Return miFactura.ventasDetalle
    End Function

    'OBtenemos el detalle de la factura de un pedido ya facturado...
    Public Function obtenerEncabezadoPedidoFacturado(ByRef _comanda As String, Optional EsAcumulada As Integer = 0) As DataTable

        Dim numerofactura As String = ""
        Dim temp As DataTable
        Dim miFactura As New cls_Facturas()

        temp = Me.f.fireSQL("Select Numerofactura From Comanda Where  NumeroComanda = " & _comanda)

        'Si el campo no es nulo, entonces obtenemos el numero de factura..
        If temp.Rows.Count > 0 Then
            If Not IsDBNull(temp.Rows(0)("Numerofactura")) Then
                numerofactura = temp.Rows(0)("Numerofactura")
            End If
            If EsAcumulada = 0 Then
                miFactura.obtenerDatosDeFactura(numerofactura)
            Else
                miFactura.obtenerDatosDeFacturaAcumulada(numerofactura)
            End If
        End If
        Return miFactura.Ventas
    End Function












End Class
