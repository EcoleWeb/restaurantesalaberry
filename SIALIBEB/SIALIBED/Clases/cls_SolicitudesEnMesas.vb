'Esta clase maneja consultas sobre la visa..
'
'

Imports System.Data

Public Class cls_SolicitudesEnMesas

    Public HoraCarga As String
    Public Nombre_Mesa As String
    Public cCodigo As String
    Public solicitud As String
    Public cMesa As String
    Public Impresora As String

    Dim f As New cls_Broker()

    Public Sub New()

        Me.HoraCarga = ""
        Me.Nombre_Mesa = ""
        Me.cCodigo = ""
        Me.solicitud = ""
        Me.cMesa = ""
        Me.Impresora = ""

    End Sub

    'OBtenemos los datos actuales de la vista...
    Public Function obtenerDatos(ByRef _impresora As String) As DataTable

        Dim dt As New DataTable()
        dt.Columns.Add("Id", GetType(Integer))
        dt.Columns.Add("Entregado", GetType(Boolean))
        dt.Columns.Add("T.Espera", GetType(Integer))
        dt.Columns.Add("Mesa", GetType(String))
        dt.Columns.Add("Comanda", GetType(String))
        dt.Columns.Add("Solicitud", GetType(Integer))
        dt.Columns.Add("Impresora", GetType(String))

        Dim temp As DataTable
        temp = Me.f.fireSQL("Select Despachado,Tiempo_espera as 'Tiempo de Espera', nombre_mesa as 'Mesa', cCodigo as 'Comanda', Solicitud, Impresora from SolicitudesEnMesas where impresora like '%" & _impresora & "%' Order By Tiempo_espera Asc ")
        Dim d As DataRow
        Dim d2 As DataRow

        For Each d In temp.Rows
            d2 = dt.NewRow()
            d2("Entregado") = d("Despachado")
            d2("T.Espera") = d("Tiempo de Espera")
            d2("Mesa") = d("Mesa")
            d2("Comanda") = d("Comanda")
            d2("Solicitud") = d("Solicitud")
            d2("Impresora") = d("Impresora")
            dt.Rows.Add(d2)
        Next

        Return dt
    End Function

    'Actualizamos el estado de despachado...
    Public Sub actualizarEstadoDespachado(ByRef _idComanda As String, ByRef _idSolicitud As String)
        Me.f.fireSQLNoReturn("update comandatemporal set despachado = 1, horaDespachado = getdate()  where ccodigo = " & _idComanda & " and solicitud = " & _idSolicitud)
    End Sub

    'Actualizamos el estado de despachado...
    Public Sub restablecerEstadoDespachado(ByRef _idComanda As String, ByRef _idSolicitud As String)
        Me.f.fireSQLNoReturn("update comandatemporal set despachado = 0  where ccodigo = " & _idComanda & " and solicitud = " & _idSolicitud)
    End Sub

    'OBtenemos las informacion de un platillo en especifico ..
    Public Function obtenerInformacionPlatillo(ByRef _idComanda As String, ByRef _idSolicitud As String) As DataTable

        Dim dt As DataTable
        dt = Me.f.fireSQL("Select cDescripcion as 'Descripcion' from comandatemporal where Impresora <> '' and Estado = 'ACTIVO' and ccodigo = " & _idComanda & " and solicitud = " & _idSolicitud)

        Return dt

    End Function

    'Obtenemos quien fue el usuario que realizo la comanda..
    Public Function obtenerInformacionUsuarioComanda(ByRef _idComanda As String, ByRef _idSolicitud As String) As String
        Dim dt As DataTable
        Dim nombre As String
        dt = Me.f.fireSQL("Select A.nombre  from seguridad.dbo.usuarios A, Comandatemporal B where A.id_usuario = B.cedula and B.cCodigo =  " & _idComanda & " and B.solicitud = " & _idSolicitud)
        If dt.Rows.Count > 0 Then
            If Not IsDBNull(dt.Rows(0)(0)) Then
                nombre = dt.Rows(0)(0).ToString()
            End If
        End If
        Return nombre
    End Function



End Class
