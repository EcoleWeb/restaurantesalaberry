Public Class cls_cajas


    Dim confs As New cls_Configuraciones()
    Dim f As New cls_Broker()
    Dim fa As New cls_facturasAcumuladas()

    '...........Campos Arqueo............
    Public efectivoColones As String
    Public efectivoDolares As String
    Public tarjetaColones As String
    Public tarjetaDolares As String
    Public travelCheck As String
    Public Total As String
    Public idApertura As String
    Public fecha As String
    Public cajero As String
    Public anulado As String
    Public efectivoEuros As String
    Public tipoCambioD As String
    Public tipoCambioE As String
    Public numDepColon As String
    Public numDepDolar As String

    '........Campos Apertura.........
    Public Napertura As String
    Public nombre As String
    Public estado As String
    Public observaciones As String
    Public cedula As String
    '.........Apertura Total Tope............
    Public codMoneda As String
    Public monto_tope As String
    Public moneda_nombre As String





    'Constructor...............................
    Public Sub New()

        Me.efectivoColones = ""
        Me.efectivoDolares = ""
        Me.tarjetaColones = ""
        Me.tarjetaDolares = ""
        Me.travelCheck = "0"
        Me.Total = ""
        Me.idApertura = ""
        Me.fecha = ""
        Me.cajero = ""
        Me.anulado = "0"
        Me.efectivoEuros = "0"
        Me.tipoCambioD = ""
        Me.tipoCambioE = "0"
        Me.numDepColon = "0"
        Me.numDepDolar = "0"

        Me.estado = "A"
        Me.observaciones = ""
        Me.anulado = "0"

    End Sub



    'Verificamos que el usuario tenga asociada una apertura ..
    Public Function verificarUsuarioApertura(ByRef _codigo As String) As Boolean

        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select count(a.NApertura) from aperturacaja  a, seguridad.dbo.usuarios b where a.cedula = b.id_usuario and a.estado = 'A' and b.clave_entrada = '" & _codigo & "'")

        If dt.Rows(0)(0) = 1 Then
            Return True
        End If

        Return False

    End Function


    Public Function obtenerMoneda(ByRef _Codigo As String) As String

        Dim dt As DataTable

        dt = Me.f.fireSQL("Select MonedaNombre from Moneda where CodMoneda = " & _Codigo)

        Return dt.Rows(0)(0).ToString()

    End Function


    Public Sub NuevaApertura()

        Dim s As String
        Me.confs.obtenerConfiguraciones()


        s = "insert into aperturacaja (fecha, nombre, estado, observaciones, anulado, cedula) values " & _
        "( getDate() , " & _
        "'" & Me.nombre & "'," & _
        "'" & Me.estado & "', " & _
        "'" & Me.observaciones & "'," & _
        "'" & Me.anulado & "', " & _
        "'" & Me.cedula & "' )"

        'Ingresamos la apertura 
        Me.f.fireSQLNoReturn(s)

        'Recuperamos el numero de apertura..
        Me.Napertura = Me.fa.obtenerNumeroApertura()
        Me.codMoneda = Me.confs.MonedaRestaurante
        Me.moneda_nombre = Me.obtenerMoneda(Me.codMoneda)

        s = "insert into Apertura_Total_Tope (Napertura, codMoneda,monto_tope, monedaNombre) values " & _
        "( '" & Me.Napertura & "'," & _
        "'" & Me.codMoneda & "', " & _
        "'" & Me.monto_tope & "', " & _
        "'" & Me.moneda_nombre & "' )"

        'Guardamos el monto de la apertura..
        Me.f.fireSQLNoReturn(s)


    End Sub




    'creamos un nuevo arqueo..
    Public Sub nuevoArqueo()

        Dim s As String
        Dim ctotal As Double
        Dim cefectivocolones As Double
        Dim cefectivodolares As Double
        Dim ctarjetacolones As Double
        Dim ctarjetadolares As Double
        Dim ctipocambio As Double

        Me.tipoCambioD = Me.confs.obtenerTipoCambio()

        cefectivocolones = Double.Parse(Me.efectivoColones)
        cefectivodolares = Double.Parse(Me.efectivoDolares)
        ctarjetacolones = Double.Parse(Me.tarjetaColones)
        ctarjetadolares = Double.Parse(Me.tarjetaDolares)

        ctipocambio = Double.Parse(Me.tipoCambioD)
        ctotal = cefectivocolones + (cefectivodolares * ctipocambio) + ctarjetacolones + (ctarjetadolares * ctipocambio)

        Me.Total = ctotal.ToString()

        s = "insert into arqueocajas (efectivoColones, efectivoDolares, tarjetaColones, tarjetaDolares ," & _
        "travelCheck, Total, idApertura, fecha, cajero, anulado, efectivoEuros, tipoCambioD, tipoCambioE," & _
        "numDepColon, numDepDolar) values (" & _
        "'" & Me.efectivoColones & "'," & _
        "'" & Me.efectivoDolares & "'," & _
        "'" & Me.tarjetaColones & "'," & _
        "'" & Me.tarjetaDolares & "'," & _
        "'" & Me.travelCheck & "'," & _
        "'" & Me.Total & "'," & _
        "'" & Me.idApertura & "'," & _
        " getDate()," & _
        "'" & Me.cajero & "'," & _
        "'" & Me.anulado & "'," & _
        "'" & Me.efectivoEuros & "'," & _
        "'" & Me.tipoCambioD & "'," & _
        "'" & Me.tipoCambioE & "'," & _
        "'" & Me.numDepColon & "'," & _
        "'" & Me.numDepDolar & "')"

        Me.f.fireSQLNoReturn(s)
        Me.f.fireSQLNoReturn("update aperturacaja set estado = 'M' where NApertura = " & Me.idApertura)

    End Sub






End Class
