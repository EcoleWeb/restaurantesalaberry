Imports System.Drawing.Printing
Public Class cls_facturasAcumuladas


#Region "Globales"
    'instancia del broker..
    Dim f As New cls_Broker()
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlClient.SqlConnection
    Shared num As Integer
    Public Anul As Boolean
    Public Id As Double
    Public num_factura As Integer
    Public tipo As String
    Public cod_cliente As Integer
    Public nombre_cliente As String
    Public orden As Integer
    Public cedula_usuario As String
    Public pago_comision As Byte
    Public subtotal As Double
    Public descuento As Double
    Public imp_venta As Double
    Public total As Double
    Public fecha As DateTime
    Public vence As DateTime
    Public cod_encargado_compra As String
    Public contabilizado As Byte
    Public asientoVenta As String
    Public contabilizadocventa As Byte
    Public asientocosto As String
    Public anulado As Byte
    Public pagoimpuesto As Integer
    Public facturacancelado As Byte
    Public num_apertura As Integer
    Public entregado As Byte
    Public cod_moneda As Integer
    Public moneda_nombre As String
    Public direccion As String
    Public telefono As String
    Public subtotalgravada As Double
    Public subtotalExcento As Double
    Public transporte As Double
    Public tipo_cambio As Double
    Public monto_ict As Double
    Public id_reservacion As Double
    Public monto_saloero As Double
    Public proveniencia_venta As Integer
    Public huesped As String
    Public descripcion As String
    Public tipocambiodolar As Double
    Public idcliente As Integer
    Public extrapropina As Double
    Public general As Byte
    Public monedaPrefactura As Integer
    Public mesa As String
    Public montoletras As String
    Public cedula As String
    Public observacion As String
    Public fechaApertura As String

    Public nombreUsuarioApertura As String
#End Region

    ''Public num As String '' ********************************************************************
    'Constructor..
    Public Sub New()
        Me.Id = 0
        Me.num_factura = 0 'x
        Me.tipo = "CON"
        Me.cod_cliente = 0
        Me.nombre_cliente = "CLIENTE CONTADO" 'x
        Me.orden = 0
        Me.cedula_usuario = "" 'x
        Me.pago_comision = False
        Me.subtotal = 0.0 'x 
        Me.descuento = 0.0 'x
        Me.imp_venta = 0.0 'x
        Me.total = 0.0 'x
        Me.fecha = DateTime.Now.ToString()
        Me.vence = DateTime.Now.ToString()
        Me.cod_encargado_compra = "NINGUNO"
        Me.contabilizado = False
        Me.asientoVenta = 0
        Me.contabilizadocventa = False
        Me.asientocosto = 0
        Me.anulado = False
        Me.pagoimpuesto = 0
        Me.facturacancelado = False
        Me.num_apertura = 0 'x
        Me.entregado = False 'x
        Me.cod_moneda = 1 'x
        Me.moneda_nombre = "COLON" 'x
        Me.direccion = "" 'x
        Me.telefono = "" 'x 
        Me.subtotalgravada = 0.0 'x
        Me.subtotalExcento = 0.0 'x
        Me.transporte = 0.0 'x"
        Me.tipo_cambio = 0.0 'x
        Me.monto_ict = 0.0 'x
        Me.id_reservacion = 0.0 'x
        Me.monto_saloero = 0.0 'x
        Me.proveniencia_venta = 1 'x
        Me.huesped = " " 'x
        Me.descripcion = "" 'x
        Me.tipocambiodolar = 0.0
        Me.idcliente = 0 'x
        Me.extrapropina = 0.0 'x
        Me.general = False 'x
        Me.monedaPrefactura = 0 'x
        Me.mesa = "" 'x
        Me.montoletras = "No" 'x
        Me.cedula = " " 'x
        Me.observacion = " " 'x

        'Me.Id = "0"
        'Me.num_factura = "" 'x
        'Me.tipo = "CON"
        'Me.cod_cliente = "0"
        'Me.nombre_cliente = "" 'x
        'Me.orden = "0"
        'Me.cedula_usuario = "" 'x
        'Me.pago_comision = "0"
        'Me.subtotal = "" 'x 
        'Me.descuento = "" 'x
        'Me.imp_venta = "" 'x
        'Me.total = "" 'x
        'Me.fecha = DateTime.Now.ToString()
        'Me.vence = DateTime.Now.ToString()
        'Me.cod_encargado_compra = "NINGUNO"
        'Me.contabilizado = "0"
        'Me.asientoVenta = "0"
        'Me.contabilizadocventa = "0"
        'Me.asientocosto = "0"
        'Me.anulado = "0"
        'Me.pagoimpuesto = "0"
        'Me.facturacancelado = "0"
        'Me.num_apertura = "0" 'x
        'Me.entregado = "0" 'x
        'Me.cod_moneda = "1" 'x
        'Me.moneda_nombre = "COLON" 'x
        'Me.direccion = "" 'x
        'Me.telefono = "" 'x 
        'Me.subtotalgravada = "" 'x
        'Me.subtotalExcento = "0" 'x
        'Me.transporte = "0" 'x
        'Me.tipo_cambio = "1" 'x
        'Me.monto_ict = "0" 'x
        'Me.id_reservacion = "0" 'x
        'Me.monto_saloero = "" 'x
        'Me.proveniencia_venta = "" 'x
        'Me.huesped = " " 'x
        'Me.descripcion = "" 'x
        'Me.tipocambiodolar = ""
        'Me.idcliente = "0" 'x
        'Me.extrapropina = "0" 'x
        'Me.general = "0" 'x
        'Me.monedaPrefactura = "0" 'x
        'Me.mesa = "" 'x
        'Me.montoletras = "No" 'x
        'Me.cedula = " " 'x
        'Me.observacion = " " 'x

    End Sub
    'Funcion que obtiene el numero de apertura mas proximo.. 
    Public Function obtenerNumeroApertura() As String
        Dim dt As DataTable
        dt = Me.f.fireSQL("select top 1 NApertura, Nombre, Cedula, fecha from aperturacaja where estado = 'A' order by nApertura desc")

        If dt.Rows.Count > 0 Then
            If Not IsDBNull(dt.Rows(0)(0)) Then
                Me.fechaApertura = dt.Rows(0)("fecha").ToString()
                Me.num_apertura = dt.Rows(0)("NApertura").ToString()
                Me.cedula_usuario = dt.Rows(0)("Cedula").ToString()
                Me.nombreUsuarioApertura = dt.Rows(0)("Nombre").ToString()
            End If
        End If

        Return Me.num_apertura
    End Function
    'Funcion que obtiene el reporte de todas las facturas acumuladas 
    'Funcion que retorna el siguiente numero de factura en la tabla de acumulados..
    Public Function siguienteNumeroFactura() As String
        Dim numeroFactura As String
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("Select max(num_factura)+1 from  facturasAcumuladas where Estado = 'ACTIVA'")
        'si el resultado es vacio, entonces nos creamos uno..
        If IsDBNull(dt.Rows(0)(0)) Then
            numeroFactura = "1"
        Else
            numeroFactura = dt.Rows(0)(0)
        End If
        Return numeroFactura
    End Function
    'Obtenemos el siguiente numerode factura..
    Public Function obtenerSiguienteNumeroFacturaReal()
        Dim numeroFactura As String
        Dim dt As New DataTable
        dt = Me.f.fireSQL("select max(num_factura)+1 from ventas where Proveniencia_Venta = " & User_Log.PuntoVenta)
        If IsDBNull(dt.Rows(0)(0)) Then
            numeroFactura = "1"
        Else
            numeroFactura = dt.Rows(0)(0)
        End If
        num = Convert.ToInt32(numeroFactura)
        Return numeroFactura
    End Function
    ''***************************************************************
    ''***************************************************************
    ' SELECCIONA EL NUMERO DE FACTURA EN LA TABLA facturasAcumuladas Y LO ELIMINA
    Public Sub seleccionaFactura(ByRef _Num_fact As Integer)
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select id from facturasAcumuladas where num_factura='" & _Num_fact & "' AND Estado ='ACTIVA' ")
        Dim d As DataRow
        For Each d In dt.Rows
            Me.obtenerFacturaAcumulada(d("id").ToString())
        Next
        Me.BorrarFacturaAcumulada(_Num_fact)
    End Sub
    ''***************************************************************
    ''***************************************************************
    ' SELECCIONA EL Id DE FACTURA EN LA TABLA facturasAcumuladas
    Public Function Obtener_ID_FacturaAcumulada(ByRef _Num_fact As Integer) As String
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select id from facturasAcumuladas where num_factura='" & _Num_fact & "' AND Estado ='ACTIVA' ")
        Dim d As DataRow
        For Each d In dt.Rows
            Return (d("id").ToString())
        Next
        Return 0
    End Function
    ''***************************************************************
    ''***************************************************************
    'SELECCIONA EL NOMBRE DE LA MESA
    Public Function nombreMesa(ByRef _id As String)
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select nombre_mesa from mesas where id = " & _id)
        Return dt.Rows(0)(0).ToString()
    End Function
    ''***************************************************************
    ''***************************************************************
    'BORRA LA FACTURA ACUMULADA DESPUES DE PASARLO A LA TABLA VENTAS DE HOTEL
    Public Sub BorrarFacturaAcumulada(ByRef _Num_fact As Integer)
        Me.f.fireSQLNoReturn("Update facturasacumuladas set Estado = 'FACTURADA' where num_factura='" & _Num_fact & "' AND Estado = 'ACTIVA'")
    End Sub
    ''***************************************************************
    ''***************************************************************
    'OBTIENE EL SIGUIENTE NUMERO DE COMANDA A UTILIZAR..
    Public Function siguienteNumeroComanda() As String
        Dim numeroComanda As String
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("Select max(NumeroComanda)+1 from comanda ")
        'si el resultado es vacio, entonces nos creamos uno..
        If IsDBNull(dt.Rows(0)(0)) Then
            numeroComanda = "1"
        Else
            numeroComanda = dt.Rows(0)(0)
        End If
        Return numeroComanda
    End Function
    '/***************************************************************
    '****************************************************************
    ' OBTIENE EL ID DE LA MESA SEGUN EL NOMBRE DE LA MISMA..
    Public Function idMesa() As String

        Dim id_mesa As String
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select id from mesas where nombre_mesa='" & Me.mesa & "' ")

        'si el resultado es vacio, entonces nos creamos uno..

        If dt.Rows.Count > 0 Then
            id_mesa = dt.Rows(0)(0)
        Else
            id_mesa = "0"
        End If

        Return id_mesa

    End Function
    '/***************************************************************
    '****************************************************************
    'INSERTA EN LA TABLA COMANDAS DESPUES DE INSERTAR EN LA TABLA VENTAS
    Public Sub ingresarVentasaComanda()

        Try
            Dim s As String
            Dim i_mesa As String = Me.mesa
            Dim NumeroComanda As String = siguienteNumeroComanda()
            s = "insert into comanda ( NumeroComanda,Fecha,Subtotal,Impuesto_venta,Total,Cedusuario,CedSalonero, " & _
            "idMesa, Comenzales, Facturado, Numerofactura, CuentaContable, contabilizado, cortesia, " & _
           "anulado,NumeroCortesia,Cod_Moneda ) values " & _
            "('" & NumeroComanda & "'," & _
            "getDate()," & _
            "'" & Me.subtotal & "'," & _
            "'" & Me.imp_venta & "'," & _
            "'" & Me.total & "'," & _
            "'" & Me.cedula_usuario & "'," & _
            "'" & Me.cedula_usuario & "'," & _
            "'" & i_mesa & "'," & _
            "'4.0'," & _
            "1," & _
            "'" & Me.num_factura & "'," & _
            "''," & _
            "0," & _
            "0," & _
            "0," & _
            "'0'," & _
            "'" & Me.cod_moneda & "' )"

            'Insertamos el nuevo registro..
            Me.f.fireSQLNoReturn(s)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    ''***************************************************************
    ''***************************************************************
    'Con este procedimiento obtenemos todos los datos de una factura acumulada
    Public Sub obtenerFacturaAcumulada(ByVal _codigo As String)

        'Declaramos una variable de tipo mesa..
        Dim mimesa As New cls_Mesas()


        Dim dt As New DataTable
        'dt = Me.f.fireSQL()
        cFunciones.Llenar_Tabla_Generico("select * from facturasAcumuladas where id = '" & _codigo & "'", dt, GetSetting("SEEsoft", "REstaurante", "Conexion"))

        'asignamos los valores..
        Me.Id = dt.Rows(0)("Id").ToString()
        Me.num_factura = dt.Rows(0)("num_factura").ToString()
        Me.tipo = dt.Rows(0)("tipo").ToString()
        Me.cod_cliente = dt.Rows(0)("cod_cliente").ToString()
        Me.nombre_cliente = dt.Rows(0)("nombre_cliente").ToString()
        Me.orden = dt.Rows(0)("orden").ToString()
        Me.cedula_usuario = dt.Rows(0)("cedula_usuario").ToString()
        Me.pago_comision = Convert.ToBoolean(dt.Rows(0)("pago_comision").ToString())
        Me.subtotal = dt.Rows(0)("subtotal").ToString()
        Me.descuento = dt.Rows(0)("descuento").ToString()
        Me.imp_venta = dt.Rows(0)("imp_venta").ToString()
        Me.total = dt.Rows(0)("total").ToString()
        Me.fecha = dt.Rows(0)("fecha").ToString()
        Me.vence = dt.Rows(0)("vence").ToString()
        Me.cod_encargado_compra = dt.Rows(0)("cod_encargado_compra").ToString()
        Me.contabilizado = Convert.ToBoolean(dt.Rows(0)("contabilizado").ToString())
        Me.asientoVenta = dt.Rows(0)("asientoVenta").ToString()
        Me.contabilizadocventa = Convert.ToBoolean(dt.Rows(0)("contabilizadocventa").ToString())
        Me.asientocosto = dt.Rows(0)("asientocosto").ToString()
        Me.anulado = Convert.ToBoolean(dt.Rows(0)("anulado").ToString())
        Me.pagoimpuesto = dt.Rows(0)("pagoimpuesto").ToString()
        Me.facturacancelado = Convert.ToBoolean(dt.Rows(0)("facturacancelado").ToString())
        Me.num_apertura = dt.Rows(0)("num_apertura").ToString()
        Me.entregado = Convert.ToBoolean(dt.Rows(0)("entregado").ToString())
        Me.cod_moneda = dt.Rows(0)("cod_moneda").ToString()
        Me.moneda_nombre = dt.Rows(0)("moneda_nombre").ToString()
        Me.direccion = dt.Rows(0)("direccion").ToString()
        Me.telefono = dt.Rows(0)("telefono").ToString()
        Me.subtotalgravada = dt.Rows(0)("subtotalgravada").ToString()
        Me.subtotalExcento = dt.Rows(0)("subtotalExcento").ToString()
        Me.transporte = dt.Rows(0)("transporte").ToString()
        Me.tipo_cambio = dt.Rows(0)("tipo_cambio").ToString()
        Me.monto_ict = dt.Rows(0)("monto_ict").ToString()
        Me.id_reservacion = dt.Rows(0)("id_reservacion").ToString()
        Me.monto_saloero = dt.Rows(0)("monto_saloero").ToString()
        Me.proveniencia_venta = dt.Rows(0)("proveniencia_venta").ToString()
        Me.huesped = dt.Rows(0)("huesped").ToString()
        Me.descripcion = dt.Rows(0)("descripcion").ToString()
        Me.tipocambiodolar = dt.Rows(0)("tipocambiodolar").ToString()
        Me.idcliente = dt.Rows(0)("idcliente").ToString()
        Me.extrapropina = dt.Rows(0)("extrapropina").ToString()
        Me.general = Convert.ToBoolean(dt.Rows(0)("general").ToString())
        Me.monedaPrefactura = dt.Rows(0)("monedaPrefactura").ToString()

        'Obtenemos el nombre de la mesa..
        'Me.mesa = dt.Rows(0)("mesa").ToString()
        Me.mesa = mimesa.obtenerIdMesaPorNombre(dt.Rows(0)("mesa").ToString())

        Me.montoletras = dt.Rows(0)("montoletras").ToString()
        Me.cedula = dt.Rows(0)("cedula").ToString()
        Me.observacion = dt.Rows(0)("observacion").ToString()
        Me.pasarAcumuladaAFacturacion()

    End Sub
    'Pasamos una factura acumlada a una factura normal..
    Public Sub pasarAcumuladaAFacturacion()

        Try
            Dim s As String
            'Asignamos el numero de factura..
            Me.num_factura = Me.obtenerSiguienteNumeroFacturaReal()
            Me.nombre_cliente = "CLIENTE CONTADO"
            'Obtenemos la apertura abierta mas reciente..
            Me.obtenerNumeroApertura()

            s = "insert into ventas ( num_factura, tipo ,cod_cliente ,nombre_cliente,orden ,cedula_usuario ,pago_comision, " & _
            "subtotal, descuento ,imp_venta ,total ,fecha ,vence ,cod_encargado_compra, " & _
            "contabilizado, asientoVenta ,contabilizadocventa ,asientocosto ,anulado ,pagoimpuesto ,facturacancelado," & _
            "num_apertura, entregado , cod_moneda , moneda_nombre , direccion , telefono , subtotalgravada, " & _
            "SubTotalExento , transporte , tipo_cambio ,monto_ict ,id_reservacion , monto_saloero ,proveniencia_venta, " & _
            "huesped ,descripcion , tipocambiodolar , idcliente , ExtraPropina , General , monedaPrefactura , mesa, " & _
            "montoletras, observacion ) values " & _
            "('" & Me.num_factura & "'," & _
            "'" & Me.tipo & "'," & _
            "'" & Me.cod_cliente & "'," & _
            "'" & Me.nombre_cliente & "'," & _
            "'" & Me.orden & "'," & _
            "'" & Me.cedula_usuario & "'," & _
            "'" & Me.pago_comision & "'," & _
            "'" & Me.subtotal & "'," & _
            "'" & Me.descuento & "'," & _
            "'" & Me.imp_venta & "'," & _
            "'" & Me.total & "'," & _
            "getDate()," & _
            "getDate()," & _
            "'" & Me.cod_encargado_compra & "'," & _
            "'" & Me.contabilizado & "'," & _
            "'" & Me.asientoVenta & "'," & _
            "'" & Me.contabilizadocventa & "'," & _
            "'" & Me.asientocosto & "'," & _
            "'" & Me.anulado & "'," & _
            "'" & Me.pagoimpuesto & "'," & _
            "'" & Me.facturacancelado & "'," & _
            "'" & Me.num_apertura & "'," & _
            "'" & Me.entregado & "'," & _
            "'" & Me.cod_moneda & "'," & _
            "'" & Me.moneda_nombre & "'," & _
            "'" & Me.direccion & "'," & _
            "'" & Me.telefono & "'," & _
            "'" & Me.subtotalgravada & "'," & _
            "'" & Me.subtotalExcento & "'," & _
            "'" & Me.transporte & "'," & _
            "'" & Me.tipo_cambio & "'," & _
            "'" & Me.monto_ict & "'," & _
            "'" & Me.id_reservacion & "'," & _
            "'" & Me.monto_saloero & "'," & _
            "'" & Me.proveniencia_venta & "'," & _
            "'" & Me.huesped & "'," & _
            "'" & Me.descripcion & "'," & _
            "'" & Me.tipocambiodolar & "'," & _
            "'" & Me.idcliente & "'," & _
            "'" & Me.extrapropina & "'," & _
            "'" & Me.general & "'," & _
            "'" & Me.monedaPrefactura & "'," & _
            "'" & Me.mesa & "'," & _
            "'" & Me.montoletras & "'," & _
            "'" & Me.observacion & "' )"
            'Insertamos el nuevo registro..
            Me.f.fireSQL_CnConexionNoReturn(s, GetSetting("SeeSoft", "Hotel", "Conexion"))
            ingresarVentasaComanda()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    'Agregamos una nueva factura acumulada..
    Public Sub nuevaFacturaAcumulada()
        Dim s As String
        'Asignamos el numero de factura..
        Me.num_factura = Me.siguienteNumeroFactura()
        'Obtenemos la apertura abierta mas reciente..
        Me.obtenerNumeroApertura()
        s = "insert into facturasAcumuladas ( num_factura, tipo ,cod_cliente ,nombre_cliente,orden ,cedula_usuario ,pago_comision, " & _
        "subtotal, descuento ,imp_venta ,total ,fecha ,vence ,cod_encargado_compra, " & _
        "contabilizado, asientoVenta ,contabilizadocventa ,asientocosto ,anulado ,pagoimpuesto ,facturacancelado," & _
        "num_apertura, entregado , cod_moneda , moneda_nombre , direccion , telefono , subtotalgravada, " & _
        "subtotalExcento , transporte , tipo_cambio ,monto_ict ,id_reservacion , monto_saloero ,proveniencia_venta, " & _
        "huesped ,descripcion , tipocambiodolar , idcliente , extrapropina ,general , monedaPrefactura , mesa, " & _
        "montoletras, cedula , observacion ) values " & _
        "('" & Me.num_factura & "'," & _
        "'" & Me.tipo & "'," & _
        "'" & Me.cod_cliente & "'," & _
        "'" & Me.nombre_cliente & "'," & _
        "'" & Me.orden & "'," & _
        "'" & Me.cedula_usuario & "'," & _
        "'" & Me.pago_comision & "'," & _
        "'" & Me.subtotal & "'," & _
        "'" & Me.descuento & "'," & _
        "'" & Me.imp_venta & "'," & _
        "'" & Me.total & "'," & _
        "getDate()," & _
        "getDate()," & _
        "'" & Me.cod_encargado_compra & "'," & _
        "'" & Me.contabilizado & "'," & _
        "'" & Me.asientoVenta & "'," & _
        "'" & Me.contabilizadocventa & "'," & _
        "'" & Me.asientocosto & "'," & _
        "'" & Me.anulado & "'," & _
        "'" & Me.pagoimpuesto & "'," & _
        "'" & Me.facturacancelado & "'," & _
        "'" & Me.num_apertura & "'," & _
        "'" & Me.entregado & "'," & _
        "'" & Me.cod_moneda & "'," & _
        "'" & Me.moneda_nombre & "'," & _
        "'" & Me.direccion & "'," & _
        "'" & Me.telefono & "'," & _
        "'" & Me.subtotalgravada & "'," & _
        "'" & Me.subtotalExcento & "'," & _
        "'" & Me.transporte & "'," & _
        "'" & Me.tipo_cambio & "'," & _
        "'" & Me.monto_ict & "'," & _
        "'" & Me.id_reservacion & "'," & _
        "'" & Me.monto_saloero & "'," & _
        "'" & Me.proveniencia_venta & "'," & _
        "'" & Me.huesped & "'," & _
        "'" & Me.descripcion & "'," & _
        "'" & Me.tipocambiodolar & "'," & _
        "'" & Me.idcliente & "'," & _
        "'" & Me.extrapropina & "'," & _
        "'" & Me.general & "'," & _
        "'" & Me.monedaPrefactura & "'," & _
        "'" & Me.mesa & "'," & _
        "'" & Me.montoletras & "'," & _
        "'" & Me.cedula & "'," & _
        "'" & Me.observacion & "' )"
        'Insertamos el nuevo registro..
        Me.f.fireSQLNoReturn(s)
    End Sub
    'Procedimiento para borrar todas las facturas acumuladas..
    Public Sub borrarTodasLasFacturasAcumuladas()
        Me.f.fireSQLNoReturn("update facturasAcumuladas set Estado = 'BORRADA' WHERE Estado = 'ACTIVA'")
        'Me.f.fireSQLNoReturn("delete facturasAcumuladasDetalle")
    End Sub
    'Cargamos todas las facturas acumuladas ...
    Public Function obtenerTodasFacturasAcumuladas() As DataTable
        Dim dt As DataTable
        dt = Me.f.fireSQL("select num_factura as 'Numero Factura', Fecha, Total from facturasAcumuladas where Estado = 'ACTIVA' order by [Numero Factura]")
        Return dt
    End Function
    'Obtenemos todos los totales de las facturas acumuladas..
    Public Function obtenerTotalesFacturasAcumuladas() As DataTable
        Dim dt As DataTable
        dt = Me.f.fireSQL("select isnull(sum(total),0) as 'Total', isnull(sum (descuento),0) as 'Descuento', isnull(sum(imp_venta),0) as 'Imp. Venta', isnull(sum( monto_saloero),0) as 'Imp. Servicio' from facturasAcumuladas where Estado = 'ACTIVA'")
        Return dt
    End Function
    'Procedimiento para imprimir el reporte de las facturas acumuladas..
    Public Sub imprimirReporteFacturasAcumuladas()
        Dim t As New LibPrintTicket.Ticket()
        Dim conf As New cls_Configuraciones()
        Dim dt As New DataTable()
        Dim dtTotales As New DataTable()
        Dim d As DataRow
        Dim impresora As String
        impresora = GetSetting("SeeSoft", "Restaurante", "ImpresoraPrefactura")
        dt = Me.obtenerTodasFacturasAcumuladas()
        dtTotales = Me.obtenerTotalesFacturasAcumuladas()
        t.AddHeaderLine(conf.Empresa)
        t.AddSubHeaderLine("Reporte de Fact. Acumuladas")
        t.AddSubHeaderLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
        For Each d In dt.Rows
            t.AddItem("", "Fact.# " & d("Numero Factura"), FormatNumber(d("Total"), 2, , , TriState.True))
        Next
        t.AddSuperTotalNotes("Total", FormatNumber(dtTotales.Rows(0)("Total"), 2, , , TriState.True))
        t.AddSuperTotalNotes("Desc.", FormatNumber(dtTotales.Rows(0)("Descuento"), 2, , , TriState.True))
        t.AddSuperTotalNotes("Imp.V", FormatNumber(dtTotales.Rows(0)("Imp. Venta"), 2, , , TriState.True))
        t.AddSuperTotalNotes("Imp.S", FormatNumber(dtTotales.Rows(0)("Imp. Servicio"), 2, , , TriState.True))
        t.AddFooterLine("Tips not Included")
        If t.PrinterExists(impresora) Then
            t.PrintTicket(impresora)
        Else
            Dim pd As New PrintDialog()
            pd.ShowDialog()
            t.PrintTicket(pd.PrinterSettings.PrinterName)
        End If
    End Sub
    'OBTIENE EN UNA VARIABLE ESTATICA EL NUMERO DE FACTURA QUE SE UTILIZO EN EL PROCESO..
    Public Function NumeroFactura() As String
        Dim num As String
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("Select max(num_factura) from  ventas ")
        NumeroFactura = dt.Rows(0)(0)
        Return NumeroFactura
    End Function
    ' FUNCION PARA IMPRIMIR LA NUEVA FACTURA...

    Public Sub Imprimir()
        Try
            Dim FacturaPVE As New CrystalDecisions.CrystalReports.Engine.ReportDocument

            Dim NFactura As Integer = num
            Dim dt As New DataTable
            Dim cedula As String
            Dim salon As String = ""
            Dim cnx As New Conexion
            Dim impresion As String = GetSetting("SeeSoft", "Restaurante", "Impresion")
            If impresion.Equals("") Then
                SaveSetting("SeeSoft", "Restaurante", "Impresion", "0")
            End If
            dt = Me.f.fireSQL("Select Cedula from Configuraciones")
            cedula = dt.Rows(0)(0)
            If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
                FacturaPVE.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVE1"))
            ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = 2 Then
                FacturaPVE = New RptFacturaPVECB
            Else
                FacturaPVE = New RptFacturaPVE  'Generica
            End If


            Dim impresora As String
            impresora = busca_impresora()
            If impresora = "" Then
                MessageBox.Show("No se seleccion� ninguna impresora", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                impresora = busca_impresora()
                If impresora = "" Then
                    Exit Sub
                End If
            End If

            Dim Conexion As String = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            dt = Me.f.fireSQL("Select CedSalonero from Comanda where numerofactura= '" & NFactura & "'")

            If dt.Rows.Count > 0 Then
                salon = dt.Rows(0)(0)
            Else
                salon = "-"
            End If


            FacturaPVE.PrintOptions.PrinterName = impresora
            FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            FacturaPVE.SetParameterValue(0, NFactura)
            FacturaPVE.SetParameterValue(1, User_Log.PuntoVenta)
            FacturaPVE.SetParameterValue(2, 0)
            FacturaPVE.SetParameterValue(3, Anul)

            Dim dt1 As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT Comanda.CedSalonero, Usuarios_1.Nombre AS NCajero,  ISNULL(Saloneros.Nombre, '') AS NSalonero, Comanda.Comenzales FROM Comanda INNER JOIN Usuarios Usuarios_1 ON Comanda.Cedusuario = Usuarios_1.Cedula LEFT OUTER JOIN Saloneros ON Comanda.CedSalonero = Saloneros.Cedula WHERE     (Comanda.Numerofactura = '" & num_factura & "')", dt1, GetSetting("Seesoft", "Restaurante", "Conexion"))

            FacturaPVE.SetParameterValue(4, dt1.Rows(0).Item("CedSalonero"))
            FacturaPVE.SetParameterValue(5, dt1.Rows(0).Item("NSalonero"))
            FacturaPVE.SetParameterValue(6, dt1.Rows(0).Item("NCajero"))
            FacturaPVE.SetParameterValue(7, dt1.Rows(0).Item("Comenzales"))

            CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVE, True, Conexion)
            FacturaPVE.PrintToPrinter(1, True, 0, 0)
            FacturaPVE.Dispose()
            FacturaPVE.Close()
            FacturaPVE = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Restaurante")
        End Try
    End Sub
    ' FUNCION BUSCA LA IMPRESORA CORRESPONDIENTE..
    Private Function busca_impresora() As String
        Try
            Dim PrintDocument1 As New PrintDocument
            Dim DefaultPrinter As String = PrintDocument1.PrinterSettings.PrinterName
            Dim PrinterInstalled As String
            'BUSCA LA IMPRESORA PREDETERMINADA PARA EL SISTEMA
            For Each PrinterInstalled In PrinterSettings.InstalledPrinters
                Select Case Split(PrinterInstalled.ToUpper, "\").GetValue(Split(PrinterInstalled.ToUpper, "\").GetLength(0) - 1)
                    Case "FACTURACION" 'FACTURACION
                        Return PrinterInstalled.ToString
                        Exit Function
                End Select
            Next

            Dim PrinterDialog As New PrintDialog
            Dim DocPrint As New PrintDocument
            PrinterDialog.Document = DocPrint
            If PrinterDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Return PrinterDialog.PrinterSettings.PrinterName 'DEVUELVE LA IMPRESORA  SELECCIONADA
            Else
                Return "" 'Ninguna Impresora
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function


End Class
