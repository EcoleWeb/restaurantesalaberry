Public Class cls_facturasAcumuladasDetalle

#Region "Globales"
    'instancia del broker..
    Dim f As New cls_Broker()
    Public cedula_usuario As String
    Public id_venta_detalle As String
    Public Id_Factura As String
    Public Codigo As String
    Public Descripcion As String
    Public Cantidad As String
    Public Precio_Costo As String
    Public TipoCambioCosto As String
    Public Precio_Base As String
    Public Precio_Flete As String
    Public Precio_Otros As String
    Public Precio_Unit As String
    Public Descuento As String
    Public Monto_Descuento As String
    Public Impuesto As String
    Public Monto_Impuesto As String
    Public SubtotalGravado As String
    Public SubTotalExcento As String
    Public SubTotal As String
    Public Devoluciones As String
    Public Numero_Entrega As String
    Public Max_Descuento As String
    Public Tipo_Cambio_ValorCompra As String
    Public Cod_MonedaVenta As String
    Public Impuesto_Ict As String
    Public Monto_Impuesto_Ict As String
    Public Propina As String
    Public Monto_Propina As String
    Public IdOperador As String
    Public NombreUsuario As String
    'Dim fa As New cls_facturasAcumuladas() ''*********************************************************

    Public num As String
#End Region
    Public Sub New()
        Me.id_venta_detalle = ""
        Me.Id_Factura = ""
        Me.Codigo = "" 'x
        Me.Descripcion = "" 'x
        Me.Cantidad = "" 'x
        Me.Precio_Costo = "" 'x
        Me.TipoCambioCosto = "0" 'x
        Me.Precio_Base = "0" 'x
        Me.Precio_Flete = "0" 'x
        Me.Precio_Otros = "0" 'x
        Me.Precio_Unit = "" 'x
        Me.Descuento = "0" 'x
        Me.Monto_Descuento = "0"
        Me.Impuesto = "0"
        Me.Monto_Impuesto = "0"
        Me.SubtotalGravado = "0"
        Me.SubTotalExcento = "0"
        Me.SubTotal = "0"
        Me.Devoluciones = "0"
        Me.Numero_Entrega = "0"
        Me.Max_Descuento = "0"
        Me.Tipo_Cambio_ValorCompra = "1"
        Me.Cod_MonedaVenta = "1"
        Me.Impuesto_Ict = "0"
        Me.Monto_Impuesto_Ict = "0"
        Me.Propina = "10"
        Me.Monto_Propina = ""
        Me.IdOperador = "0"
    End Sub

    'borramos los articulos de la mesa..
    Public Sub borrarComandaTemporal(ByRef _codigoMesa As String, ByRef _separada As String, Optional Express As Integer = 0, Optional NumComanda As Integer = 0)
        If Express = 1 Then
            Me.f.fireSQLNoReturn("Delete from ComandaTemporal_Expres where cCodigo=" & NumComanda & " and cMesa = '" & _codigoMesa & "' ")
            f.fireSQLNoReturn("Insert Into BitacoraComandaEliminada (Razon,IdReferencia,TipoReferencia,Usuario) Values ('Factura Acumulada','" & Id_Factura & "','Factura','" & NombreUsuario & "')")
            If _codigoMesa <> 0 Then
                Me.f.fireSQLNoReturn("UPDATE ComandaTemporal Set Estado='Facturada' where Estado= 'Activo' AND separada = '" & _separada & "' and cMesa = '" & _codigoMesa & "' ")
            End If
        Else
            Me.f.fireSQLNoReturn("UPDATE ComandaTemporal Set Estado='Facturada' where Estado= 'Activo' AND separada = '" & _separada & "' and cMesa = '" & _codigoMesa & "' ")
            f.fireSQLNoReturn("Insert Into BitacoraComandaEliminada (Razon,IdReferencia,TipoReferencia,Usuario) Values ('Factura Acumulada','" & Id_Factura & "','Factura','" & NombreUsuario & "')")

        End If
    End Sub
    'Creamos un detalle de la factura acumulada con lo que esta en la tabla comandatemporal..
    Public Sub nuevoDetalle(ByRef _codigoMesa As String, ByRef _separada As String, Optional NumComanda As Integer = 0)

        Dim dt As New DataTable()
        If NumComanda <> 0 Then
            dt = Me.f.fireSQL("select * from ComandaTemporal where  Estado= 'Activo' and cPrecio > 0 and separada = '" & _separada & "' and cMesa = '" & _codigoMesa & "' and cCodigo=" & NumComanda & " ")
        Else
            dt = Me.f.fireSQL("select * from ComandaTemporal where  Estado= 'Activo' and cPrecio > 0 and separada = '" & _separada & "' and cMesa = '" & _codigoMesa & "' ")

        End If
        Dim d As DataRow
        For Each d In dt.Rows

            Dim msubtotal, miv, mis, md, mprecio, mcantidad As Double
            mcantidad = Math.Abs(d("cCantidad"))
            mprecio = d("cPrecio") * mcantidad
            md = mprecio * d("Descuento") / 100
            miv = (mprecio - md) * 0.13
            mis = (mprecio - md) * 0.1
            msubtotal = mprecio - md + miv + mis

            miv = mprecio * 0.13

            Me.Codigo = d("cProducto").ToString()
            Me.Descripcion = d("cDescripcion").ToString()
            Me.Cantidad = Math.Abs(d("cCantidad")).ToString()
            Me.Precio_Costo = d("costo_real").ToString()
            Me.Precio_Unit = d("cPrecio").ToString()
            Me.Impuesto = "13"
            Me.Monto_Impuesto = miv
            Me.Descuento = d("Descuento")
            Me.Monto_Descuento = md
            Me.SubTotal = Precio_Unit * mcantidad ''msubtotal
            Me.SubtotalGravado = SubTotal ''mprecio - md + miv
            Me.Propina = "10"
            Me.Monto_Propina = mis

            Dim s As String
            s = "insert into facturasAcumuladasDetalle (Id_Factura, Codigo, Descripcion, Cantidad, Precio_Costo,TipoCambioCosto, " &
            "Precio_Base, Precio_Flete, Precio_Otros, Precio_Unit, Descuento, Monto_Descuento, Impuesto, Monto_Impuesto, SubtotalGravado," &
            "SubTotalExcento, SubTotal, Devoluciones, Numero_Entrega, Max_Descuento, Tipo_Cambio_ValorCompra, Cod_MonedaVenta," &
            "Impuesto_Ict, Monto_Impuesto_Ict, Propina, Monto_Propina, IdOperador) values " &
            "('" & Me.Id_Factura & "'," &
            "'" & Me.Codigo & "'," &
            "'" & Me.Descripcion & "'," &
            "'" & Me.Cantidad & "'," &
            "'" & Me.Precio_Costo & "'," &
            "'" & Me.TipoCambioCosto & "'," &
            "'" & Me.Precio_Base & "'," &
            "'" & Me.Precio_Flete & "'," &
            "'" & Me.Precio_Otros & "'," &
            "'" & Me.Precio_Unit & "'," &
            "'" & Me.Descuento & "'," &
            "'" & Me.Monto_Descuento & "'," &
            "'" & Me.Impuesto & "'," &
            "'" & Me.Monto_Impuesto & "'," &
            "'" & Me.SubtotalGravado & "'," &
            "'" & Me.SubTotalExcento & "'," &
            "'" & Me.SubTotal & "'," &
            "'" & Me.Devoluciones & "'," &
            "'" & Me.Numero_Entrega & "'," &
            "'" & Me.Max_Descuento & "'," &
            "'" & Me.Tipo_Cambio_ValorCompra & "'," &
            "'" & Me.Cod_MonedaVenta & "'," &
            "'" & Me.Impuesto_Ict & "'," &
            "'" & Me.Monto_Impuesto_Ict & "'," &
            "'" & Me.Propina & "'," &
            "'" & Me.Monto_Propina & "'," &
            "'" & Me.IdOperador & "')"

            Me.f.fireSQLNoReturn(s)
        Next


    End Sub
    ' obtenemos un detalle ---
    Public Sub obtenerDetalle(ByRef _codigo As String)
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("select * from facturasACumuladasDetalle where id_venta_detalle = '" & _codigo & "' ")
        Me.id_venta_detalle = _codigo
        Me.Id_Factura = dt.Rows(0)("Id_Factura").ToString()
        Me.Codigo = dt.Rows(0)("Codigo").ToString()
        Me.Descripcion = dt.Rows(0)("Descripcion").ToString()
        Me.Cantidad = dt.Rows(0)("Cantidad").ToString()
        Me.Precio_Costo = dt.Rows(0)("Precio_Costo").ToString()
        Me.TipoCambioCosto = dt.Rows(0)("TipoCambioCosto").ToString()
        Me.Precio_Base = dt.Rows(0)("Precio_Base").ToString()
        Me.Precio_Flete = dt.Rows(0)("Precio_Flete").ToString()
        Me.Precio_Otros = dt.Rows(0)("Precio_Otros").ToString()
        Me.Precio_Unit = dt.Rows(0)("Precio_Unit").ToString()
        Me.Descuento = dt.Rows(0)("Descuento").ToString()
        Me.Monto_Descuento = dt.Rows(0)("Monto_Descuento").ToString()
        Me.Impuesto = dt.Rows(0)("Impuesto").ToString()
        Me.Monto_Impuesto = dt.Rows(0)("Monto_Impuesto").ToString()
        Me.SubtotalGravado = dt.Rows(0)("SubtotalGravado").ToString()
        Me.SubTotalExcento = dt.Rows(0)("SubTotalExcento").ToString()
        Me.SubTotal = dt.Rows(0)("SubTotal").ToString()
        Me.Devoluciones = dt.Rows(0)("Devoluciones").ToString()
        Me.Numero_Entrega = dt.Rows(0)("Numero_Entrega").ToString()
        Me.Max_Descuento = dt.Rows(0)("Max_Descuento").ToString()
        Me.Tipo_Cambio_ValorCompra = dt.Rows(0)("Tipo_Cambio_ValorCompra").ToString()
        Me.Cod_MonedaVenta = dt.Rows(0)("Cod_MonedaVenta").ToString()
        Me.Impuesto_Ict = dt.Rows(0)("Impuesto_Ict").ToString()
        Me.Monto_Impuesto_Ict = dt.Rows(0)("Monto_Impuesto_Ict").ToString()
        Me.Propina = dt.Rows(0)("Propina").ToString()
        Me.Monto_Propina = dt.Rows(0)("Monto_Propina").ToString()
        Me.IdOperador = dt.Rows(0)("IdOperador").ToString()
        Me.insertarDetalle()
    End Sub
    ' SELECCIONA TODOS LOS DETALLES DE facturasACumuladasDetalle PARA ELIMINARLOS
    Public Sub seleccionaDetalles(ByRef Num_fact As Integer)
        Dim dt As New DataTable()
        Dim Fx As New cls_facturasAcumuladas

        dt = Me.f.fireSQL("select * from facturasACumuladasDetalle where Id_Factura= '" & Fx.Obtener_ID_FacturaAcumulada(Num_fact) & "'")
        Dim d As DataRow
        For Each d In dt.Rows
            Me.obtenerDetalle(d("id_venta_detalle").ToString())
        Next
        ' Borrar_datalles_FA(Num_fact)
    End Sub
    'BORRA TODOS LOS DETALLES DE UNA FACTURA ACUMULADA
    Public Sub Borrar_datalles_FA(ByRef Num_fact As Integer)
        Me.f.fireSQLNoReturn("delete from facturasACumuladasDetalle where Id_Factura= '" & Num_fact & "' ")
    End Sub
    'OBTIENE EL SIGUIENTE NUMERO DE FACTURA
    Public Function obtenerSiguienteNumeroFacturaReal()
        Dim dt As New DataTable
        dt = Me.f.fireSQL("select max(num_factura)+1 from ventas where Proveniencia_Venta = " & User_Log.PuntoVenta)
        Return dt.Rows(0)(0).ToString()
    End Function
    '**********************************************************
    '**********************************************************
    'SELECCIONA DATOS NECESARIOS PARA REALIZAR PROCESO DE PAGO DE FACTURA
    Public Function datos_factura()
        Dim dt As New DataTable
        Dim numer As String
        dt = Me.f.fireSQL("select max(num_factura) from ventas where tipo = 'CON'")
        numer = dt.Rows(0)(0).ToString()
        dt = Me.f.fireSQL("select id,total,cod_moneda from ventas where num_factura ='" & numer & "' ")
        Return dt
    End Function
    '**********************************************************
    '**********************************************************
    'RETORNA  EL MAXIMO NUMERO DE FACTURA
    Public Function retorna_num_factura()
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        dt = Me.f.fireSQL("select max(id) from ventas where Proveniencia_Venta = " & User_Log.PuntoVenta)
        Return dt.Rows(0)(0).ToString()
    End Function
    '**********************************************************
    '********************************************************
    ' INSERTA TODOS LOS DETALLES DE UNA FACTURA A LA TABLA ventas_detalle
    Public Sub insertarDetalle()
        Dim s As String
        num = Me.retorna_num_factura()
        Me.Id_Factura = num
        s = "insert into ventas_detalle (Id_Factura, Codigo, Descripcion, Cantidad, Precio_Costo,TipoCambioCosto, " & _
        "Precio_Base, Precio_Flete, Precio_Otros, Precio_Unit, Descuento, Monto_Descuento, Impuesto, Monto_Impuesto, SubtotalGravado," & _
        "SubTotalExcento, SubTotal, Devoluciones, Numero_Entrega, Max_Descuento, Tipo_Cambio_ValorCompra, Cod_MonedaVenta," & _
        "Impuesto_Ict, Monto_Impuesto_Ict, Propina, Monto_Propina, IdOperador) values " & _
        "('" & Me.Id_Factura & "'," & _
        "'" & Me.Codigo & "'," & _
        "'" & Me.Descripcion & "'," & _
        "'" & Me.Cantidad & "'," & _
        "'" & Me.Precio_Costo & "'," & _
        "'" & Me.TipoCambioCosto & "'," & _
        "'" & Me.Precio_Base & "'," & _
        "'" & Me.Precio_Flete & "'," & _
        "'" & Me.Precio_Otros & "'," & _
        "'" & Me.Precio_Unit & "'," & _
        "'" & Me.Descuento & "'," & _
        "'" & Me.Monto_Descuento & "'," & _
        "'" & Me.Impuesto & "'," & _
        "'" & Me.Monto_Impuesto & "'," & _
        "'" & Me.SubtotalGravado & "'," & _
        "'" & Me.SubTotalExcento & "'," & _
        "'" & Me.SubTotal & "'," & _
        "'" & Me.Devoluciones & "'," & _
        "'" & Me.Numero_Entrega & "'," & _
        "'" & Me.Max_Descuento & "'," & _
        "'" & Me.Tipo_Cambio_ValorCompra & "'," & _
        "'" & Me.Cod_MonedaVenta & "'," & _
        "'" & Me.Impuesto_Ict & "'," & _
        "'" & Me.Monto_Impuesto_Ict & "'," & _
        "'" & Me.Propina & "'," & _
        "'" & Me.Monto_Propina & "'," & _
        "'" & Me.IdOperador & "')"

        'Insertamos el nuevo registro..
        Me.f.fireSQL_CnConexionNoReturn(s, GetSetting("SeeSoft", "Hotel", "Conexion"))
        DetalleMenuComanda()
    End Sub
    '*******************************************************************************************
    '******************************************************************************************
    'SELECCIONA EL MAXIMO NUMERO DE COMANDA PARA INSERTAR LOS DETALLES
    Public Function NumeroComanda() As String
        Dim dt As New DataTable()
        dt = Me.f.fireSQL("Select max(IdComanda) from comanda ")
        Return dt.Rows(0)(0)
    End Function
    '*******************************************************************************************
    '******************************************************************************************
    'OBTIENE EL NUMERO DE CEDULA DEL USUARIO.
    Public Function obtenerCedula() As String
        Dim dt As DataTable
        dt = Me.f.fireSQL("select Cedula, fecha from aperturacaja where estado = 'A' order by nApertura desc")
        If dt.Rows.Count > 0 Then
            If Not IsDBNull(dt.Rows(0)(0)) Then
                Me.cedula_usuario = dt.Rows(0)("Cedula").ToString()
            End If
        End If
        Return Me.cedula_usuario
    End Function
    '**********************************************************
    '**********************************************************
    ' INGRESA LOS DETALLES DE LA COMANDA...
    Public Sub DetalleMenuComanda()
        Dim s As String
        Dim usr As String = obtenerCedula()
        Dim num_comanda As String = Me.NumeroComanda()
        s = "insert into DetalleMenuComanda (idcomanda,idmenu,cantidad,preciounitario,subtotal," & _
            "impventa,impservicio,ced_salonero,hora,costo_real) values " & _
        "('" & num_comanda & "'," & _
        "'" & Me.Codigo & "'," & _
        "'" & Me.Cantidad & "'," & _
        "'" & Me.Precio_Unit & "'," & _
        "'" & Me.SubTotal & "'," & _
        "'" & Me.Impuesto & "'," & _
        "'" & Me.Impuesto & "'," & _
       "'" & Me.cedula_usuario & "'," & _
        "getDate()," & _
        "'" & Me.Precio_Unit & "')"

        'Insertamos el nuevo registro..
        Me.f.fireSQLNoReturn(s)

    End Sub
    '*********************************************************************************************
    '********************************************************************************************

End Class
