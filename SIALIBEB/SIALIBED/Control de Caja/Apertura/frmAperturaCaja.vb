Public Class frmAperturaCaja

    Dim miCajas As New cls_cajas()
    Dim configs As New cls_Configuraciones()

    'Cargamos los datos..
    Public Sub cargarDatos()

    End Sub


    Private Sub frmAperturaCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim salir As Boolean = False
        Dim dc As New FrmDatos_Cajeros()

        dc.ShowDialog()

        If dc.cedula_cajero <> "" Then
            Me.miCajas.nombre = dc.nombre_cajero
            Me.miCajas.cedula = dc.cedula_cajero
            salir = True

            Me.lblMensaje.Visible = False

        Else
            Me.lblMensaje.Text = "Debe seleccionar un cajero para poder realizar una apertura"
            Me.lblMensaje.Visible = True
            Me.TextBox1.Enabled = False
            Me.Button2.Enabled = False
        End If
    End Sub


    'Guardamos una nueva apertura de caja..
    Public Sub guardarNuevaApertura()

        Me.miCajas.monto_tope = Me.TextBox1.Text
        Me.miCajas.NuevaApertura()

        Me.lblMensaje.Visible = True
        Me.lblMensaje.Text = "La Apertura Fue Realizada Con Exito ..!"
        Me.TextBox1.Text = "0"

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Me.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.guardarNuevaApertura()
    End Sub
End Class