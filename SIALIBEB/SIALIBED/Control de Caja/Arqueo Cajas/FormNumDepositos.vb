Public Class FormNumDepositos
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents TextBoxNumDepColon As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNumDepDolar As System.Windows.Forms.TextBox
    Friend WithEvents ButtonOK As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FormNumDepositos))
        Me.TextBoxNumDepColon = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBoxNumDepDolar = New System.Windows.Forms.TextBox
        Me.ButtonOK = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'TextBoxNumDepColon
        '
        Me.TextBoxNumDepColon.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNumDepColon.Location = New System.Drawing.Point(8, 32)
        Me.TextBoxNumDepColon.Name = "TextBoxNumDepColon"
        Me.TextBoxNumDepColon.Size = New System.Drawing.Size(312, 29)
        Me.TextBoxNumDepColon.TabIndex = 0
        Me.TextBoxNumDepColon.Text = ""
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(312, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "N�mero d�posito colones"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(0, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(312, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "N�mero d�posito dolares"
        '
        'TextBoxNumDepDolar
        '
        Me.TextBoxNumDepDolar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNumDepDolar.Location = New System.Drawing.Point(8, 112)
        Me.TextBoxNumDepDolar.Name = "TextBoxNumDepDolar"
        Me.TextBoxNumDepDolar.Size = New System.Drawing.Size(312, 29)
        Me.TextBoxNumDepDolar.TabIndex = 2
        Me.TextBoxNumDepDolar.Text = ""
        '
        'ButtonOK
        '
        Me.ButtonOK.Location = New System.Drawing.Point(136, 160)
        Me.ButtonOK.Name = "ButtonOK"
        Me.ButtonOK.TabIndex = 4
        Me.ButtonOK.Text = "OK"
        '
        'FormNumDepositos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(352, 189)
        Me.Controls.Add(Me.ButtonOK)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxNumDepDolar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBoxNumDepColon)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormNumDepositos"
        Me.Text = "Registre los n�meros de d�positos"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub ButtonOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
        Me.DialogResult = DialogResult.OK
        Close()
    End Sub
End Class
