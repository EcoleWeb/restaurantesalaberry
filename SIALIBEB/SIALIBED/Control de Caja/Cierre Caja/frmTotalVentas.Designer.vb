﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTotalVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTotalVentas))
        Me.TituloModulo = New System.Windows.Forms.Label()
        Me.txSaloneroCON = New DevExpress.XtraEditors.TextEdit()
        Me.txVentaGravadaCON = New DevExpress.XtraEditors.TextEdit()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txImpVentaCON = New DevExpress.XtraEditors.TextEdit()
        Me.txVentaExcentaCON = New DevExpress.XtraEditors.TextEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txExtraPropinaCON = New DevExpress.XtraEditors.TextEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txSubTotalGeneralCON = New DevExpress.XtraEditors.TextEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btAceptar = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbApertura = New System.Windows.Forms.Label()
        Me.txTotalGeneralCON = New DevExpress.XtraEditors.TextEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txTotalGeneralCRE = New DevExpress.XtraEditors.TextEdit()
        Me.txSubTotalGeneralCRE = New DevExpress.XtraEditors.TextEdit()
        Me.txExtraPropinaCRE = New DevExpress.XtraEditors.TextEdit()
        Me.txImpVentaCRE = New DevExpress.XtraEditors.TextEdit()
        Me.txVentaExcentaCRE = New DevExpress.XtraEditors.TextEdit()
        Me.txSaloneroCRE = New DevExpress.XtraEditors.TextEdit()
        Me.txVentaGravadaCRE = New DevExpress.XtraEditors.TextEdit()
        Me.txTotalGeneralCAR = New DevExpress.XtraEditors.TextEdit()
        Me.txSubTotalGeneralCAR = New DevExpress.XtraEditors.TextEdit()
        Me.txExtraPropinaCAR = New DevExpress.XtraEditors.TextEdit()
        Me.txImpVentaCAR = New DevExpress.XtraEditors.TextEdit()
        Me.txVentaExcentaCAR = New DevExpress.XtraEditors.TextEdit()
        Me.txSaloneroCAR = New DevExpress.XtraEditors.TextEdit()
        Me.txVentaGravadaCAR = New DevExpress.XtraEditors.TextEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txTotalGeneral = New System.Windows.Forms.Label()
        Me.txSubTotalGeneral = New System.Windows.Forms.Label()
        Me.txExtraPropina = New System.Windows.Forms.Label()
        Me.txImpVenta = New System.Windows.Forms.Label()
        Me.txSalonero = New System.Windows.Forms.Label()
        Me.txVentaExcenta = New System.Windows.Forms.Label()
        Me.txVentaGravada = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        CType(Me.txSaloneroCON.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txVentaGravadaCON.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txImpVentaCON.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txVentaExcentaCON.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txExtraPropinaCON.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txSubTotalGeneralCON.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTotalGeneralCON.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTotalGeneralCRE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txSubTotalGeneralCRE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txExtraPropinaCRE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txImpVentaCRE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txVentaExcentaCRE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txSaloneroCRE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txVentaGravadaCRE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTotalGeneralCAR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txSubTotalGeneralCAR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txExtraPropinaCAR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txImpVentaCAR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txVentaExcentaCAR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txSaloneroCAR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txVentaGravadaCAR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TituloModulo
        '
        Me.TituloModulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(91, Byte), Integer), CType(CType(165, Byte), Integer))
        Me.TituloModulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.TituloModulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.TituloModulo.ForeColor = System.Drawing.Color.White
        Me.TituloModulo.Image = CType(resources.GetObject("TituloModulo.Image"), System.Drawing.Image)
        Me.TituloModulo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.TituloModulo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.TituloModulo.Location = New System.Drawing.Point(0, 0)
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(778, 32)
        Me.TituloModulo.TabIndex = 59
        Me.TituloModulo.Text = "Ventas"
        Me.TituloModulo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txSaloneroCON
        '
        Me.txSaloneroCON.EditValue = "0.00"
        Me.txSaloneroCON.Location = New System.Drawing.Point(280, 72)
        Me.txSaloneroCON.Name = "txSaloneroCON"
        '
        '
        '
        Me.txSaloneroCON.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txSaloneroCON.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txSaloneroCON.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSaloneroCON.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txSaloneroCON.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSaloneroCON.Properties.ReadOnly = True
        Me.txSaloneroCON.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txSaloneroCON.Size = New System.Drawing.Size(98, 19)
        Me.txSaloneroCON.TabIndex = 80
        '
        'txVentaGravadaCON
        '
        Me.txVentaGravadaCON.EditValue = "0.00"
        Me.txVentaGravadaCON.Location = New System.Drawing.Point(86, 72)
        Me.txVentaGravadaCON.Name = "txVentaGravadaCON"
        '
        '
        '
        Me.txVentaGravadaCON.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txVentaGravadaCON.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txVentaGravadaCON.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaGravadaCON.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txVentaGravadaCON.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaGravadaCON.Properties.ReadOnly = True
        Me.txVentaGravadaCON.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txVentaGravadaCON.Size = New System.Drawing.Size(98, 19)
        Me.txVentaGravadaCON.TabIndex = 79
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label10.Location = New System.Drawing.Point(291, 40)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(85, 16)
        Me.Label10.TabIndex = 78
        Me.Label10.Text = "10 % Salonero"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label3.Location = New System.Drawing.Point(86, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 16)
        Me.Label3.TabIndex = 77
        Me.Label3.Text = "Ventas Gravadas"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txImpVentaCON
        '
        Me.txImpVentaCON.EditValue = "0.00"
        Me.txImpVentaCON.Location = New System.Drawing.Point(377, 72)
        Me.txImpVentaCON.Name = "txImpVentaCON"
        '
        '
        '
        Me.txImpVentaCON.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txImpVentaCON.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txImpVentaCON.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txImpVentaCON.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txImpVentaCON.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txImpVentaCON.Properties.ReadOnly = True
        Me.txImpVentaCON.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txImpVentaCON.Size = New System.Drawing.Size(98, 19)
        Me.txImpVentaCON.TabIndex = 84
        '
        'txVentaExcentaCON
        '
        Me.txVentaExcentaCON.EditValue = "0.00"
        Me.txVentaExcentaCON.Location = New System.Drawing.Point(183, 72)
        Me.txVentaExcentaCON.Name = "txVentaExcentaCON"
        '
        '
        '
        Me.txVentaExcentaCON.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txVentaExcentaCON.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txVentaExcentaCON.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaExcentaCON.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txVentaExcentaCON.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaExcentaCON.Properties.ReadOnly = True
        Me.txVentaExcentaCON.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txVentaExcentaCON.Size = New System.Drawing.Size(98, 19)
        Me.txVentaExcentaCON.TabIndex = 83
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label1.Location = New System.Drawing.Point(370, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 16)
        Me.Label1.TabIndex = 82
        Me.Label1.Text = "13 % Imp Venta"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label2.Location = New System.Drawing.Point(190, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 16)
        Me.Label2.TabIndex = 81
        Me.Label2.Text = "Ventas Excentas"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txExtraPropinaCON
        '
        Me.txExtraPropinaCON.EditValue = "0.00"
        Me.txExtraPropinaCON.Location = New System.Drawing.Point(474, 72)
        Me.txExtraPropinaCON.Name = "txExtraPropinaCON"
        '
        '
        '
        Me.txExtraPropinaCON.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txExtraPropinaCON.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txExtraPropinaCON.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txExtraPropinaCON.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txExtraPropinaCON.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txExtraPropinaCON.Properties.ReadOnly = True
        Me.txExtraPropinaCON.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txExtraPropinaCON.Size = New System.Drawing.Size(98, 19)
        Me.txExtraPropinaCON.TabIndex = 86
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label4.Location = New System.Drawing.Point(484, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 16)
        Me.Label4.TabIndex = 85
        Me.Label4.Text = "Extra Propina"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txSubTotalGeneralCON
        '
        Me.txSubTotalGeneralCON.EditValue = "0.00"
        Me.txSubTotalGeneralCON.Location = New System.Drawing.Point(571, 72)
        Me.txSubTotalGeneralCON.Name = "txSubTotalGeneralCON"
        '
        '
        '
        Me.txSubTotalGeneralCON.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txSubTotalGeneralCON.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txSubTotalGeneralCON.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSubTotalGeneralCON.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txSubTotalGeneralCON.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSubTotalGeneralCON.Properties.ReadOnly = True
        Me.txSubTotalGeneralCON.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txSubTotalGeneralCON.Size = New System.Drawing.Size(98, 19)
        Me.txSubTotalGeneralCON.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label5.Location = New System.Drawing.Point(576, 40)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 16)
        Me.Label5.TabIndex = 87
        Me.Label5.Text = "SubTotal General"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btAceptar
        '
        Me.btAceptar.Location = New System.Drawing.Point(319, 202)
        Me.btAceptar.Name = "btAceptar"
        Me.btAceptar.Size = New System.Drawing.Size(146, 33)
        Me.btAceptar.TabIndex = 0
        Me.btAceptar.Text = "Aceptar"
        Me.btAceptar.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(8, 18)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 90
        Me.Label6.Text = "Apertura :"
        '
        'lbApertura
        '
        Me.lbApertura.AutoSize = True
        Me.lbApertura.BackColor = System.Drawing.Color.White
        Me.lbApertura.Location = New System.Drawing.Point(67, 18)
        Me.lbApertura.Name = "lbApertura"
        Me.lbApertura.Size = New System.Drawing.Size(0, 13)
        Me.lbApertura.TabIndex = 91
        '
        'txTotalGeneralCON
        '
        Me.txTotalGeneralCON.EditValue = "0.00"
        Me.txTotalGeneralCON.Location = New System.Drawing.Point(668, 72)
        Me.txTotalGeneralCON.Name = "txTotalGeneralCON"
        '
        '
        '
        Me.txTotalGeneralCON.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txTotalGeneralCON.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txTotalGeneralCON.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txTotalGeneralCON.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txTotalGeneralCON.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txTotalGeneralCON.Properties.ReadOnly = True
        Me.txTotalGeneralCON.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txTotalGeneralCON.Size = New System.Drawing.Size(98, 19)
        Me.txTotalGeneralCON.TabIndex = 93
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label7.Location = New System.Drawing.Point(678, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(82, 16)
        Me.Label7.TabIndex = 92
        Me.Label7.Text = "Total General"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txTotalGeneralCRE
        '
        Me.txTotalGeneralCRE.EditValue = "0.00"
        Me.txTotalGeneralCRE.Location = New System.Drawing.Point(668, 97)
        Me.txTotalGeneralCRE.Name = "txTotalGeneralCRE"
        '
        '
        '
        Me.txTotalGeneralCRE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txTotalGeneralCRE.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txTotalGeneralCRE.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txTotalGeneralCRE.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txTotalGeneralCRE.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txTotalGeneralCRE.Properties.ReadOnly = True
        Me.txTotalGeneralCRE.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txTotalGeneralCRE.Size = New System.Drawing.Size(98, 19)
        Me.txTotalGeneralCRE.TabIndex = 100
        '
        'txSubTotalGeneralCRE
        '
        Me.txSubTotalGeneralCRE.EditValue = "0.00"
        Me.txSubTotalGeneralCRE.Location = New System.Drawing.Point(571, 97)
        Me.txSubTotalGeneralCRE.Name = "txSubTotalGeneralCRE"
        '
        '
        '
        Me.txSubTotalGeneralCRE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txSubTotalGeneralCRE.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txSubTotalGeneralCRE.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSubTotalGeneralCRE.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txSubTotalGeneralCRE.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSubTotalGeneralCRE.Properties.ReadOnly = True
        Me.txSubTotalGeneralCRE.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txSubTotalGeneralCRE.Size = New System.Drawing.Size(98, 19)
        Me.txSubTotalGeneralCRE.TabIndex = 94
        '
        'txExtraPropinaCRE
        '
        Me.txExtraPropinaCRE.EditValue = "0.00"
        Me.txExtraPropinaCRE.Location = New System.Drawing.Point(474, 97)
        Me.txExtraPropinaCRE.Name = "txExtraPropinaCRE"
        '
        '
        '
        Me.txExtraPropinaCRE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txExtraPropinaCRE.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txExtraPropinaCRE.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txExtraPropinaCRE.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txExtraPropinaCRE.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txExtraPropinaCRE.Properties.ReadOnly = True
        Me.txExtraPropinaCRE.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txExtraPropinaCRE.Size = New System.Drawing.Size(98, 19)
        Me.txExtraPropinaCRE.TabIndex = 99
        '
        'txImpVentaCRE
        '
        Me.txImpVentaCRE.EditValue = "0.00"
        Me.txImpVentaCRE.Location = New System.Drawing.Point(377, 97)
        Me.txImpVentaCRE.Name = "txImpVentaCRE"
        '
        '
        '
        Me.txImpVentaCRE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txImpVentaCRE.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txImpVentaCRE.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txImpVentaCRE.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txImpVentaCRE.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txImpVentaCRE.Properties.ReadOnly = True
        Me.txImpVentaCRE.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txImpVentaCRE.Size = New System.Drawing.Size(98, 19)
        Me.txImpVentaCRE.TabIndex = 98
        '
        'txVentaExcentaCRE
        '
        Me.txVentaExcentaCRE.EditValue = "0.00"
        Me.txVentaExcentaCRE.Location = New System.Drawing.Point(183, 97)
        Me.txVentaExcentaCRE.Name = "txVentaExcentaCRE"
        '
        '
        '
        Me.txVentaExcentaCRE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txVentaExcentaCRE.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txVentaExcentaCRE.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaExcentaCRE.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txVentaExcentaCRE.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaExcentaCRE.Properties.ReadOnly = True
        Me.txVentaExcentaCRE.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txVentaExcentaCRE.Size = New System.Drawing.Size(98, 19)
        Me.txVentaExcentaCRE.TabIndex = 97
        '
        'txSaloneroCRE
        '
        Me.txSaloneroCRE.EditValue = "0.00"
        Me.txSaloneroCRE.Location = New System.Drawing.Point(280, 97)
        Me.txSaloneroCRE.Name = "txSaloneroCRE"
        '
        '
        '
        Me.txSaloneroCRE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txSaloneroCRE.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txSaloneroCRE.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSaloneroCRE.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txSaloneroCRE.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSaloneroCRE.Properties.ReadOnly = True
        Me.txSaloneroCRE.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txSaloneroCRE.Size = New System.Drawing.Size(98, 19)
        Me.txSaloneroCRE.TabIndex = 96
        '
        'txVentaGravadaCRE
        '
        Me.txVentaGravadaCRE.EditValue = "0.00"
        Me.txVentaGravadaCRE.Location = New System.Drawing.Point(86, 97)
        Me.txVentaGravadaCRE.Name = "txVentaGravadaCRE"
        '
        '
        '
        Me.txVentaGravadaCRE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txVentaGravadaCRE.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txVentaGravadaCRE.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaGravadaCRE.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txVentaGravadaCRE.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaGravadaCRE.Properties.ReadOnly = True
        Me.txVentaGravadaCRE.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txVentaGravadaCRE.Size = New System.Drawing.Size(98, 19)
        Me.txVentaGravadaCRE.TabIndex = 95
        '
        'txTotalGeneralCAR
        '
        Me.txTotalGeneralCAR.EditValue = "0.00"
        Me.txTotalGeneralCAR.Location = New System.Drawing.Point(668, 122)
        Me.txTotalGeneralCAR.Name = "txTotalGeneralCAR"
        '
        '
        '
        Me.txTotalGeneralCAR.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txTotalGeneralCAR.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txTotalGeneralCAR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txTotalGeneralCAR.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txTotalGeneralCAR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txTotalGeneralCAR.Properties.ReadOnly = True
        Me.txTotalGeneralCAR.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txTotalGeneralCAR.Size = New System.Drawing.Size(98, 19)
        Me.txTotalGeneralCAR.TabIndex = 107
        '
        'txSubTotalGeneralCAR
        '
        Me.txSubTotalGeneralCAR.EditValue = "0.00"
        Me.txSubTotalGeneralCAR.Location = New System.Drawing.Point(571, 122)
        Me.txSubTotalGeneralCAR.Name = "txSubTotalGeneralCAR"
        '
        '
        '
        Me.txSubTotalGeneralCAR.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txSubTotalGeneralCAR.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txSubTotalGeneralCAR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSubTotalGeneralCAR.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txSubTotalGeneralCAR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSubTotalGeneralCAR.Properties.ReadOnly = True
        Me.txSubTotalGeneralCAR.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txSubTotalGeneralCAR.Size = New System.Drawing.Size(98, 19)
        Me.txSubTotalGeneralCAR.TabIndex = 101
        '
        'txExtraPropinaCAR
        '
        Me.txExtraPropinaCAR.EditValue = "0.00"
        Me.txExtraPropinaCAR.Location = New System.Drawing.Point(474, 122)
        Me.txExtraPropinaCAR.Name = "txExtraPropinaCAR"
        '
        '
        '
        Me.txExtraPropinaCAR.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txExtraPropinaCAR.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txExtraPropinaCAR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txExtraPropinaCAR.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txExtraPropinaCAR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txExtraPropinaCAR.Properties.ReadOnly = True
        Me.txExtraPropinaCAR.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txExtraPropinaCAR.Size = New System.Drawing.Size(98, 19)
        Me.txExtraPropinaCAR.TabIndex = 106
        '
        'txImpVentaCAR
        '
        Me.txImpVentaCAR.EditValue = "0.00"
        Me.txImpVentaCAR.Location = New System.Drawing.Point(377, 122)
        Me.txImpVentaCAR.Name = "txImpVentaCAR"
        '
        '
        '
        Me.txImpVentaCAR.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txImpVentaCAR.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txImpVentaCAR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txImpVentaCAR.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txImpVentaCAR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txImpVentaCAR.Properties.ReadOnly = True
        Me.txImpVentaCAR.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txImpVentaCAR.Size = New System.Drawing.Size(98, 19)
        Me.txImpVentaCAR.TabIndex = 105
        '
        'txVentaExcentaCAR
        '
        Me.txVentaExcentaCAR.EditValue = "0.00"
        Me.txVentaExcentaCAR.Location = New System.Drawing.Point(183, 122)
        Me.txVentaExcentaCAR.Name = "txVentaExcentaCAR"
        '
        '
        '
        Me.txVentaExcentaCAR.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txVentaExcentaCAR.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txVentaExcentaCAR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaExcentaCAR.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txVentaExcentaCAR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaExcentaCAR.Properties.ReadOnly = True
        Me.txVentaExcentaCAR.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txVentaExcentaCAR.Size = New System.Drawing.Size(98, 19)
        Me.txVentaExcentaCAR.TabIndex = 104
        '
        'txSaloneroCAR
        '
        Me.txSaloneroCAR.EditValue = "0.00"
        Me.txSaloneroCAR.Location = New System.Drawing.Point(280, 122)
        Me.txSaloneroCAR.Name = "txSaloneroCAR"
        '
        '
        '
        Me.txSaloneroCAR.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txSaloneroCAR.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txSaloneroCAR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSaloneroCAR.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txSaloneroCAR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txSaloneroCAR.Properties.ReadOnly = True
        Me.txSaloneroCAR.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txSaloneroCAR.Size = New System.Drawing.Size(98, 19)
        Me.txSaloneroCAR.TabIndex = 103
        '
        'txVentaGravadaCAR
        '
        Me.txVentaGravadaCAR.EditValue = "0.00"
        Me.txVentaGravadaCAR.Location = New System.Drawing.Point(86, 122)
        Me.txVentaGravadaCAR.Name = "txVentaGravadaCAR"
        '
        '
        '
        Me.txVentaGravadaCAR.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txVentaGravadaCAR.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txVentaGravadaCAR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaGravadaCAR.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txVentaGravadaCAR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txVentaGravadaCAR.Properties.ReadOnly = True
        Me.txVentaGravadaCAR.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txVentaGravadaCAR.Size = New System.Drawing.Size(98, 19)
        Me.txVentaGravadaCAR.TabIndex = 102
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label8.Location = New System.Drawing.Point(-4, 40)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 16)
        Me.Label8.TabIndex = 108
        Me.Label8.Text = "Ventas"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label9.Location = New System.Drawing.Point(-5, 75)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 16)
        Me.Label9.TabIndex = 109
        Me.Label9.Text = "Contado"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label11.Location = New System.Drawing.Point(-2, 100)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 16)
        Me.Label11.TabIndex = 110
        Me.Label11.Text = "Credito"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label12.Location = New System.Drawing.Point(0, 125)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 16)
        Me.Label12.TabIndex = 111
        Me.Label12.Text = "Cargo Hab"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txTotalGeneral
        '
        Me.txTotalGeneral.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txTotalGeneral.ForeColor = System.Drawing.Color.Red
        Me.txTotalGeneral.Location = New System.Drawing.Point(668, 154)
        Me.txTotalGeneral.Name = "txTotalGeneral"
        Me.txTotalGeneral.Size = New System.Drawing.Size(98, 16)
        Me.txTotalGeneral.TabIndex = 112
        Me.txTotalGeneral.Text = "0"
        Me.txTotalGeneral.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txSubTotalGeneral
        '
        Me.txSubTotalGeneral.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txSubTotalGeneral.ForeColor = System.Drawing.Color.Red
        Me.txSubTotalGeneral.Location = New System.Drawing.Point(571, 154)
        Me.txSubTotalGeneral.Name = "txSubTotalGeneral"
        Me.txSubTotalGeneral.Size = New System.Drawing.Size(98, 16)
        Me.txSubTotalGeneral.TabIndex = 113
        Me.txSubTotalGeneral.Text = "0"
        Me.txSubTotalGeneral.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txExtraPropina
        '
        Me.txExtraPropina.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txExtraPropina.ForeColor = System.Drawing.Color.Red
        Me.txExtraPropina.Location = New System.Drawing.Point(474, 154)
        Me.txExtraPropina.Name = "txExtraPropina"
        Me.txExtraPropina.Size = New System.Drawing.Size(98, 16)
        Me.txExtraPropina.TabIndex = 114
        Me.txExtraPropina.Text = "0"
        Me.txExtraPropina.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txImpVenta
        '
        Me.txImpVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txImpVenta.ForeColor = System.Drawing.Color.Red
        Me.txImpVenta.Location = New System.Drawing.Point(377, 154)
        Me.txImpVenta.Name = "txImpVenta"
        Me.txImpVenta.Size = New System.Drawing.Size(98, 16)
        Me.txImpVenta.TabIndex = 115
        Me.txImpVenta.Text = "0"
        Me.txImpVenta.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txSalonero
        '
        Me.txSalonero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txSalonero.ForeColor = System.Drawing.Color.Red
        Me.txSalonero.Location = New System.Drawing.Point(280, 154)
        Me.txSalonero.Name = "txSalonero"
        Me.txSalonero.Size = New System.Drawing.Size(98, 16)
        Me.txSalonero.TabIndex = 116
        Me.txSalonero.Text = "0"
        Me.txSalonero.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txVentaExcenta
        '
        Me.txVentaExcenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txVentaExcenta.ForeColor = System.Drawing.Color.Red
        Me.txVentaExcenta.Location = New System.Drawing.Point(183, 154)
        Me.txVentaExcenta.Name = "txVentaExcenta"
        Me.txVentaExcenta.Size = New System.Drawing.Size(98, 16)
        Me.txVentaExcenta.TabIndex = 117
        Me.txVentaExcenta.Text = "0"
        Me.txVentaExcenta.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txVentaGravada
        '
        Me.txVentaGravada.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txVentaGravada.ForeColor = System.Drawing.Color.Red
        Me.txVentaGravada.Location = New System.Drawing.Point(101, 154)
        Me.txVentaGravada.Name = "txVentaGravada"
        Me.txVentaGravada.Size = New System.Drawing.Size(83, 16)
        Me.txVentaGravada.TabIndex = 118
        Me.txVentaGravada.Text = "0"
        Me.txVentaGravada.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Red
        Me.Label13.Location = New System.Drawing.Point(-4, 154)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(67, 16)
        Me.Label13.TabIndex = 119
        Me.Label13.Text = "Totales"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmTotalVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(778, 247)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txVentaGravada)
        Me.Controls.Add(Me.txVentaExcenta)
        Me.Controls.Add(Me.txSalonero)
        Me.Controls.Add(Me.txImpVenta)
        Me.Controls.Add(Me.txExtraPropina)
        Me.Controls.Add(Me.txSubTotalGeneral)
        Me.Controls.Add(Me.txTotalGeneral)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txTotalGeneralCAR)
        Me.Controls.Add(Me.txSubTotalGeneralCAR)
        Me.Controls.Add(Me.txExtraPropinaCAR)
        Me.Controls.Add(Me.txImpVentaCAR)
        Me.Controls.Add(Me.txVentaExcentaCAR)
        Me.Controls.Add(Me.txSaloneroCAR)
        Me.Controls.Add(Me.txVentaGravadaCAR)
        Me.Controls.Add(Me.txTotalGeneralCRE)
        Me.Controls.Add(Me.txSubTotalGeneralCRE)
        Me.Controls.Add(Me.txExtraPropinaCRE)
        Me.Controls.Add(Me.txImpVentaCRE)
        Me.Controls.Add(Me.txVentaExcentaCRE)
        Me.Controls.Add(Me.txSaloneroCRE)
        Me.Controls.Add(Me.txVentaGravadaCRE)
        Me.Controls.Add(Me.txTotalGeneralCON)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lbApertura)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btAceptar)
        Me.Controls.Add(Me.txSubTotalGeneralCON)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txExtraPropinaCON)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txImpVentaCON)
        Me.Controls.Add(Me.txVentaExcentaCON)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txSaloneroCON)
        Me.Controls.Add(Me.txVentaGravadaCON)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TituloModulo)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(794, 285)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(794, 285)
        Me.Name = "frmTotalVentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ventas"
        CType(Me.txSaloneroCON.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txVentaGravadaCON.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txImpVentaCON.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txVentaExcentaCON.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txExtraPropinaCON.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txSubTotalGeneralCON.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTotalGeneralCON.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTotalGeneralCRE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txSubTotalGeneralCRE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txExtraPropinaCRE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txImpVentaCRE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txVentaExcentaCRE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txSaloneroCRE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txVentaGravadaCRE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTotalGeneralCAR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txSubTotalGeneralCAR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txExtraPropinaCAR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txImpVentaCAR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txVentaExcentaCAR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txSaloneroCAR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txVentaGravadaCAR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents TituloModulo As System.Windows.Forms.Label
    Friend WithEvents txSaloneroCON As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txVentaGravadaCON As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txImpVentaCON As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txVentaExcentaCON As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txExtraPropinaCON As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txSubTotalGeneralCON As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btAceptar As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lbApertura As System.Windows.Forms.Label
    Friend WithEvents txTotalGeneralCON As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txTotalGeneralCRE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txSubTotalGeneralCRE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txExtraPropinaCRE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txImpVentaCRE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txVentaExcentaCRE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txSaloneroCRE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txVentaGravadaCRE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txTotalGeneralCAR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txSubTotalGeneralCAR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txExtraPropinaCAR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txImpVentaCAR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txVentaExcentaCAR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txSaloneroCAR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txVentaGravadaCAR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txSalonero As System.Windows.Forms.Label
    Friend WithEvents txVentaGravada As System.Windows.Forms.Label
    Friend WithEvents txImpVenta As System.Windows.Forms.Label
    Friend WithEvents txVentaExcenta As System.Windows.Forms.Label
    Friend WithEvents txExtraPropina As System.Windows.Forms.Label
    Friend WithEvents txSubTotalGeneral As System.Windows.Forms.Label
    Friend WithEvents txTotalGeneral As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class
