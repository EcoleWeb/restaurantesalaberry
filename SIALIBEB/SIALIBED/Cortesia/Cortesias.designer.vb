<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Cortesias
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Cortesias))
        Me.GBCentro = New System.Windows.Forms.GroupBox
        Me.txtCodigo = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtObservaciones = New System.Windows.Forms.TextBox
        Me.txtDescripcionCuenta = New System.Windows.Forms.TextBox
        Me.txtnombre = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtCuenta = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.Label12 = New System.Windows.Forms.Label
        Me.TituloModulo = New System.Windows.Forms.Label
        Me.lbUsuario = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtClave = New System.Windows.Forms.TextBox
        Me.ToolBarHabilitar = New System.Windows.Forms.ToolBarButton
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.GBCentro.SuspendLayout()
        Me.SuspendLayout()
        '
        'GBCentro
        '
        Me.GBCentro.BackColor = System.Drawing.SystemColors.Control
        Me.GBCentro.Controls.Add(Me.txtCodigo)
        Me.GBCentro.Controls.Add(Me.Label3)
        Me.GBCentro.Controls.Add(Me.Label1)
        Me.GBCentro.Controls.Add(Me.txtObservaciones)
        Me.GBCentro.Controls.Add(Me.txtDescripcionCuenta)
        Me.GBCentro.Controls.Add(Me.txtnombre)
        Me.GBCentro.Controls.Add(Me.Label9)
        Me.GBCentro.Controls.Add(Me.txtCuenta)
        Me.GBCentro.Controls.Add(Me.Label2)
        resources.ApplyResources(Me.GBCentro, "GBCentro")
        Me.GBCentro.Name = "GBCentro"
        Me.GBCentro.TabStop = False
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.Color.White
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtCodigo, "txtCodigo")
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.ReadOnly = True
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'txtObservaciones
        '
        resources.ApplyResources(Me.txtObservaciones, "txtObservaciones")
        Me.txtObservaciones.Name = "txtObservaciones"
        '
        'txtDescripcionCuenta
        '
        Me.txtDescripcionCuenta.BackColor = System.Drawing.Color.White
        Me.txtDescripcionCuenta.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescripcionCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtDescripcionCuenta, "txtDescripcionCuenta")
        Me.txtDescripcionCuenta.Name = "txtDescripcionCuenta"
        Me.txtDescripcionCuenta.ReadOnly = True
        '
        'txtnombre
        '
        Me.txtnombre.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtnombre, "txtnombre")
        Me.txtnombre.Name = "txtnombre"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'txtCuenta
        '
        Me.txtCuenta.BackColor = System.Drawing.Color.White
        Me.txtCuenta.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtCuenta, "txtCuenta")
        Me.txtCuenta.Name = "txtCuenta"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'ToolBar1
        '
        resources.ApplyResources(Me.ToolBar1, "ToolBar1")
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarEditar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarCerrar, Me.ToolBarHabilitar})
        Me.ToolBar1.ImageList = Me.ImageList
        Me.ToolBar1.Name = "ToolBar1"
        '
        'ToolBarNuevo
        '
        resources.ApplyResources(Me.ToolBarNuevo, "ToolBarNuevo")
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        '
        'ToolBarBuscar
        '
        resources.ApplyResources(Me.ToolBarBuscar, "ToolBarBuscar")
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        '
        'ToolBarEditar
        '
        resources.ApplyResources(Me.ToolBarEditar, "ToolBarEditar")
        Me.ToolBarEditar.Name = "ToolBarEditar"
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        '
        'ToolBarCerrar
        '
        resources.ApplyResources(Me.ToolBarCerrar, "ToolBarCerrar")
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "teclado1.gif")
        Me.ImageList.Images.SetKeyName(9, "")
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Name = "Label12"
        '
        'TituloModulo
        '
        Me.TituloModulo.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.TituloModulo, "TituloModulo")
        Me.TituloModulo.ForeColor = System.Drawing.Color.White
        Me.TituloModulo.Name = "TituloModulo"
        '
        'lbUsuario
        '
        Me.lbUsuario.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.lbUsuario, "lbUsuario")
        Me.lbUsuario.ForeColor = System.Drawing.Color.White
        Me.lbUsuario.Name = "lbUsuario"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'txtClave
        '
        Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtClave, "txtClave")
        Me.txtClave.Name = "txtClave"
        '
        'ToolBarHabilitar
        '
        resources.ApplyResources(Me.ToolBarHabilitar, "ToolBarHabilitar")
        Me.ToolBarHabilitar.Name = "ToolBarHabilitar"
        '
        'CheckBox1
        '
        resources.ApplyResources(Me.CheckBox1, "CheckBox1")
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Cortesias
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtClave)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.TituloModulo)
        Me.Controls.Add(Me.GBCentro)
        Me.Name = "Cortesias"
        Me.GBCentro.ResumeLayout(False)
        Me.GBCentro.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GBCentro As System.Windows.Forms.GroupBox
    Friend WithEvents txtCuenta As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Protected Friend WithEvents TituloModulo As System.Windows.Forms.Label
    Public WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Protected Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Public WithEvents ImageList As System.Windows.Forms.ImageList
    Friend WithEvents txtDescripcionCuenta As System.Windows.Forms.TextBox
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents ToolBarHabilitar As System.Windows.Forms.ToolBarButton
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
End Class
