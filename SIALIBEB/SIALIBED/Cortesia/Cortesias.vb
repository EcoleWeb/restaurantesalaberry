Imports System.Data.SqlClient
Imports System

Public Class Cortesias

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim PMU As New PerfilModulo_Class
    Dim cedula, usuario As String
#End Region

#Region "Load"
    Private Sub Cortesias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            conectadobd = cConexion.Conectar("Restaurante")
            Me.ToolBarHabilitar.ImageIndex = 10
            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO - ORA
           
            If gloNoClave Then
                Loggin_Usuario()
            Else
                txtClave.Focus()
            End If
           
            '---------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "ToolBar"
    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Nuevo()
            Case 2 : If PMU.Find Then Buscar() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : Editar()
            Case 4 : If PMU.Update Then Registrar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Delete Then Eliminar() Else MsgBox("No tiene permiso para Eliminar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 6 : Close()
            Case 7 : Me.habilitar()

        End Select
    End Sub
#End Region

#Region "Nuevo"
    Public Sub Nuevo()
        Try
            Limpiar()
            If ToolBarNuevo.Text = "Nuevo" Then
                ToolBarNuevo.ImageIndex = 10
                ToolBarNuevo.Text = "Cancelar"
                ToolBarEditar.Enabled = False
                ToolBarRegistrar.Enabled = True
                ToolBarEliminar.Enabled = False
                Me.ToolBarHabilitar.Enabled = False
                GBCentro.Enabled = True
                txtnombre.Focus()
            Else
                ToolBarNuevo.ImageIndex = 0
                ToolBarNuevo.Text = "Nuevo"
                ToolBarEditar.Enabled = False
                ToolBarRegistrar.Enabled = False
                ToolBarEliminar.Enabled = False
                Me.ToolBarHabilitar.Enabled = False
                GBCentro.Enabled = False
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Buscar"
    Public Sub Buscar()
        Try
            Dim rs As SqlClient.SqlDataReader
            Dim fbuscador As New buscador

            fbuscador.check = 1
            fbuscador.ToolBarImprimir.Enabled = False
            fbuscador.TituloModulo.Text = "Buscando Centro de Cortesia"
            fbuscador.bd = "Restaurante" : fbuscador.tabla = "Cortesia_Centro"
            fbuscador.busca = "Nombre" : fbuscador.cantidad = 3 : fbuscador.lblEncabezado.Text = "Cortes�as"
            fbuscador.consulta = "SELECT Id_Centro, Nombre, Observaciones FROM Cortesia_Centros"
            fbuscador.ShowDialog()

            If fbuscador.Icodigo = 0 Then
                Exit Sub
            End If

            Limpiar()
            txtCodigo.Text = fbuscador.Icodigo
            cConexion.GetRecorset(conectadobd, "Select Nombre, ISNULL(CuentaContable,'') AS CuentaContable, ISNULL(DescripcionCuenta,'') AS DescripcionCuenta, Observaciones,Inhabilitado FROM Cortesia_Centros WHERE id_Centro =" & txtCodigo.Text, rs)
            While rs.Read
                txtnombre.Text = rs("Nombre")
                txtCuenta.Text = rs("CuentaContable")
                txtDescripcionCuenta.Text = rs("DescripcionCuenta")
                txtObservaciones.Text = rs("Observaciones")
                Me.CheckBox1.Checked = rs("Inhabilitado")
            End While
            rs.Close()
            If Me.CheckBox1.Checked = False Then
                Me.ToolBarHabilitar.Text = "InHabilitar"
            Else
                Me.ToolBarHabilitar.Text = "Habilitar"
            End If
            ToolBarEditar.ImageIndex = 1
            ToolBarEditar.Text = "Editar"
            ToolBarNuevo.ImageIndex = 0
            ToolBarNuevo.Text = "Nuevo"
            ToolBarNuevo.Enabled = True
            ToolBarRegistrar.Enabled = False
            ToolBarEditar.Enabled = True
            ToolBarEliminar.Enabled = True
            Me.ToolBarHabilitar.Enabled = True
            GBCentro.Enabled = False

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
#End Region
#Region "Habilitar"
    Sub habilitar()
        If Trim(txtCodigo.Text) = vbNullString Then
            Exit Sub
        Else
            Dim inh As Integer
            If Me.CheckBox1.Checked = False Then
                inh = 1
            Else
                inh = 0
            End If
            cConexion.UpdateRecords(conectadobd, "Cortesia_Centros", "Inhabilitado = " & inh, " Id_Centro=" & txtCodigo.Text)
            ToolBarEditar.ImageIndex = 1
            ToolBarEditar.Text = "Editar"
            ToolBarNuevo.ImageIndex = 0
            ToolBarNuevo.Text = "Nuevo"
            ToolBarNuevo.Enabled = True
            ToolBarRegistrar.Enabled = False
            GBCentro.Enabled = False
            Me.ToolBarEliminar.Enabled = False
            Me.ToolBarHabilitar.Text = "InHabilitar"
            Me.ToolBarHabilitar.Enabled = False
            Limpiar()
            MsgBox("Centro de cortesia Actualizado Satisfactoriamente", MsgBoxStyle.Information)
        End If

    End Sub
#End Region

#Region "Editar"
    Public Sub Editar()
        Try
            If ToolBarEditar.Text = "Editar" Then
                ToolBarEditar.ImageIndex = 10
                ToolBarEditar.Text = "Cancelar"
                ToolBarNuevo.Enabled = False
                ToolBarRegistrar.Enabled = True
                ToolBarEliminar.Enabled = False
                GBCentro.Enabled = True
                txtnombre.Focus()
            Else
                ToolBarEditar.ImageIndex = 1
                ToolBarEditar.Text = "Editar"
                ToolBarNuevo.Enabled = True
                ToolBarRegistrar.Enabled = False
                ToolBarEliminar.Enabled = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Registrar"
    Private Sub Registrar()
        Try
            If Trim(txtnombre.Text) = vbNullString Or Trim(txtCuenta.Text) = vbNullString Or Trim(txtDescripcionCuenta.Text) = vbNullString Then
                MessageBox.Show("Rellene todos los datos")
                Exit Sub
            End If

            If Trim(txtCodigo.Text) = vbNullString Then
                cConexion.AddNewRecord(conectadobd, "Cortesia_Centros", "Nombre,CuentaContable,DescripcionCuenta,Observaciones,Cedula, Usuario", "'" & txtnombre.Text & "','" & txtCuenta.Text & "','" & txtDescripcionCuenta.Text & "','" & txtObservaciones.Text & "','" & cedula & "','" & usuario & "'")
                ToolBarEditar.Enabled = False
                ToolBarEliminar.Enabled = False
                Limpiar()
            Else
                cConexion.UpdateRecords(conectadobd, "Cortesia_Centros", "Nombre='" & txtnombre.Text & "',Cedula='" & cedula & "',Usuario='" & usuario & "',CuentaContable='" & txtCuenta.Text & "',DescripcionCuenta = '" & txtDescripcionCuenta.Text & "',Observaciones= '" & txtObservaciones.Text & "'", " Id_Centro=" & txtCodigo.Text)
                ToolBarEditar.Enabled = True
                ToolBarEliminar.Enabled = True
            End If

            ToolBarEditar.ImageIndex = 1
            ToolBarEditar.Text = "Editar"
            ToolBarNuevo.ImageIndex = 0
            ToolBarNuevo.Text = "Nuevo"
            ToolBarNuevo.Enabled = True
            ToolBarRegistrar.Enabled = False
            GBCentro.Enabled = False
            MsgBox("Centro de cortesia registrado Satisfactoriamente", MsgBoxStyle.Information)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
#End Region

#Region "Eliminar"
    Private Sub Eliminar()
        Dim cant As Integer = 0
        Try
            If Trim(txtCodigo.Text) = vbNullString Then
                Exit Sub
            Else
                cant = cConexion.SlqExecuteScalar(Me.conectadobd, "SELECT COUNT(Id_Cortesia) AS Pendientes FROM Cortesia  WHERE  (Id_Centro = " & Me.txtCodigo.Text & ")")
                If cant = 0 Then
                    cConexion.SlqExecute(conectadobd, "Delete FROM Cortesia_Centros WHERE Id_Centro =" & txtCodigo.Text)
                    Limpiar()
                    ToolBarEditar.ImageIndex = 1
                    ToolBarEditar.Text = "Editar"
                    ToolBarNuevo.ImageIndex = 0
                    ToolBarNuevo.Text = "Nuevo"
                    ToolBarNuevo.Enabled = True
                    ToolBarEditar.Enabled = False
                    ToolBarRegistrar.Enabled = False
                    ToolBarEliminar.Enabled = False
                    Me.ToolBarHabilitar.Enabled = False
                    GBCentro.Enabled = False
                Else
                    MsgBox("No se puede eliminar centro de cortesia ", MsgBoxStyle.Critical)
                End If
              
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
#End Region

#Region "Cerrar"
    Private Sub Cortesia_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
        Me.Dispose()
    End Sub
#End Region

#Region "Funciones"
    Private Sub Limpiar()
        txtCodigo.Text = ""
        txtnombre.Text = ""
        txtCuenta.Text = ""
        txtDescripcionCuenta.Text = ""
        txtObservaciones.Text = ""
        Me.CheckBox1.Checked = False
    End Sub

#Region "Cuenta Contable"
    Private Sub buscacuenta(ByVal a1 As Object, ByVal a2 As Object)
        'Buscar Cuenta Contable
        Dim Fbuscador As New buscador
        Dim Conexion As New ConexionR
        Fbuscador.cantidad = 4
        Fbuscador.bd = "Contabilidad"
        Fbuscador.tabla = "CuentaContable"
        Fbuscador.lblEncabezado.Text = "          Buscador de Cuentas"
        Fbuscador.TituloModulo.Text = "Cuentas Contables"
        Fbuscador.consulta = "Select CuentaContable, Descripcion, Tipo, CuentaMadre from CuentaContable where Movimiento=1"
        Fbuscador.ShowDialog()
        a1.Text = Fbuscador.cuenta
        a2.Text = Conexion.SlqExecuteScalar(Conexion.Conectar("Contabilidad"), "Select Descripcion from contabilidad.dbo.cuentacontable where cuentaContable='" & Fbuscador.cuenta & "'")
        Fbuscador.Dispose()
    End Sub
#End Region

#End Region

#Region "Funciones Controles"
    Private Sub txtnombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtnombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtCuenta.Focus()
        End If
    End Sub

    Private Sub txtCuentaIngreso_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCuenta.KeyDown
        If e.KeyCode = Keys.F1 Then
            buscacuenta(txtCuenta, txtDescripcionCuenta)
        End If
    End Sub

    Private Sub txtCuentaIngreso_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCuenta.KeyPress
        If e.KeyChar = Chr(13) Then
            Dim datos As String
            Dim Conexion As New ConexionR

            Try
                datos = Conexion.SlqExecuteScalar(Conexion.Conectar("Contabilidad"), "Select Descripcion from contabilidad.dbo.cuentacontable where cuentaContable='" & txtCuenta.Text & "'")
                If datos <> vbNullString Then
                    txtDescripcionCuenta.Text = datos
                    txtObservaciones.Focus()
                Else
                    MessageBox.Show("Cuenta Contable incorrecta")
                    txtCuenta.Focus()
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            End Try
        End If
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & txtClave.Text & "'")
                usuario = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

                If usuario <> "" Then
                    txtClave.Text = ""
                    Me.lbUsuario.Text = usuario
                    PMU = VSM(cedula, Me.Name)
                    ToolBarNuevo.Enabled = True
                    ToolBarBuscar.Enabled = True
                End If

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            End Try
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                txtClave.Text = ""
                ToolBarNuevo.Enabled = True
                ToolBarBuscar.Enabled = True
                cedula = User_Log.Cedula
                usuario = User_Log.Nombre
                Me.lbUsuario.Text = User_Log.Nombre
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub txtCuenta_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuenta.TextChanged

    End Sub
End Class