Imports System.data.SqlClient
Imports CrystalDecisions.Shared


Public Class frmEstado_CXC
    Inherits System.Windows.Forms.Form
    Dim Contador As Byte

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents rptViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Moneda As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonMostrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarGeneral As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarVencidas As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarPagadas As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarAntiguedad As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarSaldos As System.Windows.Forms.ToolBarButton
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents ToolBarMovFac As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarMovCuenta As System.Windows.Forms.ToolBarButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ProgressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DataSet_Estado_CxC As DataSet_Estado_CxC
    Friend WithEvents ToolBarVencidasFecha As System.Windows.Forms.ToolBarButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents opEmpleados As System.Windows.Forms.RadioButton
    Friend WithEvents opAgencias As System.Windows.Forms.RadioButton
    Friend WithEvents SqlDataAdapter1 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Dts_SaldoCxC1 As SIALIBEB.dts_SaldoCxC
    Friend WithEvents SqlDataAdapter2 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDataAdapter3 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents opTodos As System.Windows.Forms.RadioButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEstado_CXC))
        Me.rptViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.AdapterMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.Moneda = New System.Windows.Forms.ComboBox
        Me.DataSet_Estado_CxC = New SIALIBEB.DataSet_Estado_CxC
        Me.ButtonMostrar = New DevExpress.XtraEditors.SimpleButton
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarGeneral = New System.Windows.Forms.ToolBarButton
        Me.ToolBarVencidas = New System.Windows.Forms.ToolBarButton
        Me.ToolBarPagadas = New System.Windows.Forms.ToolBarButton
        Me.ToolBarMovFac = New System.Windows.Forms.ToolBarButton
        Me.ToolBarMovCuenta = New System.Windows.Forms.ToolBarButton
        Me.ToolBarAntiguedad = New System.Windows.Forms.ToolBarButton
        Me.ToolBarSaldos = New System.Windows.Forms.ToolBarButton
        Me.ToolBarVencidasFecha = New System.Windows.Forms.ToolBarButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.opTodos = New System.Windows.Forms.RadioButton
        Me.opAgencias = New System.Windows.Forms.RadioButton
        Me.opEmpleados = New System.Windows.Forms.RadioButton
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.ProgressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SqlDataAdapter1 = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.Dts_SaldoCxC1 = New SIALIBEB.dts_SaldoCxC
        Me.SqlDataAdapter2 = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand = New System.Data.SqlClient.SqlCommand
        Me.SqlDataAdapter3 = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand6 = New System.Data.SqlClient.SqlCommand
        CType(Me.DataSet_Estado_CxC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dts_SaldoCxC1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'rptViewer
        '
        Me.rptViewer.ActiveViewIndex = -1
        Me.rptViewer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rptViewer.AutoScroll = True
        Me.rptViewer.BackColor = System.Drawing.SystemColors.Control
        Me.rptViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rptViewer.DisplayGroupTree = False
        Me.rptViewer.Location = New System.Drawing.Point(112, 32)
        Me.rptViewer.Name = "rptViewer"
        Me.rptViewer.SelectionFormula = ""
        Me.rptViewer.ShowRefreshButton = False
        Me.rptViewer.Size = New System.Drawing.Size(624, 436)
        Me.rptViewer.TabIndex = 75
        Me.rptViewer.ViewTimeSelectionFormula = ""
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=SEESOFT_PC;Initial Catalog=Hotel;Integrated Security=True"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'AdapterMoneda
        '
        Me.AdapterMoneda.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, Simbolo FROM Moneda"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'CheckBox1
        '
        Me.CheckBox1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CheckBox1.Enabled = False
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CheckBox1.Location = New System.Drawing.Point(8, 224)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(96, 32)
        Me.CheckBox1.TabIndex = 29
        Me.CheckBox1.Text = "Filtrar Incobrables"
        Me.CheckBox1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 12)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "ID Cliente"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.ForeColor = System.Drawing.Color.Blue
        Me.TextBox1.Location = New System.Drawing.Point(8, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(96, 13)
        Me.TextBox1.TabIndex = 27
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(8, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 12)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Fecha Corte"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(8, 110)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker1.TabIndex = 23
        Me.DateTimePicker1.Value = New Date(2006, 5, 31, 0, 0, 0, 0)
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(8, 264)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 12)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Ver en Moneda"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Moneda
        '
        Me.Moneda.DataSource = Me.DataSet_Estado_CxC
        Me.Moneda.DisplayMember = "Moneda.MonedaNombre"
        Me.Moneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Moneda.Location = New System.Drawing.Point(8, 280)
        Me.Moneda.Name = "Moneda"
        Me.Moneda.Size = New System.Drawing.Size(96, 21)
        Me.Moneda.TabIndex = 21
        Me.Moneda.ValueMember = "Moneda.CodMoneda"
        '
        'DataSet_Estado_CxC
        '
        Me.DataSet_Estado_CxC.DataSetName = "DataSet_Estado_CxC"
        Me.DataSet_Estado_CxC.Locale = New System.Globalization.CultureInfo("es-CR")
        Me.DataSet_Estado_CxC.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ButtonMostrar
        '
        Me.ButtonMostrar.Location = New System.Drawing.Point(16, 312)
        Me.ButtonMostrar.Name = "ButtonMostrar"
        Me.ButtonMostrar.Size = New System.Drawing.Size(80, 24)
        Me.ButtonMostrar.TabIndex = 20
        Me.ButtonMostrar.Text = "Mostrar"
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarGeneral, Me.ToolBarVencidas, Me.ToolBarPagadas, Me.ToolBarMovFac, Me.ToolBarMovCuenta, Me.ToolBarAntiguedad, Me.ToolBarSaldos, Me.ToolBarVencidasFecha})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(64, 30)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(742, 30)
        Me.ToolBar1.TabIndex = 80
        Me.ToolBar1.TextAlign = System.Windows.Forms.ToolBarTextAlign.Right
        '
        'ToolBarGeneral
        '
        Me.ToolBarGeneral.Name = "ToolBarGeneral"
        Me.ToolBarGeneral.Pushed = True
        Me.ToolBarGeneral.Text = "General"
        '
        'ToolBarVencidas
        '
        Me.ToolBarVencidas.Name = "ToolBarVencidas"
        Me.ToolBarVencidas.Text = "Vencidas"
        Me.ToolBarVencidas.Visible = False
        '
        'ToolBarPagadas
        '
        Me.ToolBarPagadas.Name = "ToolBarPagadas"
        Me.ToolBarPagadas.Text = "Pagadas"
        Me.ToolBarPagadas.Visible = False
        '
        'ToolBarMovFac
        '
        Me.ToolBarMovFac.Name = "ToolBarMovFac"
        Me.ToolBarMovFac.Text = "Mov. Fac"
        Me.ToolBarMovFac.Visible = False
        '
        'ToolBarMovCuenta
        '
        Me.ToolBarMovCuenta.Name = "ToolBarMovCuenta"
        Me.ToolBarMovCuenta.Text = "Mov. Ctas"
        Me.ToolBarMovCuenta.Visible = False
        '
        'ToolBarAntiguedad
        '
        Me.ToolBarAntiguedad.Name = "ToolBarAntiguedad"
        Me.ToolBarAntiguedad.Text = "Anti. Saldos"
        Me.ToolBarAntiguedad.Visible = False
        '
        'ToolBarSaldos
        '
        Me.ToolBarSaldos.Name = "ToolBarSaldos"
        Me.ToolBarSaldos.Text = "Saldos Gen."
        Me.ToolBarSaldos.Visible = False
        '
        'ToolBarVencidasFecha
        '
        Me.ToolBarVencidasFecha.Name = "ToolBarVencidasFecha"
        Me.ToolBarVencidasFecha.Text = "Fac. Venc."
        Me.ToolBarVencidasFecha.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.DateTimePicker2)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.DateTimePicker1)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Moneda)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.ButtonMostrar)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Location = New System.Drawing.Point(0, 30)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(112, 436)
        Me.Panel1.TabIndex = 81
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.opTodos)
        Me.GroupBox1.Controls.Add(Me.opAgencias)
        Me.GroupBox1.Controls.Add(Me.opEmpleados)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.GroupBox1.Location = New System.Drawing.Point(8, 136)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(96, 80)
        Me.GroupBox1.TabIndex = 83
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Opciones"
        Me.GroupBox1.Visible = False
        '
        'opTodos
        '
        Me.opTodos.Checked = True
        Me.opTodos.Location = New System.Drawing.Point(5, 57)
        Me.opTodos.Name = "opTodos"
        Me.opTodos.Size = New System.Drawing.Size(84, 16)
        Me.opTodos.TabIndex = 2
        Me.opTodos.TabStop = True
        Me.opTodos.Text = "Todos"
        '
        'opAgencias
        '
        Me.opAgencias.Location = New System.Drawing.Point(4, 36)
        Me.opAgencias.Name = "opAgencias"
        Me.opAgencias.Size = New System.Drawing.Size(84, 16)
        Me.opAgencias.TabIndex = 1
        Me.opAgencias.Text = "Agencias"
        '
        'opEmpleados
        '
        Me.opEmpleados.Location = New System.Drawing.Point(4, 16)
        Me.opEmpleados.Name = "opEmpleados"
        Me.opEmpleados.Size = New System.Drawing.Size(84, 16)
        Me.opEmpleados.TabIndex = 0
        Me.opEmpleados.Text = "Empleados"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Enabled = False
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(8, 70)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker2.TabIndex = 30
        Me.DateTimePicker2.Value = New Date(2006, 5, 2, 0, 0, 0, 0)
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(8, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 12)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Fecha Inicio"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(32, 360)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 23)
        Me.Label5.TabIndex = 82
        Me.Label5.Text = "Label5"
        '
        'ProgressBarControl1
        '
        Me.ProgressBarControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBarControl1.Location = New System.Drawing.Point(492, 40)
        Me.ProgressBarControl1.Name = "ProgressBarControl1"
        '
        '
        '
        Me.ProgressBarControl1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.ProgressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid
        Me.ProgressBarControl1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.WindowText)
        Me.ProgressBarControl1.Size = New System.Drawing.Size(120, 16)
        Me.ProgressBarControl1.TabIndex = 32
        Me.ProgressBarControl1.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 50
        '
        'SqlDataAdapter1
        '
        Me.SqlDataAdapter1.SelectCommand = Me.SqlCommand1
        Me.SqlDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Cliente", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Agencia", "Agencia"), New System.Data.Common.DataColumnMapping("Contacto", "Contacto"), New System.Data.Common.DataColumnMapping("Nacionalidad", "Nacionalidad"), New System.Data.Common.DataColumnMapping("Exento", "Exento"), New System.Data.Common.DataColumnMapping("ImpustoVentas", "ImpustoVentas"), New System.Data.Common.DataColumnMapping("Credito", "Credito"), New System.Data.Common.DataColumnMapping("Telefono1", "Telefono1"), New System.Data.Common.DataColumnMapping("Telefono2", "Telefono2"), New System.Data.Common.DataColumnMapping("Fax1", "Fax1"), New System.Data.Common.DataColumnMapping("Fax2", "Fax2"), New System.Data.Common.DataColumnMapping("Email", "Email"), New System.Data.Common.DataColumnMapping("Dereccion", "Dereccion"), New System.Data.Common.DataColumnMapping("Limite_Credito", "Limite_Credito"), New System.Data.Common.DataColumnMapping("Plazo_Credito", "Plazo_Credito"), New System.Data.Common.DataColumnMapping("Tipo_Precio", "Tipo_Precio"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Restriccion_Cuenta", "Restriccion_Cuenta"), New System.Data.Common.DataColumnMapping("Comicion", "Comicion"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Cliente_Moroso", "Cliente_Moroso"), New System.Data.Common.DataColumnMapping("NombreJuridico", "NombreJuridico"), New System.Data.Common.DataColumnMapping("CodigoSeguridad", "CodigoSeguridad"), New System.Data.Common.DataColumnMapping("NumeroTarjeta", "NumeroTarjeta"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo"), New System.Data.Common.DataColumnMapping("Vence", "Vence"), New System.Data.Common.DataColumnMapping("Id_Contrato", "Id_Contrato"), New System.Data.Common.DataColumnMapping("Contrato", "Contrato"), New System.Data.Common.DataColumnMapping("Desayunos", "Desayunos"), New System.Data.Common.DataColumnMapping("TipoPrecio", "TipoPrecio"), New System.Data.Common.DataColumnMapping("Descuento", "Descuento"), New System.Data.Common.DataColumnMapping("ComisionServicios", "ComisionServicios"), New System.Data.Common.DataColumnMapping("ComisionPaquetes", "ComisionPaquetes"), New System.Data.Common.DataColumnMapping("EsEmpleado", "EsEmpleado"), New System.Data.Common.DataColumnMapping("Id_Empleado", "Id_Empleado"), New System.Data.Common.DataColumnMapping("Empleado", "Empleado"), New System.Data.Common.DataColumnMapping("SaldoApartado", "SaldoApartado"), New System.Data.Common.DataColumnMapping("SaldoCanjeReal", "SaldoCanjeReal"), New System.Data.Common.DataColumnMapping("IDInternet", "IDInternet"), New System.Data.Common.DataColumnMapping("PassInternet", "PassInternet")})})
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "SELECT        Cliente.*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM            Cliente"
        Me.SqlCommand1.Connection = Me.SqlConnection1
        '
        'Dts_SaldoCxC1
        '
        Me.Dts_SaldoCxC1.DataSetName = "dts_SaldoCxC"
        Me.Dts_SaldoCxC1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SqlDataAdapter2
        '
        Me.SqlDataAdapter2.DeleteCommand = Me.SqlDeleteCommand
        Me.SqlDataAdapter2.InsertCommand = Me.SqlInsertCommand
        Me.SqlDataAdapter2.SelectCommand = Me.SqlCommand2
        Me.SqlDataAdapter2.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "configuraciones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Empresa", "Empresa"), New System.Data.Common.DataColumnMapping("Tel_01", "Tel_01"), New System.Data.Common.DataColumnMapping("Tel_02", "Tel_02"), New System.Data.Common.DataColumnMapping("Fax_01", "Fax_01"), New System.Data.Common.DataColumnMapping("Fax_02", "Fax_02"), New System.Data.Common.DataColumnMapping("Direccion", "Direccion"), New System.Data.Common.DataColumnMapping("Imp_Venta", "Imp_Venta"), New System.Data.Common.DataColumnMapping("Frase", "Frase"), New System.Data.Common.DataColumnMapping("Imp_Servicio", "Imp_Servicio"), New System.Data.Common.DataColumnMapping("Logo", "Logo"), New System.Data.Common.DataColumnMapping("Intereses", "Intereses"), New System.Data.Common.DataColumnMapping("Imp_ICT", "Imp_ICT"), New System.Data.Common.DataColumnMapping("Total_Dias_Cancelar", "Total_Dias_Cancelar"), New System.Data.Common.DataColumnMapping("Email", "Email"), New System.Data.Common.DataColumnMapping("Dirrecion_Web", "Dirrecion_Web"), New System.Data.Common.DataColumnMapping("Comision_Habitaciones", "Comision_Habitaciones"), New System.Data.Common.DataColumnMapping("Comision_Servicio", "Comision_Servicio"), New System.Data.Common.DataColumnMapping("Comision_Restaurant", "Comision_Restaurant"), New System.Data.Common.DataColumnMapping("Cuenta_Bancaria1", "Cuenta_Bancaria1"), New System.Data.Common.DataColumnMapping("Cuenta_Bancaria2", "Cuenta_Bancaria2"), New System.Data.Common.DataColumnMapping("Impresora_Facturacion", "Impresora_Facturacion"), New System.Data.Common.DataColumnMapping("Maximo_Descuento", "Maximo_Descuento"), New System.Data.Common.DataColumnMapping("PersonaJuridica", "PersonaJuridica"), New System.Data.Common.DataColumnMapping("CantidadHabitaciones", "CantidadHabitaciones"), New System.Data.Common.DataColumnMapping("ConsecutivoUnico", "ConsecutivoUnico"), New System.Data.Common.DataColumnMapping("Desayunos", "Desayunos"), New System.Data.Common.DataColumnMapping("Imp_Servicio_Habitacion", "Imp_Servicio_Habitacion"), New System.Data.Common.DataColumnMapping("UnicoEncargadoXMesa", "UnicoEncargadoXMesa"), New System.Data.Common.DataColumnMapping("MontoApertura", "MontoApertura"), New System.Data.Common.DataColumnMapping("MonedaRestaurante", "MonedaRestaurante"), New System.Data.Common.DataColumnMapping("OpcionFacturacion", "OpcionFacturacion"), New System.Data.Common.DataColumnMapping("Prefactura", "Prefactura"), New System.Data.Common.DataColumnMapping("monedaPrefactura", "monedaPrefactura"), New System.Data.Common.DataColumnMapping("MonedaSistema", "MonedaSistema"), New System.Data.Common.DataColumnMapping("utilizaGrupo", "utilizaGrupo"), New System.Data.Common.DataColumnMapping("IncluirServicios", "IncluirServicios"), New System.Data.Common.DataColumnMapping("NinosGratisHab", "NinosGratisHab"), New System.Data.Common.DataColumnMapping("PedirTarjeta", "PedirTarjeta"), New System.Data.Common.DataColumnMapping("General", "General"), New System.Data.Common.DataColumnMapping("TIPOFACT", "TIPOFACT"), New System.Data.Common.DataColumnMapping("FormatoCheck", "FormatoCheck"), New System.Data.Common.DataColumnMapping("TipoPlanning", "TipoPlanning"), New System.Data.Common.DataColumnMapping("AsignarHab", "AsignarHab"), New System.Data.Common.DataColumnMapping("PoliticasCancelacion", "PoliticasCancelacion"), New System.Data.Common.DataColumnMapping("AsignarHabAlloment", "AsignarHabAlloment"), New System.Data.Common.DataColumnMapping("PersonaporHab", "PersonaporHab"), New System.Data.Common.DataColumnMapping("Hora_Inicio", "Hora_Inicio"), New System.Data.Common.DataColumnMapping("Logo2", "Logo2"), New System.Data.Common.DataColumnMapping("Auditoria", "Auditoria"), New System.Data.Common.DataColumnMapping("Moneda_Vuelto", "Moneda_Vuelto"), New System.Data.Common.DataColumnMapping("Contabilidad", "Contabilidad"), New System.Data.Common.DataColumnMapping("PolCancelESP", "PolCancelESP"), New System.Data.Common.DataColumnMapping("PolCancelING", "PolCancelING"), New System.Data.Common.DataColumnMapping("PolSoloPREP", "PolSoloPREP"), New System.Data.Common.DataColumnMapping("MiniBar", "MiniBar"), New System.Data.Common.DataColumnMapping("NoClave", "NoClave"), New System.Data.Common.DataColumnMapping("CostoCortesia", "CostoCortesia"), New System.Data.Common.DataColumnMapping("facturaDirecta", "facturaDirecta")})})
        Me.SqlDataAdapter2.UpdateCommand = Me.SqlUpdateCommand
        '
        'SqlDeleteCommand
        '
        Me.SqlDeleteCommand.CommandText = "DELETE FROM [configuraciones] WHERE (([Cedula] = @Original_Cedula))"
        Me.SqlDeleteCommand.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand
        '
        Me.SqlInsertCommand.CommandText = resources.GetString("SqlInsertCommand.CommandText")
        Me.SqlInsertCommand.Connection = Me.SqlConnection1
        Me.SqlInsertCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 0, "Cedula"), New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 0, "Empresa"), New System.Data.SqlClient.SqlParameter("@Tel_01", System.Data.SqlDbType.VarChar, 0, "Tel_01"), New System.Data.SqlClient.SqlParameter("@Tel_02", System.Data.SqlDbType.VarChar, 0, "Tel_02"), New System.Data.SqlClient.SqlParameter("@Fax_01", System.Data.SqlDbType.VarChar, 0, "Fax_01"), New System.Data.SqlClient.SqlParameter("@Fax_02", System.Data.SqlDbType.VarChar, 0, "Fax_02"), New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 0, "Direccion"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 0, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Frase", System.Data.SqlDbType.VarChar, 0, "Frase"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio", System.Data.SqlDbType.Float, 0, "Imp_Servicio"), New System.Data.SqlClient.SqlParameter("@Logo", System.Data.SqlDbType.Image, 0, "Logo"), New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Int, 0, "Intereses"), New System.Data.SqlClient.SqlParameter("@Imp_ICT", System.Data.SqlDbType.Float, 0, "Imp_ICT"), New System.Data.SqlClient.SqlParameter("@Total_Dias_Cancelar", System.Data.SqlDbType.Int, 0, "Total_Dias_Cancelar"), New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 0, "Email"), New System.Data.SqlClient.SqlParameter("@Dirrecion_Web", System.Data.SqlDbType.VarChar, 0, "Dirrecion_Web"), New System.Data.SqlClient.SqlParameter("@Comision_Habitaciones", System.Data.SqlDbType.Bit, 0, "Comision_Habitaciones"), New System.Data.SqlClient.SqlParameter("@Comision_Servicio", System.Data.SqlDbType.Bit, 0, "Comision_Servicio"), New System.Data.SqlClient.SqlParameter("@Comision_Restaurant", System.Data.SqlDbType.Bit, 0, "Comision_Restaurant"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria1", System.Data.SqlDbType.VarChar, 0, "Cuenta_Bancaria1"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria2", System.Data.SqlDbType.VarChar, 0, "Cuenta_Bancaria2"), New System.Data.SqlClient.SqlParameter("@Impresora_Facturacion", System.Data.SqlDbType.VarChar, 0, "Impresora_Facturacion"), New System.Data.SqlClient.SqlParameter("@Maximo_Descuento", System.Data.SqlDbType.Float, 0, "Maximo_Descuento"), New System.Data.SqlClient.SqlParameter("@PersonaJuridica", System.Data.SqlDbType.VarChar, 0, "PersonaJuridica"), New System.Data.SqlClient.SqlParameter("@CantidadHabitaciones", System.Data.SqlDbType.Int, 0, "CantidadHabitaciones"), New System.Data.SqlClient.SqlParameter("@ConsecutivoUnico", System.Data.SqlDbType.Bit, 0, "ConsecutivoUnico"), New System.Data.SqlClient.SqlParameter("@Desayunos", System.Data.SqlDbType.Bit, 0, "Desayunos"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio_Habitacion", System.Data.SqlDbType.Float, 0, "Imp_Servicio_Habitacion"), New System.Data.SqlClient.SqlParameter("@UnicoEncargadoXMesa", System.Data.SqlDbType.Bit, 0, "UnicoEncargadoXMesa"), New System.Data.SqlClient.SqlParameter("@MontoApertura", System.Data.SqlDbType.Bit, 0, "MontoApertura"), New System.Data.SqlClient.SqlParameter("@MonedaRestaurante", System.Data.SqlDbType.SmallInt, 0, "MonedaRestaurante"), New System.Data.SqlClient.SqlParameter("@OpcionFacturacion", System.Data.SqlDbType.Bit, 0, "OpcionFacturacion"), New System.Data.SqlClient.SqlParameter("@Prefactura", System.Data.SqlDbType.Float, 0, "Prefactura"), New System.Data.SqlClient.SqlParameter("@monedaPrefactura", System.Data.SqlDbType.Bit, 0, "monedaPrefactura"), New System.Data.SqlClient.SqlParameter("@MonedaSistema", System.Data.SqlDbType.Int, 0, "MonedaSistema"), New System.Data.SqlClient.SqlParameter("@utilizaGrupo", System.Data.SqlDbType.Bit, 0, "utilizaGrupo"), New System.Data.SqlClient.SqlParameter("@IncluirServicios", System.Data.SqlDbType.Bit, 0, "IncluirServicios"), New System.Data.SqlClient.SqlParameter("@NinosGratisHab", System.Data.SqlDbType.Bit, 0, "NinosGratisHab"), New System.Data.SqlClient.SqlParameter("@PedirTarjeta", System.Data.SqlDbType.Bit, 0, "PedirTarjeta"), New System.Data.SqlClient.SqlParameter("@General", System.Data.SqlDbType.Bit, 0, "General"), New System.Data.SqlClient.SqlParameter("@TIPOFACT", System.Data.SqlDbType.VarChar, 0, "TIPOFACT"), New System.Data.SqlClient.SqlParameter("@FormatoCheck", System.Data.SqlDbType.Bit, 0, "FormatoCheck"), New System.Data.SqlClient.SqlParameter("@TipoPlanning", System.Data.SqlDbType.VarChar, 0, "TipoPlanning"), New System.Data.SqlClient.SqlParameter("@AsignarHab", System.Data.SqlDbType.Bit, 0, "AsignarHab"), New System.Data.SqlClient.SqlParameter("@PoliticasCancelacion", System.Data.SqlDbType.Bit, 0, "PoliticasCancelacion"), New System.Data.SqlClient.SqlParameter("@AsignarHabAlloment", System.Data.SqlDbType.Bit, 0, "AsignarHabAlloment"), New System.Data.SqlClient.SqlParameter("@PersonaporHab", System.Data.SqlDbType.Bit, 0, "PersonaporHab"), New System.Data.SqlClient.SqlParameter("@Hora_Inicio", System.Data.SqlDbType.DateTime, 0, "Hora_Inicio"), New System.Data.SqlClient.SqlParameter("@Logo2", System.Data.SqlDbType.Image, 0, "Logo2"), New System.Data.SqlClient.SqlParameter("@Auditoria", System.Data.SqlDbType.Bit, 0, "Auditoria"), New System.Data.SqlClient.SqlParameter("@Moneda_Vuelto", System.Data.SqlDbType.Int, 0, "Moneda_Vuelto"), New System.Data.SqlClient.SqlParameter("@Contabilidad", System.Data.SqlDbType.Int, 0, "Contabilidad"), New System.Data.SqlClient.SqlParameter("@PolCancelESP", System.Data.SqlDbType.VarChar, 0, "PolCancelESP"), New System.Data.SqlClient.SqlParameter("@PolCancelING", System.Data.SqlDbType.VarChar, 0, "PolCancelING"), New System.Data.SqlClient.SqlParameter("@PolSoloPREP", System.Data.SqlDbType.Bit, 0, "PolSoloPREP"), New System.Data.SqlClient.SqlParameter("@MiniBar", System.Data.SqlDbType.Bit, 0, "MiniBar"), New System.Data.SqlClient.SqlParameter("@NoClave", System.Data.SqlDbType.Bit, 0, "NoClave"), New System.Data.SqlClient.SqlParameter("@CostoCortesia", System.Data.SqlDbType.Bit, 0, "CostoCortesia"), New System.Data.SqlClient.SqlParameter("@facturaDirecta", System.Data.SqlDbType.Bit, 0, "facturaDirecta")})
        '
        'SqlCommand2
        '
        Me.SqlCommand2.CommandText = resources.GetString("SqlCommand2.CommandText")
        Me.SqlCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand
        '
        Me.SqlUpdateCommand.CommandText = resources.GetString("SqlUpdateCommand.CommandText")
        Me.SqlUpdateCommand.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 0, "Cedula"), New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 0, "Empresa"), New System.Data.SqlClient.SqlParameter("@Tel_01", System.Data.SqlDbType.VarChar, 0, "Tel_01"), New System.Data.SqlClient.SqlParameter("@Tel_02", System.Data.SqlDbType.VarChar, 0, "Tel_02"), New System.Data.SqlClient.SqlParameter("@Fax_01", System.Data.SqlDbType.VarChar, 0, "Fax_01"), New System.Data.SqlClient.SqlParameter("@Fax_02", System.Data.SqlDbType.VarChar, 0, "Fax_02"), New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 0, "Direccion"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 0, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Frase", System.Data.SqlDbType.VarChar, 0, "Frase"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio", System.Data.SqlDbType.Float, 0, "Imp_Servicio"), New System.Data.SqlClient.SqlParameter("@Logo", System.Data.SqlDbType.Image, 0, "Logo"), New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Int, 0, "Intereses"), New System.Data.SqlClient.SqlParameter("@Imp_ICT", System.Data.SqlDbType.Float, 0, "Imp_ICT"), New System.Data.SqlClient.SqlParameter("@Total_Dias_Cancelar", System.Data.SqlDbType.Int, 0, "Total_Dias_Cancelar"), New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 0, "Email"), New System.Data.SqlClient.SqlParameter("@Dirrecion_Web", System.Data.SqlDbType.VarChar, 0, "Dirrecion_Web"), New System.Data.SqlClient.SqlParameter("@Comision_Habitaciones", System.Data.SqlDbType.Bit, 0, "Comision_Habitaciones"), New System.Data.SqlClient.SqlParameter("@Comision_Servicio", System.Data.SqlDbType.Bit, 0, "Comision_Servicio"), New System.Data.SqlClient.SqlParameter("@Comision_Restaurant", System.Data.SqlDbType.Bit, 0, "Comision_Restaurant"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria1", System.Data.SqlDbType.VarChar, 0, "Cuenta_Bancaria1"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria2", System.Data.SqlDbType.VarChar, 0, "Cuenta_Bancaria2"), New System.Data.SqlClient.SqlParameter("@Impresora_Facturacion", System.Data.SqlDbType.VarChar, 0, "Impresora_Facturacion"), New System.Data.SqlClient.SqlParameter("@Maximo_Descuento", System.Data.SqlDbType.Float, 0, "Maximo_Descuento"), New System.Data.SqlClient.SqlParameter("@PersonaJuridica", System.Data.SqlDbType.VarChar, 0, "PersonaJuridica"), New System.Data.SqlClient.SqlParameter("@CantidadHabitaciones", System.Data.SqlDbType.Int, 0, "CantidadHabitaciones"), New System.Data.SqlClient.SqlParameter("@ConsecutivoUnico", System.Data.SqlDbType.Bit, 0, "ConsecutivoUnico"), New System.Data.SqlClient.SqlParameter("@Desayunos", System.Data.SqlDbType.Bit, 0, "Desayunos"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio_Habitacion", System.Data.SqlDbType.Float, 0, "Imp_Servicio_Habitacion"), New System.Data.SqlClient.SqlParameter("@UnicoEncargadoXMesa", System.Data.SqlDbType.Bit, 0, "UnicoEncargadoXMesa"), New System.Data.SqlClient.SqlParameter("@MontoApertura", System.Data.SqlDbType.Bit, 0, "MontoApertura"), New System.Data.SqlClient.SqlParameter("@MonedaRestaurante", System.Data.SqlDbType.SmallInt, 0, "MonedaRestaurante"), New System.Data.SqlClient.SqlParameter("@OpcionFacturacion", System.Data.SqlDbType.Bit, 0, "OpcionFacturacion"), New System.Data.SqlClient.SqlParameter("@Prefactura", System.Data.SqlDbType.Float, 0, "Prefactura"), New System.Data.SqlClient.SqlParameter("@monedaPrefactura", System.Data.SqlDbType.Bit, 0, "monedaPrefactura"), New System.Data.SqlClient.SqlParameter("@MonedaSistema", System.Data.SqlDbType.Int, 0, "MonedaSistema"), New System.Data.SqlClient.SqlParameter("@utilizaGrupo", System.Data.SqlDbType.Bit, 0, "utilizaGrupo"), New System.Data.SqlClient.SqlParameter("@IncluirServicios", System.Data.SqlDbType.Bit, 0, "IncluirServicios"), New System.Data.SqlClient.SqlParameter("@NinosGratisHab", System.Data.SqlDbType.Bit, 0, "NinosGratisHab"), New System.Data.SqlClient.SqlParameter("@PedirTarjeta", System.Data.SqlDbType.Bit, 0, "PedirTarjeta"), New System.Data.SqlClient.SqlParameter("@General", System.Data.SqlDbType.Bit, 0, "General"), New System.Data.SqlClient.SqlParameter("@TIPOFACT", System.Data.SqlDbType.VarChar, 0, "TIPOFACT"), New System.Data.SqlClient.SqlParameter("@FormatoCheck", System.Data.SqlDbType.Bit, 0, "FormatoCheck"), New System.Data.SqlClient.SqlParameter("@TipoPlanning", System.Data.SqlDbType.VarChar, 0, "TipoPlanning"), New System.Data.SqlClient.SqlParameter("@AsignarHab", System.Data.SqlDbType.Bit, 0, "AsignarHab"), New System.Data.SqlClient.SqlParameter("@PoliticasCancelacion", System.Data.SqlDbType.Bit, 0, "PoliticasCancelacion"), New System.Data.SqlClient.SqlParameter("@AsignarHabAlloment", System.Data.SqlDbType.Bit, 0, "AsignarHabAlloment"), New System.Data.SqlClient.SqlParameter("@PersonaporHab", System.Data.SqlDbType.Bit, 0, "PersonaporHab"), New System.Data.SqlClient.SqlParameter("@Hora_Inicio", System.Data.SqlDbType.DateTime, 0, "Hora_Inicio"), New System.Data.SqlClient.SqlParameter("@Logo2", System.Data.SqlDbType.Image, 0, "Logo2"), New System.Data.SqlClient.SqlParameter("@Auditoria", System.Data.SqlDbType.Bit, 0, "Auditoria"), New System.Data.SqlClient.SqlParameter("@Moneda_Vuelto", System.Data.SqlDbType.Int, 0, "Moneda_Vuelto"), New System.Data.SqlClient.SqlParameter("@Contabilidad", System.Data.SqlDbType.Int, 0, "Contabilidad"), New System.Data.SqlClient.SqlParameter("@PolCancelESP", System.Data.SqlDbType.VarChar, 0, "PolCancelESP"), New System.Data.SqlClient.SqlParameter("@PolCancelING", System.Data.SqlDbType.VarChar, 0, "PolCancelING"), New System.Data.SqlClient.SqlParameter("@PolSoloPREP", System.Data.SqlDbType.Bit, 0, "PolSoloPREP"), New System.Data.SqlClient.SqlParameter("@MiniBar", System.Data.SqlDbType.Bit, 0, "MiniBar"), New System.Data.SqlClient.SqlParameter("@NoClave", System.Data.SqlDbType.Bit, 0, "NoClave"), New System.Data.SqlClient.SqlParameter("@CostoCortesia", System.Data.SqlDbType.Bit, 0, "CostoCortesia"), New System.Data.SqlClient.SqlParameter("@facturaDirecta", System.Data.SqlDbType.Bit, 0, "facturaDirecta"), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlDataAdapter3
        '
        Me.SqlDataAdapter3.DeleteCommand = Me.SqlCommand3
        Me.SqlDataAdapter3.InsertCommand = Me.SqlCommand4
        Me.SqlDataAdapter3.SelectCommand = Me.SqlCommand5
        Me.SqlDataAdapter3.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "configuraciones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Empresa", "Empresa"), New System.Data.Common.DataColumnMapping("Tel_01", "Tel_01"), New System.Data.Common.DataColumnMapping("Tel_02", "Tel_02"), New System.Data.Common.DataColumnMapping("Fax_01", "Fax_01"), New System.Data.Common.DataColumnMapping("Fax_02", "Fax_02"), New System.Data.Common.DataColumnMapping("Direccion", "Direccion"), New System.Data.Common.DataColumnMapping("Imp_Venta", "Imp_Venta"), New System.Data.Common.DataColumnMapping("Frase", "Frase"), New System.Data.Common.DataColumnMapping("Imp_Servicio", "Imp_Servicio"), New System.Data.Common.DataColumnMapping("Logo", "Logo"), New System.Data.Common.DataColumnMapping("Intereses", "Intereses"), New System.Data.Common.DataColumnMapping("Imp_ICT", "Imp_ICT"), New System.Data.Common.DataColumnMapping("Total_Dias_Cancelar", "Total_Dias_Cancelar"), New System.Data.Common.DataColumnMapping("Email", "Email"), New System.Data.Common.DataColumnMapping("Dirrecion_Web", "Dirrecion_Web"), New System.Data.Common.DataColumnMapping("Comision_Habitaciones", "Comision_Habitaciones"), New System.Data.Common.DataColumnMapping("Comision_Servicio", "Comision_Servicio"), New System.Data.Common.DataColumnMapping("Comision_Restaurant", "Comision_Restaurant"), New System.Data.Common.DataColumnMapping("Cuenta_Bancaria1", "Cuenta_Bancaria1"), New System.Data.Common.DataColumnMapping("Cuenta_Bancaria2", "Cuenta_Bancaria2"), New System.Data.Common.DataColumnMapping("Impresora_Facturacion", "Impresora_Facturacion"), New System.Data.Common.DataColumnMapping("Maximo_Descuento", "Maximo_Descuento"), New System.Data.Common.DataColumnMapping("PersonaJuridica", "PersonaJuridica"), New System.Data.Common.DataColumnMapping("CantidadHabitaciones", "CantidadHabitaciones"), New System.Data.Common.DataColumnMapping("ConsecutivoUnico", "ConsecutivoUnico"), New System.Data.Common.DataColumnMapping("Desayunos", "Desayunos"), New System.Data.Common.DataColumnMapping("Imp_Servicio_Habitacion", "Imp_Servicio_Habitacion"), New System.Data.Common.DataColumnMapping("UnicoEncargadoXMesa", "UnicoEncargadoXMesa"), New System.Data.Common.DataColumnMapping("MontoApertura", "MontoApertura"), New System.Data.Common.DataColumnMapping("MonedaRestaurante", "MonedaRestaurante"), New System.Data.Common.DataColumnMapping("OpcionFacturacion", "OpcionFacturacion"), New System.Data.Common.DataColumnMapping("Prefactura", "Prefactura"), New System.Data.Common.DataColumnMapping("monedaPrefactura", "monedaPrefactura"), New System.Data.Common.DataColumnMapping("MonedaSistema", "MonedaSistema"), New System.Data.Common.DataColumnMapping("utilizaGrupo", "utilizaGrupo"), New System.Data.Common.DataColumnMapping("IncluirServicios", "IncluirServicios"), New System.Data.Common.DataColumnMapping("NinosGratisHab", "NinosGratisHab"), New System.Data.Common.DataColumnMapping("PedirTarjeta", "PedirTarjeta"), New System.Data.Common.DataColumnMapping("General", "General"), New System.Data.Common.DataColumnMapping("TIPOFACT", "TIPOFACT"), New System.Data.Common.DataColumnMapping("FormatoCheck", "FormatoCheck"), New System.Data.Common.DataColumnMapping("TipoPlanning", "TipoPlanning"), New System.Data.Common.DataColumnMapping("AsignarHab", "AsignarHab"), New System.Data.Common.DataColumnMapping("PoliticasCancelacion", "PoliticasCancelacion"), New System.Data.Common.DataColumnMapping("AsignarHabAlloment", "AsignarHabAlloment"), New System.Data.Common.DataColumnMapping("PersonaporHab", "PersonaporHab"), New System.Data.Common.DataColumnMapping("Hora_Inicio", "Hora_Inicio"), New System.Data.Common.DataColumnMapping("Logo2", "Logo2"), New System.Data.Common.DataColumnMapping("Auditoria", "Auditoria"), New System.Data.Common.DataColumnMapping("Moneda_Vuelto", "Moneda_Vuelto"), New System.Data.Common.DataColumnMapping("Contabilidad", "Contabilidad"), New System.Data.Common.DataColumnMapping("PolCancelESP", "PolCancelESP"), New System.Data.Common.DataColumnMapping("PolCancelING", "PolCancelING"), New System.Data.Common.DataColumnMapping("PolSoloPREP", "PolSoloPREP"), New System.Data.Common.DataColumnMapping("MiniBar", "MiniBar"), New System.Data.Common.DataColumnMapping("NoClave", "NoClave"), New System.Data.Common.DataColumnMapping("CostoCortesia", "CostoCortesia"), New System.Data.Common.DataColumnMapping("facturaDirecta", "facturaDirecta")})})
        Me.SqlDataAdapter3.UpdateCommand = Me.SqlCommand6
        '
        'SqlCommand3
        '
        Me.SqlCommand3.CommandText = "DELETE FROM [configuraciones] WHERE (([Cedula] = @Original_Cedula))"
        Me.SqlCommand3.Connection = Me.SqlConnection1
        Me.SqlCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlCommand4
        '
        Me.SqlCommand4.CommandText = resources.GetString("SqlCommand4.CommandText")
        Me.SqlCommand4.Connection = Me.SqlConnection1
        Me.SqlCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 0, "Cedula"), New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 0, "Empresa"), New System.Data.SqlClient.SqlParameter("@Tel_01", System.Data.SqlDbType.VarChar, 0, "Tel_01"), New System.Data.SqlClient.SqlParameter("@Tel_02", System.Data.SqlDbType.VarChar, 0, "Tel_02"), New System.Data.SqlClient.SqlParameter("@Fax_01", System.Data.SqlDbType.VarChar, 0, "Fax_01"), New System.Data.SqlClient.SqlParameter("@Fax_02", System.Data.SqlDbType.VarChar, 0, "Fax_02"), New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 0, "Direccion"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 0, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Frase", System.Data.SqlDbType.VarChar, 0, "Frase"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio", System.Data.SqlDbType.Float, 0, "Imp_Servicio"), New System.Data.SqlClient.SqlParameter("@Logo", System.Data.SqlDbType.Image, 0, "Logo"), New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Int, 0, "Intereses"), New System.Data.SqlClient.SqlParameter("@Imp_ICT", System.Data.SqlDbType.Float, 0, "Imp_ICT"), New System.Data.SqlClient.SqlParameter("@Total_Dias_Cancelar", System.Data.SqlDbType.Int, 0, "Total_Dias_Cancelar"), New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 0, "Email"), New System.Data.SqlClient.SqlParameter("@Dirrecion_Web", System.Data.SqlDbType.VarChar, 0, "Dirrecion_Web"), New System.Data.SqlClient.SqlParameter("@Comision_Habitaciones", System.Data.SqlDbType.Bit, 0, "Comision_Habitaciones"), New System.Data.SqlClient.SqlParameter("@Comision_Servicio", System.Data.SqlDbType.Bit, 0, "Comision_Servicio"), New System.Data.SqlClient.SqlParameter("@Comision_Restaurant", System.Data.SqlDbType.Bit, 0, "Comision_Restaurant"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria1", System.Data.SqlDbType.VarChar, 0, "Cuenta_Bancaria1"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria2", System.Data.SqlDbType.VarChar, 0, "Cuenta_Bancaria2"), New System.Data.SqlClient.SqlParameter("@Impresora_Facturacion", System.Data.SqlDbType.VarChar, 0, "Impresora_Facturacion"), New System.Data.SqlClient.SqlParameter("@Maximo_Descuento", System.Data.SqlDbType.Float, 0, "Maximo_Descuento"), New System.Data.SqlClient.SqlParameter("@PersonaJuridica", System.Data.SqlDbType.VarChar, 0, "PersonaJuridica"), New System.Data.SqlClient.SqlParameter("@CantidadHabitaciones", System.Data.SqlDbType.Int, 0, "CantidadHabitaciones"), New System.Data.SqlClient.SqlParameter("@ConsecutivoUnico", System.Data.SqlDbType.Bit, 0, "ConsecutivoUnico"), New System.Data.SqlClient.SqlParameter("@Desayunos", System.Data.SqlDbType.Bit, 0, "Desayunos"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio_Habitacion", System.Data.SqlDbType.Float, 0, "Imp_Servicio_Habitacion"), New System.Data.SqlClient.SqlParameter("@UnicoEncargadoXMesa", System.Data.SqlDbType.Bit, 0, "UnicoEncargadoXMesa"), New System.Data.SqlClient.SqlParameter("@MontoApertura", System.Data.SqlDbType.Bit, 0, "MontoApertura"), New System.Data.SqlClient.SqlParameter("@MonedaRestaurante", System.Data.SqlDbType.SmallInt, 0, "MonedaRestaurante"), New System.Data.SqlClient.SqlParameter("@OpcionFacturacion", System.Data.SqlDbType.Bit, 0, "OpcionFacturacion"), New System.Data.SqlClient.SqlParameter("@Prefactura", System.Data.SqlDbType.Float, 0, "Prefactura"), New System.Data.SqlClient.SqlParameter("@monedaPrefactura", System.Data.SqlDbType.Bit, 0, "monedaPrefactura"), New System.Data.SqlClient.SqlParameter("@MonedaSistema", System.Data.SqlDbType.Int, 0, "MonedaSistema"), New System.Data.SqlClient.SqlParameter("@utilizaGrupo", System.Data.SqlDbType.Bit, 0, "utilizaGrupo"), New System.Data.SqlClient.SqlParameter("@IncluirServicios", System.Data.SqlDbType.Bit, 0, "IncluirServicios"), New System.Data.SqlClient.SqlParameter("@NinosGratisHab", System.Data.SqlDbType.Bit, 0, "NinosGratisHab"), New System.Data.SqlClient.SqlParameter("@PedirTarjeta", System.Data.SqlDbType.Bit, 0, "PedirTarjeta"), New System.Data.SqlClient.SqlParameter("@General", System.Data.SqlDbType.Bit, 0, "General"), New System.Data.SqlClient.SqlParameter("@TIPOFACT", System.Data.SqlDbType.VarChar, 0, "TIPOFACT"), New System.Data.SqlClient.SqlParameter("@FormatoCheck", System.Data.SqlDbType.Bit, 0, "FormatoCheck"), New System.Data.SqlClient.SqlParameter("@TipoPlanning", System.Data.SqlDbType.VarChar, 0, "TipoPlanning"), New System.Data.SqlClient.SqlParameter("@AsignarHab", System.Data.SqlDbType.Bit, 0, "AsignarHab"), New System.Data.SqlClient.SqlParameter("@PoliticasCancelacion", System.Data.SqlDbType.Bit, 0, "PoliticasCancelacion"), New System.Data.SqlClient.SqlParameter("@AsignarHabAlloment", System.Data.SqlDbType.Bit, 0, "AsignarHabAlloment"), New System.Data.SqlClient.SqlParameter("@PersonaporHab", System.Data.SqlDbType.Bit, 0, "PersonaporHab"), New System.Data.SqlClient.SqlParameter("@Hora_Inicio", System.Data.SqlDbType.DateTime, 0, "Hora_Inicio"), New System.Data.SqlClient.SqlParameter("@Logo2", System.Data.SqlDbType.Image, 0, "Logo2"), New System.Data.SqlClient.SqlParameter("@Auditoria", System.Data.SqlDbType.Bit, 0, "Auditoria"), New System.Data.SqlClient.SqlParameter("@Moneda_Vuelto", System.Data.SqlDbType.Int, 0, "Moneda_Vuelto"), New System.Data.SqlClient.SqlParameter("@Contabilidad", System.Data.SqlDbType.Int, 0, "Contabilidad"), New System.Data.SqlClient.SqlParameter("@PolCancelESP", System.Data.SqlDbType.VarChar, 0, "PolCancelESP"), New System.Data.SqlClient.SqlParameter("@PolCancelING", System.Data.SqlDbType.VarChar, 0, "PolCancelING"), New System.Data.SqlClient.SqlParameter("@PolSoloPREP", System.Data.SqlDbType.Bit, 0, "PolSoloPREP"), New System.Data.SqlClient.SqlParameter("@MiniBar", System.Data.SqlDbType.Bit, 0, "MiniBar"), New System.Data.SqlClient.SqlParameter("@NoClave", System.Data.SqlDbType.Bit, 0, "NoClave"), New System.Data.SqlClient.SqlParameter("@CostoCortesia", System.Data.SqlDbType.Bit, 0, "CostoCortesia"), New System.Data.SqlClient.SqlParameter("@facturaDirecta", System.Data.SqlDbType.Bit, 0, "facturaDirecta")})
        '
        'SqlCommand5
        '
        Me.SqlCommand5.CommandText = resources.GetString("SqlCommand5.CommandText")
        Me.SqlCommand5.Connection = Me.SqlConnection1
        '
        'SqlCommand6
        '
        Me.SqlCommand6.CommandText = resources.GetString("SqlCommand6.CommandText")
        Me.SqlCommand6.Connection = Me.SqlConnection1
        Me.SqlCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 0, "Cedula"), New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 0, "Empresa"), New System.Data.SqlClient.SqlParameter("@Tel_01", System.Data.SqlDbType.VarChar, 0, "Tel_01"), New System.Data.SqlClient.SqlParameter("@Tel_02", System.Data.SqlDbType.VarChar, 0, "Tel_02"), New System.Data.SqlClient.SqlParameter("@Fax_01", System.Data.SqlDbType.VarChar, 0, "Fax_01"), New System.Data.SqlClient.SqlParameter("@Fax_02", System.Data.SqlDbType.VarChar, 0, "Fax_02"), New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 0, "Direccion"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 0, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Frase", System.Data.SqlDbType.VarChar, 0, "Frase"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio", System.Data.SqlDbType.Float, 0, "Imp_Servicio"), New System.Data.SqlClient.SqlParameter("@Logo", System.Data.SqlDbType.Image, 0, "Logo"), New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Int, 0, "Intereses"), New System.Data.SqlClient.SqlParameter("@Imp_ICT", System.Data.SqlDbType.Float, 0, "Imp_ICT"), New System.Data.SqlClient.SqlParameter("@Total_Dias_Cancelar", System.Data.SqlDbType.Int, 0, "Total_Dias_Cancelar"), New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 0, "Email"), New System.Data.SqlClient.SqlParameter("@Dirrecion_Web", System.Data.SqlDbType.VarChar, 0, "Dirrecion_Web"), New System.Data.SqlClient.SqlParameter("@Comision_Habitaciones", System.Data.SqlDbType.Bit, 0, "Comision_Habitaciones"), New System.Data.SqlClient.SqlParameter("@Comision_Servicio", System.Data.SqlDbType.Bit, 0, "Comision_Servicio"), New System.Data.SqlClient.SqlParameter("@Comision_Restaurant", System.Data.SqlDbType.Bit, 0, "Comision_Restaurant"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria1", System.Data.SqlDbType.VarChar, 0, "Cuenta_Bancaria1"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria2", System.Data.SqlDbType.VarChar, 0, "Cuenta_Bancaria2"), New System.Data.SqlClient.SqlParameter("@Impresora_Facturacion", System.Data.SqlDbType.VarChar, 0, "Impresora_Facturacion"), New System.Data.SqlClient.SqlParameter("@Maximo_Descuento", System.Data.SqlDbType.Float, 0, "Maximo_Descuento"), New System.Data.SqlClient.SqlParameter("@PersonaJuridica", System.Data.SqlDbType.VarChar, 0, "PersonaJuridica"), New System.Data.SqlClient.SqlParameter("@CantidadHabitaciones", System.Data.SqlDbType.Int, 0, "CantidadHabitaciones"), New System.Data.SqlClient.SqlParameter("@ConsecutivoUnico", System.Data.SqlDbType.Bit, 0, "ConsecutivoUnico"), New System.Data.SqlClient.SqlParameter("@Desayunos", System.Data.SqlDbType.Bit, 0, "Desayunos"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio_Habitacion", System.Data.SqlDbType.Float, 0, "Imp_Servicio_Habitacion"), New System.Data.SqlClient.SqlParameter("@UnicoEncargadoXMesa", System.Data.SqlDbType.Bit, 0, "UnicoEncargadoXMesa"), New System.Data.SqlClient.SqlParameter("@MontoApertura", System.Data.SqlDbType.Bit, 0, "MontoApertura"), New System.Data.SqlClient.SqlParameter("@MonedaRestaurante", System.Data.SqlDbType.SmallInt, 0, "MonedaRestaurante"), New System.Data.SqlClient.SqlParameter("@OpcionFacturacion", System.Data.SqlDbType.Bit, 0, "OpcionFacturacion"), New System.Data.SqlClient.SqlParameter("@Prefactura", System.Data.SqlDbType.Float, 0, "Prefactura"), New System.Data.SqlClient.SqlParameter("@monedaPrefactura", System.Data.SqlDbType.Bit, 0, "monedaPrefactura"), New System.Data.SqlClient.SqlParameter("@MonedaSistema", System.Data.SqlDbType.Int, 0, "MonedaSistema"), New System.Data.SqlClient.SqlParameter("@utilizaGrupo", System.Data.SqlDbType.Bit, 0, "utilizaGrupo"), New System.Data.SqlClient.SqlParameter("@IncluirServicios", System.Data.SqlDbType.Bit, 0, "IncluirServicios"), New System.Data.SqlClient.SqlParameter("@NinosGratisHab", System.Data.SqlDbType.Bit, 0, "NinosGratisHab"), New System.Data.SqlClient.SqlParameter("@PedirTarjeta", System.Data.SqlDbType.Bit, 0, "PedirTarjeta"), New System.Data.SqlClient.SqlParameter("@General", System.Data.SqlDbType.Bit, 0, "General"), New System.Data.SqlClient.SqlParameter("@TIPOFACT", System.Data.SqlDbType.VarChar, 0, "TIPOFACT"), New System.Data.SqlClient.SqlParameter("@FormatoCheck", System.Data.SqlDbType.Bit, 0, "FormatoCheck"), New System.Data.SqlClient.SqlParameter("@TipoPlanning", System.Data.SqlDbType.VarChar, 0, "TipoPlanning"), New System.Data.SqlClient.SqlParameter("@AsignarHab", System.Data.SqlDbType.Bit, 0, "AsignarHab"), New System.Data.SqlClient.SqlParameter("@PoliticasCancelacion", System.Data.SqlDbType.Bit, 0, "PoliticasCancelacion"), New System.Data.SqlClient.SqlParameter("@AsignarHabAlloment", System.Data.SqlDbType.Bit, 0, "AsignarHabAlloment"), New System.Data.SqlClient.SqlParameter("@PersonaporHab", System.Data.SqlDbType.Bit, 0, "PersonaporHab"), New System.Data.SqlClient.SqlParameter("@Hora_Inicio", System.Data.SqlDbType.DateTime, 0, "Hora_Inicio"), New System.Data.SqlClient.SqlParameter("@Logo2", System.Data.SqlDbType.Image, 0, "Logo2"), New System.Data.SqlClient.SqlParameter("@Auditoria", System.Data.SqlDbType.Bit, 0, "Auditoria"), New System.Data.SqlClient.SqlParameter("@Moneda_Vuelto", System.Data.SqlDbType.Int, 0, "Moneda_Vuelto"), New System.Data.SqlClient.SqlParameter("@Contabilidad", System.Data.SqlDbType.Int, 0, "Contabilidad"), New System.Data.SqlClient.SqlParameter("@PolCancelESP", System.Data.SqlDbType.VarChar, 0, "PolCancelESP"), New System.Data.SqlClient.SqlParameter("@PolCancelING", System.Data.SqlDbType.VarChar, 0, "PolCancelING"), New System.Data.SqlClient.SqlParameter("@PolSoloPREP", System.Data.SqlDbType.Bit, 0, "PolSoloPREP"), New System.Data.SqlClient.SqlParameter("@MiniBar", System.Data.SqlDbType.Bit, 0, "MiniBar"), New System.Data.SqlClient.SqlParameter("@NoClave", System.Data.SqlDbType.Bit, 0, "NoClave"), New System.Data.SqlClient.SqlParameter("@CostoCortesia", System.Data.SqlDbType.Bit, 0, "CostoCortesia"), New System.Data.SqlClient.SqlParameter("@facturaDirecta", System.Data.SqlDbType.Bit, 0, "facturaDirecta"), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing)})
        '
        'frmEstado_CXC
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(742, 466)
        Me.Controls.Add(Me.ProgressBarControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.rptViewer)
        Me.Name = "frmEstado_CXC"
        Me.Text = "Estado de Cuentas por Cobrar"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DataSet_Estado_CxC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dts_SaldoCxC1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Sub ButtonMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMostrar.Click
        CargarReporteTresEstados()
    End Sub
    Private Sub CargarReporteTresEstados()
        Dim X As Byte
        Dim Link As New Conexion
        Try
            For X = 0 To ToolBar1.Buttons.Count - 1
                If ToolBar1.Buttons(X).Pushed = True Then Exit For
            Next
            Select Case X
                Case 0 To 3
                    If Not IsNumeric(Me.TextBox1.Text) Then
                        MsgBox("La Id del Cliente Suministrada no es V�lida...", MsgBoxStyle.Exclamation, "Atenci�n...")
                        Exit Sub
                    End If
                Case 4, 5, 6
            End Select
            Link.Conectar()
            ButtonMostrar.Enabled = False
            ProgressBarControl1.Text = 0
            Timer1.Enabled = True
            Dim con As String = GetSetting("Seesoft", "Hotel", "Conexion")
            Select Case X
                Case 0 To 2 'Estado de Cuenta General,Vencidas,Pagadas. 
                    Dim Reporte As New Estado_Actual_FechaCorte

                    Me.Dts_SaldoCxC1.Estado_CxC_FechaCorte_x_Cliente.Clear()
                    Me.Dts_SaldoCxC1.Cliente.Clear()
                    Me.Dts_SaldoCxC1.Estado_CxC_Antiguedad_Saldo_x_Cliente.Clear()
                    Me.Dts_SaldoCxC1.configuraciones.Clear()

                    cFunciones.Llenar_Tabla_Generico("select * from cliente where id = " & CDbl(TextBox1.Text), Me.Dts_SaldoCxC1.Cliente, GetSetting("SeeSoft", "Restaurante", "Conexion"))
                    cFunciones.Llenar_Tabla_Generico("exec dbo.Estado_CxC_FechaCorte_x_Cliente '" & DateTimePicker1.Value & "', " & CDbl(TextBox1.Text) & "", Me.Dts_SaldoCxC1.Estado_CxC_FechaCorte_x_Cliente, GetSetting("SeeSoft", "Hotel", "Conexion"))
                    cFunciones.Llenar_Tabla_Generico("exec dbo.Estado_CxC_Antiguedad_Saldo_x_Cliente '" & DateTimePicker1.Value & "', " & CDbl(TextBox1.Text) & "", Me.Dts_SaldoCxC1.Estado_CxC_Antiguedad_Saldo_x_Cliente, GetSetting("SeeSoft", "Hotel", "Conexion"))
                    cFunciones.Llenar_Tabla_Generico("Select * from configuraciones", Me.Dts_SaldoCxC1.configuraciones, GetSetting("SeeSoft", "Hotel", "Conexion"))

                    Reporte.SetDataSource(Me.Dts_SaldoCxC1)
                    Reporte.Subreports(0).SetDataSource(Me.Dts_SaldoCxC1)
                    Reporte.Subreports(1).SetDataSource(Me.Dts_SaldoCxC1)

                    Reporte.SetParameterValue(0, CDbl(CDbl(Label5.Text))) 'TIPO DE CAMBIO $ - c
                    Reporte.SetParameterValue(1, CStr("FACTURAS " & ToolBar1.Buttons(X).Text & " - " & Moneda.Text))
                    Reporte.SetParameterValue(2, X)    'TIPO D REPORTE
                    Reporte.SetParameterValue(4, CDbl(TextBox1.Text))
                    Reporte.SetParameterValue(3, DateTimePicker1.Value.Date)
                    Reporte.SetParameterValue(5, Moneda.SelectedValue)

                    rptViewer.ReportSource = Reporte

                Case 3 ' REPORTE DE MOV DE LA CUENTA POR FACTURA  
                    Dim Reporte As New MovimientoCliente
                    Reporte.SetParameterValue(0, CDbl(TextBox1.Text))              ' Codigo de cliente
                    Reporte.SetParameterValue(1, CDbl(Label5.Text))          ' TIPO DE CAMBIO $ - c
                    Reporte.SetParameterValue(2, DateTimePicker2.Value)         ' Fecha Desde
                    Reporte.SetParameterValue(3, DateTimePicker1.Value)         ' Fecha hasta
                    Reporte.SetParameterValue(4, Moneda.Text)          ' Nombre Moneda
                    CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte, False, con)

                Case 4 'MOVIMIENTO POR CUENTA (DIAS EN QUE SE EFECTUA LOS MOVIMIENTO)
                    Dim reporte As New MovimientoSaldoCliente
                    reporte.SetParameterValue(0, CDbl(TextBox1.Text))
                    reporte.SetParameterValue(1, DateTimePicker2.Value)
                    reporte.SetParameterValue(2, DateAdd(DateInterval.Day, 1, DateTimePicker1.Value))
                    reporte.SetParameterValue(3, CDbl(Label5.Text))
                    CrystalReportsConexion.LoadReportViewer(rptViewer, reporte, False, con)

                Case 5 ' REPORTE DE ANTIGUEDAD DE SALDO 
                    Dim Reporte As New Reporte_CXC_x_Cliente
                    Reporte.SetParameterValue(0, CDbl(Label5.Text))
                    Reporte.SetParameterValue(1, CheckBox1.Checked)
                    Reporte.SetParameterValue(2, DateTimePicker1.Value)
                    Reporte.SetParameterValue(3, CDbl(Moneda.SelectedValue))
                    CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte, False, con)
                    Timer1.Enabled = True
                Case 6
                    If opEmpleados.Checked = True Then
                        Dim Reporte As New Estado_CXC_Saldo_General_Empleados
                        Reporte.SetParameterValue(0, CDbl(Label5.Text))
                        Reporte.SetParameterValue(1, CStr("GENERAL x EMPLEADOS EN " & Moneda.Text))
                        Reporte.SetParameterValue(2, Me.CheckBox1.Checked)
                        Reporte.SetParameterValue(3, Me.DateTimePicker1.Value)
                        Reporte.SetParameterValue(4, CDbl(Moneda.SelectedValue))
                        CrystalReportsConexion.LoadReportViewer(Me.rptViewer, Reporte, False, con)
                    End If
                    If opAgencias.Checked = True Then
                        Dim Reporte As New Estado_CXC_Saldo_General_Agencias
                        Reporte.SetParameterValue(0, CDbl(Label5.Text))
                        Reporte.SetParameterValue(1, CStr("GENERAL x AGENCIAS EN " & Moneda.Text))
                        Reporte.SetParameterValue(2, Me.CheckBox1.Checked)
                        Reporte.SetParameterValue(3, Me.DateTimePicker1.Value)
                        Reporte.SetParameterValue(4, CDbl(Moneda.SelectedValue))
                        CrystalReportsConexion.LoadReportViewer(Me.rptViewer, Reporte, False, con)
                    End If
                    If opTodos.Checked = True Then
                        Dim Reporte As New Estado_CXC_Saldo_General_Clientes
                        Reporte.SetParameterValue(0, CDbl(Label5.Text))
                        Reporte.SetParameterValue(1, CStr("GENERAL x CLIENTE EN " & Moneda.Text))
                        Reporte.SetParameterValue(2, Me.CheckBox1.Checked)
                        Reporte.SetParameterValue(3, Me.DateTimePicker1.Value)
                        Reporte.SetParameterValue(4, CDbl(Moneda.SelectedValue))
                        CrystalReportsConexion.LoadReportViewer(Me.rptViewer, Reporte, False, con)
                    End If

                Case 7
                    Dim Reporte As New Reporte_Facturas_Vencidas
                    Reporte.SetParameterValue(0, CDbl(Label5.Text))      ' TIPO DE CAMBIO $ - c
                    Reporte.SetParameterValue(1, Moneda.Text)
                    Reporte.SetParameterValue(2, Me.DateTimePicker1.Value)
                    Reporte.SetParameterValue(3, CDbl(Moneda.SelectedValue))
                    CrystalReportsConexion.LoadReportViewer(Me.rptViewer, Reporte, False, con)
            End Select
            rptViewer.Show()
            ProgressBarControl1.Text = 0
            Timer1.Enabled = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        ButtonMostrar.Enabled = True
    End Sub

    Private Sub frmEstado_CXC_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cconexion As New Conexion
        Dim sqlconexion As New SqlClient.SqlConnection
        Dim TipoCambio As Double
        SqlConnection1.ConnectionString = GetSetting("Hotel", "Hotel", "Conexion")
        AdapterMoneda.Fill(DataSet_Estado_CxC, "Moneda")
        DateTimePicker1.Value = Now.Date
        DateTimePicker2.Value = Now.Date
        sqlconexion = cconexion.Conectar("Seguridad")
        TipoCambio = CDbl(cconexion.SlqExecuteScalar(sqlconexion, "Select ValorCompra from Moneda where CodMoneda = 2"))
        cconexion.DesConectar(sqlconexion)
        Label5.Text = TipoCambio
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown

        If e.KeyCode = Keys.F1 Then
            Dim cFunciones As New cFunciones
            Me.TextBox1.Text = cFunciones.BuscarDatos("select id as Identificaci�n,Nombre from Cliente", "Nombre")
            cFunciones = Nothing
        ElseIf e.KeyCode = Keys.Enter Then
            CargarReporteTresEstados()
        End If

    End Sub
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Dim X As Byte
        For X = 0 To ToolBar1.Buttons.Count - 1
            ToolBar1.Buttons(X).Pushed = False
        Next
        ToolBar1.Buttons(ToolBar1.Buttons.IndexOf(e.Button)).Pushed = True
        DateTimePicker2.Enabled = False
        TextBox1.Enabled = True
        CheckBox1.Enabled = False
        GroupBox1.Visible = False

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0 To 2 : CargarReporteTresEstados()
            Case 3
                DateTimePicker1.Enabled = True
                DateTimePicker2.Enabled = True
                DateTimePicker2.Value = DateAdd(DateInterval.Day, -Me.DateTimePicker2.Value.Day + 1, Me.DateTimePicker2.Value) 'pone la fecha en 1 del mes actual
                TextBox1.Focus()
            Case 4 : DateTimePicker2.Enabled = True
            Case 5, 6
                CheckBox1.Enabled = True
                TextBox1.Enabled = False
                GroupBox1.Visible = True
            Case Else
        End Select

    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        ProgressBarControl1.Text = Me.ProgressBarControl1.Text + 1
        If ProgressBarControl1.Text = 100 Then Me.ProgressBarControl1.Text = 0
        rptViewer.Visible = True
        Application.DoEvents()

    End Sub
End Class
