Imports System.data.SqlClient

Public Class frmReciboDinero
    Inherits System.Windows.Forms.Form
    Private sqlConexion As SqlConnection
    Dim CConexion As New Conexion
    Dim Anular As Boolean = False
    Dim VariarInteres As Boolean = False
    Dim TipoCambioFact As Double 'Tipo Cambio de la Factura
    Dim TipoCambio As Double
    Dim Tabla As New DataTable
    Dim buscando As Boolean
    Public Trans As SqlTransaction
    Public Cedula_Usuario As String
    Public Nombre_Usuario As String
    Dim PMU As PerfilModulo_Class
    Dim documento As String = ""
    Dim documento1 As String = ""
    Dim Codigo As Integer
    Dim tbl As String = ""
    Dim dias As Integer = 0
    Dim id_c As Integer = 0
    Dim id_Aju As Integer = 0
    Dim doc As String
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarAnular As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents dtFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents adAbonos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsRecibos1 As dsRecibos
    Friend WithEvents daDetalle_Abono As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents ComboMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents txtSaldoAct As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSaldoAnt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtIntereses As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dtEmitida As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFactura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCedulaUsuario As System.Windows.Forms.TextBox
    Friend WithEvents gridFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents AdvBandedGridView1 As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents Factura As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents Fecha As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtSaldo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtMonto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lbSimbolo1 As System.Windows.Forms.Label
    Friend WithEvents lbSimbolo2 As System.Windows.Forms.Label
    Friend WithEvents lbSimbolo4 As System.Windows.Forms.Label
    Friend WithEvents lbSimbolo5 As System.Windows.Forms.Label
    Friend WithEvents lbSimbolo6 As System.Windows.Forms.Label
    Friend WithEvents lbSimbolo3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents colFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMonto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSaldo_Ant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIntereses As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAbono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtAbonoSuMoneda As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAbonoB As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAbono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSaldoActGen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAbonoGen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSaldoAntGen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents txtNum_Recibo As System.Windows.Forms.TextBox
    Friend WithEvents Check_Dig_Recibo As System.Windows.Forms.CheckBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label_Id_Recibo As System.Windows.Forms.Label
    Friend WithEvents txtID_Factura As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents btModFecha As System.Windows.Forms.Button
    Friend WithEvents Prov As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtTipoCambio As System.Windows.Forms.TextBox
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdAsientos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnectionContabilidad As System.Data.SqlClient.SqlConnection
    Friend WithEvents adDetallesAsientos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TextBoxComision As System.Windows.Forms.TextBox
    Friend WithEvents TxtCambioFactura As System.Windows.Forms.TextBox
    Friend WithEvents TbTramite As System.Windows.Forms.ToolBarButton
    Friend WithEvents adTramite As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection2 As System.Data.SqlClient.SqlConnection
    Friend WithEvents adTramite_Detalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand7 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmReciboDinero))
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.txtCodigo = New System.Windows.Forms.TextBox
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtNombre = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.txtObservaciones = New System.Windows.Forms.TextBox
        Me.ComboMoneda = New System.Windows.Forms.ComboBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.txtCedulaUsuario = New System.Windows.Forms.TextBox
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarAnular = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.dtFecha = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.gridFacturas = New DevExpress.XtraGrid.GridControl
        Me.AdvBandedGridView1 = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.Factura = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.Fecha = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.Prov = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtID_Factura = New System.Windows.Forms.Label
        Me.DsRecibos1 = New dsRecibos
        Me.lbSimbolo3 = New System.Windows.Forms.Label
        Me.lbSimbolo6 = New System.Windows.Forms.Label
        Me.lbSimbolo5 = New System.Windows.Forms.Label
        Me.lbSimbolo4 = New System.Windows.Forms.Label
        Me.lbSimbolo2 = New System.Windows.Forms.Label
        Me.lbSimbolo1 = New System.Windows.Forms.Label
        Me.txtMonto = New DevExpress.XtraEditors.TextEdit
        Me.txtSaldo = New DevExpress.XtraEditors.TextEdit
        Me.txtSaldoAct = New DevExpress.XtraEditors.TextEdit
        Me.txtAbono = New DevExpress.XtraEditors.TextEdit
        Me.txtSaldoAnt = New DevExpress.XtraEditors.TextEdit
        Me.txtIntereses = New DevExpress.XtraEditors.TextEdit
        Me.dtEmitida = New System.Windows.Forms.DateTimePicker
        Me.txtFactura = New DevExpress.XtraEditors.TextEdit
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtAbonoB = New DevExpress.XtraEditors.TextEdit
        Me.txtAbonoSuMoneda = New DevExpress.XtraEditors.TextEdit
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMonto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSaldo_Ant = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colIntereses = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colAbono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adAbonos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daDetalle_Abono = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtSaldoActGen = New DevExpress.XtraEditors.TextEdit
        Me.Label18 = New System.Windows.Forms.Label
        Me.txtAbonoGen = New DevExpress.XtraEditors.TextEdit
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtSaldoAntGen = New DevExpress.XtraEditors.TextEdit
        Me.Label17 = New System.Windows.Forms.Label
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.txtNum_Recibo = New System.Windows.Forms.TextBox
        Me.Check_Dig_Recibo = New System.Windows.Forms.CheckBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label_Id_Recibo = New System.Windows.Forms.Label
        Me.btModFecha = New System.Windows.Forms.Button
        Me.txtTipoCambio = New System.Windows.Forms.TextBox
        Me.AdAsientos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnectionContabilidad = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.adDetallesAsientos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.Label20 = New System.Windows.Forms.Label
        Me.TextBoxComision = New System.Windows.Forms.TextBox
        Me.TxtCambioFactura = New System.Windows.Forms.TextBox
        Me.TbTramite = New System.Windows.Forms.ToolBarButton
        Me.adTramite = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection2 = New System.Data.SqlClient.SqlConnection
        Me.adTramite_Detalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand7 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox6.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.gridFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DsRecibos1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSaldoAct.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSaldoAnt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIntereses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAbonoB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAbonoSuMoneda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtSaldoActGen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAbonoGen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSaldoAntGen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Image = CType(resources.GetObject("Label9.Image"), System.Drawing.Image)
        Me.Label9.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(-16, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(762, 32)
        Me.Label9.TabIndex = 67
        Me.Label9.Text = "Recibo de Dinero"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(1, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 12)
        Me.Label1.TabIndex = 157
        Me.Label1.Text = "Recibo N�"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtCodigo)
        Me.GroupBox6.Controls.Add(Me.Label37)
        Me.GroupBox6.Controls.Add(Me.Label5)
        Me.GroupBox6.Controls.Add(Me.Label2)
        Me.GroupBox6.Controls.Add(Me.txtNombre)
        Me.GroupBox6.Enabled = False
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox6.Location = New System.Drawing.Point(8, 32)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(472, 56)
        Me.GroupBox6.TabIndex = 1
        Me.GroupBox6.TabStop = False
        '
        'txtCodigo
        '
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.ForeColor = System.Drawing.Color.Blue
        Me.txtCodigo.Location = New System.Drawing.Point(13, 32)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(59, 13)
        Me.txtCodigo.TabIndex = 158
        Me.txtCodigo.Text = ""
        '
        'Label37
        '
        Me.Label37.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(0, -1)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(480, 16)
        Me.Label37.TabIndex = 157
        Me.Label37.Text = "Datos del Cliente"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(80, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(384, 12)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Nombre del Cliente"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(12, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "C�digo"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNombre
        '
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Enabled = False
        Me.txtNombre.ForeColor = System.Drawing.Color.Blue
        Me.txtNombre.Location = New System.Drawing.Point(80, 32)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(384, 13)
        Me.txtNombre.TabIndex = 1
        Me.txtNombre.Text = ""
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label29.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label29.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label29.Location = New System.Drawing.Point(8, 344)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(208, 12)
        Me.Label29.TabIndex = 160
        Me.Label29.Text = "Observaciones:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtObservaciones
        '
        Me.txtObservaciones.AutoSize = False
        Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservaciones.Enabled = False
        Me.txtObservaciones.ForeColor = System.Drawing.Color.Blue
        Me.txtObservaciones.Location = New System.Drawing.Point(8, 360)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(208, 48)
        Me.txtObservaciones.TabIndex = 4
        Me.txtObservaciones.Text = ""
        '
        'ComboMoneda
        '
        Me.ComboMoneda.DisplayMember = "Moneda.MonedaNombre"
        Me.ComboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboMoneda.Enabled = False
        Me.ComboMoneda.ForeColor = System.Drawing.Color.Blue
        Me.ComboMoneda.Location = New System.Drawing.Point(488, 48)
        Me.ComboMoneda.Name = "ComboMoneda"
        Me.ComboMoneda.Size = New System.Drawing.Size(97, 21)
        Me.ComboMoneda.TabIndex = 2
        Me.ComboMoneda.ValueMember = "Moneda.CodMoneda"
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label30.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label30.Location = New System.Drawing.Point(488, 32)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(96, 16)
        Me.Label30.TabIndex = 164
        Me.Label30.Text = "Moneda"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label36)
        Me.Panel1.Controls.Add(Me.txtUsuario)
        Me.Panel1.Controls.Add(Me.txtNombreUsuario)
        Me.Panel1.Controls.Add(Me.txtCedulaUsuario)
        Me.Panel1.Location = New System.Drawing.Point(432, 424)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(291, 16)
        Me.Panel1.TabIndex = 0
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(-8, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 0
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtUsuario
        '
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtUsuario.Location = New System.Drawing.Point(64, 0)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtUsuario.Size = New System.Drawing.Size(56, 13)
        Me.txtUsuario.TabIndex = 1
        Me.txtUsuario.Text = ""
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(120, 0)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(168, 13)
        Me.txtNombreUsuario.TabIndex = 2
        Me.txtNombreUsuario.Text = ""
        '
        'txtCedulaUsuario
        '
        Me.txtCedulaUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtCedulaUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCedulaUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCedulaUsuario.Enabled = False
        Me.txtCedulaUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtCedulaUsuario.Location = New System.Drawing.Point(216, 16)
        Me.txtCedulaUsuario.Name = "txtCedulaUsuario"
        Me.txtCedulaUsuario.Size = New System.Drawing.Size(72, 13)
        Me.txtCedulaUsuario.TabIndex = 170
        Me.txtCedulaUsuario.Text = ""
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarAnular, Me.ToolBarImprimir, Me.ToolBarCerrar, Me.TbTramite})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 408)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(720, 58)
        Me.ToolBar1.TabIndex = 7
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Text = "Nuevo"
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        Me.ToolBarBuscar.ImageIndex = 1
        Me.ToolBarBuscar.Text = "Buscar"
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        Me.ToolBarRegistrar.ImageIndex = 2
        Me.ToolBarRegistrar.Text = "Registrar"
        '
        'ToolBarAnular
        '
        Me.ToolBarAnular.Enabled = False
        Me.ToolBarAnular.ImageIndex = 3
        Me.ToolBarAnular.Text = "Anular"
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        Me.ToolBarImprimir.ImageIndex = 7
        Me.ToolBarImprimir.Text = "Imprimir"
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 6
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'dtFecha
        '
        Me.dtFecha.Checked = False
        Me.dtFecha.CustomFormat = "dd/MMMM/yyyy"
        Me.dtFecha.Enabled = False
        Me.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.dtFecha.Location = New System.Drawing.Point(488, 104)
        Me.dtFecha.Name = "dtFecha"
        Me.dtFecha.Size = New System.Drawing.Size(96, 20)
        Me.dtFecha.TabIndex = 3
        Me.dtFecha.Value = New Date(2006, 3, 23, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(440, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 16)
        Me.Label3.TabIndex = 165
        Me.Label3.Text = "Fecha"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gridFacturas
        '
        '
        'gridFacturas.EmbeddedNavigator
        '
        Me.gridFacturas.EmbeddedNavigator.Name = ""
        Me.gridFacturas.Location = New System.Drawing.Point(8, 88)
        Me.gridFacturas.MainView = Me.AdvBandedGridView1
        Me.gridFacturas.Name = "gridFacturas"
        Me.gridFacturas.Size = New System.Drawing.Size(224, 152)
        Me.gridFacturas.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.gridFacturas.TabIndex = 168
        Me.gridFacturas.Text = "GridControl1"
        '
        'AdvBandedGridView1
        '
        Me.AdvBandedGridView1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.AdvBandedGridView1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.Factura, Me.Fecha, Me.Prov})
        Me.AdvBandedGridView1.GroupPanelText = "Facturas Pendientes"
        Me.AdvBandedGridView1.IndicatorWidth = 8
        Me.AdvBandedGridView1.Name = "AdvBandedGridView1"
        Me.AdvBandedGridView1.OptionsView.ShowGroupedColumns = False
        Me.AdvBandedGridView1.OptionsView.ShowGroupPanel = False
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "Facturas Pendiente de Pago"
        Me.GridBand1.Columns.Add(Me.Factura)
        Me.GridBand1.Columns.Add(Me.Fecha)
        Me.GridBand1.Columns.Add(Me.Prov)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.Width = 741
        '
        'Factura
        '
        Me.Factura.Caption = "Factura No."
        Me.Factura.FieldName = "Factura"
        Me.Factura.Name = "Factura"
        Me.Factura.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.Factura.SortIndex = 0
        Me.Factura.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.Factura.Visible = True
        Me.Factura.Width = 332
        '
        'Fecha
        '
        Me.Fecha.Caption = "Fecha"
        Me.Fecha.FieldName = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.Fecha.Visible = True
        Me.Fecha.Width = 409
        '
        'Prov
        '
        Me.Prov.Caption = "Prov"
        Me.Prov.Name = "Prov"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtID_Factura)
        Me.GroupBox1.Controls.Add(Me.lbSimbolo3)
        Me.GroupBox1.Controls.Add(Me.lbSimbolo6)
        Me.GroupBox1.Controls.Add(Me.lbSimbolo5)
        Me.GroupBox1.Controls.Add(Me.lbSimbolo4)
        Me.GroupBox1.Controls.Add(Me.lbSimbolo2)
        Me.GroupBox1.Controls.Add(Me.lbSimbolo1)
        Me.GroupBox1.Controls.Add(Me.txtMonto)
        Me.GroupBox1.Controls.Add(Me.txtSaldo)
        Me.GroupBox1.Controls.Add(Me.txtSaldoAct)
        Me.GroupBox1.Controls.Add(Me.txtAbono)
        Me.GroupBox1.Controls.Add(Me.txtSaldoAnt)
        Me.GroupBox1.Controls.Add(Me.txtIntereses)
        Me.GroupBox1.Controls.Add(Me.dtEmitida)
        Me.GroupBox1.Controls.Add(Me.txtFactura)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox1.Location = New System.Drawing.Point(240, 136)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(480, 104)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'txtID_Factura
        '
        Me.txtID_Factura.BackColor = System.Drawing.Color.RoyalBlue
        Me.txtID_Factura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar.ID_Factura"))
        Me.txtID_Factura.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtID_Factura.ForeColor = System.Drawing.Color.White
        Me.txtID_Factura.Location = New System.Drawing.Point(352, -4)
        Me.txtID_Factura.Name = "txtID_Factura"
        Me.txtID_Factura.Size = New System.Drawing.Size(128, 19)
        Me.txtID_Factura.TabIndex = 182
        Me.txtID_Factura.Text = "0"
        Me.txtID_Factura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DsRecibos1
        '
        Me.DsRecibos1.DataSetName = "dsRecibos"
        Me.DsRecibos1.Locale = New System.Globalization.CultureInfo("es-CR")
        '
        'lbSimbolo3
        '
        Me.lbSimbolo3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lbSimbolo3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbSimbolo3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbSimbolo3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbSimbolo3.Location = New System.Drawing.Point(8, 78)
        Me.lbSimbolo3.Name = "lbSimbolo3"
        Me.lbSimbolo3.Size = New System.Drawing.Size(8, 12)
        Me.lbSimbolo3.TabIndex = 181
        Me.lbSimbolo3.Text = "�"
        Me.lbSimbolo3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbSimbolo6
        '
        Me.lbSimbolo6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lbSimbolo6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbSimbolo6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbSimbolo6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbSimbolo6.Location = New System.Drawing.Point(354, 78)
        Me.lbSimbolo6.Name = "lbSimbolo6"
        Me.lbSimbolo6.Size = New System.Drawing.Size(20, 12)
        Me.lbSimbolo6.TabIndex = 180
        Me.lbSimbolo6.Text = "�"
        Me.lbSimbolo6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbSimbolo5
        '
        Me.lbSimbolo5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lbSimbolo5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbSimbolo5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbSimbolo5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbSimbolo5.Location = New System.Drawing.Point(227, 77)
        Me.lbSimbolo5.Name = "lbSimbolo5"
        Me.lbSimbolo5.Size = New System.Drawing.Size(20, 12)
        Me.lbSimbolo5.TabIndex = 179
        Me.lbSimbolo5.Text = "�"
        Me.lbSimbolo5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbSimbolo4
        '
        Me.lbSimbolo4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lbSimbolo4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbSimbolo4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbSimbolo4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbSimbolo4.Location = New System.Drawing.Point(99, 77)
        Me.lbSimbolo4.Name = "lbSimbolo4"
        Me.lbSimbolo4.Size = New System.Drawing.Size(20, 12)
        Me.lbSimbolo4.TabIndex = 178
        Me.lbSimbolo4.Text = "�"
        Me.lbSimbolo4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbSimbolo2
        '
        Me.lbSimbolo2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lbSimbolo2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbSimbolo2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbSimbolo2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbSimbolo2.Location = New System.Drawing.Point(354, 40)
        Me.lbSimbolo2.Name = "lbSimbolo2"
        Me.lbSimbolo2.Size = New System.Drawing.Size(20, 12)
        Me.lbSimbolo2.TabIndex = 177
        Me.lbSimbolo2.Text = "�"
        Me.lbSimbolo2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbSimbolo1
        '
        Me.lbSimbolo1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lbSimbolo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbSimbolo1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbSimbolo1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbSimbolo1.Location = New System.Drawing.Point(228, 41)
        Me.lbSimbolo1.Name = "lbSimbolo1"
        Me.lbSimbolo1.Size = New System.Drawing.Size(20, 12)
        Me.lbSimbolo1.TabIndex = 176
        Me.lbSimbolo1.Text = "�"
        Me.lbSimbolo1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtMonto
        '
        Me.txtMonto.EditValue = ""
        Me.txtMonto.Location = New System.Drawing.Point(248, 39)
        Me.txtMonto.Name = "txtMonto"
        '
        'txtMonto.Properties
        '
        Me.txtMonto.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.txtMonto.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtMonto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtMonto.Properties.Enabled = False
        Me.txtMonto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtMonto.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtMonto.Size = New System.Drawing.Size(96, 17)
        Me.txtMonto.TabIndex = 174
        '
        'txtSaldo
        '
        Me.txtSaldo.EditValue = ""
        Me.txtSaldo.Location = New System.Drawing.Point(376, 40)
        Me.txtSaldo.Name = "txtSaldo"
        '
        'txtSaldo.Properties
        '
        Me.txtSaldo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.txtSaldo.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtSaldo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSaldo.Properties.Enabled = False
        Me.txtSaldo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtSaldo.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtSaldo.Size = New System.Drawing.Size(96, 17)
        Me.txtSaldo.TabIndex = 172
        '
        'txtSaldoAct
        '
        Me.txtSaldoAct.EditValue = ""
        Me.txtSaldoAct.Location = New System.Drawing.Point(376, 77)
        Me.txtSaldoAct.Name = "txtSaldoAct"
        '
        'txtSaldoAct.Properties
        '
        Me.txtSaldoAct.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.txtSaldoAct.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtSaldoAct.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSaldoAct.Properties.Enabled = False
        Me.txtSaldoAct.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtSaldoAct.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtSaldoAct.Size = New System.Drawing.Size(96, 17)
        Me.txtSaldoAct.TabIndex = 5
        '
        'txtAbono
        '
        Me.txtAbono.EditValue = ""
        Me.txtAbono.Location = New System.Drawing.Point(248, 77)
        Me.txtAbono.Name = "txtAbono"
        '
        'txtAbono.Properties
        '
        Me.txtAbono.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.txtAbono.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtAbono.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAbono.Properties.Enabled = False
        Me.txtAbono.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtAbono.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtAbono.Size = New System.Drawing.Size(96, 17)
        Me.txtAbono.TabIndex = 4
        '
        'txtSaldoAnt
        '
        Me.txtSaldoAnt.EditValue = ""
        Me.txtSaldoAnt.Location = New System.Drawing.Point(120, 77)
        Me.txtSaldoAnt.Name = "txtSaldoAnt"
        '
        'txtSaldoAnt.Properties
        '
        Me.txtSaldoAnt.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.txtSaldoAnt.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtSaldoAnt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSaldoAnt.Properties.Enabled = False
        Me.txtSaldoAnt.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtSaldoAnt.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtSaldoAnt.Size = New System.Drawing.Size(96, 17)
        Me.txtSaldoAnt.TabIndex = 3
        '
        'txtIntereses
        '
        Me.txtIntereses.EditValue = ""
        Me.txtIntereses.Location = New System.Drawing.Point(16, 77)
        Me.txtIntereses.Name = "txtIntereses"
        '
        'txtIntereses.Properties
        '
        Me.txtIntereses.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.txtIntereses.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtIntereses.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtIntereses.Properties.Enabled = False
        Me.txtIntereses.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtIntereses.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtIntereses.Size = New System.Drawing.Size(80, 17)
        Me.txtIntereses.TabIndex = 2
        '
        'dtEmitida
        '
        Me.dtEmitida.Checked = False
        Me.dtEmitida.CustomFormat = "dd/MMMM/yyyy"
        Me.dtEmitida.Enabled = False
        Me.dtEmitida.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.dtEmitida.Location = New System.Drawing.Point(120, 36)
        Me.dtEmitida.Name = "dtEmitida"
        Me.dtEmitida.Size = New System.Drawing.Size(96, 20)
        Me.dtEmitida.TabIndex = 1
        Me.dtEmitida.Value = New Date(2006, 3, 23, 0, 0, 0, 0)
        '
        'txtFactura
        '
        Me.txtFactura.EditValue = ""
        Me.txtFactura.Location = New System.Drawing.Point(16, 38)
        Me.txtFactura.Name = "txtFactura"
        '
        'txtFactura.Properties
        '
        Me.txtFactura.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.txtFactura.Properties.Enabled = False
        Me.txtFactura.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtFactura.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtFactura.Size = New System.Drawing.Size(80, 17)
        Me.txtFactura.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label14.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(248, 26)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(96, 12)
        Me.Label14.TabIndex = 175
        Me.Label14.Text = "Monto Factura"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(376, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(96, 12)
        Me.Label13.TabIndex = 173
        Me.Label13.Text = "Saldo"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(376, 64)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 12)
        Me.Label12.TabIndex = 171
        Me.Label12.Text = "Saldo Actual"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(248, 64)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 12)
        Me.Label11.TabIndex = 169
        Me.Label11.Text = "Abono"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(120, 64)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 12)
        Me.Label10.TabIndex = 167
        Me.Label10.Text = "Saldo Previo"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(12, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(82, 12)
        Me.Label8.TabIndex = 165
        Me.Label8.Text = "Intereses"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(120, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 12)
        Me.Label6.TabIndex = 159
        Me.Label6.Text = "Fecha"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(0, -4)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(480, 19)
        Me.Label4.TabIndex = 157
        Me.Label4.Text = "Datos de la Factura"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(18, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(76, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Factura No."
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtAbonoB
        '
        Me.txtAbonoB.EditValue = ""
        Me.txtAbonoB.Location = New System.Drawing.Point(770, 80)
        Me.txtAbonoB.Name = "txtAbonoB"
        '
        'txtAbonoB.Properties
        '
        Me.txtAbonoB.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtAbonoB.Size = New System.Drawing.Size(72, 19)
        Me.txtAbonoB.TabIndex = 182
        '
        'txtAbonoSuMoneda
        '
        Me.txtAbonoSuMoneda.EditValue = ""
        Me.txtAbonoSuMoneda.Location = New System.Drawing.Point(736, 72)
        Me.txtAbonoSuMoneda.Name = "txtAbonoSuMoneda"
        '
        'txtAbonoSuMoneda.Properties
        '
        Me.txtAbonoSuMoneda.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtAbonoSuMoneda.Size = New System.Drawing.Size(80, 19)
        Me.txtAbonoSuMoneda.TabIndex = 183
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "abonoccobrar.abonoccobrardetalle_abonoccobrar"
        '
        'GridControl2.EmbeddedNavigator
        '
        Me.GridControl2.EmbeddedNavigator.Name = ""
        Me.GridControl2.Location = New System.Drawing.Point(8, 248)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(712, 87)
        Me.GridControl2.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.TabIndex = 6
        Me.GridControl2.Text = "GridControl2"
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colFactura, Me.colMonto, Me.colSaldo_Ant, Me.colIntereses, Me.colAbono, Me.colSaldo})
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colFactura
        '
        Me.colFactura.Caption = "Factura"
        Me.colFactura.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colFactura.FieldName = "Factura"
        Me.colFactura.Name = "colFactura"
        Me.colFactura.VisibleIndex = 0
        '
        'colMonto
        '
        Me.colMonto.Caption = "Monto Fact."
        Me.colMonto.DisplayFormat.FormatString = "#,#0.00"
        Me.colMonto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colMonto.FieldName = "Monto"
        Me.colMonto.Name = "colMonto"
        Me.colMonto.VisibleIndex = 1
        '
        'colSaldo_Ant
        '
        Me.colSaldo_Ant.Caption = "Saldo Ant."
        Me.colSaldo_Ant.DisplayFormat.FormatString = "#,#0.00"
        Me.colSaldo_Ant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSaldo_Ant.FieldName = "Saldo_Ant"
        Me.colSaldo_Ant.Name = "colSaldo_Ant"
        Me.colSaldo_Ant.VisibleIndex = 2
        '
        'colIntereses
        '
        Me.colIntereses.Caption = "Intereses"
        Me.colIntereses.DisplayFormat.FormatString = "#,#0.00"
        Me.colIntereses.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colIntereses.FieldName = "Intereses"
        Me.colIntereses.Name = "colIntereses"
        Me.colIntereses.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colIntereses.VisibleIndex = 3
        '
        'colAbono
        '
        Me.colAbono.Caption = "Abono"
        Me.colAbono.DisplayFormat.FormatString = "#,#0.00"
        Me.colAbono.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colAbono.FieldName = "Abono"
        Me.colAbono.Name = "colAbono"
        Me.colAbono.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colAbono.VisibleIndex = 4
        '
        'colSaldo
        '
        Me.colSaldo.Caption = "Saldo Act."
        Me.colSaldo.DisplayFormat.FormatString = "#,#0.00"
        Me.colSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSaldo.FieldName = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colSaldo.VisibleIndex = 5
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=EDWIN;packet size=4096;integrated security=SSPI;data source=""."";pe" & _
        "rsist security info=False;initial catalog=Hotel"
        '
        'adAbonos
        '
        Me.adAbonos.DeleteCommand = Me.SqlDeleteCommand1
        Me.adAbonos.InsertCommand = Me.SqlInsertCommand1
        Me.adAbonos.SelectCommand = Me.SqlSelectCommand1
        Me.adAbonos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "abonoccobrar", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_Recibo", "Id_Recibo"), New System.Data.Common.DataColumnMapping("Num_Recibo", "Num_Recibo"), New System.Data.Common.DataColumnMapping("Cod_Cliente", "Cod_Cliente"), New System.Data.Common.DataColumnMapping("Saldo_Cuenta", "Saldo_Cuenta"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Saldo_Actual", "Saldo_Actual"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Anula", "Anula"), New System.Data.Common.DataColumnMapping("Ced_Usuario", "Ced_Usuario"), New System.Data.Common.DataColumnMapping("Contabilizado", "Contabilizado"), New System.Data.Common.DataColumnMapping("Asiento", "Asiento"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda"), New System.Data.Common.DataColumnMapping("Nombre_Cliente", "Nombre_Cliente")})})
        Me.adAbonos.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM abonoccobrar WHERE (Id_Recibo = @Original_Id_Recibo) AND (Anula = @Or" & _
        "iginal_Anula) AND (Asiento = @Original_Asiento) AND (Ced_Usuario = @Original_Ced" & _
        "_Usuario) AND (Cod_Cliente = @Original_Cod_Cliente) AND (Cod_Moneda = @Original_" & _
        "Cod_Moneda) AND (Contabilizado = @Original_Contabilizado) AND (Fecha = @Original" & _
        "_Fecha) AND (Monto = @Original_Monto) AND (Nombre_Cliente = @Original_Nombre_Cli" & _
        "ente) AND (Num_Recibo = @Original_Num_Recibo) AND (Observaciones = @Original_Obs" & _
        "ervaciones) AND (Saldo_Actual = @Original_Saldo_Actual) AND (Saldo_Cuenta = @Ori" & _
        "ginal_Saldo_Cuenta)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Recibo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Recibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Asiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Asiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ced_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ced_Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Cliente", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Cliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Recibo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Recibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Actual", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Actual", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Cuenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO abonoccobrar(Num_Recibo, Cod_Cliente, Saldo_Cuenta, Monto, Saldo_Actu" & _
        "al, Fecha, Observaciones, Anula, Ced_Usuario, Contabilizado, Asiento, Cod_Moneda" & _
        ", Nombre_Cliente) VALUES (@Num_Recibo, @Cod_Cliente, @Saldo_Cuenta, @Monto, @Sal" & _
        "do_Actual, @Fecha, @Observaciones, @Anula, @Ced_Usuario, @Contabilizado, @Asient" & _
        "o, @Cod_Moneda, @Nombre_Cliente); SELECT Id_Recibo, Num_Recibo, Cod_Cliente, Sal" & _
        "do_Cuenta, Monto, Saldo_Actual, Fecha, Observaciones, Anula, Ced_Usuario, Contab" & _
        "ilizado, Asiento, Cod_Moneda, Nombre_Cliente FROM abonoccobrar WHERE (Id_Recibo " & _
        "= @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Recibo", System.Data.SqlDbType.BigInt, 8, "Num_Recibo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 4, "Cod_Cliente"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Cuenta", System.Data.SqlDbType.Float, 8, "Saldo_Cuenta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Actual", System.Data.SqlDbType.Float, 8, "Saldo_Actual"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 1, "Anula"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ced_Usuario", System.Data.SqlDbType.VarChar, 75, "Ced_Usuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 1, "Contabilizado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Asiento", System.Data.SqlDbType.VarChar, 15, "Asiento"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, "Nombre_Cliente"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id_Recibo, Num_Recibo, Cod_Cliente, Saldo_Cuenta, Monto, Saldo_Actual, Fec" & _
        "ha, Observaciones, Anula, Ced_Usuario, Contabilizado, Asiento, Cod_Moneda, Nombr" & _
        "e_Cliente FROM abonoccobrar"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE abonoccobrar SET Num_Recibo = @Num_Recibo, Cod_Cliente = @Cod_Cliente, Sal" & _
        "do_Cuenta = @Saldo_Cuenta, Monto = @Monto, Saldo_Actual = @Saldo_Actual, Fecha =" & _
        " @Fecha, Observaciones = @Observaciones, Anula = @Anula, Ced_Usuario = @Ced_Usua" & _
        "rio, Contabilizado = @Contabilizado, Asiento = @Asiento, Cod_Moneda = @Cod_Moned" & _
        "a, Nombre_Cliente = @Nombre_Cliente WHERE (Id_Recibo = @Original_Id_Recibo) AND " & _
        "(Anula = @Original_Anula) AND (Asiento = @Original_Asiento) AND (Ced_Usuario = @" & _
        "Original_Ced_Usuario) AND (Cod_Cliente = @Original_Cod_Cliente) AND (Cod_Moneda " & _
        "= @Original_Cod_Moneda) AND (Contabilizado = @Original_Contabilizado) AND (Fecha" & _
        " = @Original_Fecha) AND (Monto = @Original_Monto) AND (Nombre_Cliente = @Origina" & _
        "l_Nombre_Cliente) AND (Num_Recibo = @Original_Num_Recibo) AND (Observaciones = @" & _
        "Original_Observaciones) AND (Saldo_Actual = @Original_Saldo_Actual) AND (Saldo_C" & _
        "uenta = @Original_Saldo_Cuenta); SELECT Id_Recibo, Num_Recibo, Cod_Cliente, Sald" & _
        "o_Cuenta, Monto, Saldo_Actual, Fecha, Observaciones, Anula, Ced_Usuario, Contabi" & _
        "lizado, Asiento, Cod_Moneda, Nombre_Cliente FROM abonoccobrar WHERE (Id_Recibo =" & _
        " @Id_Recibo)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Recibo", System.Data.SqlDbType.BigInt, 8, "Num_Recibo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 4, "Cod_Cliente"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Cuenta", System.Data.SqlDbType.Float, 8, "Saldo_Cuenta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Actual", System.Data.SqlDbType.Float, 8, "Saldo_Actual"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 1, "Anula"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ced_Usuario", System.Data.SqlDbType.VarChar, 75, "Ced_Usuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 1, "Contabilizado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Asiento", System.Data.SqlDbType.VarChar, 15, "Asiento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, "Nombre_Cliente"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Recibo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Recibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Asiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Asiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ced_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ced_Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Cliente", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Cliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Recibo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Recibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Actual", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Actual", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Cuenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Recibo", System.Data.SqlDbType.BigInt, 8, "Id_Recibo"))
        '
        'daDetalle_Abono
        '
        Me.daDetalle_Abono.DeleteCommand = Me.SqlDeleteCommand2
        Me.daDetalle_Abono.InsertCommand = Me.SqlInsertCommand2
        Me.daDetalle_Abono.SelectCommand = Me.SqlSelectCommand2
        Me.daDetalle_Abono.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "detalle_abonoccobrar", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Consecutivo", "Consecutivo"), New System.Data.Common.DataColumnMapping("Id_Recibo", "Id_Recibo"), New System.Data.Common.DataColumnMapping("Factura", "Factura"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Saldo_Ant", "Saldo_Ant"), New System.Data.Common.DataColumnMapping("Intereses", "Intereses"), New System.Data.Common.DataColumnMapping("Abono", "Abono"), New System.Data.Common.DataColumnMapping("Abono_SuMoneda", "Abono_SuMoneda"), New System.Data.Common.DataColumnMapping("Saldo", "Saldo"), New System.Data.Common.DataColumnMapping("Id_Factura", "Id_Factura")})})
        Me.daDetalle_Abono.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM detalle_abonoccobrar WHERE (Consecutivo = @Original_Consecutivo) AND " & _
        "(Abono = @Original_Abono) AND (Abono_SuMoneda = @Original_Abono_SuMoneda) AND (F" & _
        "actura = @Original_Factura) AND (Id_Factura = @Original_Id_Factura) AND (Id_Reci" & _
        "bo = @Original_Id_Recibo) AND (Intereses = @Original_Intereses) AND (Monto = @Or" & _
        "iginal_Monto) AND (Saldo = @Original_Saldo) AND (Saldo_Ant = @Original_Saldo_Ant" & _
        ") AND (Tipo = @Original_Tipo)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Abono", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Abono_SuMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono_SuMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Factura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Factura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Recibo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Recibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Intereses", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Intereses", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Ant", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Ant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO detalle_abonoccobrar(Id_Recibo, Factura, Tipo, Monto, Saldo_Ant, Inte" & _
        "reses, Abono, Abono_SuMoneda, Saldo, Id_Factura) VALUES (@Id_Recibo, @Factura, @" & _
        "Tipo, @Monto, @Saldo_Ant, @Intereses, @Abono, @Abono_SuMoneda, @Saldo, @Id_Factu" & _
        "ra); SELECT Consecutivo, Id_Recibo, Factura, Tipo, Monto, Saldo_Ant, Intereses, " & _
        "Abono, Abono_SuMoneda, Saldo, Id_Factura FROM detalle_abonoccobrar WHERE (Consec" & _
        "utivo = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Recibo", System.Data.SqlDbType.BigInt, 8, "Id_Recibo"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Factura", System.Data.SqlDbType.Float, 8, "Factura"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 3, "Tipo"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Ant", System.Data.SqlDbType.Float, 8, "Saldo_Ant"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Float, 8, "Intereses"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Abono", System.Data.SqlDbType.Float, 8, "Abono"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Abono_SuMoneda", System.Data.SqlDbType.Float, 8, "Abono_SuMoneda"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo", System.Data.SqlDbType.Float, 8, "Saldo"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Factura", System.Data.SqlDbType.BigInt, 8, "Id_Factura"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Consecutivo, Id_Recibo, Factura, Tipo, Monto, Saldo_Ant, Intereses, Abono," & _
        " Abono_SuMoneda, Saldo, Id_Factura FROM detalle_abonoccobrar"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE detalle_abonoccobrar SET Id_Recibo = @Id_Recibo, Factura = @Factura, Tipo " & _
        "= @Tipo, Monto = @Monto, Saldo_Ant = @Saldo_Ant, Intereses = @Intereses, Abono =" & _
        " @Abono, Abono_SuMoneda = @Abono_SuMoneda, Saldo = @Saldo, Id_Factura = @Id_Fact" & _
        "ura WHERE (Consecutivo = @Original_Consecutivo) AND (Abono = @Original_Abono) AN" & _
        "D (Abono_SuMoneda = @Original_Abono_SuMoneda) AND (Factura = @Original_Factura) " & _
        "AND (Id_Factura = @Original_Id_Factura) AND (Id_Recibo = @Original_Id_Recibo) AN" & _
        "D (Intereses = @Original_Intereses) AND (Monto = @Original_Monto) AND (Saldo = @" & _
        "Original_Saldo) AND (Saldo_Ant = @Original_Saldo_Ant) AND (Tipo = @Original_Tipo" & _
        "); SELECT Consecutivo, Id_Recibo, Factura, Tipo, Monto, Saldo_Ant, Intereses, Ab" & _
        "ono, Abono_SuMoneda, Saldo, Id_Factura FROM detalle_abonoccobrar WHERE (Consecut" & _
        "ivo = @Consecutivo)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Recibo", System.Data.SqlDbType.BigInt, 8, "Id_Recibo"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Factura", System.Data.SqlDbType.Float, 8, "Factura"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 3, "Tipo"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Ant", System.Data.SqlDbType.Float, 8, "Saldo_Ant"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Float, 8, "Intereses"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Abono", System.Data.SqlDbType.Float, 8, "Abono"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Abono_SuMoneda", System.Data.SqlDbType.Float, 8, "Abono_SuMoneda"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo", System.Data.SqlDbType.Float, 8, "Saldo"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Factura", System.Data.SqlDbType.BigInt, 8, "Id_Factura"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Abono", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Abono_SuMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono_SuMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Factura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Factura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Recibo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Recibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Intereses", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Intereses", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Ant", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Ant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Consecutivo", System.Data.SqlDbType.BigInt, 8, "Consecutivo"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Moneda(CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo) VAL" & _
        "UES (@CodMoneda, @MonedaNombre, @ValorCompra, @ValorVenta, @Simbolo); SELECT Cod" & _
        "Moneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda WHERE (CodMon" & _
        "eda = @CodMoneda)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Moneda SET CodMoneda = @CodMoneda, MonedaNombre = @MonedaNombre, ValorComp" & _
        "ra = @ValorCompra, ValorVenta = @ValorVenta, Simbolo = @Simbolo WHERE (CodMoneda" & _
        " = @Original_CodMoneda) AND (MonedaNombre = @Original_MonedaNombre) AND (Simbolo" & _
        " = @Original_Simbolo) AND (ValorCompra = @Original_ValorCompra) AND (ValorVenta " & _
        "= @Original_ValorVenta); SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta" & _
        ", Simbolo FROM Moneda WHERE (CodMoneda = @CodMoneda)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Moneda WHERE (CodMoneda = @Original_CodMoneda) AND (MonedaNombre = @O" & _
        "riginal_MonedaNombre) AND (Simbolo = @Original_Simbolo) AND (ValorCompra = @Orig" & _
        "inal_ValorCompra) AND (ValorVenta = @Original_ValorVenta)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'daMoneda
        '
        Me.daMoneda.DeleteCommand = Me.SqlDeleteCommand3
        Me.daMoneda.InsertCommand = Me.SqlInsertCommand3
        Me.daMoneda.SelectCommand = Me.SqlSelectCommand3
        Me.daMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        Me.daMoneda.UpdateCommand = Me.SqlUpdateCommand3
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtSaldoActGen)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.txtAbonoGen)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.txtSaldoAntGen)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox2.Location = New System.Drawing.Point(328, 344)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(392, 64)
        Me.GroupBox2.TabIndex = 171
        Me.GroupBox2.TabStop = False
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(8, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(376, 16)
        Me.Label15.TabIndex = 157
        Me.Label15.Text = "Saldos de la Cuenta"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSaldoActGen
        '
        Me.txtSaldoActGen.EditValue = ""
        Me.txtSaldoActGen.Location = New System.Drawing.Point(268, 32)
        Me.txtSaldoActGen.Name = "txtSaldoActGen"
        '
        'txtSaldoActGen.Properties
        '
        Me.txtSaldoActGen.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtSaldoActGen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSaldoActGen.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtSaldoActGen.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtSaldoActGen.Size = New System.Drawing.Size(107, 19)
        Me.txtSaldoActGen.TabIndex = 162
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label18.Location = New System.Drawing.Point(268, 16)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(108, 12)
        Me.Label18.TabIndex = 161
        Me.Label18.Text = "Saldo Act."
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtAbonoGen
        '
        Me.txtAbonoGen.EditValue = ""
        Me.txtAbonoGen.Location = New System.Drawing.Point(136, 32)
        Me.txtAbonoGen.Name = "txtAbonoGen"
        '
        'txtAbonoGen.Properties
        '
        Me.txtAbonoGen.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtAbonoGen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAbonoGen.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtAbonoGen.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtAbonoGen.Size = New System.Drawing.Size(107, 19)
        Me.txtAbonoGen.TabIndex = 160
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label16.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(136, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(108, 12)
        Me.Label16.TabIndex = 159
        Me.Label16.Text = "Monto Recibos"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtSaldoAntGen
        '
        Me.txtSaldoAntGen.EditValue = ""
        Me.txtSaldoAntGen.Location = New System.Drawing.Point(13, 32)
        Me.txtSaldoAntGen.Name = "txtSaldoAntGen"
        '
        'txtSaldoAntGen.Properties
        '
        Me.txtSaldoAntGen.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtSaldoAntGen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSaldoAntGen.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtSaldoAntGen.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtSaldoAntGen.Size = New System.Drawing.Size(107, 19)
        Me.txtSaldoAntGen.TabIndex = 158
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label17.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(12, 16)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(108, 12)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Saldo Ant."
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'CheckBox1
        '
        Me.CheckBox1.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox1.Enabled = False
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.Red
        Me.CheckBox1.Location = New System.Drawing.Point(320, 104)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox1.TabIndex = 172
        Me.CheckBox1.Text = "Anulada"
        '
        'txtNum_Recibo
        '
        Me.txtNum_Recibo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNum_Recibo.Enabled = False
        Me.txtNum_Recibo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNum_Recibo.Location = New System.Drawing.Point(74, 15)
        Me.txtNum_Recibo.Name = "txtNum_Recibo"
        Me.txtNum_Recibo.Size = New System.Drawing.Size(72, 13)
        Me.txtNum_Recibo.TabIndex = 159
        Me.txtNum_Recibo.Text = ""
        '
        'Check_Dig_Recibo
        '
        Me.Check_Dig_Recibo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Dig_Recibo.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Check_Dig_Recibo.Location = New System.Drawing.Point(592, 72)
        Me.Check_Dig_Recibo.Name = "Check_Dig_Recibo"
        Me.Check_Dig_Recibo.Size = New System.Drawing.Size(120, 16)
        Me.Check_Dig_Recibo.TabIndex = 173
        Me.Check_Dig_Recibo.Text = "Digitar Recibo"
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label19.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(592, 32)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(96, 16)
        Me.Label19.TabIndex = 174
        Me.Label19.Text = "Tipo Cambio"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label_Id_Recibo
        '
        Me.Label_Id_Recibo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Id_Recibo"))
        Me.Label_Id_Recibo.Location = New System.Drawing.Point(240, 104)
        Me.Label_Id_Recibo.Name = "Label_Id_Recibo"
        Me.Label_Id_Recibo.Size = New System.Drawing.Size(64, 16)
        Me.Label_Id_Recibo.TabIndex = 184
        '
        'btModFecha
        '
        Me.btModFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btModFecha.Location = New System.Drawing.Point(592, 104)
        Me.btModFecha.Name = "btModFecha"
        Me.btModFecha.Size = New System.Drawing.Size(88, 24)
        Me.btModFecha.TabIndex = 185
        Me.btModFecha.Text = "Cambiar Fecha"
        Me.btModFecha.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Location = New System.Drawing.Point(592, 48)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(96, 20)
        Me.txtTipoCambio.TabIndex = 186
        Me.txtTipoCambio.Text = "0"
        Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AdAsientos
        '
        Me.AdAsientos.DeleteCommand = Me.SqlDeleteCommand4
        Me.AdAsientos.InsertCommand = Me.SqlInsertCommand4
        Me.AdAsientos.SelectCommand = Me.SqlSelectCommand4
        Me.AdAsientos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AsientosContables", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumAsiento", "NumAsiento"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("IdNumDoc", "IdNumDoc"), New System.Data.Common.DataColumnMapping("NumDoc", "NumDoc"), New System.Data.Common.DataColumnMapping("Beneficiario", "Beneficiario"), New System.Data.Common.DataColumnMapping("TipoDoc", "TipoDoc"), New System.Data.Common.DataColumnMapping("Accion", "Accion"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("FechaEntrada", "FechaEntrada"), New System.Data.Common.DataColumnMapping("Mayorizado", "Mayorizado"), New System.Data.Common.DataColumnMapping("Periodo", "Periodo"), New System.Data.Common.DataColumnMapping("NumMayorizado", "NumMayorizado"), New System.Data.Common.DataColumnMapping("Modulo", "Modulo"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("NombreUsuario", "NombreUsuario"), New System.Data.Common.DataColumnMapping("TotalDebe", "TotalDebe"), New System.Data.Common.DataColumnMapping("TotalHaber", "TotalHaber"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio")})})
        Me.AdAsientos.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM AsientosContables WHERE (NumAsiento = @Original_NumAsiento) AND (Acci" & _
        "on = @Original_Accion) AND (Anulado = @Original_Anulado) AND (Beneficiario = @Or" & _
        "iginal_Beneficiario) AND (CodMoneda = @Original_CodMoneda) AND (Fecha = @Origina" & _
        "l_Fecha) AND (FechaEntrada = @Original_FechaEntrada) AND (IdNumDoc = @Original_I" & _
        "dNumDoc) AND (Mayorizado = @Original_Mayorizado) AND (Modulo = @Original_Modulo)" & _
        " AND (NombreUsuario = @Original_NombreUsuario) AND (NumDoc = @Original_NumDoc) A" & _
        "ND (NumMayorizado = @Original_NumMayorizado) AND (Observaciones = @Original_Obse" & _
        "rvaciones) AND (Periodo = @Original_Periodo) AND (TipoCambio = @Original_TipoCam" & _
        "bio) AND (TipoDoc = @Original_TipoDoc) AND (TotalDebe = @Original_TotalDebe) AND" & _
        " (TotalHaber = @Original_TotalHaber)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnectionContabilidad
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Accion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Accion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Beneficiario", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Beneficiario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdNumDoc", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdNumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Mayorizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Mayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Modulo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Modulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Periodo", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periodo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDoc", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalDebe", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalDebe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalHaber", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalHaber", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnectionContabilidad
        '
        Me.SqlConnectionContabilidad.ConnectionString = "workstation id=IALVAREZ;packet size=4096;integrated security=SSPI;data source="".""" & _
        ";persist security info=False;initial catalog=Contabilidad"
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO AsientosContables(NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, " & _
        "TipoDoc, Accion, Anulado, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modu" & _
        "lo, Observaciones, NombreUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio) " & _
        "VALUES (@NumAsiento, @Fecha, @IdNumDoc, @NumDoc, @Beneficiario, @TipoDoc, @Accio" & _
        "n, @Anulado, @FechaEntrada, @Mayorizado, @Periodo, @NumMayorizado, @Modulo, @Obs" & _
        "ervaciones, @NombreUsuario, @TotalDebe, @TotalHaber, @CodMoneda, @TipoCambio); S" & _
        "ELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDoc, Accion, Anulad" & _
        "o, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Observaciones, Nomb" & _
        "reUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM AsientosContables W" & _
        "HERE (NumAsiento = @NumAsiento)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnectionContabilidad
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 4, "Fecha"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdNumDoc", System.Data.SqlDbType.BigInt, 8, "IdNumDoc"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumDoc", System.Data.SqlDbType.VarChar, 50, "NumDoc"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Beneficiario", System.Data.SqlDbType.VarChar, 250, "Beneficiario"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDoc", System.Data.SqlDbType.Int, 4, "TipoDoc"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 50, "Accion"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 4, "FechaEntrada"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Mayorizado", System.Data.SqlDbType.Bit, 1, "Mayorizado"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Periodo", System.Data.SqlDbType.VarChar, 8, "Periodo"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Modulo", System.Data.SqlDbType.VarChar, 50, "Modulo"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalDebe", System.Data.SqlDbType.Float, 8, "TotalDebe"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalHaber", System.Data.SqlDbType.Float, 8, "TotalHaber"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDoc, Accion, Anulad" & _
        "o, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Observaciones, Nomb" & _
        "reUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM AsientosContables"
        Me.SqlSelectCommand4.Connection = Me.SqlConnectionContabilidad
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE AsientosContables SET NumAsiento = @NumAsiento, Fecha = @Fecha, IdNumDoc =" & _
        " @IdNumDoc, NumDoc = @NumDoc, Beneficiario = @Beneficiario, TipoDoc = @TipoDoc, " & _
        "Accion = @Accion, Anulado = @Anulado, FechaEntrada = @FechaEntrada, Mayorizado =" & _
        " @Mayorizado, Periodo = @Periodo, NumMayorizado = @NumMayorizado, Modulo = @Modu" & _
        "lo, Observaciones = @Observaciones, NombreUsuario = @NombreUsuario, TotalDebe = " & _
        "@TotalDebe, TotalHaber = @TotalHaber, CodMoneda = @CodMoneda, TipoCambio = @Tipo" & _
        "Cambio WHERE (NumAsiento = @Original_NumAsiento) AND (Accion = @Original_Accion)" & _
        " AND (Anulado = @Original_Anulado) AND (Beneficiario = @Original_Beneficiario) A" & _
        "ND (CodMoneda = @Original_CodMoneda) AND (Fecha = @Original_Fecha) AND (FechaEnt" & _
        "rada = @Original_FechaEntrada) AND (IdNumDoc = @Original_IdNumDoc) AND (Mayoriza" & _
        "do = @Original_Mayorizado) AND (Modulo = @Original_Modulo) AND (NombreUsuario = " & _
        "@Original_NombreUsuario) AND (NumDoc = @Original_NumDoc) AND (NumMayorizado = @O" & _
        "riginal_NumMayorizado) AND (Observaciones = @Original_Observaciones) AND (Period" & _
        "o = @Original_Periodo) AND (TipoCambio = @Original_TipoCambio) AND (TipoDoc = @O" & _
        "riginal_TipoDoc) AND (TotalDebe = @Original_TotalDebe) AND (TotalHaber = @Origin" & _
        "al_TotalHaber); SELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDo" & _
        "c, Accion, Anulado, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Ob" & _
        "servaciones, NombreUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM As" & _
        "ientosContables WHERE (NumAsiento = @NumAsiento)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnectionContabilidad
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 4, "Fecha"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdNumDoc", System.Data.SqlDbType.BigInt, 8, "IdNumDoc"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumDoc", System.Data.SqlDbType.VarChar, 50, "NumDoc"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Beneficiario", System.Data.SqlDbType.VarChar, 250, "Beneficiario"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDoc", System.Data.SqlDbType.Int, 4, "TipoDoc"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 50, "Accion"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 4, "FechaEntrada"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Mayorizado", System.Data.SqlDbType.Bit, 1, "Mayorizado"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Periodo", System.Data.SqlDbType.VarChar, 8, "Periodo"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Modulo", System.Data.SqlDbType.VarChar, 50, "Modulo"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalDebe", System.Data.SqlDbType.Float, 8, "TotalDebe"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalHaber", System.Data.SqlDbType.Float, 8, "TotalHaber"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Accion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Accion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Beneficiario", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Beneficiario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdNumDoc", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdNumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Mayorizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Mayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Modulo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Modulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Periodo", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periodo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDoc", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalDebe", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalDebe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalHaber", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalHaber", System.Data.DataRowVersion.Original, Nothing))
        '
        'adDetallesAsientos
        '
        Me.adDetallesAsientos.DeleteCommand = Me.SqlDeleteCommand5
        Me.adDetallesAsientos.InsertCommand = Me.SqlInsertCommand5
        Me.adDetallesAsientos.SelectCommand = Me.SqlSelectCommand5
        Me.adDetallesAsientos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "DetallesAsientosContable", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ID_Detalle", "ID_Detalle"), New System.Data.Common.DataColumnMapping("NumAsiento", "NumAsiento"), New System.Data.Common.DataColumnMapping("Cuenta", "Cuenta"), New System.Data.Common.DataColumnMapping("NombreCuenta", "NombreCuenta"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Debe", "Debe"), New System.Data.Common.DataColumnMapping("Haber", "Haber"), New System.Data.Common.DataColumnMapping("DescripcionAsiento", "DescripcionAsiento"), New System.Data.Common.DataColumnMapping("Tipocambio", "Tipocambio")})})
        Me.adDetallesAsientos.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM DetallesAsientosContable WHERE (ID_Detalle = @Original_ID_Detalle) AN" & _
        "D (Cuenta = @Original_Cuenta) AND (Debe = @Original_Debe) AND (DescripcionAsient" & _
        "o = @Original_DescripcionAsiento) AND (Haber = @Original_Haber) AND (Monto = @Or" & _
        "iginal_Monto) AND (NombreCuenta = @Original_NombreCuenta) AND (NumAsiento = @Ori" & _
        "ginal_NumAsiento) AND (Tipocambio = @Original_Tipocambio OR @Original_Tipocambio" & _
        " IS NULL AND Tipocambio IS NULL)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnectionContabilidad
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID_Detalle", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID_Detalle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Debe", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Debe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Haber", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Haber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipocambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipocambio", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO DetallesAsientosContable(NumAsiento, Cuenta, NombreCuenta, Monto, Deb" & _
        "e, Haber, DescripcionAsiento, Tipocambio) VALUES (@NumAsiento, @Cuenta, @NombreC" & _
        "uenta, @Monto, @Debe, @Haber, @DescripcionAsiento, @Tipocambio); SELECT ID_Detal" & _
        "le, NumAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, DescripcionAsiento, Ti" & _
        "pocambio FROM DetallesAsientosContable WHERE (ID_Detalle = @@IDENTITY)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnectionContabilidad
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta", System.Data.SqlDbType.VarChar, 255, "Cuenta"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Debe", System.Data.SqlDbType.Bit, 1, "Debe"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Haber", System.Data.SqlDbType.Bit, 1, "Haber"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, "DescripcionAsiento"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipocambio", System.Data.SqlDbType.Float, 8, "Tipocambio"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT ID_Detalle, NumAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, Descripc" & _
        "ionAsiento, Tipocambio FROM DetallesAsientosContable"
        Me.SqlSelectCommand5.Connection = Me.SqlConnectionContabilidad
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE DetallesAsientosContable SET NumAsiento = @NumAsiento, Cuenta = @Cuenta, N" & _
        "ombreCuenta = @NombreCuenta, Monto = @Monto, Debe = @Debe, Haber = @Haber, Descr" & _
        "ipcionAsiento = @DescripcionAsiento, Tipocambio = @Tipocambio WHERE (ID_Detalle " & _
        "= @Original_ID_Detalle) AND (Cuenta = @Original_Cuenta) AND (Debe = @Original_De" & _
        "be) AND (DescripcionAsiento = @Original_DescripcionAsiento) AND (Haber = @Origin" & _
        "al_Haber) AND (Monto = @Original_Monto) AND (NombreCuenta = @Original_NombreCuen" & _
        "ta) AND (NumAsiento = @Original_NumAsiento) AND (Tipocambio = @Original_Tipocamb" & _
        "io OR @Original_Tipocambio IS NULL AND Tipocambio IS NULL); SELECT ID_Detalle, N" & _
        "umAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, DescripcionAsiento, Tipocam" & _
        "bio FROM DetallesAsientosContable WHERE (ID_Detalle = @ID_Detalle)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnectionContabilidad
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta", System.Data.SqlDbType.VarChar, 255, "Cuenta"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Debe", System.Data.SqlDbType.Bit, 1, "Debe"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Haber", System.Data.SqlDbType.Bit, 1, "Haber"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, "DescripcionAsiento"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipocambio", System.Data.SqlDbType.Float, 8, "Tipocambio"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID_Detalle", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID_Detalle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Debe", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Debe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Haber", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Haber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipocambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipocambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID_Detalle", System.Data.SqlDbType.BigInt, 8, "ID_Detalle"))
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label20.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(224, 344)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(88, 12)
        Me.Label20.TabIndex = 187
        Me.Label20.Text = "Comisi�n:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBoxComision
        '
        Me.TextBoxComision.Location = New System.Drawing.Point(224, 360)
        Me.TextBoxComision.Name = "TextBoxComision"
        Me.TextBoxComision.Size = New System.Drawing.Size(96, 20)
        Me.TextBoxComision.TabIndex = 188
        Me.TextBoxComision.Text = "0"
        Me.TextBoxComision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtCambioFactura
        '
        Me.TxtCambioFactura.Location = New System.Drawing.Point(784, 336)
        Me.TxtCambioFactura.Name = "TxtCambioFactura"
        Me.TxtCambioFactura.Size = New System.Drawing.Size(32, 20)
        Me.TxtCambioFactura.TabIndex = 189
        Me.TxtCambioFactura.Text = ""
        '
        'TbTramite
        '
        Me.TbTramite.Enabled = False
        Me.TbTramite.ImageIndex = 5
        Me.TbTramite.Text = "Tr�mite"
        '
        'adTramite
        '
        Me.adTramite.DeleteCommand = Me.SqlDeleteCommand6
        Me.adTramite.InsertCommand = Me.SqlInsertCommand6
        Me.adTramite.SelectCommand = Me.SqlSelectCommand6
        Me.adTramite.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Tramite", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("CodCliente", "CodCliente"), New System.Data.Common.DataColumnMapping("NombreCliente", "NombreCliente"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("NombreUsuario", "NombreUsuario"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Recibido", "Recibido"), New System.Data.Common.DataColumnMapping("FechaRecibido", "FechaRecibido"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Pagado", "Pagado"), New System.Data.Common.DataColumnMapping("NumRecibo", "NumRecibo")})})
        Me.adTramite.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT Id, Fecha, CodCliente, NombreCliente, Usuario, NombreUsuario, Anulado, Rec" & _
        "ibido, FechaRecibido, Total, Observaciones, Pagado, NumRecibo FROM Tramite"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection2
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO Tramite(Fecha, CodCliente, NombreCliente, Usuario, NombreUsuario, Anu" & _
        "lado, Recibido, FechaRecibido, Total, Observaciones, Pagado, NumRecibo) VALUES (" & _
        "@Fecha, @CodCliente, @NombreCliente, @Usuario, @NombreUsuario, @Anulado, @Recibi" & _
        "do, @FechaRecibido, @Total, @Observaciones, @Pagado, @NumRecibo); SELECT Id, Fec" & _
        "ha, CodCliente, NombreCliente, Usuario, NombreUsuario, Anulado, Recibido, FechaR" & _
        "ecibido, Total, Observaciones, Pagado, NumRecibo FROM Tramite WHERE (Id = @@IDEN" & _
        "TITY)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection2
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodCliente", System.Data.SqlDbType.Int, 4, "CodCliente"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCliente", System.Data.SqlDbType.VarChar, 250, "NombreCliente"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 50, "Usuario"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 250, "NombreUsuario"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Recibido", System.Data.SqlDbType.Bit, 1, "Recibido"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaRecibido", System.Data.SqlDbType.DateTime, 8, "FechaRecibido"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Pagado", System.Data.SqlDbType.Bit, 1, "Pagado"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumRecibo", System.Data.SqlDbType.Int, 4, "NumRecibo"))
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE Tramite SET Fecha = @Fecha, CodCliente = @CodCliente, NombreCliente = @Nom" & _
        "breCliente, Usuario = @Usuario, NombreUsuario = @NombreUsuario, Anulado = @Anula" & _
        "do, Recibido = @Recibido, FechaRecibido = @FechaRecibido, Total = @Total, Observ" & _
        "aciones = @Observaciones, Pagado = @Pagado, NumRecibo = @NumRecibo WHERE (Id = @" & _
        "Original_Id) AND (Anulado = @Original_Anulado) AND (CodCliente = @Original_CodCl" & _
        "iente) AND (Fecha = @Original_Fecha) AND (FechaRecibido = @Original_FechaRecibid" & _
        "o) AND (NombreCliente = @Original_NombreCliente) AND (NombreUsuario = @Original_" & _
        "NombreUsuario) AND (NumRecibo = @Original_NumRecibo) AND (Observaciones = @Origi" & _
        "nal_Observaciones) AND (Pagado = @Original_Pagado) AND (Recibido = @Original_Rec" & _
        "ibido) AND (Total = @Original_Total) AND (Usuario = @Original_Usuario); SELECT I" & _
        "d, Fecha, CodCliente, NombreCliente, Usuario, NombreUsuario, Anulado, Recibido, " & _
        "FechaRecibido, Total, Observaciones, Pagado, NumRecibo FROM Tramite WHERE (Id = " & _
        "@Id)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection2
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodCliente", System.Data.SqlDbType.Int, 4, "CodCliente"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCliente", System.Data.SqlDbType.VarChar, 250, "NombreCliente"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 50, "Usuario"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 250, "NombreUsuario"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Recibido", System.Data.SqlDbType.Bit, 1, "Recibido"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaRecibido", System.Data.SqlDbType.DateTime, 8, "FechaRecibido"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Pagado", System.Data.SqlDbType.Bit, 1, "Pagado"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumRecibo", System.Data.SqlDbType.Int, 4, "NumRecibo"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodCliente", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodCliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaRecibido", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaRecibido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCliente", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumRecibo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumRecibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Pagado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Pagado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Recibido", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Recibido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id"))
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM Tramite WHERE (Id = @Original_Id) AND (Anulado = @Original_Anulado) A" & _
        "ND (CodCliente = @Original_CodCliente) AND (Fecha = @Original_Fecha) AND (FechaR" & _
        "ecibido = @Original_FechaRecibido) AND (NombreCliente = @Original_NombreCliente)" & _
        " AND (NombreUsuario = @Original_NombreUsuario) AND (NumRecibo = @Original_NumRec" & _
        "ibo) AND (Observaciones = @Original_Observaciones) AND (Pagado = @Original_Pagad" & _
        "o) AND (Recibido = @Original_Recibido) AND (Total = @Original_Total) AND (Usuari" & _
        "o = @Original_Usuario)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection2
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodCliente", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodCliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaRecibido", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaRecibido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCliente", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumRecibo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumRecibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Pagado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Pagado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Recibido", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Recibido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection2
        '
        Me.SqlConnection2.ConnectionString = "workstation id=DIEGOGAMBOA;packet size=4096;integrated security=SSPI;data source=" & _
        "DIEGOGAMBOA;persist security info=False;initial catalog=Hotel"
        '
        'adTramite_Detalle
        '
        Me.adTramite_Detalle.DeleteCommand = Me.SqlDeleteCommand7
        Me.adTramite_Detalle.InsertCommand = Me.SqlInsertCommand7
        Me.adTramite_Detalle.SelectCommand = Me.SqlSelectCommand7
        Me.adTramite_Detalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TramiteDetalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Id_Tramite", "Id_Tramite"), New System.Data.Common.DataColumnMapping("Id_Factura", "Id_Factura"), New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("MontoFactura", "MontoFactura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("VenceViejo", "VenceViejo"), New System.Data.Common.DataColumnMapping("VenceNuevo", "VenceNuevo"), New System.Data.Common.DataColumnMapping("Saldo", "Saldo")})})
        Me.adTramite_Detalle.UpdateCommand = Me.SqlUpdateCommand7
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT Id, Id_Tramite, Id_Factura, Num_Factura, MontoFactura, Fecha, VenceViejo, " & _
        "VenceNuevo, Saldo FROM TramiteDetalle"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection2
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = "INSERT INTO TramiteDetalle(Id_Tramite, Id_Factura, Num_Factura, MontoFactura, Fec" & _
        "ha, VenceViejo, VenceNuevo, Saldo) VALUES (@Id_Tramite, @Id_Factura, @Num_Factur" & _
        "a, @MontoFactura, @Fecha, @VenceViejo, @VenceNuevo, @Saldo); SELECT Id, Id_Trami" & _
        "te, Id_Factura, Num_Factura, MontoFactura, Fecha, VenceViejo, VenceNuevo, Saldo " & _
        "FROM TramiteDetalle WHERE (Id = @@IDENTITY)"
        Me.SqlInsertCommand7.Connection = Me.SqlConnection2
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Tramite", System.Data.SqlDbType.BigInt, 8, "Id_Tramite"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Factura", System.Data.SqlDbType.BigInt, 8, "Id_Factura"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.BigInt, 8, "Num_Factura"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoFactura", System.Data.SqlDbType.Float, 8, "MontoFactura"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VenceViejo", System.Data.SqlDbType.DateTime, 8, "VenceViejo"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VenceNuevo", System.Data.SqlDbType.DateTime, 8, "VenceNuevo"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo", System.Data.SqlDbType.Float, 8, "Saldo"))
        '
        'SqlUpdateCommand7
        '
        Me.SqlUpdateCommand7.CommandText = "UPDATE TramiteDetalle SET Id_Tramite = @Id_Tramite, Id_Factura = @Id_Factura, Num" & _
        "_Factura = @Num_Factura, MontoFactura = @MontoFactura, Fecha = @Fecha, VenceViej" & _
        "o = @VenceViejo, VenceNuevo = @VenceNuevo, Saldo = @Saldo WHERE (Id = @Original_" & _
        "Id) AND (Fecha = @Original_Fecha) AND (Id_Factura = @Original_Id_Factura) AND (I" & _
        "d_Tramite = @Original_Id_Tramite) AND (MontoFactura = @Original_MontoFactura) AN" & _
        "D (Num_Factura = @Original_Num_Factura) AND (Saldo = @Original_Saldo) AND (Vence" & _
        "Nuevo = @Original_VenceNuevo) AND (VenceViejo = @Original_VenceViejo); SELECT Id" & _
        ", Id_Tramite, Id_Factura, Num_Factura, MontoFactura, Fecha, VenceViejo, VenceNue" & _
        "vo, Saldo FROM TramiteDetalle WHERE (Id = @Id)"
        Me.SqlUpdateCommand7.Connection = Me.SqlConnection2
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Tramite", System.Data.SqlDbType.BigInt, 8, "Id_Tramite"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Factura", System.Data.SqlDbType.BigInt, 8, "Id_Factura"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.BigInt, 8, "Num_Factura"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoFactura", System.Data.SqlDbType.Float, 8, "MontoFactura"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VenceViejo", System.Data.SqlDbType.DateTime, 8, "VenceViejo"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VenceNuevo", System.Data.SqlDbType.DateTime, 8, "VenceNuevo"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo", System.Data.SqlDbType.Float, 8, "Saldo"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Factura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Tramite", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Tramite", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoFactura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Factura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VenceNuevo", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VenceNuevo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VenceViejo", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VenceViejo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id"))
        '
        'SqlDeleteCommand7
        '
        Me.SqlDeleteCommand7.CommandText = "DELETE FROM TramiteDetalle WHERE (Id = @Original_Id) AND (Fecha = @Original_Fecha" & _
        ") AND (Id_Factura = @Original_Id_Factura) AND (Id_Tramite = @Original_Id_Tramite" & _
        ") AND (MontoFactura = @Original_MontoFactura) AND (Num_Factura = @Original_Num_F" & _
        "actura) AND (Saldo = @Original_Saldo) AND (VenceNuevo = @Original_VenceNuevo) AN" & _
        "D (VenceViejo = @Original_VenceViejo)"
        Me.SqlDeleteCommand7.Connection = Me.SqlConnection2
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Factura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Tramite", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Tramite", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoFactura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Factura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VenceNuevo", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VenceNuevo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VenceViejo", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VenceViejo", System.Data.DataRowVersion.Original, Nothing))
        '
        'frmReciboDinero
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(720, 466)
        Me.Controls.Add(Me.TxtCambioFactura)
        Me.Controls.Add(Me.TextBoxComision)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.txtNum_Recibo)
        Me.Controls.Add(Me.dtFecha)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.btModFecha)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label_Id_Recibo)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Check_Dig_Recibo)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GridControl2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gridFacturas)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboMoneda)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtAbonoSuMoneda)
        Me.Controls.Add(Me.txtAbonoB)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(728, 500)
        Me.MinimumSize = New System.Drawing.Size(728, 500)
        Me.Name = "frmReciboDinero"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recibo de Dinero"
        Me.GroupBox6.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.gridFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DsRecibos1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSaldoAct.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSaldoAnt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIntereses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAbonoB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAbonoSuMoneda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.txtSaldoActGen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAbonoGen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSaldoAntGen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmReciboDinero_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Conexion2 As New Conexion
        Try
            SqlConnection1.ConnectionString = GetSetting("Hotel", "Hotel", "Conexion")
            Me.SqlConnectionContabilidad.ConnectionString = GetSetting("SeeSoft", "Contabilidad", "Conexion")

            Me.daMoneda.Fill(Me.DsRecibos1, "Moneda")
            'Establecer valores por defecto Abonoccobrar
            Me.DsRecibos1.abonoccobrar.Num_ReciboColumn.DefaultValue = 0
            Me.DsRecibos1.abonoccobrar.AnulaColumn.DefaultValue = 0
            Me.DsRecibos1.abonoccobrar.Cod_ClienteColumn.DefaultValue = 0
            Me.DsRecibos1.abonoccobrar.Saldo_ActualColumn.DefaultValue = 0
            Me.DsRecibos1.abonoccobrar.MontoColumn.DefaultValue = 0
            Me.DsRecibos1.abonoccobrar.Saldo_CuentaColumn.DefaultValue = 0
            Me.DsRecibos1.abonoccobrar.FechaColumn.DefaultValue = Now
            Me.DsRecibos1.abonoccobrar.ContabilizadoColumn.DefaultValue = 0
            Me.DsRecibos1.abonoccobrar.AsientoColumn.DefaultValue = 0
            Me.DsRecibos1.abonoccobrar.Cod_MonedaColumn.DefaultValue = 1
            Me.DsRecibos1.abonoccobrar.ObservacionesColumn.DefaultValue = ""
            'Establecer valores por defecto Detalla_Abonoccobrar
            Me.DsRecibos1.detalle_abonoccobrar.Id_ReciboColumn.DefaultValue = 0
            Me.DsRecibos1.detalle_abonoccobrar.FacturaColumn.DefaultValue = 0
            Me.DsRecibos1.detalle_abonoccobrar.TipoColumn.DefaultValue = "CRE"
            Me.DsRecibos1.detalle_abonoccobrar.MontoColumn.DefaultValue = 0
            Me.DsRecibos1.detalle_abonoccobrar.Saldo_AntColumn.DefaultValue = 0
            Me.DsRecibos1.detalle_abonoccobrar.InteresesColumn.DefaultValue = 0
            Me.DsRecibos1.detalle_abonoccobrar.AbonoColumn.DefaultValue = 0
            Me.DsRecibos1.detalle_abonoccobrar.Abono_SuMonedaColumn.DefaultValue = 0
            Me.DsRecibos1.detalle_abonoccobrar.SaldoColumn.DefaultValue = 0
            defaultValuesAsientos()
            TipoCambio = 1
            txtTipoCambio.Text = 1
            dtFecha.Value = Date.Now
            dtEmitida.Value = Date.Now
            ToolBar1.Buttons(1).Enabled = True
            btModFecha.Visible = False
            Binding()
            txtTipoCambio.Text = Conexion2.SlqExecuteScalar(Conexion2.Conectar, "Select ValorVenta from Moneda where CodMoneda =2")
            Conexion2.DesConectar(Conexion2.Conectar)

            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO
            Dim cConexion As New Conexion
            cConexion.DesConectar(cConexion.sQlconexion)


            If gloNoClave Then
                Loggin_Usuario()
                Me.NuevoRecibo()
                ComboMoneda.Focus()
            Else
                txtUsuario.Focus()
            End If
            cConexion.DesConectar(cConexion.sQlconexion)
            Tbl = cConexion.SlqExecuteScalar(cConexion.Conectar("Hotel"), "SELECT Cedula FROM Configuraciones")
            '---------------------------------------------------------------
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function Binding()

        txtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Cod_Cliente"))
        txtNombre.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Nombre_Cliente"))
        txtObservaciones.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Observaciones"))

        txtCedulaUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Ced_Usuario"))
        dtFecha.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Fecha"))
        lbSimbolo1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "Moneda.Simbolo"))


        txtMonto.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar.Monto"))
        txtSaldo.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar.Saldo_Ant"))
        txtSaldoAct.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar.Saldo"))
        txtIntereses.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar.Intereses"))
        txtFactura.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar.Factura"))
        txtAbonoB.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar.Abono"))
        Me.TxtCambioFactura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar.TipoCambio"))
        txtAbonoSuMoneda.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar.Abono_SuMoneda"))
        txtSaldoActGen.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.Saldo_Actual"))
        txtAbonoGen.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.Monto"))
        txtSaldoAntGen.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.Saldo_Cuenta"))

        CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsRecibos1, "abonoccobrar.Anula"))
        txtNum_Recibo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Num_Recibo"))
        ComboMoneda.DataSource = Me.DsRecibos1
        ComboMoneda.DisplayMember = "Moneda.MonedaNombre"
        GridControl2.DataMember = "abonoccobrar.abonoccobrardetalle_abonoccobrar"
        GridControl2.DataSource = Me.DsRecibos1
        ComboMoneda.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DsRecibos1, "abonoccobrar.Cod_Moneda"))
    End Function

#Region "Validacion Usuario"
    Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown

        Dim cConexion As New Conexion
        Dim rs As SqlDataReader
        If e.KeyCode = Keys.Enter Then
            If txtUsuario.Text <> "" Then
                rs = cConexion.GetRecorset(cConexion.Conectar, "SELECT Cedula, Nombre from Usuarios where Clave_Interna ='" & txtUsuario.Text & "'")
                If rs.HasRows = False Then
                    MsgBox("Clave Incorrecta....", MsgBoxStyle.Information, "Atenci�n...")
                    txtUsuario.Focus()
                End If
                While rs.Read
                    Try
                       txtNombreUsuario.Text = rs("Nombre")
                        txtCedulaUsuario.Text = rs("Cedula")
                        Cedula_Usuario = rs("Cedula")
                        Nombre_Usuario = rs("Nombre")
                        PMU = VSM(rs("Cedula"), Me.Name) 'Carga los privilegios del usuario con el modulo 
                        If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo " & Me.Text, MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                        Check_Dig_Recibo.Enabled = True
                        Anular = True
                        VariarInteres = True
                        txtUsuario.Enabled = False ' se inabilita el campo de la contrase�a
                        Me.ToolBar1.Buttons(0).Enabled = True
                        Me.ToolBar1.Buttons(1).Enabled = True
                        Me.ToolBar1.Buttons(6).Enabled = True
                        Me.ToolBar1.Buttons(2).Enabled = False
                        Me.NuevoRecibo()
                        ComboMoneda.Focus()
                    Catch ex As SystemException
                        MsgBox(ex.Message)
                    End Try
                End While
                rs.Close()
                cConexion.DesConectar(cConexion.Conectar)
            Else
                MsgBox("Debe de digitar la clave de usuario", MsgBoxStyle.Exclamation)
                txtUsuario.Focus()
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                txtNombreUsuario.Text = User_Log.Nombre
                txtCedulaUsuario.Text = User_Log.Cedula
                Cedula_Usuario = User_Log.Cedula
                Nombre_Usuario = User_Log.Nombre
                Me.DsRecibos1.abonoccobrar.Ced_UsuarioColumn.DefaultValue = Cedula_Usuario
                PMU = VSM(User_Log.Cedula, Me.Name) 'Carga los privilegios del usuario con el modulo 
                If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo " & Me.Text, MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Check_Dig_Recibo.Enabled = True
                Anular = True
                VariarInteres = True
                txtUsuario.Enabled = False ' se inabilita el campo de la contrase�a
                Me.ToolBar1.Buttons(0).Enabled = True
                Me.ToolBar1.Buttons(1).Enabled = True
                Me.ToolBar1.Buttons(2).Enabled = False

            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Private Function Numero_de_Recibo() As Double
        Dim cConexion As New Conexion
        Dim Num_Recibo As Double
        Num_Recibo = CDbl(cConexion.SlqExecuteScalar(cConexion.Conectar("Hotel"), "SELECT ISNULL(MAX(Num_Recibo), 0) AS Expr1 FROM abonoccobrar"))
        Numero_de_Recibo = Num_Recibo + 1
        cConexion.DesConectar(cConexion.Conectar)
    End Function

    Private Sub NuevoRecibo()
        id_Aju = 0
        If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then 'no hay un registro pendiente
            Me.btModFecha.Visible = True
            Me.ToolBar1.Buttons(0).Text = "Cancelar"
            Me.ToolBar1.Buttons(0).ImageIndex = 8
            Me.ToolBar1.Buttons(2).Enabled = True
            Me.ToolBar1.Buttons(1).Enabled = False
            If Me.txtUsuario.Text = "" Then
                txtUsuario.Enabled = True
                If gloNoClave Then
                    Loggin_Usuario()
                Else
                    txtUsuario.Focus()
                End If
            End If
            txtCodigo.Text = "" : txtNombre.Text = ""
            Try
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarAnular.Enabled = False
                Me.ComboMoneda.Enabled = True
                Me.BindingContext(Me.DsRecibos1, "abonoccobrar").EndCurrentEdit()
                Me.BindingContext(Me.DsRecibos1, "abonoccobrar").AddNew()
                txtNum_Recibo.Text = Numero_de_Recibo()
                Me.ComboMoneda.Focus()
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        Else
            Try
                If MessageBox.Show("Desea Guardar los Cambios del Recibo de Dinero", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                    Me.Registrar()
                Else
                    Me.BindingContext(Me.DsRecibos1, "abonoccobrar").CancelCurrentEdit()
                    Me.ToolBar1.Buttons(0).Text = "Nuevo"
                    Me.ToolBar1.Buttons(0).ImageIndex = 0
                    Me.ToolBar1.Buttons(1).Enabled = True
                    Me.ToolBar1.Buttons(2).Enabled = False
                    Me.ToolBarAnular.Enabled = False
                    Me.ToolBarRegistrar.Enabled = False
                    Me.txtUsuario.Text = ""
                    Me.txtNombreUsuario.Text = ""
                    Me.txtCedulaUsuario.Text = ""
                    Me.GroupBox6.Enabled = False
                    Me.txtObservaciones.Enabled = False
                    txtIntereses.Enabled = False
                    txtAbono.Enabled = False
                    Me.ComboMoneda.Enabled = False
                    Me.DsRecibos1.detalle_abonoccobrar.Clear()
                    Me.DsRecibos1.abonoccobrar.Clear()
                    Me.Tabla.Clear()
                    If gloNoClave Then
                        Loggin_Usuario()
                    End If
                End If
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub txtUsuario_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUsuario.GotFocus
        txtUsuario.SelectAll()
    End Sub

    Function Registrar()
        Dim i As Integer
        Dim Funciones As New Conexion
        Dim FactTemp As Double
        Try
            If Me.BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Count = 0 Then
                MsgBox("Debe abonar m�nimo una factura", MsgBoxStyle.Information)
                Exit Function
            End If
            If MessageBox.Show("�Desea guardar el recibo de dinero?", "Hotel", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                If Me.Check_Dig_Recibo.Checked = False Then txtNum_Recibo.Text = Numero_de_Recibo()
                If Funciones.SlqExecuteScalar(Funciones.Conectar("Hotel"), "SELECT Num_Recibo FROM abonoccobrar WHERE (Num_Recibo =" & CDbl(txtNum_Recibo.Text) & ")") <> Nothing Then
                    MsgBox("No se puede Registrar, ya existe un recibo con este n�mero de recibo", MsgBoxStyle.Critical)
                    Exit Function
                End If

                Me.BindingContext(DsRecibos1, "abonoccobrar").EndCurrentEdit()
                Me.BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").EndCurrentEdit()
                ''llamar al formulario de opciones de pago
                Dim pide As String = GetSetting("SeeSoft", "Hotel", "pideAperturaAbo")
                If pide.Equals("") Then
                    pide = "1"
                    SaveSetting("SeeSoft", "Hotel", "pideAperturaAbo", "1")
                End If
                Dim Movimiento_Pago_Abonos As New frmMovimientoCajaPagoAbono2
                Movimiento_Pago_Abonos.cedu = Me.Cedula_Usuario
                If Not pide.Equals("1") Then
                    Movimiento_Pago_Abonos.cambioFP = 0
                    Movimiento_Pago_Abonos.NApertura = 0
                Else
                    Movimiento_Pago_Abonos.cambioFP = 1
                    Movimiento_Pago_Abonos.NApertura = 0
                End If

                Movimiento_Pago_Abonos.nombre = Me.Nombre_Usuario
                Movimiento_Pago_Abonos.Factura = CDbl(txtNum_Recibo.Text)
                Movimiento_Pago_Abonos.fecha = "" & dtFecha.Text
                Movimiento_Pago_Abonos.Total = CDbl(txtAbonoGen.Text)
                Movimiento_Pago_Abonos.Tipo = "ABO"
                Movimiento_Pago_Abonos.TipoCambioRecibo = Me.txtTipoCambio.Text
                Movimiento_Pago_Abonos.codmod = ComboMoneda.SelectedValue
                Movimiento_Pago_Abonos.TipoCambioDolar = CDbl(Me.txtTipoCambio.Text)
                Movimiento_Pago_Abonos.ShowDialog()
                If Movimiento_Pago_Abonos.huboDeposito Then
                    documento = Movimiento_Pago_Abonos.DataSet_Opciones_Pago1.Detalle_pago_caja(0).Documento
                End If

                ' documento1 = Movimiento_Pago_Abonos.DataSet_Opciones_Pago1.OpcionesDePago(0).id
                If Movimiento_Pago_Abonos.Registra Then

                    For i = 0 To Me.BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Count - 1
                        Me.BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Position = i
                        If Me.BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo") = 0 Then
                            FactTemp = Me.BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Factura")
                            If (Funciones.UpdateRecords("Ventas", "FacturaCancelado = 1", "Num_Factura =" & FactTemp & "and Tipo = 'CRE'")) <> "" Then
                                MsgBox("Problemas al Registrar las facturas como canceladas, reintente hacer el abono", MsgBoxStyle.Critical)
                                Exit Function
                            End If
                        End If
                    Next i

                    If registrar_Abono() And Movimiento_Pago_Abonos.RegistrarOpcionesPago() Then

                        If (Not Me.TextBoxComision.Text.Equals("")) And (Not Me.TextBoxComision.Text.Equals("0")) Then

                            If Movimiento_Pago_Abonos.DataSet_Opciones_Pago1.Detalle_pago_caja.Count > 0 Then
                                Dim cuenta As String = Movimiento_Pago_Abonos.DataSet_Opciones_Pago1.Detalle_pago_caja(0).cuentaBancaria
                                doc = Movimiento_Pago_Abonos.DataSet_Opciones_Pago1.Detalle_pago_caja(0).Documento
                                Dim dt As New DataTable
                                cFunciones.Llenar_Tabla_Generico("SELECT ISNULL(MAX(Num_Ajuste), 0)+1 AS AJ FROM  AjusteBancario", dt, GetSetting("SeeSoft", "Bancos", "Conexion"))
                                Dim a As Integer
                                If dt.Rows.Count > 0 Then
                                    a = dt.Rows(0).Item(0)
                                End If
                                cFunciones.Llenar_Tabla_Generico("SELECT Id_CuentaBancaria, Codigo_banco, Cuenta " & _
                                    " FROM Cuentas_bancarias WHERE Cuenta = '" & cuenta & "'", dt, GetSetting("SeeSoft", "Bancos", "Conexion"))


                                Dim sql As String = "INSERT INTO AjusteBancario " & _
                                                       " (Num_Ajuste, Numero_Docum, Fecha, Monto, Concepto, Id_CuentaBancaria, Debito, Credito, Ced_Usuario, CodigoMoneda, TipoCambio)  " & _
                                                        " VALUES (" & a & ", " & Me.txtNum_Recibo.Text & ", '" & Me.dtFecha.Value.Date & "', " & Me.TextBoxComision.Text & ", 'COMISION BANCARIA DE DEP " & doc & "', " & dt.Rows(0).Item("Id_CuentaBancaria") & ", 1, 0, '" & User_Log.Cedula & "', " & Me.ComboMoneda.SelectedValue & ", " & Me.txtTipoCambio.Text & ") "

                                Dim cx As New Conexion
                                cx.Conectar("Bancos")
                                Dim resp As String = cx.SlqExecute(cx.sQlconexion, sql)
                                cFunciones.Llenar_Tabla_Generico("SELECT     ISNULL(MAX(Id_Ajuste), 0) AS AJ FROM  AjusteBancario", dt, GetSetting("SeeSoft", "Bancos", "Conexion"))
                                If dt.Rows.Count > 0 Then
                                    a = dt.Rows(0).Item(0)
                                End If
                                cFunciones.Llenar_Tabla_Generico("SELECT     SettingCuentaContable.IdComDep, CuentaContable.CuentaContable, CuentaContable.Descripcion FROM         SettingCuentaContable INNER JOIN  CuentaContable ON SettingCuentaContable.IdComDep = CuentaContable.id", dt, GetSetting("SeeSoft", "Contabilidad", "Conexion"))

                                sql = " INSERT INTO AjusteBancario_Detalle " & _
                              " (Id_Ajuste, Descripcion_Mov, CuentaContable, Monto, NombreCuenta)" & _
                                " VALUES     (" & a & ",'COMISION BANCARIA DE DEP " & doc & "', '" & dt.Rows(0).Item("CuentaContable") & "', " & Me.TextBoxComision.Text & ", '" & dt.Rows(0).Item("Descripcion") & "')"
                                resp &= cx.SlqExecute(cx.sQlconexion, sql)
                                If Not resp.Equals("") Then
                                    MsgBox("ERROR AL GUARDAR EL AJUSTE BANCARIO" & resp, MsgBoxStyle.OkOnly)
                                End If
                                cx.DesConectar(cx.sQlconexion)

                            End If
                        End If
                        Dim cConexion As New Conexion
                        Dim conta As Integer
                        conta = cConexion.SlqExecuteScalar(cConexion.Conectar("Hotel"), "SELECT Contabilidad FROM Configuraciones")
                        cConexion.DesConectar(cConexion.sQlconexion)
                        If conta = 1 Or conta = 2 Then
                            GuardaAsiento()
                        End If
                        Me.ToolBar1.Buttons(1).Enabled = True
                        Me.ToolBar1.Buttons(0).Text = "Nuevo"
                        Me.ToolBar1.Buttons(0).ImageIndex = 0
                        Me.ToolBarRegistrar.Enabled = False
                        MsgBox("Datos Guardados Satisfactoriamente", MsgBoxStyle.Information)
                        If MessageBox.Show("�Desea imprimir este Recibo de Dinero?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                            imprimir()
                        End If
                        Me.Tabla.Clear()
                        Me.DsRecibos1.detalle_abonoccobrar.Clear()
                        Me.DsRecibos1.abonoccobrar.Clear()


                    Else
                        MsgBox("Problemas al registrar el abono y/o pagos, intentelo de nuevo ", MsgBoxStyle.Critical)
                        Me.Trans.Rollback()
                        Movimiento_Pago_Abonos.Trans.Rollback()
                        Exit Function
                    End If

                End If
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Function


    Function registrar_Abono() As Boolean

        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Trans = Me.SqlConnection1.BeginTransaction
        Try
            Me.adAbonos.InsertCommand.Transaction = Trans
            Me.adAbonos.DeleteCommand.Transaction = Trans
            Me.adAbonos.UpdateCommand.Transaction = Trans


            Me.daDetalle_Abono.InsertCommand.Transaction = Trans
            Me.daDetalle_Abono.DeleteCommand.Transaction = Trans
            Me.daDetalle_Abono.UpdateCommand.Transaction = Trans

            Me.adAbonos.Update(Me.DsRecibos1, "abonoccobrar")
            Me.daDetalle_Abono.Update(Me.DsRecibos1, "detalle_abonoccobrar")

            Trans.Commit()
            Return True


        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            Me.ToolBar1.Buttons(2).Enabled = True
            Return False
        End Try

    End Function
    Private Sub BuscarRecibo()

        Dim Fx As New cFunciones
        Dim identificador As Double

        Try
            If Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Count > 0 Then
                If (MsgBox("Actualmente se est� realizando un Recibo de Dinero, si contin�a se perderan los datos del Recibo actual, �desea continuar?", MsgBoxStyle.YesNo)) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If

            Me.ToolBar1.Buttons(0).Text = "Nuevo"
            Me.ToolBar1.Buttons(0).ImageIndex = 0
            Me.DsRecibos1.detalle_abonoccobrar.Clear()
            Me.DsRecibos1.abonoccobrar.Clear()
            Me.ToolBarAnular.Enabled = False
            Me.ToolBarRegistrar.Enabled = False

            identificador = CDbl(Fx.Buscar_X_Descripcion_Fecha5C("SELECT abonoccobrar.Id_Recibo, abonoccobrar.Num_Recibo as Recibo , abonoccobrar.Nombre_Cliente AS Nombre_Cliente, abonoccobrar.Fecha,  abonoccobrar.Monto FROM abonoccobrar INNER JOIN  Moneda ON abonoccobrar.Cod_Moneda = Moneda.CodMoneda ORDER BY abonoccobrar.Fecha DESC", "Nombre_Cliente", "Fecha", "Buscar Recibo de Dinero"))

            buscando = True

            If identificador = 0.0 Then ' si se dio en el boton de cancelar
                Me.buscando = False
                Exit Sub
            End If
            id_Aju = 0
            Check_Dig_Recibo.Enabled = False
            Me.ComboMoneda.Enabled = False
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM abonoccobrar WHERE (Id_Recibo = " & identificador & ")", Me.DsRecibos1.abonoccobrar, GetSetting("SeeSoft", "Hotel", "Conexion"))
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM detalle_abonoccobrar WHERE (Id_Recibo = " & identificador & ")", Me.DsRecibos1.detalle_abonoccobrar, GetSetting("SeeSoft", "Hotel", "Conexion"))
            Dim dt_Ajus As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM AjusteBancario WHERE (Numero_Docum = " & Me.DsRecibos1.abonoccobrar(0).Num_Recibo & ") AND (Concepto LIKE 'COMISION BANCARIA DE DEP%')", dt_Ajus, GetSetting("SeeSoft", "Bancos", "Conexion"))
            If dt_Ajus.Rows.Count > 0 Then
                TextBoxComision.Text = dt_Ajus.Rows(0).Item("Monto")
                id_Aju = dt_Ajus.Rows(0).Item("Id_Ajuste")

            End If
            '   Me.LlenarVentas(identificador)

            ' si esta venta aun no ha sido anulada
            If Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Current("Anula") = False Then Me.ToolBar1.Buttons(3).Enabled = True
            Me.btModFecha.Visible = False
            Me.GridControl2.Enabled = False
            Me.ToolBar1.Buttons(4).Enabled = True
            Me.ToolBar1.Buttons(0).Enabled = True
            Me.ToolBarAnular.Enabled = True
            Me.ToolBarRegistrar.Enabled = False

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub defaultValuesAsientos()
        'VALORES POR DEFECTO PARA LA TABLA ASIENTOS
        DsRecibos1.AsientosContables.FechaColumn.DefaultValue = Now.Date
        DsRecibos1.AsientosContables.IdNumDocColumn.DefaultValue = 0
        DsRecibos1.AsientosContables.NumDocColumn.DefaultValue = "0"
        DsRecibos1.AsientosContables.BeneficiarioColumn.DefaultValue = ""
        DsRecibos1.AsientosContables.TipoDocColumn.DefaultValue = 25
        DsRecibos1.AsientosContables.AccionColumn.DefaultValue = "AUT"
        DsRecibos1.AsientosContables.AnuladoColumn.DefaultValue = 0
        DsRecibos1.AsientosContables.FechaEntradaColumn.DefaultValue = Now.Date
        DsRecibos1.AsientosContables.MayorizadoColumn.DefaultValue = 0
        DsRecibos1.AsientosContables.PeriodoColumn.DefaultValue = Now.Month & "/" & Now.Year
        DsRecibos1.AsientosContables.NumMayorizadoColumn.DefaultValue = 0
        DsRecibos1.AsientosContables.ModuloColumn.DefaultValue = "Abonos a Proveedor"
        DsRecibos1.AsientosContables.ObservacionesColumn.DefaultValue = ""
        DsRecibos1.AsientosContables.NombreUsuarioColumn.DefaultValue = ""
        DsRecibos1.AsientosContables.TotalDebeColumn.DefaultValue = 0
        DsRecibos1.AsientosContables.TotalHaberColumn.DefaultValue = 0
        DsRecibos1.AsientosContables.CodMonedaColumn.DefaultValue = 1
        DsRecibos1.AsientosContables.TipoCambioColumn.DefaultValue = 1
        DsRecibos1.AsientosContables.NumAsientoColumn.DefaultValue = 0

        'VALORES POR DEFECTO PARA LA TABLA DETALLES ASIENTOS
        DsRecibos1.DetallesAsientosContable.ID_DetalleColumn.AutoIncrement = True
        DsRecibos1.DetallesAsientosContable.ID_DetalleColumn.AutoIncrementStep = -1
        DsRecibos1.DetallesAsientosContable.ID_DetalleColumn.AutoIncrementSeed = -1
        DsRecibos1.DetallesAsientosContable.NumAsientoColumn.DefaultValue = ""
        DsRecibos1.DetallesAsientosContable.DescripcionAsientoColumn.DefaultValue = ""
        DsRecibos1.DetallesAsientosContable.CuentaColumn.DefaultValue = ""
        DsRecibos1.DetallesAsientosContable.NombreCuentaColumn.DefaultValue = ""
        DsRecibos1.DetallesAsientosContable.MontoColumn.DefaultValue = 0
        DsRecibos1.DetallesAsientosContable.DebeColumn.DefaultValue = 0
        DsRecibos1.DetallesAsientosContable.HaberColumn.DefaultValue = 0
        DsRecibos1.DetallesAsientosContable.TipocambioColumn.DefaultValue = 0
    End Sub

    Public Sub GuardaAsiento()
        Try
            Dim Fecha As DateTime = BindingContext(DsRecibos1, "abonoccobrar").Current("Fecha")
            Dim Fx As New cFunciones

            BindingContext(DsRecibos1, "AsientosContables").EndCurrentEdit()
            BindingContext(DsRecibos1, "AsientosContables").AddNew()
            BindingContext(DsRecibos1, "AsientosContables").Current("NumAsiento") = Fx.BuscaNumeroAsiento("CXC-" & Format(Fecha, "MM") & Format(Fecha, "yy") & "-")
            BindingContext(DsRecibos1, "AsientosContables").Current("Fecha") = Fecha.Date
            BindingContext(DsRecibos1, "AsientosContables").Current("IdNumDoc") = BindingContext(DsRecibos1, "abonoccobrar").Current("Id_Recibo")
            BindingContext(DsRecibos1, "AsientosContables").Current("NumDoc") = BindingContext(DsRecibos1, "abonoccobrar").Current("Num_Recibo")
            BindingContext(DsRecibos1, "AsientosContables").Current("Beneficiario") = txtNombre.Text
            BindingContext(DsRecibos1, "AsientosContables").Current("TipoDoc") = 1
            BindingContext(DsRecibos1, "AsientosContables").Current("Accion") = "AUT"
            BindingContext(DsRecibos1, "AsientosContables").Current("Anulado") = 0
            BindingContext(DsRecibos1, "AsientosContables").Current("FechaEntrada") = Now.Date
            BindingContext(DsRecibos1, "AsientosContables").Current("Mayorizado") = 0
            BindingContext(DsRecibos1, "AsientosContables").Current("Periodo") = Fx.BuscaPeriodo(Fecha)
            BindingContext(DsRecibos1, "AsientosContables").Current("NumMayorizado") = 0
            BindingContext(DsRecibos1, "AsientosContables").Current("Modulo") = "Recibo de Dinero"
            If documento = Nothing Then
                BindingContext(DsRecibos1, "AsientosContables").Current("Observaciones") = "Asiento de Abono de Cliente # " & BindingContext(DsRecibos1, "abonoccobrar").Current("Num_Recibo")

            Else
                BindingContext(DsRecibos1, "AsientosContables").Current("Observaciones") = "Asiento de Abono de Cliente # " & BindingContext(DsRecibos1, "abonoccobrar").Current("Num_Recibo") & " Deposito # " & documento

            End If
            BindingContext(DsRecibos1, "AsientosContables").Current("NombreUsuario") = txtNombreUsuario.Text
            BindingContext(DsRecibos1, "AsientosContables").Current("TotalDebe") = BindingContext(DsRecibos1, "abonoccobrar").Current("Monto") * txtTipoCambio.Text
            BindingContext(DsRecibos1, "AsientosContables").Current("TotalHaber") = BindingContext(DsRecibos1, "abonoccobrar").Current("Monto") * txtTipoCambio.Text
            BindingContext(DsRecibos1, "AsientosContables").Current("CodMoneda") = 1
            BindingContext(DsRecibos1, "AsientosContables").Current("TipoCambio") = txtTipoCambio.Text
            BindingContext(DsRecibos1, "AsientosContables").EndCurrentEdit()
            Dim monto1, monto2 As Double
            Dim tipoCambioF As Double
            Dim x As Integer = 0
            If Me.ComboMoneda.SelectedValue = 2 Then
                monto2 = BindingContext(DsRecibos1, "abonoccobrar").Current("Monto") * txtTipoCambio.Text
                For x = 0 To BindingContext(DsRecibos1, "detalle_abonoccobrar").Count - 1
                    monto1 += Me.DsRecibos1.detalle_abonoccobrar.Rows(x).Item("Abono") * Me.DsRecibos1.detalle_abonoccobrar.Rows(x).Item("TipoCambio") ' (BindingContext(DsRecibos1, "detalle_abonoccobrar").Current("Abono") * BindingContext(DsRecibos1, "detalle_abonoccobrar").Current("TipoCambio"))
                    ' BindingContext(DsRecibos1, "detalle_abonoccobrar").Position = x
                Next
                tipoCambioF = monto1 / BindingContext(DsRecibos1, "abonoccobrar").Current("Monto")
            Else
                monto2 = BindingContext(DsRecibos1, "abonoccobrar").Current("Monto")
                monto1 = BindingContext(DsRecibos1, "abonoccobrar").Current("Monto")
            End If

            Dim cuenta As String = ""
            Dim nombreC As String = ""
            Dim str As String
            'GUARDA DETALLES DEL PAGO DEL RECIBO
            Dim dt As New DataTable
            Dim dtOp As New DataTable
            Dim monto As Double = 0

            cFunciones.Llenar_Tabla_Generico("SELECT     OpcionesDePago.Documento, OpcionesDePago.TipoDocumento, OpcionesDePago.FormaPago, OpcionesDePago.Denominacion, " & _
                    " OpcionesDePago.CodMoneda, OpcionesDePago.TipoCambio, OpcionesDePago.Fecha, OpcionesDePago.MontoPago, " & _
                    " Detalle_pago_caja.ReferenciaTipo, Detalle_pago_caja.CuentaBancaria " & _
                    " FROM         OpcionesDePago LEFT OUTER JOIN " & _
                    " Detalle_pago_caja ON OpcionesDePago.id = Detalle_pago_caja.Id_ODP " & _
                    " WHERE (OpcionesDePago.Documento = " & BindingContext(DsRecibos1, "abonoccobrar").Current("Num_Recibo") & ") AND (OpcionesDePago.TipoDocumento = 'ABO')", dtOp)

            For i As Integer = 0 To dtOp.Rows.Count - 1

                If dtOp.Rows(i).Item("FormaPago").Equals("EFE") Then
                    'CUENTDE CX
                    str = "SELECT     dbo.CuentaContable.CuentaContable, dbo.CuentaContable.Descripcion" & _
                          " FROM      dbo.SettingCuentaContable INNER JOIN " & _
                          " dbo.CuentaContable ON dbo.SettingCuentaContable.IdCaja = dbo.CuentaContable.id "

                    cFunciones.Llenar_Tabla_Generico(str, dt, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
                    If dt.Rows.Count > 0 Then
                        cuenta = dt.Rows(0).Item("CuentaContable")
                        nombreC = dt.Rows(0).Item("Descripcion")
                    End If
                ElseIf dtOp.Rows(i).Item("FormaPago").Equals("TRA") Then

                    str = "SELECT CuentaContable, NombreCuentaContable FROM   Cuentas_bancarias WHERE Cuenta = '" & dtOp.Rows(i).Item("CuentaBancaria") & "' "

                    cFunciones.Llenar_Tabla_Generico(str, dt, GetSetting("SeeSoft", "Bancos", "Conexion"))

                    If dt.Rows.Count > 0 Then

                        cuenta = dt.Rows(0).Item("CuentaContable")
                        nombreC = dt.Rows(0).Item("NombreCuentaContable")

                    End If
                ElseIf dtOp.Rows(i).Item("FormaPago").Equals("TAR") Then

                    str = "SELECT     Id, CuentaCXC, NombreCXC FROM         TipoTarjeta WHERE     (Id = " & dtOp.Rows(i).Item("ReferenciaTipo") & ") "

                    cFunciones.Llenar_Tabla_Generico(str, dt, GetSetting("SeeSoft", "Hotel", "Conexion"))

                    If dt.Rows.Count > 0 Then

                        cuenta = dt.Rows(0).Item("CuentaCXC")
                        nombreC = dt.Rows(0).Item("NombreCXC")

                    End If
                ElseIf dtOp.Rows(i).Item("FormaPago").Equals("CHE") Then
                    str = "SELECT     dbo.CuentaContable.CuentaContable, dbo.CuentaContable.Descripcion" & _
                                              " FROM      dbo.SettingCuentaContable INNER JOIN " & _
                                              " dbo.CuentaContable ON dbo.SettingCuentaContable.IdCaja = dbo.CuentaContable.id "

                    cFunciones.Llenar_Tabla_Generico(str, dt, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
                    If dt.Rows.Count > 0 Then
                        cuenta = dt.Rows(0).Item("CuentaContable")
                        nombreC = dt.Rows(0).Item("Descripcion")
                    End If
                End If

                If BindingContext(DsRecibos1, "abonoccobrar").Current("Cod_Moneda") = 2 Then
                    If dtOp.Rows(i).Item("CodMoneda") = 1 Then
                        monto = dtOp.Rows(i).Item("MontoPago")
                    Else
                        monto = dtOp.Rows(i).Item("MontoPago") * CDbl(Me.txtTipoCambio.Text)
                    End If
                Else
                    If dtOp.Rows(i).Item("CodMoneda") = 2 Then
                        monto = dtOp.Rows(i).Item("MontoPago") * Fx.TipoCambio(BindingContext(DsRecibos1, "abonoccobrar").Current("Fecha"))
                    Else
                        monto = dtOp.Rows(i).Item("MontoPago")
                    End If
                End If
                If Me.ComboMoneda.SelectedValue = 1 Then
                    GuardaAsientoDetalle(monto, True, False, cuenta, nombreC, Fx.TipoCambio(BindingContext(DsRecibos1, "abonoccobrar").Current("Fecha")))
                Else
                    GuardaAsientoDetalle(monto, True, False, cuenta, nombreC, Me.txtTipoCambio.Text)
                End If

            Next

            'CUENTA DE CX
            str = "SELECT     CuentaContable.CuentaContable, CuentaContable.Descripcion " & _
                                " FROM         SettingCuentaContable INNER JOIN " & _
                                " CuentaContable ON SettingCuentaContable.IdCuentaCobrar = CuentaContable.id"

            cFunciones.Llenar_Tabla_Generico(str, dt, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
            If dt.Rows.Count > 0 Then
                cuenta = dt.Rows(0).Item("CuentaContable")
                nombreC = dt.Rows(0).Item("Descripcion")
            End If
            If Me.ComboMoneda.SelectedValue = 1 Then
                GuardaAsientoDetalle(monto1, False, True, cuenta, nombreC, Fx.TipoCambio(BindingContext(DsRecibos1, "abonoccobrar").Current("Fecha")))
            Else
                GuardaAsientoDetalle(monto1, False, True, cuenta, nombreC, tipoCambioF)

            End If

            If monto2 > monto1 Then
                monto = monto2 - monto1
                GuardaAsientoDetalle(monto, False, True, BuscaCuenta("CuentaContable", "IdDiferencial"), BuscaCuenta("Descripcion", "IdDiferencial"), 0)

            ElseIf monto1 > monto2 Then
                monto = monto1 - monto2
                GuardaAsientoDetalle(monto, True, False, BuscaCuenta("CuentaContable", "IdDiferencial"), BuscaCuenta("Descripcion", "IdDiferencial"), 0)

            End If
            Me.registrarAsiento()
            'ACTUALIZA EL NUMERO DE ASIENTO AL DEPOSITO
            Dim Funciones As New Conexion
            Dim conta As Integer
            conta = CConexion.SlqExecuteScalar(CConexion.Conectar("Hotel"), "SELECT Contabilidad FROM Configuraciones")
            'CConexion.DesConectar(CConexion.sQlconexion)
            ''If conta = 1 Or conta = 2 Then
            ''    If doc = Nothing Then
            ''        doc = 0
            ''    End If
            'If documento1 = Nothing Then
            '    documento1 = 0
            'End If
            'Funciones.UpdateRecords("Deposito", "Contabilizado = 1, Asiento = '" & BindingContext(DsRecibos1, "AsientosContables").Current("NumAsiento") & "'", "Id_Deposito = " & documento1, "Bancos")

            'End If
        Catch ex As Exception
            MsgBox("Problemas en creaci�n del asiento: " & ex.ToString)

        End Try

    End Sub
    Function BuscaCuenta(ByVal Tipo As String, ByVal Id As String) As String
        Dim cConexion As New Conexion
        Try
            cConexion.DesConectar(cConexion.sQlconexion)
            BuscaCuenta = cConexion.SlqExecuteScalar(cConexion.Conectar("Contabilidad"), "SELECT TOP 1 (SELECT " & Tipo & " FROM cuentacontable " & _
                            "WHERE (Id = (SELECT " & Id & " FROM settingcuentacontable))) AS Cuenta FROM CuentaContable")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        Finally
            cConexion.DesConectar(cConexion.sQlconexion)
        End Try
    End Function
    Sub registrarAsiento()
        Try

            If Me.SqlConnectionContabilidad.State = ConnectionState.Closed Then
                Me.SqlConnectionContabilidad.Open()

            End If
            Dim trans As SqlTransaction
            trans = Me.SqlConnectionContabilidad.BeginTransaction
            Me.AdAsientos.UpdateCommand.Transaction = trans
            Me.AdAsientos.InsertCommand.Transaction = trans
            Me.AdAsientos.DeleteCommand.Transaction = trans
            Me.adDetallesAsientos.UpdateCommand.Transaction = trans
            Me.adDetallesAsientos.InsertCommand.Transaction = trans
            Me.adDetallesAsientos.DeleteCommand.Transaction = trans
            AdAsientos.Update(Me.DsRecibos1, "AsientosContables")
            adDetallesAsientos.Update(Me.DsRecibos1, "DetallesAsientosContable")
            trans.Commit()
            Dim cx As New Conexion
            cx.Conectar("HOTEL")
            cx.SlqExecute(cx.sQlconexion, "UPDATE    abonoccobrar SET   Asiento = '" & BindingContext(DsRecibos1, "AsientosContables").Current("NumAsiento") & "', Contabilizado = 1 WHERE     (Num_Recibo = " & BindingContext(DsRecibos1, "AsientosContables").Current("NumDoc") & ")")
            cx.DesConectar(cx.sQlconexion)
        Catch ex As Exception
            trans.Rollback()
            MsgBox("Problemas en transaccion del asiento: " & ex.ToString)
        End Try
    End Sub
    Public Sub GuardaAsientoDetalle(ByVal Monto As Double, ByVal Debe As Boolean, ByVal Haber As Boolean, ByVal Cuenta As String, ByVal NombreCuenta As String, ByVal tipocambio As Double)
        If Monto > 0 Then
            Dim Fx As New cFunciones
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").AddNew()
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("NumAsiento") = BindingContext(DsRecibos1, "AsientosContables").Current("NumAsiento")
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("DescripcionAsiento") = BindingContext(DsRecibos1, "AsientosContables").Current("Observaciones")
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Cuenta") = Cuenta
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("NombreCuenta") = NombreCuenta
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Monto") = Monto
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Debe") = Debe
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Haber") = Haber
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Tipocambio") = tipocambio
            BindingContext(DsRecibos1, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
        End If
    End Sub
    Function LlenarVentas(ByVal Id As Double)
        Dim cnnv As SqlConnection = Nothing
        Dim dt As New DataTable
        Dim cConexion As New Conexion
        Dim IdRec As Long
        'Dentro de un Try/Catch por si se produce un error
        Try
            IdRec = CInt(cConexion.SlqExecuteScalar(cConexion.Conectar, "Select Id_Recibo from abonoccobrar where Id_Recibo =" & Id))
            cConexion.DesConectar(cConexion.Conectar)
            '''''''''LLENAR VENTAS''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Obtenemos la cadena de conexi�n adecuada
            Dim sConn As String = GetSetting("Hotel", "Hotel", "CONEXION")
            cnnv = New SqlConnection(sConn)
            cnnv.Open()
            'Creamos el comando para la consulta
            Dim cmdv As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM abonoccobrar WHERE (Id_Recibo = @Id_Factura)"
            cmdv.CommandText = sel
            cmdv.Connection = cnnv
            cmdv.CommandType = CommandType.Text
            cmdv.CommandTimeout = 90
            'Los par�metros usados en la cadena de la consulta 
            cmdv.Parameters.Add(New SqlParameter("@Id_Factura", SqlDbType.BigInt))
            cmdv.Parameters("@Id_Factura").Value = Id
            'Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim dv As New SqlDataAdapter
            dv.SelectCommand = cmdv
            ' Llenamos la tabla
            dv.Fill(Me.DsRecibos1, "abonoccobrar")
            '''''''''LLENAR VENTAS DETALLES''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Obtenemos la cadena de conexi�n adecuada
            'Dim sConn As String = GetSetting("SeePos", "SeePos", "CONEXION")
            'cnn = New SqlConnection(sConn)
            'cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            sel = "SELECT * FROM detalle_abonoccobrar WHERE (Id_Recibo = @Id_Factura) "
            cmd.CommandText = sel
            cmd.Connection = cnnv
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmd.Parameters.Add(New SqlParameter("@Id_Factura", SqlDbType.BigInt))
            cmd.Parameters("@Id_Factura").Value = IdRec
            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            ' Llenamos la tabla
            da.Fill(Me.DsRecibos1.detalle_abonoccobrar)
        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            If Not cnnv Is Nothing Then
                cnnv.Close()
            End If
        End Try
    End Function

    Function Registrar_Anulacion_Venta(ByVal documento As Int64) As Boolean
        Dim i As Long
        Dim Facttem As Double
        Dim cx As New Conexion
        Dim Funciones As New Conexion
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            Me.SqlUpdateCommand1.Transaction = Trans
            Me.adAbonos.Update(Me.DsRecibos1, "abonoccobrar")
            For i = 0 To Me.BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Count - 1
                Me.BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Position = i
                Facttem = Me.BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Factura")
                Funciones.UpdateRecords("Ventas", "FacturaCancelado = 0", "Num_Factura =" & Facttem & "and Tipo = 'CRE'")
            Next i
            Funciones.UpdateRecords("OpcionesDePago", "FormaPago = 'ANU'", "(Documento = " & txtNum_Recibo.Text & ") AND (TipoDocumento = 'ABO')")
            Trans.Commit()
            'VALIDA ASIENTO SI TIENE Y ANULA
            If Not Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Current("Asiento").Equals("0") Then
                '   Dim cx As New Conexion
                cx.Conectar("Contabilidad")
                cx.SlqExecute(cx.sQlconexion, "UPDATE AsientosContables Set Anulado = 1 WHERE NumAsiento = '" & Me.BindingContext(DsRecibos1, "abonoccobrar").Current("Asiento") & "'")
                cx.DesConectar(cx.sQlconexion)
            End If
            '---------------------------------------
            If id_Aju > 0 Then
                '  Dim cx As New Conexion
                cx.Conectar("Bancos")
                cx.SlqExecute(cx.sQlconexion, "UPDATE AjusteBancario  Set Anula = 1 WHERE Id_Ajuste = " & id_Aju)
                cx.DesConectar(cx.sQlconexion)
            End If
            If documento > 0 Then
                cx.Conectar("Bancos")
                cx.SlqExecute(cx.sQlconexion, "Delete from Deposito  WHERE id_Deposito = " & documento)
                cx.DesConectar(cx.sQlconexion)
            End If


            Return True
        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            Me.ToolBar1.Buttons(3).Enabled = True
            Return False
        End Try
    End Function

    Function AnularRecibo()
        Try
            Dim resp As Integer
            If Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Current("Anula") = True Then
                MsgBox("El Abono # " & Me.BindingContext(DsRecibos1, "abonoccobrar").Current("num_recibo") & " ya se encuentra anulado, NO se puede anular", MsgBoxStyle.OKOnly)
                Exit Function
            End If
            Dim fec As DateTime = Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Current("Fecha")
            fec = fec.Date
            If fec <> Now.Date Then
                MsgBox("El Abono # " & Me.BindingContext(DsRecibos1, "abonoccobrar").Current("num_recibo") & " no se puede anular", MsgBoxStyle.OKOnly)
                Exit Function
            End If
            If Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Count > 0 Then
                If Me.BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Count > 0 Then
                    'VALIDA ASIENTO SI TIENE
                    If Not Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Current("Asiento").Equals("0") Then
                        Dim dt As New DataTable
                        cFunciones.Llenar_Tabla_Generico("Select Mayorizado From AsientosContables WHERE NumAsiento = '" & Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Current("Asiento") & "'", dt, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0).Item(0) Then
                                MsgBox("El asiento # " & Me.BindingContext(DsRecibos1, "abonoccobrar").Current("Asiento") & " que corresponde a este ajuste ya esta mayorizado, NO se puede anular", MsgBoxStyle.OKOnly)
                                Exit Function
                            End If
                        End If
                    End If
                    If Me.id_Aju = 0 Then
                        Dim dt As New DataTable
                        cFunciones.Llenar_Tabla_Generico("Select Conciliacion From AjusteBancario  WHERE Id_Ajuste = " & Me.id_Aju & "", dt, GetSetting("SeeSoft", "Bancos", "Conexion"))
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0).Item(0) Then
                                MsgBox("El ajuste bancario por comision esta conciliado en bancos  no se puede anular ", MsgBoxStyle.OKOnly)
                                Exit Function
                            End If
                        End If

                    End If

                    Dim documento, cuenta As String
                    Dim id As Int64
                    Dim id_cuenta As Integer = 0
                    Dim tipo As String
                    Dim dt1 As New DataTable
                    Dim str As String = ""
                    Dim id_deposito As Int64 = 0

                    str = "select id from OpcionesDePago where Documento = " & txtNum_Recibo.Text & " and TipoDocumento = 'ABO'" & " and FormaPago = 'TRA'"
                    cFunciones.Llenar_Tabla_Generico(str, dt1, GetSetting("SeeSoft", "Hotel", "Conexion"))
                    If dt1.Rows.Count > 0 Then
                        id = dt1.Rows(0).Item("id")
                        dt1 = Nothing
                        dt1 = New DataTable
                        str = "select documento,CuentaBancaria from Detalle_pago_caja where Id_ODP = " & id
                        cFunciones.Llenar_Tabla_Generico(str, dt1, GetSetting("SeeSoft", "Hotel", "Conexion"))
                        documento = dt1.Rows(0).Item("documento")
                        cuenta = dt1.Rows(0).Item("CuentaBancaria")

                        dt1 = Nothing
                        dt1 = New DataTable
                        str = "select Id_CuentaBancaria from Cuentas_bancarias where Cuenta = '" & cuenta & "'"
                        cFunciones.Llenar_Tabla_Generico(str, dt1, GetSetting("SeeSoft", "Bancos", "Conexion"))
                        id_cuenta = dt1.Rows(0).Item("Id_CuentaBancaria")

                        dt1 = Nothing
                        dt1 = New DataTable
                        str = "select id_Deposito,Conciliado from Deposito WHERE NumeroDocumento = " & documento & " and Id_CuentaBancaria = " & id_cuenta
                        cFunciones.Llenar_Tabla_Generico(str, dt1, GetSetting("SeeSoft", "Bancos", "Conexion"))
                        If dt1.Rows(0).Item("Conciliado") = True Then
                            MsgBox("El Deposito conciliado en bancos  no se puede anular ", MsgBoxStyle.OKOnly)
                            Exit Function
                        End If
                        id_deposito = dt1.Rows(0).Item("id_deposito")
                    End If
                    '---------------------------------------
                    resp = MessageBox.Show("�Desea Anular este Recibo de Dinero?", "Hotel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                    If resp = 6 Then
                        BindingContext(Me.DsRecibos1, "abonoccobrar").Current("Anula") = True
                        BindingContext(Me.DsRecibos1, "abonoccobrar").EndCurrentEdit()

                        If Registrar_Anulacion_Venta(id_deposito) Then

                            Me.DsRecibos1.AcceptChanges()
                            MsgBox("El Recibo de Dinero ha sido anulado correctamente", MsgBoxStyle.Information)
                            Me.DsRecibos1.detalle_abonoccobrar.Clear()
                            Me.DsRecibos1.abonoccobrar.Clear()
                            Me.ToolBar1.Buttons(4).Enabled = True
                            Me.ToolBar1.Buttons(1).Enabled = True

                            Me.ToolBar1.Buttons(0).Text = "Nuevo"
                            Me.ToolBar1.Buttons(0).ImageIndex = 0
                            Me.ToolBar1.Buttons(3).Enabled = False
                            Me.ToolBar1.Buttons(2).Enabled = False
                            Me.ToolBar1.Buttons(4).Enabled = False
                            Me.ToolBarAnular.Enabled = False
                            Me.ToolBarRegistrar.Enabled = False


                            Me.GroupBox6.Enabled = False

                            Me.txtUsuario.Enabled = True
                            Me.txtUsuario.Text = ""
                            Me.txtNombreUsuario.Text = ""
                            If gloNoClave Then
                                Loggin_Usuario()
                            Else
                                Me.txtUsuario.Focus()
                            End If
                        End If

                    Else : Exit Function

                    End If
                End If
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : NuevoRecibo()

            Case 2 : If PMU.Find Then BuscarRecibo() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 3 : If PMU.Update Then Registrar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 4 : If PMU.Delete Then AnularRecibo() Else MsgBox("No tiene permiso para eliminar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 5 : If PMU.Print Then imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 6 : Me.Close()

            Case 7 : Tramite()

        End Select
    End Sub

    Private Sub Tramite()
        Dim Fx As New cFunciones
        Dim identificador As Double
        Dim i As Integer
        'Limpio los dataset
        DsRecibos1.detalle_abonoccobrar.Clear()
        DsRecibos1.abonoccobrar.Clear()
        DsRecibos1.TramiteDetalle.Clear()
        DsRecibos1.Tramite.Clear()
        'Buscar el n�mero de tramite
        identificador = CDbl(Fx.Buscar_X_Descripcion_Fecha5C("SELECT Tramite.Id, Tramite.Id as Tramite , Tramite.NombreCliente AS Nombre_Cliente, Tramite.Fecha,  Tramite.Total FROM Tramite ORDER BY Tramite.Fecha DESC", "Nombre_Cliente", "Fecha", "Buscar Tr�mite de Facturas"))
        buscando = True
        If identificador = 0.0 Then ' si se dio en el boton de cancelar
            Me.buscando = False
            Exit Sub
        End If
        cFunciones.Llenar_Tabla_Generico("SELECT * FROM Tramite WHERE (Id = " & identificador & ")", DsRecibos1.Tramite)
        cFunciones.Llenar_Tabla_Generico("SELECT * FROM TramiteDetalle WHERE (Id_Tramite = " & identificador & ")", DsRecibos1.TramiteDetalle)

        If BindingContext(DsRecibos1, "Tramite").Current("Pagado") = True Then
            MsgBox("Este tr�mite de factura ya gener� el recibo de dinero No. " & Me.BindingContext(Me.DsRecibos1, "Tramite").Current("NumRecibo"), MsgBoxStyle.Information, "Atenci�n ...")
            Exit Sub
        End If

        BindingContext(DsRecibos1, "Abonoccobrar").EndCurrentEdit()
        BindingContext(DsRecibos1, "Abonoccobrar").AddNew()
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Num_Recibo") = Numero_de_Recibo()
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Cod_Cliente") = Me.BindingContext(Me.DsRecibos1, "Tramite").Current("CodCliente")
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Nombre_Cliente") = Me.BindingContext(Me.DsRecibos1, "Tramite").Current("NombreCliente")
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Cod_Moneda") = 2
        ComboMoneda.Text = "DOLAR"
        CargarInformacionCliente(BindingContext(DsRecibos1, "Abonoccobrar").Current("Cod_Cliente"))
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Monto") = Me.BindingContext(Me.DsRecibos1, "Tramite").Current("Total")
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Saldo_Actual") = Me.txtSaldoAntGen.Text - Me.BindingContext(Me.DsRecibos1, "Tramite").Current("Total")
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Fecha") = Now.Date
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Observaciones") = ""
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Anula") = 0
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Ced_Usuario") = Cedula_Usuario
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Contabilizado") = 0
        BindingContext(DsRecibos1, "Abonoccobrar").Current("Asiento") = 0
        Tabla.Clear()

        BindingContext(DsRecibos1, "Abonoccobrar").EndCurrentEdit()

        For i = 0 To Me.BindingContext(Me.DsRecibos1, "Tramite.TramiteTramiteDetalle").Count - 1
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").EndCurrentEdit()
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").AddNew()
            Me.BindingContext(Me.DsRecibos1, "Tramite.TramiteTramiteDetalle").Position = i
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Factura") = Me.BindingContext(Me.DsRecibos1, "Tramite.TramiteTramiteDetalle").Current("Num_Factura")
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Tipo") = "CRE"
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Monto") = Me.BindingContext(Me.DsRecibos1, "Tramite.TramiteTramiteDetalle").Current("MontoFactura")
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo_Ant") = Me.BindingContext(Me.DsRecibos1, "Tramite.TramiteTramiteDetalle").Current("Saldo")
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Intereses") = 0
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Abono") = Me.BindingContext(Me.DsRecibos1, "Tramite.TramiteTramiteDetalle").Current("Saldo")
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Abono_SuMoneda") = Me.BindingContext(Me.DsRecibos1, "Tramite.TramiteTramiteDetalle").Current("Saldo")
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo") = 0
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Id_Factura") = Me.BindingContext(Me.DsRecibos1, "Tramite.TramiteTramiteDetalle").Current("Id_Factura")
            BindingContext(DsRecibos1, "Abonoccobrar.abonoccobrardetalle_abonoccobrar").EndCurrentEdit()
        Next
        Me.ComboMoneda.Enabled = False
    End Sub

    Function imprimir()
        Try
            Dim Recibo_reporte As New ReciboDinero
            Dim visor As New frmVisorReportes
            Recibo_reporte.SetParameterValue(0, CDbl(Me.Label_Id_Recibo.Text))
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, Recibo_reporte, False, GetSetting("SeeSoft", "Hotel", "Conexion"))
            visor.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function


    Private Sub ComboMoneda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboMoneda.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboMoneda.Enabled = False
            Me.GroupBox6.Enabled = True
            txtCodigo.Focus()
        End If
    End Sub

#Region "Clientes"
    Private Sub txtCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim cFunciones As New cFunciones
            Me.txtCodigo.Text = cFunciones.BuscarDatos("select id ,Nombre  from Cliente", "Nombre")
            CargarInformacionCliente(txtCodigo.Text)
        End If
        If e.KeyCode = Keys.Enter Then
            CargarInformacionCliente(txtCodigo.Text)
        End If
    End Sub

    Private Sub CargarInformacionCliente(ByVal codigo As String)
        Dim cConexion As New Conexion
        Dim funciones As New cFunciones
        Dim rs As SqlDataReader
        Dim i As Integer
        Dim fila As DataRow
        Dim factura As Long
        If codigo <> Nothing Then
            rs = cConexion.GetRecorset(cConexion.Conectar, "SELECT Id, Nombre,Plazo_Credito from Cliente where Id ='" & codigo & "'")
            Try
                If rs.Read Then
                    txtCodigo.Text = rs("Id")
                    txtNombre.Text = rs("Nombre")
                    Tabla = funciones.BuscarFacturas(codigo)
                    gridFacturas.DataSource = Tabla
                    Saldo_Cuenta(Tabla)
                    Me.dias = rs("Plazo_Credito")
                    If Tabla.Rows.Count = 0 Then
                        MessageBox.Show("El cliente no tiene facturas pendientes...", "Atenci�n...", MessageBoxButtons.OK)
                        txtCodigo.Focus()
                        rs.Close()
                        Exit Sub
                    Else
                        txtObservaciones.Enabled = True
                        txtObservaciones.Focus()
                        If VariarInteres = True Then
                            txtIntereses.Enabled = True
                        Else
                            txtIntereses.Enabled = False
                        End If
                        txtAbono.Enabled = True
                    End If
                Else
                    MsgBox("La identificaci�n del Cliente no se encuentra", MsgBoxStyle.Information, "Atenci�n...")
                    txtCodigo.Focus()
                    rs.Close()
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
            rs.Close()
            cConexion.DesConectar(cConexion.Conectar)
        End If
    End Sub

    Function Saldo_Cuenta(ByVal Tabla1 As DataTable)
        Dim Id_Factura As Long
        Dim i As Integer
        Dim fila As DataRow
        Dim Totaltemp As Double
        Dim SaldoCuenta As Double
        Dim CodigoMoneda As Integer
        Dim funciones As New cFunciones
        Dim ConexionLocal As New Conexion
        Dim rs As SqlDataReader
        Dim ValorFactura As Double
        SaldoCuenta = 0
        Try
            For i = 0 To Tabla1.Rows.Count - 1
                fila = Tabla1.Rows(i)
                Id_Factura = fila("id")
                Totaltemp = fila("Total")
                CodigoMoneda = fila("Cod_Moneda")
                ValorFactura = Tabla1.Rows(i).Item("Tipo_Cambio")
                ConexionLocal.DesConectar(ConexionLocal.sQlconexion)
                TipoCambio = CDbl(txtTipoCambio.Text)

                If ComboMoneda.SelectedValue = 1 Then
                    If CodigoMoneda = 1 Then
                        SaldoCuenta = SaldoCuenta + funciones.Saldo_de_Factura(Id_Factura, Totaltemp)
                    Else
                        SaldoCuenta = SaldoCuenta + (funciones.Saldo_de_Factura(Id_Factura, Totaltemp) * CDbl(txtTipoCambio.Text))
                    End If
                End If

                If ComboMoneda.SelectedValue = 2 Then
                    If CodigoMoneda = 1 Then
                        SaldoCuenta = SaldoCuenta + (funciones.Saldo_de_Factura(Id_Factura, Totaltemp) / CDbl(txtTipoCambio.Text))
                    Else
                        SaldoCuenta = SaldoCuenta + funciones.Saldo_de_Factura(Id_Factura, Totaltemp)
                    End If
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        ConexionLocal = Nothing
        txtSaldoAntGen.Text = Format(SaldoCuenta, "#,#0.00")
    End Function
#End Region

    Private Sub informacionfactura(ByRef IDfactura As Double)
        Dim cConexion As New Conexion
        Dim funciones As New cFunciones
        Dim rs As SqlDataReader
        Dim Conexion2 As New Conexion
        Dim Interes As Double
        Dim DiasAtraso As Double
        Dim FechaUltAbono As String

        If IDfactura <> Nothing Then
            rs = cConexion.GetRecorset(cConexion.Conectar, "Select id,Num_Factura, Tipo, Fecha, Vence, Cod_Moneda, Total from Ventas where Tipo = 'CRE' and Num_Factura =" & IDfactura & " AND Cod_Cliente = " & Me.txtCodigo.Text & " AND Total > 0 ")
            While rs.Read
                Try
                    BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").CancelCurrentEdit()
                    BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").EndCurrentEdit()
                    BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").AddNew()
                Catch eEndEdit As System.Data.NoNullAllowedException
                    System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
                End Try
                Try

                    IDfactura = rs("id")
                    Codigo = rs("Cod_moneda")
                    TipoCambioFact = Conexion2.SlqExecuteScalar(Conexion2.Conectar, "Select TipoCambioDolar from Ventas Where Id =" & IDfactura)
                    Me.TxtCambioFactura.Text = Me.TipoCambioFact
                    Conexion2.DesConectar(Conexion2.Conectar)
                    Interes = Conexion2.SlqExecuteScalar(Conexion2.Conectar, "Select Intereses from configuraciones")
                    Conexion2.DesConectar(Conexion2.Conectar)

                    Dim vence As Date = rs("Vence")
                    DiasAtraso = DateDiff(DateInterval.Day, vence, Date.Now)
                    txtID_Factura.Text = rs("id")
                    txtFactura.Text = rs("Num_Factura")
                    dtEmitida.Text = rs("Fecha")

                    If ComboMoneda.SelectedValue = 1 Then
                        If Codigo = 1 Then
                            txtMonto.Text = Format(rs("Total"), "#,#0.00")
                            txtSaldo.Text = Format(funciones.Saldo_de_Factura(IDfactura, rs("Total")), "#,#0.00")
                        Else
                            txtMonto.Text = Format(Math.Round(rs("Total"), 2) * CDbl(txtTipoCambio.Text), "#,#0.00")
                            txtSaldo.Text = Format((funciones.Saldo_de_Factura(IDfactura, rs("Total"))) * CDbl(txtTipoCambio.Text), "#,#0.00")
                        End If
                    End If

                    If ComboMoneda.SelectedValue = 2 Then
                        If Codigo = 1 Then
                            txtMonto.Text = Format(Math.Round(rs("Total"), 2) / CDbl(txtTipoCambio.Text), "#,#0.00")
                            txtSaldo.Text = Format((funciones.Saldo_de_Factura(IDfactura, rs("Total"))) / CDbl(txtTipoCambio.Text), "#,#0.00")
                        Else
                            txtMonto.Text = Format(rs("Total"), "#,#0.00")
                            txtSaldo.Text = Format(funciones.Saldo_de_Factura(IDfactura, rs("Total")), "#,#0.00")
                        End If
                    End If

                    FechaUltAbono = Conexion2.SlqExecuteScalar(Conexion2.Conectar("Hotel"), "Select max(Fecha) from UltimoRecibo where Tipo = 'CRE' and Factura = " & txtFactura.Text)
                    Conexion2.DesConectar(Conexion2.Conectar("Hotel"))
                    If FechaUltAbono <> Nothing Then
                        If vence < FechaUltAbono Then
                            DiasAtraso = DateDiff(DateInterval.Day, CDate(FechaUltAbono), Date.Now)
                        End If
                    End If

                    If DiasAtraso > 0 Then
                        txtIntereses.Text = Format((DiasAtraso * (Interes / 100 / 30)) * txtSaldo.Text, "#,#0.00")
                    Else
                        txtIntereses.Text = "0.00"
                    End If
                    txtSaldoAnt.Text = Format(CDbl(txtSaldo.Text) + CDbl(txtIntereses.Text), "#,#0.00")
                    txtAbono.Text = Format(CDbl(txtSaldoAnt.Text), "#,#0.00")
                    txtSaldoAct.Text = "0.00"
                    If txtIntereses.Enabled = True Then
                        txtIntereses.Focus()
                    Else
                        txtAbono.Focus()
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End While
            Conexion2 = Nothing
            cConexion.DesConectar(cConexion.Conectar)
            cConexion = Nothing
        End If
    End Sub

    Private Sub gridFacturas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gridFacturas.Click
        'Try
        '    Dim hi As DevExpress.XtraGrid.Views.BandedGrid.ViewInfo.BandedGridHitInfo = _
        '           AdvBandedGridView1.CalcHitInfo(CType(gridFacturas, Control).PointToClient(Control.MousePosition))
        '    Dim data As DataRow
        '    Dim IDFactura As Double

        '    If hi.RowHandle >= 0 Then
        '        data = AdvBandedGridView1.GetDataRow(hi.RowHandle)
        '    ElseIf AdvBandedGridView1.FocusedRowHandle >= 0 Then
        '        data = AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle)
        '    Else
        '        data = Nothing
        '    End If
        '    IDFactura = data("Id") 'Tabla.Rows(AdvBandedGridView1.FocusedRowHandle).Item("id")
        '    Me.txtID_Factura.Text = IDFactura
        '    Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Current("Ced_Usuario") = Me.Cedula_Usuario
        '    Me.BindingContext(Me.DsRecibos1, "abonoccobrar").EndCurrentEdit()
        '    informacionfactura(IDFactura)
        'Catch ex As SyntaxErrorException
        '    MsgBox(ex.Message)
        'End Try
    End Sub

    Private Sub txtCodigo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigo.GotFocus
        txtCodigo.SelectAll()
    End Sub

    Private Sub txtCodigo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub txtObservaciones_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtObservaciones.KeyDown
        If e.KeyData = Keys.Enter Then
            txtFactura.Focus()
        End If
    End Sub

    Private Sub txtIntereses_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtIntereses.KeyDown
        Dim Temp As Double
        If e.KeyData = Keys.Enter Then
            Try
                Temp = CDbl(txtIntereses.Text)
                If txtIntereses.Text = "" Then txtIntereses.Text = "0.00"
                txtSaldoAnt.Text = Format(CDbl(txtSaldo.Text) + CDbl(txtIntereses.Text), "#,#0.00")
                txtAbono.Text = Format(CDbl(txtSaldoAnt.Text), "#,#0.00")
                txtAbono.Focus()
            Catch ex As Exception
                MessageBox.Show("No era un n�merico, favor volver a ingresar el monto", "SeePos", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtIntereses.Text = 0
                txtIntereses.Focus()
            End Try
        End If
    End Sub

    Private Sub txtIntereses_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIntereses.GotFocus
        txtIntereses.SelectAll()
    End Sub

    Private Sub txtAbono_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAbono.KeyDown
        Try
            If e.KeyData = Keys.Enter Then
                Dim int As Double
                Dim ab As Double
                If txtAbono.Text = "" Then txtAbono.Text = 0
                If txtAbono.Text = 0 Then
                    MessageBox.Show("Debe de digitar un monto mayor que 0", "Atenci�n...", MessageBoxButtons.OK)
                    txtAbono.Text = 0.0 : txtAbono.Focus() : txtAbono.SelectAll() : Exit Sub
                End If
                If CDbl(txtAbono.Text) > CDbl(txtSaldoAnt.Text) Then
                    MessageBox.Show("No puede abonarle m�s de lo que adeuda, Favor revisar...", "Atenci�n...", MessageBoxButtons.OK)
                    txtAbono.Text = 0.0 : txtAbono.Focus() : txtAbono.SelectAll() : Exit Sub
                Else
                    txtAbono.Text = CDbl(txtAbono.Text)
                    If CDbl(txtAbono.Text) < CDbl(txtIntereses.Text) Then
                        txtAbonoB.Text = 0
                        txtIntereses.Text = txtAbono.Text
                    Else
                        txtAbonoB.Text = CDbl(txtAbono.Text) - CDbl(txtIntereses.Text)
                        txtSaldoAct.Text = Format(CDbl(txtSaldoAnt.Text) - CDbl(txtAbono.Text), "#,#0.00")

                        If ComboMoneda.SelectedValue = 1 Then
                            If Codigo = 1 Then
                                txtAbonoSuMoneda.Text = CDbl(txtAbonoB.Text)
                            Else
                                txtAbonoSuMoneda.Text = CDbl(txtAbonoB.Text) / CDbl(txtTipoCambio.Text)
                            End If
                        End If

                        If ComboMoneda.SelectedValue = 2 Then
                            If Codigo = 1 Then
                                txtAbonoSuMoneda.Text = CDbl(txtAbonoB.Text) * CDbl(txtTipoCambio.Text)
                            Else
                                txtAbonoSuMoneda.Text = CDbl(txtAbonoB.Text)
                            End If
                        End If
                    End If
                    Me.BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").EndCurrentEdit()
                    int = Me.colIntereses.SummaryItem.SummaryValue
                    ab = Me.colAbono.SummaryItem.SummaryValue
                    Me.txtAbonoGen.Text = Format(ab + int, "#,#0.00")
                    txtSaldoActGen.Text = Format(txtSaldoAntGen.Text + Me.colIntereses.SummaryItem.SummaryValue - txtAbonoGen.Text, "#,#0.00")
                    If Me.BindingContext(Me.Tabla).Current("Factura") = Me.txtFactura.EditValue Then
                        Me.BindingContext(Me.Tabla).RemoveAt(BindingContext(Me.Tabla).Position())
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error este campo se requiere un n�merico", "SeePos", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtAbono.Text = 0
            txtAbono.Focus()
        End Try

    End Sub

    Private Sub txtAbono_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAbono.GotFocus
        txtAbono.SelectAll()
    End Sub

    Private Sub txtIntereses_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtIntereses.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "."c) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub GridControl2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridControl2.KeyDown
        If e.KeyCode = Keys.Delete Then
            Eliminar_Factura_Detalle()
        End If
    End Sub

    Private Sub Eliminar_Factura_Detalle()
        Dim resp As Integer
        Dim FilaTabla As DataRow
        Dim Conexion2 As New Conexion
        Dim Facturatem As Long
        Dim Fechatem As Date

        Try 'se intenta hacer
            If Me.BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Count > 0 Then  ' si hay ubicaciones
                resp = MessageBox.Show("�Desea eliminar esta factura del Recibo de Dinero?", "SeeSoft", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    FilaTabla = Tabla.NewRow
                    FilaTabla("Factura") = Me.BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Factura")
                    Facturatem = FilaTabla("factura")
                    Fechatem = Conexion2.SlqExecuteScalar(Conexion2.Conectar, "Select Fecha from Ventas Where Num_Factura = " & Facturatem & " AND Cod_Cliente = " & Me.txtCodigo.Text)
                    Conexion2.DesConectar(Conexion2.Conectar)
                    FilaTabla("Fecha") = Fechatem
                    Tabla.Rows.Add(FilaTabla)
                    Me.gridFacturas.DataSource = Me.Tabla
                    Me.BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").RemoveAt(Me.BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Position)
                    Me.BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").EndCurrentEdit()
                    Me.txtAbonoGen.Text = Format(Me.colAbono.SummaryItem.SummaryValue + Me.colIntereses.SummaryItem.SummaryValue, "#,#0.00")
                    txtSaldoActGen.Text = Format(txtSaldoAntGen.Text - txtAbonoGen.Text, "#,#0.00")
                    Me.BindingContext(Me.DsRecibos1, "abonoccobrar").EndCurrentEdit()
                Else
                    Me.BindingContext(Me.DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").CancelCurrentEdit()
                End If
            Else
                MsgBox("No Existen Facturas para eliminar del Recibo de Dinero", MsgBoxStyle.Information)
            End If
        Catch eEndEdit As System.Data.NoNullAllowedException
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
        End Try
    End Sub

    Private Sub txtIntereses_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIntereses.LostFocus
        Dim Temp As Double
        Try
            Temp = CDbl(txtIntereses.Text)
        Catch ex As Exception
            txtIntereses.Text = 0
        End Try
    End Sub

    Private Sub txtAbono_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAbono.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "."c) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub lbSimbolo1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSimbolo1.TextChanged
        lbSimbolo2.Text = lbSimbolo1.Text
        lbSimbolo3.Text = lbSimbolo1.Text
        lbSimbolo4.Text = lbSimbolo1.Text
        lbSimbolo5.Text = lbSimbolo1.Text
        lbSimbolo6.Text = lbSimbolo1.Text
    End Sub

    Private Sub Check_Dig_Recibo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Dig_Recibo.CheckedChanged
        If Me.Check_Dig_Recibo.Checked = True Then
            Me.txtNum_Recibo.Enabled = True
            Me.txtNum_Recibo.Text = ""
            Me.txtNum_Recibo.Focus()
        Else
            Me.txtNum_Recibo.Enabled = False
            txtNum_Recibo.Text = Numero_de_Recibo()
        End If
    End Sub

    Private Sub txtNum_Recibo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNum_Recibo.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub btModFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btModFecha.Click
        Me.modFecha()
    End Sub

    Sub modFecha()
        If Not Me.dtFecha.Enabled Then
            Me.dtFecha.Enabled = True
            btModFecha.Text = "Aceptar"
        Else
            Me.dtFecha.Enabled = False
            btModFecha.Text = "Cambiar Fecha"
        End If

    End Sub


    Private Sub txtCodigo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.TextChanged

    End Sub

    Private Sub txtAbono_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAbono.EditValueChanged

    End Sub

    Private Sub AdvBandedGridView1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AdvBandedGridView1.Click
        Try
            If Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Count = 0 Then Exit Sub
            Dim hi As DevExpress.XtraGrid.Views.BandedGrid.ViewInfo.BandedGridHitInfo = _
                   AdvBandedGridView1.CalcHitInfo(CType(gridFacturas, Control).PointToClient(Control.MousePosition))
            Dim data As DataRow
            Dim IDFactura As Double

            If hi.RowHandle >= 0 Then
                data = AdvBandedGridView1.GetDataRow(hi.RowHandle)
            ElseIf AdvBandedGridView1.FocusedRowHandle >= 0 Then
                data = AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle)
            Else
                data = Nothing
            End If
            AdvBandedGridView1.FocusedColumn = AdvBandedGridView1.Columns(0)
            IDFactura = AdvBandedGridView1.FocusedValue
            Me.txtID_Factura.Text = IDFactura
            Me.BindingContext(Me.DsRecibos1, "abonoccobrar").Current("Ced_Usuario") = Me.Cedula_Usuario
            Me.BindingContext(Me.DsRecibos1, "abonoccobrar").EndCurrentEdit()
            informacionfactura(IDFactura)
            Me.txtID_Factura.Text = IDFactura
        Catch ex As SyntaxErrorException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboMoneda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboMoneda.SelectedIndexChanged

    End Sub

    Private Sub txtTipoCambio_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTipoCambio.TextChanged

    End Sub

    Private Sub txtUsuario_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUsuario.TextChanged

    End Sub
End Class
