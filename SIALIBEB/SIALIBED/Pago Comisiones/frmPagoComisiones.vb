Imports System.data.SqlClient

Public Class frmPagoComisiones

    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlClient.SqlConnection
    Dim Tabla As DataTable
    Dim cedula As String
    Dim ii As Integer
    Dim n, contador As Integer
    Dim facturas(500) As Integer
    Dim PMU As New PerfilModulo_Class

    Private Sub frmPagoComisiones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Hotel", "CONEXION")
            conectadobd = cConexion.Conectar("Hotel")
            Me.adCliente.Fill(Me.DsComisiones1, "Cliente")
            Me.adVentas.Fill(Me.DsComisiones1, "Ventas")
            Me.adMoneda.Fill(Me.DsComisiones1, "Moneda")

            Me.GroupBox3.Enabled = False
            Me.GroupBox1.Enabled = False
            Me.GroupBox2.Enabled = False
            Me.txtPagar.Text = "0"

            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Hotel")
            Dim NoClave As Boolean = cConexion.SlqExecuteScalar(conectadobd, "SELECT NoClave FROM Configuraciones")
            If NoClave Then
                Loggin_Usuario()
            Else
                TextBox2.Focus()
            End If
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Hotel")
            '---------------------------------------------------------------
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub txtComisionista_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComisionista.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.F1
                    Dim cFunciones As New cFunciones
                    Me.txtComisionista.Text = cFunciones.BuscarDatos("SELECT Id AS Codigo, Nombre AS Comisionista FROM Cliente", "Nombre", "Busqueda de Comisionistas...", Me.SqlConnection1.ConnectionString)
                Case Keys.Enter
                    If Not IsNumeric(Me.txtComisionista.Text) Then Exit Sub

                    CargarInformacionComisionista(Me.txtComisionista.Text)
                    ' Me.txtComisionista.Text = ii
                    Me.GroupBox1.Focus()
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub CargarInformacionComisionista(ByVal codigo As String)
        Dim cConexion As New Conexion
        Dim funciones As New cFunciones
        Dim rs As SqlDataReader
        'Dim i As Integer
        'Dim fila As DataRow
        'Dim factura As Long
        Dim conex As SqlConnection = Me.SqlConnection1

        If codigo <> Nothing Then
            conex.Open()
            rs = cConexion.GetRecorset(conex, "SELECT Id AS Codigo, Nombre AS Comisionista, Comicion as Comision FROM Cliente where Id  ='" & codigo & "'")
            Try
                If rs.Read Then
                    Me.txtComisionista.Text = rs("Codigo")
                    Me.txtNombre.Text = rs("Comisionista")
                    Me.txtPorc.Text = rs("Comision")
                    ii = rs("Codigo")
                    funciones.Cargar_Tabla_Generico(Me.adCliente, "SELECT * from Cliente where Id  ='" & codigo & "'", conex.ConnectionString)
                    Me.adCliente.Fill(Me.DsComisiones1, "Cliente")
                    Tabla = Me.BuscarComisiones(codigo, conex.ConnectionString)
                    Me.gridFacturas.DataSource = Tabla
                    'GridControl1.Enabled = False
                    'Saldo_Cuenta(Tabla)

                    conex.Close()
                    If Tabla.Rows.Count = 0 Then
                        MessageBox.Show("El Cliente no tiene comisiones pendientes...", "Atenci�n...", MessageBoxButtons.OK)
                        Me.txtComisionista.Focus()
                        rs.Close()
                        Exit Sub
                    End If
                Else
                    MsgBox("La identificaci�n del Comisionista no se encuentra", MsgBoxStyle.Information, "Atenci�n...")
                    Me.txtComisionista.Focus()
                    rs.Close()
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

            rs.Close()
            cConexion.DesConectar(cConexion.Conectar("Hotel"))
        End If
    End Sub

    Function BuscarComisiones(ByVal Codigo As Integer, Optional ByVal Conexion As String = "") As DataTable
        Dim cnn As SqlConnection = Nothing
        Dim dt As New DataTable
        ' Dentro de un Try/Catch por si se produce un error
        Try
            ' Obtenemos la cadena de conexi�n adecuada
            Dim sConn As String = IIf(Conexion = "", GetSetting("SeeSOFT", "Restaurante", "CONEXION"), Conexion)
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = _
            "SELECT Fecha,Num_Factura as Factura, SubTotal FROM Ventas WHERE (FacturaCancelado = 0) AND  (IdCliente = " & Codigo & ") and Pago_Comision=0"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmd.Parameters.Add(New SqlParameter("@Codigo", SqlDbType.Int))
            cmd.Parameters("@Codigo").Value = Codigo
            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            ' Llenamos la tabla

            da.Fill(dt)

        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
            Return Nothing
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try

        Return dt ' Devolvemos el objeto DataTable con los datos de la consulta
    End Function

    Private Sub InformacionFactura(ByVal Factura As Double, ByVal Proveedor As Integer)
        Dim cConexion As New Conexion
        Dim rs As SqlDataReader
        Try
            
            If Factura <> Nothing Then
                rs = cConexion.GetRecorset(cConexion.Conectar("Hotel"), "SELECT Num_Factura as Factura, Fecha, SubTotal FROM Ventas WHERE (FacturaCancelado = 0) AND  (IdCliente = " & Me.txtComisionista.Text & ") AND Num_Factura=" & Factura)
                While rs.Read
                    Try

                        Me.TextBox1.Text = rs!Factura
                        Me.txtSubTotal.Text = rs!SubTotal
                        Me.txtTotalPagar.Text = CDbl(Me.txtSubTotal.Text) * CDbl(Me.txtPorc.Text) / 100

                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try

                End While
                rs.Close()
                cConexion.DesConectar(cConexion.sQlconexion)

            End If
            facturas(contador) = Me.TextBox1.Text
            contador += 1
            
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Alerta...!")
        End Try
    End Sub

    

    Private Sub gridFacturas_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gridFacturas.Click
        Try
            Dim hi As DevExpress.XtraGrid.Views.BandedGrid.ViewInfo.BandedGridHitInfo = _
            AdvBandedGridView2.CalcHitInfo(CType(gridFacturas, Control).PointToClient(Control.MousePosition))
            Dim data As DataRow
            Dim factura As Double
            
            If hi.RowHandle >= 0 Then
                data = AdvBandedGridView2.GetDataRow(hi.RowHandle)
            ElseIf AdvBandedGridView2.FocusedRowHandle >= 0 Then
                data = AdvBandedGridView2.GetDataRow(AdvBandedGridView2.FocusedRowHandle)
            Else
                data = Nothing
                Exit Sub
            End If
            factura = data("Factura")

            Me.BindingContext(Me.DsComisiones1, "Ventas").EndCurrentEdit()
            InformacionFactura(factura, Me.txtComisionista.Text)
            
            Me.txtTotalPagar.Focus()

        Catch ex As SyntaxErrorException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

#Region "Funciones"
    Private Sub comision()
        Dim Num As Integer
        Num = cConexion.SlqExecuteScalar(conectadobd, "select isnull(max(id),0)  from ComicionesPagadas")
        Me.txtNum.Text = Num + 1
    End Sub

    Private Sub imprimir()
        Dim ReporteComision As New rptComision
        ReporteComision.SetParameterValue(0, CDbl(Me.txtNum.Text))
        CrystalReportsConexion.LoadShow(ReporteComision, MdiParent, Me.conectadobd.ConnectionString)
    End Sub
    Private Sub registrar()
        Try
            Dim cx As New ConexionR
            Dim campos, datos As String
            campos = "Id_Cliente,TotalMontoPagado,Fecha,Cedula,CodMoneda,MonedaNombre,TipoCambio,Anulada"
            Dim tipocambio As Integer = Me.BindingContext(Me.DsComisiones1, "Moneda").Current("ValorCompra")
            Dim codMoneda As Integer = Me.BindingContext(Me.DsComisiones1, "Moneda").Current("CodMoneda")
            datos = CDbl(Me.txtComisionista.Text) & "," & CDbl(Me.txtPagar.Text) & ",getdate(),'" & cedula & "'," & codMoneda & ",'" & Me.ComboBox1.Text & "'," & tipocambio & ",0"
            Dim idComision As Integer
            cx.AddNewRecord(conectadobd, "ComicionesPagadas", campos, datos)
            idComision = cConexion.SlqExecuteScalar(conectadobd, "select max(id)  from ComicionesPagadas")
            Dim camposd, datosd As String
            camposd = "Id_Reservacion,CodMoneda,Id_ComicionesPagadas,Monto"
            datosd = "0," & codMoneda & "," & idComision & "," & CDbl(Me.txtPagar.Text)
            cx.AddNewRecord(conectadobd, "DetalleComicionesPagadas", camposd, datosd)

            '** Se actualizan los datos de las ventas a las que se    
            'les pago la comision

            Dim sqlString, condicion As String
            sqlString = "Pago_Comision=1"
            For n = 0 To contador - 1
                condicion = "IdCliente=" & CDbl(Me.txtComisionista.Text) & " and Num_Factura=" & facturas(n)
                If Me.SqlConnection1.State = ConnectionState.Closed Then Me.SqlConnection1.Open()
                cx.UpdateRecords(Me.SqlConnection1, "Ventas", sqlString, condicion)
                If Me.SqlConnection1.State = ConnectionState.Open Then Me.SqlConnection1.Close()
            Next
            '**
            MsgBox("Datos actualizados correctamente...", MsgBoxStyle.Information)
            If MessageBox.Show("�Desea imprimir la comisi�n?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                Dim ReporteComision As New rptComision
                ReporteComision.SetParameterValue(0, CDbl(Me.txtNum.Text))
                CrystalReportsConexion.LoadShow(ReporteComision, MdiParent, Me.conectadobd.ConnectionString)
            End If
            Me.GroupBox3.Enabled = False
            Me.GroupBox1.Enabled = False
            Me.GroupBox2.Enabled = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub agregar()
        If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
            'Dim aa As String
            'aa = GetSetting("SeeSoft", "Hotel", "Conexion")
            Me.ToolBar1.Buttons(0).Text = "Cancelar"
            Me.ToolBar1.Buttons(0).ImageIndex = 4
            Me.ToolBarEliminar.Enabled = False
            Me.ToolBarRegistrar.Enabled = True
            Me.GroupBox3.Enabled = True
            Me.GroupBox1.Enabled = True
            Me.GroupBox2.Enabled = True
            Me.txtComisionista.Text = ""
            Me.txtNombre.Text = ""
            Me.txtPorc.Text = ""
            Me.TextBox1.Text = ""
            Me.txtSubTotal.Text = ""
            Me.txtTotalPagar.Text = ""
            comision()
            Me.txtComisionista.Focus()
        Else
            Me.ToolBar1.Buttons(0).Text = "Nuevo"
            Me.ToolBar1.Buttons(0).ImageIndex = 0
            Me.ToolBarEliminar.Enabled = False
            Me.ToolBarRegistrar.Enabled = False
            Me.GroupBox3.Enabled = False
            Me.GroupBox1.Enabled = False
            Me.GroupBox2.Enabled = False
        End If
    End Sub

    Private Sub Buscar()
        Try
            Dim cFunciones As New cFunciones
            Dim cConexion As New Conexion
            Dim rs As SqlDataReader
            Dim conex As SqlConnection = Me.SqlConnection1
            Dim comision As Integer
            comision = cFunciones.BuscarDatos("SELECT ComicionesPagadas.Id as Comision,Cliente.Nombre as Nombre FROM  ComicionesPagadas INNER JOIN Cliente ON ComicionesPagadas.Id_Cliente = Cliente.Id", "Nombre", "Busqueda de Comisionistas...", Me.SqlConnection1.ConnectionString)
            If comision <> Nothing Then
                conex.Open()
                rs = cConexion.GetRecorset(conectadobd, "SELECT ComicionesPagadas.Id,ComicionesPagadas.Id_Cliente ,Cliente.Nombre as Nombre,Cliente.Comicion,ComicionesPagadas.TotalMontoPagado,ComicionesPagadas.CodMoneda FROM  ComicionesPagadas INNER JOIN Cliente ON ComicionesPagadas.Id_Cliente = Cliente.Id where ComicionesPagadas.Id  ='" & comision & "'")
                Try
                    If rs.Read Then
                        Me.txtNum.Text = rs("Id")
                        Me.txtNombre.Text = rs("Nombre")
                        Me.txtPorc.Text = rs("Comicion")
                        Me.txtPagar.Text = rs("TotalMontoPagado")
                        Me.ComboBox1.SelectedValue = rs("CodMoneda")

                        Me.GroupBox1.Visible = False
                        Me.GroupBox2.Visible = False
                    End If
                Catch ex As Exception

                End Try
            End If
            Me.ToolBarEliminar.Enabled = True
            Me.ToolBarRegistrar.Enabled = False
            conex.Close()
            rs.Close()
            cConexion.DesConectar(cConexion.Conectar("Hotel"))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub
#End Region

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Agregar()

            Case 3 : Buscar()

            Case 4 : If PMU.Update Then Me.registrar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Print Then Me.imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 6 : Me.Close()

        End Select
    End Sub

#Region "Validacion Usuario"
    Private Sub TextBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox2.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim conectadobd As New SqlClient.SqlConnection
            Dim cConexion As New ConexionR
            conectadobd = cConexion.Conectar("Restaurante")
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox2.Text & "'")
            'Me.Enabled = VerificandoAcceso_a_Modulos(Me, cedula)
            'Dim usuario As String = LoginUsuario(cConexion, conectadobd, Me.TextBox2.Text, Me.Text, Me.Name)
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")
            If usuario <> "" Then
                TextBox2.Text = ""
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarEditar.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarRegistrar.Enabled = False
                Me.lbUsuario.Text = usuario
            Else
                Me.Enabled = True
            End If
            Me.ToolBar1.Focus()
            cConexion.DesConectar(conectadobd)
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                TextBox2.Text = ""
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarEditar.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarRegistrar.Enabled = False
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub txtTotalPagar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTotalPagar.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Enter
                    Me.txtPagar.Text = CDbl(Me.txtPagar.Text) + CDbl(Me.txtTotalPagar.Text)

                    If Me.BindingContext(Me.Tabla).Current("Factura") = Me.TextBox1.Text Then

                        'Dim strFactura As String = Me.BindingContext(DsComisiones1, "Ventas").Current("Factura")

                        Me.BindingContext(Me.Tabla).RemoveAt(BindingContext(Me.Tabla).Position())

                    End If
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub txtTotalPagar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalPagar.TextChanged

    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub
End Class