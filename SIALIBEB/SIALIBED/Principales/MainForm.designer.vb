<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.ToolTipController1 = New DevExpress.Utils.ToolTipController()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.ButtonFacturar = New System.Windows.Forms.Button()
        Me.bttBuffet = New System.Windows.Forms.Button()
        Me.ButtonFactura = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.ButtonComandas = New System.Windows.Forms.Button()
        Me.ButtonApertura = New System.Windows.Forms.Button()
        Me.btnAbreCaja = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.ButtonCierreViejo = New System.Windows.Forms.Button()
        Me.ButtonArqueo = New System.Windows.Forms.Button()
        Me.ButtonCierre = New System.Windows.Forms.Button()
        Me.btCentroProduccionConfig = New System.Windows.Forms.Button()
        Me.btUpdate = New System.Windows.Forms.Button()
        Me.btConfiguracionMovil = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.B_Cortesias = New System.Windows.Forms.Button()
        Me.ButtonControlClientes = New System.Windows.Forms.Button()
        Me.ButtonSalones = New System.Windows.Forms.Button()
        Me.ButtonSecciones = New System.Windows.Forms.Button()
        Me.ButtonMesas = New System.Windows.Forms.Button()
        Me.ButtonGrupoMenu = New System.Windows.Forms.Button()
        Me.ButtonCategorias = New System.Windows.Forms.Button()
        Me.ButtonMenu = New System.Windows.Forms.Button()
        Me.ButtonAcompañamientos = New System.Windows.Forms.Button()
        Me.ButtonRecetas = New System.Windows.Forms.Button()
        Me.ButtonModificadoresForzados = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.BtnReporteHoras = New System.Windows.Forms.Button()
        Me.ButtonCxCReporte = New System.Windows.Forms.Button()
        Me.B_Reporte_CostoMenu = New System.Windows.Forms.Button()
        Me.ButtonReporteComandas = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.ButtonReportes = New System.Windows.Forms.Button()
        Me.ButtonReporteComisiones = New System.Windows.Forms.Button()
        Me.ButtonPagoComision = New System.Windows.Forms.Button()
        Me.btCentroProduccionOperaciones = New System.Windows.Forms.Button()
        Me.btCentroComandas = New System.Windows.Forms.Button()
        Me.btRepartidores = New System.Windows.Forms.Button()
        Me.btRepartidoresOp = New System.Windows.Forms.Button()
        Me.btRptVarios = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnConfiguracionTamano = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Btn_ConfiAvanzada = New System.Windows.Forms.Button()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolTipController1
        '
        Me.ToolTipController1.Style = New DevExpress.Utils.ViewStyle("ToolTip style", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 12.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                Or DevExpress.Utils.StyleOptions.UseFont) _
                Or DevExpress.Utils.StyleOptions.UseForeColor) _
                Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                Or DevExpress.Utils.StyleOptions.UseImage) _
                Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.DimGray, System.Drawing.Color.Azure)
        '
        'Button10
        '
        resources.ApplyResources(Me.Button10, "Button10")
        Me.Button10.BackColor = System.Drawing.Color.White
        Me.Button10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button10.FlatAppearance.BorderSize = 0
        Me.Button10.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button10.Name = "Button10"
        Me.ToolTipController1.SetToolTip(Me.Button10, "Iniciar sesión con otro usuario.")
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Button4
        '
        resources.ApplyResources(Me.Button4, "Button4")
        Me.Button4.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button4.FlatAppearance.BorderSize = 0
        Me.Button4.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button4.Name = "Button4"
        Me.ToolTipController1.SetToolTip(Me.Button4, "Cierra el sistema de restaurante.")
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        resources.ApplyResources(Me.Button3, "Button3")
        Me.Button3.BackColor = System.Drawing.Color.White
        Me.Button3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button3.Name = "Button3"
        Me.ToolTipController1.SetToolTip(Me.Button3, "Crea una orden para pedidos a entregar a domicilio")
        Me.Button3.UseVisualStyleBackColor = True
        '
        'ButtonFacturar
        '
        resources.ApplyResources(Me.ButtonFacturar, "ButtonFacturar")
        Me.ButtonFacturar.BackColor = System.Drawing.Color.Transparent
        Me.ButtonFacturar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonFacturar.FlatAppearance.BorderSize = 0
        Me.ButtonFacturar.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonFacturar.Name = "ButtonFacturar"
        Me.ToolTipController1.SetToolTip(Me.ButtonFacturar, "Crea una orden y a continuación la factura")
        Me.ButtonFacturar.UseVisualStyleBackColor = False
        '
        'bttBuffet
        '
        resources.ApplyResources(Me.bttBuffet, "bttBuffet")
        Me.bttBuffet.BackColor = System.Drawing.Color.White
        Me.bttBuffet.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bttBuffet.FlatAppearance.BorderSize = 0
        Me.bttBuffet.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.bttBuffet.Name = "bttBuffet"
        Me.ToolTipController1.SetToolTip(Me.bttBuffet, "Administración de consumos y costos por buffets")
        Me.bttBuffet.UseVisualStyleBackColor = False
        '
        'ButtonFactura
        '
        resources.ApplyResources(Me.ButtonFactura, "ButtonFactura")
        Me.ButtonFactura.BackColor = System.Drawing.SystemColors.Control
        Me.ButtonFactura.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonFactura.FlatAppearance.BorderSize = 0
        Me.ButtonFactura.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonFactura.Name = "ButtonFactura"
        Me.ToolTipController1.SetToolTip(Me.ButtonFactura, "Buscar y reimprimir Facturas y Cortesias")
        Me.ButtonFactura.UseVisualStyleBackColor = False
        '
        'Button2
        '
        resources.ApplyResources(Me.Button2, "Button2")
        Me.Button2.BackColor = System.Drawing.Color.White
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button2.Name = "Button2"
        Me.ToolTipController1.SetToolTip(Me.Button2, "Consumos y costos de desayunos.")
        Me.Button2.UseVisualStyleBackColor = False
        '
        'ButtonComandas
        '
        resources.ApplyResources(Me.ButtonComandas, "ButtonComandas")
        Me.ButtonComandas.BackColor = System.Drawing.Color.Transparent
        Me.ButtonComandas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonComandas.FlatAppearance.BorderSize = 0
        Me.ButtonComandas.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonComandas.Name = "ButtonComandas"
        Me.ToolTipController1.SetToolTip(Me.ButtonComandas, "Crear y administrar las comandas")
        Me.ButtonComandas.UseVisualStyleBackColor = False
        '
        'ButtonApertura
        '
        resources.ApplyResources(Me.ButtonApertura, "ButtonApertura")
        Me.ButtonApertura.BackColor = System.Drawing.Color.White
        Me.ButtonApertura.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonApertura.FlatAppearance.BorderSize = 0
        Me.ButtonApertura.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonApertura.Name = "ButtonApertura"
        Me.ToolTipController1.SetToolTip(Me.ButtonApertura, "Realizar apertura de caja")
        Me.ButtonApertura.UseVisualStyleBackColor = False
        '
        'btnAbreCaja
        '
        resources.ApplyResources(Me.btnAbreCaja, "btnAbreCaja")
        Me.btnAbreCaja.BackColor = System.Drawing.Color.Transparent
        Me.btnAbreCaja.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAbreCaja.FlatAppearance.BorderSize = 0
        Me.btnAbreCaja.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btnAbreCaja.Name = "btnAbreCaja"
        Me.ToolTipController1.SetToolTip(Me.btnAbreCaja, "Abrir cajon de dinero")
        Me.btnAbreCaja.UseVisualStyleBackColor = False
        '
        'Button5
        '
        resources.ApplyResources(Me.Button5, "Button5")
        Me.Button5.BackColor = System.Drawing.Color.White
        Me.Button5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button5.FlatAppearance.BorderSize = 0
        Me.Button5.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button5.Name = "Button5"
        Me.ToolTipController1.SetToolTip(Me.Button5, "Registrar movimiento de efectivo en caja.")
        Me.Button5.UseVisualStyleBackColor = True
        '
        'ButtonCierreViejo
        '
        resources.ApplyResources(Me.ButtonCierreViejo, "ButtonCierreViejo")
        Me.ButtonCierreViejo.BackColor = System.Drawing.Color.White
        Me.ButtonCierreViejo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonCierreViejo.FlatAppearance.BorderSize = 0
        Me.ButtonCierreViejo.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonCierreViejo.Name = "ButtonCierreViejo"
        Me.ToolTipController1.SetToolTip(Me.ButtonCierreViejo, "Informes de caja para revisión.")
        Me.ButtonCierreViejo.UseVisualStyleBackColor = False
        '
        'ButtonArqueo
        '
        resources.ApplyResources(Me.ButtonArqueo, "ButtonArqueo")
        Me.ButtonArqueo.BackColor = System.Drawing.Color.White
        Me.ButtonArqueo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonArqueo.FlatAppearance.BorderSize = 0
        Me.ButtonArqueo.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonArqueo.Name = "ButtonArqueo"
        Me.ToolTipController1.SetToolTip(Me.ButtonArqueo, "Crear y administrar los arqueo de caja.")
        Me.ButtonArqueo.UseVisualStyleBackColor = False
        '
        'ButtonCierre
        '
        resources.ApplyResources(Me.ButtonCierre, "ButtonCierre")
        Me.ButtonCierre.BackColor = System.Drawing.Color.White
        Me.ButtonCierre.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonCierre.FlatAppearance.BorderSize = 0
        Me.ButtonCierre.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonCierre.Name = "ButtonCierre"
        Me.ToolTipController1.SetToolTip(Me.ButtonCierre, "Realizar cierre de caja")
        Me.ButtonCierre.UseVisualStyleBackColor = False
        '
        'btCentroProduccionConfig
        '
        resources.ApplyResources(Me.btCentroProduccionConfig, "btCentroProduccionConfig")
        Me.btCentroProduccionConfig.BackColor = System.Drawing.Color.DimGray
        Me.btCentroProduccionConfig.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.centro3__1_
        Me.btCentroProduccionConfig.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btCentroProduccionConfig.FlatAppearance.BorderSize = 0
        Me.btCentroProduccionConfig.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btCentroProduccionConfig.Name = "btCentroProduccionConfig"
        Me.ToolTipController1.SetToolTip(Me.btCentroProduccionConfig, "Centro de producción")
        Me.btCentroProduccionConfig.UseVisualStyleBackColor = False
        '
        'btUpdate
        '
        resources.ApplyResources(Me.btUpdate, "btUpdate")
        Me.btUpdate.BackColor = System.Drawing.Color.White
        Me.btUpdate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btUpdate.FlatAppearance.BorderSize = 0
        Me.btUpdate.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btUpdate.Name = "btUpdate"
        Me.ToolTipController1.SetToolTip(Me.btUpdate, "Actualizar la aplicación")
        Me.btUpdate.UseVisualStyleBackColor = False
        '
        'btConfiguracionMovil
        '
        resources.ApplyResources(Me.btConfiguracionMovil, "btConfiguracionMovil")
        Me.btConfiguracionMovil.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btConfiguracionMovil.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.Movil_Config_Inicio_Logo_V1
        Me.btConfiguracionMovil.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btConfiguracionMovil.FlatAppearance.BorderSize = 0
        Me.btConfiguracionMovil.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btConfiguracionMovil.Name = "btConfiguracionMovil"
        Me.ToolTipController1.SetToolTip(Me.btConfiguracionMovil, "Configuración de ordenamiento en dispositivos móviles.")
        Me.btConfiguracionMovil.UseVisualStyleBackColor = False
        '
        'Button6
        '
        resources.ApplyResources(Me.Button6, "Button6")
        Me.Button6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button6.FlatAppearance.BorderSize = 0
        Me.Button6.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button6.Name = "Button6"
        Me.ToolTipController1.SetToolTip(Me.Button6, "Captura hora de ingreso de personal.")
        Me.Button6.UseVisualStyleBackColor = False
        '
        'B_Cortesias
        '
        resources.ApplyResources(Me.B_Cortesias, "B_Cortesias")
        Me.B_Cortesias.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.B_Cortesias.Cursor = System.Windows.Forms.Cursors.Hand
        Me.B_Cortesias.FlatAppearance.BorderSize = 0
        Me.B_Cortesias.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.B_Cortesias.Name = "B_Cortesias"
        Me.ToolTipController1.SetToolTip(Me.B_Cortesias, "Centros de Cortesías")
        Me.B_Cortesias.UseVisualStyleBackColor = False
        '
        'ButtonControlClientes
        '
        resources.ApplyResources(Me.ButtonControlClientes, "ButtonControlClientes")
        Me.ButtonControlClientes.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonControlClientes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonControlClientes.FlatAppearance.BorderSize = 0
        Me.ButtonControlClientes.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonControlClientes.Name = "ButtonControlClientes"
        Me.ToolTipController1.SetToolTip(Me.ButtonControlClientes, "Administrar lista de clientes.")
        Me.ButtonControlClientes.UseVisualStyleBackColor = False
        '
        'ButtonSalones
        '
        resources.ApplyResources(Me.ButtonSalones, "ButtonSalones")
        Me.ButtonSalones.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonSalones.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonSalones.FlatAppearance.BorderSize = 0
        Me.ButtonSalones.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonSalones.Name = "ButtonSalones"
        Me.ToolTipController1.SetToolTip(Me.ButtonSalones, "Crear salones del Restaurante")
        Me.ButtonSalones.UseVisualStyleBackColor = False
        '
        'ButtonSecciones
        '
        resources.ApplyResources(Me.ButtonSecciones, "ButtonSecciones")
        Me.ButtonSecciones.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonSecciones.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonSecciones.FlatAppearance.BorderSize = 0
        Me.ButtonSecciones.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonSecciones.Name = "ButtonSecciones"
        Me.ToolTipController1.SetToolTip(Me.ButtonSecciones, "Crear Secciones del Restaurante")
        Me.ButtonSecciones.UseVisualStyleBackColor = False
        '
        'ButtonMesas
        '
        resources.ApplyResources(Me.ButtonMesas, "ButtonMesas")
        Me.ButtonMesas.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonMesas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonMesas.FlatAppearance.BorderSize = 0
        Me.ButtonMesas.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonMesas.Name = "ButtonMesas"
        Me.ToolTipController1.SetToolTip(Me.ButtonMesas, "Crear mesas del Restaurante")
        Me.ButtonMesas.UseVisualStyleBackColor = False
        '
        'ButtonGrupoMenu
        '
        resources.ApplyResources(Me.ButtonGrupoMenu, "ButtonGrupoMenu")
        Me.ButtonGrupoMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonGrupoMenu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonGrupoMenu.FlatAppearance.BorderSize = 0
        Me.ButtonGrupoMenu.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonGrupoMenu.Name = "ButtonGrupoMenu"
        Me.ToolTipController1.SetToolTip(Me.ButtonGrupoMenu, "Crea los Grupos de Menú")
        Me.ButtonGrupoMenu.UseVisualStyleBackColor = False
        '
        'ButtonCategorias
        '
        resources.ApplyResources(Me.ButtonCategorias, "ButtonCategorias")
        Me.ButtonCategorias.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonCategorias.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonCategorias.FlatAppearance.BorderSize = 0
        Me.ButtonCategorias.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonCategorias.Name = "ButtonCategorias"
        Me.ToolTipController1.SetToolTip(Me.ButtonCategorias, "Crea las Categorias del Menú")
        Me.ButtonCategorias.UseVisualStyleBackColor = False
        '
        'ButtonMenu
        '
        resources.ApplyResources(Me.ButtonMenu, "ButtonMenu")
        Me.ButtonMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonMenu.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.Menus_Inicio_Logo_V2
        Me.ButtonMenu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonMenu.FlatAppearance.BorderSize = 0
        Me.ButtonMenu.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonMenu.Name = "ButtonMenu"
        Me.ToolTipController1.SetToolTip(Me.ButtonMenu, "Crea el Menú")
        Me.ButtonMenu.UseVisualStyleBackColor = False
        '
        'ButtonAcompañamientos
        '
        resources.ApplyResources(Me.ButtonAcompañamientos, "ButtonAcompañamientos")
        Me.ButtonAcompañamientos.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonAcompañamientos.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.Acompañamientos_Inicio_Logo
        Me.ButtonAcompañamientos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonAcompañamientos.FlatAppearance.BorderSize = 0
        Me.ButtonAcompañamientos.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonAcompañamientos.Name = "ButtonAcompañamientos"
        Me.ToolTipController1.SetToolTip(Me.ButtonAcompañamientos, "Crear Acompañamiento")
        Me.ButtonAcompañamientos.UseVisualStyleBackColor = False
        '
        'ButtonRecetas
        '
        resources.ApplyResources(Me.ButtonRecetas, "ButtonRecetas")
        Me.ButtonRecetas.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonRecetas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonRecetas.FlatAppearance.BorderSize = 0
        Me.ButtonRecetas.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonRecetas.Name = "ButtonRecetas"
        Me.ToolTipController1.SetToolTip(Me.ButtonRecetas, "Crear una Receta")
        Me.ButtonRecetas.UseVisualStyleBackColor = False
        '
        'ButtonModificadoresForzados
        '
        resources.ApplyResources(Me.ButtonModificadoresForzados, "ButtonModificadoresForzados")
        Me.ButtonModificadoresForzados.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ButtonModificadoresForzados.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.Modificadores_Inicio_Logo_V12
        Me.ButtonModificadoresForzados.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonModificadoresForzados.FlatAppearance.BorderSize = 0
        Me.ButtonModificadoresForzados.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonModificadoresForzados.Name = "ButtonModificadoresForzados"
        Me.ToolTipController1.SetToolTip(Me.ButtonModificadoresForzados, "Crear Modificador Forzados")
        Me.ButtonModificadoresForzados.UseVisualStyleBackColor = False
        '
        'Button12
        '
        resources.ApplyResources(Me.Button12, "Button12")
        Me.Button12.BackColor = System.Drawing.Color.Transparent
        Me.Button12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button12.FlatAppearance.BorderSize = 0
        Me.Button12.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button12.Name = "Button12"
        Me.ToolTipController1.SetToolTip(Me.Button12, "Listado de ingresos acumulados")
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Button11
        '
        resources.ApplyResources(Me.Button11, "Button11")
        Me.Button11.BackColor = System.Drawing.Color.Transparent
        Me.Button11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button11.FlatAppearance.BorderSize = 0
        Me.Button11.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button11.Name = "Button11"
        Me.ToolTipController1.SetToolTip(Me.Button11, "Listado de comandas según impresora.")
        Me.Button11.UseVisualStyleBackColor = False
        '
        'BtnReporteHoras
        '
        resources.ApplyResources(Me.BtnReporteHoras, "BtnReporteHoras")
        Me.BtnReporteHoras.BackColor = System.Drawing.Color.Transparent
        Me.BtnReporteHoras.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnReporteHoras.FlatAppearance.BorderSize = 0
        Me.BtnReporteHoras.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.BtnReporteHoras.Name = "BtnReporteHoras"
        Me.ToolTipController1.SetToolTip(Me.BtnReporteHoras, "Reportes de horas trabajadas por empleado")
        Me.BtnReporteHoras.UseVisualStyleBackColor = False
        '
        'ButtonCxCReporte
        '
        resources.ApplyResources(Me.ButtonCxCReporte, "ButtonCxCReporte")
        Me.ButtonCxCReporte.BackColor = System.Drawing.Color.Transparent
        Me.ButtonCxCReporte.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonCxCReporte.FlatAppearance.BorderSize = 0
        Me.ButtonCxCReporte.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonCxCReporte.Name = "ButtonCxCReporte"
        Me.ToolTipController1.SetToolTip(Me.ButtonCxCReporte, "Listado de cuentas por cobrar")
        Me.ButtonCxCReporte.UseVisualStyleBackColor = False
        '
        'B_Reporte_CostoMenu
        '
        resources.ApplyResources(Me.B_Reporte_CostoMenu, "B_Reporte_CostoMenu")
        Me.B_Reporte_CostoMenu.BackColor = System.Drawing.Color.Transparent
        Me.B_Reporte_CostoMenu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.B_Reporte_CostoMenu.FlatAppearance.BorderSize = 0
        Me.B_Reporte_CostoMenu.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.B_Reporte_CostoMenu.Name = "B_Reporte_CostoMenu"
        Me.ToolTipController1.SetToolTip(Me.B_Reporte_CostoMenu, "Costos y margenes de ganacia de menús")
        Me.B_Reporte_CostoMenu.UseVisualStyleBackColor = False
        '
        'ButtonReporteComandas
        '
        resources.ApplyResources(Me.ButtonReporteComandas, "ButtonReporteComandas")
        Me.ButtonReporteComandas.BackColor = System.Drawing.Color.Transparent
        Me.ButtonReporteComandas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonReporteComandas.FlatAppearance.BorderSize = 0
        Me.ButtonReporteComandas.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonReporteComandas.Name = "ButtonReporteComandas"
        Me.ToolTipController1.SetToolTip(Me.ButtonReporteComandas, "Reporte de comanda realizadas.")
        Me.ButtonReporteComandas.UseVisualStyleBackColor = False
        '
        'Button9
        '
        resources.ApplyResources(Me.Button9, "Button9")
        Me.Button9.BackColor = System.Drawing.Color.Transparent
        Me.Button9.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.Recetas_de_Menu_Inicio_Logo_V2
        Me.Button9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button9.FlatAppearance.BorderSize = 0
        Me.Button9.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button9.Name = "Button9"
        Me.ToolTipController1.SetToolTip(Me.Button9, "Impresión de todas la recetas")
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button8
        '
        resources.ApplyResources(Me.Button8, "Button8")
        Me.Button8.BackColor = System.Drawing.Color.Transparent
        Me.Button8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button8.FlatAppearance.BorderSize = 0
        Me.Button8.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button8.Name = "Button8"
        Me.ToolTipController1.SetToolTip(Me.Button8, "Impresión de Menú.")
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button7
        '
        resources.ApplyResources(Me.Button7, "Button7")
        Me.Button7.BackColor = System.Drawing.Color.Transparent
        Me.Button7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button7.FlatAppearance.BorderSize = 0
        Me.Button7.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Button7.Name = "Button7"
        Me.ToolTipController1.SetToolTip(Me.Button7, "Reporte de huespedes en casa.")
        Me.Button7.UseVisualStyleBackColor = False
        '
        'ButtonReportes
        '
        resources.ApplyResources(Me.ButtonReportes, "ButtonReportes")
        Me.ButtonReportes.BackColor = System.Drawing.Color.Transparent
        Me.ButtonReportes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonReportes.FlatAppearance.BorderSize = 0
        Me.ButtonReportes.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonReportes.Name = "ButtonReportes"
        Me.ToolTipController1.SetToolTip(Me.ButtonReportes, "Reportes del Restaurante")
        Me.ButtonReportes.UseVisualStyleBackColor = False
        '
        'ButtonReporteComisiones
        '
        resources.ApplyResources(Me.ButtonReporteComisiones, "ButtonReporteComisiones")
        Me.ButtonReporteComisiones.BackColor = System.Drawing.Color.Transparent
        Me.ButtonReporteComisiones.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonReporteComisiones.FlatAppearance.BorderSize = 0
        Me.ButtonReporteComisiones.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonReporteComisiones.Name = "ButtonReporteComisiones"
        Me.ToolTipController1.SetToolTip(Me.ButtonReporteComisiones, "Comisiones por pagar.")
        Me.ButtonReporteComisiones.UseVisualStyleBackColor = False
        '
        'ButtonPagoComision
        '
        resources.ApplyResources(Me.ButtonPagoComision, "ButtonPagoComision")
        Me.ButtonPagoComision.BackColor = System.Drawing.Color.Transparent
        Me.ButtonPagoComision.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonPagoComision.FlatAppearance.BorderSize = 0
        Me.ButtonPagoComision.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.ButtonPagoComision.Name = "ButtonPagoComision"
        Me.ToolTipController1.SetToolTip(Me.ButtonPagoComision, "Listado de clientes de pedidos express.")
        Me.ButtonPagoComision.UseVisualStyleBackColor = False
        '
        'btCentroProduccionOperaciones
        '
        resources.ApplyResources(Me.btCentroProduccionOperaciones, "btCentroProduccionOperaciones")
        Me.btCentroProduccionOperaciones.BackColor = System.Drawing.Color.DimGray
        Me.btCentroProduccionOperaciones.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.centro3__1_
        Me.btCentroProduccionOperaciones.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btCentroProduccionOperaciones.FlatAppearance.BorderSize = 0
        Me.btCentroProduccionOperaciones.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btCentroProduccionOperaciones.Name = "btCentroProduccionOperaciones"
        Me.ToolTipController1.SetToolTip(Me.btCentroProduccionOperaciones, "Centro de producción")
        Me.btCentroProduccionOperaciones.UseVisualStyleBackColor = False
        '
        'btCentroComandas
        '
        resources.ApplyResources(Me.btCentroComandas, "btCentroComandas")
        Me.btCentroComandas.BackColor = System.Drawing.Color.DimGray
        Me.btCentroComandas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btCentroComandas.FlatAppearance.BorderSize = 0
        Me.btCentroComandas.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btCentroComandas.Name = "btCentroComandas"
        Me.ToolTipController1.SetToolTip(Me.btCentroComandas, "Centro de producción")
        Me.btCentroComandas.UseVisualStyleBackColor = False
        '
        'btRepartidores
        '
        resources.ApplyResources(Me.btRepartidores, "btRepartidores")
        Me.btRepartidores.BackColor = System.Drawing.Color.DimGray
        Me.btRepartidores.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btRepartidores.FlatAppearance.BorderSize = 0
        Me.btRepartidores.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btRepartidores.Name = "btRepartidores"
        Me.ToolTipController1.SetToolTip(Me.btRepartidores, "Centro de producción")
        Me.btRepartidores.UseVisualStyleBackColor = False
        '
        'btRepartidoresOp
        '
        resources.ApplyResources(Me.btRepartidoresOp, "btRepartidoresOp")
        Me.btRepartidoresOp.BackColor = System.Drawing.Color.DimGray
        Me.btRepartidoresOp.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btRepartidoresOp.FlatAppearance.BorderSize = 0
        Me.btRepartidoresOp.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btRepartidoresOp.Name = "btRepartidoresOp"
        Me.ToolTipController1.SetToolTip(Me.btRepartidoresOp, "Centro de producción")
        Me.btRepartidoresOp.UseVisualStyleBackColor = False
        '
        'btRptVarios
        '
        resources.ApplyResources(Me.btRptVarios, "btRptVarios")
        Me.btRptVarios.BackColor = System.Drawing.Color.Transparent
        Me.btRptVarios.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btRptVarios.FlatAppearance.BorderSize = 0
        Me.btRptVarios.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btRptVarios.Name = "btRptVarios"
        Me.ToolTipController1.SetToolTip(Me.btRptVarios, "Reporte varios")
        Me.btRptVarios.UseVisualStyleBackColor = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        resources.ApplyResources(Me.TabControl1, "TabControl1")
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.DarkGray
        resources.ApplyResources(Me.TabPage1, "TabPage1")
        Me.TabPage1.Controls.Add(Me.btRepartidoresOp)
        Me.TabPage1.Controls.Add(Me.btCentroComandas)
        Me.TabPage1.Controls.Add(Me.btCentroProduccionOperaciones)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Controls.Add(Me.Button10)
        Me.TabPage1.Controls.Add(Me.Button4)
        Me.TabPage1.Controls.Add(Me.Button3)
        Me.TabPage1.Controls.Add(Me.ButtonFacturar)
        Me.TabPage1.Controls.Add(Me.bttBuffet)
        Me.TabPage1.Controls.Add(Me.ButtonFactura)
        Me.TabPage1.Controls.Add(Me.Button2)
        Me.TabPage1.Controls.Add(Me.ButtonComandas)
        Me.TabPage1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.TabPage1.Name = "TabPage1"
        '
        'Panel1
        '
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.blanco_3
        Me.Panel1.Name = "Panel1"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.DarkGray
        resources.ApplyResources(Me.TabPage2, "TabPage2")
        Me.TabPage2.Controls.Add(Me.Panel2)
        Me.TabPage2.Controls.Add(Me.ButtonApertura)
        Me.TabPage2.Controls.Add(Me.btnAbreCaja)
        Me.TabPage2.Controls.Add(Me.Button5)
        Me.TabPage2.Controls.Add(Me.ButtonCierreViejo)
        Me.TabPage2.Controls.Add(Me.ButtonArqueo)
        Me.TabPage2.Controls.Add(Me.ButtonCierre)
        Me.TabPage2.Name = "TabPage2"
        '
        'Panel2
        '
        resources.ApplyResources(Me.Panel2, "Panel2")
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.blanco_3
        Me.Panel2.Name = "Panel2"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.DarkGray
        resources.ApplyResources(Me.TabPage3, "TabPage3")
        Me.TabPage3.Controls.Add(Me.btRepartidores)
        Me.TabPage3.Controls.Add(Me.btCentroProduccionConfig)
        Me.TabPage3.Controls.Add(Me.btnConfiguracionTamano)
        Me.TabPage3.Controls.Add(Me.Panel3)
        Me.TabPage3.Controls.Add(Me.Btn_ConfiAvanzada)
        Me.TabPage3.Controls.Add(Me.btUpdate)
        Me.TabPage3.Controls.Add(Me.btConfiguracionMovil)
        Me.TabPage3.Controls.Add(Me.Button6)
        Me.TabPage3.Controls.Add(Me.B_Cortesias)
        Me.TabPage3.Controls.Add(Me.ButtonControlClientes)
        Me.TabPage3.Controls.Add(Me.ButtonSalones)
        Me.TabPage3.Controls.Add(Me.ButtonSecciones)
        Me.TabPage3.Controls.Add(Me.ButtonMesas)
        Me.TabPage3.Controls.Add(Me.ButtonGrupoMenu)
        Me.TabPage3.Controls.Add(Me.ButtonCategorias)
        Me.TabPage3.Controls.Add(Me.ButtonMenu)
        Me.TabPage3.Controls.Add(Me.ButtonAcompañamientos)
        Me.TabPage3.Controls.Add(Me.ButtonRecetas)
        Me.TabPage3.Controls.Add(Me.ButtonModificadoresForzados)
        Me.TabPage3.Name = "TabPage3"
        '
        'btnConfiguracionTamano
        '
        resources.ApplyResources(Me.btnConfiguracionTamano, "btnConfiguracionTamano")
        Me.btnConfiguracionTamano.BackColor = System.Drawing.Color.DimGray
        Me.btnConfiguracionTamano.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnConfiguracionTamano.FlatAppearance.BorderSize = 0
        Me.btnConfiguracionTamano.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.btnConfiguracionTamano.Name = "btnConfiguracionTamano"
        Me.btnConfiguracionTamano.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        resources.ApplyResources(Me.Panel3, "Panel3")
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.blanco_3
        Me.Panel3.Name = "Panel3"
        '
        'Btn_ConfiAvanzada
        '
        resources.ApplyResources(Me.Btn_ConfiAvanzada, "Btn_ConfiAvanzada")
        Me.Btn_ConfiAvanzada.BackColor = System.Drawing.Color.DimGray
        Me.Btn_ConfiAvanzada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_ConfiAvanzada.FlatAppearance.BorderSize = 0
        Me.Btn_ConfiAvanzada.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Btn_ConfiAvanzada.Name = "Btn_ConfiAvanzada"
        Me.Btn_ConfiAvanzada.UseVisualStyleBackColor = False
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.DarkGray
        resources.ApplyResources(Me.TabPage4, "TabPage4")
        Me.TabPage4.Controls.Add(Me.btRptVarios)
        Me.TabPage4.Controls.Add(Me.Panel4)
        Me.TabPage4.Controls.Add(Me.Button12)
        Me.TabPage4.Controls.Add(Me.Button11)
        Me.TabPage4.Controls.Add(Me.BtnReporteHoras)
        Me.TabPage4.Controls.Add(Me.ButtonCxCReporte)
        Me.TabPage4.Controls.Add(Me.B_Reporte_CostoMenu)
        Me.TabPage4.Controls.Add(Me.ButtonReporteComandas)
        Me.TabPage4.Controls.Add(Me.Button9)
        Me.TabPage4.Controls.Add(Me.Button8)
        Me.TabPage4.Controls.Add(Me.Button7)
        Me.TabPage4.Controls.Add(Me.ButtonReportes)
        Me.TabPage4.Controls.Add(Me.ButtonReporteComisiones)
        Me.TabPage4.Controls.Add(Me.ButtonPagoComision)
        Me.TabPage4.Name = "TabPage4"
        '
        'Panel4
        '
        resources.ApplyResources(Me.Panel4, "Panel4")
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.blanco_3
        Me.Panel4.Name = "Panel4"
        '
        'MainForm
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.Controls.Add(Me.TabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "MainForm"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButtonSecciones As System.Windows.Forms.Button
    Friend WithEvents ButtonSalones As System.Windows.Forms.Button
    Friend WithEvents ButtonRecetas As System.Windows.Forms.Button
    Friend WithEvents ButtonGrupoMenu As System.Windows.Forms.Button
    Friend WithEvents ButtonReportes As System.Windows.Forms.Button
    Friend WithEvents ButtonMesas As System.Windows.Forms.Button
    Friend WithEvents ButtonMenu As System.Windows.Forms.Button
    Friend WithEvents ButtonCategorias As System.Windows.Forms.Button
    Friend WithEvents ButtonAcompañamientos As System.Windows.Forms.Button
    Friend WithEvents ButtonModificadoresForzados As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents ButtonComandas As System.Windows.Forms.Button
    Friend WithEvents ButtonArqueo As System.Windows.Forms.Button
    Friend WithEvents ButtonCierre As System.Windows.Forms.Button
    Friend WithEvents ToolTipController1 As DevExpress.Utils.ToolTipController
    Friend WithEvents ButtonPagoComision As System.Windows.Forms.Button
    Friend WithEvents ButtonReporteComisiones As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents ButtonCierreViejo As System.Windows.Forms.Button
    Friend WithEvents ButtonReporteComandas As System.Windows.Forms.Button
    Friend WithEvents ButtonControlClientes As System.Windows.Forms.Button
    Friend WithEvents B_Reporte_CostoMenu As System.Windows.Forms.Button
    Friend WithEvents B_Cortesias As System.Windows.Forms.Button
    Friend WithEvents ButtonFactura As System.Windows.Forms.Button
    Friend WithEvents bttBuffet As System.Windows.Forms.Button
    Friend WithEvents ButtonFacturar As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents btnAbreCaja As System.Windows.Forms.Button
    Friend WithEvents ButtonCxCReporte As System.Windows.Forms.Button
    Friend WithEvents BtnReporteHoras As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents btConfiguracionMovil As System.Windows.Forms.Button
    Friend WithEvents ButtonApertura As System.Windows.Forms.Button
    Friend WithEvents btUpdate As System.Windows.Forms.Button
    Friend WithEvents Btn_ConfiAvanzada As System.Windows.Forms.Button
	Friend WithEvents Panel1 As Panel
	Friend WithEvents Panel2 As Panel
	Friend WithEvents Panel3 As Panel
	Friend WithEvents Panel4 As Panel
	Friend WithEvents btnConfiguracionTamano As Button
    Friend WithEvents btCentroProduccionConfig As Button
    Friend WithEvents btCentroProduccionOperaciones As Button
    Friend WithEvents btCentroComandas As Button
    Friend WithEvents btRepartidores As Button
    Friend WithEvents btRepartidoresOp As Button
    Friend WithEvents btRptVarios As Button
End Class
