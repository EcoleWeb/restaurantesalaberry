Imports System.Runtime.InteropServices
Imports System.Globalization
Imports AutoActualiza
Imports System.Threading

Public Class MainForm

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim Splash As New SplashScreens
    Dim PMU As New PerfilModulo_Class
#End Region
    Dim rs As SqlClient.SqlDataReader
    Private Sub CargarForm(ByVal Form As Form)
        Try
            Form.MdiParent = Me
            Form.StartPosition = FormStartPosition.CenterParent
            Form.ShowDialog()
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If MsgBox("Desea cerrar la aplicación actual...", MsgBoxStyle.YesNo, "Atención...") = MsgBoxResult.Yes Then Close()
    End Sub
    Private Sub btnMesas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSecciones.Click
        MuestraForm(New SeccionesR)
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalones.Click
        MuestraForm(New GrupoMesas)
    End Sub
    Private Sub MuestraForm(ByRef FormDialog As Form)
        Hide()
        FormDialog.ShowDialog(Me)
        Show()
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonComandas.Click
        Try
            MuestraForm(New Comanda)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub
    Private Sub Main_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        conectadobd = cConexion.Conectar("Restaurante")
    End Sub

    '========================= MODULO DE PARA VALIDAR TRIAL DE LA APLICACION ================================
    ' This function does all the work
    <DllImport("Restaurante.dll", EntryPoint:="ReadSettingsStr", CharSet:=CharSet.Ansi)>
    Private Shared Function InitTrial(ByVal aKeyCode As String, ByVal aHWnd As IntPtr) As UInteger
    End Function

    ' Use this function to register the application when the application is running
    <DllImport("Restaurante.dll", EntryPoint:="DisplayRegistrationStr", CharSet:=CharSet.Ansi)>
    Private Shared Function DisplayRegistration(ByVal aKeyCode As String, ByVal aHWnd As IntPtr) As UInteger
    End Function
    '099390767147188B679C4A8BBDDB28A0EA85AE7EF6DD48B4971CE24DABC37B85B7908AA02589

    ' The kLibraryKey is meant to prevent unauthorized use of the library.
    ' Do not share this key. Replace this key with your own from Advanced Installer 
    ' project > Licensing > Registration > Library Key
    Private Shared kLibraryKey As String = "9F2E00629DF10D31A6B066DB7803DE04692FE60687F1264CD4B77EA36D26D299B429446CEF7E"

    Sub spEstablecerFechaLimpieza()
        Dim fechaLimpieza As Date = Now.Date.AddDays(30)
        SaveSetting("SeeSoft", "Restaurante", "LimpiarArchivo", fechaLimpieza.Date)
    End Sub

    'Octubre
    Private Function fnMostrasConfigAvanzada() As Boolean
        Dim Mostrar As String = GetSetting("SeeSoft", "Restaurante", "Tributable")
        Dim CentroComanda As String = GetSetting("SeeSoft", "Restaurante", "CentroComanda")
        Dim Express As String = GetSetting("SeeSoft", "Restaurante", "Repartidores")

        If CentroComanda.Equals("") Then
            SaveSetting("SeeSoft", "Restaurante", "CentroComanda", "No")
        End If

        If Mostrar.Equals("") Then
            SaveSetting("SeeSoft", "Restaurante", "Tributable", 0)

            Me.Btn_ConfiAvanzada.Visible = GetSetting("SeeSoft", "Restaurante", "Tributable")

        Else

            Me.Btn_ConfiAvanzada.Visible = GetSetting("SeeSoft", "Restaurante", "Tributable")
        End If

        If CentroComanda.Equals("Si") Then
            Me.btCentroComandas.Visible = True
            Me.btCentroProduccionConfig.Visible = True
            Me.btCentroProduccionOperaciones.Visible = True
        Else
            Me.btCentroComandas.Visible = False
            Me.btCentroProduccionConfig.Visible = False
            Me.btCentroProduccionOperaciones.Visible = False
        End If


    End Function
    Private Function fnInstanciaUnica() As Boolean
        Dim Instancia As String = GetSetting("SeeSoft", "Restaurante", "InstanciaUnica")
        If Instancia.Equals("") Then
            SaveSetting("SeeSoft", "Restaurante", "InstanciaUnica", "0")
        ElseIf (Instancia.Equals("1")) Then
            If Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName).Length > 1 Then
                Me.Close()
            End If
        End If

    End Function
    Dim HiloInicializarFactura As Thread
    Sub spInicializarFactura()
        If TipoImpres = 1 Then
            inicializarFactura("espanol")
        ElseIf TipoImpres = 2 Then
            inicializarFactura("Ingles")
        ElseIf TipoImpres = 3 Then
            inicializarFactura("ambos")
        ElseIf TipoImpres = 4 Then
            inicializarFactura("espanol")
        End If
    End Sub

    Dim HiloInicializarPrefactura As Thread
    Dim HiloInicializarComanda As Thread

    Private Sub TabControl1_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles TabControl1.DrawItem
        Dim g As Graphics = e.Graphics
        Dim _TextBrush As Brush
        ' Get the item from the collection.
        Dim _TabPage As TabPage = TabControl1.TabPages(e.Index)
        ' Get the real bounds for the tab rectangle.
        Dim _TabBounds As Rectangle = TabControl1.GetTabRect(e.Index)
        If (e.State = DrawItemState.Selected) Then
            ' Draw a different background color, and don't paint a focus rectangle.
            '_TextBrush = New SolidBrush(Color.White)
            'g.FillRectangle(Brushes.LightSkyBlue, e.Bounds)
        Else
            _TextBrush = New System.Drawing.SolidBrush(e.ForeColor)
            e.DrawBackground()
        End If

        _TextBrush = New SolidBrush(Color.White)
        g.FillRectangle(Brushes.DimGray, e.Bounds)
        ' Use our own font.
        Dim _TabFont As New Font("Arial", 16.0, FontStyle.Bold, GraphicsUnit.Pixel)
        ' Draw string. Center the text.
        Dim _StringFlags As New StringFormat()
        _StringFlags.Alignment = StringAlignment.Center
        _StringFlags.LineAlignment = StringAlignment.Center
        g.DrawString(_TabPage.Text, _TabFont, _TextBrush, _TabBounds, New StringFormat(_StringFlags))
    End Sub


    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        fnMostrasConfigAvanzada()

        Try
            '== VALIDA TRIAL / REGISTRO ====
            'Dim Proc As Process = Process.GetCurrentProcess()
            'InitTrial(kLibraryKey, Proc.MainWindowHandle)
            '  cFunciones.spEnviar_Correo(Me.Name, "Load", "Prueba de mensaje de errores de try Catch en modulo de A&B")
            fnInstanciaUnica()
            Dim Hilo As New Thread(AddressOf spCargarDatosComanda)
            Hilo.Start()

            Me.TabControl1.DrawMode = TabDrawMode.OwnerDrawFixed

            If GetSetting("SeeSoft", "Restaurante", "LimpiarArchivo").Equals("") Then
                spEstablecerFechaLimpieza()
            Else
                Dim fechaLimpieza As Date = GetSetting("SeeSoft", "Restaurante", "LimpiarArchivo")
				If fechaLimpieza.Date = Now.Date Then
					Try
						My.Computer.FileSystem.WriteAllText("C:\REPORTES\ClicsLog.txt", "", False)
					Catch ex As Exception

					End Try

					spEstablecerFechaLimpieza()
				End If

			End If

            Dim Usuario As New Frm_login("Seguridad")
            Usuario.ShowDialog()
            If Usuario.conectado Then
                'Application.DoEvents()
                Splash.Show()
                User_Log = New Usuario_Logeado()
                User_Log = Usuario.Usuario
                conectadobd.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")

                Dim dt As New DataTable

                cFunciones.Llenar_Tabla_Generico("Select OpcionFacturacion, Imp_ICT,Imp_Venta, Imp_Servicio, Imp_Servicio_Habitacion , UnicoEncargadoXMesa , CostoCortesia, Cedula, NoClave From configuraciones", dt, GetSetting("SeeSoft", "Hotel", "Conexion"))

                If dt.Rows.Count > 0 Then
                    User_Log.ICT = dt.Rows(0).Item("Imp_ICT")
                    User_Log.IVI = dt.Rows(0).Item("Imp_Venta")
                    User_Log.ISS = dt.Rows(0).Item("Imp_Servicio")
                    User_Log.ISH = dt.Rows(0).Item("Imp_Servicio_Habitacion")
                    User_Log.SolicitarClaveMesa = dt.Rows(0).Item("UnicoEncargadoXMesa")
                    User_Log.CostoCortesia = dt.Rows(0).Item("CostoCortesia")
                    cedula = dt.Rows(0).Item("Cedula")
                    OpcionFacturacion = dt.Rows(0).Item("OpcionFacturacion")
                    gloNoClave = dt.Rows(0).Item("NoClave")
                End If
                If GetSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta").Equals("") Then
                    If cedula = "3-101-104775" Then ' BUENA VISTA
                        SaveSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta", 1)
                    Else
                        SaveSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta", 0)
                    End If

                End If
                If cedula = "602570351" Then ' SABOR PORTEÑO
                    SaveSetting("SeeSOFT", "Restaurante", "Utiliza_Inventario", 0)
                End If



                cFunciones.Llenar_Tabla_Generico("Select utilizaGrupo, ImpFactura from conf", dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
                If dt.Rows.Count > 0 Then
                    usaGrupo = dt.Rows(0).Item("UtilizaGrupo")
                    TipoImpres = dt.Rows(0).Item("ImpFactura")
                    dt.Clear()
                End If

                '--------------------------------------------- ------------------------
                Dim ImprimeFactura As Integer
                Dim ImprimePrefactura As Integer
                Dim PeticionesMoviles As Integer
                If GetSetting("SeeSOFT", "Restaurante", "ImprimeFactura").Equals("") Then SaveSetting("SeeSOFT", "Restaurante", "ImprimeFactura", 1)
                ImprimeFactura = GetSetting("SeeSOFT", "Restaurante", "ImprimeFactura")
                If GetSetting("SeeSOFT", "Restaurante", "ImprimePrefactura").Equals("") Then SaveSetting("SeeSOFT", "Restaurante", "ImprimePrefactura", 1)
                ImprimePrefactura = GetSetting("SeeSOFT", "Restaurante", "ImprimePrefactura")
                If GetSetting("SeeSOFT", "Restaurante", "PeticionesMoviles").Equals("") Then SaveSetting("SeeSOFT", "Restaurante", "PeticionesMoviles", 0)
                PeticionesMoviles = GetSetting("SeeSOFT", "Restaurante", "PeticionesMoviles")

                If ImprimeFactura = 1 Then
                    HiloInicializarFactura = New Thread(AddressOf spInicializarFactura)
                    HiloInicializarFactura.Start()
                End If
                If ImprimePrefactura = 1 Then
                    HiloInicializarPrefactura = New Thread(AddressOf inicializarPreFactura)
                    HiloInicializarPrefactura.Start()
                End If
                If PeticionesMoviles = 0 Then
                    HiloInicializarComanda = New Thread(AddressOf inicializarComandas)
                    HiloInicializarComanda.Start()
                End If


                If GetSetting("SeeSoft", "Planilla", "Marcador") = "2" Then
                    BtnReporteHoras.Enabled = True
                    BtnReporteHoras.Visible = True
                End If

                '---------------------------------------------------------------------
                If GetSetting("SeeSoft", "Seguridad", "Segura") = "0" Then
                    Perfil_Usuario(Usuario.Usuario.Cedula)
                End If

                Splash.Close()
                Splash.Dispose()
                Text = "SEESOFT A&B PUNTO VENTA : " & User_Log.NombrePunto & " - " & "USUARIO: " & User_Log.Nombre & " v." & Globales.GetVersionPublicacion()

                If GetSetting("SeeSoft", "Restaurante", "Acumular") = "1" Then
                    Me.Button12.Visible = True
                    Me.btRptVarios.Visible = True
                Else
                    Me.Button12.Visible = False
                    Me.btRptVarios.Visible = False
                End If
                WindowState = FormWindowState.Maximized
            Else
                End
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            Process.GetCurrentProcess().Kill()
        End Try
        spForzarConfiguracionRegionl()
    End Sub
 
    Sub spForzarConfiguracionRegionl()
        Dim oldDecimalSeparator As String = _
            Application.CurrentCulture.NumberFormat.NumberDecimalSeparator

        Dim forceDotCulture As CultureInfo
        forceDotCulture = Application.CurrentCulture.Clone()
        forceDotCulture.NumberFormat.NumberDecimalSeparator = "."
        forceDotCulture.NumberFormat.NumberGroupSeparator = ","

        forceDotCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
        forceDotCulture.DateTimeFormat.AMDesignator = "a.m."
        forceDotCulture.DateTimeFormat.PMDesignator = "p.m."
        forceDotCulture.DateTimeFormat.ShortTimePattern = "hh:mm tt"

        Application.CurrentCulture = forceDotCulture

    End Sub
    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMenu.Click
        MuestraForm(New MenuI)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRecetas.Click
        MuestraForm(New Receta)
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMesas.Click
        MuestraForm(New Mesas)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonGrupoMenu.Click
        MuestraForm(New MenuGrupo)
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCategorias.Click
        MuestraForm(New GruposMenu)
    End Sub

    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonModificadoresForzados.Click

        Dim dt As New DataTable
        cFunciones.Llenar_Tabla_Generico("Select * From conf", dt)
        If dt.Rows.Count = 0 Then
            MuestraForm(New GCategorias)
        Else
            If dt.Rows(0).Item("tipo") = 1 Then
                Dim frm As New FormLISTADO
                frm.Text = "Modificadores"
                frm.tipo = 1
                MuestraForm(frm)
            Else
                MuestraForm(New GCategorias)

            End If
        End If
    End Sub

    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAcompañamientos.Click

        Dim dt As New DataTable
        cFunciones.Llenar_Tabla_Generico("Select * From conf", dt)
        If dt.Rows.Count = 0 Then
            MuestraForm(New Acompanamiento)
        Else
            If dt.Rows(0).Item("tipo") = 1 Then
                Dim frm As New FormLISTADO
                frm.tipo = 2
                frm.Text = "Acompañamientos"
                MuestraForm(frm)
            Else
                MuestraForm(New Acompanamiento)

            End If

        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MuestraForm(New Saloneros)
    End Sub

   

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCierre.Click
        MuestraForm(New CierreCaja)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReportes.Click
        MuestraForm(New MuestraReportes)
    End Sub


    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonArqueo.Click
        MuestraForm(New ArqueoCaja)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPagoComision.Click
        MuestraForm(New RExpress)
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReporteComisiones.Click
        MuestraForm(New rptComisiones)
    End Sub



    Private Sub Perfil_Usuario(ByVal Id_Usuario As String)
        ButtonSalones.Enabled = VerificandoAcceso_a_Modulos("Saloneros", "Salonero", Id_Usuario, "A&B")
        ButtonGrupoMenu.Enabled = VerificandoAcceso_a_Modulos("GrupoMesas", "Grupo de Mesas", Id_Usuario, "A&B")
        ButtonSecciones.Enabled = VerificandoAcceso_a_Modulos("SeccionesR", "Secciones de Mesas", Id_Usuario, "A&B")
        ButtonMesas.Enabled = VerificandoAcceso_a_Modulos("Mesas", "MESAS", Id_Usuario, "A&B")
        ButtonPagoComision.Enabled = VerificandoAcceso_a_Modulos("frmPagoComisiones", "Pago de Comisiones", Id_Usuario, "A&B")
        ButtonGrupoMenu.Enabled = VerificandoAcceso_a_Modulos("GrupoMenu", "Grupos de Menu", Id_Usuario, "A&B")
        ButtonCategorias.Enabled = VerificandoAcceso_a_Modulos("GruposMenu", "Categorías de Menu", Id_Usuario, "A&B")
        ButtonMenu.Enabled = VerificandoAcceso_a_Modulos("MenuI", "Ingreso del Menu", Id_Usuario, "A&B")
        ButtonRecetas.Enabled = VerificandoAcceso_a_Modulos("Receta", "Registros de Recetas", Id_Usuario, "A&B")
        ButtonModificadoresForzados.Enabled = VerificandoAcceso_a_Modulos("GCategorias", "Modificadores Forzados", Id_Usuario, "A&B")
        ButtonAcompañamientos.Enabled = VerificandoAcceso_a_Modulos("Acompanamiento", "Registro de Acompañamientos", Id_Usuario, "A&B")
        ButtonComandas.Enabled = VerificandoAcceso_a_Modulos("Comanda", "Comandas", Id_Usuario, "A&B")
        ButtonReportes.Enabled = VerificandoAcceso_a_Modulos("MuestraReportes", "Reportes de Ventas", Id_Usuario, "A&B")
        ButtonControlClientes.Enabled = VerificandoAcceso_a_Modulos("Cliente", "Registros de Clientes", Id_Usuario, "A&B")
        Call VerificandoAcceso_a_Modulos("MesaF", "", Id_Usuario, "A&B")
        ButtonApertura.Enabled = VerificandoAcceso_a_Modulos("AperturaCaja", "Apertura de Caja de Restaurante", Id_Usuario, "A&B")
        ButtonCierre.Enabled = VerificandoAcceso_a_Modulos("CierreCaja", "Cierre de Caja de Restaurante", Id_Usuario, "A&B")
        Call VerificandoAcceso_a_Modulos("ComandaMenu", "Comandas", Id_Usuario, "A&B")
        ButtonReporteComisiones.Enabled = VerificandoAcceso_a_Modulos("rptComisiones", "Reportes Pago Comisión", Id_Usuario, "A&B")
        ButtonArqueo.Enabled = VerificandoAcceso_a_Modulos("ArqueoCaja", "Arqueo Caja Restaurante", Id_Usuario, "A&B")

        Button6.Enabled = VerificandoAcceso_a_Modulos("FrmCapturaHorario", "Captura Horas", Id_Usuario, "A&B")
        Call VerificandoAcceso_a_Modulos("Relacion_Saloneros", "Relaciones Saloneros - Cajeros", Id_Usuario, "A&B")

        ButtonReporteComandas.Enabled = VerificandoAcceso_a_Modulos("FrmReporteComandas", "Reporte de Comandas", Id_Usuario, "A&B")
        B_Reporte_CostoMenu.Enabled = VerificandoAcceso_a_Modulos("FrmReporteMenu", "Reporte del Menu", Id_Usuario, "A&B")
        B_Cortesias.Enabled = VerificandoAcceso_a_Modulos("Cortesias", "Registro de Cortesía", Id_Usuario, "A&B")
        Me.bttBuffet.Enabled = VerificandoAcceso_a_Modulos("Requisicion", "Registro de Buffet", Id_Usuario, "A&B")

        Call VerificandoAcceso_a_Modulos("PlatosExpress", "Pedidos Express", Id_Usuario, "A&B")
    End Sub

    Private Sub BuscarFacturaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MuestraForm(New frmFacturaDesayunos)
    End Sub

    Private Sub Button7_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        MuestraForm(New FrmInHouse)
    End Sub

    Private Sub Button8_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Dim ReporteMenu As New ReportMENU
        CrystalReportsConexion.LoadShow(ReporteMenu, ParentForm, conectadobd.ConnectionString)
    End Sub

    Private Sub Button10_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Try
            Dim Usuario As New Frm_login("Seguridad")
            Usuario.ShowDialog()
            If Usuario.conectado Then
                User_Log = Usuario.Usuario
                conectadobd.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
                User_Log.ICT = cConexion.SlqExecuteScalar(conectadobd, "select Imp_ICT from Configuraciones")
                User_Log.IVI = cConexion.SlqExecuteScalar(conectadobd, "select Imp_Venta from Configuraciones")
                User_Log.ISS = cConexion.SlqExecuteScalar(conectadobd, "select Imp_Servicio from Configuraciones")
                User_Log.ISH = cConexion.SlqExecuteScalar(conectadobd, "select Imp_Servicio_Habitacion from Configuraciones")
                User_Log.SolicitarClaveMesa = cConexion.SlqExecuteScalar(conectadobd, "Select UnicoEncargadoXMesa FROM Configuraciones")
                Perfil_Usuario(Usuario.Usuario.Cedula)
                Text = "SEESOFT A&B PUNTO VENTA : " & User_Log.NombrePunto & " - " & "USUARIO: " & User_Log.Nombre
            Else
                End
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button11_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCierreViejo.Click
        MuestraForm(New frmInfoCajas)
    End Sub

    Private Sub ButtonReporteComandas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReporteComandas.Click
        Dim visor_reportes As New FrmReporteComandas
        visor_reportes.punto_venta = User_Log.PuntoVenta
        MuestraForm(visor_reportes)
    End Sub

    Private Sub ButtonControlClientes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonControlClientes.Click
        MuestraForm(New Cliente)
    End Sub

    Private Sub Button9_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        ''Dim Reporte As New Rrecetas
        ''Dim visor As New frmVisorReportes
        ''CrystalReportsConexion.LoadReportViewer(visor.rptViewer, Reporte, False, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
        'MuestraForm(New FrmRecetas)
        '  Me.imprimirPorDataSet()
        Dim cls As New clsReporteRecetas
        cls.imprimirPorDataSet()

    End Sub
    Sub imprimirPorDataSet()
        Dim ds As New DataSetRecetaImp
        Dim consultaReceta As String = "SELECT     ID, Nombre, Porciones, CostoTotal, CostoPorcion AS CostoPorciones " & _
                                        " FROM Recetas"
        cFunciones.Llenar_Tabla_Generico(consultaReceta, ds.Receta)

        Dim consultaDetalles As String = "Select * FROM Recetas_Detalles"
        cFunciones.Llenar_Tabla_Generico(consultaDetalles, ds.Recetas_Detalles)

        For j As Integer = 0 To ds.Receta.Count - 1

            ds.Receta(j).CostoPorciones = CalculoTotalReceta(ds.Receta(j).Id)
            ds.Receta(j).CostoTotal = ds.Receta(j).Porciones * ds.Receta(j).CostoPorciones

        Next

        For i As Integer = 0 To ds.Recetas_Detalles.Count - 1
            If ds.Recetas_Detalles(i).Articulo = False Then
                'Dim porciones As Double = 1
                'For h As Integer = 0 To ds.Receta.Count - 1
                '    If ds.Recetas_Detalles(i).IdReceta = ds.Receta(h).Id Then
                '        porciones = ds.Receta(h).Porciones
                '    End If
                'Next
                ds.Recetas_Detalles(i).CostoUnit = Me.CalculaCostoReceta(ds.Recetas_Detalles(i).Codigo)


                ds.Recetas_Detalles(i).CostoTotal = ds.Recetas_Detalles(i).CostoUnit * ds.Recetas_Detalles(i).Cantidad '* porciones

            Else
                ds.Recetas_Detalles(i).CostoUnit = Me.CalculaCostoArticulo(ds.Recetas_Detalles(i).Codigo, ds.Recetas_Detalles(i).idBodegaDescarga, ds.Recetas_Detalles(i).Descripcion, ds.Recetas_Detalles(i).IdReceta)
                ds.Recetas_Detalles(i).CostoTotal = ds.Recetas_Detalles(i).CostoUnit * ds.Recetas_Detalles(i).Cantidad

            End If

        Next
        Dim rpt As New CrystalReportRecetas

        rpt.SetDataSource(ds)
        rpt.SetParameterValue(0, False)
        rpt.SetParameterValue(1, 0)

        Dim visor As New frmVisorReportes
        visor.rptViewer.ReportSource = rpt
        visor.ShowDialog()
        visor.Dispose()
        ds = Nothing

    End Sub
    Private Function CalculaCostoReceta(ByVal codigo_receta As Integer) As Double
        Try
            Dim cnx As New SqlClient.SqlConnection
            Dim exe As New ConexionR
            Dim calculando_costo As Double
            Dim dsDetalle_RecetaAnidada As New DataSet
            Dim fila_receta As DataRow
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()

            exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Cantidad, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")
            For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows
                If fila_receta("Articulo") = 0 Then
                    calculando_costo += CalculaCostoReceta(fila_receta("Codigo"))
                Else
                    calculando_costo += (CalculaCostoArticulo(fila_receta("Codigo"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), fila_receta("idreceta")) * fila_receta("Cantidad"))
                End If
            Next
            Return calculando_costo
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function

    Private Function CalculaCostoArticulo(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal Receta As Integer) As Double
        Try
            Dim costoXarticulo As Double
            Dim exe As New ConexionR
            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()

            costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoUnit From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & "and IdReceta = " & Receta)
            If costoXarticulo = 0 Then
                'MsgBox("El Articulo " + nombre + " no se ha registrado en la bodega que se indico para descarga, se utilizará el precio base para el calculo de la receta", MsgBoxStyle.OkOnly, "Servicios Estructurales SeeSoft")
                costoXarticulo = 0
            End If
            Return costoXarticulo
            cnx.Close()
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function

    Function CalculoTotalReceta(ByVal Id_Receta As Integer) As Double
        Dim costo_enbodega As Double = 0
        Dim costo_total As Double = 0

        Dim Totales As Double = 0
        Dim especiesl As Double = 0
        Dim aprovechamientol As Double = 0
        Dim porciones As Double = 0
        Dim cnx As New SqlClient.SqlConnection
        cConexion.GetRecorset(conectadobd, "Select Aprovechamiento, Especies, Porciones From Recetas Where id =" & Id_Receta, rs)
        If rs.Read() Then
            especiesl = rs("Especies")
            aprovechamientol = rs("Aprovechamiento")
            porciones = rs("Porciones")

        End If
        rs.Close()
        'AQUI SE OBTIENEN LOS DATOS DE LA RECETA O DEL ARTICULO DE MENU QUE SE COMPONE LA RECETA
        cConexion.GetRecorset(conectadobd, "Select idreceta,codigo,descripcion,cantidad,costoUnit,costoTotal,UConversion,PUConversion,Articulo,Disminuye,idBodegaDescarga from Recetas_Detalles Where idreceta=" & Id_Receta, rs)
        While rs.Read
            If rs("Articulo") = True Then
                costo_enbodega = CalculaCostoArticulo(rs("codigo"), rs("idBodegaDescarga"), rs("descripcion"), rs("idreceta"))
                costo_total = costo_enbodega * rs("Cantidad")
            Else
                costo_enbodega = CalculaCostoReceta(rs("codigo"))
                costo_total = costo_enbodega * rs("Cantidad")
            End If
            If costo_enbodega = 0 Then
                costo_enbodega = rs("costoUnit")
                costo_total = rs("costoTotal")
            End If
            Totales += costo_total
            '--------------------------------------------------------------------

        End While
        rs.Close()

        Dim especiesD As Double = CDbl(Totales) * (CDbl(especiesl) / 100)
        Dim desaprovechamientoD As Double = Totales * (CDbl(aprovechamientol) / 100)
        Totales = (Totales + especiesD + desaprovechamientoD) / porciones

        Return Totales

    End Function
    Private Sub B_Reporte_CostoMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Reporte_CostoMenu.Click
        MuestraForm(New FrmReporteMenu)
    End Sub

    Private Sub B_Cortesias_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Cortesias.Click
        MuestraForm(New Cortesias)
    End Sub

    Private Sub ButtonFactura_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonFactura.Click
        Dim BuscarFacturas As New BuscarFactura
        Hide()
        BuscarFacturas.cedula = User_Log.Cedula
        BuscarFacturas.ShowDialog()
        BuscarFacturas.Dispose()
        Show()
    End Sub

    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bttBuffet.Click
        MuestraForm(New Requisicion)
    End Sub

    Private Sub ButtonFacturar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonFacturar.Click
        crearFactura()
    End Sub
	Sub crearFactura()
		Dim cedula1 As String = ""
		Dim idMesa As String = "0"
		Dim comenzales As String = "1"
		Dim numeroComanda As String = "0"
		Dim idGrupoConta As String = "0"
ReintentarClave:
		Dim rEmpleado0 As New registro
		rEmpleado0.Text = "DIGITE CONTRASEÑA"
		rEmpleado0.txtCodigo.PasswordChar = "*"
		'---------------------------------------------------------------
		'VERIFICA SI PIDE O NO EL USUARIO
		cConexion.DesConectar(cConexion.SqlConexion)
		Dim NoClave As Boolean = True 'cConexion.SlqExecuteScalar(cConexion.Conectar("Hotel"), "SELECT NoClave FROM Configuraciones")
		If NoClave Then
			clave = User_Log.Clave_Interna
			rEmpleado0.iOpcion = 1
		Else
			rEmpleado0.ShowDialog()
			clave = rEmpleado0.txtCodigo.Text
		End If
		cConexion.DesConectar(cConexion.SqlConexion)
		'---------------------------------------------------------------

		rEmpleado0.Dispose()
		If rEmpleado0.iOpcion = 0 Then Exit Sub

		If clave <> "" Then
			cedula1 = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Cedula from Usuarios WITH (NOLOCK) where Clave_Interna='" & clave & "'")
			If Trim(cedula1) = "" Or Trim(cedula1) = vbNullString Or rEmpleado0.iOpcion = 0 Then
				MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atención...")
				GoTo ReintentarClave
				Exit Sub
			End If
		Else
			GoTo ReintentarClave
		End If
		Dim iCategoria As Integer = 1
		Dim primera As Boolean = True
		While iCategoria = 1
			Dim platos As New PlatosExpress

			Hide()

			If usaGrupo = True Then
				Dim cGrupo As New GrupoPrincipal
				cGrupo.ShowDialog()
				idGrupoConta = cGrupo.idGrupo
				If cGrupo.idGrupo = -1 Then
					Show()
					Exit Sub
				End If
				cGrupo.Dispose()
				If primera Then
					numeroComanda = 0
					primera = False
				End If
			Else
				idGrupoConta = 0
			End If
			platos.TipoPedido = 1
			platos.idMesa = idMesa
			platos.comenzales = comenzales
			platos.numeroComanda = numeroComanda
			platos.nombreMesa = "Pedido Express"
			platos.idGrupo = idGrupoConta
			platos.usuario = cedula1
			platos.cCuentas = 0
			platos.ShowDialog()
			iCategoria = platos.iCategoria
			numeroComanda = platos.numeroComanda
			platos.Dispose()
			Show()


		End While
	End Sub

	Private Sub Button3_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        verListadoLlevar()
    End Sub
    Sub verListadoLlevar()
        Dim frm_Express As New FormCuentasALlevar
        Hide()
        'frm_Express.cedula = User_Log.Cedula
        frm_Express.ShowDialog()
        frm_Express.Dispose()
        Show()

    End Sub

    Private Sub Button5_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim frm_movimientocaja As New MovimientoCaja
        Hide()
        frm_movimientocaja.usua = User_Log
        frm_movimientocaja.ShowDialog()
        frm_movimientocaja.Dispose()
        Show()
    End Sub

    Private Sub Button6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        MuestraForm(New FrmCapturaHorario)
    End Sub

    Private Sub ButtonCxCR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAbreCaja.Click
        Dim frm As New frmQuestion
        frm.LblMensaje.Text = "Confirmar que desea abrir la caja"
        frm.ShowDialog()
        If frm.resultado = True Then
            valida()
        End If
    End Sub

    Private Sub valida()
        Dim FRM As New registro
        FRM.ShowDialog()
        If FRM.iOpcion = 1 Then
            Dim CLAVE = FRM.txtCodigo.Text
            Dim DT As New DataTable
            cFunciones.Llenar_Tabla_Generico("select * from usuarios where clave_interna = '" & CLAVE & "'", DT, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            If DT.Rows.Count > 0 Then
                Dim msg As String
                Do While msg = ""
                    msg = InputBox("", "Digite el Motivo de la Apertura")
                Loop
                Guarda(DT, msg)
                abre()
            Else
                MsgBox("Clave Incorrecta", MsgBoxStyle.Exclamation, Text)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub Guarda(ByVal dt As DataTable, ByVal motivo As String)
        Dim con As New Conexion
        Dim sql As New SqlClient.SqlConnection
        sql = con.Conectar()
        con.SlqExecute(sql, "insert into AbreCaja values('" & dt.Rows(0).Item("cedula") & "', '" & dt.Rows(0).Item("Nombre") & "', getdate(),'" & motivo & "')")
        con.DesConectar(sql)
    End Sub

    Private Sub abre()
        Dim puertocaja As String = GetSetting("Seesoft", "Restaurante", "PuertoCaja")
        Dim intFileNo As Integer = FreeFile()
        FileOpen(1, "c:\escapes.txt", OpenMode.Output)
        PrintLine(1, Chr(27) & "p" & Chr(0) & Chr(25) & Chr(250))
        FileClose(1)
        Shell("print /d:" & puertocaja & " c:\escapes.txt", vbNormalFocus)
    End Sub

    Private Sub ButtonCxCReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCxCReporte.Click
        MuestraForm(frmEstado_CXC)
    End Sub

    Private Sub BtnReporteHoras_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReporteHoras.Click
        MuestraForm(frm_HabiInhabi)
    End Sub

    Private Sub Button11_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        MuestraForm(frmcomandasximpre)
    End Sub

    Private Sub Button12_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        MuestraForm(frmFacturasAcumuladas)
    End Sub

    Private Sub btnSoporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Process.Start(".\Soporte\Soporte.exe")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub


    Private Sub btConfiguracionMovil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btConfiguracionMovil.Click
        MuestraForm(New frmConfiguracionCategoria)
    End Sub

    Private Sub ButtonApertura_Click(sender As Object, e As EventArgs) Handles ButtonApertura.Click
        MuestraForm(New AperturaCaja)
    End Sub

    Private Sub btUpdate_Click(sender As Object, e As EventArgs) Handles btUpdate.Click
        Dim ver As New clsVersion(My.Application.Deployment)
        ver.EsPosible = My.Application.IsNetworkDeployed
        Dim fr As New frmActualiza(ver)
        If fr.ShowDialog = Windows.Forms.DialogResult.OK Then
            Close()
        End If

    End Sub
    'Prueba sistema

    Private Sub Btn_ConfiAvanzada_Click(sender As Object, e As EventArgs) Handles Btn_ConfiAvanzada.Click
        Try
            PMU = VSM(User_Log.Cedula, "FrmConfiguracionAvanzada")
            If Not PMU.Execute Then
                MsgBox("No tiene permiso para ver esta información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            End If
            MuestraForm(New FrmConfiguracionAvanzada)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btnConfiguracionTamano.Click
		MuestraForm(New frmCambiarTamañoMenu)
	End Sub

    Private Sub btCentroProduccion_Click(sender As Object, e As EventArgs) Handles btCentroProduccionConfig.Click
        MuestraForm(New frmCentroProduccionConfig)
    End Sub

    Private Sub btCentroProduccionOperaciones_Click(sender As Object, e As EventArgs) Handles btCentroProduccionOperaciones.Click
        MuestraForm(New frmCentroProduccionOperaciones)
    End Sub

    Private Sub btCentroComandas_Click(sender As Object, e As EventArgs) Handles btCentroComandas.Click
        MuestraForm(New frmCentroComandas)
    End Sub

    Private Sub btRepartidores_Click(sender As Object, e As EventArgs) Handles btRepartidores.Click
        MuestraForm(New frmRepartidores)
    End Sub

    Private Sub btRepartidoresOp_Click(sender As Object, e As EventArgs) Handles btRepartidoresOp.Click
        MuestraForm(New frmRepartidoresFact)
    End Sub

    Private Sub btRptVarios_Click(sender As Object, e As EventArgs) Handles btRptVarios.Click
        MuestraForm(New frmReporteVarios)
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        Try
            Dim Configuracion As Integer
            Dim dtVerificaPermiso As New DataTable

            cFunciones.Llenar_Tabla_Generico("Select top 1 Permisos from conf", dtVerificaPermiso)

            If dtVerificaPermiso.Rows.Count > 0 Then
                Configuracion = dtVerificaPermiso.Rows(0).ItemArray(0)
            End If

            If Configuracion <> 0 Then
                PMU = VSM(User_Log.Cedula, "Configuracion")
                If TabControl1.SelectedIndex = 2 Or TabControl1.SelectedIndex = 3 Then
                    If Not PMU.Execute Then
                        MsgBox("No tiene permiso para ver esta información...", MsgBoxStyle.Information, "Atención...")
                        TabControl1.SelectedIndex = 0
                    End If
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class

#Region "LLamados de archivos externos"

Public Class WinApi
    ' Hace que una ventana sea hija (o esté contenida) en otra
    <System.Runtime.InteropServices.DllImport("user32.dll")> _
    Public Shared Function SetParent(ByVal hWndChild As IntPtr, _
    ByVal hWndNewParent As IntPtr) As IntPtr
    End Function
    ' Devuelve el Handle (hWnd) de una ventana de la que sabemos el título
    <System.Runtime.InteropServices.DllImport("user32.dll")> _
    Public Shared Function FindWindow(ByVal lpClassName As String, _
    ByVal lpWindowName As String) As IntPtr
    End Function
    ' Cambia el tamaño y la posición de una ventana
    <System.Runtime.InteropServices.DllImport("user32.dll")> _
    Public Shared Function MoveWindow(ByVal hWnd As IntPtr, _
    ByVal x As Integer, ByVal y As Integer, _
                                ByVal nWidth As Integer, ByVal nHeight As Integer, _
                                ByVal bRepaint As Integer) As Integer
    End Function
End Class
#End Region
