﻿Imports System.Data.SqlClient

Public Class frmEditar_NuevoRepartidor

    Public Id As Integer = 0


    Private Sub frmEditar_NuevoRepartidor_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtCedula.Select()
        CargarCbEstado()

        If Id <> 0 Then
            btCrear.Text = "Editar"
            Dim dtEditar As New DataTable()
            cFunciones.Llenar_Tabla_Generico("Select * from tb_Repartidores where Id=" & Id & "", dtEditar, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dtEditar.Rows.Count > 0 Then
                txtNombre.Text = dtEditar.Rows(0).Item("Nombre")
                txtApellidos.Text = dtEditar.Rows(0).Item("Apellidos")
                txtCedula.Text = dtEditar.Rows(0).Item("Cedula")
                txtTelefono.Text = dtEditar.Rows(0).Item("Telefono")
                txtCorreo.Text = dtEditar.Rows(0).Item("Correo")
                txtDireccion.Text = dtEditar.Rows(0).Item("Direccion")
                txtPlaca.Text = dtEditar.Rows(0).Item("Placa")
                txtInicial.Text = dtEditar.Rows(0).Item("Inicial")
                If dtEditar.Rows(0).Item("Estado") = "True" Then
                    cbEstado.SelectedIndex = 1
                Else
                    cbEstado.SelectedIndex = 0
                End If

            End If
        Else
            cbEstado.SelectedIndex = 1
        End If
    End Sub

    Private Sub btCancelar_Click(sender As Object, e As EventArgs) Handles btCancelar.Click
        Me.Close()
    End Sub

    Sub CargarCbEstado() 'Carga el combobox de estados
        Try
            cbEstado.Items.Insert(0, "Inactivo")
            cbEstado.Items.Insert(1, "Activo")
        Catch ex As Exception
            MsgBox("Error al cargar cb de Tipos. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Function Guardar() As Boolean 'funcion para guardar en la base de datos
        Try

            Dim cx As New Conexion
            Dim Sql As SqlCommand

            Dim dtVerifica As New DataTable
            dtVerifica.Clear()


            If Id <> 0 Then

                cFunciones.Llenar_Tabla_Generico("Select Id from Restaurante.dbo.tb_Repartidores where Id<>" & Id & " and Inicial='" & txtInicial.Text.ToUpper() & "' and Estado=1", dtVerifica)
                If dtVerifica.Rows.Count > 0 Then
                    MsgBox("Ya existe un repartidor con estas iniciales. ", MsgBoxStyle.Information)
                    Exit Function
                Else

                    Sql = New SqlCommand("Update tb_Repartidores set [Nombre]=@Nombre
           ,[Apellidos]=@Apellidos
           ,[Cedula]=@Cedula
           ,[Telefono]=@Telefono
           ,[Direccion]=@Direccion
           ,[Placa]=@Placa
           ,[Correo]=@Correo
           ,[Estado]=@Estado
           ,[Inicial]=@Inicial where Id=@Id")

                    Sql.Parameters.AddWithValue("@Nombre", txtNombre.Text)
                    Sql.Parameters.AddWithValue("@Apellidos", txtApellidos.Text)
                    Sql.Parameters.AddWithValue("@Cedula", txtCedula.Text)
                    Sql.Parameters.AddWithValue("@Telefono", txtTelefono.Text)
                    Sql.Parameters.AddWithValue("@Direccion", txtDireccion.Text)
                    Sql.Parameters.AddWithValue("@Placa", txtPlaca.Text)
                    Sql.Parameters.AddWithValue("@Correo", txtCorreo.Text)
                    Sql.Parameters.AddWithValue("@Estado", cbEstado.SelectedIndex)
                    Sql.Parameters.AddWithValue("@Inicial", txtInicial.Text.ToUpper())
                    Sql.Parameters.AddWithValue("@Id", Id)
                End If
            Else

                cFunciones.Llenar_Tabla_Generico("Select Id from Restaurante.dbo.tb_Repartidores where Inicial='" & txtInicial.Text.ToUpper() & "' and Estado=1", dtVerifica)
                If dtVerifica.Rows.Count > 0 Then
                    MsgBox("Ya existe un repartidor con estas iniciales. ", MsgBoxStyle.Information)
                    Exit Function
                Else

                    Sql = New SqlCommand("INSERT INTO tb_Repartidores([Nombre]
           ,[Apellidos]
           ,[Cedula]
           ,[Telefono]
           ,[Correo]
           ,[Direccion]
           ,[Placa]
           ,[Estado]
            ,[Inicial]) VALUES (@Nombre,@Apellidos,@Cedula,@Telefono,@Correo,@Direccion,@Placa,@Estado,@Inicial)")

                    Sql.Parameters.AddWithValue("@Nombre", txtNombre.Text)
                    Sql.Parameters.AddWithValue("@Apellidos", txtApellidos.Text)
                    Sql.Parameters.AddWithValue("@Cedula", txtCedula.Text)
                    Sql.Parameters.AddWithValue("@Telefono", txtTelefono.Text)
                    Sql.Parameters.AddWithValue("@Direccion", txtDireccion.Text)
                    Sql.Parameters.AddWithValue("@Placa", txtPlaca.Text)
                    Sql.Parameters.AddWithValue("@Correo", txtCorreo.Text)
                    Sql.Parameters.AddWithValue("@Estado", cbEstado.SelectedIndex)
                    Sql.Parameters.AddWithValue("@Inicial", txtInicial.Text.ToUpper())
                End If
            End If




            If ValidarCampos() Then
                cFunciones.spEjecutar(Sql, GetSetting("SeeSOFT", "Restaurante", "CONEXION"))
                cx.DesConectar(cx.sQlconexion)
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Error en procedimiento de guardado. " & ex.Message, MsgBoxStyle.Information)
            Return False
        End Try
    End Function

    Function ValidarCampos() As Boolean
        Try
            Dim Validador As Integer = 0

            If txtNombre.Text.Trim().Equals("") Then
                Validador = 1
            ElseIf txtApellidos.Text.Trim().Equals("") Then
                Validador = 2
            ElseIf txtCedula.Text.Trim().Equals("") Then
                Validador = 3
            ElseIf txtTelefono.Text.Trim().Equals("") Then
                Validador = 4
            ElseIf txtInicial.Text.Trim().Equals("") Then
                Validador = 5
            End If

            Select Case Validador
                Case 1
                    MsgBox("El nombre es obligatorio.", MsgBoxStyle.Information)
                Case 2
                    MsgBox("El apelleido es obligatorio.", MsgBoxStyle.Information)
                Case 3
                    MsgBox("La cédula  es obligatorio.", MsgBoxStyle.Information)
                Case 4
                    MsgBox("El teléfono es obligatorio.", MsgBoxStyle.Information)
                Case 5
                    MsgBox("El campo Inicial es requerido.", MsgBoxStyle.Information)
            End Select

            If Validador <> 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            MsgBox("Error al validar campos obligatorios. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Function
    Private Sub btCrear_Click(sender As Object, e As EventArgs) Handles btCrear.Click

        Try
            If Guardar() Then
                MsgBox("La infomación se guardo correctamente", MsgBoxStyle.Information)
                Me.Close()
            End If

        Catch ex As Exception
            MsgBox("La infomación NO se guardo correctamente. " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub txtTelefono_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTelefono.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not e.KeyChar = "-"
    End Sub

    Private Sub txtCedula_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCedula.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar)
    End Sub
End Class