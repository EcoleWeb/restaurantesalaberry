﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmRepartidores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvRepartidores = New System.Windows.Forms.DataGridView()
        Me.btNuevo = New System.Windows.Forms.Button()
        Me.btEditar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbInactivos = New System.Windows.Forms.RadioButton()
        Me.rbActivos = New System.Windows.Forms.RadioButton()
        Me.btAsignar = New System.Windows.Forms.Button()
        CType(Me.dgvRepartidores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtBuscar
        '
        Me.txtBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscar.Location = New System.Drawing.Point(98, 19)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(504, 26)
        Me.txtBuscar.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(22, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Buscar:"
        '
        'dgvRepartidores
        '
        Me.dgvRepartidores.AllowUserToAddRows = False
        Me.dgvRepartidores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvRepartidores.BackgroundColor = System.Drawing.Color.White
        Me.dgvRepartidores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRepartidores.Location = New System.Drawing.Point(26, 74)
        Me.dgvRepartidores.MultiSelect = False
        Me.dgvRepartidores.Name = "dgvRepartidores"
        Me.dgvRepartidores.ReadOnly = True
        Me.dgvRepartidores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRepartidores.Size = New System.Drawing.Size(749, 257)
        Me.dgvRepartidores.TabIndex = 2
        '
        'btNuevo
        '
        Me.btNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btNuevo.BackColor = System.Drawing.Color.LightSkyBlue
        Me.btNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btNuevo.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btNuevo.Location = New System.Drawing.Point(159, 337)
        Me.btNuevo.Name = "btNuevo"
        Me.btNuevo.Size = New System.Drawing.Size(84, 40)
        Me.btNuevo.TabIndex = 4
        Me.btNuevo.Text = "Nuevo"
        Me.btNuevo.UseVisualStyleBackColor = False
        '
        'btEditar
        '
        Me.btEditar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btEditar.BackColor = System.Drawing.Color.DimGray
        Me.btEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btEditar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btEditar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btEditar.Location = New System.Drawing.Point(555, 337)
        Me.btEditar.Name = "btEditar"
        Me.btEditar.Size = New System.Drawing.Size(84, 40)
        Me.btEditar.TabIndex = 5
        Me.btEditar.Text = "Editar"
        Me.btEditar.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbInactivos)
        Me.GroupBox1.Controls.Add(Me.rbActivos)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.GroupBox1.Location = New System.Drawing.Point(608, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(167, 56)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Estados"
        '
        'rbInactivos
        '
        Me.rbInactivos.AutoSize = True
        Me.rbInactivos.Location = New System.Drawing.Point(79, 24)
        Me.rbInactivos.Name = "rbInactivos"
        Me.rbInactivos.Size = New System.Drawing.Size(81, 17)
        Me.rbInactivos.TabIndex = 1
        Me.rbInactivos.Text = "Inactivos."
        Me.rbInactivos.UseVisualStyleBackColor = True
        '
        'rbActivos
        '
        Me.rbActivos.AutoSize = True
        Me.rbActivos.Checked = True
        Me.rbActivos.Location = New System.Drawing.Point(7, 24)
        Me.rbActivos.Name = "rbActivos"
        Me.rbActivos.Size = New System.Drawing.Size(71, 17)
        Me.rbActivos.TabIndex = 0
        Me.rbActivos.TabStop = True
        Me.rbActivos.Text = "Activos."
        Me.rbActivos.UseVisualStyleBackColor = True
        '
        'btAsignar
        '
        Me.btAsignar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btAsignar.BackColor = System.Drawing.Color.Tomato
        Me.btAsignar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btAsignar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAsignar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btAsignar.Location = New System.Drawing.Point(347, 337)
        Me.btAsignar.Name = "btAsignar"
        Me.btAsignar.Size = New System.Drawing.Size(84, 40)
        Me.btAsignar.TabIndex = 7
        Me.btAsignar.Text = "Asignar"
        Me.btAsignar.UseVisualStyleBackColor = False
        '
        'frmRepartidores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkGray
        Me.ClientSize = New System.Drawing.Size(800, 386)
        Me.Controls.Add(Me.btAsignar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btEditar)
        Me.Controls.Add(Me.btNuevo)
        Me.Controls.Add(Me.dgvRepartidores)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtBuscar)
        Me.MaximizeBox = False
        Me.Name = "frmRepartidores"
        Me.Text = "Administrador de Repartidores"
        CType(Me.dgvRepartidores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtBuscar As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btNuevo As Button
    Friend WithEvents btEditar As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rbInactivos As RadioButton
    Friend WithEvents rbActivos As RadioButton
    Friend WithEvents btAsignar As Button
    Friend WithEvents dgvRepartidores As DataGridView
End Class
