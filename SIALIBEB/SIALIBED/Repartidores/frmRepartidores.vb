﻿Imports System.Data.SqlClient

Public Class frmRepartidores

    Dim FiltroEstado As Integer = 1
    Public QuienMeLlama As String = ""
    Public IdFactura As Integer = 0
    Dim _AsignarRepartidor As Integer = 0
    Dim _IdRepartidor As Integer = 0

    Private Sub frmRepartidores_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If QuienMeLlama = "Asignar" Then
            btAsignar.Visible = True
            _AsignarRepartidor = ValidarRepartidor()
        Else
            btAsignar.Visible = False
        End If
        CargarGrid()

    End Sub

    Private Sub MuestraForm(ByRef FormDialog As Form)
        Hide()
        FormDialog.ShowDialog(Me)
        CargarGrid()
        Show()
    End Sub

    Sub CargarGrid()
        Try
            Dim dt As New DataTable()

            cFunciones.Llenar_Tabla_Generico("Select Id as 'Interno', Nombre + ' '+Apellidos as 'Nombre',Inicial as 'Código', Cedula as 'Cédula', Telefono as 'Teléfono', Estado from tb_Repartidores where Estado=1", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then
                dgvRepartidores.DataSource = dt
                dgvRepartidores.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            Else
                MsgBox("No hay repartidores para mostrar.", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox("Error al cargar datos. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub BuscarRepartidor()
        Try

            Dim consulta As String = ""

            If txtBuscar.Text.Trim() = "" Then
                consulta = "Select Id as 'Interno', Nombre + ' '+Apellidos as 'Nombre',Inicial as 'Código', Cedula as 'Cédula', Telefono as 'Teléfono', Estado from tb_Repartidores where  Estado=" & FiltroEstado & ""
            Else
                consulta = "Select Id as 'Interno', Nombre + ' '+Apellidos as 'Nombre', Inicial as 'Código', Cedula as 'Cédula', Telefono as 'Teléfono', Estado from tb_Repartidores where (Apellidos like '%" & txtBuscar.Text & "%' or Nombre like '%" & txtBuscar.Text & "%' or Cedula like'%" & txtBuscar.Text & "%') and Estado=" & FiltroEstado & ""
            End If

            Dim dtBuscar As New DataTable()

            cFunciones.Llenar_Tabla_Generico(consulta, dtBuscar, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dtBuscar.Rows.Count > 0 Then
                dgvRepartidores.DataSource = dtBuscar
            End If

        Catch ex As Exception
            MsgBox("Error al buscar repartidor. " & +ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


    Private Sub btNuevo_Click(sender As Object, e As EventArgs) Handles btNuevo.Click
        MuestraForm(New frmEditar_NuevoRepartidor)
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        BuscarRepartidor()
    End Sub

    Private Sub btEditar_Click(sender As Object, e As EventArgs) Handles btEditar.Click
        Try
            If dgvRepartidores.SelectedRows.Count > 0 Then
                Dim Id As Integer = dgvRepartidores.CurrentRow.Cells(0).Value
                Dim frmNuevo As New frmEditar_NuevoRepartidor
                frmNuevo.Id = Id
                MuestraForm(frmNuevo)
            Else
                MsgBox("Selecciona una fila para editar la información.", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox("Error al seleccionar repartidor. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub rbActivos_CheckedChanged(sender As Object, e As EventArgs) Handles rbActivos.CheckedChanged
        If rbActivos.Checked Then
            FiltroEstado = 1
            BuscarRepartidor()
        End If
    End Sub

    Private Sub rbInactivos_CheckedChanged(sender As Object, e As EventArgs) Handles rbInactivos.CheckedChanged
        If rbInactivos.Checked Then
            FiltroEstado = 0
            BuscarRepartidor()
        End If
    End Sub

    Private Sub btAsignar_Click(sender As Object, e As EventArgs) Handles btAsignar.Click
        AsignarRepartidor()
    End Sub

    Function ValidarRepartidor() As Integer
        Try
            Dim dt As New DataTable
            'Dim Id As Integer = dgvRepartidores.CurrentRow.Cells(0).Value

            dt.Clear()

            cFunciones.Llenar_Tabla_Generico("Select IdRepartidor, Nombre +' '+Apellidos as Nombre from tb_RepartidorAsignado left outer join tb_Repartidores on tb_RepartidorAsignado.IdRepartidor=tb_Repartidores.Id where  tb_RepartidorAsignado.IdFactura=" & IdFactura & " ", dt)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("IdRepartidor") = "0" Then
                    Return 1
                Else
                    _IdRepartidor = dt.Rows(0).Item("IdRepartidor")
                    Dim result As DialogResult = MessageBox.Show("Este pedido ya lo tiene asignado el repartidor (" & dt.Rows(0).Item("Nombre") & "). Desea reemplazar el repartidor?", "Atención...",
                                  MessageBoxButtons.YesNo)

                    If (result = DialogResult.Yes) Then
                        Return 1
                    Else
                        Me.Close()
                    End If
                End If
            Else
                Return 0
            End If
        Catch ex As Exception
            MsgBox("Error al validar repartidor. " & ex.Message, MsgBoxStyle.Critical)
            Return 2
        End Try
    End Function

    Sub AsignarRepartidor()
        Try

            Dim sql As SqlCommand
            Dim Correcto As Integer = 0

            If dgvRepartidores.SelectedRows.Count > 0 Then

                Dim Id As Integer = dgvRepartidores.CurrentRow.Cells(0).Value

                Select Case _AsignarRepartidor
                    Case 0
                        sql = New SqlCommand("Insert into tb_RepartidorAsignado (IdRepartidor,IdFactura,FechaAsignacion) values (@IdRepartidor,@IdFactura,GETDATE())")

                        sql.Parameters.AddWithValue("@IdRepartidor", Id)
                        sql.Parameters.AddWithValue("@IdFactura", IdFactura)

                        cFunciones.spEjecutar(sql, GetSetting("SeeSOFT", "Restaurante", "CONEXION"))
                        Correcto = 1
                    Case 1

                        sql = New SqlCommand("Update tb_RepartidorAsignado set IdRepartidor=@NuevoRepartidor where  IdFactura=@IdFactura")

                        sql.Parameters.AddWithValue("@IdFactura", IdFactura)
                        sql.Parameters.AddWithValue("@NuevoRepartidor", Id)

                        cFunciones.spEjecutar(sql, GetSetting("SeeSOFT", "Restaurante", "CONEXION"))
                        Correcto = 1
                    Case 2
                        Me.Close()
                End Select

                If Correcto = 1 Then
                    MsgBox("Repartidor asignado correctamente.", MsgBoxStyle.Information)
                    Dim result As DialogResult = MessageBox.Show("Deseas cambiar el estado de la factura a pagada?", "Atención...",
                              MessageBoxButtons.YesNo)

                    If (result = DialogResult.Yes) Then
                        CancelarFactura(IdFactura)
                        Me.Close()
                    Else
                        Me.Close()
                    End If

                Else
                    MsgBox("No se realizo ninguna acción.", MsgBoxStyle.Information)
                End If

            Else
                MsgBox("Selecciona una fila para asignar un repartidor. ", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox("Error al seleccionar repartidor. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub CancelarFactura(ByVal Factura As Integer)
        Try
            Dim cx As New Conexion
            Dim Sql As New SqlCommand("Update tb_RepartidorAsignado set Estado=1 where IdFactura=@NumFactura")

            Sql.Parameters.AddWithValue("@NumFactura", Factura)

            cFunciones.spEjecutar(Sql, GetSetting("SeeSOFT", "Restaurante", "CONEXION"))
            cx.DesConectar(cx.sQlconexion)

        Catch ex As Exception
            MsgBox("Error al cambiar de estado a cancelada la factura. " & ex.Message)
        End Try
    End Sub
End Class