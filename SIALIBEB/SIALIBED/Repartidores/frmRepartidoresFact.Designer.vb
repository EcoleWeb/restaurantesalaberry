﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmRepartidoresFact
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.dgvFacturas = New System.Windows.Forms.DataGridView()
        Me.bsFacturas = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtsRepartidores1 = New SIALIBEB.dtsRepartidores()
        Me.btBuscar = New System.Windows.Forms.Button()
        Me.btDetalle = New System.Windows.Forms.Button()
        Me.btHoy = New System.Windows.Forms.Button()
        Me.btAyer = New System.Windows.Forms.Button()
        Me.btSemanaPadada = New System.Windows.Forms.Button()
        Me.txtTotalGeneral = New System.Windows.Forms.TextBox()
        Me.txtTotalFlete = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btReporte = New System.Windows.Forms.Button()
        Me.btPagar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lbContador = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTotalSinpeTarSinFlete = New System.Windows.Forms.TextBox()
        Me.ckMarcar = New System.Windows.Forms.CheckBox()
        Me.txtTotalCobrar = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtIgreso = New System.Windows.Forms.Label()
        Me.txtTotalIngreso = New System.Windows.Forms.TextBox()
        Me.txtSinpeConFlete = New System.Windows.Forms.TextBox()
        Me.lbsinpe = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTarConFlete = New System.Windows.Forms.TextBox()
        Me.txtEfectivo = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCreditoConFlete = New System.Windows.Forms.TextBox()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDataGridViewTextBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.Inicial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FacturaDataGridViewTextBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.ClienteDataGridViewTextBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.DireccionDataGridViewTextBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.RepartidorDataGridViewTextBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.TotalSinFleteDataGridViewTextBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.FleteDataGridViewTextBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.TotalGeneralDataGridViewTextBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.EstadoDataGridViewCheckBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.PagoDataGridViewTextBoxColumn = New DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn()
        Me.IVADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsRepartidores1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(12, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(200, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre del repartidor:"
        '
        'txtBuscar
        '
        Me.txtBuscar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscar.Location = New System.Drawing.Point(218, 17)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(214, 26)
        Me.txtBuscar.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.dtpHasta)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dtpDesde)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(442, -2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(370, 55)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Entre Fechas"
        '
        'dtpHasta
        '
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(258, 23)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(106, 23)
        Me.dtpHasta.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(193, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 24)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Hasta:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(6, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 24)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Desde:"
        '
        'dtpDesde
        '
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(82, 22)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(105, 23)
        Me.dtpDesde.TabIndex = 0
        '
        'dgvFacturas
        '
        Me.dgvFacturas.AllowUserToAddRows = False
        Me.dgvFacturas.AllowUserToDeleteRows = False
        Me.dgvFacturas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvFacturas.AutoGenerateColumns = False
        Me.dgvFacturas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvFacturas.BackgroundColor = System.Drawing.Color.White
        Me.dgvFacturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFacturas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.FechaDataGridViewTextBoxColumn, Me.Inicial, Me.FacturaDataGridViewTextBoxColumn, Me.ClienteDataGridViewTextBoxColumn, Me.DireccionDataGridViewTextBoxColumn, Me.RepartidorDataGridViewTextBoxColumn, Me.TotalSinFleteDataGridViewTextBoxColumn, Me.FleteDataGridViewTextBoxColumn, Me.TotalGeneralDataGridViewTextBoxColumn, Me.EstadoDataGridViewCheckBoxColumn, Me.Estado, Me.PagoDataGridViewTextBoxColumn, Me.IVADataGridViewTextBoxColumn})
        Me.dgvFacturas.DataSource = Me.bsFacturas
        Me.dgvFacturas.Location = New System.Drawing.Point(16, 103)
        Me.dgvFacturas.Name = "dgvFacturas"
        Me.dgvFacturas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvFacturas.Size = New System.Drawing.Size(892, 223)
        Me.dgvFacturas.TabIndex = 3
        '
        'bsFacturas
        '
        Me.bsFacturas.DataMember = "dtFacturas"
        Me.bsFacturas.DataSource = Me.DtsRepartidores1
        '
        'DtsRepartidores1
        '
        Me.DtsRepartidores1.DataSetName = "dtsRepartidores"
        Me.DtsRepartidores1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btBuscar
        '
        Me.btBuscar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btBuscar.BackColor = System.Drawing.Color.DimGray
        Me.btBuscar.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.btBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btBuscar.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btBuscar.FlatAppearance.BorderSize = 0
        Me.btBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btBuscar.ForeColor = System.Drawing.Color.White
        Me.btBuscar.Location = New System.Drawing.Point(823, 10)
        Me.btBuscar.Name = "btBuscar"
        Me.btBuscar.Size = New System.Drawing.Size(84, 38)
        Me.btBuscar.TabIndex = 4
        Me.btBuscar.Text = "Buscar"
        Me.btBuscar.UseVisualStyleBackColor = False
        '
        'btDetalle
        '
        Me.btDetalle.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btDetalle.BackColor = System.Drawing.Color.DimGray
        Me.btDetalle.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.btDetalle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btDetalle.FlatAppearance.BorderSize = 0
        Me.btDetalle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btDetalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btDetalle.ForeColor = System.Drawing.Color.White
        Me.btDetalle.Location = New System.Drawing.Point(16, 362)
        Me.btDetalle.Name = "btDetalle"
        Me.btDetalle.Size = New System.Drawing.Size(108, 35)
        Me.btDetalle.TabIndex = 5
        Me.btDetalle.Text = "Ver Detalle"
        Me.btDetalle.UseVisualStyleBackColor = False
        Me.btDetalle.Visible = False
        '
        'btHoy
        '
        Me.btHoy.BackColor = System.Drawing.Color.DimGray
        Me.btHoy.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.btHoy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btHoy.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btHoy.FlatAppearance.BorderSize = 0
        Me.btHoy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btHoy.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btHoy.ForeColor = System.Drawing.Color.White
        Me.btHoy.Location = New System.Drawing.Point(16, 59)
        Me.btHoy.Name = "btHoy"
        Me.btHoy.Size = New System.Drawing.Size(84, 38)
        Me.btHoy.TabIndex = 6
        Me.btHoy.Text = "Hoy"
        Me.btHoy.UseVisualStyleBackColor = False
        '
        'btAyer
        '
        Me.btAyer.BackColor = System.Drawing.Color.DimGray
        Me.btAyer.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.btAyer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btAyer.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btAyer.FlatAppearance.BorderSize = 0
        Me.btAyer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btAyer.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAyer.ForeColor = System.Drawing.Color.White
        Me.btAyer.Location = New System.Drawing.Point(109, 59)
        Me.btAyer.Name = "btAyer"
        Me.btAyer.Size = New System.Drawing.Size(84, 38)
        Me.btAyer.TabIndex = 7
        Me.btAyer.Text = "Ayer"
        Me.btAyer.UseVisualStyleBackColor = False
        '
        'btSemanaPadada
        '
        Me.btSemanaPadada.BackColor = System.Drawing.Color.DimGray
        Me.btSemanaPadada.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.btSemanaPadada.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btSemanaPadada.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btSemanaPadada.FlatAppearance.BorderSize = 0
        Me.btSemanaPadada.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btSemanaPadada.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSemanaPadada.ForeColor = System.Drawing.Color.White
        Me.btSemanaPadada.Location = New System.Drawing.Point(202, 59)
        Me.btSemanaPadada.Name = "btSemanaPadada"
        Me.btSemanaPadada.Size = New System.Drawing.Size(165, 38)
        Me.btSemanaPadada.TabIndex = 8
        Me.btSemanaPadada.Text = "Semana Pasada"
        Me.btSemanaPadada.UseVisualStyleBackColor = False
        '
        'txtTotalGeneral
        '
        Me.txtTotalGeneral.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalGeneral.Enabled = False
        Me.txtTotalGeneral.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalGeneral.Location = New System.Drawing.Point(773, 329)
        Me.txtTotalGeneral.Name = "txtTotalGeneral"
        Me.txtTotalGeneral.Size = New System.Drawing.Size(135, 26)
        Me.txtTotalGeneral.TabIndex = 11
        Me.txtTotalGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalFlete
        '
        Me.txtTotalFlete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalFlete.Enabled = False
        Me.txtTotalFlete.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalFlete.Location = New System.Drawing.Point(773, 361)
        Me.txtTotalFlete.Name = "txtTotalFlete"
        Me.txtTotalFlete.Size = New System.Drawing.Size(135, 26)
        Me.txtTotalFlete.TabIndex = 12
        Me.txtTotalFlete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(640, 364)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 20)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Total Flete:"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(640, 332)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(123, 20)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Total General:"
        '
        'btReporte
        '
        Me.btReporte.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btReporte.BackColor = System.Drawing.Color.DimGray
        Me.btReporte.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.btReporte.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btReporte.FlatAppearance.BorderSize = 0
        Me.btReporte.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btReporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btReporte.ForeColor = System.Drawing.Color.White
        Me.btReporte.Location = New System.Drawing.Point(16, 448)
        Me.btReporte.Name = "btReporte"
        Me.btReporte.Size = New System.Drawing.Size(118, 35)
        Me.btReporte.TabIndex = 15
        Me.btReporte.Text = "Ver Reporte"
        Me.btReporte.UseVisualStyleBackColor = False
        '
        'btPagar
        '
        Me.btPagar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btPagar.BackColor = System.Drawing.Color.DimGray
        Me.btPagar.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.btPagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btPagar.FlatAppearance.BorderSize = 0
        Me.btPagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btPagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btPagar.ForeColor = System.Drawing.Color.Gold
        Me.btPagar.Location = New System.Drawing.Point(153, 448)
        Me.btPagar.Name = "btPagar"
        Me.btPagar.Size = New System.Drawing.Size(108, 35)
        Me.btPagar.TabIndex = 18
        Me.btPagar.Text = "Pagar"
        Me.btPagar.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(12, 407)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 24)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Facturas:"
        '
        'lbContador
        '
        Me.lbContador.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lbContador.AutoSize = True
        Me.lbContador.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbContador.ForeColor = System.Drawing.Color.White
        Me.lbContador.Location = New System.Drawing.Point(124, 407)
        Me.lbContador.Name = "lbContador"
        Me.lbContador.Size = New System.Drawing.Size(20, 24)
        Me.lbContador.TabIndex = 22
        Me.lbContador.Text = "0"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(640, 400)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 20)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "Rebajos:"
        '
        'txtTotalSinpeTarSinFlete
        '
        Me.txtTotalSinpeTarSinFlete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalSinpeTarSinFlete.Enabled = False
        Me.txtTotalSinpeTarSinFlete.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalSinpeTarSinFlete.Location = New System.Drawing.Point(772, 394)
        Me.txtTotalSinpeTarSinFlete.Name = "txtTotalSinpeTarSinFlete"
        Me.txtTotalSinpeTarSinFlete.Size = New System.Drawing.Size(135, 26)
        Me.txtTotalSinpeTarSinFlete.TabIndex = 24
        Me.txtTotalSinpeTarSinFlete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ckMarcar
        '
        Me.ckMarcar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ckMarcar.AutoSize = True
        Me.ckMarcar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.ckMarcar.Location = New System.Drawing.Point(773, 73)
        Me.ckMarcar.Name = "ckMarcar"
        Me.ckMarcar.Size = New System.Drawing.Size(86, 17)
        Me.ckMarcar.TabIndex = 25
        Me.ckMarcar.Text = "Marcar todo."
        Me.ckMarcar.UseVisualStyleBackColor = True
        '
        'txtTotalCobrar
        '
        Me.txtTotalCobrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalCobrar.Enabled = False
        Me.txtTotalCobrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCobrar.Location = New System.Drawing.Point(772, 427)
        Me.txtTotalCobrar.Name = "txtTotalCobrar"
        Me.txtTotalCobrar.Size = New System.Drawing.Size(135, 26)
        Me.txtTotalCobrar.TabIndex = 26
        Me.txtTotalCobrar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Yellow
        Me.Label7.Location = New System.Drawing.Point(640, 430)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 20)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "T. Cobrar:"
        '
        'txtIgreso
        '
        Me.txtIgreso.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtIgreso.AutoSize = True
        Me.txtIgreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIgreso.ForeColor = System.Drawing.Color.White
        Me.txtIgreso.Location = New System.Drawing.Point(324, 460)
        Me.txtIgreso.Name = "txtIgreso"
        Me.txtIgreso.Size = New System.Drawing.Size(95, 20)
        Me.txtIgreso.TabIndex = 35
        Me.txtIgreso.Text = "T. Ingreso:"
        '
        'txtTotalIngreso
        '
        Me.txtTotalIngreso.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalIngreso.Enabled = False
        Me.txtTotalIngreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalIngreso.Location = New System.Drawing.Point(491, 457)
        Me.txtTotalIngreso.Name = "txtTotalIngreso"
        Me.txtTotalIngreso.Size = New System.Drawing.Size(135, 26)
        Me.txtTotalIngreso.TabIndex = 34
        Me.txtTotalIngreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSinpeConFlete
        '
        Me.txtSinpeConFlete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSinpeConFlete.Enabled = False
        Me.txtSinpeConFlete.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSinpeConFlete.Location = New System.Drawing.Point(491, 395)
        Me.txtSinpeConFlete.Name = "txtSinpeConFlete"
        Me.txtSinpeConFlete.Size = New System.Drawing.Size(135, 26)
        Me.txtSinpeConFlete.TabIndex = 33
        Me.txtSinpeConFlete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbsinpe
        '
        Me.lbsinpe.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbsinpe.AutoSize = True
        Me.lbsinpe.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbsinpe.ForeColor = System.Drawing.Color.White
        Me.lbsinpe.Location = New System.Drawing.Point(324, 401)
        Me.lbsinpe.Name = "lbsinpe"
        Me.lbsinpe.Size = New System.Drawing.Size(155, 20)
        Me.lbsinpe.TabIndex = 32
        Me.lbsinpe.Text = "T. Sinpe con flete:"
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(324, 333)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(99, 20)
        Me.Label11.TabIndex = 31
        Me.Label11.Text = "T. Efectivo:"
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(324, 365)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(165, 20)
        Me.Label12.TabIndex = 30
        Me.Label12.Text = "T. Tarjeta con flete:"
        '
        'txtTarConFlete
        '
        Me.txtTarConFlete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTarConFlete.Enabled = False
        Me.txtTarConFlete.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTarConFlete.Location = New System.Drawing.Point(492, 362)
        Me.txtTarConFlete.Name = "txtTarConFlete"
        Me.txtTarConFlete.Size = New System.Drawing.Size(135, 26)
        Me.txtTarConFlete.TabIndex = 29
        Me.txtTarConFlete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtEfectivo
        '
        Me.txtEfectivo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEfectivo.Enabled = False
        Me.txtEfectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEfectivo.Location = New System.Drawing.Point(492, 330)
        Me.txtEfectivo.Name = "txtEfectivo"
        Me.txtEfectivo.Size = New System.Drawing.Size(135, 26)
        Me.txtEfectivo.TabIndex = 28
        Me.txtEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(324, 430)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(167, 20)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "T. Credito con flete:"
        '
        'txtCreditoConFlete
        '
        Me.txtCreditoConFlete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCreditoConFlete.Enabled = False
        Me.txtCreditoConFlete.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCreditoConFlete.Location = New System.Drawing.Point(491, 426)
        Me.txtCreditoConFlete.Name = "txtCreditoConFlete"
        Me.txtCreditoConFlete.Size = New System.Drawing.Size(135, 26)
        Me.txtCreditoConFlete.TabIndex = 37
        Me.txtCreditoConFlete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'FechaDataGridViewTextBoxColumn
        '
        Me.FechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.FechaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle1
        Me.FechaDataGridViewTextBoxColumn.HeaderText = "Fecha"
        Me.FechaDataGridViewTextBoxColumn.Name = "FechaDataGridViewTextBoxColumn"
        Me.FechaDataGridViewTextBoxColumn.ReadOnly = True
        Me.FechaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Inicial
        '
        Me.Inicial.DataPropertyName = "Inicial"
        Me.Inicial.HeaderText = "Codigo"
        Me.Inicial.Name = "Inicial"
        Me.Inicial.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Inicial.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'FacturaDataGridViewTextBoxColumn
        '
        Me.FacturaDataGridViewTextBoxColumn.DataPropertyName = "Factura"
        Me.FacturaDataGridViewTextBoxColumn.HeaderText = "Factura"
        Me.FacturaDataGridViewTextBoxColumn.Name = "FacturaDataGridViewTextBoxColumn"
        Me.FacturaDataGridViewTextBoxColumn.ReadOnly = True
        Me.FacturaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'ClienteDataGridViewTextBoxColumn
        '
        Me.ClienteDataGridViewTextBoxColumn.DataPropertyName = "Cliente"
        Me.ClienteDataGridViewTextBoxColumn.HeaderText = "Cliente"
        Me.ClienteDataGridViewTextBoxColumn.Name = "ClienteDataGridViewTextBoxColumn"
        Me.ClienteDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClienteDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DireccionDataGridViewTextBoxColumn
        '
        Me.DireccionDataGridViewTextBoxColumn.DataPropertyName = "Direccion"
        Me.DireccionDataGridViewTextBoxColumn.HeaderText = "Direccion"
        Me.DireccionDataGridViewTextBoxColumn.Name = "DireccionDataGridViewTextBoxColumn"
        Me.DireccionDataGridViewTextBoxColumn.ReadOnly = True
        Me.DireccionDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'RepartidorDataGridViewTextBoxColumn
        '
        Me.RepartidorDataGridViewTextBoxColumn.DataPropertyName = "Repartidor"
        Me.RepartidorDataGridViewTextBoxColumn.HeaderText = "Repartidor"
        Me.RepartidorDataGridViewTextBoxColumn.Name = "RepartidorDataGridViewTextBoxColumn"
        Me.RepartidorDataGridViewTextBoxColumn.ReadOnly = True
        Me.RepartidorDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'TotalSinFleteDataGridViewTextBoxColumn
        '
        Me.TotalSinFleteDataGridViewTextBoxColumn.DataPropertyName = "TotalSinFlete"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.TotalSinFleteDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.TotalSinFleteDataGridViewTextBoxColumn.HeaderText = "T. Sin Flete"
        Me.TotalSinFleteDataGridViewTextBoxColumn.Name = "TotalSinFleteDataGridViewTextBoxColumn"
        Me.TotalSinFleteDataGridViewTextBoxColumn.ReadOnly = True
        Me.TotalSinFleteDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'FleteDataGridViewTextBoxColumn
        '
        Me.FleteDataGridViewTextBoxColumn.DataPropertyName = "Flete"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.FleteDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.FleteDataGridViewTextBoxColumn.HeaderText = "Flete"
        Me.FleteDataGridViewTextBoxColumn.Name = "FleteDataGridViewTextBoxColumn"
        Me.FleteDataGridViewTextBoxColumn.ReadOnly = True
        Me.FleteDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'TotalGeneralDataGridViewTextBoxColumn
        '
        Me.TotalGeneralDataGridViewTextBoxColumn.DataPropertyName = "TotalGeneral"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.TotalGeneralDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.TotalGeneralDataGridViewTextBoxColumn.HeaderText = "T. General"
        Me.TotalGeneralDataGridViewTextBoxColumn.Name = "TotalGeneralDataGridViewTextBoxColumn"
        Me.TotalGeneralDataGridViewTextBoxColumn.ReadOnly = True
        Me.TotalGeneralDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'EstadoDataGridViewCheckBoxColumn
        '
        Me.EstadoDataGridViewCheckBoxColumn.DataPropertyName = "Estado"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EstadoDataGridViewCheckBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.EstadoDataGridViewCheckBoxColumn.HeaderText = "Pagado"
        Me.EstadoDataGridViewCheckBoxColumn.Name = "EstadoDataGridViewCheckBoxColumn"
        Me.EstadoDataGridViewCheckBoxColumn.ReadOnly = True
        Me.EstadoDataGridViewCheckBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.EstadoDataGridViewCheckBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        '
        'PagoDataGridViewTextBoxColumn
        '
        Me.PagoDataGridViewTextBoxColumn.DataPropertyName = "Pago"
        Me.PagoDataGridViewTextBoxColumn.HeaderText = "Pago"
        Me.PagoDataGridViewTextBoxColumn.Name = "PagoDataGridViewTextBoxColumn"
        Me.PagoDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'IVADataGridViewTextBoxColumn
        '
        Me.IVADataGridViewTextBoxColumn.DataPropertyName = "IVA"
        Me.IVADataGridViewTextBoxColumn.HeaderText = "IVA"
        Me.IVADataGridViewTextBoxColumn.Name = "IVADataGridViewTextBoxColumn"
        Me.IVADataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.IVADataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.IVADataGridViewTextBoxColumn.Visible = False
        '
        'frmRepartidoresFact
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(916, 493)
        Me.Controls.Add(Me.txtCreditoConFlete)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtIgreso)
        Me.Controls.Add(Me.txtTotalIngreso)
        Me.Controls.Add(Me.txtSinpeConFlete)
        Me.Controls.Add(Me.lbsinpe)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtTarConFlete)
        Me.Controls.Add(Me.txtEfectivo)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtTotalCobrar)
        Me.Controls.Add(Me.ckMarcar)
        Me.Controls.Add(Me.txtTotalSinpeTarSinFlete)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lbContador)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btPagar)
        Me.Controls.Add(Me.btReporte)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtTotalFlete)
        Me.Controls.Add(Me.txtTotalGeneral)
        Me.Controls.Add(Me.btSemanaPadada)
        Me.Controls.Add(Me.btAyer)
        Me.Controls.Add(Me.btHoy)
        Me.Controls.Add(Me.btDetalle)
        Me.Controls.Add(Me.btBuscar)
        Me.Controls.Add(Me.dgvFacturas)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmRepartidoresFact"
        Me.Text = "Repartidores"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsRepartidores1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txtBuscar As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dtpHasta As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents dtpDesde As DateTimePicker
    Friend WithEvents dgvFacturas As DataGridView
    Friend WithEvents btBuscar As Button
    Friend WithEvents btDetalle As Button
    Friend WithEvents btHoy As Button
    Friend WithEvents btAyer As Button
    Friend WithEvents btSemanaPadada As Button
    Friend WithEvents txtTotalGeneral As TextBox
    Friend WithEvents txtTotalFlete As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents btReporte As Button
    Friend WithEvents DtsRepartidores1 As dtsRepartidores
    Friend WithEvents bsFacturas As BindingSource
    Friend WithEvents btPagar As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents lbContador As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtTotalSinpeTarSinFlete As TextBox
    Friend WithEvents ckMarcar As CheckBox
    Friend WithEvents txtTotalCobrar As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtIgreso As Label
    Friend WithEvents txtTotalIngreso As TextBox
    Friend WithEvents txtSinpeConFlete As TextBox
    Friend WithEvents lbsinpe As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtTarConFlete As TextBox
    Friend WithEvents txtEfectivo As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtCreditoConFlete As TextBox
    Friend WithEvents Id As DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents Inicial As DataGridViewTextBoxColumn
    Friend WithEvents FacturaDataGridViewTextBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents ClienteDataGridViewTextBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents DireccionDataGridViewTextBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents RepartidorDataGridViewTextBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents TotalSinFleteDataGridViewTextBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents FleteDataGridViewTextBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents TotalGeneralDataGridViewTextBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents EstadoDataGridViewCheckBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents Estado As DataGridViewCheckBoxColumn
    Friend WithEvents PagoDataGridViewTextBoxColumn As DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn
    Friend WithEvents IVADataGridViewTextBoxColumn As DataGridViewButtonColumn
End Class
