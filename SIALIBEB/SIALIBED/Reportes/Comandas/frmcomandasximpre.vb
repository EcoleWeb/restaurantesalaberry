Imports System.Data.SqlClient
Imports System.IO


Public Class frmcomandasximpre
    Dim ds As DataSet
    Dim punto_venta As Integer
    'Instancia para menejar las consultas a las comandas..
    Dim misComandas As New cls_Comandas()
#Region "funciones"
    Public Sub inicio()
        Try
            Me.cbximpresoras.DataSource = Me.misComandas.obtenerTodasLasImpresoras()
            Me.cbximpresoras.DisplayMember = "impresora"
            Me.cbximpresoras.ValueMember = "impresora"
            Me.cbximpresoras.Text = "COCINA"
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    'Cargamos el reporte con los parametros estableciddos por el usuario.
    Public Sub cargarReporte()
        Try
            Dim rpt As New reportecomandasximpresora
            Me.punto_venta = Convert.ToInt32(Me.misComandas.obtenerPuntoVenta())
            rpt.SetParameterValue("fechaDesde", Me.fecha(Me.fechainicio.Value))
            rpt.SetParameterValue("fechaHasta", Me.fecha2(Me.fechaFinal.Value))
            rpt.SetParameterValue(2, Me.punto_venta)
            rpt.SetParameterValue("impresora", Me.cbximpresoras.Text)
            CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, rpt, , )
            PictureBox3.Focus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    'Agrega hora de diurna 
    Public Function fecha(ByVal a As DateTime) As DateTime
        Try
            Dim fecha1 As DateTime = a
            Dim fecha2 As String = fecha1.ToShortDateString()
            fecha1 = Convert.ToDateTime(fecha2 + " 12:00:00 a.m.")
            Return fecha1
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    'Agrega hora de nocturna
    Public Function fecha2(ByVal a As DateTime) As DateTime
        Try
            Dim fecha1 As DateTime = a
            Dim fecha3 As String = fecha1.ToShortDateString()
            fecha1 = Convert.ToDateTime(fecha3 + " 11:59:59 p.m.")
            Return fecha1
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    'Cargamos el reporte con los parametros del dia actual.
    Public Sub cargarDiaActual()
        Try
            Dim rpt As New reportecomandasximpresora
            Me.punto_venta = Convert.ToInt32(Me.misComandas.obtenerPuntoVenta())
            rpt.SetParameterValue("fechaDesde", fecha(Date.Now))
            rpt.SetParameterValue("fechaHasta", fecha2(Date.Now))
            rpt.SetParameterValue(2, Me.punto_venta)
            rpt.SetParameterValue("impresora", Me.cbximpresoras.Text)
            CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, rpt, , )
            PictureBox1.Focus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    ' Carga el reporte del dia anterior
    Public Sub efecto1()
        Me.lblhoy.BackColor = Color.LightSkyBlue
        Me.lblayer.BackColor = Color.White
        Me.lblconsult.BackColor = Color.White
        Me.PictureBox1.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
    End Sub
    Public Sub efecto2()
        Me.lblayer.BackColor = Color.LightSkyBlue
        Me.lblhoy.BackColor = Color.White
        Me.lblconsult.BackColor = Color.White
        Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
    End Sub
    Public Sub efecto3()
        Me.lblconsult.BackColor = Color.LightSkyBlue
        Me.lblayer.BackColor = Color.White
        Me.lblhoy.BackColor = Color.White
        Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro2
    End Sub
    Public Sub cargarDiaAnterior()
        Try
            Dim fecha1 As DateTime
            fecha1 = Date.Now
            fecha1 = DateAdd(DateInterval.Day, -1, fecha1)
            Dim rpt As New reportecomandasximpresora
            Me.punto_venta = Convert.ToInt32(Me.misComandas.obtenerPuntoVenta())
            rpt.SetParameterValue("fechaDesde", Me.fecha(fecha1))
            rpt.SetParameterValue("fechaHasta", Me.fecha2(fecha1))
            rpt.SetParameterValue(2, Me.punto_venta)
            rpt.SetParameterValue("impresora", Me.cbximpresoras.Text)
            CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, rpt, , )
            PictureBox2.Focus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub frmcomandasximpre_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.inicio()
        Me.cargarDiaActual()
    End Sub
    Private Sub PictureBox22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Me.efecto1()
        Me.cargarDiaActual()
    End Sub
    Private Sub lblhoy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblhoy.Click
        Me.efecto1()
        Me.cargarDiaActual()
    End Sub
    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Me.efecto2()
        Me.cargarDiaAnterior()
    End Sub
    Private Sub lblayer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblayer.Click
        Me.efecto2()
        Me.cargarDiaAnterior()
    End Sub
    Private Sub lblconsult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblconsult.Click
        Me.efecto3()
        Me.cargarReporte()
    End Sub
    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Me.efecto3()
        Me.cargarReporte()
    End Sub
    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click
        Me.Close()
    End Sub
    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click
        Me.Close()
    End Sub

    Private Sub fechainicio_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechainicio.ValueChanged
        If Me.fechainicio.Value.Date > Me.fechaFinal.Value.Date Then 'ambas fechas queden iguales
            Me.fechaFinal.Value = Me.fechainicio.Value
        End If
    End Sub

    Private Sub fechaFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechaFinal.ValueChanged
        If Me.fechaFinal.Value.Date < Me.fechainicio.Value.Date Then
            Me.fechainicio.Value = Me.fechaFinal.Value
        End If
    End Sub
End Class

