Imports System.Data.SqlClient
Public Class RExpress

#Region "Variables"
    Dim cs As String = ""
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim DataS As New DataSet
    Dim PMU As New PerfilModulo_Class
    Dim cedula As String
    Dim puntos As Integer = 0
#End Region
#Region "Mostrar Reporte"
    Sub mostrar()
        Me.BMostrar.Enabled = False
        Me.BMostrar.Text = "LOAD.."
        Try
            If Me.RadioButton1.Checked = True Then
                Dim Reporte As New RClienteExpress
                CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, Reporte, False, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
                Me.RadioButton1.Focus()
            End If
            If Me.RBCantidad.Checked = True Then
                Dim dt As DateTime
                dt = DPhasta.Value.AddDays(1)
                Dim reporte As New RCantidadExpress
                reporte.SetParameterValue(0, CDate(DPfechadesde.Value))
                reporte.SetParameterValue(1, CDate(dt.Date))
                CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, reporte, False, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
            End If
            If Me.RBMonto.Checked = True Then
                Dim dt As DateTime
                dt = DPhasta.Value.AddDays(1)
                Dim Reporte As New RMontoExpress
                If CbMoneda.Text <> "" Then
                    Reporte.SetParameterValue(0, CDate(DPfechadesde.Value))
                    Reporte.SetParameterValue(1, CDate(dt.Date))
                    Reporte.SetParameterValue(2, CDbl(Me.TxtValorMoneda.Text))
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, Reporte, False, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
                Else
                    MessageBox.Show("Elige Moneda", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            End If
        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema:" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        End Try
        Me.BMostrar.Text = "MOSTRAR"
        Me.BMostrar.Enabled = True
    End Sub


#End Region

#Region "Funciones Controles"
    Private Sub BSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BSalir.Click
        If MsgBox("Se cerrar� el m�dulo, �desea cerrar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Me.Close()
        End If

    End Sub
    Private Sub BMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BMostrar.Click
        mostrar()
    End Sub
    Private Sub RadioButton1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            Me.GroupBox2.Visible = False
            Me.GroupBox3.Visible = False
        End If
    End Sub

    Private Sub RBMonto_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBMonto.CheckedChanged
        If RBMonto.Checked = True Then
            Me.GroupBox2.Visible = True
            Me.GroupBox3.Visible = True
        End If
    End Sub

    Private Sub RBCantidad_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBCantidad.CheckedChanged
        If RBCantidad.Checked = True Then
            Me.GroupBox2.Visible = True
            Me.GroupBox3.Visible = False
        End If
    End Sub

    Private Sub CbMoneda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CbMoneda.SelectedIndexChanged
        Dim row As DataRow
        For Each row In DataS.Tables("Moneda").Rows
            If CbMoneda.SelectedItem = row("MonedaNombre") Then
                TxtValorMoneda.Text = row("ValorCompra")
            End If
        Next
    End Sub
#End Region
#Region "Load"
    Private Sub Clientesexpress_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = FormWindowState.Maximized
        Try
            Text = "Reportes " & User_Log.NombrePunto
            Me.DPfechadesde.Value = DateAdd(DateInterval.Day, -Now.Day + 1, Now.Date).AddHours(6)
            Me.DPhasta.Value = Now.Date.AddHours(23).AddMinutes(59)
            carga_cbox()

        Catch ex As Exception
            MessageBox.Show("Error al cargar: " & ex.Message, "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
#End Region
#Region "Cargar CbBox"
    Private Sub cargar_Combo( _
           ByVal ComboBox As ComboBox, _
           ByVal sql As String)

        ' nueva conexi�n indicando al SqlConnection la cadena de conexi�n  
        Dim cn As New SqlConnection(cs)

        Try

            ' Abrir la conexi�n a Sql  
            cn.Open()

            ' Pasar la consulta sql y la conexi�n al Sql Command   
            Dim cmd As New SqlCommand(sql, cn)

            ' Inicializar un nuevo SqlDataAdapter   
            Dim da As New SqlDataAdapter(cmd)

            'Crear y Llenar un Dataset  
            Dim ds As New DataSet
            da.Fill(ds)

            ' asignar el DataSource al combobox  
            ComboBox.DataSource = ds.Tables(0)


            ' Asignar el campo a la propiedad DisplayMember del combo   
            ComboBox.DisplayMember = ds.Tables(0).Columns(0).Caption.ToString
            ComboBox.ValueMember = ds.Tables(0).Columns(1).Caption.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, _
                            "error", MessageBoxButtons.OK, _
                            MessageBoxIcon.Error)
        Finally
            If cn.State = ConnectionState.Open Then
                cn.Close()
            End If
        End Try
    End Sub
    Private Sub carga_cbox()
        'carga los tipos de moneda
        conectadobd = cConexion.Conectar("Seguridad")
        cConexion.GetDataSet(conectadobd, "Select * from Moneda", DataS, "Moneda")
        Dim fila As DataRow
        For Each fila In DataS.Tables("Moneda").Rows
            CbMoneda.Items.Add(fila("MonedaNombre"))
        Next
        cConexion.DesConectar(conectadobd)
        conectadobd = cConexion.Conectar("Hotel")
        'carga los clientes
        cConexion.GetDataSet(conectadobd, "Select Id, Nombre from Cliente", DataS, "Cliente")
    End Sub
#End Region

    
    Private Sub DPfechadesde_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DPfechadesde.ValueChanged
        If Me.DPfechadesde.Value.Date > Me.DPhasta.Value.Date Then
            Me.DPhasta.Value = Me.DPfechadesde.Value
            Me.DPhasta.Value = Me.DPhasta.Value.AddDays(1)
        End If
    End Sub

    Private Sub DPhasta_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DPhasta.ValueChanged
        If Me.DPhasta.Value.Date < Me.DPfechadesde.Value.Date Then
            Me.DPfechadesde.Value = Me.DPhasta.Value
            Me.DPfechadesde.Value = Me.DPfechadesde.Value.AddDays(-1)
        End If
    End Sub
End Class