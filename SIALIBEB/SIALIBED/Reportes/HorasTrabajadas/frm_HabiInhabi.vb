Public Class frm_HabiInhabi
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents fechaInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents fechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.fechaInicio = New System.Windows.Forms.DateTimePicker
        Me.fechaFin = New System.Windows.Forms.DateTimePicker
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(279, 33)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Mostrar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Inicio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(21, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Fin"
        '
        'fechaInicio
        '
        Me.fechaInicio.CustomFormat = "dd/MM/yyyy hh:mm tt"
        Me.fechaInicio.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.fechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.fechaInicio.Location = New System.Drawing.Point(53, 10)
        Me.fechaInicio.Name = "fechaInicio"
        Me.fechaInicio.Size = New System.Drawing.Size(138, 20)
        Me.fechaInicio.TabIndex = 6
        Me.fechaInicio.Value = New Date(2007, 4, 12, 0, 0, 0, 0)
        '
        'fechaFin
        '
        Me.fechaFin.CustomFormat = "dd/MM/yyyy hh:mm tt"
        Me.fechaFin.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.fechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.fechaFin.Location = New System.Drawing.Point(53, 36)
        Me.fechaFin.Name = "fechaFin"
        Me.fechaFin.Size = New System.Drawing.Size(138, 20)
        Me.fechaFin.TabIndex = 7
        Me.fechaFin.Value = New Date(2007, 4, 12, 0, 0, 0, 0)
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(279, 10)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(40, 17)
        Me.CheckBox1.TabIndex = 8
        Me.CheckBox1.Text = "PV"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'frm_HabiInhabi
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(384, 74)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.fechaFin)
        Me.Controls.Add(Me.fechaInicio)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(400, 112)
        Me.MinimumSize = New System.Drawing.Size(400, 112)
        Me.Name = "frm_HabiInhabi"
        Me.Text = "Reporte Horas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Sub imprimir()
        Dim rC
        If Me.CheckBox1.Checked = True Then rC = New reporte_horasPV Else rC = New reporte_horas
        Dim visor As New frmVisorReportes
        rC.SetParameterValue(0, Me.fechaInicio.Value)
        rC.SetParameterValue(1, Me.fechaFin.Value)
        CrystalReportsConexion.LoadReportViewer(visor.rptViewer, rC, True)
        visor.rptViewer.ReportSource = rC
        visor.MdiParent = MdiParent
        visor.Show()
        visor.rptViewer.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        imprimir()
    End Sub

    Private Sub frm_HabiInhabi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.fechaFin.Value = Date.Now
        Me.fechaInicio.Value = Date.Now
    End Sub

    Private Sub fechaInicio_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechaInicio.ValueChanged
        If Me.fechaInicio.Value.Date > Me.fechaFin.Value.Date Then
            fechaFin.Value = fechaInicio.Value
        End If
    End Sub

    Private Sub fechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechaFin.ValueChanged
        If Me.fechaFin.Value.Date < Me.fechaInicio.Value.Date Then
            fechaInicio.Value = fechaFin.Value
        End If
    End Sub
End Class
