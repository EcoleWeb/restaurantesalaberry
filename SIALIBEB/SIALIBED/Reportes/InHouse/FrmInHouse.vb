Public Class FrmInHouse
    Inherits System.Windows.Forms.Form
    'Public picture As New PictureBox

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterHabitacion As System.Data.SqlClient.SqlDataAdapter

    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents AdapterConfiguraciones As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DataSetInhouse1 As SIALIBEB.DataSetInhouse
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmInHouse))
        Me.AdapterHabitacion = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.AdapterConfiguraciones = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.DataSetInhouse1 = New SIALIBEB.DataSetInhouse
        CType(Me.DataSetInhouse1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'AdapterHabitacion
        '
        Me.AdapterHabitacion.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterHabitacion.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterHabitacion.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Habitacion1", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("CodigoHabitacion", "CodigoHabitacion"), New System.Data.Common.DataColumnMapping("Libre", "Libre"), New System.Data.Common.DataColumnMapping("Ocupada", "Ocupada"), New System.Data.Common.DataColumnMapping("Id_Reservacion", "Id_Reservacion")})})
        Me.AdapterHabitacion.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodigoHabitacion", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoHabitacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Libre", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Libre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Ocupada", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ocupada", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Reservacion", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Reservacion", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=NEMESIS;packet size=4096;integrated security=SSPI;data source=""."";" & _
            "persist security info=False;initial catalog=Hotel"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Codigo, CodigoHabitacion, Libre, Ocupada, Id_Reservacion FROM Habitacion1"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.VarChar, 0, "Codigo"), New System.Data.SqlClient.SqlParameter("@CodigoHabitacion", System.Data.SqlDbType.BigInt, 0, "CodigoHabitacion"), New System.Data.SqlClient.SqlParameter("@Libre", System.Data.SqlDbType.Bit, 0, "Libre"), New System.Data.SqlClient.SqlParameter("@Ocupada", System.Data.SqlDbType.Bit, 0, "Ocupada"), New System.Data.SqlClient.SqlParameter("@Id_Reservacion", System.Data.SqlDbType.BigInt, 0, "Id_Reservacion"), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodigoHabitacion", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoHabitacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Libre", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Libre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Ocupada", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ocupada", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Reservacion", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Reservacion", System.Data.DataRowVersion.Original, Nothing)})
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(-16, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.ShowRefreshButton = False
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(688, 328)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'AdapterConfiguraciones
        '
        Me.AdapterConfiguraciones.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterConfiguraciones.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterConfiguraciones.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterConfiguraciones.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "configuraciones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Empresa", "Empresa"), New System.Data.Common.DataColumnMapping("Tel_01", "Tel_01"), New System.Data.Common.DataColumnMapping("Tel_02", "Tel_02"), New System.Data.Common.DataColumnMapping("Fax_01", "Fax_01"), New System.Data.Common.DataColumnMapping("Fax_02", "Fax_02"), New System.Data.Common.DataColumnMapping("Direccion", "Direccion"), New System.Data.Common.DataColumnMapping("Imp_Venta", "Imp_Venta"), New System.Data.Common.DataColumnMapping("Frase", "Frase"), New System.Data.Common.DataColumnMapping("Imp_Servicio", "Imp_Servicio"), New System.Data.Common.DataColumnMapping("Logo", "Logo"), New System.Data.Common.DataColumnMapping("Intereses", "Intereses"), New System.Data.Common.DataColumnMapping("Imp_ICT", "Imp_ICT"), New System.Data.Common.DataColumnMapping("Total_Dias_Cancelar", "Total_Dias_Cancelar"), New System.Data.Common.DataColumnMapping("Email", "Email"), New System.Data.Common.DataColumnMapping("Dirrecion_Web", "Dirrecion_Web"), New System.Data.Common.DataColumnMapping("Comision_Habitaciones", "Comision_Habitaciones"), New System.Data.Common.DataColumnMapping("Comision_Servicio", "Comision_Servicio"), New System.Data.Common.DataColumnMapping("Comision_Restaurant", "Comision_Restaurant"), New System.Data.Common.DataColumnMapping("Cuenta_Bancaria1", "Cuenta_Bancaria1"), New System.Data.Common.DataColumnMapping("Cuenta_Bancaria2", "Cuenta_Bancaria2"), New System.Data.Common.DataColumnMapping("Impresora_Facturacion", "Impresora_Facturacion"), New System.Data.Common.DataColumnMapping("Maximo_Descuento", "Maximo_Descuento"), New System.Data.Common.DataColumnMapping("PersonaJuridica", "PersonaJuridica"), New System.Data.Common.DataColumnMapping("CantidadHabitaciones", "CantidadHabitaciones")})})
        Me.AdapterConfiguraciones.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CantidadHabitaciones", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CantidadHabitaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Comision_Habitaciones", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Comision_Habitaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Comision_Restaurant", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Comision_Restaurant", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Comision_Servicio", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Comision_Servicio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Bancaria1", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Bancaria1", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Bancaria2", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Bancaria2", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Dirrecion_Web", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Dirrecion_Web", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Empresa", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Empresa", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fax_01", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fax_02", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Frase", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Frase", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Imp_ICT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_ICT", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Imp_Servicio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Servicio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Imp_Venta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Venta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Impresora_Facturacion", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Impresora_Facturacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Intereses", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Intereses", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Maximo_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maximo_Descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PersonaJuridica", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersonaJuridica", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tel_01", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tel_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tel_02", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tel_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Total_Dias_Cancelar", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total_Dias_Cancelar", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = resources.GetString("SqlInsertCommand2.CommandText")
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 255, "Cedula"), New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 255, "Empresa"), New System.Data.SqlClient.SqlParameter("@Tel_01", System.Data.SqlDbType.VarChar, 255, "Tel_01"), New System.Data.SqlClient.SqlParameter("@Tel_02", System.Data.SqlDbType.VarChar, 255, "Tel_02"), New System.Data.SqlClient.SqlParameter("@Fax_01", System.Data.SqlDbType.VarChar, 255, "Fax_01"), New System.Data.SqlClient.SqlParameter("@Fax_02", System.Data.SqlDbType.VarChar, 255, "Fax_02"), New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 255, "Direccion"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 8, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Frase", System.Data.SqlDbType.VarChar, 255, "Frase"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio", System.Data.SqlDbType.Float, 8, "Imp_Servicio"), New System.Data.SqlClient.SqlParameter("@Logo", System.Data.SqlDbType.VarBinary, 2147483647, "Logo"), New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Int, 4, "Intereses"), New System.Data.SqlClient.SqlParameter("@Imp_ICT", System.Data.SqlDbType.Float, 8, "Imp_ICT"), New System.Data.SqlClient.SqlParameter("@Total_Dias_Cancelar", System.Data.SqlDbType.Int, 4, "Total_Dias_Cancelar"), New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 100, "Email"), New System.Data.SqlClient.SqlParameter("@Dirrecion_Web", System.Data.SqlDbType.VarChar, 100, "Dirrecion_Web"), New System.Data.SqlClient.SqlParameter("@Comision_Habitaciones", System.Data.SqlDbType.Bit, 1, "Comision_Habitaciones"), New System.Data.SqlClient.SqlParameter("@Comision_Servicio", System.Data.SqlDbType.Bit, 1, "Comision_Servicio"), New System.Data.SqlClient.SqlParameter("@Comision_Restaurant", System.Data.SqlDbType.Bit, 1, "Comision_Restaurant"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria1", System.Data.SqlDbType.VarChar, 200, "Cuenta_Bancaria1"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria2", System.Data.SqlDbType.VarChar, 200, "Cuenta_Bancaria2"), New System.Data.SqlClient.SqlParameter("@Impresora_Facturacion", System.Data.SqlDbType.VarChar, 200, "Impresora_Facturacion"), New System.Data.SqlClient.SqlParameter("@Maximo_Descuento", System.Data.SqlDbType.Float, 8, "Maximo_Descuento"), New System.Data.SqlClient.SqlParameter("@PersonaJuridica", System.Data.SqlDbType.VarChar, 255, "PersonaJuridica"), New System.Data.SqlClient.SqlParameter("@CantidadHabitaciones", System.Data.SqlDbType.Int, 4, "CantidadHabitaciones")})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = resources.GetString("SqlSelectCommand2.CommandText")
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 255, "Cedula"), New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 255, "Empresa"), New System.Data.SqlClient.SqlParameter("@Tel_01", System.Data.SqlDbType.VarChar, 255, "Tel_01"), New System.Data.SqlClient.SqlParameter("@Tel_02", System.Data.SqlDbType.VarChar, 255, "Tel_02"), New System.Data.SqlClient.SqlParameter("@Fax_01", System.Data.SqlDbType.VarChar, 255, "Fax_01"), New System.Data.SqlClient.SqlParameter("@Fax_02", System.Data.SqlDbType.VarChar, 255, "Fax_02"), New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 255, "Direccion"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 8, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Frase", System.Data.SqlDbType.VarChar, 255, "Frase"), New System.Data.SqlClient.SqlParameter("@Imp_Servicio", System.Data.SqlDbType.Float, 8, "Imp_Servicio"), New System.Data.SqlClient.SqlParameter("@Logo", System.Data.SqlDbType.VarBinary, 2147483647, "Logo"), New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Int, 4, "Intereses"), New System.Data.SqlClient.SqlParameter("@Imp_ICT", System.Data.SqlDbType.Float, 8, "Imp_ICT"), New System.Data.SqlClient.SqlParameter("@Total_Dias_Cancelar", System.Data.SqlDbType.Int, 4, "Total_Dias_Cancelar"), New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 100, "Email"), New System.Data.SqlClient.SqlParameter("@Dirrecion_Web", System.Data.SqlDbType.VarChar, 100, "Dirrecion_Web"), New System.Data.SqlClient.SqlParameter("@Comision_Habitaciones", System.Data.SqlDbType.Bit, 1, "Comision_Habitaciones"), New System.Data.SqlClient.SqlParameter("@Comision_Servicio", System.Data.SqlDbType.Bit, 1, "Comision_Servicio"), New System.Data.SqlClient.SqlParameter("@Comision_Restaurant", System.Data.SqlDbType.Bit, 1, "Comision_Restaurant"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria1", System.Data.SqlDbType.VarChar, 200, "Cuenta_Bancaria1"), New System.Data.SqlClient.SqlParameter("@Cuenta_Bancaria2", System.Data.SqlDbType.VarChar, 200, "Cuenta_Bancaria2"), New System.Data.SqlClient.SqlParameter("@Impresora_Facturacion", System.Data.SqlDbType.VarChar, 200, "Impresora_Facturacion"), New System.Data.SqlClient.SqlParameter("@Maximo_Descuento", System.Data.SqlDbType.Float, 8, "Maximo_Descuento"), New System.Data.SqlClient.SqlParameter("@PersonaJuridica", System.Data.SqlDbType.VarChar, 255, "PersonaJuridica"), New System.Data.SqlClient.SqlParameter("@CantidadHabitaciones", System.Data.SqlDbType.Int, 4, "CantidadHabitaciones"), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CantidadHabitaciones", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CantidadHabitaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Comision_Habitaciones", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Comision_Habitaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Comision_Restaurant", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Comision_Restaurant", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Comision_Servicio", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Comision_Servicio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Bancaria1", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Bancaria1", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Bancaria2", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Bancaria2", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Dirrecion_Web", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Dirrecion_Web", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Empresa", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Empresa", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fax_01", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fax_02", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Frase", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Frase", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Imp_ICT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_ICT", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Imp_Servicio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Servicio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Imp_Venta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Venta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Impresora_Facturacion", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Impresora_Facturacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Intereses", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Intereses", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Maximo_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maximo_Descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PersonaJuridica", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersonaJuridica", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tel_01", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tel_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tel_02", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tel_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Total_Dias_Cancelar", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total_Dias_Cancelar", System.Data.DataRowVersion.Original, Nothing)})
        '
        'DataSetInhouse1
        '
        Me.DataSetInhouse1.DataSetName = "DataSetInhouse"
        Me.DataSetInhouse1.Locale = New System.Globalization.CultureInfo("es-MX")
        Me.DataSetInhouse1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FrmInHouse
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(672, 326)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "FrmInHouse"
        Me.Text = "Reporte Inhouse"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DataSetInhouse1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub FrmInHouse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Hotel", "CONEXION")
        Me.AdapterHabitacion.Fill(Me.DataSetInhouse1.Habitacion1)
        Me.AdapterConfiguraciones.Fill(Me.DataSetInhouse1.configuraciones)
        Inhouse()
        Cargar()
    End Sub

#Region "INHOUSE"

    Function Cargar()
        Dim ReporteInhouse As New ReporteInhouse
        ReporteInhouse.SetDataSource(Me.DataSetInhouse1)
        ReporteInhouse.Subreports(0).SetDataSource(Me.DataSetInhouse1)
        ReporteInhouse.SetParameterValue(0, Now.Date)
        Me.CrystalReportViewer1.ReportSource = ReporteInhouse
    End Function


    Function Inhouse()
        Dim i As Integer
        Dim row As DataRow
        Dim rowreserva As DataRow
        Dim rowDetalleCheckIn As DataRow
        Dim Id_CheckIn As Double

        For i = 0 To Me.DataSetInhouse1.Habitacion1.Rows.Count - 1
            If Me.DataSetInhouse1.Habitacion1.Rows(i).Item("Ocupada") = True And Me.DataSetInhouse1.Habitacion1.Rows(i).Item("Id_Reservacion") <> 0 Then
                row = Me.DataSetInhouse1.Reporte.NewReporteRow
                row("Habitacion") = Me.DataSetInhouse1.Habitacion1.Rows(i).Item("codigo")
                rowreserva = BuscarFechaReservacion(Me.DataSetInhouse1.Habitacion1.Rows(i).Item("Id_Reservacion"))
                row("FechaIngreso") = rowreserva("Fecha_Ingreso")
                row("FechaSalida") = rowreserva("Fecha_Salida")
                row("Agencia") = rowreserva("Nombre_Compa�ia")
                Id_CheckIn = BuscarCheckIn(Me.DataSetInhouse1.Habitacion1.Rows(i).Item("Id_Reservacion"))
                'Si solo se asigno la habitacion
                If Id_CheckIn = 0 Then
                    row("Nombre") = "HABITACION ASIGNADA"
                    row("Apellidos") = "HABITACION ASIGNADA"
                    row("CantidadAdultos") = 0
                    row("CantidadNinos") = 0
                    row("TotalPax") = 0
                    Me.DataSetInhouse1.Reporte.AddReporteRow(row)
                Else
                    rowDetalleCheckIn = BuscarDetalleCheckInn(Id_CheckIn, Me.DataSetInhouse1.Habitacion1.Rows(i).Item("codigo"))
                    If rowDetalleCheckIn Is Nothing Then
                        row("Nombre") = "HABITACION ASIGNADA"
                        row("Apellidos") = "HABITACION ASIGNADA"
                        row("CantidadAdultos") = 0
                        row("CantidadNinos") = 0
                        row("TotalPax") = 0
                        Me.DataSetInhouse1.Reporte.AddReporteRow(row)
                    Else
                        row("Nombre") = rowDetalleCheckIn("Nombre")
                        row("Apellidos") = rowDetalleCheckIn("Primer_Apellido")
                        row("CantidadAdultos") = rowDetalleCheckIn("TotalAdultos")
                        row("CantidadNinos") = rowDetalleCheckIn("TotalNinos")
                        row("TotalPax") = rowDetalleCheckIn("TotalPax")
                        Me.DataSetInhouse1.Reporte.AddReporteRow(row)
                    End If

                End If

            End If
        Next

        '        Me.DataGrid1.DataSource = Me.DataSetInhouse1.Reporte
    End Function

    Function BuscarFechaReservacion(ByVal Id_Reservacion As Double) As DataRow
        Dim cx As New cFunciones
        Dim tabla As New DataTable
        cx.Llenar_Tabla_Generico("SELECT Fecha_Ingreso, Fecha_Salida,Nombre_Compa�ia FROM Reservacion WHERE Id_Reservacion = " & Id_Reservacion, tabla, Me.SqlConnection1.ConnectionString)
        If tabla.Rows.Count > 0 Then
            Return tabla.Rows(0)
        Else
            Return Nothing
        End If

    End Function
    Function BuscarDetalleCheckInn(ByVal Id_CheckIn As Double, ByVal Codigo As String) As DataRow
        Dim cx As New cFunciones
        Dim tabla As New DataTable
        cx.Llenar_Tabla_Generico("SELECT Primer_Apellido, Nombre,TotalNinos, TotalAdultos, TotalPax  FROM Check_In_Detalle WHERE Id_Check_In = " & Id_CheckIn & " AND Codigo = '" & Codigo & "'", tabla, Me.SqlConnection1.ConnectionString)
        If tabla.Rows.Count > 0 Then
            Return tabla.Rows(0)
        Else
            Return Nothing
        End If
    End Function

    Function BuscarCheckIn(ByVal Id_Reservacion As Double) As Double
        Dim cx As New cFunciones
        Dim tabla As New DataTable
        cx.Llenar_Tabla_Generico("SELECT Id_Check_In FROM Check_In WHERE Id_Reservacion = " & Id_Reservacion, tabla, Me.SqlConnection1.ConnectionString)
        If tabla.Rows.Count > 0 Then
            Return tabla.Rows(0).Item("Id_Check_In")
        Else
            Return 0
        End If
    End Function

#End Region



    'Private Sub Impresion_Analisis_Saldos()

    '    'UPGRADE_ISSUE: qcPrinter objeto no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2068"'
    '    Dim Preview As New qPrinter2.qcPrinter
    '    Dim NoTiempo As Byte
    '    Dim Porc As Double
    '    Dim Fecha As Date
    '    Dim qFT As qPrinter2.qcFormatBlock


    '    With Preview

    '        .ChoosePrinter()
    '        .Language = qPrinter2.qeLocaleLanguageEnum.llSpanish
    '        .Orientation = qPrinter2.qePrintOrientation.qLandscape
    '        With .Document

    '            .Reset()

    '            .ScaleMode = qPrinter2.qePrinterScale.eCentimetre

    '            .MarginLeft = 0.5

    '            .MarginRight = 0.5

    '            '***********************************************************
    '            'IMAGEN LOGO DEL REPORTE
    '            '***********************************************************

    '            .AddImage(False, Me.picture, , , True)

    '            .DocumentItems.RecentImage.Height = 2

    '            .DocumentItems.RecentImage.Width = 2
    '            '***********************************************************
    '            'PIE DE PAGINA DEL REPORTE
    '            '***********************************************************

    '            .SetFooterContent(qPrinter2.qePrinterHeadFootPage.Standard_hf, "P�gina #PageNumber#", "Arial", 9, True, , , , qPrinter2.qePrinterAlign.eCentre)



    '            .SetFooter(qPrinter2.qePrinterHeadFootPage.Standard_hf) = True
    '            '***********************************************************
    '            'ENCABEZADO DE EMPRESA
    '            '***********************************************************

    '            .AddTemplateBlock("Arial", 11, "InfoCompany", True, , , , , 2.5, , , True)

    '            qFT = .Templates("InfoCompany")

    '            qFT.Top = 0

    '            .AddTemplateBlock("Arial", 11, "InfoCompany01", True, , , , , 2.5, , , True)

    '            qFT = .Templates("InfoCompany01")

    '            qFT.Top = 0.5

    '            .AddTemplateBlock("Arial", 11, "InfoCompany02", True, , , , , 2.5, , , True)

    '            qFT = .Templates("InfoCompany02")

    '            qFT.Top = 1

    '            .AddTemplateBlock("Arial", 11, "InfoCompany03", True, , , , , 2.5)

    '            qFT = .Templates("InfoCompany03")

    '            qFT.Top = 1.5
    '            'Agregar elementos a las plantillas informacion de la empresa

    '            .AddTextBlock(Usuario.Empresa, "InfoCompany")

    '            .AddTextBlock("C�dula Jur�dica " & Usuario.Cedula, "InfoCompany01")

    '            .AddTextBlock("Tel(s) " & Usuario.Telefonos & " Fax(es) " & Usuario.Faxes, "InfoCompany02")

    '            .AddTextBlock(Usuario.Direccion, "InfoCompany03")
    '            '***********************************************************
    '            ' Titulo de la pagina
    '            '***********************************************************

    '            .AddFormatBlock("CUENTAS POR COBRAR", "Arial", 11, True, , , , eCentre, , , , , , , "fTitle")

    '            .AddFormatBlock("ANALISIS DE SALDOS", "Arial", 11, True, , , , eCentre, , , , , , , "fTitle")

    '            .AddFormatBlock(VB6.Format(Today, "LONG DATE"), "Arial", 11, True, , , , eCentre, , , , , , , "fTitle")

    '            .AddTemplateBlock("Arial", 10, "ClienteInfo", True, , , , eCentre)

    '            qFT = .Templates("ClienteInfo")

    '            qFT.ShowBorder = True
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.BorderLine. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.BorderLine = 1
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("INFORMACI�N DEL CLIENTE", "ClienteInfo")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "NombreCliente", True, , , , eCentre, , , .AvailableWidth - ((45 / 100) * .AvailableWidth), True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT = .Templates("NombreCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.ShowBorder. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.ShowBorder = True
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.BorderLine. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.BorderLine = 1
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Nombre", "NombreCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "CodigoCliente", True, , , , eCentre, .AvailableWidth - ((45 / 100) * .AvailableWidth), , .AvailableWidth - ((80 / 100) * .AvailableWidth), True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT = .Templates("CodigoCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.ShowBorder. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.ShowBorder = True
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.BorderLine. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.BorderLine = 1
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("C�digo", "CodigoCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "TelefonoCliente", True, , , , eCentre, .AvailableWidth - ((20 / 100) * .AvailableWidth), , .AvailableWidth - ((80 / 100) * .AvailableWidth))
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT = .Templates("TelefonoCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.ShowBorder. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.ShowBorder = True
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.BorderLine. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.BorderLine = 1
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Tel�fono", "TelefonoCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "NCliente", True, , , , eCentre, , , .AvailableWidth - ((45 / 100) * .AvailableWidth), True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Text1().Text. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock(Text1(1).Text, "NCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "CCliente", True, , , , eCentre, .AvailableWidth - ((45 / 100) * .AvailableWidth), , .AvailableWidth - ((80 / 100) * .AvailableWidth), True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Text1().Text. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock(Text1(0).Text, "CCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Text1().Text. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Usuario.Server. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            Cliente = Usuario.Server.OpenRecordset("SELECT Telefono_01, Telefono_02, Direccion FROM Clientes WHERE Identificacion =" & Text1(0).Text)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "TCliente", True, , , , eCentre, .AvailableWidth - ((20 / 100) * .AvailableWidth), , .AvailableWidth - ((80 / 100) * .AvailableWidth))
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Cliente.BOF. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Cliente.EOF. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Cliente!Telefono_02. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Cliente!Telefono_01. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            If Not Cliente.EOF And Not Cliente.BOF Then
    '                .AddTextBlock(Cliente!Telefono_01 & "   " & Cliente!Telefono_02, "TCliente")
    '            Else
    '                'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                .AddTextBlock("", "TCliente")
    '            End If
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "DireccionCliente", True, , , , eCentre)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT = .Templates("DireccionCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.ShowBorder. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.ShowBorder = True
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.BorderLine. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.BorderLine = 1
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Direcci�n", "DireccionCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "DCliente", True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Cliente.BOF. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Cliente.EOF. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            If Not Cliente.EOF And Not Cliente.BOF Then
    '                .AddTextBlock(Cliente!Direccion, "DCliente")
    '            Else
    '                'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                .AddTextBlock("", "DCliente")
    '            End If
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "ResumenCliente", True, , , , eCentre, .AvailableWidth - ((50 / 100) * .AvailableWidth))
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT = .Templates("ResumenCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.ShowBorder. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.ShowBorder = True
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.BorderLine. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.BorderLine = 1
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Resumen de Cuenta", "ResumenCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "SaldoCliente", True, , , , , .AvailableWidth - ((50 / 100) * .AvailableWidth), , , True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Saldo Cuenta", "SaldoCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "SaldoClienteD", True, , , , eRight, .AvailableWidth - ((20 / 100) * .AvailableWidth))
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Text1().Text. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Saldo_Total_Cuenta(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock(VB6.Format(Saldo_Total_Cuenta(Text1(0).Text), "#,#0.00"), "SaldoClienteD")

    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "VencidoCliente", True, , , , , .AvailableWidth - ((50 / 100) * .AvailableWidth), , , True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Saldo Vencido", "VencidoCliente")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "VencidoClienteD", True, , , , eRight, .AvailableWidth - ((20 / 100) * .AvailableWidth))
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Text1().Text. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Saldo_Total_Vencido_Cuenta(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock(VB6.Format(Saldo_Total_Vencido_Cuenta(Text1(0).Text), "#,#0.00"), "VencidoClienteD")




    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 10, "ClienteDetalle", True, , , , eCentre)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT = .Templates("ClienteDetalle")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.ShowBorder. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.ShowBorder = True
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.BorderLine. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.BorderLine = 1
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("DETALLE DE CUENTA", "ClienteDetalle")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 9, "FacturaLabel", True, , , , , , , , True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT = .Templates("FacturaLabel")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.ShowBorder. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.ShowBorder = True
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.BorderLine. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            qFT.BorderLine = 1
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Factura", "FacturaLabel")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 9, "FechaLabel", True, , , , , .AvailableWidth - ((90 / 100) * .AvailableWidth), , .AvailableWidth - ((90 / 100) * .AvailableWidth), True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Fecha", "FechaLabel")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 9, "PlazoLabel", True, , , , , .AvailableWidth - ((80 / 100) * .AvailableWidth), , .AvailableWidth - ((90 / 100) * .AvailableWidth), True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Pl.", "PlazoLabel")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 9, "VenceLabel", True, , , , , .AvailableWidth - ((77 / 100) * .AvailableWidth), , .AvailableWidth - ((95 / 100) * .AvailableWidth), True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Vence", "VenceLabel")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 9, "SaldoLabel", True, , , , , .AvailableWidth - ((67 / 100) * .AvailableWidth), , .AvailableWidth - ((90 / 100) * .AvailableWidth), True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("Saldo", "SaldoLabel")
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto OpcionesProyeccionFrm.Fecha. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            Fecha = OpcionesProyeccionFrm.Fecha
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 9, "FechaInit", True, , , , , .AvailableWidth - ((55 / 100) * .AvailableWidth), , .AvailableWidth - ((90 / 100) * .AvailableWidth), True)
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock("<=" & Fecha, "FechaInit")
    '            Porc = 43
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto OpcionesProyeccionFrm.Tiempo. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            For NoTiempo = 1 To OpcionesProyeccionFrm.Tiempo
    '                'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto OpcionesProyeccionFrm.Periodo. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                If OpcionesProyeccionFrm.Periodo = 1 Then Fecha = DateAdd(Microsoft.VisualBasic.DateInterval.WeekOfYear, 1, Fecha) Else Fecha = DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, Fecha)
    '                'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                .AddTemplateBlock("Arial", 9, "Fecha" & NoTiempo, True, , , , , .AvailableWidth - ((Porc / 100) * .AvailableWidth), , .AvailableWidth - ((90 / 100) * .AvailableWidth), True)
    '                'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                .AddTextBlock("" & Fecha, "Fecha" & NoTiempo)
    '                Porc = Porc - 10
    '            Next NoTiempo
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTemplateBlock("Arial", 9, "Fecha" & NoTiempo, True, , , , , .AvailableWidth - ((Porc / 100) * .AvailableWidth), , .AvailableWidth - ((90 / 100) * .AvailableWidth))
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            .AddTextBlock(">" & Fecha, "Fecha" & NoTiempo)
    '            Porc = 100
    '            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1(0).Rows. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '            For Fil = 1 To MSFlexGrid1(0).Rows - 1
    '                Porc = 100
    '                'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1().Col. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                MSFlexGrid1(0).Col = 0
    '                'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1(0).Text. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                If MSFlexGrid1(0).Text <> "" Then
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1().Col. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    MSFlexGrid1(0).Col = 1
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1().Row. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    .AddTemplateBlock("Arial", 9, "NameCliente" & MSFlexGrid1(0).Row, True, , , , eCentre, .AvailableWidth - ((Porc / 100) * .AvailableWidth))
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1().Row. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    qFT = .Templates("NameCliente" & MSFlexGrid1(0).Row)
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.ShowBorder. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    qFT.ShowBorder = True
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto qFT.BorderLine. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    qFT.BorderLine = 1
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1(0).Row. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1().Text. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    .AddTextBlock(MSFlexGrid1(0).Text, "NameCliente" & MSFlexGrid1(0).Row)
    '                End If
    '                'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1(0).Cols. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                For Col = Init To MSFlexGrid1(0).Cols - 1
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1().Row. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Fil. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    MSFlexGrid1(0).Row = Fil
    '                    'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1().Col. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                    MSFlexGrid1(0).Col = Col
    '                    If Col <> Init + 5 Then
    '                        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1(0).Cols. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                        If Col = MSFlexGrid1(0).Cols - 1 Then
    '                            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                            .AddTemplateBlock("Arial", 9, "Item" & Value, True, , , , , .AvailableWidth - ((Porc / 100) * .AvailableWidth), , .AvailableWidth - ((90 / 100) * .AvailableWidth))
    '                        Else
    '                            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                            .AddTemplateBlock("Arial", 9, "Item" & Value, True, , , , , .AvailableWidth - ((Porc / 100) * .AvailableWidth), , .AvailableWidth - ((90 / 100) * .AvailableWidth), True)
    '                        End If
    '                        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto MSFlexGrid1().Text. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Document. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                        .AddTextBlock(MSFlexGrid1(0).Text, "Item" & Value)
    '                        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '                        Value = Value + 1
    '                    End If
    '                    If Col = Init + 2 Then Porc = Porc - 3 Else If Col = Init + 4 Then Porc = Porc - 4 Else Porc = Porc - 10
    '                Next Col
    '            Next Fil

    '        End With
    '        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Preview.Preview. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
    '        .Preview()
    '    End With
    'End Sub


End Class
