Imports DevExpress.Utils

Public Class FrmReporteMenu

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim Wait As WaitDialogForm
#End Region

#Region "Load"
    Private Sub FrmReporteComandas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCBMoneda()
        cboxmoneda.Focus()
    End Sub

    Private Sub CargaCBMoneda()
        Try
            Dim Dataset As New DataSet
            conectadobd = cConexion.Conectar()
            cConexion.GetDataSet(conectadobd, "SELECT CodMoneda, MonedaNombre, ValorCompra FROM Moneda", Dataset, "Moneda")
            cboxmoneda.DataSource = Dataset.Tables("Moneda")
            cboxmoneda.DisplayMember = "MonedaNombre"
            cboxmoneda.ValueMember = "CodMoneda"
        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema:" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        Finally
            cConexion.DesConectar(conectadobd)
        End Try
    End Sub
#End Region

#Region "Mostrar"
    Private Sub Mostrar()
        BMostrar.Enabled = False
        Wait = New WaitDialogForm("Generando Reporte")
        Try
            Dim Reporte As New Reporte_CostoMenu
            Reporte.SetParameterValue(0, cboxmoneda.SelectedValue)
            CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, Reporte, , )

        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema:" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        Finally
            Wait.Close()
            BMostrar.Enabled = True
        End Try
    End Sub
#End Region

#Region "Cerrar"
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BSalir.Click
        Me.Close()
    End Sub
#End Region

#Region "Controles Funciones"
    Private Sub cboxmoneda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboxmoneda.KeyDown
        If e.KeyCode = Keys.Enter Then
            BMostrar.Focus()
        End If
    End Sub

    Private Sub Button3_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles BMostrar.KeyDown
        If e.KeyCode = Keys.Enter Then
            Mostrar()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BMostrar.Click
        Mostrar()
    End Sub
#End Region

End Class