﻿Imports System.ComponentModel

Public Class frmReporteVarios
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim DataS As New DataSet
    Dim PMU As New PerfilModulo_Class
    Dim cedula As String
    Dim puntos As Integer = 0

    Sub cargarHoraUltimaApertura()
        Dim dt As New DataTable
        Dim sql As String = "SELECT MAX(Fecha) AS ULTIMA fROM         aperturacaja WHERE     (Anulado = 0)"
        cFunciones.Llenar_Tabla_Generico(sql, dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
        If dt.Rows.Count > 0 Then
            Me.fechaInicio.Value = dt.Rows(0).Item(0)
        End If
    End Sub

    Private Sub frmReporteVarios_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Me.cmbTipoVentaReporte.SelectedIndex = 0
            Dim FechaFinal As Boolean = False
            If GetSetting("SeeSoft", "Restaurante", "ReportesFechaFinalNow").Equals("1") Then FechaFinal = True Else SaveSetting("SeeSoft", "Restaurante", "ReportesFechaFinalNow", "0")

            Dim FechaInicio As Boolean = False
            If GetSetting("SeeSoft", "Restaurante", "ReportesFechaInicioNow").Equals("1") Then FechaInicio = True Else SaveSetting("SeeSoft", "Restaurante", "ReportesFechaInicioNow", "0")

            Text = "Reportes " & User_Log.NombrePunto

            If FechaInicio Then
                Me.fechaInicio.Value = DateAdd(DateInterval.Day, -Now.Day + 1, Now.Date).AddHours(6)

            Else
                cargarHoraUltimaApertura()
            End If


            Me.fechaFinal.Value = IIf(FechaFinal, Now, Now.Date.AddHours(23).AddMinutes(59))
            carga_cbox()
            Controles(False)

            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Hotel")
            Dim NoClave As Boolean = cConexion.SlqExecuteScalar(conectadobd, "SELECT NoClave FROM Configuraciones")
            If NoClave Then
                Loggin_Usuario()
            Else
                TextBox3.Focus()
            End If
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Restaurante")
            '---------------------------------------------------------------
        Catch ex As Exception
            MessageBox.Show("Error al cargar: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub frmReporteVarios_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        cConexion.DesConectar(conectadobd)
    End Sub


    Private Sub carga_cbox()
        'carga los tipos de moneda
        conectadobd = cConexion.Conectar("Seguridad")
        cConexion.GetDataSet(conectadobd, "Select * from Moneda", DataS, "Moneda")
        Dim fila As DataRow
        For Each fila In DataS.Tables("Moneda").Rows
            cboxmoneda.Items.Add(fila("MonedaNombre"))
        Next
        cConexion.DesConectar(conectadobd)
        conectadobd = cConexion.Conectar("Hotel")
        'carga los clientes
        cConexion.GetDataSet(conectadobd, "Select Id, Nombre from Cliente", DataS, "Cliente")
    End Sub

    Dim cargoBien As Boolean = False
    Sub spMuestraReporte()

        Try
            CrystalReportViewer1.ReportSource = Nothing

            PMU = VSM(cedula, "MuestraReportes")
            If PMU.Find Then

                If fechaInicio.Text = "" Or fechaFinal.Text = "" Or cboxmoneda.SelectedItem = "" Then
                    MessageBox.Show("Debe llenar los campos de fechas y tipo de moneda", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

                'reporte detallado
                If rbdetalladas.Checked = True Then
                    Dim Reporte As New rptVarios
                    Reporte.SetParameterValue(0, "REPORTE DE VENTAS ACOMULADAS '" & Me.fechaInicio.Text & "' HASTA EL '" & Me.fechaFinal.Text & "'")
                    Reporte.SetParameterValue(1, CDbl(txtvalormoneda.Text))
                    Reporte.SetParameterValue(2, CStr(cboxmoneda.SelectedItem))
                    Reporte.SetParameterValue(3, fechaInicio.Value)
                    Reporte.SetParameterValue(4, fechaFinal.Value)
                    Reporte.SetParameterValue(5, User_Log.PuntoVenta)
                    Reporte.SetParameterValue(6, CStr(User_Log.NombrePunto))
                    Reporte.SetParameterValue(7, CStr(cmbTipoVentaReporte.SelectedIndex))
                    'Me.CrystalReportViewer1.SelectionFormula = "{Ventas.Fecha} >= {?FechaInicialPrincipal} and {Ventas.Fecha} <={?FechaFinalPrincipal} and {Ventas.Anulado} = false and {Ventas.Num_Factura} <> 0 and {Ventas.Proveniencia_Venta} = {?PuntodeVenta} "
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, Reporte, , conectadobd.ConnectionString)
                    cargoBien = True
                ElseIf Me.RBtnCostoVentas.Checked = True Then
                    Dim Reporte As New rptCostoVentaVarios
                    Reporte.SetParameterValue(0, fechaInicio.Value)
                    Reporte.SetParameterValue(1, fechaFinal.Value)
                    Reporte.SetParameterValue(2, User_Log.PuntoVenta)
                    Reporte.SetParameterValue(3, CDbl(txtvalormoneda.Text))
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, Reporte, False, GetSetting("SeeSOFT", "Restaurante", "Conexion"))

                End If
            Else : MsgBox("No tiene permiso para buscar información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Dim primerTiro As Boolean = True
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        spMuestraReporte()
        If primerTiro And cargoBien Then
            spMuestraReporte()
            primerTiro = False
        End If

    End Sub



    Private Sub cboxmoneda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxmoneda.SelectedIndexChanged
        Dim row As DataRow
        For Each row In DataS.Tables("Moneda").Rows
            If cboxmoneda.SelectedItem = row("MonedaNombre") Then
                txtvalormoneda.Text = row("ValorCompra")
            End If
        Next
    End Sub

    Private Sub rbdetalladas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbdetalladas.CheckedChanged
        Reporte_Seleccionado()
    End Sub

    Private Sub Reporte_Seleccionado()
        Me.cboxmoneda.Enabled = True
        cboxcliente.Enabled = False

        cboxcliente.Text = ""
        Label4.Enabled = False
        txtmonto.Enabled = False
    End Sub

    Private Sub rddetalladaspunto_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub rbdetalladascliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub rbdiarias_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub rbEmpleado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub rbFamilia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub rbfiscal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub rbtipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub rbAnuladas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub cboxcliente_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboxcliente.KeyDown
        If e.KeyCode = Keys.Enter Then
            fechaInicio.Focus()
        End If
    End Sub

    Private Sub cboxcliente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxcliente.SelectedIndexChanged
        Dim row As DataRow
        For Each row In DataS.Tables("Cliente").Rows
            If cboxcliente.SelectedItem = row("Nombre") Then
                txtcodcliente.Text = row("Id")
            End If
        Next
    End Sub

    Private Sub fechaInicio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles fechaInicio.KeyDown
        If e.KeyCode = Keys.Enter Then
            fechaFinal.Focus()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub txtmonto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtmonto.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub fechaFinal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles fechaFinal.KeyDown
        If e.KeyCode = Keys.Enter Then
            cboxmoneda.Focus()
        End If
    End Sub

#Region "Validacion Usuario"
    Private Sub TextBox3_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox3.KeyDown
        If e.KeyCode = Keys.Enter Then
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Restaurante")
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox3.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                ' TextBox1.Text = ""
                Me.lbUsuario.Text = usuario
                Controles(True)
                fechaInicio.Focus()
            Else
                Me.Enabled = True
            End If
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Hotel")
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
                TextBox1.Text = ""
                Controles(True)
                fechaInicio.Focus()
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub RadioButtonventasporcategoria_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RBtnCostoVentas.CheckedChanged
        Reporte_Seleccionado()
        Me.cboxmoneda.Enabled = False
        Me.cboxmoneda.SelectedIndex = 0

    End Sub

    Private Sub RadioButton1_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Reporte_Seleccionado()
    End Sub

    Private Sub porcen_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If (e.KeyCode = Keys.Enter) Then
            Evalua()
        End If
    End Sub
    Public Function Evalua()
        Dim Res As Boolean = True
        Return Res
    End Function

    Private Sub porcen_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) And Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
        End If
    End Sub

    Private Sub Controles(ByVal estado As Boolean)
        Try
            GroupBox1.Enabled = estado
            cboxcliente.Enabled = estado
            fechaInicio.Enabled = estado
            fechaFinal.Enabled = estado
            cboxmoneda.Enabled = estado
            txtmonto.Enabled = estado
            Button1.Enabled = estado
        Catch ex As Exception
            MsgBox("Ocurio el siguiente problema " + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        End Try
    End Sub

    Private Sub RBCantidad_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cboxmoneda.Enabled = True
        cboxcliente.Enabled = False
    End Sub

    'Private Sub RBVendido_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    tarjetas.Visible = False
    '    porcen.Visible = False
    '    cboxmoneda.Enabled = True
    '    cboxcliente.Enabled = False
    'End Sub
    Function validar() As Boolean

        Return True
    End Function

    Private Sub RBcateg_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cboxmoneda.Enabled = True
        cboxcliente.Enabled = False
    End Sub

    Private Sub fechaInicio_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechaInicio.ValueChanged
        If Me.fechaInicio.Value.Date > Me.fechaFinal.Value.Date Then
            Me.fechaFinal.Value = Me.fechaInicio.Value
            Me.fechaFinal.Value = Me.fechaFinal.Value.AddDays(1)
        End If
    End Sub

    Private Sub fechaFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechaFinal.ValueChanged
        If Me.fechaFinal.Value.Date < Me.fechaInicio.Value.Date Then
            Me.fechaInicio.Value = Me.fechaFinal.Value
            Me.fechaInicio.Value = Me.fechaInicio.Value.AddDays(-1)
        End If
    End Sub

    Private Sub rbVentaXEmpleado_CheckedChanged(sender As Object, e As EventArgs)
        Reporte_Seleccionado()
    End Sub
End Class