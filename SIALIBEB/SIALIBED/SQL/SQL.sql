-- Este documento contiene las modificaciones necesarias para correr la aplicacion

/*Se agrega un nuevo campo a la tabla comandatemporal para llevar el control
  de las comandas que ya se han llevado a la mesa..
*/
alter table mesas add tem varchar(100);
update mesas set tem = '';
alter table comandatemporal add despachado int default 0;
update comandatemporal set despachado = 0;
alter table comandatemporal add horaDespachado datetime;

/* modificar la siguiente vista..*/
ALTER   VIEW dbo.SOLICITUDES
AS
SELECT     cCodigo, cMesa, solicitud, MIN(hora) AS HoraCarga, Impresora, despachado, dbo.ComandaTemporal.id
FROM         dbo.ComandaTemporal
GROUP BY cCodigo, cMesa, solicitud, Impresora,despachado,dbo.ComandaTemporal.id

/*MOdificamos la vista de las solicitudes en mesas*/
ALTER       VIEW dbo.SolicitudesEnMesas AS 
SELECT TOP 100 PERCENT 
dbo.SOLICITUDES.despachado, 
dbo.Mesas.Nombre_Mesa, 
dbo.SOLICITUDES.cCodigo, 
dbo.SOLICITUDES.solicitud, 
dbo.SOLICITUDES.cMesa, 
dbo.SOLICITUDES.Impresora,
avg( DATEDIFF ( n ,dbo.SOLICITUDES.HoraCarga, getdate())  ) as 'Tiempo_Espera' 
FROM 
dbo.SOLICITUDES 
INNER JOIN dbo.Mesas ON dbo.SOLICITUDES.cMesa = dbo.Mesas.Id 
group by dbo.SOLICITUDES.despachado,
dbo.Mesas.Nombre_Mesa,dbo.SOLICITUDES.cCodigo,dbo.SOLICITUDES.solicitud,
dbo.SOLICITUDES.cMesa, dbo.SOLICITUDES.Impresora

/*Agregamos un campo nuevo a la table de cortesias*/
alter table cortesia add Anulado bit default 0;