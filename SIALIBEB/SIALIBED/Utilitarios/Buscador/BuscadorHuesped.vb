Imports System.Data.SqlClient

Public Class BuscadorHuesped

#Region "Variables"
    Public nombre, idReservacion, idCliente As String
    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlConnection
    Dim DataS As New DataSet
    Dim DataV As DataView
#End Region

    Private Sub BuscadorHuesped_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Hotel")
        Dim str As String = ""

        If cFunciones.fnValidaIntegracionFdNuevo() Then
			str = "SELECT C.[Id] as Codigo,C.[Nombre],C.[FechaIngreso] as Fecha_Ingreso,C.[FechaSalida] as Fecha_Salida ,C.Id,C.[IdCliente] as Id_Cliente,C.Codigo as habitacion FROM [Proyecto].[dbo].[vs_FD_CuentaEnCasaCliente] as C inner join   [Proyecto].[dbo].tb_FD_CuentaEnCasaPuntoDeVenta as CPV on C.Id = CPV.IdCuentaEnCasa where  (C.Abierta = 1) and CPV.Credito = 1 and CPV.IdPuntoVenta = '" & GetSetting("TourCo", "Conf", "RE-IdPuntoVentaAsignado") & "'"
		Else
            'Dim str As String = "SELECT dbo.Cuentas.Id_Reservacion as habitacion, dbo.Cuentas.Nombre + ' -- ' + dbo.Reservacion.Nombre_Cliente  + '(' + CAST(Reservacion.Id_Reservacion AS varchar) + ')' AS Nombre, dbo.Reservacion.Fecha_Ingreso,  dbo.Reservacion.Fecha_Salida, dbo.Cuentas.Id, dbo.Cuentas.Id_Cliente FROM dbo.Cuentas INNER JOIN  dbo.Reservacion ON dbo.Cuentas.Id_Reservacion = dbo.Reservacion.Id_Reservacion WHERE Nombre like '%" & "" & "%' and cuentas.codigo LIKE '%" & "" & "%' and(dbo.Cuentas.Abierta = 1) AND (dbo.Cuentas.Codigo = '0') UNION  SELECT dbo.Cuentas.Codigo as habitacion, dbo.Cuentas.Codigo + ' -- ' + dbo.Cuentas.Nombre AS Nombre, dbo.Reservacion.Fecha_Ingreso, dbo.Reservacion.Fecha_Salida,  dbo.Cuentas.Id, dbo.Cuentas.Id_Cliente FROM dbo.Cuentas INNER JOIN  dbo.Reservacion ON dbo.Cuentas.Id_Reservacion = dbo.Reservacion.Id_Reservacion WHERE Nombre like '%" & "" & "%' and cuentas.codigo LIKE '%" & "" & "%' and (dbo.Cuentas.Abierta = 1) AND (dbo.Cuentas.Codigo <> '0')"
            If GetSetting("SeeSoft", "Hotel", "Todas").Equals("Si") Then
                str = "SELECT Cuentas.Id_Reservacion AS habitacion, ISNULL(Cliente.NombreJuridico, Cuentas.Nombre)  + ' -- ' + Reservacion.Nombre_Cliente + '(' + CAST(Reservacion.Id_Reservacion AS varchar) + ')' AS Nombre, Reservacion.Fecha_Ingreso,  Reservacion.Fecha_Salida, Cuentas.Id, Cuentas.Id_Cliente FROM Cuentas INNER JOIN  Reservacion ON Cuentas.Id_Reservacion = Reservacion.Id_Reservacion LEFT OUTER JOIN  Cliente ON Cuentas.Id_Cliente = Cliente.Id WHERE NombreJuridico like '%" & "" & "%' and cuentas.codigo LIKE '%" & "" & "%' and(dbo.Cuentas.Abierta = 1) AND (dbo.Cuentas.Codigo = '0') UNION  SELECT dbo.Cuentas.Codigo as habitacion, dbo.Cuentas.Codigo + ' -- ' + dbo.Cuentas.Nombre AS Nombre, dbo.Reservacion.Fecha_Ingreso, dbo.Reservacion.Fecha_Salida,  dbo.Cuentas.Id, dbo.Cuentas.Id_Cliente FROM dbo.Cuentas INNER JOIN  dbo.Reservacion ON dbo.Cuentas.Id_Reservacion = dbo.Reservacion.Id_Reservacion WHERE Nombre like '%" & "" & "%' and cuentas.codigo LIKE '%" & "" & "%' and (dbo.Cuentas.Abierta = 1) AND (dbo.Cuentas.Codigo <> '0') UNION SELECT 0 AS habitacion, dbo.Cuentas.Nombre + '--' + 'VISITA' AS Nombre, getdate(), getdate(), dbo.cuentas.id, 0 AS Id_Cliente FROM dbo.Cuentas WHERE dbo.Cuentas.Id_Reservacion = 0"
            Else
                str = "SELECT dbo.Cuentas.Codigo, dbo.Cuentas.Codigo + ' -- ' + dbo.Cuentas.Nombre AS Nombre, dbo.Reservacion.Fecha_Ingreso, dbo.Reservacion.Fecha_Salida,  dbo.Cuentas.Id, dbo.Cuentas.Id_Cliente FROM dbo.Cuentas INNER JOIN  dbo.Reservacion ON dbo.Cuentas.Id_Reservacion = dbo.Reservacion.Id_Reservacion WHERE (dbo.Cuentas.Abierta = 1) AND (dbo.Cuentas.Codigo <> '0')"

            End If
        End If


        '
        cConexion.GetDataSet(conectadobd, str, DataS, "Cuentas")
        DataV = New DataView(DataS.Tables("Cuentas"))
        Me.DataGridView1.DataSource = DataV
        Me.DataGridView1.Visible = True
        DataGridView1.Columns(4).Visible = False
        DataGridView1.Columns(5).Visible = False
        Me.DataGridView1.Columns(1).Width = 50
        Me.DataGridView1.Columns(1).Width = 300
        DataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically


    End Sub



    Private Sub TextBox1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyUp
        If Trim(TextBox1.Text).Length >= 0 Then
            If RadioButton1.Checked = True Then
				If TextBox1.Text <> "" Then
					Try

						DataV.RowFilter = "habitacion = " & TextBox1.Text
					Catch ex As Exception

					End Try
				Else
					DataV = New DataView(DataS.Tables("Cuentas"))
                End If
            Else
                DataV.RowFilter = "nombre Like '%" & TextBox1.Text & "%'"
            End If
            ElseIf Trim(TextBox1.Text).Length = 0 Then
                DataV = New DataView(DataS.Tables("Cuentas"))
            End If
        DataGridView1.DataSource = DataV ' .Table("Cuentas") 'customerOrders.Tables("Listado")
    End Sub

    Private Sub btnListo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cargar()
    End Sub

    Private Sub cargar()
        nombre = DataGridView1(1, DataGridView1.CurrentRow.Index).Value()
        idReservacion = DataGridView1(4, DataGridView1.CurrentRow.Index).Value()
        idCliente = DataGridView1(5, DataGridView1.CurrentRow.Index).Value()
        Close()
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        cargar()
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 4 : cargar()
            Case 7 : teclado()
            Case 8 : Close()

        End Select
    End Sub

    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicación", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
End Class