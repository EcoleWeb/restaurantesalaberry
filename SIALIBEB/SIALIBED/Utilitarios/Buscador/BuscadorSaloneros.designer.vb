<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BuscadorSaloneros
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BuscadorSaloneros))
        Me.TituloModulo = New System.Windows.Forms.Label
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtNombre = New System.Windows.Forms.TextBox
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar2 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarTeclado = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.rbplanilla = New System.Windows.Forms.RadioButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.rbUsuarios = New System.Windows.Forms.RadioButton
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TituloModulo
        '
        Me.TituloModulo.AccessibleDescription = Nothing
        Me.TituloModulo.AccessibleName = Nothing
        resources.ApplyResources(Me.TituloModulo, "TituloModulo")
        Me.TituloModulo.BackColor = System.Drawing.SystemColors.ControlLight
        Me.TituloModulo.ForeColor = System.Drawing.Color.White
        Me.TituloModulo.Name = "TituloModulo"
        '
        'DataGridView1
        '
        Me.DataGridView1.AccessibleDescription = Nothing
        Me.DataGridView1.AccessibleName = Nothing
        resources.ApplyResources(Me.DataGridView1, "DataGridView1")
        Me.DataGridView1.BackgroundImage = Nothing
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Font = Nothing
        Me.DataGridView1.Name = "DataGridView1"
        '
        'Label1
        '
        Me.Label1.AccessibleDescription = Nothing
        Me.Label1.AccessibleName = Nothing
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Font = Nothing
        Me.Label1.Name = "Label1"
        '
        'txtNombre
        '
        Me.txtNombre.AccessibleDescription = Nothing
        Me.txtNombre.AccessibleName = Nothing
        resources.ApplyResources(Me.txtNombre, "txtNombre")
        Me.txtNombre.BackgroundImage = Nothing
        Me.txtNombre.Name = "txtNombre"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "")
        Me.ImageList.Images.SetKeyName(9, "teclado1.gif")
        Me.ImageList.Images.SetKeyName(10, "")
        '
        'ToolBar2
        '
        Me.ToolBar2.AccessibleDescription = Nothing
        Me.ToolBar2.AccessibleName = Nothing
        resources.ApplyResources(Me.ToolBar2, "ToolBar2")
        Me.ToolBar2.BackgroundImage = Nothing
        Me.ToolBar2.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarEditar, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarTeclado, Me.ToolBarCerrar})
        Me.ToolBar2.Font = Nothing
        Me.ToolBar2.ImageList = Me.ImageList
        Me.ToolBar2.Name = "ToolBar2"
        '
        'ToolBarNuevo
        '
        resources.ApplyResources(Me.ToolBarNuevo, "ToolBarNuevo")
        '
        'ToolBarEditar
        '
        resources.ApplyResources(Me.ToolBarEditar, "ToolBarEditar")
        '
        'ToolBarBuscar
        '
        resources.ApplyResources(Me.ToolBarBuscar, "ToolBarBuscar")
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        '
        'ToolBarImprimir
        '
        resources.ApplyResources(Me.ToolBarImprimir, "ToolBarImprimir")
        '
        'ToolBarTeclado
        '
        resources.ApplyResources(Me.ToolBarTeclado, "ToolBarTeclado")
        '
        'ToolBarCerrar
        '
        resources.ApplyResources(Me.ToolBarCerrar, "ToolBarCerrar")
        '
        'rbplanilla
        '
        Me.rbplanilla.AccessibleDescription = Nothing
        Me.rbplanilla.AccessibleName = Nothing
        resources.ApplyResources(Me.rbplanilla, "rbplanilla")
        Me.rbplanilla.BackgroundImage = Nothing
        Me.rbplanilla.Font = Nothing
        Me.rbplanilla.Name = "rbplanilla"
        Me.rbplanilla.TabStop = True
        Me.rbplanilla.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AccessibleDescription = Nothing
        Me.Label2.AccessibleName = Nothing
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Font = Nothing
        Me.Label2.Name = "Label2"
        '
        'rbUsuarios
        '
        Me.rbUsuarios.AccessibleDescription = Nothing
        Me.rbUsuarios.AccessibleName = Nothing
        resources.ApplyResources(Me.rbUsuarios, "rbUsuarios")
        Me.rbUsuarios.BackgroundImage = Nothing
        Me.rbUsuarios.Font = Nothing
        Me.rbUsuarios.Name = "rbUsuarios"
        Me.rbUsuarios.TabStop = True
        Me.rbUsuarios.UseVisualStyleBackColor = True
        '
        'BuscadorSaloneros
        '
        Me.AccessibleDescription = Nothing
        Me.AccessibleName = Nothing
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Nothing
        Me.Controls.Add(Me.rbUsuarios)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.rbplanilla)
        Me.Controls.Add(Me.ToolBar2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.TituloModulo)
        Me.Font = Nothing
        Me.Icon = Nothing
        Me.Name = "BuscadorSaloneros"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Protected Friend WithEvents TituloModulo As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Public WithEvents ImageList As System.Windows.Forms.ImageList
    Public WithEvents ToolBar2 As System.Windows.Forms.ToolBar
    Protected Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarTeclado As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents rbplanilla As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbUsuarios As System.Windows.Forms.RadioButton
End Class
