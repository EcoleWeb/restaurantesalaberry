Imports System.Data.SqlClient
Public Class BuscadorSaloneros

#Region "Variables"
    Public nombre, cedula, direccion, telefono, sexo, turno As String
    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlConnection
    Public str, bd, tabla As String
    Public tipo As Boolean
    Dim DataS, dataS2 As New DataSet
    Dim DataV, dataV2 As DataView
#End Region

    Private Sub BuscadorSaloneros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar(bd)
        If tipo = True Then
            cConexion.GetDataSet(conectadobd, str, DataS, tabla)
            rbUsuarios.Visible = False
            rbplanilla.Visible = False
            Label2.Visible = False
        Else
            buscaUsuarios()
            buscaPlanilla()
        End If
        DataV = New DataView(DataS.Tables(tabla))
        Me.DataGridView1.DataSource = DataV
        Me.DataGridView1.Visible = True
        On Error GoTo sigue
        Me.DataGridView1.Columns(0).Width = 70
        Me.DataGridView1.Columns(1).Width = 250
        Me.DataGridView1.Columns(2).Width = 60
        Me.DataGridView1.Columns(3).Width = 70
        Me.DataGridView1.Columns(4).Width = 250
        Me.DataGridView1.Columns(5).Width = 180
        DataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically
sigue:
        turno = ""
        direccion = ""
        telefono = "0"
        sexo = ""
    End Sub

    Private Sub buscaPlanilla()
        rbUsuarios.Visible = True
        rbplanilla.Visible = True
        Label2.Visible = True
        rbplanilla.Checked = True

        str = "Select * from EmpleadosPlanilla"
        tabla = "EmpleadosPlanilla"
        cConexion.GetDataSet(conectadobd, str, DataS, tabla)
    End Sub

    Private Sub buscaUsuarios()
        rbUsuarios.Visible = True
        rbplanilla.Visible = True
        Label2.Visible = True
        rbUsuarios.Checked = True
        'str = "Select Cedula AS Cedula, Nombre from Usuarios"
        str = "Select Cedula , Nombre from Saloneros"
        tabla = "Saloneros"
        cConexion.GetDataSet(conectadobd, str, dataS2, tabla)
    End Sub
    Private Sub BuscadorSaloneros_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 4 : cargar()
            Case 7 : teclado()
            Case 8 : Close()

        End Select
    End Sub

    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicación", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub TextBox1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombre.KeyUp
        If Trim(txtNombre.Text).Length >= 0 Then
            DataV.RowFilter = "Nombre Like '%" & txtNombre.Text & "%'"
        ElseIf Trim(txtNombre.Text).Length = 0 Then
            DataV = New DataView(DataS.Tables(tabla))
        End If
        DataGridView1.DataSource = DataV
    End Sub


    Private Sub cargar()
        If IsDBNull(DataGridView1(0, DataGridView1.CurrentRow.Index).Value()) = True Then
            MessageBox.Show("Debe seleccionar un registro", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Exit Sub
        End If
        cedula = DataGridView1(0, DataGridView1.CurrentRow.Index).Value
        nombre = DataGridView1(1, DataGridView1.CurrentRow.Index).Value()
        If rbplanilla.Checked = True Then
            telefono = DataGridView1(2, DataGridView1.CurrentRow.Index).Value()
            direccion = DataGridView1(3, DataGridView1.CurrentRow.Index).Value()
            sexo = DataGridView1(4, DataGridView1.CurrentRow.Index).Value()
        ElseIf rbplanilla.Visible = False And rbUsuarios.Visible = False Then 'editando los saloneros
            sexo = DataGridView1(2, DataGridView1.CurrentRow.Index).Value()
            telefono = DataGridView1(3, DataGridView1.CurrentRow.Index).Value()
            direccion = DataGridView1(4, DataGridView1.CurrentRow.Index).Value()
            turno = DataGridView1(5, DataGridView1.CurrentRow.Index).Value()
        End If
        Close()
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        cargar()
    End Sub

    Private Sub rbplanilla_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbplanilla.CheckedChanged
        tabla = "EmpleadosPlanilla"
        TituloModulo.Text = "Personas en Planilla"
        DataV = New DataView(DataS.Tables(tabla))
        Me.DataGridView1.DataSource = DataV
    End Sub

    Private Sub rbUsuarios_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbUsuarios.CheckedChanged
        tabla = "Saloneros"
        TituloModulo.Text = "Usuarios del Sistema"
        DataV = New DataView(dataS2.Tables(tabla))
        Me.DataGridView1.DataSource = DataV
    End Sub

    Private Sub txtNombre_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.TextChanged

    End Sub
End Class