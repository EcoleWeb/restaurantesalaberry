Public Class BuscarCliente
    Inherits System.Windows.Forms.Form
    Public Pos As Integer
    Public Cliente As DataTable
    Public Dv As System.Data.DataView

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub


    Public Sub New(ByVal TablaCleinte As DataTable)
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        'Add any initialization after the InitializeComponent() call
        Cliente = TablaCleinte
    End Sub


    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents AdvBandedGridView1 As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtBusqueda As System.Windows.Forms.TextBox
    Friend WithEvents DataSetClientes1 As SIALIBEB.DatasetCliente
    Friend WithEvents colNombre As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colCedula As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BuscarCliente))
        Dim ColumnFilterInfo1 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo2 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.AdvBandedGridView1 = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
        Me.GridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.colCedula = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.colNombre = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.DataSetClientes1 = New SIALIBEB.DatasetCliente
        Me.btnCancelar = New System.Windows.Forms.Button
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.txtBusqueda = New System.Windows.Forms.TextBox
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetClientes1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        '
        '
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        resources.ApplyResources(Me.GridControl1, "GridControl1")
        Me.GridControl1.MainView = Me.AdvBandedGridView1
        Me.GridControl1.Name = "GridControl1"
        '
        'AdvBandedGridView1
        '
        Me.AdvBandedGridView1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand2})
        Me.AdvBandedGridView1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.colNombre, Me.colCedula})
        Me.AdvBandedGridView1.Name = "AdvBandedGridView1"
        Me.AdvBandedGridView1.OptionsView.ShowGroupedColumns = False
        Me.AdvBandedGridView1.OptionsView.ShowGroupPanel = False
        '
        'GridBand2
        '
        Me.GridBand2.Caption = "Clientes"
        Me.GridBand2.Columns.Add(Me.colCedula)
        Me.GridBand2.Columns.Add(Me.colNombre)
        Me.GridBand2.Name = "GridBand2"
        Me.GridBand2.Width = 354
        '
        'colCedula
        '
        resources.ApplyResources(Me.colCedula, "colCedula")
        Me.colCedula.FilterInfo = ColumnFilterInfo1
        Me.colCedula.Name = "colCedula"
        Me.colCedula.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCedula.Visible = True
        Me.colCedula.Width = 107
        '
        'colNombre
        '
        resources.ApplyResources(Me.colNombre, "colNombre")
        Me.colNombre.FilterInfo = ColumnFilterInfo2
        Me.colNombre.Name = "colNombre"
        Me.colNombre.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colNombre.Visible = True
        Me.colNombre.Width = 247
        '
        'DataSetClientes1
        '
        Me.DataSetClientes1.DataSetName = "DataSetClientes"
        Me.DataSetClientes1.Locale = New System.Globalization.CultureInfo("es-CR")
        Me.DataSetClientes1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        resources.ApplyResources(Me.btnCancelar, "btnCancelar")
        Me.btnCancelar.ForeColor = System.Drawing.Color.Transparent
        Me.btnCancelar.Name = "btnCancelar"
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.ForeColor = System.Drawing.Color.Transparent
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'txtBusqueda
        '
        Me.txtBusqueda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtBusqueda, "txtBusqueda")
        Me.txtBusqueda.Name = "txtBusqueda"
        '
        'BuscarCliente
        '
        resources.ApplyResources(Me, "$this")
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtBusqueda)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "BuscarCliente"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetClientes1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub BuscarCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dv = Cliente.DefaultView
        'Asignar las restricciones al dataview
        Dv.AllowDelete = False
        Dv.AllowEdit = False
        Dv.AllowNew = False
        Me.GridControl1.DataSource = Dv
    End Sub
    Private Sub BusquedaCliente(ByVal Texto As String)
        Dim filtro As String
        'Agencia
        filtro = "Nombre like '%" & Texto & "%'"
        Dv.RowFilter = filtro
    End Sub
    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If txtBusqueda.Text.Length > 2 Then
            Me.BusquedaCliente(Me.txtBusqueda.Text)
        Else
            Me.BusquedaCliente("")
        End If
    End Sub

    Private Sub BuscargripHabitacion()
        Dim hi As DevExpress.XtraGrid.Views.BandedGrid.ViewInfo.BandedGridHitInfo = _
                AdvBandedGridView1.CalcHitInfo(CType(GridControl1, Control).PointToClient(Control.MousePosition))

        If hi.RowHandle >= 0 Then

            BuscarById(AdvBandedGridView1.GetDataRow(hi.RowHandle))
            Me.DialogResult = DialogResult.OK

        ElseIf AdvBandedGridView1.FocusedRowHandle >= 0 Then

            BuscarById(AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle))
            Me.DialogResult = DialogResult.OK
        Else
            BuscarById(Nothing)
        End If
    End Sub

    Private Sub BuscarById(ByVal dr As DataRow)
        Dim s As String = ""
        If Not dr Is Nothing Then
            BuscarPorId(dr("Id"))
        End If
    End Sub
    Private Sub BuscarPorId(ByVal Id As Integer)
        Dim Posicion As Integer
        Dim Vista = New DataView(Cliente)
        Vista.Sort = "Id"
        Posicion = Vista.Find(Id)
        Pos = Posicion
    End Sub


    Private Sub GridControl1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridControl1.Click
        BuscargripHabitacion()
    End Sub

    Private Sub txtBusqueda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBusqueda.KeyDown
        If e.KeyCode = Keys.Enter Then
            BuscargripHabitacion()
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

            BuscargripHabitacion()

    End Sub

End Class
