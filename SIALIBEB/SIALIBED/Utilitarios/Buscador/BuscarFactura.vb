Public Class BuscarFactura

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim datas As New DataSet
    Public cedula As String
    Dim PMU As New PerfilModulo_Class
#End Region

    Private Sub BuscaAnularFactura_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub


    'este procedimient busca las cortesias..
    Private Sub buscarCortesias()

        datas.Clear()
        Me.DataGridView1.DataSource = Nothing
        Me.DataGridView1.Rows.Clear()

        Dim str As String
        'BUSCA CORTESIA
        If conectadobd.State = ConnectionState.Open Then conectadobd.Close()
        conectadobd = cConexion.Conectar("Restaurante")
        PMU = VSM(User_Log.Cedula, "Cortesias")
        If PMU.Find Then

            str = " NumeroCortesia like '%" & txtnfactura.Text & "%' "
            str += "and ( DBO.DATEONLY(Fecha)  >= '" & Me.fechaInicio.Text & "') and ( DBO.DATEONLY(Fecha) <= '" & Me.FechaHasta.Text & "')"

            cConexion.GetDataSet(conectadobd, "SELECT Cortesia.id_cortesia AS ID, Cortesia.numero_cortesia AS Num, Comanda.Fecha, Cortesia.autorizado + REPLACE(CAST(REPLACE(CAST(Cortesia.Anulado AS VARCHAR), '1', ' (ANULADO)') AS VARCHAR), '0', '') as Autorizado,  Cortesia.descripcion_cuenta AS Cuenta FROM Cortesia INNER JOIN  Comanda ON Cortesia.numero_cortesia = Comanda.NumeroCortesia WHERE Comanda.cortesia = 1 and " & str & "", datas, "Comanda")
            DataGridView1.DataSource = datas.Tables("Comanda")
            DataGridView1.Columns(0).Visible = False
            DataGridView1.Columns(1).Width = 80
            DataGridView1.Columns(3).Width = 270

        Else : MsgBox("No tiene permiso para buscar información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
        End If

    End Sub

    'este procedimient busca las cortesias..
    Private Sub buscarFacturas()

        Dim str As String

        datas.Clear()
        Me.DataGridView1.DataSource = Nothing
        Me.DataGridView1.Rows.Clear()

        conectadobd = cConexion.Conectar("Hotel")
        PMU = VSM(User_Log.Cedula, "Factura")
        If PMU.Find Then

            str = " Num_Factura like '%" & txtnfactura.Text & "%' "
            str += " and ( DBO.DATEONLY(Fecha)  >= '" & Me.fechaInicio.Text & "') and ( DBO.DATEONLY(Fecha) <= '" & Me.FechaHasta.Text & "')"

			cConexion.GetDataSet(conectadobd, "Select Id, Num_Factura as Factura, Tipo, Nombre_Cliente + REPLACE(CAST(REPLACE(CAST(Anulado AS VARCHAR), '1', ' (ANULADO)') AS VARCHAR), '0', '') as Cliente, (cast(Total as varchar)+ ' ' + Moneda_Nombre) as Total, Descuento, SubTotal, Imp_Venta, Monto_Saloero, Moneda_Nombre, Total as TTotal,Mesa,Anulado,Observacion from Ventas where Proveniencia_Venta=" & User_Log.PuntoVenta & " and " & str & " order by Num_Factura desc", datas, "Ventas")
			'cConexion.GetDataSet(conectadobd, "Select Id, Num_Factura as Factura, Tipo, Nombre_Cliente + REPLACE(CAST(REPLACE(CAST(Anulado AS VARCHAR), '1', ' (ANULADO)') AS VARCHAR), '0', '') as Cliente, (cast(Total as varchar)+ ' ' + Moneda_Nombre) as Total from Ventas where Proveniencia_Venta=" & User_Log.PuntoVenta & " and " & str & "", datas, "Ventas")
			DataGridView1.DataSource = datas.Tables("Ventas")
            DataGridView1.Columns(0).Visible = False
            DataGridView1.Columns(1).Width = 60
            DataGridView1.Columns(2).Width = 50
            DataGridView1.Columns(3).Width = 200
            DataGridView1.Columns(5).Visible = False
            DataGridView1.Columns(6).Visible = False
            DataGridView1.Columns(7).Visible = False
            DataGridView1.Columns(8).Visible = False
            DataGridView1.Columns(9).Visible = False
            DataGridView1.Columns(10).Visible = False
			DataGridView1.Columns(12).Visible = False
			DataGridView1.Columns(13).Visible = False
		Else : MsgBox("No tiene permiso para buscar información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
        End If

    End Sub



    Private Sub buscarFacturasTexto()

        Dim str As String

        datas.Clear()
        Me.DataGridView1.DataSource = Nothing
        Me.DataGridView1.Rows.Clear()

        conectadobd = cConexion.Conectar("Hotel")
        PMU = VSM(User_Log.Cedula, "Factura")


        If PMU.Find Then

            str = " Num_Factura like '%" & txtnfactura.Text & "%' "
            'str += " and ( DBO.DATEONLY(Fecha)  >= '" & Me.fechaInicio.Text & "') and ( DBO.DATEONLY(Fecha) <= '" & Me.FechaHasta.Text & "')"

            cConexion.GetDataSet(conectadobd, "Select Id, Num_Factura as Factura, Tipo, Nombre_Cliente + REPLACE(CAST(REPLACE(CAST(Anulado AS VARCHAR), '1', ' (ANULADO)') AS VARCHAR), '0', '') as Cliente, (cast(Total as varchar)+ ' ' + Moneda_Nombre) as Total, Descuento, SubTotal, Imp_Venta, Monto_Saloero, Moneda_Nombre, Total as TTotal,Mesa,Anulado,Observacion from Ventas where Proveniencia_Venta=" & User_Log.PuntoVenta & " and " & str & " order by Num_Factura desc", datas, "Ventas")
            'cConexion.GetDataSet(conectadobd, "Select Id, Num_Factura as Factura, Tipo, Nombre_Cliente + REPLACE(CAST(REPLACE(CAST(Anulado AS VARCHAR), '1', ' (ANULADO)') AS VARCHAR), '0', '') as Cliente, (cast(Total as varchar)+ ' ' + Moneda_Nombre) as Total from Ventas where Proveniencia_Venta=" & User_Log.PuntoVenta & " and " & str & "", datas, "Ventas")
            DataGridView1.DataSource = datas.Tables("Ventas")
            DataGridView1.Columns(0).Visible = False
            DataGridView1.Columns(1).Width = 60
            DataGridView1.Columns(2).Width = 50
            DataGridView1.Columns(3).Width = 200
            DataGridView1.Columns(5).Visible = False
            DataGridView1.Columns(6).Visible = False
            DataGridView1.Columns(7).Visible = False
            DataGridView1.Columns(8).Visible = False
            DataGridView1.Columns(9).Visible = False
            DataGridView1.Columns(10).Visible = False
			DataGridView1.Columns(12).Visible = False
			DataGridView1.Columns(13).Visible = False
		Else : MsgBox("No tiene permiso para buscar información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
        End If

    End Sub


   
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub btnVer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVer.Click
        Dim numerocomanda, comenzales As Integer
        Dim factura As Int64
        Dim Anula As Boolean
        Dim mesa As String = ""
        Dim idmesa As String = ""
        Dim subTotalesC, IVsC, codMonedaC, TotalesC As Double
        If Me.RadioButtonFac.Checked = True Then
            Try
                factura = DataGridView1(1, DataGridView1.CurrentRow.Index).Value()
            Catch ex As Exception
                MsgBox("Debe de seleccionar una factura antes de continuar...", MsgBoxStyle.Information, "Atención")
                Exit Sub
            End Try


            Try
                Dim cont As Integer = 0
                If IsNumeric(factura) = False Then
                    MessageBox.Show("Verifique la selección del registro", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

                If Me.RadioButtonFac.Checked = True Then
                    cConexion.DesConectar(conectadobd)
                    conectadobd = cConexion.Conectar("Restaurante")
                    cConexion.GetRecorset(conectadobd, "SELECT Comanda.NumeroComanda, Comanda.Comenzales, Mesas.Nombre_Mesa, Mesas.Id as IdMesa FROM Comanda INNER JOIN Mesas ON Comanda.IdMesa = Mesas.Id WHERE Comanda.Numerofactura = " & factura, rs)
                    While rs.Read
                        numerocomanda = rs("NumeroComanda")
                        comenzales = rs("Comenzales")
                        mesa = rs("Nombre_Mesa")
                        idmesa = rs("IdMesa")
                        cont += 1
                    End While
                    rs.Close()
                    If cont = 0 Then
                        cConexion.GetRecorset(conectadobd, "SELECT NumeroComanda,Comenzales, IdMesa FROM Comanda WHERE Numerofactura = " & factura, rs)
                        While rs.Read
                            numerocomanda = rs("NumeroComanda")
                            comenzales = rs("Comenzales")
                            idmesa = rs("IdMesa")
                            mesa = 0
                        End While
                        rs.Close()
                    End If
                    cConexion.DesConectar(conectadobd)

                ElseIf Me.RadioButtonCortesia.Checked = True Then 'para las cortesias
                    cConexion.DesConectar(conectadobd)
                    conectadobd = cConexion.Conectar("Restaurante")
                    cConexion.GetRecorset(conectadobd, "SELECT Comanda.NumeroComanda, Comanda.Comenzales, comanda.subtotal, comanda.impuesto_venta,comanda.total, comanda.cod_moneda, Mesas.Nombre_Mesa, Mesas.Id as IdMesa FROM Comanda INNER JOIN Mesas ON Comanda.IdMesa = Mesas.Id WHERE Comanda.NumeroCortesia = " & factura, rs)
                    While rs.Read
                        numerocomanda = rs("NumeroComanda")
                        comenzales = rs("Comenzales")
                        mesa = rs("Nombre_Mesa")
                        subTotalesC += rs("Subtotal")
                        IVsC += rs("Impuesto_venta")
                        TotalesC += rs("Total")
                        codMonedaC = rs("Cod_Moneda")
                        idmesa = rs("IdMesa")
                        cont += 1
                    End While
                    rs.Close()
                    If cont = 0 Then
                        cConexion.GetRecorset(conectadobd, "SELECT NumeroComanda,Comenzales,Subtotal, Impuesto_venta, Total, Cod_Moneda , IdMesa FROM Comanda WHERE NumeroCortesia = " & factura, rs)
                        While rs.Read
                            numerocomanda = rs("NumeroComanda")
                            comenzales = rs("Comenzales")
                            subTotalesC += rs("Subtotal")
                            IVsC += rs("Impuesto_venta")
                            TotalesC += rs("Total")
                            codMonedaC = rs("Cod_Moneda")
                            idmesa = rs("IdMesa")
                            mesa = 0
                        End While
                        rs.Close()
                    End If
                    cConexion.DesConectar(conectadobd)
                End If

                Dim cFacturaVenta As New Factura
                If cont = 0 Then
                    cFacturaVenta.tipo_factura = False
                Else
                    cFacturaVenta.tipo_factura = True
                End If
                cFacturaVenta.nombremesa = mesa
                cFacturaVenta.idmesa = idmesa
                cFacturaVenta.numeroComanda = numerocomanda
                cFacturaVenta.Comenzales = comenzales
                cFacturaVenta.separada = False
                cFacturaVenta.reimprimir = True
                cFacturaVenta.ButtonCambioFV.Visible = True
                cFacturaVenta.NFactura = factura
                cFacturaVenta.idfactura = DataGridView1(0, DataGridView1.CurrentRow.Index).Value()
                conectadobd = cConexion.Conectar("Restaurante")
                cFacturaVenta.anular = True
                cConexion.DesConectar(conectadobd)

                If Me.RadioButtonFac.Checked = True Then
                    cFacturaVenta.txtcliente.Text = DataGridView1(3, DataGridView1.CurrentRow.Index).Value()
                    cFacturaVenta.TextBoxDescuento.Text = DataGridView1(5, DataGridView1.CurrentRow.Index).Value()
                    cFacturaVenta.txtSubtotal.Text = DataGridView1(6, DataGridView1.CurrentRow.Index).Value()
                    cFacturaVenta.txtIV.Text = DataGridView1(7, DataGridView1.CurrentRow.Index).Value()
                    cFacturaVenta.txtIS.Text = DataGridView1(8, DataGridView1.CurrentRow.Index).Value()
                    cFacturaVenta.cboxMoneda.Text = DataGridView1(9, DataGridView1.CurrentRow.Index).Value()
					cFacturaVenta.txtTotal.Text = DataGridView1(10, DataGridView1.CurrentRow.Index).Value()
					cFacturaVenta.txtObservacion1.Text = DataGridView1(13, DataGridView1.CurrentRow.Index).Value()

					If DataGridView1(2, DataGridView1.CurrentRow.Index).Value() = "CON" Then
                        cFacturaVenta.rbContado.Checked = True
                    ElseIf DataGridView1(2, DataGridView1.CurrentRow.Index).Value() = "CRE" Then
                        cFacturaVenta.rbCredito.Checked = True
                    End If
                ElseIf Me.RadioButtonCortesia.Checked = True Then 'para cortesias
                    cFacturaVenta.vCortesia = True
                    cFacturaVenta.txtcliente.Text = "Cortesía"
                    cFacturaVenta.TextBoxDescuento.Text = 0
                    cFacturaVenta.txtSubtotal.Text = subTotalesC
                    cFacturaVenta.txtIV.Text = IVsC
                    cFacturaVenta.txtIS.Text = 0
                    cFacturaVenta.cboxMoneda.Text = codMonedaC
                    cFacturaVenta.txtTotal.Text = TotalesC
                    cFacturaVenta.cbkCortesia.Checked = True
                End If
                cFacturaVenta.GroupBox2.Enabled = False
                'cFacturaVenta.GroupBox4.Enabled = False
                cFacturaVenta.txtObservaciones.Enabled = False
                cFacturaVenta.cboxMoneda.Enabled = False

                cFacturaVenta.CedulaU = User_Log.Cedula


                cConexion.Conectar("Restaurante")
                If Me.RadioButtonFac.Checked = True Then
                    Anula = cConexion.SlqExecuteScalar(cConexion.SqlConexion, "SELECT ANULADO FROM VENTAS WHERE ID =" & cFacturaVenta.idfactura)
                    cFacturaVenta.Anul = Anula
                    cFacturaVenta.ToolBarEliminar.Enabled = Anula
                    cFacturaVenta.ToolBarImprimir.Visible = True
                ElseIf Me.RadioButtonCortesia.Checked = True Then 'para cortesias
                    Anula = cConexion.SlqExecuteScalar(cConexion.SqlConexion, "SELECT ANULADO FROM Cortesia WHERE id_cortesia =" & factura)
                    cFacturaVenta.Anul = Anula
                    cFacturaVenta.ToolBarEliminar.Enabled = Anula
                    cFacturaVenta.ToolBarImprimir.Visible = True
                End If

                Hide()
                cFacturaVenta.ShowDialog()
                'Me.Close()
                Show()

            Catch ex As Exception
                MessageBox.Show("Error al anular la factura, verifique la selección del registro" & vbCrLf & Err.Description, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            End Try

        End If

    End Sub

    Dim cargado As Boolean = False
    Private Sub BuscaFactura_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        'cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & clave & "'")
        'Me.Enabled = VerificandoAcceso_a_Modulos(Me, cedula)
        'Dim usuario As String = LoginUsuario(cConexion, conectadobd, clave, Me.Text, Me.Name)
        Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

        If cedula <> "" Then
            Me.lbUsuario.Text = usuario
            Buscar.Enabled = True
        Else
            Me.Enabled = True
        End If
        cConexion.DesConectar(conectadobd)
        conectadobd = cConexion.Conectar("Hotel")

        Me.cargado = True

        'Mostramos la busqueda inicial de las facturas
        'Me.buscarFacturas()
        Me.PictureBox1_Click(sender, e)

    End Sub

    Private Sub txtnfactura_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtnfactura.KeyDown
        If e.KeyCode = Keys.Enter Then

            If Me.RadioButtonFac.Checked = True Then
                Me.buscarFacturas()
            End If
        End If
    End Sub

    Private Sub txtnfactura_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtnfactura.KeyPress
        If Not (Char.IsDigit(e.KeyChar) Or Char.IsControl(e.KeyChar)) Then
            e.Handled = True
        End If
    End Sub

    Private Sub fechaInicio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles fechaInicio.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.FechaHasta.Focus()
        End If
    End Sub

    Private Sub FechaHasta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles FechaHasta.KeyDown
        If e.KeyCode = Keys.Enter Then

        End If
    End Sub

    Private Sub rbfactura_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
       
    End Sub

    Private Sub rbfecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
       
    End Sub

    Private Sub Buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Dim miFacturas As New cls_Facturas()
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim numCortesia As Int64
        Try
            If Me.RadioButtonFac.Checked Then
                Dim Impresora As String
                Dim num_factura As String = Me.DataGridView1.Item(1, Me.DataGridView1.CurrentRow.Index).Value
                Impresora = Busca_Impresora()
                If Impresora = "" Then
                    MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Impresora = Busca_Impresora()
                    If Impresora = "" Then
                        Exit Sub
                    End If
                End If

                Dim dt1 As New DataTable
                cFunciones.Llenar_Tabla_Generico("Select NumeroComanda From Comanda Where Numerofactura = " & Me.BindingContext(datas, "Ventas").Current("Factura"), dt1)

                If (GetSetting("SeeSOFT", "Restaurante", "Rapido") = 1) Then
                    Dim factura As New cls_Facturas()
                    factura.ImprimirFacturaEsp(num_factura, dt1.Rows(0).Item(0), Impresora, False)
                    Exit Sub
                End If

                FacturaPVE.Refresh()
                FacturaPVE.PrintOptions.PrinterName = Impresora
                FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                FacturaPVE.SetParameterValue(0, num_factura)

                FacturaPVE.SetParameterValue(1, User_Log.PuntoVenta)
               If dt1.Rows.Count > 0 Then

                    FacturaPVE.SetParameterValue(2, dt1.Rows(0).Item(0))

                Else
                    FacturaPVE.SetParameterValue(2, 0)
                End If
                FacturaPVE.SetParameterValue(3, False)
                Dim dt As New DataTable
                cFunciones.Llenar_Tabla_Generico("SELECT Comanda.CedSalonero, Usuarios_1.Nombre AS NCajero,  ISNULL(Usuarios_2.Nombre, '') AS NSalonero, Comanda.Comenzales FROM Comanda INNER JOIN Usuarios Usuarios_1 ON Comanda.Cedusuario = Usuarios_1.Cedula LEFT OUTER JOIN Usuarios Usuarios_2 ON Comanda.CedSalonero = Usuarios_2.Cedula WHERE (Comanda.Numerofactura = '" & num_factura & "')", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                'EL TIPO 0 Sera para factura muy simple con margen pegado a la izq
                If GetSetting("SeeSoft", "Restaurante", "Impresion") = "0" Then
                    If dt.Rows.Count > 0 Then
                        FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                    Else
                        FacturaPVE.SetParameterValue(4, 0)
                    End If
                    'impresion de tipo 1 formato ampliado a la derecha
                ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = "1" Or GetSetting("SeeSoft", "Restaurante", "Impresion") = "2" Then
                    If dt.Rows.Count > 0 Then
                        FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                        FacturaPVE.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
                        FacturaPVE.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
                        FacturaPVE.SetParameterValue(7, dt.Rows(0).Item("Comenzales"))
                        FacturaPVE.SetParameterValue(8, 0)
                    Else
                    FacturaPVE.SetParameterValue(4, "")
                    FacturaPVE.SetParameterValue(5, "")
                    FacturaPVE.SetParameterValue(6, "")
                    FacturaPVE.SetParameterValue(7, 0)
                End If
                    End If


                FacturaPVE.PrintToPrinter(1, True, 0, 0)

                Exit Sub
            Else
                numCortesia = DataGridView1(1, DataGridView1.CurrentRow.Index).Value()
                imprimirCortesia(numCortesia)
            End If

        Catch ex As Exception
            MsgBox("Debe de seleccionar una factura antes de continuar...", MsgBoxStyle.Information, "Atención")
            Exit Sub
        End Try

    End Sub
    Private Sub imprimirCortesia(ByVal numero_cortesia As Integer)
        Try
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("Select NumeroComanda, Idcomanda, autorizado, observaciones From ImpresionCortesia WHERE Num = " & numero_cortesia, dt)

            Dim facturaPVE As New rptCortesias
            Dim impresora As String
            impresora = Busca_Impresora()
            If impresora = "" Then
                MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                impresora = Busca_Impresora()
                If impresora = "" Then
                    Exit Sub
                End If
            End If

            facturaPVE.PrintOptions.PrinterName = impresora
            facturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            facturaPVE.SetParameterValue(0, dt.Rows(0).Item("NumeroComanda"))
            facturaPVE.SetParameterValue(1, numero_cortesia)
            facturaPVE.SetParameterValue(2, dt.Rows(0).Item("autorizado"))
            'Falso para no mostrar los costos de la cortesía, True para mostrarlos.
            If User_Log.CostoCortesia = True Then
                facturaPVE.SetParameterValue(3, True)
            Else
                facturaPVE.SetParameterValue(3, False)
            End If
            facturaPVE.SetParameterValue(4, dt.Rows(0).Item("observaciones"))

            CrystalReportsConexion.LoadReportViewer(Nothing, facturaPVE, True, GetSetting("SeeSoft", "Restaurante", "Conexion"))

            facturaPVE.PrintToPrinter(1, True, 0, 0)
            facturaPVE.Dispose()
            facturaPVE.Close()
            facturaPVE = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Imprimir(ByVal NFactura As Integer, ByVal Idioma As String)
        Dim FacturaPVE
        Dim cedula As String
        Dim cnx As New Conexion
        cedula = cnx.SlqExecuteScalar(conectadobd, "Select Cedula from Configuraciones")
        'Arenal Springs usa unas impresoras q no se xq se corren a la derecha x eso ahy un reporte para ellos.
        'Esta es la cedula juridica de Arenal: '3-101-374928-30
        If cedula = "3-101-374928-30" Then
            If Idioma = "espanol" Then
                FacturaPVE = New RptFacturaPVE_ArenalVersion
            Else
                FacturaPVE = New RptFacturaPVE_ArenalIngles
            End If
        End If

        If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
            If Idioma = "espanol" Then
                FacturaPVE = New RptFacturaPVE1   'Generica
            Else
                FacturaPVE = New RptFacturaPVEIngles1
            End If
        ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = 2 Then
            If Idioma = "espanol" Then
                FacturaPVE = New RptFacturaPVECB    'Generica
            Else
                FacturaPVE = New RptFacturaPVEIngles1
            End If
        Else
            If Idioma = "espanol" Then
                FacturaPVE = New RptFacturaPVE  'Generica
            Else
                FacturaPVE = New RptFacturaPVEIngles
            End If

        End If
        Dim impresora As String
        impresora = Busca_Impresora()
        If impresora = "" Then
            MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            impresora = Busca_Impresora()
            If impresora = "" Then
                Exit Sub
            End If
        End If
        Dim Conexion As String = GetSetting("SeeSOFT", "Restaurante", "Conexion")
        Dim salon As String
        conectadobd = cnx.Conectar("Restaurante")
        salon = cnx.SlqExecuteScalar(conectadobd, "Select Cedsalonero from comanda where numerofactura=" & NFactura)
        CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVE, True, Conexion)

        FacturaPVE.PrintOptions.PrinterName = impresora
        FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
        FacturaPVE.SetParameterValue(0, NFactura)
        FacturaPVE.SetParameterValue(1, User_Log.PuntoVenta)
        FacturaPVE.SetParameterValue(2, 0)
        FacturaPVE.SetParameterValue(3, DataGridView1(12, DataGridView1.CurrentRow.Index).Value())
        FacturaPVE.SetParameterValue(4, salon)
        FacturaPVE.PrintToPrinter(1, True, 0, 0)


        Dim Visor As New frmVisorReportes
        Visor.rptViewer.ReportSource = FacturaPVE
        Visor.rptViewer.Show()
        Visor.ShowDialog()


    End Sub
    '
    Public Sub cort()
        Dim intFileNo As Integer = FreeFile()
        FileOpen(1, "c:\corte.txt", OpenMode.Output)
        PrintLine(1, Chr(29) & Chr(86) & Chr(0))
        FileClose(1)
        Shell("print LPT1 c:\corte.txt", AppWinStyle.NormalFocus)
    End Sub
   

    Private Sub RadioButtonCortesia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonCortesia.CheckedChanged
        If RadioButtonCortesia.Checked Then
            Me.Label4.Text = "Cortesia"
            Me.RadioButtonCortesia.BackgroundImage = My.Resources.cuadro2
            Me.RadioButtonFac.BackgroundImage = My.Resources.cuadro1
            buscarCortesias()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim factura As Int64
        Dim numCortesia As Int64
        Dim mesa As String = ""
        Try
            If Me.RadioButtonFac.Checked Then
                factura = DataGridView1(1, DataGridView1.CurrentRow.Index).Value()
            Else
                numCortesia = DataGridView1(0, DataGridView1.CurrentRow.Index).Value()
            End If

        Catch ex As Exception
            MsgBox("Debe de seleccionar una factura antes de continuar...", MsgBoxStyle.Information, "Atención")
            Exit Sub
        End Try

        Try
            If IsNumeric(factura) = False Then
                MessageBox.Show("Verifique la selección del registro", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If
            If Me.RadioButtonFac.Checked Then
                Imprimir(factura, "ingles")
            Else
                imprimirCortesia(numCortesia)
            End If


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Sub

    Private Sub Devolver_a_la_Mesa(ByVal _numCortecia)
        Dim numCortesia As String = _numCortecia
        Dim cProducto, cCodigo, cMesa, personas, idc As Integer
        Dim cCantidad, cPrecio, costo_real As Double
        Dim cDescripcion, impresora, nombre_reserva As String
        Dim fila As DataRow
        Dim fila_acom As DataRow ' Recorre los acompañamientos asociados a la comanda anulada
        Dim datos As String
        nombre_reserva = ""
        Dim ds_acompañamientos As New DataSet
        conectadobd = cConexion.Conectar
        cConexion.GetDataSet(conectadobd, "SELECT dbo.DetalleMenuComanda.IdDetalle,Comanda.Idcomanda AS Idcomanda,DetalleMenuComanda.Hora, DetalleMenuComanda.Idmenu AS cProducto, DetalleMenuComanda.Cantidad AS cCantidad, Menu_Restaurante.Nombre_Menu, DetalleMenuComanda.PrecioUnitario, Comanda.NumeroComanda, Comanda.IdMesa AS Id_Mesa, DetalleMenuComanda.Cantidad AS Impreso, Menu_Restaurante.Impresora AS Impresora, Comanda.Comenzales AS Comenzales, Menu_Restaurante.Tipo AS Tipo_Plato, Comanda.CedSalonero AS Cedula, Comanda.Numerofactura, dbo.Menu_Restaurante.Numero_Acompañamientos, dbo.DetalleMenuComanda.costo_real FROM DetalleMenuComanda INNER JOIN Comanda ON DetalleMenuComanda.IdComanda = Comanda.Idcomanda INNER JOIN Menu_Restaurante ON DetalleMenuComanda.Idmenu = Menu_Restaurante.Id_Menu WHERE Comanda.NumeroCortesia = " & numCortesia, datas, "ComandaT")

        For Each fila In datas.Tables("ComandaT").Rows
            idc = fila("idcomanda")
            cProducto = fila("CPRODUCTO")
            cCantidad = fila("CCantidad")
            cDescripcion = fila("Nombre_Menu")
            cPrecio = fila("PrecioUnitario")
            cCodigo = fila("NumeroComanda")
            cMesa = fila("Id_Mesa")
            impresora = fila("Impresora")
            personas = fila("Comenzales")
            costo_real = fila("costo_real")

            'If codMoneda <> CodMonedaC Then
            '    If codMoneda = 2 Then
            '        cPrecio = (cPrecio * valor)
            '        costo_real = (costo_real * valor)
            '    ElseIf valor < ValorMonedaC Then
            '        cPrecio = (cPrecio / ValorMonedaC)
            '        costo_real = (costo_real / ValorMonedaC)
            '    End If
            'End If

            cPrecio = Format(cPrecio, "##,##0.00")
            costo_real = Format(costo_real, "##,##0.00")

            datos = cProducto & "," & cCantidad & ",'" & cDescripcion & "'," & cPrecio & "," & cCodigo & "," & cMesa & ",1,'" & impresora & "'," & personas & ",'Principal'" & ", 1 , " & costo_real
            cConexion.AddNewRecord(conectadobd, "ComandaTemporal", "cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Impreso, Impresora, Comenzales, Tipo_Plato, comandado, costo_real", datos)

            If fila("Numero_Acompañamientos") > 0 Then
                cConexion.GetDataSet(conectadobd, "SELECT dbo.Comanda.Idcomanda, DetalleAcompañamiento_Comanda.IdAcompañamiento as IdMenu , dbo.DetalleMenuComanda.Cantidad , dbo.Acompañamientos_Menu.Nombre, dbo.Comanda.NumeroComanda, dbo.Comanda.IdMesa, dbo.Comanda.Comenzales, dbo.Acompañamientos_Menu.Tipo, dbo.DetalleMenuComanda.costo_real, dbo.Comanda.CedSalonero, dbo.Comanda.Numerofactura FROM dbo.DetalleMenuComanda INNER JOIN dbo.Comanda ON dbo.DetalleMenuComanda.IdComanda = dbo.Comanda.Idcomanda INNER JOIN dbo.DetalleAcompañamiento_Comanda ON dbo.DetalleMenuComanda.IdDetalle = dbo.DetalleAcompañamiento_Comanda.IdDetalle INNER JOIN dbo.Acompañamientos_Menu ON dbo.DetalleAcompañamiento_Comanda.IdAcompañamiento = dbo.Acompañamientos_Menu.Id WHERE dbo.DetalleMenuComanda.IdDetalle = " & fila("IdDetalle"), ds_acompañamientos, "Acompañamientos")
                For Each fila_acom In ds_acompañamientos.Tables("Acompañamientos").Rows
                    'datos = fila_acom("Idmenu") & "," & fila_acom("Cantidad") & ",'" & fila_acom("Nombre") & "'," & cPrecio & "," & fila_acom("NumeroComanda") & "," & fila_acom("IdMesa") & ", 1, '', " & fila_acom("Comenzales") & ", 'Acompañamiento', 1," & costo_real
                    datos = fila_acom("Idmenu") & "," & 0 & ",'" & fila_acom("Nombre") & "'," & 0 & "," & fila_acom("NumeroComanda") & "," & fila_acom("IdMesa") & ", 1, '', " & fila_acom("Comenzales") & ", 'Acompañamiento', 1," & costo_real
                    cConexion.AddNewRecord(conectadobd, "ComandaTemporal", "cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Impreso, Impresora, Comenzales, Tipo_Plato, comandado, costo_real", datos)
                Next
                ds_acompañamientos.Tables("Acompañamientos").Clear()
            End If
        Next

        cConexion.UpdateRecords(conectadobd, "Comanda", "anulado = 1 ", "idcomanda=" & idc)
        cConexion.UpdateRecords(conectadobd, "Mesas", "activa=1", "Id=" & cMesa)
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If DataGridView1.RowCount > 0 Then
        Else
            Exit Sub
        End If

        Dim numCortesia As String = DataGridView1(1, DataGridView1.CurrentRow.Index).Value()
        Dim nombremesa As String
        Dim dts As New DataTable
        cFunciones.Llenar_Tabla_Generico("select Anulado, Contabilizado from Cortesia where numero_cortesia = " & numCortesia, dts, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
        If dts.Rows.Count > 0 Then
            If dts.Rows(0).Item("Contabilizado") = True Then MsgBox("La cortesia ya esta Contabilizada!!!", MsgBoxStyle.Exclamation, "No se puede realizar la operacion.") : Exit Sub
            If dts.Rows(0).Item("Anulado") = False Then
                If EstadoMesa(numCortesia, nombremesa) <> 0 Then
                    If MessageBox.Show("La mesa '" & nombremesa & "' esta ocupada, desea continuar con la anulación ?" & vbCrLf & "NOTA: Los productos de la cortesia al anular se cargaran nuevamente en la mesa.!!", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                        Close()
                    End If
                End If

                If numCortesia = Nothing Then
                    Exit Sub
                End If

                cConexion.Conectar("Restaurante")
                cConexion.UpdateRecords(conectadobd, "Restaurante.dbo.Cortesia", "Anulado=1", "numero_cortesia=" & numCortesia)
                cConexion.DesConectar(conectadobd)

                Me.Devolver_a_la_Mesa(numCortesia)
                MsgBox("Cortesia Anulada", MsgBoxStyle.Information)
                Me.Close()
            Else
                MsgBox("El documento ya esta anulado!!!", MsgBoxStyle.Exclamation, Text)
            End If
        End If
    End Sub

    Private Function EstadoMesa(ByVal _num As String, Optional ByRef _mesa As String = "") As Integer
        Dim dts As New DataTable
        Dim resultado As Integer
        cFunciones.Llenar_Tabla_Generico("select IdMesa from Comanda where NumeroCortesia = " & _num, dts, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
        If dts.Rows.Count > 0 Then
            conectadobd = cConexion.Conectar
            _mesa = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre_Mesa From Mesas where Id = " & dts.Rows(0).Item(0))
            resultado = cConexion.SlqExecuteScalar(conectadobd, "Select activa From Mesas where Id = " & dts.Rows(0).Item(0))
            cConexion.DesConectar(conectadobd)
            Return resultado
        Else
            Return -1
        End If
    End Function

    Private Sub RadioButtonFac_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonFac.CheckedChanged
        If RadioButtonFac.Checked Then
            Me.RadioButtonCortesia.BackgroundImage = My.Resources.cuadro1
            Me.RadioButtonFac.BackgroundImage = My.Resources.cuadro2

            Me.Label4.Text = "Factura"

        End If
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click, Label5.Click

        Me.Label5.BackColor = Color.LightSkyBlue
        Me.Label6.BackColor = Color.White
        Me.Label7.BackColor = Color.White

        Me.PictureBox1.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox4.BackgroundImage = My.Resources.cuadro20


        'realizamos la consulta de las facturas del dia...
        Me.cargado = False
        Me.fechaInicio.Value = Date.Now
        Me.FechaHasta.Value = Date.Now
        Me.cargado = True

        If Me.RadioButtonFac.Checked = True Then
            Me.buscarFacturas()
        Else
            Me.buscarCortesias()
        End If


    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click, Label6.Click
        Me.Label5.BackColor = Color.White
        Me.Label6.BackColor = Color.LightSkyBlue
        Me.Label7.BackColor = Color.White

        Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox4.BackgroundImage = My.Resources.cuadro20



        'realizamos la consulta de las facturas del dia...
        Me.cargado = False
        Me.fechaInicio.Value = Date.Now.Subtract(TimeSpan.FromDays(1))
        Me.FechaHasta.Value = Date.Now.Subtract(TimeSpan.FromDays(1))
        Me.cargado = True

        If Me.RadioButtonFac.Checked = True Then
            Me.buscarFacturas()
        Else
            Me.buscarCortesias()
        End If
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click, Label7.Click
        Me.Label5.BackColor = Color.White
        Me.Label6.BackColor = Color.White
        Me.Label7.BackColor = Color.LightSkyBlue

        Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox4.BackgroundImage = My.Resources.cuadro20
        'realizamos la consulta de las facturas del dia...

        Me.cargado = False
        Me.fechaInicio.Value = Date.Now.Subtract(TimeSpan.FromDays(7))
        Me.FechaHasta.Value = Date.Now
        Me.cargado = True

        If Me.RadioButtonFac.Checked = True Then
            Me.buscarFacturas()
        Else
            Me.buscarCortesias()
        End If
    End Sub

    Private Sub FechaHasta_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FechaHasta.ValueChanged
        If Me.FechaHasta.Value.Date < Me.fechaInicio.Value.Date Then ' para que las fechas siempre sean iguales
            Me.fechaInicio.Value = Me.FechaHasta.Value
        End If
        If Me.cargado Then


            Me.Label5.BackColor = Color.White
            Me.Label6.BackColor = Color.White
            Me.Label7.BackColor = Color.White

            Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
            Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
            Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
            Me.PictureBox4.BackgroundImage = My.Resources.cuadro11

            If Me.RadioButtonFac.Checked = True Then
                Me.buscarFacturas()
            Else
                Me.buscarCortesias()
            End If
        End If
    End Sub

    Private Sub fechaInicio_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechaInicio.ValueChanged
        If Me.fechaInicio.Value.Date > Me.FechaHasta.Value.Date Then
            Me.FechaHasta.Value = Me.fechaInicio.Value
        End If
        If Me.cargado Then

            Me.Label5.BackColor = Color.White
            Me.Label6.BackColor = Color.White
            Me.Label7.BackColor = Color.White

            Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
            Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
            Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
            Me.PictureBox4.BackgroundImage = My.Resources.cuadro11

            If Me.RadioButtonFac.Checked = True Then
                Me.buscarFacturas()
            Else
                Me.buscarCortesias()
            End If
        End If
    End Sub

    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click

        Me.Label5.BackColor = Color.White
        Me.Label6.BackColor = Color.White
        Me.Label7.BackColor = Color.White

        Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox4.BackgroundImage = My.Resources.cuadro11

        System.Windows.Forms.SendKeys.Send("{F4}")

    End Sub

    Private Sub fechaInicio_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechaInicio.Enter

        Me.Label5.BackColor = Color.White
        Me.Label6.BackColor = Color.White
        Me.Label7.BackColor = Color.White

        Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox4.BackgroundImage = My.Resources.cuadro11

        System.Windows.Forms.SendKeys.Send("{F4}")

    End Sub

    Private Sub FechaHasta_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FechaHasta.Enter
        Me.Label5.BackColor = Color.White
        Me.Label6.BackColor = Color.White
        Me.Label7.BackColor = Color.White

        Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox4.BackgroundImage = My.Resources.cuadro11
    End Sub

    Private Sub txtnfactura_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtnfactura.TextChanged
        If txtnfactura.Text <> "" Then
            buscarFacturasTexto()
        End If
    End Sub
End Class