Imports System.Data.SqlClient

Public Class buscador

#Region "Globales"
    Public Icodigo As Integer
    Public iv As Double
    Public presentacion, descripcion, detalle, cuenta, nombre, especies, aprovechamiento As String
    Public consulta, busca As String
    Public cantidad As Integer
    Public bd As String
    Public tabla As String
    Public check As Integer
    Dim DataV As DataView
    Dim DataS As New DataSet
#End Region

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        descripcion = ""
        detalle = ""
        cuenta = ""
        nombre = ""
        Icodigo = 0
        Close()
    End Sub

    Private Sub buscador_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim cConexion As New ConexionR
            Dim conectadobd As New SqlConnection
            'If bd <> Nothing Then
            If bd = "Proveeduria" Then
                conectadobd = cConexion.Conectar(bd)
            ElseIf bd = "Restaurante" Then
                conectadobd = cConexion.Conectar("Restaurante")
            End If
            If bd = "Contabilidad" Then
                conectadobd = cConexion.Conectar(bd)
            End If
            cConexion.GetDataSet(conectadobd, consulta, DataS, tabla)
            DataV = New DataView(DataS.Tables(tabla))
            DataGridView1.DataSource = DataV

            If cantidad = 3 Then
                DataGridView1.Columns(0).Visible = False
                'DataGridView1.Columns(0).Width = 120
                If tabla <> "Secciones_Restaurante" Then
                    DataGridView1.Columns(1).Width = 300
                    DataGridView1.Columns(2).Width = 100
                Else
                    DataGridView1.Columns(1).Width = 180
                    DataGridView1.Columns(2).Width = 220
                End If
            ElseIf cantidad = 4 Then
                DataGridView1.Columns(0).Visible = True
                DataGridView1.Columns(1).Width = 300
                DataGridView1.Columns(2).Width = 100
            Else
                DataGridView1.Columns(0).Visible = False
                DataGridView1.Columns(1).Width = 100
            End If
            LbBuscador.Text = busca '.ToUpper()
            DataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically

            cConexion.DesConectar(conectadobd)
        Catch ex As Exception
            MessageBox.Show("Error al conectar, " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        cargar()
    End Sub

    Private Sub cargar()
        Try
            If bd = "Restaurante" Or bd = "Proveeduria" Then
                If IsDBNull(DataGridView1(0, DataGridView1.CurrentRow.Index).Value()) = True Then
                    MessageBox.Show("Debe seleccionar un registro", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If
                Icodigo = DataGridView1(0, DataGridView1.CurrentRow.Index).Value()
                descripcion = DataGridView1(1, DataGridView1.CurrentRow.Index).Value()
                presentacion = "1 UNIDAD"
                'presentacion1 = DataGridView1(3, DataGridView1.CurrentRow.Index).Value()
                If cantidad = 3 Then
                    detalle = DataGridView1(2, DataGridView1.CurrentRow.Index).Value()
                    If tabla = "Recetas" Then
                        ' especies = DataGridView1(3, DataGridView1.CurrentRow.Index).Value()
                        ' aprovechamiento = DataGridView1(4, DataGridView1.CurrentRow.Index).Value()
                    End If
                End If


                If bd = "Proveeduria" Then
                    Try
                        presentacion = ""
                        presentacion = DataGridView1(3, DataGridView1.CurrentRow.Index).Value()
                        iv = DataGridView1(4, DataGridView1.CurrentRow.Index).Value()
                    Catch ex As Exception
                    End Try
                End If
            ElseIf bd = "Contabilidad" Then
                cuenta = DataGridView1(0, DataGridView1.CurrentRow.Index).Value()
                nombre = DataGridView1(1, DataGridView1.CurrentRow.Index).Value()
            End If
            Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub

    
    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 4 : cargar()
            Case 7 : teclado()
            Case 8 : Close()
        End Select
    End Sub

    Private Sub txtBuscador_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBuscador.KeyDown
        If Trim(txtBuscador.Text).Length >= 0 Then
            'DataV.RowFilter = "" & busca & " Like '%" & txtBuscador.Text & "%'"
            If check = 1 Then
                DataV.RowFilter = " Nombre Like '%" & txtBuscador.Text & "%'"
            Else
                DataV.RowFilter = " Descripcion Like '%" & txtBuscador.Text & "%'"
            End If

        ElseIf Trim(txtBuscador.Text).Length = 0 Then
            DataV = New DataView(DataS.Tables(tabla))
        End If
        DataGridView1.DataSource = DataV
    End Sub

    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicación", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
End Class