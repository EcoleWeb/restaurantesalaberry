<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBuscarCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBuscarCliente))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.pbYZ = New System.Windows.Forms.PictureBox()
        Me.pbWX = New System.Windows.Forms.PictureBox()
        Me.pbUV = New System.Windows.Forms.PictureBox()
        Me.pbST = New System.Windows.Forms.PictureBox()
        Me.pbQR = New System.Windows.Forms.PictureBox()
        Me.pbOP = New System.Windows.Forms.PictureBox()
        Me.pbMN = New System.Windows.Forms.PictureBox()
        Me.pbKL = New System.Windows.Forms.PictureBox()
        Me.pbIJ = New System.Windows.Forms.PictureBox()
        Me.pbGH = New System.Windows.Forms.PictureBox()
        Me.pbEF = New System.Windows.Forms.PictureBox()
        Me.pbCD = New System.Windows.Forms.PictureBox()
        Me.pbAB = New System.Windows.Forms.PictureBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbYZ, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbWX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbUV, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbST, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbQR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbOP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbMN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbKL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbIJ, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbGH, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbEF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbCD, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbAB, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToResizeColumns = False
        Me.DataGridView1.AllowUserToResizeRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightSkyBlue
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.Location = New System.Drawing.Point(12, 59)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.RowTemplate.Height = 35
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(489, 355)
        Me.DataGridView1.TabIndex = 14
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(403, 446)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 24)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Cancelar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(222, 446)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 24)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Aceptar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(28, 446)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 24)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Nuevo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 20)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Buscar :"
        '
        'txtBuscar
        '
        Me.txtBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscar.Location = New System.Drawing.Point(81, 24)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(420, 29)
        Me.txtBuscar.TabIndex = 22
        '
        'PictureBox3
        '
        Me.PictureBox3.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox3.Location = New System.Drawing.Point(10, 432)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(117, 50)
        Me.PictureBox3.TabIndex = 19
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox2.Location = New System.Drawing.Point(383, 432)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(117, 50)
        Me.PictureBox2.TabIndex = 16
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.cuadro1
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Location = New System.Drawing.Point(197, 432)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(117, 50)
        Me.PictureBox1.TabIndex = 15
        Me.PictureBox1.TabStop = False
        '
        'pbYZ
        '
        Me.pbYZ.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.yzw
        Me.pbYZ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbYZ.Location = New System.Drawing.Point(480, 2)
        Me.pbYZ.Name = "pbYZ"
        Me.pbYZ.Size = New System.Drawing.Size(34, 39)
        Me.pbYZ.TabIndex = 12
        Me.pbYZ.TabStop = False
        Me.pbYZ.Visible = False
        '
        'pbWX
        '
        Me.pbWX.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.wxw
        Me.pbWX.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbWX.Location = New System.Drawing.Point(440, 2)
        Me.pbWX.Name = "pbWX"
        Me.pbWX.Size = New System.Drawing.Size(34, 39)
        Me.pbWX.TabIndex = 11
        Me.pbWX.TabStop = False
        Me.pbWX.Visible = False
        '
        'pbUV
        '
        Me.pbUV.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.uvw
        Me.pbUV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbUV.Location = New System.Drawing.Point(400, 2)
        Me.pbUV.Name = "pbUV"
        Me.pbUV.Size = New System.Drawing.Size(34, 39)
        Me.pbUV.TabIndex = 10
        Me.pbUV.TabStop = False
        Me.pbUV.Visible = False
        '
        'pbST
        '
        Me.pbST.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.stw
        Me.pbST.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbST.Location = New System.Drawing.Point(360, 2)
        Me.pbST.Name = "pbST"
        Me.pbST.Size = New System.Drawing.Size(34, 39)
        Me.pbST.TabIndex = 9
        Me.pbST.TabStop = False
        Me.pbST.Visible = False
        '
        'pbQR
        '
        Me.pbQR.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.qrw
        Me.pbQR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbQR.Location = New System.Drawing.Point(320, 2)
        Me.pbQR.Name = "pbQR"
        Me.pbQR.Size = New System.Drawing.Size(34, 39)
        Me.pbQR.TabIndex = 8
        Me.pbQR.TabStop = False
        Me.pbQR.Visible = False
        '
        'pbOP
        '
        Me.pbOP.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.opw
        Me.pbOP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbOP.Location = New System.Drawing.Point(280, 2)
        Me.pbOP.Name = "pbOP"
        Me.pbOP.Size = New System.Drawing.Size(34, 39)
        Me.pbOP.TabIndex = 7
        Me.pbOP.TabStop = False
        Me.pbOP.Visible = False
        '
        'pbMN
        '
        Me.pbMN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbMN.Location = New System.Drawing.Point(241, 2)
        Me.pbMN.Name = "pbMN"
        Me.pbMN.Size = New System.Drawing.Size(34, 39)
        Me.pbMN.TabIndex = 6
        Me.pbMN.TabStop = False
        Me.pbMN.Visible = False
        '
        'pbKL
        '
        Me.pbKL.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.klw
        Me.pbKL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbKL.Location = New System.Drawing.Point(201, 2)
        Me.pbKL.Name = "pbKL"
        Me.pbKL.Size = New System.Drawing.Size(34, 39)
        Me.pbKL.TabIndex = 5
        Me.pbKL.TabStop = False
        Me.pbKL.Visible = False
        '
        'pbIJ
        '
        Me.pbIJ.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.ijw
        Me.pbIJ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbIJ.Location = New System.Drawing.Point(161, 2)
        Me.pbIJ.Name = "pbIJ"
        Me.pbIJ.Size = New System.Drawing.Size(34, 39)
        Me.pbIJ.TabIndex = 4
        Me.pbIJ.TabStop = False
        Me.pbIJ.Visible = False
        '
        'pbGH
        '
        Me.pbGH.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.ghw
        Me.pbGH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbGH.Location = New System.Drawing.Point(121, 2)
        Me.pbGH.Name = "pbGH"
        Me.pbGH.Size = New System.Drawing.Size(34, 39)
        Me.pbGH.TabIndex = 3
        Me.pbGH.TabStop = False
        Me.pbGH.Visible = False
        '
        'pbEF
        '
        Me.pbEF.BackgroundImage = CType(resources.GetObject("pbEF.BackgroundImage"), System.Drawing.Image)
        Me.pbEF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbEF.Location = New System.Drawing.Point(81, 2)
        Me.pbEF.Name = "pbEF"
        Me.pbEF.Size = New System.Drawing.Size(34, 39)
        Me.pbEF.TabIndex = 2
        Me.pbEF.TabStop = False
        Me.pbEF.Visible = False
        '
        'pbCD
        '
        Me.pbCD.BackgroundImage = CType(resources.GetObject("pbCD.BackgroundImage"), System.Drawing.Image)
        Me.pbCD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbCD.Location = New System.Drawing.Point(41, 2)
        Me.pbCD.Name = "pbCD"
        Me.pbCD.Size = New System.Drawing.Size(34, 39)
        Me.pbCD.TabIndex = 1
        Me.pbCD.TabStop = False
        Me.pbCD.Visible = False
        '
        'pbAB
        '
        Me.pbAB.BackgroundImage = CType(resources.GetObject("pbAB.BackgroundImage"), System.Drawing.Image)
        Me.pbAB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pbAB.Location = New System.Drawing.Point(2, 2)
        Me.pbAB.Name = "pbAB"
        Me.pbAB.Size = New System.Drawing.Size(34, 39)
        Me.pbAB.TabIndex = 0
        Me.pbAB.TabStop = False
        Me.pbAB.Visible = False
        '
        'frmBuscarCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(518, 503)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.pbYZ)
        Me.Controls.Add(Me.pbWX)
        Me.Controls.Add(Me.pbUV)
        Me.Controls.Add(Me.pbST)
        Me.Controls.Add(Me.pbQR)
        Me.Controls.Add(Me.pbOP)
        Me.Controls.Add(Me.pbMN)
        Me.Controls.Add(Me.pbKL)
        Me.Controls.Add(Me.pbIJ)
        Me.Controls.Add(Me.pbGH)
        Me.Controls.Add(Me.pbEF)
        Me.Controls.Add(Me.pbCD)
        Me.Controls.Add(Me.pbAB)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmBuscarCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Buscar Cliente"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbYZ, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbWX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbUV, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbST, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbQR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbOP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbMN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbKL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbIJ, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbGH, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbEF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbCD, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbAB, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pbAB As System.Windows.Forms.PictureBox
    Friend WithEvents pbCD As System.Windows.Forms.PictureBox
    Friend WithEvents pbGH As System.Windows.Forms.PictureBox
    Friend WithEvents pbEF As System.Windows.Forms.PictureBox
    Friend WithEvents pbKL As System.Windows.Forms.PictureBox
    Friend WithEvents pbIJ As System.Windows.Forms.PictureBox
    Friend WithEvents pbOP As System.Windows.Forms.PictureBox
    Friend WithEvents pbMN As System.Windows.Forms.PictureBox
    Friend WithEvents pbST As System.Windows.Forms.PictureBox
    Friend WithEvents pbQR As System.Windows.Forms.PictureBox
    Friend WithEvents pbYZ As System.Windows.Forms.PictureBox
    Friend WithEvents pbWX As System.Windows.Forms.PictureBox
    Friend WithEvents pbUV As System.Windows.Forms.PictureBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
End Class
