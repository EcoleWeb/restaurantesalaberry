Imports SIALIBEB.Utilitarios.Buscador

Public Class frmBuscarCliente

    Public pos As Integer

    Public nombre As String
    Public comision As Double





    Public resultado As Boolean = False


    Public Sub clearAllImages()
        Me.pbAB.BackgroundImage = My.Resources.abw
        Me.pbCD.BackgroundImage = My.Resources.cdw
        Me.pbEF.BackgroundImage = My.Resources.efw
        Me.pbGH.BackgroundImage = My.Resources.ghw
        Me.pbIJ.BackgroundImage = My.Resources.ijw
        Me.pbKL.BackgroundImage = My.Resources.klw
        'Me.pbMN.BackgroundImage = My.Resources.mnw
        Me.pbOP.BackgroundImage = My.Resources.opw
        Me.pbQR.BackgroundImage = My.Resources.qrw
        Me.pbST.BackgroundImage = My.Resources.stw
        Me.pbUV.BackgroundImage = My.Resources.uvw
        Me.pbWX.BackgroundImage = My.Resources.wxw
        Me.pbYZ.BackgroundImage = My.Resources.yzw

    End Sub


    Public Sub buscarCliente(ByRef _filtro1 As String)

        Dim clientes As New cls_Clientes()
        Me.DataGridView1.DataSource = clientes.obtenerClientes(_filtro1)

        Me.DataGridView1.Columns("Id").Visible = False
        Me.DataGridView1.Columns("Nombre").Width = 525


    End Sub


    'Private Sub pbAB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbAB.Click
    '    Me.clearAllImages()
    '    Me.pbAB.BackgroundImage = My.Resources.ab
    '    Me.buscarCliente("A", "B")
    'End Sub

    'Private Sub pbCD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbCD.Click
    '    Me.clearAllImages()
    '    Me.pbCD.BackgroundImage = My.Resources.cd
    '    Me.buscarCliente("C", "D")
    'End Sub

    'Private Sub pbEF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbEF.Click
    '    Me.clearAllImages()
    '    Me.pbEF.BackgroundImage = My.Resources.ef
    '    Me.buscarCliente("E", "F")
    'End Sub

    'Private Sub pbGH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbGH.Click
    '    Me.clearAllImages()
    '    Me.pbGH.BackgroundImage = My.Resources.gh
    '    Me.buscarCliente("G", "H")
    'End Sub

    'Private Sub pbIJ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbIJ.Click
    '    Me.clearAllImages()
    '    Me.pbIJ.BackgroundImage = My.Resources.ij
    '    Me.buscarCliente("I", "J")
    'End Sub

    'Private Sub pbKL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbKL.Click
    '    Me.clearAllImages()
    '    Me.pbKL.BackgroundImage = My.Resources.kl
    '    Me.buscarCliente("K", "L")
    'End Sub

    'Private Sub pbMN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbMN.Click
    '    Me.clearAllImages()
    '    'Me.pbMN.BackgroundImage = My.Resources.mn
    '    Me.buscarCliente("M", "N")
    'End Sub

    'Private Sub pbOP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbOP.Click
    '    Me.clearAllImages()
    '    Me.pbOP.BackgroundImage = My.Resources.op
    '    Me.buscarCliente("O", "P")
    'End Sub

    'Private Sub pbQR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbQR.Click
    '    Me.clearAllImages()
    '    Me.pbQR.BackgroundImage = My.Resources.qr
    '    Me.buscarCliente("Q", "R")
    'End Sub

    'Private Sub pbST_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbST.Click
    '    Me.clearAllImages()
    '    Me.pbST.BackgroundImage = My.Resources.st
    '    Me.buscarCliente("S", "T")
    'End Sub

    'Private Sub pbUV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbUV.Click
    '    Me.clearAllImages()
    '    Me.pbUV.BackgroundImage = My.Resources.uv
    '    Me.buscarCliente("U", "V")
    'End Sub

    'Private Sub pbWX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbWX.Click
    '    Me.clearAllImages()
    '    Me.pbWX.BackgroundImage = My.Resources.wx
    '    Me.buscarCliente("W", "X")
    'End Sub

    'Private Sub pbYZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbYZ.Click
    '    Me.clearAllImages()
    '    Me.pbYZ.BackgroundImage = My.Resources.yz
    '    Me.buscarCliente("Y", "Z")
    'End Sub

    'Private Sub pb09_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click, PictureBox1.Click

        Me.pos = Me.DataGridView1.SelectedRows(0).Cells("Id").Value
        Me.nombre = Me.DataGridView1.SelectedRows(0).Cells("Nombre").Value
        Me.comision = Me.DataGridView1.SelectedRows(0).Cells("Comicion").Value

        Me.resultado = True

        Dim frm As New frmCorreoCliente
        frm.IdCliente = pos
        frm.ShowDialog()
        Me.Close()
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click, PictureBox2.Click
        Me.resultado = False
        Me.Close()
    End Sub

    Private Sub frmBuscarCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtBuscar.Focus()
        Me.buscarCliente(txtBuscar.Text)
    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click, PictureBox3.Click
        Try
            Dim frm As New Cliente
            frm.ShowDialog()
            Me.buscarCliente(frm.Txtnombre.Text)
        Catch ex As Exception

        End Try
    End Sub


    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Me.buscarCliente(txtBuscar.Text)
    End Sub
End Class