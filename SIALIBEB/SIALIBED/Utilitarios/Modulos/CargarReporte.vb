﻿Module CargaReporte

    Public Function CargarReporte(ByRef Viewer As CrystalDecisions.Windows.Forms.CrystalReportViewer, ByRef objReport As CrystalDecisions.CrystalReports.Engine.ReportDocument) As Boolean
        Dim intCounter As Integer
        Dim intCounter1 As Integer

        Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim paraValue As New CrystalDecisions.Shared.ParameterDiscreteValue
        Dim mySubReportObject As CrystalDecisions.CrystalReports.Engine.SubreportObject
        Dim mySubRepDoc As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim index As Integer
        Dim SQLConexion As New SqlClient.SqlConnection
        SQLConexion.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
        If SQLConexion.State <> ConnectionState.Open Then SQLConexion.Open()

        Try
            ConInfo.ConnectionInfo.IntegratedSecurity = True
            ConInfo.ConnectionInfo.ServerName = SQLConexion.DataSource
            ConInfo.ConnectionInfo.DatabaseName = SQLConexion.Database


            For intCounter = 0 To objReport.Database.Tables.Count - 1
                objReport.Database.Tables(intCounter).ApplyLogOnInfo(ConInfo)
            Next

            For index = 0 To objReport.ReportDefinition.Sections.Count - 1
                For intCounter = 0 To _
                      objReport.ReportDefinition.Sections(index).ReportObjects.Count - 1
                    With objReport.ReportDefinition.Sections(index)
                        If .ReportObjects(intCounter).Kind = _
                           CrystalDecisions.Shared.ReportObjectKind.SubreportObject Then
                            mySubReportObject = CType(.ReportObjects(intCounter),  _
                            CrystalDecisions.CrystalReports.Engine.SubreportObject)
                            mySubRepDoc = _
                             mySubReportObject.OpenSubreport(mySubReportObject.SubreportName)
                            For intCounter1 = 0 To mySubRepDoc.Database.Tables.Count - 1
                                mySubRepDoc.Database.Tables(intCounter1).ApplyLogOnInfo(ConInfo)

                            Next
                        End If
                    End With
                Next
            Next
            Viewer.ReportSource = objReport
            Return True

        Catch ex As System.Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function
End Module

