Imports System.Data.SqlClient

Public Class ConexionR
    Public SqlConexion As New SqlConnection
    Public SQLStringConexion As String


    Public Function Conectar(Optional ByVal Modulo As String = "Restaurante") As SqlConnection
        Try
            If SqlConexion.State <> ConnectionState.Open Then
                SQLStringConexion = GetSetting("SeeSOFT", Modulo, "Conexion")
                SqlConexion.ConnectionString = SQLStringConexion
                SqlConexion.Open()
            End If
            Return SqlConexion
        Catch ex As Exception
            MessageBox.Show("No se puede realizar la conexi�n con el servidor de base de datos", "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Return Nothing
        End Try
    End Function

    Public Sub DesConectar(ByRef sqlConexion As SqlConnection)
        Try
            If sqlConexion.State <> ConnectionState.Closed Then sqlConexion.Close()
            sqlConexion.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

       
    End Sub
    ' DEVUELVE EL DataReader DE LA CONSULTA
    Public Sub GetRecorset(ByRef conexion As SqlConnection, ByVal StrQuery As String, ByRef sqlDatos As SqlDataReader)
        Dim Command As SqlCommand = Nothing
        Dim Mensaje As String
        Try
            If conexion.State <> ConnectionState.Open Then conexion.Open()
            Command = New SqlCommand(StrQuery, conexion)
            sqlDatos = Command.ExecuteReader
        Catch ex As Exception
            Mensaje = ex.Message
        Finally
            Command.Dispose()
            Command = Nothing
        End Try
    End Sub

    Public Sub GetDataSet(ByRef conexion As SqlConnection, ByVal StrQuery As String, ByRef DataS As DataSet, ByVal tabla As String)
        Dim mensaje As String
        Dim adapter As New SqlDataAdapter(StrQuery, conexion)
        If conexion.State <> ConnectionState.Open Then conexion.Open()
        Try
            adapter.Fill(DataS, tabla)
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            adapter.Dispose()
            adapter = Nothing
        End Try
    End Sub
    'DEVUELVE  EL RESULTADO DE LA CONSULTA
    Public Function SlqExecuteScalar(ByRef Conexion As SqlConnection, ByVal StrQuery As String) As String
        Dim Command As SqlCommand
        Dim Dato As String = ""
        Dim Mensaje As String
        If Conexion.State <> ConnectionState.Open Then Conexion.Open()
        Command = New SqlCommand(StrQuery, Conexion)
        Try
            Dato = Command.ExecuteScalar()

            Conexion.Close()
        Catch ex As Exception
            Mensaje = ex.Message
            MsgBox(Mensaje)

        Finally
            Command.Dispose()
            Command = Nothing
        End Try
        Return Dato

    End Function


    Public Function SlqExecute(ByRef conexion As SqlConnection, ByVal strQuery As String) As String
        Dim Command As SqlCommand
        Dim Mensaje As String = ""
        If conexion.State <> ConnectionState.Open Then conexion.Open()

        Command = New SqlCommand(strQuery, conexion)
        Try
            Command.ExecuteNonQuery()
        Catch ex As Exception
            Mensaje = ex.Message
            MsgBox(strQuery & conexion.ConnectionString & ex.ToString, MsgBoxStyle.Information, "Atenci�n...")
        Finally
            Command.Dispose()
            Command.Connection.Close()
            Command = Nothing
        End Try
        Return Mensaje

    End Function

    Public Function AddNewRecord(ByRef conexion As SqlConnection, ByRef Table As Object, ByRef Campos As Object, ByRef Datos As Object) As String
        Dim Command As SqlCommand
        Dim Mensaje As String = ""
        If conexion.State <> ConnectionState.Open Then conexion.Open()

        Command = New SqlCommand("INSERT INTO " & Table & " (" & Campos & ") VALUES (" & Datos & ")", conexion)
        Try
            Command.ExecuteNonQuery()
        Catch ex As Exception
            Mensaje = ex.Message
            MsgBox(Mensaje)
        Finally
            Command.Dispose()
            Command = Nothing
        End Try
        Return Mensaje

    End Function
    'FUNCION QUE PERMITE LA ACTUALIZACION DE REGISTROS SEGUN DETERMINADA
    Public Function UpdateRecords(ByRef conexion As SqlConnection, ByRef Table As Object, ByRef Datos As Object, ByRef Condicion As Object) As String
        Dim Command As SqlCommand
        Dim Mensaje As String = ""
        If conexion.State <> ConnectionState.Open Then conexion.Open()

        If Condicion <> "" Then
            Command = New SqlCommand("UPDATE " & Table & " SET " & Datos & " WHERE " & Condicion, conexion)
        Else
            Command = New SqlCommand("UPDATE " & Table & " SET " & Datos, conexion)
        End If
        Try
            Command.ExecuteNonQuery()
        Catch ex As Exception
            Mensaje = ex.Message
            MsgBox(Mensaje, MsgBoxStyle.Information, "Atenci�n..")
        Finally
            conexion.Close()
            Command.Dispose()
            Command = Nothing
        End Try
        Return Mensaje
    End Function

    'FUNCION DEFINIDA PARA LA ELIMINACION DE UNO O VARIOS REGISTROS 
    Public Function DeleteRecords(ByRef conexion As SqlConnection, ByRef Table As String, ByRef Condicion As Object) As String
        Dim Command As SqlCommand
        Dim adaptador As New SqlDataAdapter
        If conexion.State <> ConnectionState.Open Then conexion.Open()

        Dim Mensaje As String = ""
        If Condicion = "" Then
            Command = New SqlCommand("DELETE FROM " & Table, conexion)
        Else
            Command = New SqlCommand("DELETE FROM " & Table & " WHERE " & Condicion, conexion)
        End If
        Try
            Command.ExecuteNonQuery()
        Catch ex As Exception
            Mensaje = ex.Message
        Finally
            Command.Dispose()
            Command = Nothing
        End Try
        Return Mensaje
    End Function

End Class