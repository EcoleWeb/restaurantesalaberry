Imports System.Drawing.Printing
Public Module Globales
    'Public PuntoVenta, CodigoBodega As Integer
    Public clave As String ' NombrePunto
    Public User_Log As New Usuario_Logeado
    Public version As String = "08.2015.1"
    'reportes 
    '------------------------------------------------------------------------------------------------
    Public PreFactura As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Public PreFacturaIng As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Public rptComanda As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Public rptComandaIzquierda As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Public rptComandaExpres As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Public FacturaPVE As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Public FacturaPVEIng As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Public FacturaPVESR As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    '------------------------------------------------------------------------------------------------
    Public cedula As String
    Public usaGrupo As Boolean = False
    Public TipoImpres As Integer = 0
    Public OpcionFacturacion As Boolean
    '---------------------------------------------------------------------------------------------------
    Public gloNoClave As Boolean

    Public glodtsGlobal As New dtsGlobal
    Public gloCargandodtsGlobal As Boolean = False

    Public Sub spCargarDatosComanda()
        Try
            gloCargandodtsGlobal = True
            Dim Con As String = GetSetting("SeeSoft", "Restaurante", "Conexion")

            Dim Menu_RestauranteTableAdapter As New dtsGlobalTableAdapters.Menu_RestauranteTableAdapter
            Dim Categorias_MenuTableAdapter As New dtsGlobalTableAdapters.Categorias_MenuTableAdapter
            Dim Modificadores_ForzadosTableAdapter As New dtsGlobalTableAdapters.Modificadores_ForzadosTableAdapter
            Dim ModificadoresPrecioTableAdapter As New dtsGlobalTableAdapters.ModificadoresPrecioTableAdapter
            Dim Acompañamientos_MenuPrecioTableAdapter As New dtsGlobalTableAdapters.Acompañamientos_MenuPrecioTableAdapter
            Dim Acompañamientos_MenuTableAdapter As New dtsGlobalTableAdapters.Acompañamientos_MenuTableAdapter
            Dim Categoria_DimesionTableAdapter As New dtsGlobalTableAdapters.Categoria_DimesionTableAdapter
            Dim UsuariosTableAdapter As New dtsGlobalTableAdapters.UsuariosTableAdapter
            Dim MesasTableAdapter As New dtsGlobalTableAdapters.MesasTableAdapter
            Dim PermisosSeguridadTableAdapter As New dtsGlobalTableAdapters.PermisosSeguridadTableAdapter
            Dim confTableAdapter As New dtsGlobalTableAdapters.confTableAdapter
            Dim ConfiguracionesTableAdapter As New dtsGlobalTableAdapters.ConfiguracionesTableAdapter

            Menu_RestauranteTableAdapter.Connection.ConnectionString = Con
            Categorias_MenuTableAdapter.Connection.ConnectionString = Con
            Modificadores_ForzadosTableAdapter.Connection.ConnectionString = Con
            ModificadoresPrecioTableAdapter.Connection.ConnectionString = Con
            Acompañamientos_MenuPrecioTableAdapter.Connection.ConnectionString = Con
            Acompañamientos_MenuTableAdapter.Connection.ConnectionString = Con
            Categoria_DimesionTableAdapter.Connection.ConnectionString = Con
            UsuariosTableAdapter.Connection.ConnectionString = Con
            MesasTableAdapter.Connection.ConnectionString = Con
            PermisosSeguridadTableAdapter.Connection.ConnectionString = GetSetting("SeeSoft", "Seguridad", "Conexion")
            confTableAdapter.Connection.ConnectionString = Con
            ConfiguracionesTableAdapter.Connection.ConnectionString = Con

            Menu_RestauranteTableAdapter.Fill(glodtsGlobal.Menu_Restaurante)
            Categorias_MenuTableAdapter.Fill(glodtsGlobal.Categorias_Menu)
            Modificadores_ForzadosTableAdapter.Fill(glodtsGlobal.Modificadores_Forzados)
            ModificadoresPrecioTableAdapter.Fill(glodtsGlobal.ModificadoresPrecio)
            Acompañamientos_MenuPrecioTableAdapter.Fill(glodtsGlobal.Acompañamientos_MenuPrecio)
            Acompañamientos_MenuTableAdapter.Fill(glodtsGlobal.Acompañamientos_Menu)
            Categoria_DimesionTableAdapter.Fill(glodtsGlobal.Categoria_Dimesion)
            UsuariosTableAdapter.Fill(glodtsGlobal.Usuarios)
            MesasTableAdapter.Fill(glodtsGlobal.Mesas)
            PermisosSeguridadTableAdapter.Fill(glodtsGlobal.PermisosSeguridad)
            confTableAdapter.Fill(glodtsGlobal.conf)
            ConfiguracionesTableAdapter.Fill(glodtsGlobal.Configuraciones)


        Catch ex As Exception
            spCargarDatosComanda()

        Finally
            gloCargandodtsGlobal = False
        End Try

    End Sub
    Public Function GetVersionPublicacion() As String

        Dim ver As String = ""
        If (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed) Then
            Dim ad As System.Deployment.Application.ApplicationDeployment
            ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment
            ver = ad.CurrentVersion.ToString()
        End If

        Return ver
    End Function
    Private Function busca_impresoraCompuesto() As String
        Try
            Dim PrintDocument1 As New PrintDocument
            Dim DefaultPrinter As String = PrintDocument1.PrinterSettings.PrinterName
            Dim PrinterInstalled As String

            'BUSCA LA IMPRESORA PREDETERMINADA PARA EL SISTEMA
            Dim impresoraPredeterminada As String = GetSetting("SeeSoft", "Restaurante", "ImpresoraPrefactura")
            If Not impresoraPredeterminada.Equals("") Then

                For Each PrinterInstalled In PrinterSettings.InstalledPrinters

                    If cedula = "3-101-139559" Then
                        Select Case PrinterInstalled.ToUpper
                            Case impresoraPredeterminada
                                Return PrinterInstalled.ToString
                        End Select
                    Else
                        Select Case Split(PrinterInstalled.ToUpper, "\").GetValue(Split(PrinterInstalled.ToUpper, "\").GetLength(0) - 1)
                            Case impresoraPredeterminada 'FACTURACION
                                Return PrinterInstalled.ToString
                                Exit Function
                        End Select
                    End If

                Next
            Else
                'CREA EL REGISTRO PARA QUE PODAMOS MODIFICARLO LUEGO
                SaveSetting("SeeSoft", "Restaurante", "ImpresoraPrefactura", "")

            End If
            Dim PrinterDialog As New PrintDialog
            Dim DocPrint As New PrintDocument
            PrinterDialog.Document = DocPrint
            If PrinterDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Return PrinterDialog.PrinterSettings.PrinterName 'DEVUELVE LA IMPRESORA  SELECCIONADA
            Else
                Return "" 'Ninguna Impresora
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Sub inicializarComandas()
        Try
            If GetSetting("SeeSoft", "Restaurante", "Impresion") = "1" Then
                rptComanda.Load(GetSetting("SeeSoft", "A&B Reports", "rptComandas"))
                CrystalReportsConexion.LoadReportViewer(Nothing, rptComanda, True, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            Else
                rptComandaIzquierda.Load(GetSetting("SeeSoft", "A&B Reports", "rptComandasIzq"))
                CrystalReportsConexion.LoadReportViewer(Nothing, rptComandaIzquierda, True, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            End If
            rptComandaExpres.Load(GetSetting("SeeSoft", "A&B Reports", "rptComandasexpress"))
            CrystalReportsConexion.LoadReportViewer(Nothing, rptComandaExpres, True, GetSetting("SeeSoft", "Restaurante", "Conexion"))

        Catch ex As Exception
            cFunciones.spEnviar_Correo("Glovales.vb", "inicializarComandas", ex.Message)
        End Try
    End Sub

    Sub inicializarFactura(ByVal Idioma As String)
        Try
            If Idioma = "espanol" Then
                If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
                    FacturaPVE.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVE1"))
                ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = 2 Then
                    FacturaPVE.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVECB"))
                Else
                    FacturaPVE.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVE"))
                End If
            ElseIf Idioma = "Ingles" Then
                If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
                    FacturaPVE.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVEIngles1"))
                Else
                    FacturaPVE.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVEIngles"))
                End If
            ElseIf Idioma = "ambos" Then
                If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
                    FacturaPVE.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVE1"))
                    FacturaPVEIng.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVEIngles1"))
                Else
                    FacturaPVE.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVECB"))
                    FacturaPVEIng.Load(GetSetting("SeeSoft", "A&B Reports", "RptFacturaPVEIngles"))
                End If
            End If
            CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVE, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))

        Catch ex As Exception
            cFunciones.spEnviar_Correo("Glovales.vb", "inicializarFactura", ex.Message)
        End Try
     End Sub

    Sub inicializarPreFactura()
        Try
            If cedula = "3-101-104611-04" Then
                PreFacturaIng.Load(GetSetting("SeeSoft", "A&B Reports", "RptPre-FacturaIngles1"))
                CrystalReportsConexion.LoadReportViewer(Nothing, PreFacturaIng, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
                PreFactura.Load(GetSetting("SeeSoft", "A&B Reports", "RptPre-Factura1"))
                CrystalReportsConexion.LoadReportViewer(Nothing, PreFactura, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
            Else
                PreFacturaIng.Load(GetSetting("SeeSoft", "A&B Reports", "RptPre-FacturaIngles"))
                CrystalReportsConexion.LoadReportViewer(Nothing, PreFacturaIng, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
                PreFactura.Load(GetSetting("SeeSoft", "A&B Reports", "RptPre-Factura"))
                CrystalReportsConexion.LoadReportViewer(Nothing, PreFactura, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo("Glovales.vb", "inicializarPreFactura", ex.Message)
        End Try
     
    End Sub

    Public Sub imprimirFactura(ByVal NFactura As Integer, ByVal Idioma As String, ByVal numeroComanda As Integer, Optional ByVal servRest As Boolean = False)

        Dim cnx As New Conexion
        Dim salon As String = ""

        Dim Impresora As String
        Impresora = busca_impresoraCompuesto()

        If Impresora = "" Then
            MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Impresora = Busca_Impresora()
            If Impresora = "" Then
                Exit Sub
            End If
        End If

        'Temporal.. si la impresion es rapida, entonces cargamos las inicializaciones..
        'si no, previamente ya se habian cargado en el formulario principal..
        'PENDIENTE DE CAMBIO..

        Dim factura As New cls_Facturas()

        If GetSetting("SeeSoft", "Restaurante", "Rapido") = 1 Then
            factura.ImprimirFacturaEsp(NFactura, numeroComanda, Impresora, servRest)
            Exit Sub
        End If
        ' -----
        If GetSetting("SeeSoft", "Restaurante", "Rapido") = 0 Then
            If TipoImpres = 1 Then
                inicializarFactura("espanol")
            ElseIf TipoImpres = 2 Then
                inicializarFactura("Ingles")
            ElseIf TipoImpres = 3 Then
                inicializarFactura("espanol")
            ElseIf TipoImpres = 4 Then
                inicializarFactura("espanol")
            End If

            If Not (cedula = "3-120-1067732") Then
                'FacturaPVE = New MARES4
            End If

            If servRest Then
                If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
                    FacturaPVE = New RptFacturaPVE_SR   'Generica
                ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = 2 Then
                    FacturaPVE = New RptFacturaPVE_SR_IZ
                End If
                CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVE, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
            End If
            'Dim rpt As New rptComandasexpress
            'rpt.Subreports("Descrip").SetParameterValue(0, "")

            FacturaPVE.Refresh()
            FacturaPVE.PrintOptions.PrinterName = Impresora
            FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            FacturaPVE.SetParameterValue(0, NFactura)
            FacturaPVE.SetParameterValue(1, User_Log.PuntoVenta)
            FacturaPVE.SetParameterValue(2, numeroComanda)
            FacturaPVE.SetParameterValue(3, False)

            If Not servRest Then
                If Not ((cedula = "3-101-374928-3") And Idioma.Equals("espanol")) Then
                    Dim dt As New DataTable
                    cFunciones.Llenar_Tabla_Generico("SELECT     Usuarios_1.Nombre AS NCajero, Saloneros.Nombre AS NSalonero FROM         Comanda INNER JOIN  Saloneros ON Comanda.CedSalonero = Saloneros.Cedula INNER JOIN   Usuarios AS Usuarios_1 ON Comanda.Cedusuario = Usuarios_1.Cedula WHERE     (Comanda.Numerofactura = '" & NFactura & "')", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))
                    If TypeOf FacturaPVE Is RptFacturaPVECB = True Then
                        If dt.Rows.Count > 0 Then
                            FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                        End If
                        'impresion de tipo 1 formato ampliado a la derecha
                    ElseIf TypeOf FacturaPVE Is RptFacturaPVE1 = True Then

                        If dt.Rows.Count > 0 Then
                            FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                            FacturaPVE.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
                            FacturaPVE.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
                        Else
                            FacturaPVE.SetParameterValue(4, "")
                            FacturaPVE.SetParameterValue(5, "")
                            FacturaPVE.SetParameterValue(6, "")
                        End If
                    ElseIf TypeOf FacturaPVE Is RptFacturaPVE Then
                        If dt.Rows.Count > 0 Then
                            FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                        End If
                    End If
                End If
            End If
            FacturaPVE.PrintToPrinter(1, True, 0, 0)
        End If

        ' FacturaPVE = Nothing
    End Sub
    Function spMensaje(ByVal _msj As String, ByVal _estilo As Integer) As DialogResult
        Dim fr As New frmMensaje(_msj, _estilo)
        Dim i As Integer = fr.ShowDialog()
        Return i
    End Function
    Public Function Busca_Impresora() As String
        Dim PrinterDialog As New PrintDialog
        Dim DocPrint As New PrintDocument
        PrinterDialog.Document = DocPrint
        If PrinterDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Return PrinterDialog.PrinterSettings.PrinterName 'DEVUELVE LA IMPRESORA  SELECCIONADA
        Else
            Return "" 'Ninguna Impresora
        End If
    End Function
End Module
