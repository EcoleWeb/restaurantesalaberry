Public Class Usuario_Logeado
    Public Cedula As String         ';-)
    Public Nombre As String         ':-)
    Public Clave_Entrada As String  ':(
    Public Clave_Interna As String  ':)
    Public CambiarPrecio As Boolean ' Si se le permite cambiar precios 
    Public Aplicar_Desc As Boolean ' Boolean si se le permite aplicar descuento 
    Public Exist_Negativa As Boolean ' Venta con existencia en negativo
    Public Porc_Desc As Double      ' porcentaje de descuento maximo a aplicar en las ventas
    Public Porc_Precio As Double    ' porcentaje de variacion de precio a aplicar en las ventas
    Public Bodega As Integer        ' bodega de inventario a la que pertenece el punto de venta
    Public PuntoVenta As Integer    ' cod. del punto de venta
    Public NombrePunto As String    ' nombre del Punto de venta actual
    Public IVI As Double            ' impuesto por venta
    Public ISS As Double            ' impuesto por servicio 
    Public ISH As Double            ' Impuesto por servicio a la habitacion
    Public ICT As Double            ' Impuesto para el Instituto Costarricense de Turismo
    Public SolicitarClaveMesa As Boolean  'solicitar la clave cada vez que quiera entrar en una mesa 
    Public CostoCortesia As Boolean
End Class
