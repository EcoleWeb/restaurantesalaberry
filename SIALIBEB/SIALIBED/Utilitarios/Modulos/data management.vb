'rolado obando rojas
Public Module General
    Private conexion As New SqlClient.SqlConnection(GetSetting("Seesoft", "Restaurante", "Conexion"))
    Private adaptador As SqlClient.SqlDataAdapter

    Public Property StringConection() As String
        Get
            Return conexion.ConnectionString
        End Get
        Set(ByVal value As String)
            conexion.ConnectionString = value
        End Set
    End Property

    Public Function Ejecuta(ByVal _strSQL As String) As DataTable
        Try
            Dim dts As New DataTable
            conexion.Open()
            adaptador = New SqlClient.SqlDataAdapter(_strSQL, conexion)
            adaptador.SelectCommand.CommandType = CommandType.Text
            adaptador.Fill(dts)
            conexion.Close()
            Return dts
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Sub AddNombre(ByVal _id As Integer, ByVal _NuevoNombre As String)
        If _NuevoNombre <> "" Then
            If Exist(_id) = False Then
                Ejecuta("UPDATE Mesas SET TEM = Nombre_Mesa WHERE id = " & _id)
                Ejecuta("UPDATE Mesas SET Nombre_Mesa = '" & _nuevoNombre & "' WHERE Id = " & _id)
            Else
                Ejecuta("UPDATE Mesas SET Nombre_Mesa = '" & _nuevoNombre & "' WHERE Id = " & _id)
            End If
        Else
            DeleteNombre(_id)
        End If
    End Sub

    Public Function GetPosByMesa(ByVal mesa As Integer) As Integer
        Return Ejecuta("SELECT Id FROM Mesas WHERE Id = " & mesa).Rows(0).Item(0)
    End Function

    Public Sub DeleteNombre(ByVal key As Integer)
        If GetTem(key) <> "" Then
            Ejecuta("UPDATE Mesas SET Nombre_Mesa = TEM WHERE Id = " & key)
            Ejecuta("UPDATE Mesas SET TEM = '' WHERE Id = " & key)
        End If
    End Sub

    Public Function GetTem(ByVal key As Integer) As String
        Return Ejecuta("SELECT Tem FROM Mesas WHERE Id = " & key).Rows(0).Item(0)
    End Function

    Public Function GetNombre(ByVal key As Integer) As String
        Return Ejecuta("SELECT Nombre_Mesa FROM Mesas WHERE Id = " & key).Rows(0).Item(0)
    End Function

    Public Function Exist(ByVal key As Integer) As Boolean
        Return IIf(Ejecuta("SELECT TEM FROM Mesas WHERE Id = " & key).Rows(0).Item(0) <> "", True, False)
    End Function

    ''' <summary>
    ''' busca el menu_restaurante por id_menu, y verifica si tiene correcto el vinculo con su receta o articulo
    ''' </summary>
    ''' <param name="_id_menu">id_menu del menu_restaurant a buscar</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TieneVinculo(ByVal _id_menu As Long) As Boolean
        'busca  el articulo o reseta vinculada al menu
        'Return IIf(General.Ejecuta("select case tipo  when 1 then (select count(*) from Restaurante.dbo.Recetas where ID = id_receta)when 2 then (select count(*) from proveeduria.dbo.inventario where codigo = id_receta and inhabilitado = 0) end as Vinculo from Restaurante.dbo.Menu_Restaurante where Restaurante.dbo.Menu_Restaurante.Id_Menu = " & _id_menu).Rows(0).Item(0) = 0, False, True)
        Return True
    End Function


End Module