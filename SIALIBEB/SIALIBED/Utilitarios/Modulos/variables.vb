
Module variables
    'Public punto_venta As String
    'Public Id_UsuarioConectado As String
End Module

Class Class_Usuario
    Public Id_Usuario As String
    Public Nombre As String

    Public Punto_Venta As String
    Public Cedula As String

    Public Clave_Entrada As String
    Public Clave_Interna As String

    Public CambiarPrecio As Boolean
    Public Aplicar_Desc As Boolean

    Public Porc_Desc As Double
    Public Porc_Precio As Double

    Public ImpVent As Double
    Public ImpServ As Double
    Public ImpServRoom As Double

End Class
