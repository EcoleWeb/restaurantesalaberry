<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Prompt
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Prompt))
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.txtDato = New System.Windows.Forms.TextBox()
        Me.txtMEsas = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.OpenFile = New System.Windows.Forms.OpenFileDialog()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.txtRecetas = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.comboGrupos = New System.Windows.Forms.ComboBox()
        Me.ListItems = New System.Windows.Forms.ListBox()
        Me.rbArticulo = New System.Windows.Forms.RadioButton()
        Me.rbReceta = New System.Windows.Forms.RadioButton()
        Me.Grupo_Bodega = New System.Windows.Forms.GroupBox()
        Me.cmbxBodegas = New System.Windows.Forms.ComboBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.ckParaLlevar = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.Grupo_Bodega.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnAceptar
        '
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'txtDato
        '
        Me.txtDato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtDato, "txtDato")
        Me.txtDato.Name = "txtDato"
        '
        'txtMEsas
        '
        Me.txtMEsas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtMEsas, "txtMEsas")
        Me.txtMEsas.Name = "txtMEsas"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'btnCancelar
        '
        resources.ApplyResources(Me.btnCancelar, "btnCancelar")
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnBuscar
        '
        resources.ApplyResources(Me.btnBuscar, "btnBuscar")
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'OpenFile
        '
        Me.OpenFile.FileName = "OpenFileDialog1"
        '
        'btnBorrar
        '
        resources.ApplyResources(Me.btnBorrar, "btnBorrar")
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.UseVisualStyleBackColor = True
        '
        'txtRecetas
        '
        Me.txtRecetas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtRecetas, "txtRecetas")
        Me.txtRecetas.Name = "txtRecetas"
        Me.txtRecetas.ReadOnly = True
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'txtId
        '
        resources.ApplyResources(Me.txtId, "txtId")
        Me.txtId.Name = "txtId"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.comboGrupos)
        Me.GroupBox1.Controls.Add(Me.ListItems)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'comboGrupos
        '
        Me.comboGrupos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.comboGrupos, "comboGrupos")
        Me.comboGrupos.FormattingEnabled = True
        Me.comboGrupos.Name = "comboGrupos"
        '
        'ListItems
        '
        Me.ListItems.FormattingEnabled = True
        resources.ApplyResources(Me.ListItems, "ListItems")
        Me.ListItems.Name = "ListItems"
        '
        'rbArticulo
        '
        resources.ApplyResources(Me.rbArticulo, "rbArticulo")
        Me.rbArticulo.Name = "rbArticulo"
        Me.rbArticulo.TabStop = True
        Me.rbArticulo.UseVisualStyleBackColor = True
        '
        'rbReceta
        '
        resources.ApplyResources(Me.rbReceta, "rbReceta")
        Me.rbReceta.Name = "rbReceta"
        Me.rbReceta.TabStop = True
        Me.rbReceta.UseVisualStyleBackColor = True
        '
        'Grupo_Bodega
        '
        Me.Grupo_Bodega.Controls.Add(Me.cmbxBodegas)
        Me.Grupo_Bodega.Controls.Add(Me.ListBox1)
        resources.ApplyResources(Me.Grupo_Bodega, "Grupo_Bodega")
        Me.Grupo_Bodega.Name = "Grupo_Bodega"
        Me.Grupo_Bodega.TabStop = False
        '
        'cmbxBodegas
        '
        Me.cmbxBodegas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.cmbxBodegas, "cmbxBodegas")
        Me.cmbxBodegas.FormattingEnabled = True
        Me.cmbxBodegas.Name = "cmbxBodegas"
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        resources.ApplyResources(Me.ListBox1, "ListBox1")
        Me.ListBox1.Name = "ListBox1"
        '
        'TextBox1
        '
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        '
        'TextBox2
        '
        Me.TextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.TextBox2, "TextBox2")
        Me.TextBox2.Name = "TextBox2"
        '
        'ckParaLlevar
        '
        resources.ApplyResources(Me.ckParaLlevar, "ckParaLlevar")
        Me.ckParaLlevar.Name = "ckParaLlevar"
        Me.ckParaLlevar.UseVisualStyleBackColor = True
        '
        'Prompt
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ckParaLlevar)
        Me.Controls.Add(Me.Grupo_Bodega)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.rbReceta)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.rbArticulo)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtId)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtRecetas)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtMEsas)
        Me.Controls.Add(Me.txtDato)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Name = "Prompt"
        Me.GroupBox1.ResumeLayout(False)
        Me.Grupo_Bodega.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtDato As System.Windows.Forms.TextBox
    Friend WithEvents txtMEsas As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents OpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents txtRecetas As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents comboGrupos As System.Windows.Forms.ComboBox
    Friend WithEvents ListItems As System.Windows.Forms.ListBox
    Friend WithEvents rbArticulo As System.Windows.Forms.RadioButton
    Friend WithEvents rbReceta As System.Windows.Forms.RadioButton
    Friend WithEvents Grupo_Bodega As System.Windows.Forms.GroupBox
    Friend WithEvents cmbxBodegas As System.Windows.Forms.ComboBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ckParaLlevar As CheckBox
End Class
