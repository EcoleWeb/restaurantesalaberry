Public Class Prompt
    Public receta As Integer
    Public tipo As Integer
    Public grupo As String
    Public ruta As String
    Public Cargando As Boolean
    Public ID_BODEGA As Integer
    Public ID_RECETA As Integer = 0
    Public COSTO_PROMEDIO As Double
    Public RECETA_COMPLETA As Boolean = True




    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        tipo = 0
        Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If (Me.Text = "Acompaņamientos") Then
            If (txtDato.Text = Nothing Or txtRecetas.Text = Nothing) Then
                MsgBox("Verifica que hayas completado todos los datos", MsgBoxStyle.Information, "Faltan datos ...")
                Exit Sub
            End If
        ElseIf (txtDato.Text = Nothing Or (comboGrupos.Text = Nothing And comboGrupos.Visible = True)) Then
            MsgBox("Verifica que hayas completado todos los datos", MsgBoxStyle.Information, "Faltan datos ...")
            Exit Sub
        End If
        tipo = 1
        Close()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim fImagen As New Imagen
        fImagen.PictureBox1.Image = btnBuscar.Image
        fImagen.ShowDialog()
        btnBuscar.Image = fImagen.PictureBox1.Image
        ruta = fImagen.ruta
        fImagen.Dispose()
    End Sub

    Private Sub btnBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.Click
        tipo = 3
        Close()
    End Sub

    Private Sub txtRecetas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRecetas.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim Fbuscador As New buscador
            Fbuscador.TituloModulo.Text = "Restaurante"
            If rbReceta.Checked = True Then
                'BuscarUnaReceta
                Fbuscador.bd = "Restaurante"
                Fbuscador.tabla = "Recetas"
                Fbuscador.busca = "Nombre"
                Fbuscador.cantidad = 2
                Fbuscador.lblEncabezado.Text = "          RECETAS"
                Fbuscador.consulta = "SELECT ID, Nombre, CostoPorcion FROM Recetas"
                Fbuscador.check = 1
                Fbuscador.ShowDialog()                
                Receta = 1
            Else
                Fbuscador.bd = "Proveeduria"
                Fbuscador.tabla = "inventario"
                Fbuscador.busca = "descripcion"
                Fbuscador.cantidad = 2
                Fbuscador.lblEncabezado.Text = "          Inventario"
                Fbuscador.consulta = "Select codigo,descripcion,costo from inventario"
                Fbuscador.check = 0
                Fbuscador.ShowDialog()
                Receta = 2
            End If
            txtRecetas.Text = Fbuscador.descripcion
            txtId.Text = Fbuscador.Icodigo
            txtRecetas.CharacterCasing = CharacterCasing.Upper
            ID_RECETA = Fbuscador.Icodigo

        End If
    End Sub

    Private Sub Prompt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cConexion As New ConexionR
        Dim bd As New SqlClient.SqlConnection
        Dim rs As SqlClient.SqlDataReader = Nothing
        Cargando = True
        bd = cConexion.Conectar("Restaurante")
        cConexion.GetRecorset(bd, "Select nombre_grupo ,id from grupos_menu", rs)
        While rs.read
            comboGrupos.Items.Add(rs("nombre_grupo"))
            ListItems.Items.Add(rs("id"))
        End While
        cConexion.DesConectar(bd)

        If grupo <> vbNullString Then
            comboGrupos.SelectedIndex = ListItems.FindString(grupo)
        End If

        If Grupo_Bodega.Visible = True And cmbxBodegas.Visible = True Then
            Cargar_Bodegas()
        End If

        If ID_BODEGA <> 7005 Then
            cmbxBodegas.SelectedValue = ID_BODEGA
        End If
        Cargando = False
    End Sub


    Private Sub Cargar_Bodegas()
        Try
            Dim Datasets As New DataSet
            Dim Conexiones As New ConexionR
            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSoft", "Restaurante", "conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()

            Conexiones.GetDataSet(cnx, "Select idBodega, Nombre  from Proveeduria.dbo.Bodega", Datasets, "Bodega")
            cmbxBodegas.DataSource = Datasets.Tables("Bodega")
            cmbxBodegas.DisplayMember = "Nombre"
            cmbxBodegas.ValueMember = "idBodega"
        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        End Try
    End Sub

    Private Sub comboGrupos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboGrupos.SelectedIndexChanged
        ListItems.SelectedIndex = comboGrupos.SelectedIndex
    End Sub

    Private Sub txtDato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDato.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Me.Text = "Nuevo Grupo Mesas" Then
                txtMEsas.Focus()
            End If
        End If
    End Sub

    Private Sub txtMEsas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMEsas.KeyDown
        If e.KeyCode = Keys.Enter Then
            btnAceptar.Focus()
        End If
    End Sub

    Private Sub cmbxBodegas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbxBodegas.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                ID_BODEGA = cmbxBodegas.SelectedValue
                BuscaCostoPromedio()
            End If
        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        End Try
    End Sub

    Private Sub cmbxBodegas_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbxBodegas.SelectedValueChanged
        Try
            If cmbxBodegas.Items.Count > 0 And (IsNumeric(cmbxBodegas.SelectedValue)) Then
                If Cargando = False Then
                    ID_BODEGA = cmbxBodegas.SelectedValue
                End If
                BuscaCostoPromedio()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BuscaCostoPromedio()
        Try
            If txtDato.Text = "" Then Exit Sub
            Dim exe As New ConexionR
            Dim costopromedio As Double
            Dim codigo As Integer
            Dim dsDetalle_Receta As New DataSet
            Dim cnx As New SqlClient.SqlConnection
            Dim fila_receta As DataRow
            codigo = CInt(ID_RECETA)
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()

            If rbReceta.Checked = True Then


                exe.GetDataSet(cnx, "SELECT idDetalle, Descripcion, idReceta, Codigo, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo, dsDetalle_Receta, "Recetas_Detalles")
                For Each fila_receta In dsDetalle_Receta.Tables("Recetas_Detalles").Rows
                    If fila_receta("Articulo") = 0 Then
                        'AQUI MANDA A BUSCAR SI ES UNA RECETA ANIDADA
                        costopromedio += CalculaCostoReceta(fila_receta("Codigo"))
                    Else
                        'SI ES UN ARTICULO DE MENU ENTONCES LO SUMA NADA MAS
                        costopromedio += CalculaCostoArticulo(fila_receta("Codigo"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"))
                    End If
                    If RECETA_COMPLETA = False Then
                        If MsgBox("La receta no esta completa desea continuar", MsgBoxStyle.YesNo, "Servicios Estructurales SeeSoft") <> MsgBoxResult.Yes Then
                            COSTO_PROMEDIO = costopromedio
                            Exit Sub
                        End If
                    End If
                Next
            End If

            'EN CASO DE QUE SEA SOLO UN ARTICULO ASI YA ESTA LISTO
            If rbArticulo.Checked = True Then

                costopromedio = exe.SlqExecuteScalar(cnx, "Select Costo_Promedio From Proveeduria.dbo.ArticulosXBodega Where idBodega = " & ID_BODEGA & " AND Proveeduria.dbo.ArticulosXBodega.Codigo = " & codigo)
                If costopromedio <> 0 Then
                    COSTO_PROMEDIO = costopromedio
                Else
                    MsgBox("Este articulo no esta registrado en esta bodega, proceda a registrarlo o escoja otra bodega")
                End If

            End If
            COSTO_PROMEDIO = costopromedio

        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Sub


    'ESTA FUNCION CALCULA DE FORMA RECURSIVA EL COSTO DE UNA RECETA EN CASO DE QUE TUVIERA ANIDADAS OTRAS RECETAS
    Private Function CalculaCostoReceta(ByVal codigo_receta As Integer) As Double
        Try
            Dim cnx As New SqlClient.SqlConnection
            Dim exe As New ConexionR
            Dim calculando_costo As Double
            Dim dsDetalle_RecetaAnidada As New DataSet
            Dim fila_receta As DataRow
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()
            exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")
            For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows
                If fila_receta("Articulo") = 0 Then
                    calculando_costo += CalculaCostoReceta(fila_receta("Codigo"))
                Else
                    calculando_costo += CalculaCostoArticulo(fila_receta("Codigo"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"))
                End If
            Next
            Return calculando_costo
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function


    'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA 
    Private Function CalculaCostoArticulo(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String) As Double
        Try
            Dim costoXarticulo As Double
            Dim exe As New ConexionR
            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()
            costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
            If costoXarticulo = 0 Then
                Receta_Completa = False
                MsgBox("El articulo " + nombre + " no se encuentra registrado en la bodega escogida, no es posible calcular el costo de este plato", MsgBoxStyle.Information, "Sistemas estructurales SeeSoft")
            End If
            Return costoXarticulo
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function

    Private Sub cmbxBodegas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxBodegas.SelectedIndexChanged

    End Sub
End Class