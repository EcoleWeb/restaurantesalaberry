Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.IO

Public Class Frm_login
    Inherits System.Windows.Forms.Form

#Region "Variables"
    Public Usuario As New Usuario_Logeado
    Friend WithEvents cboxpunto As System.Windows.Forms.ComboBox
    Friend WithEvents AdapterPuntoVenta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Private SubSistema As String
    Dim X As Byte
    Friend tipo As Int16 = 0
    Private conn As New SqlConnection
    Private rdrlogin As SqlDataReader
    Dim contador As Byte
    Dim objmutex As Mutex
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents SqlDeleteCommand As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand As System.Data.SqlClient.SqlCommand
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btMostrarIconoUsuario As System.Windows.Forms.Button
    Friend WithEvents btMostrarIconoClave As System.Windows.Forms.Button
    Public conectado As Boolean
#End Region

#Region " Windows Form Designer generated code "

    Public Sub New(Optional ByVal Sistema As String = "Restaurante")
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()

        SubSistema = Sistema
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txt_clave As System.Windows.Forms.TextBox
    Friend WithEvents bttn_salir As System.Windows.Forms.Button
    Friend WithEvents bttn_aceptar As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents Adapter_Usua_Loggin As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataSetUsuario_logging1 As DataSetUsuario_logging
    Friend WithEvents PictureEdit2 As System.Windows.Forms.PictureBox
    Friend WithEvents lnkCambiarContrasena As System.Windows.Forms.LinkLabel
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_login))
		Me.txt_clave = New System.Windows.Forms.TextBox()
		Me.bttn_salir = New System.Windows.Forms.Button()
		Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
		Me.bttn_aceptar = New System.Windows.Forms.Button()
		Me.ComboBox1 = New System.Windows.Forms.ComboBox()
		Me.DataSetUsuario_logging1 = New SIALIBEB.DataSetUsuario_logging()
		Me.ComboBox2 = New System.Windows.Forms.ComboBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
		Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
		Me.Adapter_Usua_Loggin = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlDeleteCommand = New System.Data.SqlClient.SqlCommand()
		Me.SqlInsertCommand = New System.Data.SqlClient.SqlCommand()
		Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
		Me.SqlUpdateCommand = New System.Data.SqlClient.SqlCommand()
		Me.PictureEdit2 = New System.Windows.Forms.PictureBox()
		Me.lnkCambiarContrasena = New System.Windows.Forms.LinkLabel()
		Me.cboxpunto = New System.Windows.Forms.ComboBox()
		Me.AdapterPuntoVenta = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Button1 = New System.Windows.Forms.Button()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
		Me.btMostrarIconoUsuario = New System.Windows.Forms.Button()
		Me.btMostrarIconoClave = New System.Windows.Forms.Button()
		CType(Me.DataSetUsuario_logging1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.PictureEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'txt_clave
		'
		Me.txt_clave.Anchor = System.Windows.Forms.AnchorStyles.Top
		Me.txt_clave.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txt_clave.Location = New System.Drawing.Point(56, 209)
		Me.txt_clave.MaxLength = 15
		Me.txt_clave.Name = "txt_clave"
		Me.txt_clave.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
		Me.txt_clave.Size = New System.Drawing.Size(243, 23)
		Me.txt_clave.TabIndex = 1
		'
		'bttn_salir
		'
		Me.bttn_salir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.bttn_salir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
		Me.bttn_salir.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.bttn_salir.FlatAppearance.BorderSize = 0
		Me.bttn_salir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.bttn_salir.Image = CType(resources.GetObject("bttn_salir.Image"), System.Drawing.Image)
		Me.bttn_salir.Location = New System.Drawing.Point(279, 2)
		Me.bttn_salir.Name = "bttn_salir"
		Me.bttn_salir.Size = New System.Drawing.Size(45, 46)
		Me.bttn_salir.TabIndex = 3
		Me.ToolTip1.SetToolTip(Me.bttn_salir, "Cancelar")
		'
		'ImageList1
		'
		Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
		Me.ImageList1.Images.SetKeyName(0, "Cancelar V5.png")
		Me.ImageList1.Images.SetKeyName(1, "Aceptar Login V2.png")
		'
		'bttn_aceptar
		'
		Me.bttn_aceptar.BackColor = System.Drawing.Color.LightSkyBlue
		Me.bttn_aceptar.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.bttn_aceptar.FlatAppearance.BorderSize = 0
		Me.bttn_aceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.bttn_aceptar.Image = CType(resources.GetObject("bttn_aceptar.Image"), System.Drawing.Image)
		Me.bttn_aceptar.Location = New System.Drawing.Point(0, 390)
		Me.bttn_aceptar.Name = "bttn_aceptar"
		Me.bttn_aceptar.Size = New System.Drawing.Size(324, 75)
		Me.bttn_aceptar.TabIndex = 2
		Me.ToolTip1.SetToolTip(Me.bttn_aceptar, "Aceptar")
		Me.bttn_aceptar.UseVisualStyleBackColor = False
		'
		'ComboBox1
		'
		Me.ComboBox1.Anchor = System.Windows.Forms.AnchorStyles.Top
		Me.ComboBox1.DataSource = Me.DataSetUsuario_logging1
		Me.ComboBox1.DisplayMember = "Usuarios.Nombre"
		Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.ComboBox1.Location = New System.Drawing.Point(56, 157)
		Me.ComboBox1.Name = "ComboBox1"
		Me.ComboBox1.Size = New System.Drawing.Size(243, 24)
		Me.ComboBox1.TabIndex = 0
		Me.ComboBox1.ValueMember = "Usuarios.Cedula"
		'
		'DataSetUsuario_logging1
		'
		Me.DataSetUsuario_logging1.DataSetName = "DataSetUsuario_logging"
		Me.DataSetUsuario_logging1.Locale = New System.Globalization.CultureInfo("es-MX")
		Me.DataSetUsuario_logging1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'ComboBox2
		'
		Me.ComboBox2.Anchor = System.Windows.Forms.AnchorStyles.Top
		Me.ComboBox2.Enabled = False
		Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.ComboBox2.Items.AddRange(New Object() {"(""Local"")"})
		Me.ComboBox2.Location = New System.Drawing.Point(102, 313)
		Me.ComboBox2.Name = "ComboBox2"
		Me.ComboBox2.Size = New System.Drawing.Size(114, 21)
		Me.ComboBox2.TabIndex = 16
		Me.ComboBox2.Text = "(""Local"")"
		Me.ComboBox2.Visible = False
		'
		'Label1
		'
		Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Top
		Me.Label1.ForeColor = System.Drawing.SystemColors.Highlight
		Me.Label1.Location = New System.Drawing.Point(105, 293)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(64, 16)
		Me.Label1.TabIndex = 17
		Me.Label1.Text = "Engenie"
		Me.Label1.Visible = False
		'
		'Timer1
		'
		Me.Timer1.Enabled = True
		Me.Timer1.Interval = 50
		'
		'SqlConnection1
		'
		Me.SqlConnection1.ConnectionString = "Data Source=Servidor-pc\GeneralHotel;Initial Catalog=Seguridad;Integrated Securit" &
	"y=True"
		Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
		'
		'Adapter_Usua_Loggin
		'
		Me.Adapter_Usua_Loggin.DeleteCommand = Me.SqlDeleteCommand
		Me.Adapter_Usua_Loggin.InsertCommand = Me.SqlInsertCommand
		Me.Adapter_Usua_Loggin.SelectCommand = Me.SqlSelectCommand1
		Me.Adapter_Usua_Loggin.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Clave_Entrada", "Clave_Entrada"), New System.Data.Common.DataColumnMapping("Clave_Interna", "Clave_Interna"), New System.Data.Common.DataColumnMapping("Foto", "Foto")})})
		Me.Adapter_Usua_Loggin.UpdateCommand = Me.SqlUpdateCommand
		'
		'SqlDeleteCommand
		'
		Me.SqlDeleteCommand.CommandText = "DELETE FROM [Usuarios] WHERE (([Id_Usuario] = @Original_Cedula) AND ([Nombre] = @" &
	"Original_Nombre) AND ([Clave_Entrada] = @Original_Clave_Entrada) AND ([Clave_Int" &
	"erna] = @Original_Clave_Interna))"
		Me.SqlDeleteCommand.Connection = Me.SqlConnection1
		Me.SqlDeleteCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Clave_Entrada", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Entrada", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Clave_Interna", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Interna", System.Data.DataRowVersion.Original, Nothing)})
		'
		'SqlInsertCommand
		'
		Me.SqlInsertCommand.CommandText = resources.GetString("SqlInsertCommand.CommandText")
		Me.SqlInsertCommand.Connection = Me.SqlConnection1
		Me.SqlInsertCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 0, "Cedula"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 0, "Nombre"), New System.Data.SqlClient.SqlParameter("@Clave_Entrada", System.Data.SqlDbType.VarChar, 0, "Clave_Entrada"), New System.Data.SqlClient.SqlParameter("@Clave_Interna", System.Data.SqlDbType.VarChar, 0, "Clave_Interna"), New System.Data.SqlClient.SqlParameter("@Foto", System.Data.SqlDbType.Image, 0, "Foto"), New System.Data.SqlClient.SqlParameter("@Id_Usuario", System.Data.SqlDbType.VarChar, 50, "Cedula")})
		'
		'SqlSelectCommand1
		'
		Me.SqlSelectCommand1.CommandText = "SELECT        Id_Usuario AS Cedula, Nombre, Clave_Entrada, Clave_Interna, Foto" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "F" &
	"ROM            Usuarios" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "WHERE        (Inhabilitado = 0)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ORDER BY Nombre"
		Me.SqlSelectCommand1.Connection = Me.SqlConnection1
		'
		'SqlUpdateCommand
		'
		Me.SqlUpdateCommand.CommandText = resources.GetString("SqlUpdateCommand.CommandText")
		Me.SqlUpdateCommand.Connection = Me.SqlConnection1
		Me.SqlUpdateCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 0, "Cedula"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 0, "Nombre"), New System.Data.SqlClient.SqlParameter("@Clave_Entrada", System.Data.SqlDbType.VarChar, 0, "Clave_Entrada"), New System.Data.SqlClient.SqlParameter("@Clave_Interna", System.Data.SqlDbType.VarChar, 0, "Clave_Interna"), New System.Data.SqlClient.SqlParameter("@Foto", System.Data.SqlDbType.Image, 0, "Foto"), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Clave_Entrada", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Entrada", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Clave_Interna", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Interna", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id_Usuario", System.Data.SqlDbType.VarChar, 50, "Cedula")})
		'
		'PictureEdit2
		'
		Me.PictureEdit2.Anchor = System.Windows.Forms.AnchorStyles.Top
		Me.PictureEdit2.BackgroundImage = Global.SIALIBEB.My.Resources.Resources.blanco_1
		Me.PictureEdit2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
		Me.PictureEdit2.InitialImage = Nothing
		Me.PictureEdit2.Location = New System.Drawing.Point(84, 293)
		Me.PictureEdit2.Name = "PictureEdit2"
		Me.PictureEdit2.Size = New System.Drawing.Size(157, 51)
		Me.PictureEdit2.TabIndex = 19
		Me.PictureEdit2.TabStop = False
		'
		'lnkCambiarContrasena
		'
		Me.lnkCambiarContrasena.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.lnkCambiarContrasena.Font = New System.Drawing.Font("Trebuchet MS", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lnkCambiarContrasena.LinkColor = System.Drawing.Color.LightSteelBlue
		Me.lnkCambiarContrasena.Location = New System.Drawing.Point(163, 366)
		Me.lnkCambiarContrasena.Name = "lnkCambiarContrasena"
		Me.lnkCambiarContrasena.Size = New System.Drawing.Size(149, 21)
		Me.lnkCambiarContrasena.TabIndex = 20
		Me.lnkCambiarContrasena.TabStop = True
		Me.lnkCambiarContrasena.Text = "Cambiar contrase�a"
		Me.lnkCambiarContrasena.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.ToolTip1.SetToolTip(Me.lnkCambiarContrasena, "De click aqu� para cambiar la contrase�a del usuario seleccionado.")
		'
		'cboxpunto
		'
		Me.cboxpunto.Anchor = System.Windows.Forms.AnchorStyles.Top
		Me.cboxpunto.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetUsuario_logging1, "PuntoVenta.IdPuntoVenta", True))
		Me.cboxpunto.DataSource = Me.DataSetUsuario_logging1
		Me.cboxpunto.DisplayMember = "PuntoVenta.Nombre"
		Me.cboxpunto.Enabled = False
		Me.cboxpunto.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.cboxpunto.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cboxpunto.FormattingEnabled = True
		Me.cboxpunto.Location = New System.Drawing.Point(31, 105)
		Me.cboxpunto.Name = "cboxpunto"
		Me.cboxpunto.Size = New System.Drawing.Size(268, 24)
		Me.cboxpunto.TabIndex = 21
		Me.cboxpunto.ValueMember = "PuntoVenta.IdPuntoVenta"
		'
		'AdapterPuntoVenta
		'
		Me.AdapterPuntoVenta.SelectCommand = Me.SqlCommand1
		Me.AdapterPuntoVenta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "PuntoVenta", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("IdPuntoVenta", "IdPuntoVenta"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Id_Bodega", "Id_Bodega")})})
		'
		'SqlCommand1
		'
		Me.SqlCommand1.CommandText = "SELECT     IdPuntoVenta, Nombre, Id_Bodega" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM         Hotel.dbo.PuntoVenta" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "WH" &
	"ERE     (Tipo = 'RESTAURANTE') OR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                      (Tipo = 'BAR')"
		Me.SqlCommand1.Connection = Me.SqlConnection1
		'
		'Label2
		'
		Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Top
		Me.Label2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetUsuario_logging1, "PuntoVenta.Id_Bodega", True))
		Me.Label2.ForeColor = System.Drawing.SystemColors.Highlight
		Me.Label2.Location = New System.Drawing.Point(202, 112)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(24, 21)
		Me.Label2.TabIndex = 23
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Button1
		'
		Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.Top
		Me.Button1.FlatAppearance.BorderSize = 0
		Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.Button1.Font = New System.Drawing.Font("Trebuchet MS", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Button1.ForeColor = System.Drawing.Color.LightSteelBlue
		Me.Button1.Location = New System.Drawing.Point(31, 249)
		Me.Button1.Name = "Button1"
		Me.Button1.Size = New System.Drawing.Size(166, 31)
		Me.Button1.TabIndex = 24
		Me.Button1.Text = "Block n�merico t�ctil."
		Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.ToolTip1.SetToolTip(Me.Button1, "Espliega keypad n�merico para ingresar la clave de acceso.")
		Me.Button1.UseVisualStyleBackColor = True
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.ForeColor = System.Drawing.Color.LightSteelBlue
		Me.Label3.Location = New System.Drawing.Point(25, 10)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(195, 35)
		Me.Label3.TabIndex = 25
		Me.Label3.Text = "INICIAR SESION"
		'
		'btMostrarIconoUsuario
		'
		Me.btMostrarIconoUsuario.BackgroundImage = CType(resources.GetObject("btMostrarIconoUsuario.BackgroundImage"), System.Drawing.Image)
		Me.btMostrarIconoUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
		Me.btMostrarIconoUsuario.FlatAppearance.BorderSize = 0
		Me.btMostrarIconoUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btMostrarIconoUsuario.ImageList = Me.ImageList1
		Me.btMostrarIconoUsuario.Location = New System.Drawing.Point(31, 157)
		Me.btMostrarIconoUsuario.Name = "btMostrarIconoUsuario"
		Me.btMostrarIconoUsuario.Size = New System.Drawing.Size(28, 24)
		Me.btMostrarIconoUsuario.TabIndex = 26
		Me.btMostrarIconoUsuario.UseVisualStyleBackColor = True
		'
		'btMostrarIconoClave
		'
		Me.btMostrarIconoClave.BackgroundImage = CType(resources.GetObject("btMostrarIconoClave.BackgroundImage"), System.Drawing.Image)
		Me.btMostrarIconoClave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
		Me.btMostrarIconoClave.FlatAppearance.BorderSize = 0
		Me.btMostrarIconoClave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btMostrarIconoClave.ImageList = Me.ImageList1
		Me.btMostrarIconoClave.Location = New System.Drawing.Point(31, 209)
		Me.btMostrarIconoClave.Name = "btMostrarIconoClave"
		Me.btMostrarIconoClave.Size = New System.Drawing.Size(28, 23)
		Me.btMostrarIconoClave.TabIndex = 27
		Me.btMostrarIconoClave.UseVisualStyleBackColor = True
		'
		'Frm_login
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
		Me.BackColor = System.Drawing.Color.DimGray
		Me.CancelButton = Me.bttn_salir
		Me.ClientSize = New System.Drawing.Size(324, 465)
		Me.ControlBox = False
		Me.Controls.Add(Me.btMostrarIconoClave)
		Me.Controls.Add(Me.btMostrarIconoUsuario)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Button1)
		Me.Controls.Add(Me.PictureEdit2)
		Me.Controls.Add(Me.ComboBox1)
		Me.Controls.Add(Me.cboxpunto)
		Me.Controls.Add(Me.lnkCambiarContrasena)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.ComboBox2)
		Me.Controls.Add(Me.bttn_salir)
		Me.Controls.Add(Me.bttn_aceptar)
		Me.Controls.Add(Me.txt_clave)
		Me.Controls.Add(Me.Label2)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "Frm_login"
		Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		CType(Me.DataSetUsuario_logging1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.PictureEdit2, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region "Load"
	Private Sub Frm_login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", SubSistema, "Conexion")
            Me.Adapter_Usua_Loggin.Fill(Me.DataSetUsuario_logging1, "Usuarios")
            Me.AdapterPuntoVenta.Fill(Me.DataSetUsuario_logging1, "PuntoVenta")
            cboxpunto.Text = GetSetting("SeeSOFT", "Restaurante", "LastPoint")
            Me.ComboBox1.Text = GetSetting("SeeSOFT", "Restaurante", "LastUser")



			Dim regKey = GetSetting("SeeSoft", "Restaurante", "ModificaTamano")
			If regKey = "" Then

				SaveSetting("SeeSoft", "Restaurante", "anchoComanda", "80")
				SaveSetting("SeeSoft", "Restaurante", "largoComanda", "120")
				SaveSetting("SeeSoft", "Restaurante", "largoMesas", "77")
				SaveSetting("SeeSoft", "Restaurante", "anchoMesas", "77")

			End If



			'objmutex = New Mutex(False, "SINGLE_INSTANCE_APP_MUTEX")
			'If objmutex.WaitOne(0, False) = False Then
			'    objmutex.Close()
			'    objmutex = Nothing
			'    MessageBox.Show("Hay una instancia de la aplicaci�n corriendo actualmente.  El m�dulo se desactivar�", "Sistema Restaurante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
			'    Exit Sub
			'End If

		Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Aceptar"
    Private Sub bttn_aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bttn_aceptar.Click
        If Loggin_Usuario() Then
            Me.Close()
            conectado = True
        Else
            conectado = False
        End If
    End Sub
#End Region

#Region "Salir"
    Private Sub bttn_salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bttn_salir.Click
        conectado = False
        Me.SqlConnection1.Close()
        Me.Close()
    End Sub
#End Region

#Region "Loggin_Usuario"
    Function Loggin_Usuario() As Boolean
        Try
            If Me.BindingContext(Me.DataSetUsuario_logging1.Usuarios).Count > 0 Then
                Dim Usuario_autorizadores() As System.Data.DataRow
                Dim Usua As System.Data.DataRow
                Usuario_autorizadores = Me.DataSetUsuario_logging1.Usuarios.Select("Clave_Entrada = '" & txt_clave.Text & "' And Nombre = '" & Me.ComboBox1.Text & "'")
                If Usuario_autorizadores.Length <> 0 Then
                    Usua = Usuario_autorizadores(0)
                    Usuario.Cedula = Usua("Cedula")
                    Usuario.Nombre = Usua("Nombre")
                    Usuario.Clave_Entrada = Usua("Clave_Entrada")
                    Usuario.Clave_Interna = Usua("Clave_Interna")

                    Usuario.PuntoVenta = Me.cboxpunto.SelectedValue 'Trim(txtpuntoventa.Text)
                    Usuario.NombrePunto = Me.cboxpunto.Text
                    'Usuario.Bodega = Me.Label2.Text
                    If Not GetSetting("SeeSOFT", "Restaurante", "Clave").Equals("") Then
                        SaveSetting("SeeSOFT", "Restaurante", "Clave", Usua("Clave_Interna"))
                    End If


                    SaveSetting("SeeSOFT", "Restaurante", "LastPoint", cboxpunto.Text)
                    SaveSetting("SeeSOFT", "Restaurante", "LastUser", ComboBox1.Text)

                    Me.DialogResult = DialogResult.OK
                    Return True

                Else ' si no existe una contrase�la como esta
                    MsgBox("Clave Entrada incorrecta", MsgBoxStyle.Exclamation)
                    txt_clave.Text = ""
                    txt_clave.Focus()
                    Return False
                End If
            Else

                MsgBox("No Existen Usuarios, ingrese datos")
                Return False
                txt_clave.Focus()
            End If
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function
#End Region

#Region "Controles Funciones"
    Private Sub txt_clave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_clave.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Loggin_Usuario() Then
                Me.Close()
                conectado = True
            End If
        End If
    End Sub

    Private Sub cboxpunto_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboxpunto.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboBox1.Focus()
        End If
    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            txt_clave.Focus()
        End If
    End Sub
#End Region

#Region "Cambiar Contrase�a"
    Private Sub lnkCambiarContrasena_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCambiarContrasena.LinkClicked
        Dim frmCambiarContrasena As New frmCambioContrasena
        frmCambiarContrasena.IdUsuario = DataSetUsuario_logging1.Usuarios(Me.ComboBox1.SelectedIndex).Cedula
        frmCambiarContrasena.ContrasenaActual = Me.txt_clave.Text
        frmCambiarContrasena.ShowDialog()
        Me.Adapter_Usua_Loggin.Fill(Me.DataSetUsuario_logging1, "Usuarios")
    End Sub
#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '----------------------------------------------------------------------------------------------------------------------------

        Dim rEmpleado0 As New registro
        rEmpleado0.Text = "DIGITE CONTRASE�A"
        rEmpleado0.txtCodigo.PasswordChar = "*"

        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO

        clave = User_Log.Clave_Interna
        rEmpleado0.iOpcion = 1
        rEmpleado0.ShowDialog()
        Me.txt_clave.Text = rEmpleado0.txtCodigo.Text

        rEmpleado0.Dispose()

        If Loggin_Usuario() Then
            Me.Close()
            conectado = True
        Else
            conectado = False
        End If

    End Sub

End Class