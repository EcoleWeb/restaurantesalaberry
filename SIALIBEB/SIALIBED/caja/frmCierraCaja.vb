﻿Public Class frmCierraCaja
    Dim id_usuario As String
    Public NUMAPERTURA As Long

    Private Sub CargarInfo()
        Dim documentos As New DataTable
        Dim apertura As New DataTable
        Dim subtotal As New DataTable
        cFunciones.Llenar_Tabla_Generico("select documento as DOCUMENTO,tipodocumento AS TIPO,montopago AS TOTAL,nombremoneda AS MONEDA from opcionesdepago where tipodocumento <> 'MCS' and tipodocumento <> 'MCE' and numapertura = " & NUMAPERTURA, documentos, GetSetting("SeeSoft", "Restaurante", "Conexion"))
        Me.DataGridView1.DataSource = documentos
        cFunciones.Llenar_Tabla_Generico("SELECT * FROM APERTURACAJA WHERE NAPERTURA = " & NUMAPERTURA, apertura, GetSetting("SeeSoft", "Restaurante", "Conexion"))
        If apertura.Rows.Count > 0 Then
            lblApertura.Text = apertura.Rows(0).Item("napertura")
            lblCajero.Text = apertura.Rows(0).Item("nombre")
            lblFecha.Text = apertura.Rows(0).Item("fecha")
        Else
            lblApertura.Text = ""
            lblCajero.Text = ""
            lblFecha.Text = ""
        End If
        cFunciones.Llenar_Tabla_Generico("select isnull(sum(montopago*tipocambio),0) as total from opcionesdepago where tipodocumento <> 'MCS' and tipodocumento <> 'MCE' and numapertura = " & NUMAPERTURA, subtotal, GetSetting("Seesoft", "Restaurante", "Conexion"))
        If subtotal.Rows.Count > 0 Then
            Me.lblMonto.Text = "¢ " & Format(Val(CDbl(subtotal.Rows(0).Item(0))), "#,###,###,##0.00")
        End If

    End Sub

    Private Sub Loggin_Usuario()
        Try
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("select * from usuarios where clave_interna = '" & Me.txtUsuario.Text & "'", dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            If dt.Rows.Count > 0 Then
                Me.txtNombreUsuario.Text = dt.Rows(0).Item("nombre")
                Me.id_usuario = dt.Rows(0).Item("id_usuario")
                Me.btnCerrar.Enabled = True
                Me.btnCerrar.Enabled = VerificandoAcceso_a_Modulos("frmCierraCaja", "Cierre Forzado", id_usuario, "A&B")
            Else
                Me.btnCerrar.Enabled = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, Text)
        End Try
    End Sub

    Private Sub txtUsuario_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.Loggin_Usuario()
        End If
    End Sub

    Private Sub frmCierraCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargarInfo()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Dim frm As New frmQuestion
        frm.LblMensaje.Text = "Confirmar que desea Cerrar la Caja # " & lblApertura.Text & " ?"
        frm.ShowDialog()
        If frm.resultado = True Then
            Try
                General.Ejecuta("update aperturacaja set estado = 'C' where napertura = " & NUMAPERTURA)
                Me.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try            
        End If

    End Sub
End Class
