﻿Public Class frmInfoCajas
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.DataGridView1.DataSource = General.Ejecuta("SELECT NAPERTURA AS NUMERO_APERTURA, NOMBRE AS CAJERO, FECHA, 'ABIERTA' as ESTADO  from aperturacaja WHERE ANULADO = 0 AND ESTADO = 'A'")
    End Sub

    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        On Error Resume Next
        Dim frm As New frmCierraCaja
        frm.NUMAPERTURA = Me.DataGridView1.Item(0, e.RowIndex).Value
        frm.ShowDialog()
        Me.DataGridView1.DataSource = General.Ejecuta("SELECT NAPERTURA AS NUMERO_APERTURA, NOMBRE AS CAJERO, FECHA, 'ABIERTA' as ESTADO  from aperturacaja WHERE ANULADO = 0 AND ESTADO = 'A'")
    End Sub
End Class
