﻿Public Class ClientesHacienda
    Public Shared Function AbrirForm(Optional Comision As Boolean = False) As ClienteSeleccionado
        Dim cliente As New ClienteSeleccionado
        If Comision Then
            Dim frmC As New frmClienteComision
            If frmC.ShowDialog() = DialogResult.OK Then
                cliente.Id = frmC.ID
                cliente.Nombre = frmC.txNombreCliente.Text
                cliente.Comision = frmC.numComision.Value
                cliente.Telefono = frmC.txTelefonoCliente.Text
            End If
            Return cliente
        End If
        Dim frm As New frmCliente
        If frm.ShowDialog() = DialogResult.OK Then
            cliente.Id = frm.ID
            cliente.Nombre = frm.txtNombre.Text
            cliente.Telefono = frm.txTelefonoCliente.Text
        End If
        Return cliente
    End Function

End Class
Public Class ClienteSeleccionado
    Public Id As Integer = 0
    Public Nombre As String = ""
    Public Comision As Double = 0
    Public Telefono As String = ""
End Class

