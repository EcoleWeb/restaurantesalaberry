﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CorreoControl
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.txCorreoCliente = New System.Windows.Forms.TextBox()
		Me.chSelCorreo = New System.Windows.Forms.CheckBox()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(15, 8)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(38, 13)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Correo"
		'
		'txCorreoCliente
		'
		Me.txCorreoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txCorreoCliente.Location = New System.Drawing.Point(108, 4)
		Me.txCorreoCliente.Name = "txCorreoCliente"
		Me.txCorreoCliente.Size = New System.Drawing.Size(390, 26)
		Me.txCorreoCliente.TabIndex = 1
		'
		'chSelCorreo
		'
		Me.chSelCorreo.AutoSize = True
		Me.chSelCorreo.Location = New System.Drawing.Point(75, 7)
		Me.chSelCorreo.Name = "chSelCorreo"
		Me.chSelCorreo.Size = New System.Drawing.Size(15, 14)
		Me.chSelCorreo.TabIndex = 2
		Me.chSelCorreo.UseVisualStyleBackColor = True
		'
		'CorreoControl
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.Controls.Add(Me.chSelCorreo)
		Me.Controls.Add(Me.txCorreoCliente)
		Me.Controls.Add(Me.Label1)
		Me.Name = "CorreoControl"
		Me.Size = New System.Drawing.Size(545, 30)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txCorreoCliente As System.Windows.Forms.TextBox
    Friend WithEvents chSelCorreo As System.Windows.Forms.CheckBox

End Class
