﻿Imports System.Data.SqlClient


Public Class conexionCliente

    Public Shared Function cadenaConexion() As String
        Return GetSetting("seesoft", "Hotel", "conexion")
    End Function

    Public Shared Sub cargarDatos(ByVal consulta As String, ByVal dt As DataTable)
        Dim enlace As New SqlConnection(cadenaConexion)
        Dim sqlcomando As New SqlCommand()
        sqlcomando.Connection = enlace
        sqlcomando.CommandText = consulta
        Dim adapter As New SqlDataAdapter(sqlcomando)
        dt.Rows.Clear()
        adapter.Fill(dt)

    End Sub
    Public Shared Sub cargarDatosConParametros(ByVal consulta As SqlCommand, ByVal dt As DataTable)
        Dim enlace As New SqlConnection(cadenaConexion)
        consulta.Connection = enlace
        Dim adapter As New SqlDataAdapter(consulta)
        dt.Rows.Clear()
        adapter.Fill(dt)

    End Sub
    Public Shared Sub guardarDatos(ByVal consulta As String)
        Dim enlace As New SqlConnection(cadenaConexion)
        enlace.Open()
        Dim sqlcomando As New SqlCommand()
        sqlcomando.Connection = enlace
        sqlcomando.CommandText = consulta
        sqlcomando.ExecuteNonQuery()
        enlace.Close()

    End Sub

    Public Shared Sub guardarDatosConParametros(ByVal consulta As SqlCommand)
        Dim enlace As New SqlConnection(cadenaConexion)
        enlace.Open()
        consulta.Connection = enlace
        consulta.ExecuteNonQuery()
        enlace.Close()

    End Sub

End Class
