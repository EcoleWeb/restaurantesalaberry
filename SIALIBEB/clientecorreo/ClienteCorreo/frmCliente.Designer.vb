﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Identificacion = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txTelefonoCliente = New System.Windows.Forms.TextBox()
        Me.tipoCliente = New System.Windows.Forms.ComboBox()
        Me.btGuardar = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.pnCorreos = New System.Windows.Forms.FlowLayoutPanel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cmbIdentificacion = New System.Windows.Forms.ComboBox()
        Me.gbExoneracion = New System.Windows.Forms.GroupBox()
        Me.dtpFechaVencimientoExoneracion = New System.Windows.Forms.DateTimePicker()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtDocExoneracion = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboTipoCliente = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.listNombres = New System.Windows.Forms.ListBox()
        Me.gbExoneracion.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(35, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tipo de Identificación:"
        '
        'Identificacion
        '
        Me.Identificacion.AutoSize = True
        Me.Identificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Identificacion.Location = New System.Drawing.Point(35, 55)
        Me.Identificacion.Name = "Identificacion"
        Me.Identificacion.Size = New System.Drawing.Size(88, 13)
        Me.Identificacion.TabIndex = 1
        Me.Identificacion.Text = "Identificacion:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(35, 89)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Nombre:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(343, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Telefono:"
        '
        'txTelefonoCliente
        '
        Me.txTelefonoCliente.Enabled = False
        Me.txTelefonoCliente.Location = New System.Drawing.Point(411, 20)
        Me.txTelefonoCliente.Name = "txTelefonoCliente"
        Me.txTelefonoCliente.Size = New System.Drawing.Size(136, 20)
        Me.txTelefonoCliente.TabIndex = 2
        '
        'tipoCliente
        '
        Me.tipoCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.tipoCliente.FormattingEnabled = True
        Me.tipoCliente.Location = New System.Drawing.Point(191, 20)
        Me.tipoCliente.Name = "tipoCliente"
        Me.tipoCliente.Size = New System.Drawing.Size(140, 21)
        Me.tipoCliente.TabIndex = 0
        '
        'btGuardar
        '
        Me.btGuardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btGuardar.Location = New System.Drawing.Point(601, 18)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(125, 23)
        Me.btGuardar.TabIndex = 10001
        Me.btGuardar.Text = "Guardar"
        Me.btGuardar.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(603, 50)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(122, 23)
        Me.Button2.TabIndex = 10002
        Me.Button2.Text = "Realizar Factura"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'pnCorreos
        '
        Me.pnCorreos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnCorreos.AutoScroll = True
        Me.pnCorreos.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.pnCorreos.Location = New System.Drawing.Point(38, 137)
        Me.pnCorreos.Name = "pnCorreos"
        Me.pnCorreos.Size = New System.Drawing.Size(687, 92)
        Me.pnCorreos.TabIndex = 12
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(603, 84)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(122, 23)
        Me.Button1.TabIndex = 10003
        Me.Button1.Text = "Agregar Correo"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cmbIdentificacion
        '
        Me.cmbIdentificacion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbIdentificacion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbIdentificacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbIdentificacion.FormattingEnabled = True
        Me.cmbIdentificacion.Location = New System.Drawing.Point(191, 55)
        Me.cmbIdentificacion.Name = "cmbIdentificacion"
        Me.cmbIdentificacion.Size = New System.Drawing.Size(355, 21)
        Me.cmbIdentificacion.TabIndex = 10004
        '
        'gbExoneracion
        '
        Me.gbExoneracion.Controls.Add(Me.dtpFechaVencimientoExoneracion)
        Me.gbExoneracion.Controls.Add(Me.Label22)
        Me.gbExoneracion.Controls.Add(Me.txtDocExoneracion)
        Me.gbExoneracion.Controls.Add(Me.Label10)
        Me.gbExoneracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.gbExoneracion.ForeColor = System.Drawing.Color.Black
        Me.gbExoneracion.Location = New System.Drawing.Point(287, 245)
        Me.gbExoneracion.Name = "gbExoneracion"
        Me.gbExoneracion.Size = New System.Drawing.Size(439, 69)
        Me.gbExoneracion.TabIndex = 10006
        Me.gbExoneracion.TabStop = False
        Me.gbExoneracion.Text = "Exoneración"
        '
        'dtpFechaVencimientoExoneracion
        '
        Me.dtpFechaVencimientoExoneracion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVencimientoExoneracion.Location = New System.Drawing.Point(189, 37)
        Me.dtpFechaVencimientoExoneracion.Name = "dtpFechaVencimientoExoneracion"
        Me.dtpFechaVencimientoExoneracion.Size = New System.Drawing.Size(156, 20)
        Me.dtpFechaVencimientoExoneracion.TabIndex = 3
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(185, 20)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(119, 13)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "Fecha Vencimiento:"
        '
        'txtDocExoneracion
        '
        Me.txtDocExoneracion.Location = New System.Drawing.Point(12, 37)
        Me.txtDocExoneracion.Name = "txtDocExoneracion"
        Me.txtDocExoneracion.Size = New System.Drawing.Size(156, 20)
        Me.txtDocExoneracion.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Documento :"
        '
        'cboTipoCliente
        '
        Me.cboTipoCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoCliente.FormattingEnabled = True
        Me.cboTipoCliente.Location = New System.Drawing.Point(38, 262)
        Me.cboTipoCliente.Name = "cboTipoCliente"
        Me.cboTipoCliente.Size = New System.Drawing.Size(177, 21)
        Me.cboTipoCliente.TabIndex = 10008
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(35, 246)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(101, 13)
        Me.Label23.TabIndex = 10007
        Me.Label23.Text = "Tipo de Cliente :"
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(191, 84)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(355, 20)
        Me.txtNombre.TabIndex = 10009
        '
        'listNombres
        '
        Me.listNombres.FormattingEnabled = True
        Me.listNombres.Location = New System.Drawing.Point(191, 103)
        Me.listNombres.Name = "listNombres"
        Me.listNombres.Size = New System.Drawing.Size(355, 4)
        Me.listNombres.TabIndex = 10010
        Me.listNombres.Visible = False
        '
        'frmCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(740, 326)
        Me.Controls.Add(Me.listNombres)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.cboTipoCliente)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.gbExoneracion)
        Me.Controls.Add(Me.cmbIdentificacion)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.pnCorreos)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.tipoCliente)
        Me.Controls.Add(Me.txTelefonoCliente)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Identificacion)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximumSize = New System.Drawing.Size(756, 600)
        Me.MinimumSize = New System.Drawing.Size(697, 250)
        Me.Name = "frmCliente"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clientes"
        Me.gbExoneracion.ResumeLayout(False)
        Me.gbExoneracion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Identificacion As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
	Friend WithEvents txTelefonoCliente As System.Windows.Forms.TextBox
	Friend WithEvents btGuardar As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents pnCorreos As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Private WithEvents tipoCliente As ComboBox
	Friend WithEvents cmbIdentificacion As ComboBox
    Friend WithEvents gbExoneracion As GroupBox
    Friend WithEvents dtpFechaVencimientoExoneracion As DateTimePicker
    Friend WithEvents Label22 As Label
    Friend WithEvents txtDocExoneracion As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents cboTipoCliente As ComboBox
    Friend WithEvents Label23 As Label
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents listNombres As ListBox
End Class
