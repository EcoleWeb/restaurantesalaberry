﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmClienteComision
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Identificacion = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txIdentiCliente = New System.Windows.Forms.TextBox()
        Me.txNombreCliente = New System.Windows.Forms.TextBox()
        Me.txTelefonoCliente = New System.Windows.Forms.TextBox()
        Me.tipoCliente = New System.Windows.Forms.ComboBox()
        Me.btGuardar = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.pnCorreos = New System.Windows.Forms.FlowLayoutPanel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.numComision = New System.Windows.Forms.NumericUpDown()
        CType(Me.numComision, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(49, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(92, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tipo de Cliente:"
        '
        'Identificacion
        '
        Me.Identificacion.AutoSize = True
        Me.Identificacion.Location = New System.Drawing.Point(49, 55)
        Me.Identificacion.Name = "Identificacion"
        Me.Identificacion.Size = New System.Drawing.Size(82, 15)
        Me.Identificacion.TabIndex = 1
        Me.Identificacion.Text = "Identificacion:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(49, 89)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Nombre:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(294, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 15)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Telefono:"
        '
        'txIdentiCliente
        '
        Me.txIdentiCliente.AutoCompleteCustomSource.AddRange(New String() {"klllll", "lllljhjihgyugfyu", "ghughghjg"})
        Me.txIdentiCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txIdentiCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txIdentiCliente.Location = New System.Drawing.Point(157, 52)
        Me.txIdentiCliente.Name = "txIdentiCliente"
        Me.txIdentiCliente.Size = New System.Drawing.Size(312, 20)
        Me.txIdentiCliente.TabIndex = 1
        '
        'txNombreCliente
        '
        Me.txNombreCliente.Enabled = False
        Me.txNombreCliente.Location = New System.Drawing.Point(157, 89)
        Me.txNombreCliente.Name = "txNombreCliente"
        Me.txNombreCliente.Size = New System.Drawing.Size(312, 20)
        Me.txNombreCliente.TabIndex = 3
        '
        'txTelefonoCliente
        '
        Me.txTelefonoCliente.Enabled = False
        Me.txTelefonoCliente.Location = New System.Drawing.Point(352, 20)
        Me.txTelefonoCliente.Name = "txTelefonoCliente"
        Me.txTelefonoCliente.Size = New System.Drawing.Size(117, 20)
        Me.txTelefonoCliente.TabIndex = 2
        '
        'tipoCliente
        '
        Me.tipoCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.tipoCliente.FormattingEnabled = True
        Me.tipoCliente.Location = New System.Drawing.Point(157, 20)
        Me.tipoCliente.Name = "tipoCliente"
        Me.tipoCliente.Size = New System.Drawing.Size(121, 21)
        Me.tipoCliente.TabIndex = 0
        '
        'btGuardar
        '
        Me.btGuardar.Location = New System.Drawing.Point(475, 20)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(107, 52)
        Me.btGuardar.TabIndex = 10001
        Me.btGuardar.Text = "Guardar"
        Me.btGuardar.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(475, 89)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(107, 53)
        Me.Button2.TabIndex = 10002
        Me.Button2.Text = "Realizar Factura"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'pnCorreos
        '
        Me.pnCorreos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnCorreos.AutoScroll = True
        Me.pnCorreos.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.pnCorreos.Location = New System.Drawing.Point(52, 160)
        Me.pnCorreos.Name = "pnCorreos"
        Me.pnCorreos.Size = New System.Drawing.Size(530, 147)
        Me.pnCorreos.TabIndex = 12
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(297, 119)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(172, 23)
        Me.Button1.TabIndex = 10003
        Me.Button1.Text = "Agregar Correo"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(49, 124)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 15)
        Me.Label2.TabIndex = 10004
        Me.Label2.Text = "Comision"
        '
        'numComision
        '
        Me.numComision.DecimalPlaces = 2
        Me.numComision.Enabled = False
        Me.numComision.Location = New System.Drawing.Point(158, 122)
        Me.numComision.Name = "numComision"
        Me.numComision.Size = New System.Drawing.Size(120, 20)
        Me.numComision.TabIndex = 10006
        '
        'frmClienteComision
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(594, 319)
        Me.Controls.Add(Me.numComision)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.pnCorreos)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.tipoCliente)
        Me.Controls.Add(Me.txTelefonoCliente)
        Me.Controls.Add(Me.txNombreCliente)
        Me.Controls.Add(Me.txIdentiCliente)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Identificacion)
        Me.Controls.Add(Me.Label1)
        Me.MaximumSize = New System.Drawing.Size(650, 600)
        Me.MinimumSize = New System.Drawing.Size(610, 360)
        Me.Name = "frmClienteComision"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clientes"
        CType(Me.numComision, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Identificacion As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txIdentiCliente As System.Windows.Forms.TextBox
    Friend WithEvents txNombreCliente As System.Windows.Forms.TextBox
    Friend WithEvents txTelefonoCliente As System.Windows.Forms.TextBox
    Friend WithEvents btGuardar As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents pnCorreos As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Private WithEvents tipoCliente As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents numComision As NumericUpDown
End Class
