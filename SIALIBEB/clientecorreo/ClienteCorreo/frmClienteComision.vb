﻿Public Class frmClienteComision

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Dim tiposIdCliente As New DataTable
    Dim clientes As New DataTable
    Dim clientesCorrres As New DataTable
    Dim correos As New List(Of CorreoControl)
    Dim selCorreos As New List(Of CorreoControl)

    Public ID As Integer = 0

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        conexionCliente.cargarDatos("SELECT [id_Tipo_Identificacion],[Desc_Tipo_Identificacion] FROM [dbo].[TB_CE_TIPO_IDENTIFICACION]", tiposIdCliente)
        tipoCliente.DataSource = tiposIdCliente
        tipoCliente.DisplayMember = "Desc_Tipo_Identificacion"
        tipoCliente.ValueMember = "id_Tipo_Identificacion"

        conexionCliente.cargarDatos("SELECT Cedula FROM Cliente", clientes)
        txIdentiCliente.AutoCompleteCustomSource.Clear()
        For Each fila As DataRow In clientes.Rows
            txIdentiCliente.AutoCompleteCustomSource.Add(fila.Item("Cedula"))
        Next

    End Sub

    Private Sub identiCliente_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txIdentiCliente.KeyDown
        If e.KeyCode = Keys.Enter Then
            Buscar()
        End If
    End Sub


    Sub Buscar()
        Dim sqlComando As New SqlClient.SqlCommand
        Dim clienteBusqueda As New DataTable
        correos.Clear()
        sqlComando.CommandText = "SELECT Id, Nombre, ISNULL(Email,'') as Ema, ISNULL(Telefono1,'') as Tel, ISNULL(Comicion,0) as Comision FROM Cliente where Cedula = @Cedula"
        sqlComando.Parameters.AddWithValue("@cedula", txIdentiCliente.Text)
        conexionCliente.cargarDatosConParametros(sqlComando, clienteBusqueda)
        If clienteBusqueda.Rows.Count > 0 Then
            ID = clienteBusqueda.Rows(0).Item("Id")
            txTelefonoCliente.Text = clienteBusqueda.Rows(0).Item("Tel")
            txNombreCliente.Text = clienteBusqueda.Rows(0).Item("Nombre")
            numComision.Value = clienteBusqueda.Rows(0).Item("Comision")
            BuscarCorreo(ID)

        Else
            ID = 0
            'txTelefonoCliente.Clear()
            'txNombreCliente.Clear()
            mostrarCorreos()
        End If
        txTelefonoCliente.Enabled = True
        txNombreCliente.Enabled = True
        numComision.Enabled = True
    End Sub
    Sub BuscarCorreo(ByVal codCliente As Integer)
        Dim sqlComando As New SqlClient.SqlCommand
        Dim correoBusqueda As New DataTable

        sqlComando.CommandText = "SELECT codCliente, idCorreo, correoCliente, correoSeleccionado FROM CorreosCliente where codCliente = @idCliente"
        sqlComando.Parameters.AddWithValue("@idCliente", codCliente)
        conexionCliente.cargarDatosConParametros(sqlComando, correoBusqueda)
        If correoBusqueda.Rows.Count > 0 Then

            For Each fila As DataRow In correoBusqueda.Rows
                Dim isCheck As Boolean = fila.Item("correoSeleccionado")
                Dim correo As New CorreoControl(isCheck, fila.Item("correoCliente"), fila.Item("idCorreo"), correos)
                correos.Add(correo)
            Next
            'Else
            'ID = 0
        End If
        mostrarCorreos()

    End Sub
    Sub mostrarCorreos()
        pnCorreos.Controls.Clear()
        If correos.Count > 0 Then
            For Each correo As CorreoControl In correos
                pnCorreos.Controls.Add(correo)
            Next
        Else
            AgregarCorreo()

        End If

    End Sub
    Private Sub LimpiarCampos()
        txIdentiCliente.Text = ""
        txNombreCliente.Text = ""
        txTelefonoCliente.Text = ""
        selCorreos.Clear()
        correos.Clear()
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        Guardar()
    End Sub
    Sub Guardar(Optional returning As Boolean = False)
        Dim _r As DialogResult = DialogResult.Cancel
        If txIdentiCliente.Text.Equals("") Or txNombreCliente.Text.Equals("") Or txTelefonoCliente.Text.Equals("") Or (txTelefonoCliente.Text.Length < 8) Then
            If txTelefonoCliente.Text.Equals("") Or (txTelefonoCliente.Text.Length < 8) Then
                MsgBox("Por favor digite un numero valido para el cliente", MsgBoxStyle.Exclamation)
            End If
            If txIdentiCliente.Text.Equals("") Then
                MsgBox("Por favor no deje sin identificación al cliente", MsgBoxStyle.Exclamation)
            End If
            If txNombreCliente.Text.Equals("") Then
                MsgBox("Por favor digite el nombre del cliente", MsgBoxStyle.Exclamation)
            End If
            Return
        Else
            Try
                If IfDo(returning) Then
                    Dim correoPrincipal As String = ""
                    Dim correoSele As Integer = 0
                    For Each correo As CorreoControl In correos
                        If correo.chSelCorreo.Checked Then
                            correoPrincipal = correo.txCorreoCliente.Text
                            correoSele = CInt(correo.chSelCorreo.Checked)
                            'correoSele = correoSele * -1
                        End If
                    Next
                    If ID = 0 Then
                        Dim ejecutar As String = "INSERT INTO [dbo].[Cliente]  ([Nombre]  ,[Observaciones]  ,[Cedula]  ,[Agencia]  ,[Contacto]  ,[Nacionalidad]  ,[Exento]  ,[ImpustoVentas]  ,[Credito]  ,[Telefono1]  ,[Email]  ,[Dereccion]  ,[Limite_Credito]  ,[Plazo_Credito]  ,[Tipo_Precio]  ,[Usuario]  ,[Restriccion_Cuenta]  ,[Comicion]  ,[CodMoneda]  ,[Cliente_Moroso]  ,[NombreJuridico]  ,[CodigoSeguridad]  ,[NumeroTarjeta]  ,[Tipo]  ,[Vence]  ,[Id_Contrato]  ,[Contrato]  ,[Desayunos]  ,[TipoPrecio]  ,[Descuento]  ,[ComisionServicios]  ,[ComisionPaquetes]  ,[EsEmpleado]  ,[Id_Empleado]  ,[Empleado]  ,[SaldoCanjeReal]  ,[SaldoApartado]  ,[IDInternet]  ,[PassInternet]  ,[agente]  ,[tipo_cliente]  ,[IdZona]) " &
                        " VALUES  ('" & txNombreCliente.Text & "'  ,'','" & txIdentiCliente.Text & "'  ,0,'',0,0,0,'' ,'" & txTelefonoCliente.Text & "', '" & correoPrincipal & "' ,''  ,0 ,0 ,0  ,''  ,0  ," & numComision.Value & "  ,0  ,0  ,''  ,''  ,''  ,''  ,''  ,0  ,0  ,0  ,''  ,0  ,0  ,0  ,0  ,''  ,''  ,0  ,0  ,0  ,''  ,0  ,0  ,0) "
                        conexionCliente.guardarDatos(ejecutar)
                        Dim dtIDFactura As New DataTable
                        conexionCliente.cargarDatos("Select TOP 1 ID FROM Cliente ORDER BY ID DESC", dtIDFactura)
                        If dtIDFactura.Rows.Count > 0 Then
                            ID = dtIDFactura.Rows(0).Item("ID")
                            GuardarCorreos()
                        End If
                        _r = DialogResult.OK
                    Else
                        Dim ejecutar As String = "UPDATE Cliente SET Nombre = '" & txNombreCliente.Text & "' ,Telefono1 = '" & txTelefonoCliente.Text & "' ,Email = '" & correoPrincipal & "', Comicion = " & numComision.Value & " WHERE Id = " & ID
                        conexionCliente.guardarDatos(ejecutar)
                        GuardarCorreos()
                        _r = DialogResult.OK
                    End If
                End If

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        If returning Then
            DialogResult = _r
        End If
    End Sub
    Private Function IfDo(ByVal doit As Boolean) As Boolean
        Return If(doit, True, MsgBox("Desea guardar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes)
    End Function
    Sub GuardarCorreos()
        For Each correo As CorreoControl In correos
            Dim correoSele As Integer = 0
            correoSele = CInt(correo.chSelCorreo.Checked)
            'correoSele = correoSele * -1
            If correo.id = 0 Then
                Dim insertarCorreosNuevos As String = "INSERT INTO [dbo].[CorreosCliente]  ([codCliente]  ,[correoCliente]  ,[correoSeleccionado]) " &
                        "VALUES  ('" & ID & "','" & correo.txCorreoCliente.Text & "'  ," & correoSele & ")"
                conexionCliente.guardarDatos(insertarCorreosNuevos)
            Else

                Dim actualizarCorreosNuevos As String = "UPDATE [dbo].[CorreosCliente]  SET [correoCliente] = '" & correo.txCorreoCliente.Text & "' , [correoSeleccionado] = " & correoSele & " WHERE [idCorreo] = " & correo.id
                conexionCliente.guardarDatos(actualizarCorreosNuevos)
            End If

        Next
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Guardar(True)
    End Sub

    Private Sub txIdentiCliente_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txIdentiCliente.Leave
        Buscar()
    End Sub

    Private Sub AgregarCorreo()
        Dim camposCorreo As New CorreoControl(pnCorreos, correos)
        correos.Add(camposCorreo)
        pnCorreos.Controls.Add(camposCorreo)
        Dim heigth As Integer = Size.Height + 30
        Dim nSize As New Size(Size.Width, heigth)
        If nSize.Height < MaximumSize.Height Then
            Size = nSize
        End If
    End Sub

    Private Sub AgregarCorreo_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        AgregarCorreo()
    End Sub

    Private Sub txTelefonoCliente_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txTelefonoCliente.KeyPress
        e.Handled = True
        If (txTelefonoCliente.Text.Length = 0) And (e.KeyChar = "0") Then
            e.Handled = True
        ElseIf Char.IsNumber(e.KeyChar) Or Char.IsControl(e.KeyChar) Then
            e.Handled = False
        End If
    End Sub

    Private Sub numComision_KeyPress(sender As Object, e As KeyPressEventArgs) Handles numComision.KeyPress
        If Char.IsNumber(e.KeyChar) Or Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If

    End Sub

    Private Sub frmClienteComision_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        'DialogResult = DialogResult.Cancel
    End Sub
End Class
