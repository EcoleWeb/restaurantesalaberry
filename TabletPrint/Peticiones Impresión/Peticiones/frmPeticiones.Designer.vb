﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPeticiones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPeticiones))
        Me.tiRefrescamiento = New System.Windows.Forms.Timer(Me.components)
        Me.bwPeticiones = New System.ComponentModel.BackgroundWorker()
        Me.ssEstado = New System.Windows.Forms.StatusStrip()
        Me.lbEstadoObtencion = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbEstadoImpresion = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbRevisando = New System.Windows.Forms.ToolStripStatusLabel()
        Me.bwImpresion = New System.ComponentModel.BackgroundWorker()
        Me.dgvPeticiones = New System.Windows.Forms.DataGridView()
        Me.CCodigoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImpresoraDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimaModiDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DtsPeticiones = New Peticiones.dtsPeticiones()
        Me.bsPeticiones = New System.Windows.Forms.BindingSource(Me.components)
        Me.PeticionesTableAdapter = New Peticiones.dtsPeticionesTableAdapters.PeticionesTableAdapter()
        Me.niPeticiones = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.btPausa = New System.Windows.Forms.Button()
        Me.DtsImprimeComanda1 = New Peticiones.DtsImprimeComanda()
        Me.ssEstado.SuspendLayout()
        CType(Me.dgvPeticiones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsPeticiones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsPeticiones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsImprimeComanda1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tiRefrescamiento
        '
        Me.tiRefrescamiento.Interval = 1000
        '
        'bwPeticiones
        '
        Me.bwPeticiones.WorkerReportsProgress = True
        Me.bwPeticiones.WorkerSupportsCancellation = True
        '
        'ssEstado
        '
        Me.ssEstado.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lbEstadoObtencion, Me.lbEstadoImpresion, Me.lbRevisando})
        Me.ssEstado.Location = New System.Drawing.Point(0, 357)
        Me.ssEstado.Name = "ssEstado"
        Me.ssEstado.Size = New System.Drawing.Size(590, 22)
        Me.ssEstado.TabIndex = 1
        Me.ssEstado.Text = "StatusStrip1"
        '
        'lbEstadoObtencion
        '
        Me.lbEstadoObtencion.Name = "lbEstadoObtencion"
        Me.lbEstadoObtencion.Size = New System.Drawing.Size(136, 17)
        Me.lbEstadoObtencion.Text = "Obteniendo peticiones..."
        Me.lbEstadoObtencion.Visible = False
        '
        'lbEstadoImpresion
        '
        Me.lbEstadoImpresion.Name = "lbEstadoImpresion"
        Me.lbEstadoImpresion.Size = New System.Drawing.Size(85, 17)
        Me.lbEstadoImpresion.Text = "Imprimiendo..."
        Me.lbEstadoImpresion.Visible = False
        '
        'lbRevisando
        '
        Me.lbRevisando.Name = "lbRevisando"
        Me.lbRevisando.Size = New System.Drawing.Size(64, 17)
        Me.lbRevisando.Text = "Revisando:"
        '
        'bwImpresion
        '
        Me.bwImpresion.WorkerReportsProgress = True
        Me.bwImpresion.WorkerSupportsCancellation = True
        '
        'dgvPeticiones
        '
        Me.dgvPeticiones.AllowUserToAddRows = False
        Me.dgvPeticiones.AllowUserToDeleteRows = False
        Me.dgvPeticiones.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvPeticiones.AutoGenerateColumns = False
        Me.dgvPeticiones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvPeticiones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPeticiones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CCodigoDataGridViewTextBoxColumn, Me.ImpresoraDataGridViewTextBoxColumn, Me.UltimaModiDataGridViewTextBoxColumn, Me.MesaDataGridViewTextBoxColumn, Me.UsuarioDataGridViewTextBoxColumn})
        Me.dgvPeticiones.DataMember = "Peticiones"
        Me.dgvPeticiones.DataSource = Me.DtsPeticiones
        Me.dgvPeticiones.Location = New System.Drawing.Point(12, 38)
        Me.dgvPeticiones.Name = "dgvPeticiones"
        Me.dgvPeticiones.ReadOnly = True
        Me.dgvPeticiones.RowHeadersVisible = False
        Me.dgvPeticiones.Size = New System.Drawing.Size(566, 316)
        Me.dgvPeticiones.TabIndex = 0
        '
        'CCodigoDataGridViewTextBoxColumn
        '
        Me.CCodigoDataGridViewTextBoxColumn.DataPropertyName = "cCodigo"
        Me.CCodigoDataGridViewTextBoxColumn.HeaderText = "Comanda"
        Me.CCodigoDataGridViewTextBoxColumn.Name = "CCodigoDataGridViewTextBoxColumn"
        Me.CCodigoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ImpresoraDataGridViewTextBoxColumn
        '
        Me.ImpresoraDataGridViewTextBoxColumn.DataPropertyName = "Impresora"
        Me.ImpresoraDataGridViewTextBoxColumn.HeaderText = "Impresora"
        Me.ImpresoraDataGridViewTextBoxColumn.Name = "ImpresoraDataGridViewTextBoxColumn"
        Me.ImpresoraDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UltimaModiDataGridViewTextBoxColumn
        '
        Me.UltimaModiDataGridViewTextBoxColumn.DataPropertyName = "UltimaModi"
        Me.UltimaModiDataGridViewTextBoxColumn.HeaderText = "Modificación"
        Me.UltimaModiDataGridViewTextBoxColumn.Name = "UltimaModiDataGridViewTextBoxColumn"
        Me.UltimaModiDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MesaDataGridViewTextBoxColumn
        '
        Me.MesaDataGridViewTextBoxColumn.DataPropertyName = "Mesa"
        Me.MesaDataGridViewTextBoxColumn.HeaderText = "Mesa"
        Me.MesaDataGridViewTextBoxColumn.Name = "MesaDataGridViewTextBoxColumn"
        Me.MesaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UsuarioDataGridViewTextBoxColumn
        '
        Me.UsuarioDataGridViewTextBoxColumn.DataPropertyName = "Usuario"
        Me.UsuarioDataGridViewTextBoxColumn.HeaderText = "Usuario"
        Me.UsuarioDataGridViewTextBoxColumn.Name = "UsuarioDataGridViewTextBoxColumn"
        Me.UsuarioDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DtsPeticiones
        '
        Me.DtsPeticiones.DataSetName = "dtsPeticiones"
        Me.DtsPeticiones.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'bsPeticiones
        '
        Me.bsPeticiones.DataMember = "Peticiones"
        Me.bsPeticiones.DataSource = Me.DtsPeticiones
        '
        'PeticionesTableAdapter
        '
        Me.PeticionesTableAdapter.ClearBeforeFill = True
        '
        'niPeticiones
        '
        Me.niPeticiones.BalloonTipText = "Peticiones de Impresión "
        Me.niPeticiones.Icon = CType(resources.GetObject("niPeticiones.Icon"), System.Drawing.Icon)
        Me.niPeticiones.Text = "Peticiones de Impresión"
        Me.niPeticiones.Visible = True
        '
        'btPausa
        '
        Me.btPausa.Location = New System.Drawing.Point(12, 9)
        Me.btPausa.Name = "btPausa"
        Me.btPausa.Size = New System.Drawing.Size(75, 23)
        Me.btPausa.TabIndex = 2
        Me.btPausa.Text = "Pausar"
        Me.btPausa.UseVisualStyleBackColor = True
        '
        'DtsImprimeComanda1
        '
        Me.DtsImprimeComanda1.DataSetName = "DtsImprimeComanda"
        Me.DtsImprimeComanda1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DtsComandas1
        '
       
        'frmPeticiones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 379)
        Me.Controls.Add(Me.btPausa)
        Me.Controls.Add(Me.ssEstado)
        Me.Controls.Add(Me.dgvPeticiones)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPeticiones"
        Me.ShowInTaskbar = False
        Me.Text = "Peticiones de Impresión [Comada App]"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        Me.ssEstado.ResumeLayout(False)
        Me.ssEstado.PerformLayout()
        CType(Me.dgvPeticiones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsPeticiones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsPeticiones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsImprimeComanda1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvPeticiones As System.Windows.Forms.DataGridView
    Friend WithEvents tiRefrescamiento As System.Windows.Forms.Timer
    Friend WithEvents bwPeticiones As System.ComponentModel.BackgroundWorker
    Friend WithEvents ssEstado As System.Windows.Forms.StatusStrip
    Friend WithEvents lbEstadoObtencion As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lbEstadoImpresion As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents bwImpresion As System.ComponentModel.BackgroundWorker
    Friend WithEvents DtsPeticiones As Peticiones.dtsPeticiones
    Friend WithEvents bsPeticiones As System.Windows.Forms.BindingSource
    Friend WithEvents PeticionesTableAdapter As Peticiones.dtsPeticionesTableAdapters.PeticionesTableAdapter
    Friend WithEvents CCodigoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImpresoraDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimaModiDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents niPeticiones As System.Windows.Forms.NotifyIcon
    Friend WithEvents btPausa As System.Windows.Forms.Button
    Friend WithEvents lbRevisando As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents DtsImprimeComanda1 As Peticiones.DtsImprimeComanda

End Class
