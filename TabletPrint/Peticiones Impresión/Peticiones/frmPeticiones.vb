﻿Public Class frmPeticiones
    Dim sec As Integer = 0
    Dim espera As Integer = 0
    Dim secCola As Integer = 0
    Dim lapsoCola As Integer = 3
    Dim comanda As clsImpresionComanda
    Dim puntoVenta As String = ""
    Dim cMesa As Integer

    Dim impresora As String
    Dim EsReimpresion As Boolean = False

    Private Sub frmPeticiones_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        terminarProcesos()
        End
    End Sub
    Sub terminarProcesos()
        If bwPeticiones.IsBusy Then
            bwPeticiones.CancelAsync()
        End If
        If bwImpresion.IsBusy Then
            bwImpresion.CancelAsync()
        End If
    End Sub

    Private Sub frmPeticiones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
       iniciarForm()
    End Sub

    Private Sub spConecion()
Volver:
        Try
            Dim conn As New SqlClient.SqlConnection
            Dim cmd As New SqlClient.SqlCommand
            conn.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
            conn.Open()
            conn.Close()
            Exit Sub
        Catch ex As Exception
            GoTo Volver
        End Try
    End Sub
  
    Sub iniciarForm()
        Espera = My.Settings.Espera
        puntoVenta = GetSetting("SeeSOFT", "Restaurante", "LastPoint")
        informarEstadoImpresion(True, "Cargando Formatos")
        spConecion()

        If GetSetting("SeeSoft", "Restaurante", "TipoReporteComanda").Equals("") Then SaveSetting("SeeSoft", "Restaurante", "TipoReporteComanda", "0")
        If GetSetting("SeeSoft", "Restaurante", "TipoReporteComanda").Equals("0") Then
            comanda = New clsImpresionComanda
            comanda.inicializarComandas()
        Else
            '  rptComandadts = New rptComandas_dts
        End If

        tiRefrescamiento.Start()
        informarEstadoImpresion(False)
        realizarPeticiones()
        niPeticiones.ShowBalloonTip(100)
    End Sub

    Sub informarEstadoImpresion(estado As Boolean, Optional msj As String = "Imprimiendo...")
        lbEstadoImpresion.Text = msj
        lbEstadoImpresion.Visible = estado
    End Sub

    Private Sub tiRefrescamiento_Tick(sender As Object, e As EventArgs) Handles tiRefrescamiento.Tick
        If btPausa.Text.Equals(CONTINUAR) Then
            Exit Sub
        End If
        sec += 1
        If sec = espera Then
            sec = 0
            realizarPeticiones()
        End If
        secCola = secCola + 1
        lbRevisando.Text = "Revisando en:" & (lapsoCola - secCola)
        If secCola = lapsoCola Then
            secCola = 0
            ' If Not errorImp Then
            realizarImpresion()
            'End If

        End If
    End Sub

#Region "Envio de Impresion"
    Sub realizarImpresion()
        If bwImpresion.IsBusy Then Exit Sub
        bwImpresion.RunWorkerAsync()
    End Sub

    Private Function FnValidarReintentoImpresion(ByVal cComanda As Integer, ByVal Mesa As String) As Boolean
        Try
            If bsPeticiones.Current("Impreso") >= 2 Then

                Dim LastPoint As String

                LastPoint = GetSetting("SeeSoft", "Restaurante", "LastPoint")
                Dim rptError = New rptErrorPeticiones
                rptError.Refresh()
                rptError.PrintOptions.PrinterName = "COCINA"
                rptError.SetParameterValue(0, LastPoint)
                rptError.SetParameterValue(1, cComanda)
                rptError.SetParameterValue(2, Mesa)
                rptError.PrintToPrinter(1, True, 0, 0)
                actualizarComanda()
                actualizaImpresora()
                informarEstadoImpresion(False)
                errorImp = False
                Return False
            End If
            Return True
        Catch ex As Exception
            My.Computer.FileSystem.WriteAllText("C:\Test.txt", "-ERR rptError [" & Format(Now, "dd/MM/yy HH:mm:ss") & "]:" & ex.ToString, True)
            Return False
        End Try
        Return False
    End Function
    Dim errorImp As Boolean = False
    Dim Imprimiendo As Boolean = False
    Sub imprimirPeticion()
        Try
            If bsPeticiones.Count = 0 Then Exit Sub
            If bsPeticiones.Current("EnCurso") Then Exit Sub
            Imprimiendo = True
            bsPeticiones.Current("EnCurso") = True
            informarEstadoImpresion(True)
            niPeticiones.Text = "Imprimiendo Comanda Movil..."
            cMesa = bsPeticiones.Current("cMesa")
            impresora = bsPeticiones.Current("Impresora")
            If fnVerifica() Then
                If FnValidarReintentoImpresion(cMesa, bsPeticiones.Current("Mesa")) Then
                    If imprimirComanda(EsReimpresion) Then
                        actualizarComanda()
                        informarEstadoImpresion(False)
                        errorImp = False
                    Else
                        errorImp = True
                        informarEstadoImpresion(True, "Error al imprimir")

                    End If
                End If
            Else
                informarEstadoImpresion(False)
            End If
            bsPeticiones.Current("EnCurso") = False
            Imprimiendo = False
        Catch ex As Exception
            Imprimiendo = False
        End Try
    End Sub

    Function fnVerifica() As Boolean


        Dim dt As New DataTable
        Dim conn As New SqlClient.SqlConnection
        Dim cmd As New SqlClient.SqlCommand
        Dim adap As New SqlClient.SqlDataAdapter(cmd)
        conn.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
        conn.Open()
        cmd.Connection = conn
        cmd.CommandText = "Select * From ComandaTemporal INNER JOIN Peticiones On ComandaTemporal.cCodigo = peticiones.cCodigo WHERE ComandaTemporal.cMesa = @cMesa AND ComandaTemporal.Impresora = @Impresora"
        cmd.Parameters.AddWithValue("@cMesa", cMesa)
        cmd.Parameters.AddWithValue("@Impresora", impresora)
        adap.SelectCommand = cmd
        adap.Fill(dt)
        If dt.Rows.Count > 0 Then
            If dt.Select("Impreso = -2").Count > 0 Then
                actualizarComandaReimpresion()
                EsReimpresion = True
            End If
            Return True
        Else
            Return False
        End If

    End Function
    Sub actualizarComandaReimpresion()
        Dim conn As New SqlClient.SqlConnection
        Dim cmd As New SqlClient.SqlCommand
        conn.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
        conn.Open()
        cmd.Connection = conn
        cmd.CommandText = "Update ComandaTemporal set impreso=0 where Estado='Activo' and cMesa= @cMesa"
        cmd.Parameters.AddWithValue("@cMesa", cMesa)
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub


    Sub actualizarComanda()
        Dim conn As New SqlClient.SqlConnection
        Dim cmd As New SqlClient.SqlCommand
        conn.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
        conn.Open()
        cmd.Connection = conn
        cmd.CommandText = "UPDATE ComandaTemporal Set Impreso = cCantidad WHERE Estado = 'Activo' AND Comandado = 1 AND cMesa = @cMesa AND Impresora = @Impresora"
        cmd.Parameters.AddWithValue("@cMesa", cMesa)
        cmd.Parameters.AddWithValue("@Impresora", impresora)
        cMesa = 0
        impresora = ""
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub
    Sub actualizaImpresora()
        Dim conn As New SqlClient.SqlConnection
        Dim cmd As New SqlClient.SqlCommand
        conn.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
        conn.Open()
        cmd.Connection = conn
        cmd.CommandText = "UPDATE ComandaTemporal Set Impresora = 'COCINA' WHERE cMesa = @cMesa AND Len(Impresora) > 2"
        cmd.Parameters.AddWithValue("@cMesa", cMesa)
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub


    Private Function FnCargarDatosImpresionComanda(ByVal _cMesa As String, ByVal _Impresora As String) As Boolean
        Dim conn As New SqlClient.SqlConnection
        Dim sql As New SqlClient.SqlCommand
        Dim adap As New SqlClient.SqlDataAdapter(sql)

        Try

            DtsImprimeComanda1.Tb_ComandaTemporal.Clear()
            DtsImprimeComanda1.Tb_Mesa.Clear()

            conn.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
            conn.Open()
            sql.Connection = conn
            sql.CommandText = "SELECT dbo.ComandaTemporal.cProducto ,dbo.ComandaTemporal.Habitacion,dbo.ComandaTemporal.Impreso,dbo.ComandaTemporal.Estado,  dbo.ComandaTemporal.cCantidad ,  dbo.ComandaTemporal.cDescripcion ," & _
                              " dbo.ComandaTemporal.Habitacion, dbo.ComandaTemporal.primero, dbo.ComandaTemporal.Express,dbo.ComandaTemporal.Solicitud " & _
                              " FROM dbo.ComandaTemporal INNER JOIN dbo.Usuarios ON dbo.ComandaTemporal.Cedula = dbo.Usuarios.Cedula INNER JOIN " & _
                              " dbo.Mesas ON dbo.ComandaTemporal.cMesa = dbo.Mesas.Id INNER JOIN dbo.Grupos_Mesas ON dbo.Mesas.Id_GrupoMesa = dbo.Grupos_Mesas.id " & _
                              " where ComandaTemporal.Estado = 'ACTIVO' and ComandaTemporal.Impreso = 0 and (UPPER(ComandaTemporal.Impresora) = UPPER(@PImpresora)) and (ComandaTemporal.cMesa = @cMesa) "
            sql.Parameters.AddWithValue("@PImpresora", _Impresora)
            sql.Parameters.AddWithValue("@cMesa", _cMesa)
            adap.SelectCommand = sql
            adap.Fill(DtsImprimeComanda1.Tb_ComandaTemporal) 'DOS?


            sql.CommandText = "SELECT    distinct  top 1 dbo.Mesas.Nombre_Mesa , dbo.Grupos_Mesas.Nombre_Grupo,dbo.Usuarios.Nombre , dbo.ComandaTemporal.Id" & _
                              " FROM dbo.ComandaTemporal INNER JOIN dbo.Usuarios ON dbo.ComandaTemporal.Cedula = dbo.Usuarios.Cedula INNER JOIN " & _
                              " dbo.Mesas ON dbo.ComandaTemporal.cMesa = dbo.Mesas.Id INNER JOIN dbo.Grupos_Mesas ON dbo.Mesas.Id_GrupoMesa = dbo.Grupos_Mesas.id " & _
                              " where (UPPER(ComandaTemporal.Impresora) = UPPER(@PImpresora)) and (ComandaTemporal.cMesa = @cMesa) order by dbo.ComandaTemporal.Id desc"
            adap.SelectCommand = sql
            adap.Fill(DtsImprimeComanda1.Tb_Mesa) 'Esto es lo muestra en el grid?
            Return True
        Catch ex As Exception
            My.Computer.FileSystem.WriteAllText("C:\Test.txt", "-ERR [" & Format(Now, "dd/MM/yy HH:mm:ss") & "]:" & ex.Message, True)
            Dim cant As Integer = bsPeticiones.Current("Impreso")
            bsPeticiones.Current("Impreso") = cant + 1
            bsPeticiones.EndEdit()
            Return False
        End Try
    End Function

    Private Function imprimirComanda(ByVal Reimpresion As Boolean) As Boolean
      If GetSetting("SeeSoft", "Restaurante", "TipoReporteComanda").Equals("0") Then
            Return spReporteConConexionSql(Reimpresion)
        Else
            Return spReporteConDTS(Reimpresion)
        End If
    End Function
    Private Function spReporteConDTS(ByVal Reimpresion As Boolean) As Boolean

        If GetSetting("SeeSoft", "Restaurante", "NoComanda").Equals("1") Then Return True
        Dim ImpresoraBar As String = ""
        If impresora.IndexOf("BAR") > 0 Then
            ImpresoraBar = "BAR"
        End If
        Try
            Dim rptComandadts As New rptComandas_dts
            If GetSetting("SeeSoft", "Restaurante", "noComanda") = "2" And ImpresoraBar.Equals("BAR", StringComparison.OrdinalIgnoreCase) Then Exit Function
            If GetSetting("SeeSoft", "Restaurante", "Impresion") = "1" Then
                If FnCargarDatosImpresionComanda(cMesa, impresora) Then
                    rptComandadts.Refresh()
                    rptComandadts.SetDataSource(DtsImprimeComanda1)
                    rptComandadts.PrintOptions.PrinterName = impresora
                    rptComandadts.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rptComandadts.SetParameterValue(0, bsPeticiones.Current("cCodigo"))
                    rptComandadts.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                    If Reimpresion = True Then
                        rptComandadts.SetParameterValue(2, "REIMPRESIÓN")
                    Else
                        rptComandadts.SetParameterValue(2, "")
                    End If
                    Dim Peticiones As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                    rptComandadts.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                    rptComandadts.PrintToPrinter(1, True, 0, 0)

                End If
            Else
                If FnCargarDatosImpresionComanda(cMesa, impresora) Then
                    rptComandadts.Refresh()
                    rptComandadts.SetDataSource(DtsImprimeComanda1)
                    rptComandadts.PrintOptions.PrinterName = impresora
                    rptComandadts.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rptComandadts.SetParameterValue(0, bsPeticiones.Current("cCodigo"))
                    rptComandadts.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                    If Reimpresion = True Then
                        rptComandadts.SetParameterValue(2, "REIMPRESIÓN")
                    Else
                        rptComandadts.SetParameterValue(2, "")
                    End If
                    Dim Peticiones As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                    rptComandadts.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                    rptComandadts.PrintToPrinter(1, True, 0, 0)

                End If
            End If
            EsReimpresion = False
        Catch ex As Exception
            'ESCRIBIR LOG DE ERRRORES

            My.Computer.FileSystem.WriteAllText("C:\Test.txt", "-ERR rptDTS [" & Format(Now, "dd/MM/yy HH:mm:ss") & "]:" & ex.ToString, True)
            Dim cant As Integer = bsPeticiones.Current("Impreso")
            bsPeticiones.Current("Impreso") = cant + 1
            bsPeticiones.EndEdit()
            Return False
        End Try
        Return True
    End Function

    Private Function spReporteConConexionSql(ByVal Reimpresion As Boolean) As Boolean

        If GetSetting("SeeSoft", "Restaurante", "NoComanda").Equals("1") Then Return True
        Dim ImpresoraBar As String = ""
        If impresora.IndexOf("BAR") > 0 Then
            ImpresoraBar = "BAR"
        End If
        Try
            If GetSetting("SeeSoft", "Restaurante", "noComanda") = "2" And ImpresoraBar.Equals("BAR", StringComparison.OrdinalIgnoreCase) Then Exit Function
            If GetSetting("SeeSoft", "Restaurante", "Impresion") = "1" Then
                comanda.rptComanda.Refresh()
                comanda.rptComanda.PrintOptions.PrinterName = impresora
                comanda.rptComanda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                comanda.rptComanda.SetParameterValue(0, cMesa)
                comanda.rptComanda.SetParameterValue(1, 0)
                comanda.rptComanda.SetParameterValue(2, impresora)
                comanda.rptComanda.SetParameterValue(3, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                If Reimpresion = True Then
                    comanda.rptComanda.SetParameterValue(4, "REIMPRESIÓN")
                Else
                    comanda.rptComanda.SetParameterValue(4, "")
                End If
                comanda.rptComanda.PrintToPrinter(1, True, 0, 0)

            Else
                comanda.rptComandaIzquierda.Refresh()
                comanda.rptComandaIzquierda.PrintOptions.PrinterName = impresora 'Automatic_Printer_Dialog(3) 'FACTURACION
                comanda.rptComandaIzquierda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                comanda.rptComandaIzquierda.SetParameterValue(0, cMesa)
                comanda.rptComandaIzquierda.SetParameterValue(1, 0)
                comanda.rptComandaIzquierda.SetParameterValue(2, impresora)
                comanda.rptComandaIzquierda.SetParameterValue(3, puntoVenta)
                If Reimpresion = True Then
                    comanda.rptComandaIzquierda.SetParameterValue(4, "REIMPRESIÓN")
                Else
                    comanda.rptComandaIzquierda.SetParameterValue(4, "")
                End If
                comanda.rptComandaIzquierda.PrintToPrinter(1, True, 0, 0)
            End If
            EsReimpresion = False
        Catch ex As Exception
            'ESCRIBIR LOG DE ERRRORES

            My.Computer.FileSystem.WriteAllText("C:\Test.txt", "-ERR rtpSQL [" & Format(Now, "dd/MM/yy HH:mm:ss") & "]:" & ex.ToString, True)
            Dim cant As Integer = bsPeticiones.Current("Impreso")
            bsPeticiones.Current("Impreso") = cant + 1
            bsPeticiones.EndEdit()
            Return False
        End Try
        Return True
    End Function

#End Region
#Region "Consulta de Peticiones"
    Sub obtenerPeticiones()

        Try
            If Imprimiendo Then Exit Sub
            informarEstadoObtencion(True)
            Me.PeticionesTableAdapter.Connection.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
            Me.PeticionesTableAdapter.Fill(Me.DtsPeticiones.Peticiones)
            informarEstadoObtencion(False)


        Catch ex As Exception
            My.Computer.FileSystem.WriteAllText("C:\Test.txt", "-ERR-obtenerPeticiones [" & Format(Now, "dd/MM/yy HH:mm:ss") & "]:" & ex.Message, True)
        End Try
    End Sub

    Private Sub bwPeticiones_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwPeticiones.DoWork

        If Not errorImp Then
            obtenerPeticiones()
        End If

    End Sub

    Sub realizarPeticiones()
        If (Not bwPeticiones.IsBusy) And (Not bwImpresion.IsBusy) Then
            Me.dgvPeticiones.DataMember = ""
            Me.dgvPeticiones.DataSource = ""
            bwPeticiones.RunWorkerAsync()
        End If
    End Sub

    Sub informarEstadoObtencion(activa As Boolean)
        lbEstadoObtencion.Visible = activa
    End Sub

    Private Sub bwPeticiones_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwPeticiones.RunWorkerCompleted
        Me.dgvPeticiones.DataSource = DtsPeticiones
        Me.dgvPeticiones.DataMember = "Peticiones"
        dgvPeticiones.Refresh()
    End Sub
#End Region


    Private Sub bwImpresion_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwImpresion.DoWork
        imprimirPeticion()

    End Sub


    Private Sub niPeticiones_Click(sender As Object, e As EventArgs) Handles niPeticiones.Click
        WindowState = FormWindowState.Normal
        BringToFront()

    End Sub
    Private Const CONTINUAR As String = "Continuar"
    Private Const PAUSAR As String = "Pausar"

    Private Sub btPausa_Click(sender As Object, e As EventArgs) Handles btPausa.Click
        If btPausa.Text.Equals(PAUSAR) Then
            btPausa.Text = CONTINUAR
        Else
            btPausa.Text = PAUSAR
        End If
    End Sub

End Class
